-- Release Build Steps:

1) ionic cordova build --release --prod android

2) jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore lotus_laundary.keystore app-release-unsigned.apk lotus_laundary

Password - laundary@lotus.com

3) zipalign -v 4 "app-release-unsigned.apk" "Lotus Laundary.apk"



-- Generating Keystore

D:\Projects\git\paytrac_wallet\docs>keytool -genkey -v -keystore lotus_laundary.keystore -alias lotus_laundary -keyalg RSA -keysize 2048 -validity 10000
Enter keystore password:
Re-enter new password:
What is your first and last name?
  [Unknown]:  Lotus Laundary
What is the name of your organizational unit?
  [Unknown]:  Iotasol
What is the name of your organization?
  [Unknown]:  Iotasol
What is the name of your City or Locality?
  [Unknown]:  Iotasol
What is the name of your State or Province?
  [Unknown]:  Iotasol
What is the two-letter country code for this unit?
  [Unknown]:  Iotasol
Is CN="Sliptrac+ Wallet", OU=NBB Solutions, O=NBB Solutions, L=Plympton Wyoming, ST=Ontario, C=CA correct?
  [no]:  y

  Generating 2,048 bit RSA key pair and self-signed certificate (SHA256withRSA) with a validity of 10,000 days
          for: CN="Sliptrac+ Wallet", OU=NBB Solutions, O=NBB Solutions, L=Plympton Wyoming, ST=Ontario, C=CA
  Enter key password for <sliptrac>
        (RETURN if same as keystore password): [Press Enter]

Warning:
The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using "keytool -importkeystore -srckeystore splitrac.keystore -destkeystore splitrac.keystore -deststoretype pkcs12".


-- REQUIRED INFORMATION
Common name: Iotasol
Organization: Iotasol
City: Iotasol
State/Province: Iotasol
Country/region: Iotasol


**STEPS FOR FOLLOW BEFORE CREATING NEW BUILD**
1) In AndroidMenifest.xml file replace  android:windowSoftInputMode="adjustResize" with android:windowSoftInputMode="adjustPan"
2) Change versions after colon(:) to +
    FROM:
    cordova.system.library.1=androidx.annotation:annotation:1.0.0
    cordova.system.library.2=androidx.legacy:legacy-support-v4:1.0.0
    TO:
    cordova.system.library.1=androidx.annotation:annotation:+
    cordova.system.library.2=androidx.legacy:legacy-support-v4:+

