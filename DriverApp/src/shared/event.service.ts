import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Event} from '../app/dto/event';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    private eventObservable = new BehaviorSubject<Event>(null);
    event = this.eventObservable.asObservable();

    publish(event: Event) {
        this.eventObservable.next(event);
    }
}
