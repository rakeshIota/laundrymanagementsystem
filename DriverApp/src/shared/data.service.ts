import {Injectable} from '@angular/core';
import {Language} from 'src/app/models/language.model';
import {LanguageService} from 'src/app/services/language.service';
import {isNullOrUndefined} from 'util';
import {RestResponse} from '../app/models/authorization.model';
import {AppSettingsService} from '../app/services/app.settings.service';
import {ToastService} from './toast.service';
import {CartService} from '../app/services/cart.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    appSettings: Array<any>;
    languages: Language[];
    cartItemTotal: number;

    constructor(private appSettingService: AppSettingsService, private toastService: ToastService, private languageService: LanguageService,
                private cartService: CartService) {
        this.languages = [] as Language[];
    }

    async init() {
        try {
            const response: RestResponse = await this.appSettingService.fetch().toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.appSettings = response.data;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async getAppSettings() {
        if (!isNullOrUndefined(this.appSettings)) {
            return this.appSettings;
        }
        await this.init();
        return this.appSettings;
    }

    async fetchAllLanguages() {
        try {
            const response: RestResponse = await this.languageService.fetchAll().toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.languages = response.data;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async getLanguages() {
        await this.fetchAllLanguages();
        return this.languages;
    }
}
