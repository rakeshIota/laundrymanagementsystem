import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {

    constructor(private loadingController: LoadingController) {
    }

    async presentLoading(input?: string) {
        const loading = await this.loadingController.create({
            message: '<ion-img src="/assets/images/loading.png" alt="loading..."></ion-img>',
            // message: isNullOrUndefined(input) ? '' : input,
            spinner: null,
            translucent: true,
        });
        await loading.present();
        return loading;
    }

    hideLoading(loading) {
        if (loading && loading !== null) {
            loading.then((response) => {
                response.dismiss();
            });
        }
    }

    setContent(loading, message) {
        if (loading && loading !== null) {
            loading.then((response) => {
                response.message = message;
            });
        }
    }
}
