import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Language } from 'src/app/models/language.model';
import { isNullOrUndefined } from 'util';
import { AuthService } from './auth/auth.service';
import { EventService } from './event.service';
import { LocalStorageService } from './local.storage.service';
import { environment } from '../environments/environment';
import { ToastService } from './toast.service';

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {
    constructor(private localStorageService: LocalStorageService, private eventService: EventService,
        private authService: AuthService, private toastService: ToastService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.authService.getToken();
        const headers = {} as any;
        const defaultLanguage: Language = JSON.parse(this.localStorageService.get('DEFAULT_LANGUAGE'));
        if (!isNullOrUndefined(defaultLanguage)) {
            headers.Language = defaultLanguage.id;
        }

        const startUrl = environment.BaseApiUrl + '/app/';
        const isAuthorizationRequest = req.url.startsWith(startUrl);
        const isLogoutRequest = req.url.endsWith('/logout');
        if (!isNullOrUndefined(token) && token.expires && new Date(token.expires).getTime() > new Date().getTime() && !isAuthorizationRequest) {
            headers.Authorization = 'Bearer ' + token.accessToken;
        }
        const authReq = req.clone({ setHeaders: headers });
        return next.handle(authReq).pipe(catchError((error) => {
            if (!isLogoutRequest && error.status === 401 || error.error.message === 'Un-Authorized Request') {
                this.authService.logout();
                this.eventService.publish({ key: 'http:forbidden', value: 'Session Expired' });
            }
            if (error.status === 504 || error.status === 503) {
                this.toastService.error("Application Server currently unavailable. Please try later.");
            }
            return throwError(error);
        }));
    }

    getMinutesBetweenDates(endDate, startDate) {
        return ((endDate - startDate) / 60000);
    }
}
