import {Injectable} from '@angular/core';
import {Platform} from '@ionic/angular';
import {Toast, ToastOptions} from '@ionic-native/toast/ngx';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    constructor(private toast: Toast, private platform: Platform) {
    }

    show(message: string): void {
        console.log(message);
        const option = {} as ToastOptions;
        option.message = message;
        option.duration = 12000;
        option.position = 'bottom';
        if (this.platform.is('android')) {
            option.styling = {
                cornerRadius: '50%',
                backgroundColor: '#00BAFF',
                color: '#fff'
            } as any;
        }
        this.toast.showWithOptions(option)
            .subscribe(() => {
            });
    }

    error(message: string): void {
        console.log(message);
        const option = {} as ToastOptions;
        option.message = message;
        option.duration = 12000;
        option.position = 'bottom';
        if (this.platform.is('android')) {
            option.styling = {
                cornerRadius: '50%',
                backgroundColor: '#cc0000',
                color: '#fff'
            } as any;
        }
        this.toast.showWithOptions(option)
            .subscribe(() => {
            });
    }
}
