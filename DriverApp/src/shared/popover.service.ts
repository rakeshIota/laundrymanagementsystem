import {Injectable} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {PopoverComponent} from 'src/app/components/popover/popover.component';

@Injectable({
    providedIn: 'root'
})
export class PopoverService {
    popover: HTMLIonPopoverElement;

    constructor(private popoverController: PopoverController) {
    }

    async passwordRulePopover(iEvent) {
        this.popover = await this.popoverController.create({
            component: PopoverComponent,
            event: iEvent,
            translucent: true,
            showBackdrop: false,
            mode: 'md'
        });
        return await this.popover.present();
    }

    async dismissPopover() {
        this.popover.dismiss();
    }
}
