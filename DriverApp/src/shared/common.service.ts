import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {isNullOrUndefined} from 'util';

@Injectable({
    providedIn: 'root'
})

export class CommonService {
    hasTwilioInitialize: boolean;

    constructor(private alert: AlertController) {
    }

    async info(title, body, confirmButtonText, callbackConfirm, targetId) {
        const options = {
            header: title,
            message: body,
            cssClass: 'info-alert',
            buttons: [
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm(targetId);
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    async confirm(title, body, cancelButtonText, confirmButtonText, callbackConfirm, targetId) {
        const options = {
            header: title,
            message: body,
            buttons: [
                {
                    text: cancelButtonText,
                    role: 'cancel',
                    cssClass: 'disagreeBtn',
                    handler: () => {
                        alert.dismiss();
                    }
                },
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm(targetId);
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    async confirmWithCallback(title, body, cancelButtonText, confirmButtonText, callbackConfirm, callbackCancel) {
        const options = {
            header: title,
            message: body,
            buttons: [
                {
                    text: cancelButtonText,
                    role: 'cancel',
                    cssClass: 'disagreeBtn',
                    handler: () => {
                        alert.dismiss();
                        callbackCancel();
                    }
                },
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm();
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    locationPopup() {
        this.alert.create({
            header: 'Need Location',
            message: 'We need a your location for better exprience. Please change settings',
            cssClass: 'custom-model',
            buttons: ['Dismiss']
        }).then(alert => alert.present());
    }

    filterRecords(inputRecords) {
        const temp = {};
        const filterRecords = inputRecords.reduce((records, record) => {
            if (temp.hasOwnProperty(record.service.id)) {
                records[temp[record.service.id]].push(record);
            } else {
                temp[record.service.id] = records.length;
                records.push([record]);
            }
            return records;
        }, []);
        return filterRecords;
    }

    testSingleOrderData() {
        return {
            id: 'UTfAkhfKNCrezpgDKA53cQ5AHAM83d5AHAM83d',
            orderId: '000001',
            total: 485,
            subtotal: 420,
            tax: 0,
            delivery: 0,
            deliveryType: 'NORMAL',
            items: [
                {
                    product: {
                        id: '1',
                        name: 'T-Shirt',
                        icon: '/assets/images/tshirt-image.png'
                    },
                    price: 48.00,
                    quantity: 8,
                    service: {
                        id: '1',
                        name: 'Dry Cleaning'
                    }
                },
                {
                    product: {
                        id: '2',
                        name: 'Shirt',
                        icon: '/assets/images/shirt-image.png'
                    },
                    price: 48.00,
                    quantity: 6,
                    service: {
                        id: '1',
                        name: 'Dry Cleaning'
                    }
                },
                {
                    product: {
                        id: '3',
                        name: 'Coat',
                        icon: '/assets/images/flat-image.png',
                    },
                    price: 25.39,
                    quantity: 5,
                    service: {
                        id: '1',
                        name: 'Dry Cleaning'
                    }
                },
                {
                    product: {
                        id: '1',
                        name: 'T-Shirt',
                        icon: '/assets/images/tshirt-image.png'
                    },
                    price: 20.00,
                    quantity: 2,
                    service: {
                        id: '2',
                        name: 'Wash & Iron'
                    }
                },
                {
                    product: {
                        id: '2',
                        name: 'Shirt',
                        icon: '/assets/images/shirt-image.png'
                    },
                    price: 58.55,
                    quantity: 2,
                    service: {
                        id: '2',
                        name: 'Wash & Iron'
                    }
                },
                {
                    product: {
                        id: '3',
                        name: 'Coat',
                        icon: '/assets/images/flat-image.png',
                    },
                    price: 12.00,
                    quantity: 2,
                    service: {
                        id: '2',
                        name: 'Wash & Iron'
                    }
                }
            ],
            price: 48.00,
            paid: true,
            paymentType: 'CARD',
            createdDate: new Date(),
            status: 'ORDER_PLACED'
        };
    }
}
