import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    constructor() {
    }

    set(key: string, value: any) {
        localStorage.setItem(key, value);
    }

    get(key) {
        return localStorage.getItem(key);
    }

    remove(key) {
        return localStorage.removeItem(key);
    }

    hasOwnProperty(key) {
        return localStorage.hasOwnProperty(key);
    }
}

