import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {NavController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate {
    constructor(
        private navController: NavController,
        private authService: AuthService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const roles = route.data.roles as Array<string>;
        return this.authService.isAuthorizedUser(roles).then((response: any) => {
            if (roles.indexOf('ROLE_ANONYMOUS') !== -1) {
                if (response.hasAccess) {
                    this.navController.navigateRoot(['landing']);
                    return false;
                }
                return true;
            }
            if (!response.hasAccess || !response.hasRoleAccess) {
                this.authService.logout();
                this.navController.navigateRoot(['login'], {queryParams: {redirectTo: state.url}});
                return false;
            }
            return true;
        });
    }
}
