import {Injectable} from '@angular/core';
import {User} from 'src/app/models/user.model';
import {isNullOrUndefined} from 'util';
import {AuthToken} from '../../app/models/authorization.model';
import {LocalStorageService} from '../local.storage.service';
import {UserService} from '../../app/services/user.service';
import {CommonService} from '../common.service';

declare const Twilio: any;

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    authToken: AuthToken;
    user: User;
    roles: Array<string>;
    deviceId: string;

    constructor(private localStorageService: LocalStorageService, private userService: UserService, private commonService: CommonService) {
    }

    getToken(): AuthToken {
        if (!isNullOrUndefined(this.authToken)) {
            return this.authToken;
        }
        if (!this.localStorageService.hasOwnProperty('token')) {
            return null;
        }
        return JSON.parse(this.localStorageService.get('token'));
    }

    getRoles() {
        if (!isNullOrUndefined(this.roles)) {
            return this.roles;
        }
        if (!this.localStorageService.hasOwnProperty('roles')) {
            return null;
        }
        return JSON.parse(this.localStorageService.get('roles'));
    }

    getUser() {
        if (!isNullOrUndefined(this.user)) {
            return this.user;
        }
        if (!this.localStorageService.hasOwnProperty('user')) {
            return null;
        }
        return JSON.parse(this.localStorageService.get('user'));
    }

    updateUserOnLocal(userToUpdate: User) {
        if (isNullOrUndefined(userToUpdate)) {
            return;
        }
        this.user = JSON.parse(JSON.stringify(userToUpdate));
        if (this.localStorageService.hasOwnProperty('user')) {
            this.localStorageService.set('user', JSON.stringify(userToUpdate));
        }
    }

    hasValidToken() {
        const token: AuthToken = this.getToken();
        return !isNullOrUndefined(token) && token.expires_at && token.expires_at > new Date().getTime();
    }

    hasRoles(roles: Array<string>) {
        if (!roles || roles.length === 0) {
            return true;
        }
        const userRoles: any = this.getRoles();
        if (userRoles == null) {
            return false;
        }
        const access = roles.some((e) => {
            return userRoles.indexOf(e) >= 0;
        });
        return access;
    }

    async logout() {
        this.commonService.hasTwilioInitialize = false;
        if (!isNullOrUndefined(Twilio.TwilioVoiceClient)) {
            Twilio.TwilioVoiceClient.destroy();
        }
        try {
            if (isNullOrUndefined(this.deviceId)) {
                return;
            }
            await this.userService.logout(this.deviceId);
            this.clearUserDetail();
        } catch (e) {
            console.log(e.message);
            this.clearUserDetail();
        }
    }

    clearUserDetail() {
        this.authToken = null;
        this.localStorageService.remove('token');
        this.roles = null;
        this.localStorageService.remove('roles');
        this.user = null;
        this.localStorageService.remove('user');
    }

    isAuthorizedUser(roles: Array<string>) {
        const promise = new Promise(
            (resolve, reject) => {
                if (!this.hasValidToken()) {
                    this.logout();
                }
                resolve({hasAccess: this.hasValidToken(), hasRoleAccess: this.hasRoles(roles)});
            }
        );
        return promise;
    }
}
