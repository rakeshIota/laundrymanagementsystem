import {Routes} from '@angular/router';
import {AuthGuard} from '../shared/auth/auth.guard';

export const ROUTES: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {
        path: 'language',
        loadChildren: './pages/language/language.module#LanguagePageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'login',
        loadChildren: './pages/authorization/login/login.module#LoginPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'forgot-password',
        loadChildren: './pages/authorization/forgot-password/forgot-password.module#ForgotPasswordPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'otp-verification',
        loadChildren: './pages/authorization/otp-verification/otp-verification.module#OtpVerificationPageModule',
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'landing', loadChildren: './pages/landing/landing.module#LandingPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'my/orders', loadChildren: './pages/landing/landing.module#LandingPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'map', loadChildren: './pages/map-view/map-view.module#MapViewPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'marker/detail', loadChildren: './pages/map-view/marker-detail/marker-detail.module#MarkerDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'account/settings',
        loadChildren: './pages/account-settings/account-settings.module#AccountSettingsPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'user-settings',
        loadChildren: './pages/account-settings/user-settings/user-settings.module#UserSettingsPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'reset-password',
        loadChildren: './pages/account-settings/verify-password/reset-password/reset-password.module#ResetPasswordPageModule'
    },
    {
        path: 'reset-successful',
        loadChildren: './pages/account-settings/verify-password/reset-successful/reset-successful.module#ResetSuccessfulPageModule'
    },
    {
        path: 'completed/orders', loadChildren: './pages/completed-orders/completed-orders.module#CompletedOrdersPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/basic/detail',
        loadChildren: './pages/order/basic-detail/order-basic-detail.module#OrderBasicDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'notification/order/:orderId/basic/detail',
        loadChildren: './pages/order/basic-detail/order-basic-detail.module#OrderBasicDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/basic/detail',
        loadChildren: './pages/order/basic-detail/order-basic-detail.module#OrderBasicDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    }, {
        path: 'order/:orderId/detail', loadChildren: './pages/order/details/order-details.module#OrderDetailsPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/chat/:me/:from',
        loadChildren: './pages/chat/chat.module#ChatPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/tracking',
        loadChildren: './pages/order/details/order-tracking/order-tracking.module#OrderTrackingPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/tracking/basic',
        loadChildren: './pages/order/details/order-tracking-basic/order-tracking-basic.module#OrderTrackingBasicPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/:orderId/success',
        loadChildren: './pages/order/confirmation/order-confirmation.module#OrderConfirmationPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    },
    {
        path: 'order/qr-code',
        loadChildren: './pages/qr-code-detail/qr-code-detail.module#QrCodeDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_DRIVER']}
    }
];
