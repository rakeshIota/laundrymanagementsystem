import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Header} from 'src/app/models/header.model';
import {DataService} from '../../../shared/data.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
    @Input() header: Header;
    @Output() closeModalEmitter;


    constructor(private navController: NavController, public dataService: DataService) {
        this.closeModalEmitter = new EventEmitter<boolean>();
    }

    ngOnInit() {
    }

    dismissModal() {
        this.closeModalEmitter.emit(true);
    }

    loadCart() {
        this.navController.navigateRoot('cart');
    }
}
