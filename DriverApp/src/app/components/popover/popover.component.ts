import {Component, OnInit} from '@angular/core';
import {PasswordRule} from 'src/app/models/common.model';
import {ModalService} from 'src/shared/modal.service';
import {PopoverService} from '../../../shared/popover.service';

@Component({
    selector: 'app-popover',
    templateUrl: './popover.component.html',
    styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {
    passwordRule: PasswordRule;

    constructor(private popoverService: PopoverService) {
    }

    ngOnInit() {
        this.passwordRule = new PasswordRule();
        this.passwordRule.lowercaseLetters = `a-z`;
        this.passwordRule.uppercaseLetters = `A-Z`;
        this.passwordRule.numbers = `0-9`;
        this.passwordRule.specialCharacters = `-!$%@^&*#()_+|~={}[]:\";'<>?,./`;
    }

    close() {
        this.popoverService.dismissPopover();
    }
}