import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {Address} from '../../models/user.model';
import {RestResponse} from '../../models/authorization.model';
import {ToastService} from '../../../shared/toast.service';
import {AddressService} from '../../services/address.service';
import {District} from '../../models/district.model';
import {SubDistrict} from '../../models/sub-district.model';
import {LoadingService} from '../../../shared/loading.service';
import {ModalService} from '../../../shared/modal.service';
import {CommonService} from '../../../shared/common.service';

@Component({
    selector: 'app-addresses',
    templateUrl: './addresses.component.html',
    styleUrls: ['./addresses.component.scss'],
})
export class AddressesComponent implements OnInit {
    @Input()
    data: Address;
    onClickValidation: boolean;
    address: Address;
    actionSheetOptions: HTMLIonActionSheetElement;
    districts: Array<District>;
    subDistricts: Array<SubDistrict>;
    provinces: Array<any>;

    constructor(private translateService: TranslateService, private toastService: ToastService, private commonService: CommonService,
                private addressService: AddressService, private loadingService: LoadingService, private modalService: ModalService) {
        this.onClickValidation = false;
        this.address = new Address();
    }

    async ngOnInit() {
        this.actionSheetOptions = {} as HTMLIonActionSheetElement;
        const selectValue = await this.translateService.get('SELECT_LABEL').toPromise();
        this.actionSheetOptions.header = selectValue;
        if (!isNullOrUndefined(this.data)) {
            this.address = this.data;
        }
        if (isNullOrUndefined(this.address.residenceType)) {
            this.address.residenceType = 'HOME';
        }
        await this.fetchDistricts();
        await this.fetchProvince();
        if (this.address.provinceId) {
            await this.onProvinceSelection(this.address.provinceId);
        }
        if (this.address.districtId) {
            await this.onDistrictSelection(this.address.districtId, false);
            await this.onSubDistrictSelection(this.address.subDistrictId);
        }
    }

    async fetchProvince() {
        try {
            this.provinces = new Array<any>();
            const response: RestResponse = await this.addressService.fetchProvince().toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.provinces = response.data;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    onProvinceSelection(provinceId: string) {
        const selectedProvince = this.provinces.find(x => x.id === provinceId);
        if (!isNullOrUndefined(selectedProvince)) {
            this.address.province = selectedProvince.name;
        }
    }

    async fetchDistricts() {
        try {
            this.districts = new Array<District>();
            const response: RestResponse = await this.addressService.fetchDistricts().toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.districts = response.data;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async onDistrictSelection(districtId: string, ui: boolean) {
        try {
            const selectedDistrict = this.districts.find(x => x.id === districtId);
            if (!isNullOrUndefined(selectedDistrict)) {
                this.address.district = selectedDistrict.name;
            }
            if (ui) {
                this.address.subDistrict = undefined;
            }
            this.subDistricts = new Array<District>();
            const response: RestResponse = await this.addressService.fetchSubDistricts(districtId).toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.subDistricts = response.data;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async onSubDistrictSelection(subDistrictId: string) {
        const selectedSubDistrict = this.subDistricts.find(x => x.id === subDistrictId);
        if (!isNullOrUndefined(selectedSubDistrict)) {
            this.address.subDistrict = selectedSubDistrict.name;
        }
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        if (isNullOrUndefined(this.address.latitude) || isNullOrUndefined(this.address.longitude)) {
            const gpsLocationText = await this.translateService.get('PAGES.COMMON.GPS_LOCATION_REQUIRED').toPromise();
            this.toastService.show(gpsLocationText);
            return;
        }
        const loading = this.loadingService.presentLoading();
        const isValidPostalCode = await this.validatePostalCode();
        if (isValidPostalCode === 'FAILED') {
            this.loadingService.hideLoading(loading);
            return;
        }
        if (isValidPostalCode === 'OUT_OF_SERVICE') {
            this.loadingService.hideLoading(loading);
            const headerText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_HEADER').toPromise();
            const bodyText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_TEXT').toPromise();
            const noText = await this.translateService.get('CONFIRMATION_NO').toPromise();
            const yesText = await this.translateService.get('CONFIRMATION_YES').toPromise();
            this.commonService.confirm(headerText, bodyText,
                noText, yesText, this.onYesCallback.bind(this), null);
            return;
        }
        this.processAddress(loading);
    }

    onYesCallback() {
        const loading = this.loadingService.presentLoading();
        this.processAddress(loading);
    }

    async validatePostalCode(): Promise<string> {
        try {
            const response: RestResponse = await this.addressService.validatePostalCode(this.address.postalCode);
            if (!response.status) {
                return 'OUT_OF_SERVICE';
            }
            return 'VALID';
        } catch (e) {
            this.toastService.error(e.message);
            return 'FAILED';
        }
    }

    async processAddress(loading) {
        try {
            const method = this.address.id ? 'update' : 'save';
            const response: RestResponse = await this.addressService[method](this.address);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.modalService.dismiss(true, this.address);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }

    async close() {
        this.modalService.dismiss(false, null);
    }


    hasValidAddress() {
        if (this.address.residenceType === 'BUILDING') {
            return this.isNonEmpty(this.address.buildingName)
                && this.isNonEmpty(this.address.floor)
                && this.isNonEmpty(this.address.unit)
                && this.isNonEmpty(this.address.streetNumber)
                && this.isNonEmpty(this.address.districtId)
                && this.isNonEmpty(this.address.subDistrictId)
                && this.isNonEmpty(this.address.postalCode);
        }
        return this.isNonEmpty(this.address.houseNumber)
            && this.isNonEmpty(this.address.streetNumber)
            && this.isNonEmpty(this.address.districtId)
            && this.isNonEmpty(this.address.subDistrictId)
            && this.isNonEmpty(this.address.postalCode);
    }

    isNonEmpty(value) {
        return !isNullOrUndefined(value) && value.trim() !== '';
    }

    openLocationPopup() {
        
    }
}
