import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {OrderModule} from 'ngx-order-pipe';
import {HeaderComponent} from 'src/app/components/header/header.component';
import {AddressesComponent} from './addresses/addresses.component';
import {PopoverComponent} from './popover/popover.component';
import {AgmCoreModule} from '@agm/core';
import {BrMaskerModule} from 'br-mask';
import {CallingComponent} from '../pages/call/calling/calling.component';
import {CallButtonComponent} from '../pages/common/call-button/call-button.component';
import {QRCodeModule} from 'angularx-qrcode';

const pages: Array<any> = [
    HeaderComponent,
    AddressesComponent,
    PopoverComponent,
    CallingComponent,
    CallButtonComponent
];

@NgModule({
    declarations: [pages],
    imports: [
        FormsModule,
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        TranslateModule,
        OrderModule,
        BrMaskerModule,
        AgmCoreModule,
        QRCodeModule
    ],
    exports: [pages],
    entryComponents: [PopoverComponent]
})

export class CommonAppModule {

}
