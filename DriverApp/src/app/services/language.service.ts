import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpServiceRequests } from 'src/shared/http.service';
import { IResourceWithId, RestResponse } from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})

export class LanguageService extends HttpServiceRequests<IResourceWithId>{

    constructor(public http: HttpClient) {
        super(http);
    }

    fetchAll(): Observable<RestResponse> {
        return this.getRecords('/app/languages');
    }

    change(languageId: string): Promise<RestResponse> {
        return this.updateRecord(`/api/language-change/${languageId}`, null);
    }
}