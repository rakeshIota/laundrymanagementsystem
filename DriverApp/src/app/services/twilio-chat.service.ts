import {Injectable} from '@angular/core';
import {TwilioService} from './twilio.service';
import {ToastService} from '../../shared/toast.service';
import {AuthService} from '../../shared/auth/auth.service';
import {RestResponse} from '../models/authorization.model';
import {File} from '@ionic-native/file/ngx';

declare var Twilio: any;
declare var window;

@Injectable({
    providedIn: 'root'
})
export class TwilioChatService {

    twilioToken: string;
    accessManager: any;
    chatClient: any;
    activeChannel: any;
    userChatChannels: Array<any>;
    currentChannelSid: string;

    constructor(
        public twilioService: TwilioService,
        public toastService: ToastService,
        public file: File,
        private authService: AuthService,
    ) {
        this.twilioToken = null;
        this.accessManager = null;
        this.chatClient = null;
        this.activeChannel = null;
        this.userChatChannels = new Array<any>();
        this.currentChannelSid = null;
    }

    async generateToken() {
        this.chatClient = null;
        const device = 'device' + this.authService.getUser().id;
        try {
            const response: RestResponse = await this.twilioService.generateToken(device).toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.twilioToken = response.data.token;
            this.createTwilioChatClient();
        } catch (e) {

        }
    }

    createTwilioChatClient() {
        Twilio.Chat.Client.create(this.twilioToken).then(client => {
            this.chatClient = client;
        });
    }

    groupBy(collection, property): any {
        let i = 0, val, index;
        const values = [], result = [];
        for (; i < collection.length; i++) {
            val = collection[i][property];
            index = values.indexOf(val);
            if (index > -1) {
                result[index].push(collection[i]);
            } else {
                values.push(val);
                result.push([collection[i]]);
            }
        }
        return result;
    }

    generateIcon(contentType: any): any {
        const type = contentType.split('/')[0];
        let icon = 'document';

        switch (type) {
            case 'image':
                icon = 'images';
                break;
            case 'video':
                icon = 'videocam';
                break;
        }
        return icon;
    }

    createProjectDirectory() {
        const parentDirectory = this.file.dataDirectory;
        const directoryToCreate = 'CarersCouch';
        window.resolveLocalFileSystemURL(parentDirectory, (dirEntry) => {
            dirEntry.getDirectory(directoryToCreate, {create: true}, () => {
                console.log('Root Directory Created');
            }, (error) => {
                console.log('Error Creating Root Directory');
            });
        });
    }

    clearChatClient() {
        this.chatClient = null;
    }
}