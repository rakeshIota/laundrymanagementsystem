import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})
export class TwilioService extends HttpServiceRequests<IResourceWithId> {
    constructor(public http: HttpClient) {
        super(http);
    }

    updateCallStatus(status): Promise<RestResponse> {
        return this.updateRecord(`/api/application/user/call/status`, status);
    }

    generateToken(device): Observable<RestResponse> {
        return this.getRecord(`/api/get/token?device=${device}`);
    }

    fetchAllTwilioUsers(): Observable<RestResponse> {
        return this.getRecord(`/api/twillio/get/users`);
    }

    fetchUser(): Observable<RestResponse> {
        return this.getRecord(`/api/twillio/get/user`);
    }

    createUser(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/twillio/create/user`, obj);
    }

    updateUser(param: any): Promise<RestResponse> {
        return this.saveRecord(`/api/twillio/user/update`, param);
    }

    getChannelDetails(uniqueName): Observable<RestResponse> {
        return this.getRecord(`/api/get/channel?channelSid=${uniqueName}`);
    }

    getChannelMembers(channelSid): Observable<RestResponse> {
        return this.getRecord(`/api/get/channel/members?channelSid=${channelSid}`);
    }

    sendMessage(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/twilio/send/message`, obj);
    }

    getChannelMember(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/account/get/user/info`, obj);
    }

    sendMessageNotification(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/send/notification/user/chat`, obj);
    }

    generateCapabilityToken(identity, deviceType): Observable<RestResponse> {
        return this.getRecord(`/twilio/voice/capability-token?identity=${identity}&deviceType=${deviceType}`);
    }

    updateCurrentCallStatus(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/twilio/voice/call-status`, obj);
    }

    logActivity(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/activity`, obj);
    }

    blockUser(obj: any): Promise<RestResponse> {
        return this.saveRecord('/api/blocked/user', obj);
    }

    fetchDefaultMessages(): Promise<RestResponse> {
        return this.saveRecord('/api/defaultmessages', null);
    }

    answerCalled(obj: any): Promise<RestResponse> {
        return this.saveRecord(`/api/twilio/voice/answered`, obj);
    }
}
