import { Injectable } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { RestResponse } from '../models/authorization.model';
import { Events, Platform } from '@ionic/angular';
import { TwilioService } from './twilio.service';
import { Device } from '@ionic-native/device/ngx';
import { ToastService } from '../../shared/toast.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AuthService } from '../../shared/auth/auth.service';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { UserService } from './user.service';
import { CommonService } from 'src/shared/common.service';

declare const Twilio: any;

@Injectable({
    providedIn: 'root'
})
export class TwilioVoiceService {
    onCall: boolean;
    twilioToken: string;
    callStatus: string;

    constructor(
        public twilioService: TwilioService,
        private platform: Platform,
        private androidPermissions: AndroidPermissions,
        private firebase: FirebaseX,
        public events: Events,
        public device: Device,
        private toastService: ToastService,
        private authService: AuthService,
        private uniqueDeviceID: UniqueDeviceID,
        private userService: UserService,
        private commonService: CommonService
    ) {
        this.onCall = false;
        this.commonService.hasTwilioInitialize = false;
    }


    checkPermissions(): any {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
            result => '',
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO)
        );
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.RECORD_AUDIO,
        this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    }


    async initializeTwilio() {
        if (this.commonService.hasTwilioInitialize) {
            return;
        }
        const user = this.authService.getUser();
        if (this.platform.is('android')) {
            this.checkPermissions();
        }
        try {
            const callerId = user.fullName.replace(' ', '-1') + '_' + user.id;
            console.log('Driver CallerId while starting', callerId);
            const response: RestResponse = await this.twilioService.generateCapabilityToken(callerId, this.device.platform).toPromise();
            if (!response.status) {
                return;
            }
            this.twilioToken = response.data.token;
            Twilio.TwilioVoiceClient.initialize(this.twilioToken);
            Twilio.TwilioVoiceClient.clientinitialized(() => {
                console.log('Clientinitialized Successfully');
                this.commonService.hasTwilioInitialize = true;
            });
            setTimeout(() => {
                this.firebase.getToken().then(token => {
                    Twilio.TwilioVoiceClient.setFCMToken(token);
                    this.processToken(token);
                }).catch(error => console.error('Error getting token', error));
            }, 5000);
        } catch (e) {
            this.toastService.error('Failed to get token for Twilio');
        }
    }

    async processToken(token: string) {
        try {
            const uuid: string = await this.uniqueDeviceID.get();
            this.authService.deviceId = uuid;
            const data = {} as any;
            data.deviceId = uuid;
            data.notificationToken = token;
            this.saveTokenOnServer(data);
        } catch (e) {
            this.toastService.error('Failed to get user device id');
        }
    }

    async saveTokenOnServer(token: any) {
        try {
            const response: RestResponse = await this.userService.saveToken(token);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
        } catch (e) {
            this.toastService.error(e.message);
        }
    }
}
