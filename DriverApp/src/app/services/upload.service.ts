import {Injectable} from '@angular/core';
import {FileTransfer, FileTransferObject, FileUploadOptions, FileUploadResult} from '@ionic-native/file-transfer/ngx';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UploadService {
    url: string = environment.BaseApiUrl + '/api/file/group/items/upload';

    constructor(private transfer: FileTransfer) {
    }

    uploadFile(filePath: string): Promise<FileUploadResult> {
        const fileTransfer: FileTransferObject = this.transfer.create();
        const options: FileUploadOptions = {
            chunkedMode: false,
            mimeType: 'multipart/form-data',
            headers: {},
            fileName: this.randomString(12, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') + '.jpg',
        };
        return fileTransfer.upload(filePath, this.url, options);
    }

    randomString(length, chars) {
        let result = '';
        for (let i = length; i > 0; --i) {
            result += chars[Math.floor(Math.random() * chars.length)];
        }
        return result;
    }
}
