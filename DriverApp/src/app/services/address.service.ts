import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';
import {Address} from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class AddressService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetchDistricts(): Observable<RestResponse> {
        return this.getRecords('/api/application/districts');
    }

    fetchSubDistricts(districtId: string): Observable<RestResponse> {
        return this.getRecords(`/api/application/district/${districtId}/sub/districts`);
    }

    fetchProvince(): Observable<RestResponse> {
        return this.getRecords('/api/application/provinces');
    }

    save(data: Address): Promise<RestResponse> {
        return this.saveRecord('/api/application/address', data);
    }

    update(data: Address): Promise<RestResponse> {
        return this.updateRecord('/api/application/address', data);
    }

    validatePostalCode(iPostalCode) {
        const data = {} as any;
        data.postalCode = iPostalCode;
        return this.saveRecord('/api/application/validate/PostalCode', data);
    }
}
