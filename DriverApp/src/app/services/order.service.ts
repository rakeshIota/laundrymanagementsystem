import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpServiceRequests } from '../../shared/http.service';
import { IResourceWithId, RestResponse } from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})
export class OrderService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetch(orderId: string): Observable<RestResponse> {
        return this.getRecord(`/api/application/order/${orderId}?needDriverLoad=true`);
    }

    place(order: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/order/complete', order);
    }

    myOrders(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/driver/my/orders', data);
    }

    cancelOrder(orderId: string) {
        return this.postUrl(`/api/application/orFder/${orderId}/cancel`);
    }

    complete(ordeId): Promise<RestResponse> {
        return this.putUrl(`/api/application/order/${ordeId}/complete`);
    }

    fetchCompleted(data: any): Promise<RestResponse> {
        // return this.saveRecord('/api/application/driver/my/orders', data);
        return this.saveRecord('/api/application/driver/my/rides', data);
    }

    updateDriverLocation(data) {
        return this.updateRecord('/api/driver/location', data);
    }

    completeQrScanning(data: any): Promise<RestResponse> {
        return this.saveRecord('/payment/received', data);
    }
}
