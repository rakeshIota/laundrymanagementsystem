import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';
import {isNullOrUndefined} from 'util';

@Injectable({
    providedIn: 'root'
})
export class ProductService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetchCategories(): Observable<RestResponse> {
        return this.getRecords('/api/application/services');
    }

    fetchItemsByCategory(serviceId: string, type: string): Observable<RestResponse> {
        if (isNullOrUndefined(type) || type.toLowerCase() === 'all') {
            return this.getRecord(`/api/application/service/${serviceId}/products`);
        }
        return this.getRecord(`/api/application/service/${serviceId}/products?type=${type}`);
    }
}
