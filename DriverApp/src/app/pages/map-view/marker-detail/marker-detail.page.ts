import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user.model';
import {Header} from '../../../models/header.model';
import {isNullOrUndefined} from 'util';
import {AuthService} from '../../../../shared/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {MapsAPILoader} from '@agm/core';
import {CallingComponent} from '../../call/calling/calling.component';
import {ModalService} from '../../../../shared/modal.service';

@Component({
    selector: 'app-marker-detail',
    templateUrl: './marker-detail.page.html',
    styleUrls: ['./marker-detail.page.scss'],
})
export class MarkerDetailPage implements OnInit {
    user: User;
    header: Header;
    order: any;

    constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router,
                private navController: NavController, private mapsAPILoader: MapsAPILoader, private modalService: ModalService) {
    }

    ngOnInit() {
        this.header = new Header({
            title: 'PICKUP',
            showBackButton: true
        });
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.order = this.router.getCurrentNavigation().extras.state.order;
            }
            if (isNullOrUndefined(this.order)) {
                this.navController.navigateRoot('landing');
                return;
            }
            this.init();
        });
    }

    init() {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: (this.order.status === 'ASSIGNED_DRIVER' || this.order.status === 'AWAITING_COLLECTION') ? 'PICKUP' : 'DELIVERY',
            showBackButton: true
        });
    }

    getDeliveryType() {
        return (this.order.status === 'ASSIGNED_DRIVER' || this.order.status === 'AWAITING_COLLECTION') ? 'pickup' : 'delivery';
    }

    async chat() {
        await this.navController.navigateForward(`/order/${this.order.id}/chat/${this.user.id}/${this.order.customerDetails.id}`);
    }

    async call() {
        const input = {} as any;
        input.to = this.order.customerDetails;
        input.type = 'NEW_CALL';
        this.modalService.callingPopup = true;
        const model = await this.modalService.presentModal(CallingComponent, {data: input});
        this.modalService.onComplete(model, this.onCallDisconnect.bind(this));
    }

    async onCallDisconnect(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
    }
}
