import {Component, OnInit, ViewChild} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Header} from '../../models/header.model';
import {User} from '../../models/user.model';
import {AuthService} from '../../../shared/auth/auth.service';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {AgmMap, AgmMarker, MapsAPILoader} from '@agm/core';
import {GeocodeService} from '../../services/geocode.service';
import {Order} from '../../models/order.model';
import {CallNumber} from '@ionic-native/call-number/ngx';

declare const google: any;

@Component({
    selector: 'app-map-view',
    templateUrl: './map-view.page.html',
    styleUrls: ['./map-view.page.scss'],
})
export class MapViewPage implements OnInit {
    user: User;
    header: Header;
    orders: Array<Order>;
    @ViewChild(AgmMap, {static: false})
    map: AgmMap;
    @ViewChild(AgmMap, {static: false})
    marker: AgmMarker;
    detail: any;
    geocoder: any;

    constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router,
                private navController: NavController, private mapsAPILoader: MapsAPILoader, private geocodeService: GeocodeService,
                private callNumber: CallNumber) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.orders = this.router.getCurrentNavigation().extras.state.orders;
            }
            if (isNullOrUndefined(this.orders)) {
                this.navController.navigateRoot('landing');
                return;
            }
            this.init();
        });
    }

    init() {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'VIEW_ON_MAP',
            showBackButton: true
        });
        this.detail = {} as any;
        this.detail.zoom = 12;
        this.detail.isDefault = false;
        this.mapsAPILoader.load().then(() => {
            this.geocoder = new google.maps.Geocoder();
            console.log(this.orders);
            this.orders.forEach((order) => {
                console.log(order.orderId + ' => ' + order.status);
            });
            this.detail.showMap = true;
        });
    }

    getDeliveryType(order) {
        return order.status === 'ASSIGNED_DRIVER' ? 'pickup' : 'delivery';
    }

    async onMarkerDetail(iOrder) {
        console.log(iOrder);
        const navigationExtras: NavigationExtras = {
            state: {
                order: iOrder
            }
        };
        await this.navController.navigateForward([`/marker/detail`], navigationExtras);
    }

    call() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
