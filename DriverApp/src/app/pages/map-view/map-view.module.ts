import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {MapViewPage} from './map-view.page';
import {RouterModule} from '@angular/router';
import {AgmCoreModule} from '@agm/core';
import {CommonAppModule} from '../../components/common.app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: MapViewPage
        }]),
        AgmCoreModule,
        CommonAppModule
    ],
    declarations: [MapViewPage]
})
export class MapViewPageModule {
}
