import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { OrderDetailsPage } from './order-details.page';
import {CommonAppModule} from '../../../components/common.app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{path: '', component: OrderDetailsPage}]),
        FontAwesomeModule,
        TranslateModule,
        CommonAppModule
    ],
    declarations: [OrderDetailsPage]
})
export class OrderDetailsPageModule { }
