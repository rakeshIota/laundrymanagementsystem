import {Component, OnDestroy, OnInit} from '@angular/core';
import {Header} from '../../../../models/header.model';
import {User} from '../../../../models/user.model';
import {CommonService} from '../../../../../shared/common.service';
import {ToastService} from '../../../../../shared/toast.service';
import {OrderService} from '../../../../services/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../../shared/auth/auth.service';
import {MapsAPILoader} from '@agm/core';
import {isNullOrUndefined} from 'util';
import {NavController} from '@ionic/angular';
import {RestResponse} from '../../../../models/authorization.model';
import {interval} from 'rxjs';
import {DataService} from '../../../../../shared/data.service';
import {TwilioService} from '../../../../services/twilio.service';
import {ModalService} from '../../../../../shared/modal.service';
import {TwilioVoiceService} from '../../../../services/twilio-voice.service';
import {CallingComponent} from '../../../call/calling/calling.component';
import {filter} from 'rxjs/operators';
import {Geolocation} from '@ionic-native/geolocation/ngx';

declare const google: any;
declare const Twilio: any;

@Component({
    selector: 'app-order-tracking',
    templateUrl: './order-tracking.page.html',
    styleUrls: ['./order-tracking.page.scss'],
})
export class OrderTrackingPage implements OnInit, OnDestroy {
    header: Header;
    order: any;
    orderId: string;
    user: User;
    detail: any;
    tracking: any;
    origin: any;
    destination: any;
    landing: boolean;
    driver: any;
    driverLocationInterval: any;
    appSettings: any;
    watchLocation: any;
    minFrequency: number;
    lastUpdateTime: Date;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
                private route: ActivatedRoute, private authService: AuthService, private mapsAPILoader: MapsAPILoader,
                private router: Router, private navController: NavController, private dataService: DataService,
                private twilioService: TwilioService, private modalService: ModalService,
                private twilioVoiceService: TwilioVoiceService, private location: Geolocation) {
    }

    async ngOnInit() {
        this.orderId = this.route.snapshot.paramMap.get('orderId');
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                const data = this.router.getCurrentNavigation().extras.state;
                this.order = data.order;
                this.landing = data.landing;
            }
            if (isNullOrUndefined(this.order)) {
                this.navController.navigateRoot(`order/${this.orderId}/detail`);
                return;
            }
            this.init();
        });
    }

    async init() {
        this.appSettings = await this.dataService.getAppSettings();
        this.header = new Header({
            title: 'PAGES.ORDER_TRACKING.HEADING',
            showBackButton: true,
            parentLink: `order/${this.orderId}/basic/detail`
        });
        this.user = this.authService.getUser();
        await this.mapsAPILoader.load();
        if (isNullOrUndefined(this.order.deliveryAddress)
            || isNullOrUndefined(this.order.deliveryAddress.latitude)
            || isNullOrUndefined(this.order.deliveryAddress.longitude)) {
            this.toastService.show('Location is not complete');
            this.router.navigateByUrl(this.landing ? '/landing' : '/order/' + this.orderId + '/detail');
            return;
        }
        this.tracking = {} as any;
        this.tracking.showMap = false;
        this.tracking.latitude = this.order.deliveryAddress.latitude;
        this.tracking.longitude = this.order.deliveryAddress.longitude;
        this.tracking.zoom = 15;
        this.tracking.origin = {lat: this.order.driverDetails.latitude, lng: this.order.driverDetails.longitude};
        this.tracking.destination = {lat: this.tracking.latitude, lng: this.tracking.longitude};
        this.tracking.markerOptions = {} as any;
        this.tracking.markerOptions.origin = {
            icon: '/assets/icon/tracking/food-delivery.png'
        };
        this.tracking.markerOptions.destination = {icon: '/assets/icon/tracking/customer.png'};
        this.tracking.renderOptions = {suppressMarkers: true};
        this.tracking.showMap = true;
        this.calculateDistance();
        const timeInterval = this.appSettings.driverLocationInterval || 60;
        this.minFrequency = Number(timeInterval) * 1000;
    }

    async watchDriverLocations() {
        this.lastUpdateTime = new Date();
        const watch = this.location.watchPosition({
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        }).pipe(
            filter(p => p.coords !== undefined)
        );
        this.watchLocation = watch.subscribe((data) => {
            const now = new Date();
            if (this.lastUpdateTime && now.getTime() - this.lastUpdateTime.getTime() < Number(this.minFrequency)) {
                console.log('Ignoring position');
                return;
            }
            this.lastUpdateTime = now;
            this.updateDriverLocation(data.coords);
        });
    }

    async updateDriverLocation(coords) {
        try {
            this.tracking.showMap = false;
            this.tracking.origin.lat = coords.latitude;
            this.tracking.origin.lng = coords.longitude;
            setTimeout(() => {
                this.tracking.showMap = true;
                this.calculateDistance();
            }, 100);
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    calculateDistance() {
        const origin = new google.maps.LatLng(this.tracking.origin.lat, this.tracking.origin.lng);
        const destination = new google.maps.LatLng(this.tracking.destination.lat, this.tracking.destination.lng);
        const service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING',
            }, this.callback.bind(this));

    }

    callback(response, status) {
        this.tracking.time = undefined;
        if (response.rows.length <= 0) {
            return;
        }
        const selectedElement = response.rows[0];
        if (selectedElement.elements.length <= 0) {
            return;
        }
        this.tracking.time = selectedElement.elements[0];
    }

    async call() {
        const input = {} as any;
        input.to = this.order.customerDetails;
        input.type = 'NEW_CALL';
        this.modalService.callingPopup = true;
        const model = await this.modalService.presentModal(CallingComponent, {data: input});
        this.modalService.onComplete(model, this.onCallDisconnect.bind(this));
    }

    async onCallDisconnect(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
    }

    async chat() {
        await this.navController.navigateForward(`/order/${this.orderId}/chat/${this.user.id}/${this.order.driverDetails.id}`);
    }

    ngOnDestroy(): void {
        if (!isNullOrUndefined(this.driverLocationInterval)) {
            this.driverLocationInterval.unsubscribe();
        }
        if (!isNullOrUndefined(this.watchLocation)) {
            this.watchLocation.unsubscribe();
            this.watchLocation = undefined;
        }
    }
}
