import {Component, OnInit} from '@angular/core';
import {Header} from '../../../../models/header.model';
import {User} from '../../../../models/user.model';
import {CommonService} from '../../../../../shared/common.service';
import {ToastService} from '../../../../../shared/toast.service';
import {OrderService} from '../../../../services/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../../shared/auth/auth.service';
import {MapsAPILoader} from '@agm/core';
import {isNullOrUndefined} from 'util';
import {NavController} from '@ionic/angular';
import {DataService} from '../../../../../shared/data.service';
import {TwilioService} from '../../../../services/twilio.service';
import {ModalService} from '../../../../../shared/modal.service';
import {TwilioVoiceService} from '../../../../services/twilio-voice.service';
import {Geolocation} from '@ionic-native/geolocation/ngx';

declare const google: any;
declare const Twilio: any;

@Component({
    selector: 'app-order-tracking-basic',
    templateUrl: './order-tracking-basic.page.html',
    styleUrls: ['./order-tracking-basic.page.scss'],
})
export class OrderTrackingBasicPage implements OnInit {
    header: Header;
    order: any;
    orderId: string;
    user: User;
    driver: any;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
                private route: ActivatedRoute, private authService: AuthService, private mapsAPILoader: MapsAPILoader,
                private router: Router, private navController: NavController, private dataService: DataService,
                private twilioService: TwilioService, private modalService: ModalService,
                private twilioVoiceService: TwilioVoiceService, private location: Geolocation) {
    }

    async ngOnInit() {
        this.driver = {} as any;
        this.orderId = this.route.snapshot.paramMap.get('orderId');
        this.header = new Header({
            title: 'PAGES.ORDER_TRACKING.HEADING',
            showBackButton: true,
            parentLink: `order/${this.orderId}/basic/detail`
        });
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                const data = this.router.getCurrentNavigation().extras.state;
                this.order = data.order;
            }
            if (isNullOrUndefined(this.order)) {
                this.navController.navigateRoot(`order/${this.orderId}/detail`);
                return;
            }
            this.init();
        });
    }

    async init() {
        try {
            const options = {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 0
            };
            const response = await this.location.getCurrentPosition(options);
            this.driver.latitude = response.coords.latitude;
            this.driver.longitude = response.coords.longitude;
        } catch (e) {
            this.toastService.error('Sorry, failed to get current locaiton');
        }
    }
}
