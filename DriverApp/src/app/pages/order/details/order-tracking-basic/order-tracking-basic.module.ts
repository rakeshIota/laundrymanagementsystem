import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {OrderTrackingBasicPage} from './order-tracking-basic.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../../components/common.app.module';
import {AgmCoreModule} from '@agm/core';
import {AgmDirectionModule} from 'agm-direction';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: OrderTrackingBasicPage
        }]),
        CommonAppModule,
        AgmCoreModule,
        AgmDirectionModule,
        TranslateModule
    ],
    declarations: [OrderTrackingBasicPage]
})
export class OrderTrackingBasicPageModule {
}
