import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {Header} from '../../../models/header.model';
import {User} from '../../../models/user.model';
import {OrderService} from '../../../services/order.service';
import {ToastService} from '../../../../shared/toast.service';
import {CommonService} from '../../../../shared/common.service';
import {AuthService} from '../../../../shared/auth/auth.service';
import {isNullOrUndefined} from 'util';
import {CallNumber} from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.page.html',
    styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
    header: Header;
    order: any;
    orderId: string;
    user: User;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
                private route: ActivatedRoute, private authService: AuthService, private navController: NavController,
                private router: Router, private callNumber: CallNumber) {

    }

    async ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.order = this.router.getCurrentNavigation().extras.state.order;
            }
            this.orderId = this.route.snapshot.paramMap.get('orderId');
            if (isNullOrUndefined(this.order)) {
                this.navController.navigateRoot(`/order/${this.orderId}/basic/detail`);
                return;
            }
            this.init();
        });
    }

    async init() {
        this.header = new Header({
            title: 'PAGES.ORDER_DETAIL.HEADER_TEXT',
            showBackButton: true,
            parentLink: `/order/${this.orderId}/basic/detail`
        });
        this.user = this.authService.getUser();
        this.order.uiItems = this.commonService.filterRecords(this.order.orderItems);
    }

    adminCall() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
