import { Component, OnDestroy, OnInit } from '@angular/core';
import { Header } from '../../../models/header.model';
import { User } from '../../../models/user.model';
import { CommonService } from '../../../../shared/common.service';
import { ToastService } from '../../../../shared/toast.service';
import { OrderService } from '../../../services/order.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AuthService } from '../../../../shared/auth/auth.service';
import { NavController } from '@ionic/angular';
import { RestResponse } from '../../../models/authorization.model';
import { isNullOrUndefined } from 'util';
import { ModalService } from '../../../../shared/modal.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CallingComponent } from '../../call/calling/calling.component';
import { EventService } from '../../../../shared/event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-order-basic-detail',
    templateUrl: './order-basic-detail.page.html',
    styleUrls: ['./order-basic-detail.page.scss'],
})
export class OrderBasicDetailPage implements OnInit, OnDestroy {

    header: Header;
    order: any;
    orderId: string;
    user: User;
    futureTodayDate: boolean;
    todayDate: boolean;
    eventSubscribe: any;
    authUser: User;
    completed: boolean;
    rideType: string;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
        private route: ActivatedRoute, private authService: AuthService, private navController: NavController,
        private router: Router, private modalService: ModalService, private callNumber: CallNumber,
        private eventService: EventService, private translateService: TranslateService) {
        this.authUser = new User();
    }

    async ngOnInit() {
        this.orderId = this.route.snapshot.paramMap.get('orderId');
        this.authUser = this.authService.getUser();
        this.route.queryParamMap.subscribe((params: any) => {
            this.beforeInit(params);
        });
    }

    async beforeInit(params) {
        this.completed = params.params.completed === 'YES';
        console.log(this.completed);
        this.rideType = params.params.rideType;
        let chat: boolean = params.params.chat;
        let hasRootPage = false;
        if (this.router.url.startsWith('/place/order/')) {
            hasRootPage = true;
        }
        if (this.router.url.startsWith('/notification/order/')) {
            hasRootPage = true;
        }
        this.header = new Header({
            title: 'PAGES.ORDER_SUMMARY.HEADER_TEXT',
            showBackButton: !hasRootPage,
            showMenu: hasRootPage,
            parentLink: this.completed ? `/completed/orders` : `/landing`
        });
        this.user = this.authService.getUser();
        await this.init();
        this.order.uiItems = this.commonService.filterRecords(this.order.orderItems);
        this.registerEvents();
        if (chat && chat.toString() === 'true') {
            await this.router.navigate([], {
                queryParams: {
                    completed: 'NO',
                    chat: false
                }
            });
            this.chat();
            chat = false;
        }
    }

    registerEvents() {
        if (!isNullOrUndefined(this.eventSubscribe)) {
            return;
        }
        this.eventSubscribe = this.eventService.event.subscribe((response: any) => {
            if (isNullOrUndefined(response)) {
                return;
            }
            if (response.key !== 'order:payment:complete' && response.data !== this.orderId) {
                return;
            }
            this.init();
        });
    }

    async init() {
        try {
            const response: RestResponse = await this.orderService.fetch(this.orderId).toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.order = response.data;
            this.order.processing = false;
            if (this.order.status === 'ASSIGNED_DRIVER' || this.order.status === 'AWAITING_COLLECTION') {
                this.todayDate = new Date(this.order.pickupDate).toDateString() === new Date().toDateString();
                this.futureTodayDate = new Date(this.order.pickupDate).getTime() > new Date().getTime() && !this.todayDate;
            }
            if (this.order.status === 'ASSIGNED_DRIVER_FOR_DELIVERY' || this.order.status === 'ON_THE_WAY') {
                this.todayDate = new Date(this.order.deliveryDate).toDateString() === new Date().toDateString();
                this.futureTodayDate = new Date(this.order.deliveryDate).getTime() > new Date().getTime() && !this.todayDate;
            }
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async proceed() {
        if (this.order.driverActiveOrders > 0 && (this.order.status === 'ASSIGNED_DRIVER' || this.order.status === 'ASSIGNED_DRIVER_FOR_DELIVERY')) {
            const pickupDateText = await this.translateService.get('ORDER.ERROR.ALREADY_ACTIVE_ORDER').toPromise();
            this.toastService.error(pickupDateText);
            return;
        }
        try {
            this.order.processing = true;
            const response: RestResponse = await this.orderService.complete(this.order.id);
            if (!response.status) {
                this.order.processing = false;
                this.toastService.error(response.message);
                return;
            }
            if (this.order.status === 'AWAITING_COLLECTION' || this.order.status === 'ON_THE_WAY') {
                await this.navController.navigateRoot(`order/${this.order.id}/success?orderNumber=${this.order.orderId}`);
                return;
            }
            await this.init();
        } catch (e) {
            this.order.processing = false;
            this.toastService.error(e.message);
        }
    }

    async detail() {
        const navigationExtras: NavigationExtras = {
            state: {
                order: this.order
            }
        };
        await this.navController.navigateForward([`/order/${this.order.id}/detail`], navigationExtras);
    }

    async chat() {
        await this.navController.navigateForward(`/order/${this.order.id}/chat/${this.user.id}/${this.order.customerDetails.id}`);
    }

    async call() {
        const input = {} as any;
        input.to = this.order.customerDetails;
        input.type = 'NEW_CALL';
        this.modalService.callingPopup = true;
        const model = await this.modalService.presentModal(CallingComponent, { data: input });
        this.modalService.onComplete(model, this.onCallDisconnect.bind(this));
    }

    async onCallDisconnect(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
    }

    map() {
        if (this.order.status === 'ASSIGNED_DRIVER' || this.order.status === 'ASSIGNED_DRIVER_FOR_DELIVERY') {
            this.tracking();
        } else if (this.order.status === 'AWAITING_COLLECTION' || this.order.status === 'ON_THE_WAY') {
            this.tracking();
        }
    }

    async tracking() {
        const navigationExtras: NavigationExtras = {
            state: {
                order: this.order,
                landing: false
            }
        };
        if (this.order.status === 'AWAITING_COLLECTION' || this.order.status === 'ON_THE_WAY') {
            await this.navController.navigateForward([`/order/${this.order.id}/tracking`], navigationExtras);
            return;
        }
        await this.navController.navigateForward([`/order/${this.order.id}/tracking/basic`], navigationExtras);
    }

    async mapIt() {
        const navigationExtras: NavigationExtras = {
            state: {
                orders: [this.order]
            }
        };
        await this.navController.navigateForward([`/map`], navigationExtras);
    }

    adminCall() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    async openQrCodePayment() {
        const navigationExtras: NavigationExtras = {
            state: {
                order: this.order
            }
        };
        await this.navController.navigateForward([`/order/qr-code`], navigationExtras);
    }

    ngOnDestroy(): void {
        if (!isNullOrUndefined(this.eventSubscribe)) {
            this.eventSubscribe.unsubscribe();
        }
    }
}
