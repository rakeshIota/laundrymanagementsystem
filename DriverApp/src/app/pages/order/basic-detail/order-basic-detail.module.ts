import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {OrderBasicDetailPage} from './order-basic-detail.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: OrderBasicDetailPage
        }]),
        CommonAppModule,
        TranslateModule
    ],
    declarations: [OrderBasicDetailPage]
})
export class OrderBasicDetailPageModule {
}
