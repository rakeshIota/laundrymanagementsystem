import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-order-confirmation',
    templateUrl: './order-confirmation.page.html',
    styleUrls: ['./order-confirmation.page.scss'],
})
export class OrderConfirmationPage implements OnInit {
    orderNumber: string;

    constructor(private navController: NavController, private route: ActivatedRoute, private callNumber: CallNumber) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.orderNumber = params.orderNumber;
        });
    }

    async detail() {
        const orderId = this.route.snapshot.paramMap.get('orderId');
        await this.navController.navigateRoot(`/landing`);
    }

    call() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
