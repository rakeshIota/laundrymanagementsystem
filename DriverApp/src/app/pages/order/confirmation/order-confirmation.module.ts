import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {OrderConfirmationPage} from './order-confirmation.page';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CommonAppModule} from '../../../components/common.app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: OrderConfirmationPage
        }]),
        CommonAppModule,
        TranslateModule
    ],
    declarations: [OrderConfirmationPage]
})
export class OrderConfirmationPageModule {
}
