import {Component, OnInit} from '@angular/core';
import {CallNumber} from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-call-button',
    templateUrl: './call-button.component.html',
    styleUrls: ['./call-button.component.scss'],
})
export class CallButtonComponent implements OnInit {

    constructor(private callNumber: CallNumber) {
    }

    ngOnInit() {
    }

    call() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
