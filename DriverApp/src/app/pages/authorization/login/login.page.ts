import {Component, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {Language} from 'src/app/models/language.model';
import {User} from 'src/app/models/user.model';
import {LanguageService} from 'src/app/services/language.service';
import {AuthService} from 'src/shared/auth/auth.service';
import {LoadingService} from 'src/shared/loading.service';
import {LocalStorageService} from 'src/shared/local.storage.service';
import {ToastService} from 'src/shared/toast.service';
import {isNullOrUndefined} from 'util';
import {AuthorizationService} from '../authorization.service';
import {TranslateService} from '@ngx-translate/core';
import {CommonService} from '../../../../shared/common.service';
import {UserService} from '../../../services/user.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    user: User;
    onClickValidation: boolean;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService,
                private localStorageService: LocalStorageService, private toastService: ToastService, private authService: AuthService,
                private navController: NavController, private languageService: LanguageService, private menuCtrl: MenuController,
                private userService: UserService, private commonService: CommonService, private translate: TranslateService) {
    }

    ngOnInit() {
        this.menuCtrl.enable(false);
        this.user = new User();
        this.onClickValidation = false;
        this.user.userName = this.localStorageService.get('userName');
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.login(this.user);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.processToken(response.data);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }

    async processToken(data: any) {
        data.token.expires_at = new Date(data.token.expires).getTime();
        this.localStorageService.set('userName', this.user.userName);
        if (!this.user.rememberMe) {
            this.authService.user = data.user;
            this.authService.authToken = data.token;
            this.authService.roles = data.user.roles;
            await this.changeDefaultLanguge(data.user.languageId);
            await this.fetchMyDetail();
            this.navController.navigateRoot('landing');
            return;
        }
        this.localStorageService.set('token', JSON.stringify(data.token));
        this.localStorageService.set('user', JSON.stringify(data.user));
        this.localStorageService.set('roles', JSON.stringify(data.user.roles));
        await this.changeDefaultLanguge(data.user.languageId);
        await this.fetchMyDetail();
        this.localStorageService.set('user', JSON.stringify(data.user));
        this.navController.navigateRoot('landing');
    }

    async fetchMyDetail() {
        try {
            const response: RestResponse = await this.userService.fetchMyDetail().toPromise();
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            if (!this.user.rememberMe) {
                this.authService.user = response.data;
                return;
            }
            const user = response.data;
            if (isNullOrUndefined(user.addressdetails)) {
                user.addressdetails = new Array<any>();
            }
            this.authService.user = user;
            if (this.user.rememberMe) {
                this.localStorageService.set('user', JSON.stringify(user));
            }
            user.pickupAddress = user.addressdetails.find(x => x.type === 'PICKUP_DELIVERY');
            delete user.addressdetails;
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    async changeDefaultLanguge(languageId: string) {
        const language: Language = JSON.parse(this.localStorageService.get('DEFAULT_LANGUAGE'));
        if (!isNullOrUndefined(language) && language.id !== languageId) {
            try {
                await this.languageService.change(language.id);
            } catch (e) {
                this.toastService.error(e.message);
            }
        }
    }
}
