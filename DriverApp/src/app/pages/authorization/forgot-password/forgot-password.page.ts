import {Component, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {User} from 'src/app/models/user.model';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../authorization.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.page.html',
    styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
    user: User;
    onClickValidation: boolean;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService,
                private toastService: ToastService, private navController: NavController, private menuController: MenuController) {
    }

    ngOnInit() {
        this.menuController.enable(false);
        this.user = new User();
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.forgotPassword(this.user);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            await this.navController.navigateRoot('otp-verification', {
                queryParams: {
                    userName: this.user.userName,
                    otpType: 'FORGOT_PASSWORD'
                }
            });
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }
}
