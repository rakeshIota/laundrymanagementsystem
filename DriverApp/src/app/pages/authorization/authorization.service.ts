import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IResourceWithId, RestResponse} from 'src/app/models/authorization.model';
import {User, UserOtpVerification} from 'src/app/models/user.model';
import {HttpServiceRequests} from 'src/shared/http.service';

@Injectable({
    providedIn: 'root'
})

export class AuthorizationService extends HttpServiceRequests<IResourceWithId> {
    constructor(public http: HttpClient) {
        super(http);
    }

    login(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/driver/login', user);
    }

    forgotPassword(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/forgot-password', user);
    }

    resetPassword(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/reset-password', user);
    }

    verifyPassword(password): Promise<RestResponse> {
        return this.saveRecord('/api/validate/password', password);
    }

    verifyForgotPasswordOtp(otpDetails: UserOtpVerification): Promise<RestResponse> {
        return this.saveRecord('/app/customer/verify-resetOtp', otpDetails);
    }

    sendForgotOTP(data: any) {
        return this.postUrl('/app/customer/resend-emailotp?userName=' + data.userName);
    }
}
