import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {UserOtpVerification} from 'src/app/models/user.model';
import {LoadingService} from 'src/shared/loading.service';
import {LocalStorageService} from 'src/shared/local.storage.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../authorization.service';
import {AuthService} from '../../../../shared/auth/auth.service';

@Component({
    selector: 'app-otp-verification',
    templateUrl: './otp-verification.page.html',
    styleUrls: ['./otp-verification.page.scss'],
})
export class OtpVerificationPage implements OnInit {
    onClickValidation: boolean;
    userOtpVerification: UserOtpVerification;
    otpType: string;
    nextOTP: number;
    timerInterval: any;

    constructor(private toastService: ToastService, private authorizationService: AuthorizationService, private navController: NavController,
                private activatedRoute: ActivatedRoute, private loadingService: LoadingService, private localStorageService: LocalStorageService,
                private menuController: MenuController, private authService: AuthService) {
    }

    ngOnInit() {
        if (this.authService.getToken() === null) {
            this.menuController.enable(false);
        }
        this.nextOTP = 0;
        this.userOtpVerification = new UserOtpVerification();
        this.activatedRoute.queryParams.subscribe(params => {
            this.userOtpVerification.mobileNo = params.userMobileNo;
            this.userOtpVerification.countryCode = params.countryCode;
            this.userOtpVerification.userName = params.userName;
            this.otpType = params.otpType;
        });
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const method: string = this.getApiCallMethod();
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService[method](this.userOtpVerification);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            if (this.otpType === 'REGISTRATION') {
                this.toastService.error(response.message);
                await this.navController.navigateRoot('registration/success', {
                    queryParams: {
                        email: this.userOtpVerification.userName,
                        type: 'REGISTER'
                    }
                });
                return;
            }
            if (this.otpType === 'FORGOT_PASSWORD') {
                await this.navController.navigateRoot('reset-password', {
                    queryParams: {
                        uniqueCode: response.data.uniqueCode,
                        code: response.data.code
                    }
                });
                return;
            }
            if (this.otpType === 'PHONE_NUMBER') {
                await this.navController.navigateRoot('registration/success', {
                    queryParams: {
                        type: 'PHONE_NUMBER_CHANGE'
                    }
                });
                return;
            }
            await this.navController.navigateRoot('landing');
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }

    getApiCallMethod(): string {
        switch (this.otpType) {
            case 'REGISTRATION':
                return 'verifyRegistrationOtp';
            case 'PHONE_NUMBER':
                return 'verifyPhoneOtp';
            case 'EMAIL':
                return 'verifyEmailOtp';
            case 'FORGOT_PASSWORD':
                return 'verifyForgotPasswordOtp';
            default:
                return '';
        }
    }

    async sendOtp() {
        const loading = this.loadingService.presentLoading();
        try {
            const method: string = this.getResendApiCallMethod();
            const response: RestResponse = await this.authorizationService[method](this.userOtpVerification);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.nextOTP = 179;
            this.timerInterval = setInterval(this.handleTimeInterval.bind(this), 1000);
            this.toastService.error(response.message);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }

    getResendApiCallMethod() {
        switch (this.otpType) {
            case 'REGISTRATION':
            case 'EMAIL':
                return 'sendOTP';
            case 'PHONE_NUMBER':
                return 'sendPhoneChangeOTP';
            case 'FORGOT_PASSWORD':
                return 'sendForgotOTP';
            default:
                return '';
        }
    }

    handleTimeInterval() {
        this.nextOTP = Number(this.nextOTP) - 1;
        if (this.nextOTP > 0) {
            return;
        }
        if (this.timerInterval) {
            clearInterval(this.timerInterval);
        }
    }

    back() {
        this.navController.pop();
    }

    getDisplayTime() {
        const min = Math.floor(this.nextOTP / 60);
        const second = Math.floor(this.nextOTP % 60);
        return '0' + min + ':' + (second >= 10 ? second : '0' + second);
    }
}
