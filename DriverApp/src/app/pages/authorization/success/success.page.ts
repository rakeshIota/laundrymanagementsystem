import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MenuController} from '@ionic/angular';

@Component({
    selector: 'app-success',
    templateUrl: './success.page.html',
    styleUrls: ['./success.page.scss'],
})
export class SuccessPage implements OnInit {
    email: string;
    type: string;

    constructor(private activatedRoute: ActivatedRoute, private menuController: MenuController) {
    }

    ngOnInit() {
        this.menuController.enable(false);
        this.activatedRoute.queryParams.subscribe(params => {
            this.email = params.email;
            this.type = params.type;
        });
    }
}
