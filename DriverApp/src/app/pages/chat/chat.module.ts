import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ChatPage} from './chat.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../components/common.app.module';
import {LinkyModule} from 'angular-linky';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: ChatPage
        }]),
        CommonAppModule,
        LinkyModule
    ],
    declarations: [ChatPage]
})
export class ChatPageModule {
}
