import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { RestResponse } from 'src/app/models/authorization.model';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/services/order.service';
import { CommonService } from 'src/shared/common.service';
import { LoadingService } from 'src/shared/loading.service';
import { ToastService } from 'src/shared/toast.service';
import { Header } from '../../models/header.model';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-completed-orders',
    templateUrl: './completed-orders.page.html',
    styleUrls: ['./completed-orders.page.scss'],
})
export class CompletedOrdersPage implements OnInit {
    completedOrders: Order[];
    ordersLoaded: boolean;
    header: Header;

    constructor(private loadingService: LoadingService, private toastService: ToastService, private orderService: OrderService,
        private menuController: MenuController, private commonService: CommonService, private navController: NavController,
        private callNumber: CallNumber) {
        this.completedOrders = [] as Order[];
    }

    async ngOnInit() {
        this.header = new Header({
            title: 'PAGES.COMPLETED_ORDERS.HEADING',
            showBackButton: true
        });
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.orderService.fetchCompleted({ type: 'COMPLETE' });
            this.ordersLoaded = true;
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.completedOrders = response.data;
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
            this.ordersLoaded = true;
        }
    }

    toggleMenu() {
        this.menuController.toggle();
    }

    openOrder(order: Order) {
        this.navController.navigateForward(`order/${order.id}/basic/detail`, {
            queryParams: {
                completed: 'YES',
                rideType: order.rideType
            }
        });
    }

    call() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
