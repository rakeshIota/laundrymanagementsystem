import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CompletedOrdersPage } from './completed-orders.page';
import {CommonAppModule} from '../../components/common.app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{path: '', component: CompletedOrdersPage}]),
        FontAwesomeModule,
        TranslateModule,
        CommonAppModule
    ],
    declarations: [CompletedOrdersPage]
})
export class CompletedOrdersPageModule { }
