import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {OrderModule} from 'ngx-order-pipe';
import {CommonAppModule} from 'src/app/components/common.app.module';
import {LandingPage} from './landing.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: LandingPage
            }
        ]),
        TranslateModule,
        CommonAppModule,
        OrderModule,
        FontAwesomeModule
    ],
    declarations: [LandingPage]
})
export class LandingPageModule {
}
