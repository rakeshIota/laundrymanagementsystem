import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {Order} from 'src/app/models/order.model';
import {OrderService} from 'src/app/services/order.service';
import {CommonService} from 'src/shared/common.service';
import {LoadingService} from '../../../shared/loading.service';
import {ToastService} from '../../../shared/toast.service';
import {Header} from '../../models/header.model';
import {DatePicker} from '@ionic-native/date-picker/ngx';
import {NavigationExtras} from '@angular/router';
import {TwilioVoiceService} from '../../services/twilio-voice.service';
import {DataService} from '../../../shared/data.service';
import {interval} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {filter} from 'rxjs/operators';
import {AuthService} from '../../../shared/auth/auth.service';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {CallNumber} from '@ionic-native/call-number/ngx';

@Component({
    selector: 'app-landing',
    templateUrl: 'landing.page.html',
    styleUrls: ['landing.page.scss'],
})
export class LandingPage implements OnInit, OnDestroy {
    orders: Array<Order>;
    pickupOrdersCount: number;
    deliveryOrdersCount: number;
    ordersLoaded: boolean;
    header: Header;
    selectedDate: Date;
    allOrders: Array<Order>;
    selectedType: string;
    todayDate: boolean;
    appSettings: any;
    driverOrderInterval: any;
    futureTodayDate: boolean;
    showDate: boolean;
    user: any;
    lastUpdateTime: Date;
    watchLocation: any;
    minFrequency: number;
    isFirstTime: boolean;
    datePickerShown: boolean;

    constructor(private loadingService: LoadingService, private toastService: ToastService, private orderService: OrderService,
                private menuController: MenuController, private commonService: CommonService, private menuCtrl: MenuController,
                private datePicker: DatePicker, private navController: NavController, private twilioVoiceService: TwilioVoiceService,
                private dataService: DataService, private ngZone: NgZone, private authService: AuthService,
                private location: Geolocation, private callNumber: CallNumber) {
        this.orders = new Array<Order>();
        this.pickupOrdersCount = 0;
        this.deliveryOrdersCount = 0;
        this.header = new Header({
            title: 'ORDER_PAGE_HEADER',
            showMenu: true
        });
    }

    async ngOnInit() {
        this.showDate = true;
        this.selectedType = 'total';
        this.selectedDate = new Date();
        this.todayDate = this.selectedDate.toDateString() === new Date().toDateString();
        this.futureTodayDate = this.selectedDate.getTime() > new Date().getTime();
        await this.menuCtrl.enable(true);
        this.appSettings = await this.dataService.getAppSettings();
        const timeInterval = this.appSettings.driverOrderInterval || 60;
        this.fetchOrders(true);
        this.driverOrderInterval = interval(Number(timeInterval) * 1000)
            .subscribe((val) => {
                this.fetchOrders(false);
            });
        this.initCall();
        this.user = this.authService.getUser();
        this.minFrequency = Number(timeInterval) * 1000;
        this.watchDriverLocations();
    }

    openDatepicker() {
        if (this.datePickerShown) {
            return;
        }
        this.datePickerShown = true;
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then((date) => {
                this.datePickerShown = false;
                this.selectedDate = date;
                this.todayDate = (this.selectedDate.toDateString() === new Date().toDateString());
                this.futureTodayDate = this.selectedDate.getTime() > new Date().getTime();
                this.fetchOrders(true);
            },
            err => {
                this.datePickerShown = false;
            }
        );
    }

    async fetchOrders(wantLoading: boolean) {
        let loading;
        if (wantLoading) {
            loading = this.loadingService.presentLoading();
        }
        try {
            this.orders = new Array<Order>();
            this.selectedDate.setHours(12);
            this.selectedDate.setMinutes(0);
            this.selectedDate.setSeconds(0);
            const data = {} as any;
            data.isCompleted = false;
            data.date = this.selectedDate;
            const response: RestResponse = await this.orderService.myOrders(data);
            this.ordersLoaded = true;
            if (wantLoading) {
                this.loadingService.hideLoading(loading);
            }
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            response.data.forEach((order: Order) => {
                order.customerDetails = {} as any;
                order.customerDetails.id = order.customerId;
                order.customerDetails.fullName = order.customerName;
            });
            this.orders = response.data;
            this.allOrders = JSON.parse(JSON.stringify(response.data));
            this.pickupOrdersCount = this.orders.filter(x => x.status === 'ASSIGNED_DRIVER' || x.status === 'AWAITING_COLLECTION').length;
            this.deliveryOrdersCount = this.orders.filter(x => x.status === 'ASSIGNED_DRIVER_FOR_DELIVERY' || x.status === 'ON_THE_WAY').length;
        } catch (e) {
            if (wantLoading) {
                this.loadingService.hideLoading(loading);
            }
            this.toastService.error(e.message);
            this.ordersLoaded = true;
        }
    }

    async mapAll() {
        const navigationExtras: NavigationExtras = {
            state: {
                orders: this.allOrders
            }
        };
        await this.navController.navigateForward([`/map`], navigationExtras);
    }

    async mapIt(order) {
        const navigationExtras: NavigationExtras = {
            state: {
                orders: [order]
            }
        };
        await this.navController.navigateForward([`/map`], navigationExtras);
    }

    async doIt(order) {
        await this.navController.navigateForward([`/order/${order.id}/basic/detail`], {queryParams: {completed: 'NO'}});
    }

    initCall() {
        this.twilioVoiceService.initializeTwilio();
    }

    filter(type: string) {
        this.selectedType = type;
        if (type === 'pickup') {
            this.orders = Object.assign([], this.allOrders.filter(x => x.status === 'ASSIGNED_DRIVER' || x.status === 'AWAITING_COLLECTION'));
        } else if (type === 'delivery') {
            this.orders = Object.assign([], this.allOrders.filter(x => x.status === 'ASSIGNED_DRIVER_FOR_DELIVERY' || x.status === 'ON_THE_WAY'));
        } else {
            this.orders = Object.assign([], this.allOrders);
        }
    }

    ngOnDestroy(): void {
        if (!isNullOrUndefined(this.driverOrderInterval)) {
            this.driverOrderInterval.unsubscribe();
        }
    }

    previousDate() {
        this.showDate = false;
        this.selectedDate.setDate(this.selectedDate.getDate() - 1);
        this.todayDate = (this.selectedDate.toDateString() === new Date().toDateString());
        this.futureTodayDate = this.selectedDate.getTime() > new Date().getTime();
        setTimeout(() => {
            this.showDate = true;
        }, 1000);
        this.fetchOrders(true);
    }

    nextDate() {
        this.showDate = false;
        this.selectedDate.setDate(this.selectedDate.getDate() + 1);
        this.todayDate = (this.selectedDate.toDateString() === new Date().toDateString());
        this.futureTodayDate = this.selectedDate.getTime() > new Date().getTime();
        setTimeout(() => {
            this.showDate = true;
        }, 1000);
        this.fetchOrders(true);
    }

    async watchDriverLocations() {
        if (!isNullOrUndefined(this.watchLocation)) {
            console.log('Skip Location update initialization');
            return;
        }
        this.lastUpdateTime = new Date();
        this.isFirstTime = true;
        const watch = this.location.watchPosition({
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        }).pipe(
            filter(p => p.coords !== undefined)
        );
        this.watchLocation = watch.subscribe((data) => {
            const now = new Date();
            if (!this.isFirstTime && this.lastUpdateTime && now.getTime() - this.lastUpdateTime.getTime() < Number(this.minFrequency)) {
                console.log('Ignoring position');
                return;
            }
            this.isFirstTime = false;
            this.lastUpdateTime = now;
            this.updateDriverLocationOnServer(data.coords);
        });
    }

    async updateDriverLocationOnServer(coords) {
        try {
            const input = {} as any;
            input.latitude = coords.latitude;
            input.longitude = coords.longitude;
            input.userId = this.user.id;
            const response: RestResponse = await this.orderService.updateDriverLocation(input);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
        } catch (e) {
            this.toastService.error(e.message);
        }
    }

    call() {
        this.callNumber.callNumber('+66-2-2134567', false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
}
