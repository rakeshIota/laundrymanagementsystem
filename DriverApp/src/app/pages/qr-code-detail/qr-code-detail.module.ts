import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {QrCodeDetailPage} from './qr-code-detail.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {QRCodeModule} from 'angularx-qrcode';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: QrCodeDetailPage
        }]),
        CommonAppModule,
        TranslateModule,
        QRCodeModule
    ],
    declarations: [QrCodeDetailPage]
})
export class QrCodeDetailPageModule {
}
