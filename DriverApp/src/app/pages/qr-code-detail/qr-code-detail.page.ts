import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { RestResponse } from 'src/app/models/authorization.model';
import { OrderService } from 'src/app/services/order.service';
import { LoadingService } from 'src/shared/loading.service';
import { ToastService } from 'src/shared/toast.service';
import { isNullOrUndefined } from 'util';
import { Header } from '../../models/header.model';

@Component({
    selector: 'app-qr-code-detail',
    templateUrl: './qr-code-detail.page.html',
    styleUrls: ['./qr-code-detail.page.scss'],
})
export class QrCodeDetailPage implements OnInit {
    header: Header;
    qrCodeText: string;
    qrSize: number;
    order: any;

    constructor(private router: Router, private route: ActivatedRoute, private navController: NavController, private orderService: OrderService, private loadingService: LoadingService, private toastService: ToastService) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe((params) => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.order = this.router.getCurrentNavigation().extras.state.order;
            }
            if (isNullOrUndefined(this.order)) {
                this.navController.pop();
                return;
            }
            this.header = new Header({
                title: 'PAGES.QR_CODE_PAYMENT_DETAIL.HEADER_TEXT',
                showBackButton: true,
                showMenu: false
            });
            this.qrSize = this.order.qrCode.length;
        });
    }

    async doneCallback() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.orderService.completeQrScanning({ statusCode: '00', partnerTxnUid: `LMS000000${this.order.orderId}` });
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            this.navController.pop();
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }
}
