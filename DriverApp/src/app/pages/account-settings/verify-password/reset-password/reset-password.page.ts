import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {AuthorizationService} from 'src/app/pages/authorization/authorization.service';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {ActivatedRoute} from '@angular/router';
import {RestResponse} from '../../../../models/authorization.model';
import {AuthService} from '../../../../../shared/auth/auth.service';
import {PopoverService} from '../../../../../shared/popover.service';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.page.html',
    styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
    data: any;
    onClickValidation: boolean;
    passwordNotMatched: boolean;
    misMatchCount: number;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService,
                private toastService: ToastService, private navController: NavController, private activatedRoute: ActivatedRoute,
                private authService: AuthService, public modalService: PopoverService) {
    }

    ngOnInit() {
        this.misMatchCount = 0;
        this.data = {} as any;
        this.activatedRoute.queryParams.subscribe(params => {
            this.data.uniqueCode = params.uniqueCode;
            this.data.code = params.code;
        });
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        if (this.data.password !== this.data.confirmPassword) {
            this.passwordNotMatched = true;
            this.onClickValidation = true;
            this.misMatchCount = this.misMatchCount + 1;
            if (this.misMatchCount >= 5) {
                this.authService.logout();
                await this.navController.navigateRoot('login');
            }
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.resetPassword(this.data);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            await this.navController.navigateRoot('reset-successful');
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
        }
    }

    back() {
        this.navController.pop();
    }

    onChange() {
        this.passwordNotMatched = false;
    }
}
