import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {AuthService} from 'src/shared/auth/auth.service';

@Component({
    selector: 'app-reset-successful',
    templateUrl: './reset-successful.page.html',
    styleUrls: ['./reset-successful.page.scss'],
})
export class ResetSuccessfulPage {
    constructor(private authService: AuthService, private navController: NavController) {
    }

    login() {
        this.authService.logout();
        this.navController.navigateRoot('login');
    }
}
