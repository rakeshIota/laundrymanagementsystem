import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../../authorization/authorization.service';
import {AuthService} from '../../../../shared/auth/auth.service';

@Component({
    selector: 'app-verify-password',
    templateUrl: './verify-password.page.html',
    styleUrls: ['./verify-password.page.scss'],
})
export class VerifyPasswordPage implements OnInit {
    oldPassword: string;
    onClickValidation: boolean;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService, private toastService: ToastService,
                private navController: NavController, private authService: AuthService) {
    }

    ngOnInit() {
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const user = this.authService.getUser();
        const loading = this.loadingService.presentLoading();
        try {
            const data = {} as any;
            data.password = this.oldPassword;
            const response: RestResponse = await this.authorizationService.verifyPassword(data);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.error(response.message);
                return;
            }
            await this.navController.navigateRoot('reset-password', {
                queryParams: {
                    uniqueCode: response.data.uniqueCode,
                    code: response.data.code
                }
            });
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.error(e.message);
            await this.navController.navigateRoot('reset-password', {
                queryParams: {
                    userName: user.userName
                }
            });
        }
    }

    back() {
        this.navController.pop();
    }
}
