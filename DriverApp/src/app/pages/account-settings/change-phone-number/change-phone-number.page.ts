import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {User} from 'src/app/models/user.model';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../../authorization/authorization.service';

@Component({
    selector: 'app-change-phone-number',
    templateUrl: './change-phone-number.page.html',
    styleUrls: ['./change-phone-number.page.scss'],
})
export class ChangePhoneNumberPage implements OnInit {
    user: User;
    onClickValidation: boolean;
    this;
    actionSheetOptions = {} as HTMLIonActionSheetElement;

    constructor(private navController: NavController, private authorizationService: AuthorizationService, private loadingService: LoadingService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.user = new User();
        this.user.countryCode = '+66';
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
    }

    back() {
        this.navController.pop();
    }
}
