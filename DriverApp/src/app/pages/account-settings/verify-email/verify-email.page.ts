import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../../authorization/authorization.service';

@Component({
    selector: 'app-verify-email',
    templateUrl: './verify-email.page.html',
    styleUrls: ['./verify-email.page.scss'],
})
export class VerifyEmailPage implements OnInit {
    emailAddress: string;
    onClickValidation: boolean;
    emailForm: FormGroup;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService, private toastService: ToastService,
                private navController: NavController) {
    }

    ngOnInit() {
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        this.navController.navigateForward('otp-verification', {queryParams: {otpType: 'EMAIL'}}); // TODO : REMOVE THIS WHEN API WORKS
        // TODO : UNCOMMENT THIS WHEN API WORKS
        // const loading = this.loadingService.presentLoading();
        // try {
        //     const response: RestResponse = await this.authorizationService.verifyEmail(this.emailAddress);
        //     this.loadingService.hideLoading(loading);
        //     if (!response.status) {
        //         this.toastService.show(response.message);
        //         return;
        //     }
        //     this.navController.navigateForward('otp-verification', { queryParams: { otpType: 'EMAIL' } });
        // } catch (e) {
        //     this.loadingService.hideLoading(loading);
        //     this.toastService.show(e.message);
        // }
    }

    back() {
        this.navController.pop();
    }
}
