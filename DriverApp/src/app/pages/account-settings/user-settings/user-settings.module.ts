import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CommonAppModule } from 'src/app/components/common.app.module';
import { UserSettingsPage } from './user-settings.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{ path: '', component: UserSettingsPage }]),
        CommonAppModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        TranslateModule
    ],
    declarations: [UserSettingsPage]
})
export class UserSettingsPageModule { }
