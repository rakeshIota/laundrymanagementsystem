import {IResourceWithId} from './authorization.model';
import {Address} from './user.model';

export class Order implements IResourceWithId {
    id: string;
    createdOn: Date;
    deliveryDate: Date;
    deliveryType: string;
    totalItems: number;
    status: string;
    paymentType: string;
    totalPrice: number;
    pickupDate: Date;
    deliverySlot: string;
    pickupSlot: string;
    orderId: string;
    customerName: string;
    customerId: string;
    paymentStatus: string;
    paymentMode: string;
    items: Array<any>;
    deliveryAddress: Address;
    itemCount: number;
    orderNo: string;
    customerDetails: any;
    rideType: string;

    constructor() {
        this.items = new Array<any>();
    }
}