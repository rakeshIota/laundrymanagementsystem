import {IResourceWithId} from './authorization.model';
import {BaseModel} from './base.model';

export class User extends BaseModel {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    nickName: string;
    fullName: string;
    mobileNo: string;
    password: string;
    emailConfirmed: boolean;
    mobileNoConfirmed: boolean;
    languageId: string;
    otp: string;
    rememberMe: boolean;
    locality: string;
    gender: string;
    agreementAccepted: boolean;
    securityAnswer: string;
    dob: Date;
    countryCode: string;
    confirmPassword: string;
    userName: string;
    profilePic: any;
    DOB: Date;
    addressdetails: Array<Address>;
    pickupAddress: Address;

    constructor() {
        super();
        this.rememberMe = false;
    }
}

export class Address extends BaseModel {
    residenceType: string;
    houseNumber: string;
    streetNumber: string;
    buildingName: string;
    floor: string;
    unit: string;
    district: string;
    districtId: string;
    subDistrict: string;
    subDistrictId: string;
    postalCode: string;
    phoneNo: string;
    ext: string;
    note: string;
    type: string;
    latitude: number;
    longitude: number;
    districtName: string;
    subDistrictName: string;
    alternatePhoneNo: string;
    provinceId: string;
    province: string;
    provinceName: string;
}

export class UserOtpVerification implements IResourceWithId {
    id: string;
    mobileNo: string;
    otp: string;
    userName: string;
    countryCode: string;
}
