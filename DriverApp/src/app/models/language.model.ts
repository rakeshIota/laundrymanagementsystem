
export class Language {
    id: string;
    name: string;
    code: string;
    icon: string;
    isSelected: boolean;
}