export class PasswordRule {
    lowercaseLetters: string;
    uppercaseLetters: string;
    numbers: string;
    specialCharacters: string;
}