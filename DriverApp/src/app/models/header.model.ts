import { DEFAULT_STRING_VALUE } from 'src/environments/environment.prod';

export class Header {
    title: string;
    showBackButton: boolean;
    showMenu: boolean;
    showModalBackButton: boolean;
    parentLink: string;
    constructor(header?) {
        this.title = header.title || DEFAULT_STRING_VALUE;
        this.showBackButton = header.showBackButton || false;
        this.showMenu = header.showMenu || false;
        this.showModalBackButton = header.showModalBackButton || false;
        this.parentLink = header.parentLink || DEFAULT_STRING_VALUE;
    }
}
