import {IResourceWithId} from './authorization.model';

export class BaseModel implements IResourceWithId {
    id: string;
    createdBy: number;
    createdOn: Date;
    isActive: boolean;
    isDeleted: boolean;
    updatedBy: number;
    updatedOn: Date;
}
