export interface IResourceWithId {
    id: string;
}

export interface RestResponse {
    message: string;
    status: boolean;
    data: any;
}

export class AuthToken {
    accessToken: string;
    expires: Date;
    expires_at: number;
    tokenType: string;
}
