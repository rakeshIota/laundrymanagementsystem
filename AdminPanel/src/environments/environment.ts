import { HttpHeaders } from '@angular/common/http';
const zone = -new Date().getTimezoneOffset();
export const environment = {
  production: false,
   BaseApiUrl: 'http://localhost:61039',
  //BaseApiUrl: 'http://lotuslaundry.iotasol.com',
  //BaseApiUrl: 'https://lotuslaundry.azurewebsites.net',
  AppHeaders: new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    userTimeZone: zone.toString()
  })
};
