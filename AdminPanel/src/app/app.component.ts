import {Component, ViewEncapsulation} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {RoutingState} from './shared/routing-state.service';
import {DataService} from './shared/data.service';
import { ConnectionService } from 'ng-connection-service';
import { CommonService } from './shared/common.service';

@Component({
  selector: '.app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'Welcome to Application';

  constructor(private translate: TranslateService, private routingState: RoutingState, private dataService: DataService, private connectionService: ConnectionService,
    private commonService: CommonService) {
    translate.setDefaultLang('en');
    this.routingState.loadRouting();
    this.dataService.init();
    this.checkConnectionService();
  }

  checkConnectionService() {
    this.connectionService.monitor().subscribe(currentState => {
      if (!currentState) {
        this.commonService.infoAlert('Please check your internet connection', 'Ok', null);
      }
    });
  }
}
