import {Component, OnDestroy, OnInit} from '@angular/core';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {isNullOrUndefined} from 'util';
import {UploadService} from '../upload.service';
import {CommonEventService} from '../common.event.service';

declare const $: any;

@Component({
    selector: 'app-file-cropper',
    templateUrl: './file-cropper.component.html',
    styleUrls: ['./file-cropper.component.scss']
})
export class FileCropperComponent implements OnInit, OnDestroy {

    data: any;
    onSuccesCallback: (data) => void;
    onCancelCallback: () => void;
    request: any;
    searchEvent: any;
    isOpenCrop: boolean;

    constructor(private uploadService: UploadService, private commonEventService: CommonEventService) {
        this.request = {} as any;
        this.isOpenCrop = false;
    }

    ngOnInit() {
        this.searchEvent = this.commonEventService.search.subscribe((response) => {
            if (!isNullOrUndefined(response)) {
                this.loadPopup(response);
                this.commonEventService.clearSearch();
            }
        });
    }

    async loadPopup(data: any) {
        this.data = data.data;
        this.onSuccesCallback = data.callback1;
        this.onCancelCallback = data.callback2;
        this.request = {} as any;
        this.isOpenCrop = true;
        await this.delay(1000);
        await this.delay(1000);
        $('#cropImagePopup').appendTo('body').modal('show');
        if (isNullOrUndefined(this.data.files)) {
            this.data.files = new Array<any>();
        }
        let hasImageFile = false;
        for (let index = 0; index < this.data.files.length; index++) {
            const file = this.data.files[index];
            if (this.uploadService.validImageTypes.includes(file.type)) {
                hasImageFile = true;
            }
        }
        if (!hasImageFile) {
            $('#cropImagePopup').appendTo('body').modal('hide');
            await this.delay(1000);
            this.isOpenCrop = false;
            this.onSuccesCallback(this.data.files);
            return;
        }
        for (let index = 0; index < this.data.files.length; index++) {
            if (this.uploadService.validImageTypes.includes(this.data.files[index].type)) {
                const fileReader = new FileReader();
                fileReader.onload = (e: any) => {
                    this.data.files[index].base64 = e.target.result;
                    this.request.imageBase64 = this.data.files[0].base64;
                };
                fileReader.readAsDataURL(this.data.files[index]);
            }
        }
        this.request.selectedIndex = 0;
    }

    selectCropFile(index: number) {
        this.request.imageBase64 = this.data.files[index].base64;
        this.request.selectedIndex = index;
    }

    applyCrop() {
        this.data.files[this.request.selectedIndex].base64 = this.request.croppedImage;
        this.request.imageBase64 = this.request.croppedImage;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.request.croppedImage = event.base64;
    }

    async uploadImages() {
        $('#cropImagePopup').appendTo('body').modal('hide');
        await this.delay(1000);
        this.isOpenCrop = false;
        const files = new Array<any>();
        for (let index = 0; index < this.data.files.length; index++) {
            if (this.uploadService.validImageTypes.includes(this.data.files[index].type)) {
                files.push(this.uploadService.base64ToFile(this.data.files[index]));
            } else {
                files.push(this.data.files[index]);
            }
        }
        this.onSuccesCallback(files);
    }

    async close() {
        $('#cropImagePopup').appendTo('body').modal('hide');
        await this.delay(1000);
        this.isOpenCrop = false;
        this.onCancelCallback();
    }

    ngOnDestroy() {
        this.searchEvent.unsubscribe();
    }

    async delay(ms: number) {
        await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log('fired'));
    }
}
