import {Injectable} from '@angular/core';

declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor() {
  }

  error(text: string): void {
    $.notify({
      title: '',
      message: text,
    }, {
      type: 'danger',
      allow_dismiss: true,
      placement: {
        from: 'top',
        align: 'right'
      },
      newest_on_top: true,
      offset: 20,
      z_index: 999999999,
      delay: 3000,
      timer: 3000,
    });
  }

  warning(text: string): void {
    $.notify({
      title: '',
      message: text,
    }, {
      type: 'warning',
      allow_dismiss: true,
      placement: {
        from: 'top',
        align: 'right'
      },
      newest_on_top: true,
      offset: 20,
      z_index: 999999999,
      delay: 3000,
      timer: 3000,
    });
  }

  info(text: string): void {
    $.notify({
      title: '',
      message: text,
    }, {
      type: 'info',
      allow_dismiss: true,
      placement: {
        from: 'top',
        align: 'right'
      },
      newest_on_top: true,
      offset: 20,
      z_index: 999999999,
      delay: 3000,
      timer: 3000,
    });
  }

  success(text: string): void {
    $.notify({
      title: '',
      message: text,
    }, {
      type: 'success',
      allow_dismiss: true,
      placement: {
        from: 'top',
        align: 'right'
      },
      newest_on_top: true,
      offset: 20,
      z_index: 999999999,
      delay: 3000,
      timer: 3000,
    });
  }
}
