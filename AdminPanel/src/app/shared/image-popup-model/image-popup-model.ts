import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { CommonEventService } from '../common.event.service';
import { isNullOrUndefined } from 'util';

declare const $: any;

@Component({
    selector: 'app-image-popup-model',
    templateUrl: './image-popup-model.html',
    styleUrls: ['./image-popup-model.scss']
})
export class ImagePopUPModel implements OnInit, OnDestroy {
    data: any;
    clickEvent: any;
    imageUrl: string | null;
    constructor(private commonEventService: CommonEventService) {
        this.imageUrl = null;
    }

    ngOnInit() {
        this.clickEvent = this.commonEventService.clickevent.subscribe(response => {
            if (isNullOrUndefined(response)) {
                return;
            }
            this.openPopUp(response);
        })
    }

    openPopUp(data: any) {
        $('#imageFilePopUP').appendTo('body').modal('show');
        this.imageUrl = data.data;
    }

    ngOnDestroy() {
        this.clickEvent.unsubscribe();
    }
}
