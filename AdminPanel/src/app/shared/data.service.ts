import {Injectable} from '@angular/core';
import {RestResponse} from './auth.model';
import {ToastService} from './toast.service';
import {AppSettingsService} from '../services/app.settings.service';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  appSettings: Array<any>;

  constructor(private appSettingService: AppSettingsService, private toastService: ToastService) {
  }

  async init() {
    try {
      const response: RestResponse = await this.appSettingService.fetch().toPromise();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.appSettings = response.data;
    } catch (e) {
      this.toastService.error(e.message);
    }
  }

  async getAppSettings() {
    if (!isNullOrUndefined(this.appSettings)) {
      return this.appSettings;
    }
    await this.init();
    return this.appSettings;
  }
}
