import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
    constructor() { }

    /**
     * Method checks if email is valid?
     * @param arg: value to be validated
     */
    static isEmailInValid(arg:string) : boolean {
    	return !(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(arg));
    }

    /**
     * Method checks if URL is valid?
     * @param arg: value to be validated
     */
    static isUrlInValid(arg:string) : boolean {
    	return false;
    	//return !(/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/.test(arg));
    }

    /**
     * Method checks if Phone Number is valid?
     * @param arg: value to be validated
     */
    static isPhoneNumberInValid(arg:any) : boolean {
    	return false;
    	// return !(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(arg));
    }

}
