import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CommonEventService {
    private performSearch = new BehaviorSubject<any>(null);
    private performClick = new BehaviorSubject<any>(null);
    
    search = this.performSearch.asObservable();
    clickevent = this.performClick.asObservable();

    showSearch(data: any, callback1: Function, callback2: Function) {
        this.performSearch.next({ data: data, callback1: callback1, callback2: callback2 });
    }
    
    showImageOnClick(data: any) {
        this.performClick.next({ data: data });
    }
    
    clearSearch() {
		this.performSearch.next(null);
	}
}
