import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router, RoutesRecognized} from '@angular/router';
import {LocalStorageService} from 'angular-2-local-storage';
import {isNullOrUndefined} from 'util';
import {Constant} from '../config/constants';
import {IResourceWithId} from './auth.model';
import {HttpServiceRequests} from './http.service';
import {filter, map, mergeMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends HttpServiceRequests<IResourceWithId> {

  constructor(public http: HttpClient, private localStorageService: LocalStorageService, private router: Router,
              private activatedRoute: ActivatedRoute) {
    super(http);
  }

  logout() {
    this.localStorageService.remove('token');
    this.localStorageService.remove('user');
    this.localStorageService.remove('project');
    this.router.navigate(['login']);
  }

  getToken(): any {
    return this.localStorageService.get('token') as any;
  }

  getUser(): any {
    return this.localStorageService.get('user') as any;
  }

  getRoles() {
    const user = this.localStorageService.get('user') as any;
    if (isNullOrUndefined(user)) {
      return '';
    }
    return user.roles;
  }

  hasRole(roles: any[]): boolean {
    // this is used in case user has single role
    // return roles.indexOf(this.getRoles()) !== -1;
    return roles.some(r => this.getUser().roles.includes(r));
  }

  hasValidToken(): boolean {
    const token: any = this.getToken();
    return !isNullOrUndefined(token) && token.accessToken && token.expires_at && token.expires_at > new Date().getTime();
  }

  isAuthorizedUser(roles: Array<string>) {
    const promise = new Promise((resolve) => {
      if (!this.hasValidToken()) {
        this.localStorageService.remove('token');
        this.localStorageService.remove('user');
      }
      resolve({hasAccess: this.hasValidToken(), hasRoleAccess: roles.some(x => this.getRoles().indexOf(x) !== -1)});
    });
    return promise;
  }

  isAccessible(ENTITY, VIEW) {
    const userRoles = this.getUser().roles;
    if (isNullOrUndefined(ENTITY) || isNullOrUndefined(VIEW)) {
      return false;
    }
    const allowedRoles = Constant.VIEW_USER_MAPPING[ENTITY + '_ACCESS'][VIEW];
    if (isNullOrUndefined(allowedRoles)) {
      return false;
    }
    return allowedRoles.SHOW_TO_ROLE.some((ele) => userRoles.includes(ele));
  }

  isDisabled(ENTITY, VIEW) {
    const userRoles = this.getUser().roles;
    if (isNullOrUndefined(ENTITY) || isNullOrUndefined(VIEW)) {
      return false;
    }
    const allowedRoles = Constant.VIEW_USER_MAPPING[ENTITY + '_ACCESS'][VIEW];
    if (isNullOrUndefined(allowedRoles)) {
      return true;
    }
    return !allowedRoles.ENABLED_FOR_ROLE.some((ele) => userRoles.includes(ele));
  }
}
