import {Injectable} from '@angular/core';
import {IResourceWithId, RestResponse} from './auth.model';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs/index';
import {environment} from 'src/environments/environment';
import {FilterParam} from '../models/filterparam';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceRequests<TResource extends IResourceWithId> {
  headers = environment.AppHeaders;

  constructor(public http: HttpClient) {
  }

  getRecords(path: string, filterParam: FilterParam): Promise<RestResponse> {
    return this.http
      .post(environment.BaseApiUrl + path, filterParam, {headers: environment.AppHeaders})
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  getRecord(path: string): Observable<RestResponse> {
    return this.http
      .get(environment.BaseApiUrl + path, {headers: environment.AppHeaders})
      .pipe(catchError(this.handleError));
  }

  saveRecord(path: string, resource: TResource): Promise<RestResponse> {
    return this.http
      .post(environment.BaseApiUrl + path, resource, {headers: environment.AppHeaders})
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  updateRecord(path: string, resource: TResource): Promise<RestResponse> {
    return this.http
      .put(environment.BaseApiUrl + path, resource, {headers: environment.AppHeaders})
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  removeRecord(path: string): Promise<RestResponse> {
    return this.http
      .delete(environment.BaseApiUrl + path, {headers: environment.AppHeaders})
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  postUrl(path: string): Promise<RestResponse> {
    return this.http
      .post(environment.BaseApiUrl + path, {headers: environment.AppHeaders})
      .toPromise()
      .then((response) => {
        return response;
      })
      .catch(this.handleError);
  }

  fetchRecords(path: string): Observable<RestResponse> {
    return this.http
      .get(environment.BaseApiUrl + path, {headers: environment.AppHeaders})
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Promise<any> {
    return Promise.reject(error.error);
  }
}
