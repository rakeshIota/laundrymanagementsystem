import { Injectable } from '@angular/core';
// import * as XLSX from 'xlsx';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

const EXCEL_TYPE = 'application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet;charset=UTF-8';

const EXCEL_EXTENSION = '.xlsx';

@Injectable({
    providedIn: 'root'
})
export class ExcelService {

    constructor() { }

    private toExportFileName(excelFileName: string): string {
        return `${excelFileName}.xlsx`;
    }

    // public exportAsExcelFile(json: any[], excelFileName: string, colName: string[]): void {
    //     const worksheet: XLSX.WorkSheet = XLSX.utils.sheet_add_json({}, json, { origin: 0 })
    //     for (var i = 0; i < colName.length; i++) {
    //         var tempChar = String.fromCharCode(97 + i).toUpperCase();
    //         worksheet[tempChar + "1"].v = colName[i];
    //         worksheet[tempChar + "1"].s = { color: "red" };
    //     }
    //     const workbook: XLSX.WorkBook = { Sheets: { [excelFileName]: worksheet }, SheetNames: [excelFileName] };
    //     XLSX.writeFile(workbook, this.toExportFileName(excelFileName));
    // }


    // public exportAsExcelMultipleSheet(json: any[]): any {
    //     const ws = XLSX.utils.json_to_sheet(json[0]);
    //     const worksheet: XLSX.WorkSheet = XLSX.utils.sheet_add_json(ws, json[1], { origin: 3 });
    //     return worksheet;
    // }

    // public writeFile(wb, templatesName, reportName) {
    //     /** Creates a new workbook */
    //     let workbook: XLSX.WorkBook = XLSX.utils.book_new();
    //     wb.forEach((ele, index) => {
    //         /** Append a worksheet to a workbook */
    //         XLSX.utils.book_append_sheet(workbook, ele, templatesName[index].length > 31 ? `File${index}` : templatesName[index]);
    //     });
    //     XLSX.writeFile(workbook, `${reportName}.xlsx`);
    // }

    public exportAsExcelFile(json: any[], excelFileName: string, headersArray: any[]): void {
        //Excel Title, Header, Data
        const header = headersArray;
        const keyHeaders = Object.keys(json[0]);
        const data = json;
        //Create workbook and worksheet
        let workbook = new Workbook();
        let worksheet = workbook.addWorksheet(excelFileName);
        //Add Header Row
        let headerRow = worksheet.addRow(header);
        this.styleCell(headerRow, true);
        // Add Data and Conditional Formatting
        data.forEach((element) => {
            let eachRow = [];
            keyHeaders.forEach((headers) => {
                eachRow.push(element[headers])
            });
            let dataRow = worksheet.addRow(eachRow);
            this.styleCell(dataRow, false);
        });
        worksheet.columns.forEach(ele => {
            ele.width = 40;
        });
        workbook.xlsx.writeBuffer().then((data) => {
            let blob = new Blob([data], { type: EXCEL_TYPE });
            fs.saveAs(blob, excelFileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        })
    }

    styleCell(row: any, isHeaderRow: boolean) {
        // Cell Style : Fill and Border
        row.eachCell((cell, number) => {
            if(isHeaderRow){
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FFFFFF00' },
                    bgColor: { argb: 'FF0000FF' }
                }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            cell.alignment = { wrapText: true };

        });
    }

}
