import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class PrintReportService {
    constructor() { }

    /**
     * 
     * @param innerBody 
     * @param reportTitle 
     */
    print(innerBody: any, reportTitle: string) {
        let printContents, popupWin;
        printContents = innerBody;

        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,resizable=1');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>${reportTitle}</title>
                <!-- Latest compiled and minified CSS -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                 integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

}
