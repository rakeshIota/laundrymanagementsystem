import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class CommonUtil {

    logging(type, message, functionName, fileName) {
        if (!environment.production) {
            console.log('Type:' + type + ' In Function:' + functionName + ' In File: ' + fileName);
            console.log(message);
        }
    }

    toggleMenu() {
        if ($('#sidebar').hasClass('active')) {
            $('#sidebar').addClass('content-body-active');
            $('#sidebar').removeClass('active');
            $('#content').addClass('window-content-body');
            $('#content').removeClass('mobile-content-body');
        } else {
            $('#sidebar').addClass('active');
            $('#sidebar').removeClass('content-body-active');
            $('#content').removeClass('window-content-body');
            $('#content').addClass('mobile-content-body');
        }
    }
}
