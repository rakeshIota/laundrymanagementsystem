import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from '../config/base.service';
import { FilterParam } from '../models/filterparam';
import { RestResponse } from '../shared/auth.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseService {

  constructor(public http: HttpClient) {
    super(http, '/api/account/user', '/api/account/users');
  }

  fetchUser(id: number): Observable<RestResponse> {
    return this.getRecord('/api/account/user/' + id);
  }

  fetchDriver(id: number): Observable<RestResponse> {
    return this.getRecord('/api/account/driver/' + id);
  }

  save(data: any): Promise<RestResponse> {
    return this.saveRecord('/api/account/register', data);
  }

  update(data: any): Promise<RestResponse> {
    return this.updateRecord('/api/account/update', data);
  }

  GetAllRoles(data: any): Promise<RestResponse> {
    return this.getRecords('/api/account/roles', data);
  }

  fetchAllUsers(filterParam: FilterParam): Promise<RestResponse> {
    return this.getRecords('/api/account/GetCompanies', filterParam);
  }

  fetchCustomer(customerId: string): Observable<RestResponse> {
    return this.getRecord(`/api/account/customer/${customerId}`);
  }

  getDriverLocation(filterParam: FilterParam): Promise<RestResponse> {
    return this.getRecords('/api/driver/locations', filterParam);
  }

  saveNotes(data: any): Promise<RestResponse> {
    return this.updateRecord('/api/account/admin/customerNote', data);
  }
}
