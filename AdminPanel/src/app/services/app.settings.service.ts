import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResourceWithId, RestResponse} from '../shared/auth.model';
import {HttpServiceRequests} from '../shared/http.service';

@Injectable({
  providedIn: 'root'
})
export class AppSettingsService extends HttpServiceRequests<IResourceWithId> {

  constructor(public http: HttpClient) {
    super(http);
  }

  fetch(): Observable<RestResponse> {
    return this.fetchRecords('/app/application/settings');
  }
}
