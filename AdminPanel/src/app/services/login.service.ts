import {Injectable} from '@angular/core';
import {HttpServiceRequests} from '../shared/http.service';
import {IResourceWithId, RestResponse} from '../shared/auth.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LoginService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    login(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/account/login', data);
    }

    resetPassword(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/account/forgot/password', data);
    }

    recoverPassword(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/account/reset/password', data);
    }

    fetchCompanies(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/account/GetCompanies', data);
    }
}
