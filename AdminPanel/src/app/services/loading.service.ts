import {Injectable} from '@angular/core';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private spinnerService: Ng4LoadingSpinnerService) {
  }

  show() {
    this.spinnerService.show();
  }

  hide() {
    this.spinnerService.hide();
  }
}
