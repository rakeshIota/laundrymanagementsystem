import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { PostalCode } from '../../../models/postalcode';
import { ActivatedRoute, Router } from '@angular/router';
import { PostalCodeManager } from '../postalcode.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-postalcode-detail',
  templateUrl: './postalcode-detail.component.html',
  styleUrls: ['./postalcode-detail.component.scss']
})
export class PostalCodeDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected postalCodeManager: PostalCodeManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(postalCodeManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new PostalCode();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "PostalCode";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
