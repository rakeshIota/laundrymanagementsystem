import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';

import { PostalCode } from '../../../models/postalcode';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import { CommonUtil } from '../../../shared/common.util';
import { PostalCodeManager } from '../postalcode.manager';

declare const $: any;

@Component({
  selector: 'app-postalcode-edit',
  templateUrl: './postalcode-edit.component.html',
  styleUrls: ['./postalcode-edit.component.scss']
})

export class PostalCodeEditComponent extends BaseEditComponent implements OnInit {
  public postalCode: PostalCode;

  constructor(protected route: ActivatedRoute, protected postalcodeManager: PostalCodeManager,
    protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
    protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
    public commonUtil: CommonUtil) {
    super(postalcodeManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.postalCode = new PostalCode();
    this.postalCode.isActive = true;
    this.setRecord(this.postalCode);


    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.postalCode = PostalCode.fromResponse(this.record);
    this.setRecord(this.postalCode);
  }


  afterFetchAssociatedCompleted() {
  }

  onSaveSuccess(data: any) {
    this.toastService.success(this.translateService.instant('PostalCode.POSTAL_ADD_SUCCESS'));
    this.navigate('/dashboard/postal-code');
  }
}
