import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { PostalCodeService } from './postalcode.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class PostalCodeManager extends BaseManager {

    constructor(private postalCodeService: PostalCodeService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(postalCodeService, loadingService, toastService);
    }
}
