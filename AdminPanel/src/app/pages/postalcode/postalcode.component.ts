import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { BaseListComponent } from '../../config/base.list.component';
import { PostalCode } from '../../models/postalcode';
import { LoadingService } from '../../services/loading.service';
import { RestResponse } from '../../shared/auth.model';
import { AuthService } from '../../shared/auth.services';
import { CommonService } from '../../shared/common.service';
import { CommonUtil } from '../../shared/common.util';
import { ToastService } from '../../shared/toast.service';
import { PostalCodeManager } from './postalcode.manager';
import { TranslateService } from '@ngx-translate/core';

declare const $: any;

@Component({
  selector: 'app-postalcode',
  templateUrl: './postalcode.component.html',
  styleUrls: ['./postalcode.component.scss']
})
export class PostalCodeComponent extends BaseListComponent implements OnInit, OnDestroy {

  constructor(protected postalCodeManager: PostalCodeManager, protected toastService: ToastService,
    protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService,
    protected router: Router, public commonUtil: CommonUtil, private translateService: TranslateService) {
    super(postalCodeManager, commonService, toastService, loadingService, router);
  }

  ngOnInit() {
    this.request.loadEditPage = false;
    this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
    this.records = new Array<PostalCode>();
    this.init();
  }

  onItemSelection(record: any) {
    this.onAssociatedValueSelected(record);
  }

  onCancel() {
    this.request.loadEditPage = false;
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  onNewRecord() {
    if (!this.isPlusButton) {
      if (this.filterParam) {
        this.router.navigate(['/dashboard/postal-code/edit/0'], { queryParams: { [this.filterParam.relationTable]: this.filterParam.relationId } });
      } else {
        this.router.navigate(['/dashboard/postal-code/edit/0']);
      }
      return;
    }
    this.request.loadEditPage = true;
  }

  removeSuccess() {
    this.onCancel();
  }


  ngOnDestroy() {
    this.clean();
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#postalCodeDetailPage').appendTo('body').modal('show');
      $('#postalCodeDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }

  toggle(record) {
    this.save(record);
  }

  async save(record: PostalCode) {
    const temp = Object.assign({}, record);
    temp.isActive = !temp.isActive;
    try {
      this.loadingService.show();
      const response: RestResponse = await this.manager.update(temp);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      record.isActive = !record.isActive;
      if (!record.isActive) {
        this.toastService.error(`${record.postalCode} ${this.translateService.instant('PostalCode.POSTAL_DISABLED')}`);
        return;
      }
      this.toastService.success(`${record.postalCode} ${this.translateService.instant('PostalCode.POSTAL_ENABLED')}`);
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }
}
