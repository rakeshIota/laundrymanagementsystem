import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';

import {BaseEditComponent} from '../../../config/base.edit.component';
import {BaseModel} from '../../../config/base.model';

import {DefaultMessage} from '../../../models/defaultmessage';
import {LoadingService} from '../../../services/loading.service';

import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {DefaultMessageManager} from '../defaultmessage.manager';


import {CommonEventService} from '../../../shared/common.event.service';

declare const $: any;

@Component({
	selector: 'app-defaultmessage-edit',
	templateUrl: './defaultmessage-edit.component.html',
	styleUrls: ['./defaultmessage-edit.component.scss']
})

export class DefaultMessageEditComponent extends BaseEditComponent implements OnInit {
	public defaultMessage: DefaultMessage;

	constructor(protected route: ActivatedRoute, protected defaultmessageManager: DefaultMessageManager,
				protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
				protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
				public commonUtil: CommonUtil) {
		super(defaultmessageManager, commonService, toastService, loadingService, route, router, translateService);
	}

	ngOnInit() {
		this.defaultMessage = new DefaultMessage();
		this.defaultMessage.isActive = true;
		this.setRecord(this.defaultMessage);


		this.isPlusButton = !isNullOrUndefined(this.onCancel);
		this.init();
	}

	onFetchCompleted() {
		this.defaultMessage = DefaultMessage.fromResponse(this.record);
		this.setRecord(this.defaultMessage);
	}


	afterFetchAssociatedCompleted() {
	}

	onSaveSuccess(data: any) {
		this.navigate('/dashboard/default-message');
	}


}
