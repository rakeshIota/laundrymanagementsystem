import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { DefaultMessage } from '../../../models/defaultmessage';
import { ActivatedRoute, Router } from '@angular/router';
import { DefaultMessageManager } from '../defaultmessage.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-defaultmessage-detail',
  templateUrl: './defaultmessage-detail.component.html',
  styleUrls: ['./defaultmessage-detail.component.scss']
})
export class DefaultMessageDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected defaultMessageManager: DefaultMessageManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(defaultMessageManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new DefaultMessage();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "DefaultMessage";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
