import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { DefaultMessageService } from './defaultmessage.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class DefaultMessageManager extends BaseManager {

    constructor(private defaultMessageService: DefaultMessageService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(defaultMessageService, loadingService, toastService);
    }
}
