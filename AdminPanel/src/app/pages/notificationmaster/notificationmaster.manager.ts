import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { NotificationMasterService } from './notificationmaster.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationMasterManager extends BaseManager {

    constructor(private notificationMasterService: NotificationMasterService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(notificationMasterService, loadingService, toastService);
    }
}
