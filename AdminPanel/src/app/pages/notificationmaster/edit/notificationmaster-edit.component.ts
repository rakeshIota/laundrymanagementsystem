import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { NotificationMaster } from '../../../models/notificationmaster';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { NotificationMasterManager } from '../notificationmaster.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-notificationmaster-edit',
  templateUrl: './notificationmaster-edit.component.html',
  styleUrls: ['./notificationmaster-edit.component.scss']
})

export class NotificationMasterEditComponent extends BaseEditComponent implements OnInit {
  public notificationMaster: NotificationMaster;
  
  constructor(protected route: ActivatedRoute, protected notificationmasterManager: NotificationMasterManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(notificationmasterManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.notificationMaster = new NotificationMaster();
  	this.notificationMaster.isActive=true;   
    this.setRecord(this.notificationMaster);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.notificationMaster = NotificationMaster.fromResponse(this.record);
    this.setRecord(this.notificationMaster);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/notification-master');
  }	
	  
	
}
