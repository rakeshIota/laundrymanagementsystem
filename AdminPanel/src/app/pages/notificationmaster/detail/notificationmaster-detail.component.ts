import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { NotificationMaster } from '../../../models/notificationmaster';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationMasterManager } from '../notificationmaster.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-notificationmaster-detail',
  templateUrl: './notificationmaster-detail.component.html',
  styleUrls: ['./notificationmaster-detail.component.scss']
})
export class NotificationMasterDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected notificationMasterManager: NotificationMasterManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(notificationMasterManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new NotificationMaster();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "NotificationMaster";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
