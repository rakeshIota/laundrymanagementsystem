import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { Language } from '../../../models/language';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { LanguageManager } from '../language.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-language-edit',
  templateUrl: './language-edit.component.html',
  styleUrls: ['./language-edit.component.scss']
})

export class LanguageEditComponent extends BaseEditComponent implements OnInit {
  public language: Language;
  
  constructor(protected route: ActivatedRoute, protected languageManager: LanguageManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(languageManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.language = new Language();
  	this.language.isActive=true;   
    this.setRecord(this.language);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.language = Language.fromResponse(this.record);
    this.setRecord(this.language);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/language');
  }	
	  
	
}
