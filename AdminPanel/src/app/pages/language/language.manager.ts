import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { LanguageService } from './language.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class LanguageManager extends BaseManager {

    constructor(private languageService: LanguageService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(languageService, loadingService, toastService);
    }
}
