import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/auth.services';
import {CommonUtil} from '../../shared/common.util';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor(public authService: AuthService, public commonUtil: CommonUtil) {
  }

  ngOnInit() {
  }

}
