import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {FilterParam} from 'src/app/models/filterparam';
import {RestResponse} from 'src/app/shared/auth.model';
import {BaseService} from '../../config/base.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseService {

  constructor(public http: HttpClient) {
    super(http, '/api/order', '/api/orders');
  }

  getOrderStats(): Promise<RestResponse> {
    return this.getRecords('/api/order/stats', null);
  }

  getDriverLocations(data: any): Promise<RestResponse> {
    return this.getRecords('/api/driver/locations', data);
  }

  getDrivers(): Promise<RestResponse> {
    return this.getRecords('/api/account/admin/drivers', null);
  }

  getOrders(): Promise<RestResponse> {
    return this.getRecords('/api/orders', null);
  }

  cancelOrder(orderId: string): Promise<RestResponse> {
    return this.saveRecord(`/api/application/order/${orderId}/cancel`, null);
  }

  assignDriver(order): Promise<RestResponse> {
    return this.updateRecord('/api/Assign/Driver', order);
  }

  printPdf(orderId: string): Promise<RestResponse> {
    return this.saveRecord(`/api/orderpdf/${orderId}`, null);
  }

  updateOrder(order): Promise<RestResponse> {
    return this.updateRecord('/api/admin/order/orderId', order);
  }

  updateOrderItems(order): Promise<RestResponse> {
    return this.updateRecord('/api/admin/Order/orderitems', order);
  }
}

