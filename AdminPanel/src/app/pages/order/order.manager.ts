import {Injectable} from '@angular/core';
import {CustomerFilterOption, Order, OrderStatus} from 'src/app/models/order';
import {LoadingService} from 'src/app/services/loading.service';
import {RestResponse} from 'src/app/shared/auth.model';
import {ToastService} from 'src/app/shared/toast.service';
import {isNullOrUndefined} from 'util';
import {BaseManager} from '../../config/base.manager';
import {DistrictService} from '../district/district.service';
import {SubDistrictService} from '../subdistrict/subdistrict.service';
import {OrderService} from './order.service';

@Injectable({
  providedIn: 'root'
})
export class OrderManager extends BaseManager {

  constructor(private orderService: OrderService, protected loadingService: LoadingService, protected toastService: ToastService, protected districtService: DistrictService, protected subDistrictService: SubDistrictService) {
    super(orderService, loadingService, toastService);
  }

  getOrders(): Promise<any> {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.getOrders();
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  getOrderStats(): Promise<any> {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.getOrderStats();
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  getDriverLocations(data) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.getDriverLocations(data);
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  getDrivers() {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.getDrivers();
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  assignDriver(order) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.assignDriver(order);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }

  updateOrder(order) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.updateOrder(order);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }

  cancelOrder(orderId: string) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.cancelOrder(orderId);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }

  generateStatusNames(order?: Order, orders?: Order[]) {
    if (!isNullOrUndefined(order)) {
      this.generateNames(order);
    } else if (!isNullOrUndefined(orders) && !isNullOrUndefined(orders.length > 0)) {
      orders.forEach(x => this.generateNames(x));
    }
  }

  generateNames(x: Order) {
    if (!isNullOrUndefined(x.status)) {
      switch (x.status) {
        case 'NEW':
          x.statusSlug = 'New';
          break;
        case 'ASSIGNED_DRIVER':
          x.statusSlug = 'Driver Assigned';
          break;
        case 'AWAITING_COLLECTION':
          x.statusSlug = 'Awaiting Collection';
          break;
        case 'COLLECTED':
          x.statusSlug = 'Collected';
          break;
        case 'CLEANING_IN_PROGRESS':
          x.statusSlug = 'Cleaning In Progress';
          break;
        case 'CLEANING_COMPLETED':
          x.statusSlug = 'Cleaning Completed';
          break;
        case 'ASSIGNED_DRIVER_FOR_DELIVERY':
          x.statusSlug = 'Driver Assigned For Delivery';
          break;
        case 'ON_THE_WAY':
          x.statusSlug = 'On The Way';
          break;
        case 'DELIVERED':
          x.statusSlug = 'Delivered';
          break;
        case 'CANCELLED':
          x.statusSlug = 'Cancelled';
          break;
      }
    }

    if (!isNullOrUndefined(x.deliveryType)) {
      switch (x.deliveryType) {
        case 'NORMAL':
          x.deliveryTypeSlug = 'Standard';
          break;
        case 'SAMEDAY':
          x.deliveryTypeSlug = 'Same Day';
          break;
        case 'EXPRESS':
          x.deliveryTypeSlug = 'Express';
          break;
      }
    }

    if (!isNullOrUndefined(x.paymentStatus)) {
      switch (x.paymentStatus) {
        case 'PAID':
          x.paymentStatusSlug = 'PAID';
          break;
        case 'UNPAID':
          x.paymentStatusSlug = 'NOT PAID';
          break;
      }
    }

    if (!isNullOrUndefined(x.paymentType)) {
      switch (x.paymentType) {
        case 'CARD':
          x.paymentTypeSlug = 'Card';
          break;
        case 'CASH':
          x.paymentTypeSlug = 'Cash';
          break;
        case 'QRCODE':
          x.paymentTypeSlug = 'QR Code';
          break;
      }
    }
  }

  async getSpecificOrderStatuses(statuses?: Array<string>) {
    const orderStatuses: OrderStatus[] = [
      {name: 'Driver Assigned', status: 'ASSIGNED_DRIVER'},
      {name: 'Awaiting Collection', status: 'AWAITING_COLLECTION'},
      {name: 'Collected', status: 'COLLECTED'},
      {name: 'Cleaning In Progress', status: 'CLEANING_IN_PROGRESS'},
      {name: 'Cleaning Completed', status: 'CLEANING_COMPLETED'},
      {name: 'Driver Assigned For Delivery', status: 'ASSIGNED_DRIVER_FOR_DELIVERY'},
      {name: 'On The Way', status: 'ON_THE_WAY'},
      {name: 'Delivered', status: 'DELIVERED'},
      {name: 'Cancelled', status: 'CANCELLED'}
    ];
    return orderStatuses.filter(orderStatus => statuses.find(status => status === orderStatus.status));
  }

  async getAllOrderStatuses() {
    const orderStatuses: OrderStatus[] = [
      {name: 'New', status: 'NEW'},
      {name: 'Driver Assigned', status: 'ASSIGNED_DRIVER'},
      {name: 'Awaiting Collection', status: 'AWAITING_COLLECTION'},
      {name: 'Collected', status: 'COLLECTED'},
      {name: 'Cleaning In Progress', status: 'CLEANING_IN_PROGRESS'},
      {name: 'Cleaning Completed', status: 'CLEANING_COMPLETED'},
      {name: 'Driver Assigned For Delivery', status: 'ASSIGNED_DRIVER_FOR_DELIVERY'},
      {name: 'On The Way', status: 'ON_THE_WAY'},
      {name: 'Delivered', status: 'DELIVERED'},
      {name: 'Cancelled', status: 'CANCELLED'}
    ];
    return orderStatuses;
  }

  async getCustomerFilterOptions() {
    const filterOptions: CustomerFilterOption[] = [
      {name: 'Order Id', filterType: 'ORDER_ID'},
      {name: 'Customer Name', filterType: 'CUSTOMER_NAME'},
      {name: 'Customer Email', filterType: 'EMAIL'},
      {name: 'Customer Mobile No', filterType: 'CUSTOMER_PHONE_NUMBER'}
    ];
    return filterOptions;
  }

  async getFilterOptions() {
    const filterOptions: CustomerFilterOption[] = [
      {name: 'First Name', filterType: 'FIRST_NAME'},
      {name: 'Last Name', filterType: 'LAST_NAME'},
      {name: 'Nickname', filterType: 'NICK_NAME'},
      {name: 'Email', filterType: 'EMAIL'},
      {name: 'Customer Mobile No', filterType: 'PHONE_NUMBER'},
      {name: 'Address', filterType: 'ADDRESS'},
      {name: 'Registered Date', filterType: 'REGISTERED_DATE'},
      {name: 'Last Order Date', filterType: 'LAST_ORDER_DATE'}
    ];
    return filterOptions;
  }

  updateOrderItems(order: Order) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.updateOrderItems(order);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }

  printPdf(orderId: string) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.orderService.printPdf(orderId);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }
}
