import {HttpClient} from '@angular/common/http';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import * as lodash from 'lodash';
import {Subject} from 'rxjs';
import {District} from 'src/app/models/district';
import {CustomerFilterOption, DataTablesResponse, Order, OrderFilterOption, OrderStatus} from 'src/app/models/order';
import {OrderStats} from 'src/app/models/order-stats';
import {SubDistrict} from 'src/app/models/subdistrict';
import {RestResponse} from 'src/app/shared/auth.model';
import {CommonService} from 'src/app/shared/common.service';
import {environment} from 'src/environments/environment';
import {isNullOrUndefined} from 'util';
import * as XLSX from 'xlsx';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../shared/auth.services';
import {CommonUtil} from '../../shared/common.util';
import {ToastService} from '../../shared/toast.service';
import {DistrictManager} from '../district/district.manager';
import {SubDistrictManager} from '../subdistrict/subdistrict.manager';
import {OrderManager} from './order.manager';

declare const $: any;
declare const google;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, AfterViewInit {
  records: Order[];
  orderStats: OrderStats;
  showLocationModal: boolean;
  showDriverAssignModal: boolean;
  selectedDriver: any;
  drivers: any[];
  selectedOrder: Order;
  selectedId: string;
  hasDataLoad: boolean;
  driverLocations: any[];
  selectedDriverId: string;
  dtOptions: DataTables.Settings;
  dtTrigger: Subject<any>;
  filterOption: OrderFilterOption;
  orderStatuses: OrderStatus[];
  districts: District[];
  subDistricts: SubDistrict[];
  customerFilterOptions: CustomerFilterOption[];
  searchTerm: string;
  selectedOrderDriver: any;
  currentUrl: string;

  constructor(private orderManager: OrderManager, private toastService: ToastService, private loadingService: LoadingService, public authService: AuthService,
              private router: Router, public commonUtil: CommonUtil, private commonService: CommonService, private http: HttpClient, private districtManager: DistrictManager, private subDistrictManager: SubDistrictManager) {
    this.dtTrigger = new Subject();
    this.currentUrl = lodash.cloneDeep(this.router.url);
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.currentUrl !== val.url) {
          setTimeout(() => {
            $('#assignDriver').modal('hide');
          }, 100);
        }
      }
    });
  }

  async ngOnInit() {
    this.initializeEntities();
    this.getFilterOptions();
    this.getOrders();
    await this.getOrderStats();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 500);
  }

  initializeEntities() {
    this.hasDataLoad = false;
    this.showLocationModal = false;
    this.showDriverAssignModal = false;
    this.orderStats = new OrderStats();
    this.selectedDriver = {};
    this.drivers = new Array<any>();
    this.driverLocations = new Array<any>();
    this.selectedOrder = new Order();
  }

  getOrders() {
    this.dtOptions = {
      order: [],
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      searching: false,
      autoWidth: false,
      processing: false,
      ajax: (dataTablesParameters: any, dataTableCallback) => {
        this.fetchRecords(dataTablesParameters, dataTableCallback);
        $('#customerSearchButton').on('click', event => {
          this.fetchRecords(dataTablesParameters, dataTableCallback);
        });
      },
    };
  }

  async fetchRecords(dataTablesParameters, callback) {
    await this.createParams(dataTablesParameters);
    await this.checkforFilters(dataTablesParameters);
    this.http.post<DataTablesResponse>(environment.BaseApiUrl + '/api/ordersList', dataTablesParameters, {}).subscribe(resp => {
      if (!isNullOrUndefined(resp.data)) {
        this.records = resp.data;
        this.orderManager.generateStatusNames(null, this.records);
        resp.recordsFiltered = resp.recordsTotal = resp.data.length > 0 ? resp.data[0].totalCount : 0;
        callback({recordsTotal: resp.recordsFiltered, recordsFiltered: resp.recordsFiltered, data: []});
      }
    });
  }

  async createParams(dataTablesParameters) {
    dataTablesParameters.offset = (dataTablesParameters.start / dataTablesParameters.length) + 1;
    dataTablesParameters.next = dataTablesParameters.length;
    dataTablesParameters.order.forEach(element => {
      dataTablesParameters.filterType = element.dir.toUpperCase();
      switch (element.column) {
        case 0:
          dataTablesParameters.filterOn = 'ORDER_ID';
          break;
        case 1:
          dataTablesParameters.filterOn = 'CUSTOMER_FULLNAME';
          break;
        case 2:
          dataTablesParameters.filterOn = 'CREATED_ON';
          break;
        case 3:
          dataTablesParameters.filterOn = 'PICKUP_DATE';
          break;
        case 4:
          dataTablesParameters.filterOn = 'DELIVERY_DATE';
          break;
        case 5:
          dataTablesParameters.filterOn = 'DELIVERY_TYPE';
          break;
        case 6:
          dataTablesParameters.filterOn = 'DISTRICT_NAME';
          break;
        case 7:
          dataTablesParameters.filterOn = 'SUBDISTRICT_NAME';
          break;
        case 8:
          dataTablesParameters.filterOn = 'TOTAL_PRICE';
          break;
        case 9:
          dataTablesParameters.filterOn = 'PAYMENT_STATUS';
          break;
        case 10:
          dataTablesParameters.filterOn = 'PAYMENT_TYPE';
          break;
        case 11:
          dataTablesParameters.filterOn = 'INVOICE_NO';
          break;
        case 12:
          dataTablesParameters.filterOn = 'STATUS';
          break;
        case 13:
          dataTablesParameters.filterOn = 'DRIVER_FULLNAME';
          break;
        default:
          dataTablesParameters.filterType = null;
          dataTablesParameters.filterOn = null;
          break;
      }
    });
  }

  async getFilterOptions() {
    this.filterOption = new OrderFilterOption();
    this.orderStatuses = Array<OrderStatus>();
    this.districts = Array<District>();
    this.subDistricts = Array<SubDistrict>();
    this.customerFilterOptions = Array<CustomerFilterOption>();
    this.getDrivers();
    await this.getDistricts();
    this.orderStatuses = await this.orderManager.getAllOrderStatuses();
    this.customerFilterOptions = await this.orderManager.getCustomerFilterOptions();
    
  }

  async getDistricts() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.districtManager.getDistricts();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.districts = response.data;
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  async getSubDistricts(districtId) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.subDistrictManager.getSubDistricts(districtId);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.subDistricts = response.data;
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  async getOrderStats() {
    try {
      this.hasDataLoad = false;
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.getOrderStats();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.orderStats = response.data;
      this.hasDataLoad = true;
    } catch (error) {
      this.loadingService.hide();
      this.hasDataLoad = true;
      this.toastService.error(error.message);
    }
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#orderDetailPage').appendTo('body').modal('show');
      $('#orderDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }

  async loadDriverLocationMap() {
    this.showLocationModal = true;
    await this.getDriverLocations();
    if (this.driverLocations.length === 0) {
      return;
    }
    setTimeout(() => {
      $('#driverLocationMap').appendTo('body').modal('show');
      this.loadMap();
      $('#driverLocationMap').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showLocationModal = false;
        });
      });
    }, 500);
  }

  loadMap() {
    const map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: new google.maps.LatLng(this.driverLocations[0].latitude, this.driverLocations[0].longitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: true,
      scaleControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      fullscreenControl: false
    });
    const infowindow = new google.maps.InfoWindow();
    const bound = new google.maps.LatLngBounds();
    this.driverLocations.forEach((location, index) => {
      bound.extend(new google.maps.LatLng(location.latitude, location.longitude));
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.latitude, location.longitude),
        map
      });
      google.maps.event.addListener(marker, 'click', ((marker, i) => {
        return () => {
          infowindow.setContent(location.name);
          infowindow.open(map, marker);
        };
      })(marker, index));
    });
    map.center = bound.getCenter();
    new google.maps.event.trigger(map, 'resize');
  }

  async changeDriver(recordId) {
    await this.getDrivers();
    this.showDriverAssignModal = true;
    this.selectedOrder = this.records.find(x => x.id === recordId);
    /**
     * AUTO SELECT ASSIGNED DRIVER
     */
    if (!isNullOrUndefined(this.selectedOrder.driverId) && this.drivers.length > 0) {
      this.selectedDriverId = this.selectedOrder.driverId;
      this.selectedDriver = this.drivers.find(x => x.id === this.selectedDriverId);
      if (!isNullOrUndefined(this.selectedDriver)) {
        this.selectedOrderDriver = lodash.cloneDeep(this.selectedDriver);
      } else {
        this.selectedDriverId = undefined;
      }
    } else {
      this.selectedOrderDriver = undefined;
      this.selectedDriver = undefined;
      this.selectedDriverId = undefined;
    }

    setTimeout(() => {
      $('#assignDriver').appendTo('body').modal('show');
      $('#assignDriver').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showDriverAssignModal = false;
        });
      });
    }, 500);
  }

  async getDriverLocations() {
    try {
      const driverList = new Array<string>();
      this.records.forEach((order) => {
        if (order.status === 'ASSIGNED_DRIVER' || order.status === 'AWAITING_COLLECTION' ||
          order.status === 'ASSIGNED_DRIVER_FOR_DELIVERY' || order.status === 'ON_THE_WAY') {
          if (driverList.indexOf(order.driverId) === -1) {
            driverList.push(order.driverId);
          }
        }
      });
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.getDriverLocations({'drivers': driverList});
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error('Unable to fetch drivers locations');
        return;
      }
      this.driverLocations = response.data;
      if (this.driverLocations.length <= 0) {
        this.toastService.info('Driver Location is not available yet');
      }
    } catch (error) {
      this.toastService.error('Unable to fetch drivers locations');
      this.loadingService.hide();
      return;
    }
  }

  async getDrivers() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.getDrivers();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error('Unable to fetch drivers');
        return;
      }
      this.drivers = response.data;
    } catch (error) {
      this.toastService.error('Unable to fetch drivers');
      this.loadingService.hide();
    }
  }

  selectDriver() {
    if (this.drivers.length === 0) {
      this.toastService.error('No drivers found');
      return;
    }
    this.selectedDriver = this.drivers.find(x => x.id === this.selectedDriverId);
    this.selectedOrder.driverId = this.selectedDriver.id;
    this.selectedOrder.driverName = this.selectedDriver.fullName;
  }

  async assignDriver() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.assignDriver(this.selectedOrder);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      $('#assignDriver').modal('hide');
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  cancel() {
    if (isNullOrUndefined(this.selectedOrderDriver)) {
      this.selectedOrder.driverName = undefined;
      this.selectedOrder.driverId = undefined;
      this.selectedDriver = undefined;
      this.selectedDriverId = undefined;
      $('#assignDriver').modal('hide');
      return;
    }
    this.selectedOrder.driverName = this.selectedOrderDriver.fullName;
    this.selectedOrder.driverId = this.selectedOrderDriver.id;
    $('#assignDriver').modal('hide');
  }

  cancelOrder(record) {
    this.commonService.confirmation(`Would you like to cancel order #${record.orderId}?`, this.cancelOrderCallback.bind(this), record.id);
  }

  async cancelOrderCallback(recordId) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.cancelOrder(recordId);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  editOrder(record) {
    this.commonService.customConfirmation(`Would you like to edit order #${record.orderId}?`, this.editOrderCallback.bind(this), null, record.id);
  }

  async editOrderCallback(recordId) {
    this.router.navigate(['/dashboard/order/detail/' + recordId], {queryParams: {editMode: true}});
  }

  viewOrder(recordId) {
    this.router.navigate(['/dashboard/order/detail/' + recordId], {queryParams: {editMode: false}});
  }

  async selectFilter(filterType: string) {
    this.filterOption.relationTable = null;
    this.filterOption.deliveryType = null;
    if (filterType === 'DISTRICT') {
      if (!isNullOrUndefined(this.filterOption.districtId)) {
        await this.getSubDistricts(this.filterOption.districtId);
        $('#customerSearchButton').trigger('click');
        return;
      }
      this.filterOption.subDistrictId = null;
    }
    if (filterType === 'CUSTOMER' && !isNullOrUndefined(this.filterOption.customerFilterType)) {
      return;
    }
    $('#customerSearchButton').trigger('click');
  }

  async checkforFilters(dataTablesParameters) {
    if (!isNullOrUndefined(this.filterOption)) {
      dataTablesParameters.district = this.filterOption.districtId;
      dataTablesParameters.subdistrict = this.filterOption.subDistrictId;
      dataTablesParameters.status = this.filterOption.status;
      dataTablesParameters.driver = this.filterOption.driverId;
      dataTablesParameters.relationTable = this.filterOption.relationTable;
      dataTablesParameters.deliveryType = this.filterOption.deliveryType;
      dataTablesParameters.orderId = null;
      dataTablesParameters.name = null;
      dataTablesParameters.email = null;
      dataTablesParameters.phoneNumber = null;
      if (!isNullOrUndefined(this.filterOption.customerFilterType)) {
        switch (this.filterOption.customerFilterType) {
          case 'ORDER_ID':
            dataTablesParameters.orderId = !isNullOrUndefined(this.searchTerm) ? this.searchTerm.trim() : this.searchTerm;
            break;
          case 'CUSTOMER_NAME':
            dataTablesParameters.name = !isNullOrUndefined(this.searchTerm) ? this.searchTerm.trim() : this.searchTerm;
            break;
          case 'EMAIL':
            dataTablesParameters.email = !isNullOrUndefined(this.searchTerm) ? this.searchTerm.trim() : this.searchTerm;
            break;
          case 'CUSTOMER_PHONE_NUMBER':
            dataTablesParameters.phoneNumber = !isNullOrUndefined(this.searchTerm) ? this.searchTerm.trim() : this.searchTerm;
            break;
        }
      }
    }
    return dataTablesParameters;
  }

  viewDetailFilter(relationTable: string, deliveryType?: string, status?: string) {
    this.filterOption.relationTable = null;
    this.filterOption.deliveryType = null;
    this.filterOption.status = null;
    this.filterOption.customerFilterType = null;
    this.filterOption.districtId = null;
    this.filterOption.subDistrictId = null;
    this.filterOption.driverId = null;
    this.searchTerm = null;
    this.filterOption.relationTable = relationTable;
    this.filterOption.deliveryType = !isNullOrUndefined(deliveryType) ? deliveryType : null;
    this.filterOption.status = !isNullOrUndefined(status) ? status : null;
    $('#customerSearchButton').trigger('click');
  }

  exportExcel() {
    const element = document.getElementById('orderTable');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, `${new Date().getDate()}-${new Date().getMonth() + 1}-${new Date().getFullYear()}`);
    XLSX.writeFile(wb, 'Orders' + '.xlsx');
  }
}
