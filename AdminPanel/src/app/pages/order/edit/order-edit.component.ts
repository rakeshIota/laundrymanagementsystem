import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IMyDpOptions} from 'mydatepicker';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';

import {BaseEditComponent} from '../../../config/base.edit.component';
import {BaseModel} from '../../../config/base.model';

import {Order} from '../../../models/order';
import {LoadingService} from '../../../services/loading.service';

import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {OrderManager} from '../order.manager';

import {Constant} from '../../../config/constants';

declare const $: any;

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})

export class OrderEditComponent extends BaseEditComponent implements OnInit {
  public myDatePickerOptions: IMyDpOptions;
  public order: Order;

  constructor(protected route: ActivatedRoute, protected orderManager: OrderManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService
    , public commonUtil: CommonUtil) {
    super(orderManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.order = new Order();
    this.order.isActive = true;
    this.setRecord(this.order);

    this.myDatePickerOptions = {
      dateFormat: Constant.MY_DATE_PICKER.DATE_TYPE
    };

    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.order = Order.fromResponse(this.record);
    this.setRecord(this.order);
  }


  async fetchAssociatedData() {
    this.afterFetchAssociatedCompleted();
  }

  afterFetchAssociatedCompleted() {
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/order');
  }


  checkConditionToReload(records: BaseModel[], selectedRecord: any) {
    if (!records.some(x => x.id === selectedRecord.id)) {
      this.fetchAssociatedData();
    }
  }

  onAssociatedValueSelected(selectedRecord: any, selectedField: any) {
    if (this.request.popupId) {
      $('#' + this.request.popupId).appendTo('body').modal('hide');
    }

  }
}
