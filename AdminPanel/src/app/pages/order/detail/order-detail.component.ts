import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as lodash from 'lodash';
import * as moment from 'moment';
import { IMyDateModel, IMyOptions } from 'mydatepicker';
import { OrderItem } from 'src/app/models/orderitem';
import { Product } from 'src/app/models/product';
import { Service } from 'src/app/models/service';
import { RestResponse } from 'src/app/shared/auth.model';
import { isNullOrUndefined } from 'util';
import { BaseDetailComponent } from '../../../config/base.detail.component';
import { Order, OrderStatus } from '../../../models/order';
import { LoadingService } from '../../../services/loading.service';
import { AuthService } from '../../../shared/auth.services';
import { CommonService } from '../../../shared/common.service';
import { CommonUtil } from '../../../shared/common.util';
import { DataService } from '../../../shared/data.service';
import { ToastService } from '../../../shared/toast.service';
import { ProductService } from '../../product/product.service';
import { ServiceService } from '../../service/service.service';
import { OrderManager } from '../order.manager';

declare const $: any;
declare const google;

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent extends BaseDetailComponent implements OnInit {
  record: Order;
  editMode: boolean;
  showDriverAssignModal: boolean;
  showAdminNoteModal: boolean;
  showstatusModal: boolean;
  showDateSlotModal: boolean;
  drivers: any[];
  selectedDriver: any;
  previousDriverDetails: any;
  previousAdminNote: string;
  previousOrderStatus: string;
  orderStatuses: OrderStatus[];
  products: Product[];
  services: Service[];
  selectedProduct: Product;
  selectedService: Service;
  selectedProductId: string;
  selectedServiceId: string;
  orderItems: OrderItem[];
  selectedDriverId: string;
  showOrderItemModal: boolean;
  selectedOrderItem: OrderItem;
  selectedOrderItemClone: OrderItem;
  currentOrderItemIndex: number;
  deletedRecords: OrderItem[];
  pickupSlots: Array<any>;
  deliverySlots: Array<any>;
  request: any;
  previousPickupDate: any;
  previousDeliveryDate: any;
  currentDate: any;
  datePickerOptions: IMyOptions;
  pickerDate: IMyDateModel;
  currentDateModalType: string;
  pickupChanged: boolean;
  currentUrl: string;
  orderCompleted: boolean;
  previousOrderSlug: string;
  appSettings: any;
  isEdit: boolean= false;

  constructor(protected route: ActivatedRoute, protected orderManager: OrderManager, protected toastService: ToastService,
    protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
    public authService: AuthService, protected translateService: TranslateService, private productService: ProductService,
    private serviceService: ServiceService, private http: HttpClient, public commonUtil: CommonUtil, private dataService: DataService) {
    super(orderManager, commonService, toastService, loadingService, route, router, translateService);
    this.currentUrl = lodash.cloneDeep(this.router.url);
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.currentUrl !== val.url) {
          setTimeout(() => {
            $('#assignDriver').modal('hide');
            $('#adminNote').modal('hide');
            $('#statusModal').modal('hide');
            $('#orderItemModal').modal('hide');
            $('#dateSlotModal').modal('hide');
          }, 100);
        }
      }
    });
  }

  async ngOnInit() {
    this.editMode = JSON.parse(this.route.snapshot.queryParamMap.get('editMode'));
    this.showDriverAssignModal = false;
    this.showAdminNoteModal = false;
    this.showstatusModal = false;
    this.showOrderItemModal = false;
    this.showDateSlotModal = false;
    this.pickupChanged = false;
    this.record = new Order();
    this.drivers = Array<any>();
    this.pickupSlots = Array<any>();
    this.deliverySlots = Array<any>();
    this.selectedDriver = {};
    this.previousDriverDetails = {};
    this.request = {} as any;
    this.currentDate = {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    };
    this.isDetailPage = true;
    this.initializationDateTime();
    this.appSettings = await this.dataService.getAppSettings();
    await this.init();
    this.initializePickupDeliveryDates();
  }

  async onFetchCompleted() {
    super.onFetchCompleted();
    this.orderCompleted = false;
    if (this.record.status === 'DELIVERED' || this.record.status === 'CANCELLED') {
      this.orderCompleted = true;
      this.orderManager.generateStatusNames(this.record);
      await this.loadCustomerMap();
      setTimeout(() => {
        this.loadDriverMap();
      });
      return;
    }
    this.filterParam.relationTable = 'Order';
    this.filterParam.relationId = this.record.id;
    this.orderManager.generateStatusNames(this.record);
    this.previousPickupDate = {} as any;
    this.previousDeliveryDate = {} as any;
    this.previousPickupDate.date = lodash.cloneDeep(this.record.pickupDate);
    if (!isNullOrUndefined(this.record.pickupSlot)) {
      this.previousPickupDate.slot = lodash.cloneDeep(this.record.pickupSlot);
    }
    if (!isNullOrUndefined(this.record.deliveryDate)) {
      this.previousDeliveryDate.date = lodash.cloneDeep(this.record.deliveryDate);
    }
    if (!isNullOrUndefined(this.record.deliverySlot)) {
      this.previousDeliveryDate.slot = lodash.cloneDeep(this.record.deliverySlot);
    }
    this.orderStatuses = new Array<OrderStatus>();
    await this.getOrderStatuses();
    if (this.editMode) {
      this.orderItems = new Array<OrderItem>();
      this.products = new Array<Product>();
      this.services = new Array<Service>();
      this.selectedProduct = new Product();
      this.selectedService = new Service();
      this.checkPreviousData();
      this.orderItems = lodash.cloneDeep(this.record.orderItems);
      this.deletedRecords = new Array<OrderItem>();
      await this.fetchServices();
    }
    await this.loadCustomerMap();
    if (isNullOrUndefined(this.record.driverDetails)) {
      return;
    }
    setTimeout(() => {
      this.loadDriverMap();
    });
  }

  async getOrderStatuses() {
    switch (this.record.status) {
      case 'AWAITING_COLLECTION':
        if (this.authService.hasRole(['ROLE_ADMIN'])) {
          this.orderStatuses = await this.orderManager.getSpecificOrderStatuses(['COLLECTED']);
        }
        break;
      case 'COLLECTED':
        this.orderStatuses = await this.orderManager.getSpecificOrderStatuses(['CLEANING_IN_PROGRESS']);
        break;
      case 'CLEANING_IN_PROGRESS':
        this.orderStatuses = await this.orderManager.getSpecificOrderStatuses(['CLEANING_COMPLETED']);
        break;
      case 'ON_THE_WAY':
        if (this.authService.hasRole(['ROLE_ADMIN'])) {
          this.orderStatuses = await this.orderManager.getSpecificOrderStatuses(['DELIVERED']);
        }
        break;
    }
  }

  initializationDateTime() {
    const date = new Date();
    let day: any = date.getDate();
    const year = date.getFullYear();
    let month: any = date.getMonth() + 1;
    month = month > 9 ? month : '0' + month;
    day = day > 9 ? day : '0' + day;
    this.request.min = year + '-' + month + '-' + day;
    this.request.max = year + '-' + month + '-' + (Number(day) + 20);
    /**
     * DATE PICKER OPTIONS
     */
    this.datePickerOptions = {
      minYear: new Date().getFullYear(),
      dateFormat: 'mm/dd/yyyy',
      disableUntil: { year: new Date().getFullYear(), day: new Date().getDate() - 1, month: new Date().getMonth() + 1 }
    } as IMyOptions;
  }

  async initializePickupDeliveryDates() {
    if (this.pickupSlots.length === 0) {
      moment(this.record.pickupDate).add(1, 'day').calendar();
    }
    await this.onPickupDateChanged(this.record.pickupDate);
    this.deliverySlots = new Array<any>();
    this.deliverySlots.push({ id: 'Afternoon', slug: 'AFTERNOON' });
    this.deliverySlots.push({ id: 'Evening', slug: 'EVENING' });
    if (isNullOrUndefined(this.record.pickupSlot)) {
      this.record.pickupSlot = undefined;
    }
    if (isNullOrUndefined(this.record.deliverySlot)) {
      this.record.deliverySlot = undefined;
    }
  }

  async loadCustomerMap() {
    const map = new google.maps.Map(document.getElementById('customerMap'), {
      zoom: 15,
      center: new google.maps.LatLng(this.record.deliveryAddress.latitude, this.record.deliveryAddress.longitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      fullscreenControl: false
    });
    const infowindow = new google.maps.InfoWindow();
    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(this.record.deliveryAddress.latitude, this.record.deliveryAddress.longitude),
      map
    });
    const that = this;
    google.maps.event.addListener(marker, 'click', function (event) {
      infowindow.setContent(that.record.customerDetails.fullName);
      infowindow.open(map, this);
    }.bind(marker));
    window.addEventListener('resize', () => {
      google.maps.event.trigger(map, 'resize');
    }, false);
  }

  async loadDriverMap() {
    const map = new google.maps.Map(document.getElementById('driverMap'), {
      zoom: 15,
      center: new google.maps.LatLng(this.record.driverDetails.latitude, this.record.driverDetails.longitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      fullscreenControl: false
    });
    const infowindow = new google.maps.InfoWindow();
    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(this.record.driverDetails.latitude, this.record.driverDetails.longitude),
      map
    });
    const that = this;
    google.maps.event.addListener(marker, 'click', function (event) {
      infowindow.setContent(that.record.driverDetails.fullName);
      infowindow.open(map, this);
    }.bind(marker));
    window.addEventListener('resize', () => {
      google.maps.event.trigger(map, 'resize');
    }, false);
  }

  checkPreviousData() {
    if (!isNullOrUndefined(this.record.driverDetails) && this.record.driverDetails.id && this.drivers.length > 0) {
      this.selectedDriverId = this.record.driverDetails.id;
      this.selectedDriver = this.drivers.find(x => x.id === this.record.driverDetails.id);
      this.previousDriverDetails = lodash.cloneDeep(this.record.driverDetails);
    } else {
      this.previousDriverDetails = undefined;
      this.selectedDriver = undefined;
      this.selectedDriverId = undefined;
    }
    if (!isNullOrUndefined(this.record.adminNote)) {
      this.previousAdminNote = lodash.cloneDeep(this.record.adminNote);
    } else {
      this.previousAdminNote = undefined;
    }
    if (!isNullOrUndefined(this.record.status)) {
      this.previousOrderStatus = lodash.cloneDeep(this.record.status);
    } else {
      this.previousOrderStatus = undefined;
    }
    if (!isNullOrUndefined(this.record.statusSlug)) {
      this.previousOrderSlug = lodash.cloneDeep(this.record.statusSlug);
    } else {
      this.previousOrderSlug = undefined;
    }
  }

  selectDriver() {
    if (this.drivers.length === 0) {
      this.toastService.error('No drivers found');
      return;
    }
    this.selectedDriver = this.drivers.find(x => x.id === this.selectedDriverId);
    this.record.driverId = this.selectedDriver.id;
    this.record.driverDetails = Object.assign({}, this.selectedDriver);
  }

  async changeDriver() {
    this.showDriverAssignModal = true;
    await this.getDrivers();
    /**
     * AUTO SELECT ASSIGNED DRIVER
     */
    this.checkPreviousData();
    setTimeout(() => {
      $('#assignDriver').appendTo('body').modal('show');
      $('#assignDriver').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showDriverAssignModal = false;
        });
      });
    }, 500);
  }

  async getDrivers() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.getDrivers();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error('Unable to fetch drivers');
        return;
      }
      this.drivers = response.data;
    } catch (error) {
      this.toastService.error('Unable to fetch drivers');
      this.loadingService.hide();
    }
  }

  async assignDriver() {
    try {
      if (isNullOrUndefined(this.previousDriverDetails) && isNullOrUndefined(this.record.driverDetails) || !isNullOrUndefined(this.previousDriverDetails) && !isNullOrUndefined(this.record.driverDetails) && this.previousDriverDetails.id === this.record.driverDetails.id) {
        this.cancelDriverAssignment();
        return;
      }
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.assignDriver(this.record);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.showDriverAssignModal = false;
      $('#assignDriver').modal('hide');
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  cancelDriverAssignment() {
    if (isNullOrUndefined(this.previousDriverDetails)) {
      this.previousDriverDetails = undefined;
      this.record.driverId = undefined;
      this.record.driverDetails = undefined;
      this.selectedDriver = undefined;
      this.selectedDriverId = undefined;
      $('#assignDriver').modal('hide');
      return;
    }
    this.record.driverId = this.previousDriverDetails.id;
    this.record.driverDetails = this.previousDriverDetails;
    $('#assignDriver').modal('hide');
  }

  changeAdminNote() {
    this.showAdminNoteModal = true;
    setTimeout(() => {
      $('#adminNote').appendTo('body').modal('show');
      $('#adminNote').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showAdminNoteModal = false;
        });
      });
    }, 500);
  }

  cancelAdminNote() {
    if (!isNullOrUndefined(this.previousAdminNote)) {
      this.record.adminNote = this.previousAdminNote;
    } else {
      this.record.adminNote = undefined;
    }
  }

  async changeNote() {
    if (isNullOrUndefined(this.record.adminNote) || (!isNullOrUndefined(this.record.adminNote) && this.record.adminNote.trim() === '')) {
      this.toastService.error('Please add notes first!');
      return;
    }
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.updateOrder(this.record);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.showAdminNoteModal = false;
      $('#adminNote').modal('hide');
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  async selectStatus() {
    this.orderManager.generateStatusNames(this.record);
  }

  changeStatus() {
    this.showstatusModal = true;
    this.checkPreviousData();
    setTimeout(() => {
      $('#statusModal').appendTo('body').modal('show');
      $('#statusModal').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showstatusModal = false;
        });
      });
    }, 500);
  }

  async saveStatus() {
    try {
      if (this.previousOrderStatus === this.record.status) {
        this.cancelStatusAssignment();
        return;
      }
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.updateOrder(this.record);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      $('#statusModal').modal('hide');
      this.showstatusModal = false;
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  cancelStatusAssignment() {
    if (!isNullOrUndefined(this.previousOrderStatus)) {
      this.record.status = this.previousOrderStatus;
      this.record.statusSlug = this.previousOrderSlug;
      this.orderManager.generateStatusNames(this.record);
    } else {
      this.record.status = undefined;
      this.record.statusSlug = undefined;
    }
    $('#statusModal').modal('hide');
  }

  async fetchProducts() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.productService.productsByService(this.selectedOrderItem.serviceId).toPromise();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error('Unable to fetch products!');
        return;
      }
      this.products = response.data;
    } catch (error) {
      this.toastService.error('Unable to fetch products!');
      this.loadingService.hide();
      return;
    }
  }

  async fetchServices() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.serviceService.fetchAll(null);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.services = response.data;
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  //#region ORDER EDIT (POPUP)
  addOrderItem() {
    this.isEdit = true;
    this.showOrderItemModal = true;
    this.selectedOrderItem = new OrderItem();
    this.currentOrderItemIndex = -1;
    setTimeout(() => {
      $('#orderItemModal').appendTo('body').modal('show');
      $('#orderItemModal').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showOrderItemModal = false;
        });
      });
    }, 500);
  }

  async editOrderItem(orderItem: OrderItem, index: number) {
    this.isEdit = true;
    this.showOrderItemModal = true;
    this.currentOrderItemIndex = index;
    this.selectedOrderItemClone = lodash.cloneDeep(orderItem);
    this.selectedOrderItem = lodash.cloneDeep(orderItem);
    await this.fetchProducts();
    setTimeout(() => {
      $('#orderItemModal').appendTo('body').modal('show');
      $('#orderItemModal').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showOrderItemModal = false;
        });
      });
    }, 500);
  }

  async selectOrderItemEntity(selectedOrderItem: OrderItem, resetQuantity: boolean, entityType?: string) {
    if (!isNullOrUndefined(entityType) && entityType === 'SERVICE') {
      await this.fetchProducts();
      selectedOrderItem.productId = null;
      selectedOrderItem.product = null;
      return;
    }
    if (!isNullOrUndefined(selectedOrderItem) && !isNullOrUndefined(selectedOrderItem.serviceId) && !isNullOrUndefined(selectedOrderItem.productId)) {
      let updatedProductPrices: any = {};
      if (resetQuantity) {
        this.selectedOrderItem.quantity = 1;
      }
      updatedProductPrices = await this.getUpdatedPrices(selectedOrderItem.productId);
      this.selectedOrderItem.totalPrice = updatedProductPrices.totalPrice;
      this.selectedOrderItem.subTotal = updatedProductPrices.subTotal;
    }
  }

  async getUpdatedPrices(productId: string) {
    const updatedProductPrices = {
      totalPrice: 0,
      subTotal: 0
    };
    const selectedProduct: Product = this.products.find(x => x.id === productId);
    updatedProductPrices.totalPrice = selectedProduct.price;
    updatedProductPrices.subTotal = selectedProduct.price;
    return updatedProductPrices;
  }

  async calculateOrderItemPrice() {
    await this.selectOrderItemEntity(this.selectedOrderItem, false);
    if (this.selectedOrderItem.quantity > 0) {
      this.selectedOrderItem.totalPrice = this.selectedOrderItem.subTotal * this.selectedOrderItem.quantity;
    }
  }

  saveUpdatedOrderItem() {
    if (isNullOrUndefined(this.selectedOrderItem.productId) || isNullOrUndefined(this.selectedOrderItem.serviceId)) {
      this.toastService.error('Product and service selection is mandatory!');
      return;
    }
    if (isNullOrUndefined(this.selectedOrderItem.quantity) || (!isNullOrUndefined(this.selectedOrderItem.quantity) && this.selectedOrderItem.quantity === 0)) {
      this.toastService.error('Quantity cannot be null or zero, use delete option instead!');
      return;
    }
    this.selectedOrderItem.product = this.products.find(x => x.id === this.selectedOrderItem.productId);
    this.selectedOrderItem.service = this.services.find(x => x.id === this.selectedOrderItem.serviceId);
    if (this.currentOrderItemIndex === -1) {
      this.record.orderItems.push(this.selectedOrderItem);
    } else {
      this.record.orderItems.splice(this.currentOrderItemIndex, 1, this.selectedOrderItem);
    }
    this.reCalculateTotal(this.selectedOrderItem);
    $('#orderItemModal').modal('hide');
  }

  async removeOrderItem(orderItem: OrderItem, index: number) {
    this.isEdit = true;
    this.commonService.customConfirmation('Do you want to delete this item?', this.removeOrderItemCallback.bind(this), null, orderItem, index);
  }

  async removeOrderItemCallback(orderItem: OrderItem, index: number) {
    if (this.record.orderItems.length === 1) {
      this.toastService.error('Cart must have at least one item!');
      return;
    }
    const itemToDelete: OrderItem = this.record.orderItems.find(x => x.id === orderItem.id);
    itemToDelete.isDeleted = true;
    this.deletedRecords.push(itemToDelete);
    await this.reCalculateTotal(orderItem);
    this.record.orderItems.splice(index, 1);
  }


  async reCalculateTotal(orderItem: OrderItem) {
    // this.record.subTotal = this.record.subTotal - orderItem.totalPrice;
    // this.record.totalPrice = this.record.subTotal + this.record.tax + this.record.deliveryPrice + this.record.logisticCharge;
    this.record.subTotal = this.getSubtotal();
    this.record.deliveryPrice = this.getDeliveryFees();
    this.record.logisticCharge = this.getLogisticCharge();
    this.record.tax = this.getTax();
    this.record.totalPrice = this.getSubtotal() + this.getTax() + this.getDeliveryFees() + this.getLogisticCharge();
  }

  getDeliveryFees() {
    const sameDayRate = this.appSettings.samedayDeliveryRate;
    const expressRate = this.appSettings.expressDeliveryRate;
    const normalRate = this.appSettings.normalDeliveryRate;
    if (this.record.deliveryType.toLowerCase() === 'sameday') {
      return this.getSubtotal() * Number(sameDayRate);
    }
    if (this.record.deliveryType.toLowerCase() === 'express') {
      return this.getSubtotal() * Number(expressRate);
    }
    if (this.record.deliveryType.toLowerCase() === 'normal') {
      return this.getSubtotal() * Number(normalRate);
    }
    return 0.00;
  }

  getSubtotal() {
    let total = 0.00;
    this.record.orderItems.forEach((item) => {
      if (!item.isDeleted) {
        total += item.quantity * item.subTotal;
      }
    });
    return total;
  }

  getTotal() {
    return this.getSubtotal() + this.getDeliveryFees() + this.getTax();
  }

  getTax() {
    return this.getSubtotal() * 0.0;
  }

  getLogisticCharge() {
    const total = this.getSubtotal() + this.getDeliveryFees() + this.getTax();
    if (Number(total) < Number(this.appSettings.minimumOrderAmount)) {
      return Number(this.appSettings.logisticCharge);
    }
    return 0.0;
  }

  cancelOrderItemEdit() {
    if (!isNullOrUndefined(this.selectedOrderItemClone)) {
      this.selectedOrderItem = lodash.cloneDeep(this.selectedOrderItemClone);
    }
  }

  saveOrderItems() {
    this.commonService.customConfirmation('Do you want to save these changes?', this.saveOrderItemsCallback.bind(this), 'Do you want to save the change? By clicking OK, the new order confirmation will be sent to the customer.');
  }

  async saveOrderItemsCallback() {
    try {
      this.loadingService.show();
      const recordObj: Order = lodash.cloneDeep(this.record);
      this.addDeletedItems(recordObj);
      const response: RestResponse = await this.orderManager.updateOrderItems(recordObj);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.isEdit = false;
      this.toastService.success(response.message);
      this.router.navigate(['dashboard/order']);
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  addDeletedItems(recordObj: Order) {
    this.deletedRecords.forEach(x => {
      recordObj.orderItems.push(x);
    });
  }

  //#endregion

  async changeDate(modalType: string) {
    this.currentDateModalType = modalType;
    this.showDateSlotModal = true;
    await this.autoSelectDate(modalType);
    if (this.pickupSlots.length === 0) {
      moment(this.record.pickupDate).add(1, 'day').calendar();
    }
    await this.onPickupDateChanged(this.record.pickupDate);
    setTimeout(() => {
      $('#dateSlotModal').appendTo('body').modal('show');
      $('#dateSlotModal').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showDateSlotModal = false;
        });
      });
    }, 500);
  }

  async autoSelectDate(modalType: string) {
    let day: number = new Date().getDate();
    let month: number = new Date().getMonth() + 1;
    let year: number = new Date().getFullYear();
    switch (modalType) {
      case 'PICKUP':
        if (!isNullOrUndefined(this.record.pickupDate)) {
          day = Number(moment(this.record.pickupDate).format('DD'));
          year = Number(moment(this.record.pickupDate).format('YYYY'));
          month = Number(moment(this.record.pickupDate).format('MM'));
          this.pickerDate = {} as IMyDateModel;
          this.pickerDate.date = { day, month, year };
        }
        break;
      case 'DELIVERY':
        if (!isNullOrUndefined(this.record.deliveryDate)) {
          day = Number(moment(this.record.deliveryDate).format('DD'));
          year = Number(moment(this.record.deliveryDate).format('YYYY'));
          month = Number(moment(this.record.deliveryDate).format('MM'));
          this.pickerDate = {} as IMyDateModel;
          this.pickerDate.date = { day, month, year };
        }
        break;
    }
  }

  async onPickupDateChanged(pickupDate: Date, event?) {
    if (isNullOrUndefined(pickupDate) && !isNullOrUndefined(event)) {
      pickupDate = event.jsdate;
    }
    let day: any = moment(pickupDate).format('DD');
    const year = moment(pickupDate).format('YYYY');
    let month: any = Number(moment(pickupDate).format('MM'));
    month = month > 9 ? month : '0' + month;
    if (this.record.deliveryType === 'NORMAL') {
      day = Number(day) + 2;
    } else if (this.record.deliveryType === 'EXPRESS') {
      day = Number(day) + 1;
    }
    day = day > 9 ? day : '0' + day;
    this.datePickerOptions.disableUntil = { year: Number(year), month: Number(month), day: Number(day) };
    if (this.currentDateModalType === 'PICKUP') {
      this.datePickerOptions.disableUntil = { year: this.currentDate.year, month: this.currentDate.month, day: this.currentDate.day - 1 };
    }
    this.generatePickupSlots(pickupDate);
    /*if (this.pickupSlots.length > 0) {
      this.record.pickupSlot = this.pickupSlots[0].id;
    }*/
  }

  generatePickupSlots(date: Date) {
    const tMonth = Number(moment(date).format('MM'));
    const month = tMonth <= 9 ? '0' + tMonth : tMonth;
    const tDay = Number(moment(date).format('DD'));
    const day = tDay <= 9 ? '0' + tDay : tDay;
    const startTime = '09:00:00 AM';
    let endTime = '06:59:00 PM';
    if (this.record.deliveryType === 'NORMAL') {
      endTime = '06:59:00 PM';
    } else if (this.record.deliveryType === 'EXPRESS') {
      endTime = '04:00:00 PM';
    } else if (this.record.deliveryType === 'SAMEDAY') {
      endTime = '02:00:00 PM';
    }
    const startDate = `${moment(date).format('YYYY')} - ${month} - ${day} ${startTime}`;
    const endDate = `${moment(date).format('YYYY')} - ${month} - ${day} ${endTime}`;
    this.pickupSlots = this.intervals(startDate, endDate);
  }

  intervals(startString, endString) {
    const start = moment(startString, 'YYYY-MM-DD hh:mm a');
    const end = moment(endString, 'YYYY-MM-DD hh:mm a');
    start.minutes(Math.ceil(start.minutes() / 120) * 120);
    const result = [];
    const current = moment(start);
    while (current <= end) {
      const iFrom = current.format('hh:mm A');
      const hasPassedTime = current.format('YYYY-MM-DD HH:mm') > moment().format('YYYY-MM-DD HH:mm');
      current.add(120, 'minutes');
      const iEnd = current.format('hh:mm A');
      if (hasPassedTime) {
        result.push({ slug: `${iFrom} - ${iEnd}`, id: `${iFrom} - ${iEnd}` });
      }
    }
    return result;
  }

  onPickupSlotSelection(slot) {
    this.record.pickupSlot = slot.id;
  }

  cancelSlotChange() {
    switch (this.currentDateModalType) {
      case 'PICKUP':
        if (!isNullOrUndefined(this.previousPickupDate)) {
          this.record.pickupDate = this.previousPickupDate.date;
          this.record.pickupSlot = this.previousPickupDate.slot;
        }
        break;
      case 'DELIVERY':
        if (!isNullOrUndefined(this.previousDeliveryDate) && !this.pickupChanged) {
          this.record.deliveryDate = this.previousDeliveryDate.date;
          this.record.deliverySlot = this.previousDeliveryDate.slot;
        }
        break;
    }
    this.pickerDate = undefined;
  }

  onDeliverySelection(slot) {
    this.record.deliverySlot = slot.id;
  }

  async updateDate() {
    try {
      if (!await this.updateData()) {
        return;
      }
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.updateOrder(this.record);
      $('#dateSlotModal').modal('hide');
      this.loadingService.hide();
      if (!response.status) {
        this.cancelSlotChange();
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.onUpdateCompleted();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
      $('#dateSlotModal').modal('hide');
    }
  }

  async updateData() {
    switch (this.currentDateModalType) {
      case 'PICKUP':
        if (isNullOrUndefined(this.pickerDate)) {
          this.toastService.error('Please select a date first!');
          return false;
        }
        if (!isNullOrUndefined(this.pickerDate) && isNullOrUndefined(this.record.pickupSlot)) {
          this.toastService.error('Please select pickup slot!');
          return false;
        }
        const date = new Date(this.pickerDate.jsdate);
        date.setHours(12);
        date.setMinutes(0);
        this.record.pickupDate = date;
        break;
      case 'DELIVERY':
        if (isNullOrUndefined(this.pickerDate)) {
          this.toastService.error('Please select a date first!');
          return false;
        }
        if (!isNullOrUndefined(this.pickerDate) && isNullOrUndefined(this.record.deliverySlot)) {
          this.toastService.error('Please select delivery slot!');
          return false;
        }
        const date1 = new Date(this.pickerDate.jsdate);
        date1.setHours(12);
        date1.setMinutes(0);
        this.record.deliveryDate = date1;
        break;
    }
    return true;
  }

  onUpdateCompleted() {
    if (this.currentDateModalType === 'PICKUP') {
      this.onPickupDateChanged(this.record.pickupDate);
      this.pickupChanged = true;
    }
  }

  async printPdf(orderId: string) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.orderManager.printPdf(orderId);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.downloadPdf(response.data);
      this.toastService.success(response.message);
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  downloadPdf(orderBase64: string) {
    const source = 'data:application/pdf;base64,' + ` ${orderBase64}`;
    const downloadLink = document.createElement('a');
    const fileName = `#${this.record.orderId}.pdf`;
    downloadLink.href = source;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}


