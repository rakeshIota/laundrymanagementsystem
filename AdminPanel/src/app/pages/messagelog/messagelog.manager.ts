import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { MessageLogService } from './messagelog.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class MessageLogManager extends BaseManager {

    constructor(private messageLogService: MessageLogService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(messageLogService, loadingService, toastService);
    }
}
