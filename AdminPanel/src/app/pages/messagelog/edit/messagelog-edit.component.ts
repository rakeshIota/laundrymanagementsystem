import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { MessageLog } from '../../../models/messagelog';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { MessageLogManager } from '../messagelog.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-messagelog-edit',
  templateUrl: './messagelog-edit.component.html',
  styleUrls: ['./messagelog-edit.component.scss']
})

export class MessageLogEditComponent extends BaseEditComponent implements OnInit {
  public messageLog: MessageLog;
  
  constructor(protected route: ActivatedRoute, protected messagelogManager: MessageLogManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(messagelogManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.messageLog = new MessageLog();
  	this.messageLog.isActive=true;   
    this.setRecord(this.messageLog);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.messageLog = MessageLog.fromResponse(this.record);
    this.setRecord(this.messageLog);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/message-log');
  }	
	  
	
}
