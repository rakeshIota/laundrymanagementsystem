import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/shared/auth.services';
import { Users } from '../../models/users';
import { AccountService } from '../../services/account.service';
import { LoadingService } from '../../services/loading.service';
import { RestResponse } from '../../shared/auth.model';
import { CommonUtil } from '../../shared/common.util';
import { ToastService } from '../../shared/toast.service';

declare const $: any;

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {

  user: Users;
  onClickValidation: boolean;

  constructor(private toastService: ToastService, private accountService: AccountService, private loadingService: LoadingService,
    private localStorageService: LocalStorageService, private authService: AuthService, private usersService: UsersService,
    public commonUtil: CommonUtil, private translateService: TranslateService) {
    this.user = new Users();
  }

  ngOnInit() {
    this.loadingService.show();

    this.usersService.fetchUser(this.authService.getUser().id)
      .subscribe((response) => {
        this.loadingService.hide();
        if (!response.status) {
          this.toastService.error(response.message);
          return;
        }
        this.user = Users.fromResponse(response.data);
        this.user.userName = this.user.email;
      }, (error: RestResponse) => {
        this.loadingService.hide();
        this.toastService.error(error.message);
      });
  }

  update(form) {
    this.onClickValidation = !form.valid;
    if (!form.valid) {
      return false;
    }
    if (!this.user.isValidAccountSettingRequest(form)) {
      return;
    }
    this.loadingService.show();
    this.accountService.update(this.user.forRequest())
      .then((response) => {
        this.loadingService.hide();
        if (!response.status) {
          this.toastService.error(response.message);
          return;
        }
        const user = this.localStorageService.get('user') as Users;
        user.firstName = this.user.firstName;
        user.lastName = this.user.lastName;
        user.phoneNumber = this.user.phoneNumber;
        user.fullName = this.user.firstName + ' ' + this.user.lastName;
        this.localStorageService.set('user', user);
        this.toastService.success(response.message);
      }, (error: RestResponse) => {
        this.loadingService.hide();
        this.toastService.error(error.message);
      });
  }

  changeLanguage(lanuage: string) {
    this.translateService.setDefaultLang(lanuage);
  }
}
