import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {BaseEditComponent} from '../../../config/base.edit.component';
import {State} from '../../../models/state';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {StateManager} from '../state.manager';
import {CountryManager} from '../../country/country.manager';
import {Country} from '../../../models/country';
import {StateLanguageMapping} from '../../../models/statelanguagemapping';
import {Language} from '../../../models/language';
import {LanguageManager} from '../../language/language.manager';

declare const $: any;

@Component({
  selector: 'app-state-edit',
  templateUrl: './state-edit.component.html',
  styleUrls: ['./state-edit.component.scss']
})

export class StateEditComponent extends BaseEditComponent implements OnInit {
  state: State;
  countries: Country[];
  stateLanguage: StateLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected stateManager: StateManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              private countryManager: CountryManager, public commonUtil: CommonUtil, private languageManager: LanguageManager) {
    super(stateManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.state = new State();
    this.setRecord(this.state);
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.countries = new Array<Country>();
    this.onClickMappingValidation = false;
    this.stateLanguage = new StateLanguageMapping();
    this.init();
  }

  onFetchCompleted() {
    this.state = State.fromResponse(this.record);
    this.setRecord(this.state);
  }


  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.countries = await this.countryManager.fetchAllData(null);
    this.stateLanguage.languageId = this.languages[0].id;
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/state');
  }

  addStateLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.state.stateLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.stateLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('State Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.stateLanguage.languageId;
    });
    this.stateLanguage.languageName = language[0].name;
    this.state.stateLanguages.push(Object.assign({}, this.stateLanguage));
    this.stateLanguage = new StateLanguageMapping();
    this.stateLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: StateLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: StateLanguageMapping) {
    record.isDeleted = true;
  }
}
