import {Component, OnDestroy, OnInit, Input, Output, ViewChild} from '@angular/core';
import {BaseListComponent} from '../../config/base.list.component';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../shared/auth.services';
import {CommonService} from '../../shared/common.service';
import {ToastService} from '../../shared/toast.service';
import {StateManager} from './state.manager';
import {State} from '../../models/state';
import {isNullOrUndefined} from 'util';
import {Router} from '@angular/router';
import {CommonUtil} from '../../shared/common.util';

declare const $: any;

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})
export class StateComponent extends BaseListComponent implements OnInit, OnDestroy {

  constructor(protected stateManager: StateManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService,
              protected router: Router, public commonUtil: CommonUtil) {
    super(stateManager, commonService, toastService, loadingService, router);
  }

  ngOnInit() {
    this.request.loadEditPage = false;
    this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
    this.records = new Array<State>();
    this.init();
  }

  onItemSelection(record: any) {
    this.onAssociatedValueSelected(record);
  }

  onCancel() {
    this.request.loadEditPage = false;
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  onNewRecord() {
    if (!this.isPlusButton) {
      if (this.filterParam) {
        this.router.navigate(['/dashboard/state/edit/0'], {queryParams: {[this.filterParam.relationTable]: this.filterParam.relationId}});
      } else {
        this.router.navigate(['/dashboard/state/edit/0']);
      }
      return;
    }
    this.request.loadEditPage = true;
  }

  removeSuccess() {
    this.onCancel();
  }


  ngOnDestroy() {
    this.clean();
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#stateDetailPage').appendTo('body').modal('show');
      $('#stateDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }
}
