import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { StateLanguageMappingService } from './statelanguagemapping.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class StateLanguageMappingManager extends BaseManager {

    constructor(private stateLanguageMappingService: StateLanguageMappingService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(stateLanguageMappingService, loadingService, toastService);
    }
}
