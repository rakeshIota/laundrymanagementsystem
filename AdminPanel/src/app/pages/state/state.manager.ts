import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { StateService } from './state.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class StateManager extends BaseManager {

    constructor(private stateService: StateService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(stateService, loadingService, toastService);
    }
}
