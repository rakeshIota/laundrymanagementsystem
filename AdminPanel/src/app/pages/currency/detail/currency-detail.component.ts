import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { Currency } from '../../../models/currency';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyManager } from '../currency.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-currency-detail',
  templateUrl: './currency-detail.component.html',
  styleUrls: ['./currency-detail.component.scss']
})
export class CurrencyDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected currencyManager: CurrencyManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(currencyManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new Currency();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "Currency";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
