import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { Currency } from '../../../models/currency';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { CurrencyManager } from '../currency.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-currency-edit',
  templateUrl: './currency-edit.component.html',
  styleUrls: ['./currency-edit.component.scss']
})

export class CurrencyEditComponent extends BaseEditComponent implements OnInit {
  public currency: Currency;
  
  constructor(protected route: ActivatedRoute, protected currencyManager: CurrencyManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(currencyManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.currency = new Currency();
  	this.currency.isActive=true;   
    this.setRecord(this.currency);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.currency = Currency.fromResponse(this.record);
    this.setRecord(this.currency);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/currency');
  }	
	  
	
}
