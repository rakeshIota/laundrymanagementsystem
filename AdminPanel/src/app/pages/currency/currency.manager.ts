import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { CurrencyService } from './currency.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class CurrencyManager extends BaseManager {

    constructor(private currencyService: CurrencyService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(currencyService, loadingService, toastService);
    }
}
