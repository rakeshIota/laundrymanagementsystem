import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastService} from '../../shared/toast.service';
import {isNullOrUndefined} from 'util';
import {LoadingService} from '../../services/loading.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  data: any;
  onClickValidation: boolean;

  constructor(private loginService: LoginService, private router: Router, private toastService: ToastService,
              private loadingService: LoadingService, private route: ActivatedRoute) {
    this.onClickValidation = false;
    this.data = {} as any;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.data.code = encodeURIComponent(params.code);
      this.data.uniqueCode = this.route.snapshot.params.ucode;
      if (isNullOrUndefined(this.data.code) || this.data.code === '') {
        this.router.navigate(['404']);
      }
    });
  }

  resetPassword(bol) {
    this.onClickValidation = !bol;
    if (!bol) {
      return false;
    }
    if (this.data.confirmPassword !== this.data.password) {
      this.toastService.error('Password doesn\'t matches');
      return;
    }
    this.loadingService.show();
    this.loginService.recoverPassword(this.data)
      .then((data) => {
        this.loadingService.hide();
        if (!data.status) {
          this.toastService.error(data.message);
          return;
        }
        this.toastService.success(data.message);
        this.router.navigate(['/']);
      }, (error) => {
        this.loadingService.hide();
        this.toastService.error(error.message);
      });
  }
}
