import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { UserCordinateService } from './usercordinate.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class UserCordinateManager extends BaseManager {

    constructor(private userCordinateService: UserCordinateService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(userCordinateService, loadingService, toastService);
    }
}
