import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { UserCordinate } from '../../../models/usercordinate';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCordinateManager } from '../usercordinate.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-usercordinate-detail',
  templateUrl: './usercordinate-detail.component.html',
  styleUrls: ['./usercordinate-detail.component.scss']
})
export class UserCordinateDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected userCordinateManager: UserCordinateManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(userCordinateManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new UserCordinate();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "UserCordinate";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
