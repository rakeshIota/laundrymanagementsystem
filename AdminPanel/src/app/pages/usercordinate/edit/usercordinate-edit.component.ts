import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { UserCordinate } from '../../../models/usercordinate';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { UserCordinateManager } from '../usercordinate.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-usercordinate-edit',
  templateUrl: './usercordinate-edit.component.html',
  styleUrls: ['./usercordinate-edit.component.scss']
})

export class UserCordinateEditComponent extends BaseEditComponent implements OnInit {
  public userCordinate: UserCordinate;
  
  constructor(protected route: ActivatedRoute, protected usercordinateManager: UserCordinateManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(usercordinateManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.userCordinate = new UserCordinate();
  	this.userCordinate.isActive=true;   
    this.setRecord(this.userCordinate);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.userCordinate = UserCordinate.fromResponse(this.record);
    this.setRecord(this.userCordinate);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/user-cordinate');
  }	
	  
	
}
