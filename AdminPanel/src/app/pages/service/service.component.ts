import {Component, OnDestroy, OnInit, Input, Output, ViewChild} from '@angular/core';
import {BaseListComponent} from '../../config/base.list.component';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../shared/auth.services';
import {CommonService} from '../../shared/common.service';
import {ToastService} from '../../shared/toast.service';
import {ServiceManager} from './service.manager';
import {Service} from '../../models/service';
import {isNullOrUndefined} from 'util';
import {Router} from '@angular/router';
import {CommonUtil} from '../../shared/common.util';
import {CommonEventService} from '../../shared/common.event.service';

declare const $: any;

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent extends BaseListComponent implements OnInit, OnDestroy {

  constructor(protected serviceManager: ServiceManager, protected toastService: ToastService, private commonEventService: CommonEventService,
              protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService,
              protected router: Router, public commonUtil: CommonUtil) {
    super(serviceManager, commonService, toastService, loadingService, router);
  }

  ngOnInit() {
    this.request.loadEditPage = false;
    this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
    this.records = new Array<Service>();
    this.init();
  }

  onItemSelection(record: any) {
    this.onAssociatedValueSelected(record);
  }

  onCancel() {
    this.request.loadEditPage = false;
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  onNewRecord() {
    if (!this.isPlusButton) {
      if (this.filterParam) {
        this.router.navigate(['/dashboard/service/edit/0'], {queryParams: {[this.filterParam.relationTable]: this.filterParam.relationId}});
      } else {
        this.router.navigate(['/dashboard/service/edit/0']);
      }
      return;
    }
    this.request.loadEditPage = true;
  }

  removeSuccess() {
    this.onCancel();
  }


  ngOnDestroy() {
    this.clean();
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#serviceDetailPage').appendTo('body').modal('show');
      $('#serviceDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }

  openFiles(imageUrl: string) {
    this.commonEventService.showImageOnClick(imageUrl);
  }
}
