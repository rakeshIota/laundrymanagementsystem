import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { ServiceService } from './service.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class ServiceManager extends BaseManager {

    constructor(private serviceService: ServiceService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(serviceService, loadingService, toastService);
    }
}
