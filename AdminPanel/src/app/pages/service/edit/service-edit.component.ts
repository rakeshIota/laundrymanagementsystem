import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {BaseEditComponent} from '../../../config/base.edit.component';
import {Service} from '../../../models/service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {ServiceManager} from '../service.manager';
import {FileUploader} from 'ng2-file-upload';
import {Language} from '../../../models/language';
import {ServiceLanguageMapping} from '../../../models/servicelanguagemapping';
import {LanguageManager} from '../../language/language.manager';
import {CityLanguageMapping} from '../../../models/citylanguagemapping';

declare const $: any;

@Component({
  selector: 'app-service-edit',
  templateUrl: './service-edit.component.html',
  styleUrls: ['./service-edit.component.scss']
})

export class ServiceEditComponent extends BaseEditComponent implements OnInit {
  service: Service;
  serviceImageUploader: FileUploader;
  serviceLanguage: ServiceLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected serviceManager: ServiceManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              public commonUtil: CommonUtil, private languageManager: LanguageManager) {
    super(serviceManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.service = new Service();
    this.service.isActive = true;
    this.setRecord(this.service);
    this.onClickMappingValidation = false;
    this.serviceLanguage = new ServiceLanguageMapping();
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.service = Service.fromResponse(this.record);
    this.setRecord(this.service);
    this.pluginIntialization();
  }

  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.serviceLanguage.languageId = this.languages[0].id;
  }

  pluginIntialization() {
    this.serviceImageUploader = this.initializeUploader(this.service.image, '.jpg,.jpeg,.png', 102400, 1, this.toastService);
  }

  afterFetchAssociatedCompleted() {
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/service');
  }

  addLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.service.serviceLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.serviceLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('Language Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.serviceLanguage.languageId;
    });
    this.serviceLanguage.languageName = language[0].name;
    this.service.serviceLanguages.push(Object.assign({}, this.serviceLanguage));
    this.serviceLanguage = new ServiceLanguageMapping();
    this.serviceLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: ServiceLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: ServiceLanguageMapping) {
    record.isDeleted = true;
  }
}
