import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {Service} from '../../../models/service';
import {ActivatedRoute, Router} from '@angular/router';
import {ServiceManager} from '../service.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';
import {CommonEventService} from '../../../shared/common.event.service';

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.scss']
})
export class ServiceDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected serviceManager: ServiceManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil,
              private commonEventService: CommonEventService) {
    super(serviceManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new Service();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'Service';
    this.filterParam.relationId = this.record.id;
  }

  openFiles(imageUrl: string) {
    this.commonEventService.showImageOnClick(imageUrl);
  }
}
