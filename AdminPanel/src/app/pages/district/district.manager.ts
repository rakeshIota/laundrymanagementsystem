import { Injectable } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { RestResponse } from 'src/app/shared/auth.model';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseManager } from '../../config/base.manager';
import { DistrictService } from './district.service';

@Injectable({
  providedIn: 'root'
})
export class DistrictManager extends BaseManager {

  constructor(private districtService: DistrictService, protected loadingService: LoadingService, protected toastService: ToastService) {
    super(districtService, loadingService, toastService);
  }

  getDistricts() {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.districtService.fetchAll(null);
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }
}
