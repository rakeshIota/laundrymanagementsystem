import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {District} from '../../../models/district';
import {ActivatedRoute, Router} from '@angular/router';
import {DistrictManager} from '../district.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-district-detail',
  templateUrl: './district-detail.component.html',
  styleUrls: ['./district-detail.component.scss']
})
export class DistrictDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected districtManager: DistrictManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil) {
    super(districtManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new District();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'District';
    this.filterParam.relationId = this.record.id;
  }

}
