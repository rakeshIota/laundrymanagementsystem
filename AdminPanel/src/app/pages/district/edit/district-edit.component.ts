import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {BaseEditComponent} from '../../../config/base.edit.component';
import {District} from '../../../models/district';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {DistrictManager} from '../district.manager';
import {State} from '../../../models/state';
import {CityLanguageMapping} from '../../../models/citylanguagemapping';
import {Language} from '../../../models/language';
import {City} from '../../../models/city';
import {DistrictLanguageMapping} from '../../../models/districtlanguagemapping';
import {LanguageManager} from '../../language/language.manager';
import {CityManager} from '../../city/city.manager';

declare const $: any;

@Component({
  selector: 'app-district-edit',
  templateUrl: './district-edit.component.html',
  styleUrls: ['./district-edit.component.scss']
})

export class DistrictEditComponent extends BaseEditComponent implements OnInit {
  district: District;
  cities: City[];
  districtLanguage: DistrictLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected districtManager: DistrictManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              public commonUtil: CommonUtil, private languageManager: LanguageManager, private cityManager: CityManager) {
    super(districtManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.district = new District();
    this.district.isActive = true;
    this.setRecord(this.district);
    this.onClickMappingValidation = false;
    this.districtLanguage = new DistrictLanguageMapping();
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.district = District.fromResponse(this.record);
    this.setRecord(this.district);
  }

  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.cities = await this.cityManager.fetchAllData(null);
    this.districtLanguage.languageId = this.languages[0].id;
  }

  afterFetchAssociatedCompleted() {
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/district');
  }

  addLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.district.districtLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.districtLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('City Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.districtLanguage.languageId;
    });
    this.districtLanguage.languageName = language[0].name;
    this.district.districtLanguages.push(Object.assign({}, this.districtLanguage));
    this.districtLanguage = new DistrictLanguageMapping();
    this.districtLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: DistrictLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: DistrictLanguageMapping) {
    record.isDeleted = true;
  }
}
