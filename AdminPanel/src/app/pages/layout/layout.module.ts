import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { EditorModule } from '@tinymce/tinymce-angular';
import { DataTablesModule } from 'angular-datatables';
import { MyDatePickerModule } from 'mydatepicker';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageCropperModule } from 'ngx-image-cropper';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { MaterialAppModule } from 'src/app/material.module';
import { HttpLoaderFactory } from '../../app.module';
import { FileCropperComponent } from '../../shared/file-cropper/file-cropper.component';
import { ImagePopUPModel } from '../../shared/image-popup-model/image-popup-model';
import { AccountSettingsComponent } from '../account-settings/account-settings.component';
import { AppSettingComponent } from '../appsetting/appsetting.component';
import { AppSettingDetailComponent } from '../appsetting/detail/appsetting-detail.component';
import { AppSettingEditComponent } from '../appsetting/edit/appsetting-edit.component';
import { BroadcastComponent } from '../broadcast/broadcast.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { CityComponent } from '../city/city.component';
import { CityDetailComponent } from '../city/detail/city-detail.component';
import { CityEditComponent } from '../city/edit/city-edit.component';
import { CountryComponent } from '../country/country.component';
import { CountryDetailComponent } from '../country/detail/country-detail.component';
import { CountryEditComponent } from '../country/edit/country-edit.component';
import { CurrencyComponent } from '../currency/currency.component';
import { CurrencyDetailComponent } from '../currency/detail/currency-detail.component';
import { CurrencyEditComponent } from '../currency/edit/currency-edit.component';
import { DefaultMessageComponent } from '../defaultmessage/defaultmessage.component';
import { DefaultMessageDetailComponent } from '../defaultmessage/detail/defaultmessage-detail.component';
import { DefaultMessageEditComponent } from '../defaultmessage/edit/defaultmessage-edit.component';
import { DeliveryTypeComponent } from '../deliverytype/deliverytype.component';
import { DeliveryTypeDetailComponent } from '../deliverytype/detail/deliverytype-detail.component';
import { DeliveryTypeEditComponent } from '../deliverytype/edit/deliverytype-edit.component';
import { DistrictDetailComponent } from '../district/detail/district-detail.component';
import { DistrictComponent } from '../district/district.component';
import { DistrictEditComponent } from '../district/edit/district-edit.component';
import { FeedbackDetailComponent } from '../feedback/detail/feedback-detail.component';
import { FeedbackComponent } from '../feedback/feedback.component';
import { LandingComponent } from '../landing/landing.component';
import { LanguageDetailComponent } from '../language/detail/language-detail.component';
import { LanguageEditComponent } from '../language/edit/language-edit.component';
import { LanguageComponent } from '../language/language.component';
import { LayoutComponent } from '../layout/layout.component';
import { MessageLogDetailComponent } from '../messagelog/detail/messagelog-detail.component';
import { MessageLogEditComponent } from '../messagelog/edit/messagelog-edit.component';
import { MessageLogComponent } from '../messagelog/messagelog.component';
import { NotificationDetailComponent } from '../notification/detail/notification-detail.component';
import { NotificationEditComponent } from '../notification/edit/notification-edit.component';
import { NotificationComponent } from '../notification/notification.component';
import { NotificationMasterDetailComponent } from '../notificationmaster/detail/notificationmaster-detail.component';
import { NotificationMasterEditComponent } from '../notificationmaster/edit/notificationmaster-edit.component';
import { NotificationMasterComponent } from '../notificationmaster/notificationmaster.component';
import { OrderDetailComponent } from '../order/detail/order-detail.component';
import { OrderEditComponent } from '../order/edit/order-edit.component';
import { OrderComponent } from '../order/order.component';
import { PackingDetailComponent } from '../packing/detail/packing-detail.component';
import { PackingEditComponent } from '../packing/edit/packing-edit.component';
import { PackingComponent } from '../packing/packing.component';
import { PaymentLogDetailComponent } from '../paymentlog/detail/paymentlog-detail.component';
import { PaymentLogComponent } from '../paymentlog/paymentlog.component';
import { PostalCodeDetailComponent } from '../postalcode/detail/postalcode-detail.component';
import { PostalCodeEditComponent } from '../postalcode/edit/postalcode-edit.component';
import { PostalCodeComponent } from '../postalcode/postalcode.component';
import { ProductDetailComponent } from '../product/detail/product-detail.component';
import { ProductEditComponent } from '../product/edit/product-edit.component';
import { ProductComponent } from '../product/product.component';
import { PromotionDetailComponent } from '../promotion/detail/promotion-detail.component';
import { PromotionEditComponent } from '../promotion/edit/promotion-edit.component';
import { PromotionComponent } from '../promotion/promotion.component';
import { ProvinceDetailComponent } from '../province/detail/province-detail.component';
import { ProvinceEditComponent } from '../province/edit/province-edit.component';
import { ProvinceComponent } from '../province/province.component';
import { ServiceDetailComponent } from '../service/detail/service-detail.component';
import { ServiceEditComponent } from '../service/edit/service-edit.component';
import { ServiceComponent } from '../service/service.component';
import { StateDetailComponent } from '../state/detail/state-detail.component';
import { StateEditComponent } from '../state/edit/state-edit.component';
import { StateComponent } from '../state/state.component';
import { SubDistrictDetailComponent } from '../subdistrict/detail/subdistrict-detail.component';
import { SubDistrictEditComponent } from '../subdistrict/edit/subdistrict-edit.component';
import { SubDistrictComponent } from '../subdistrict/subdistrict.component';
import { UserAddressDetailComponent } from '../useraddress/detail/useraddress-detail.component';
import { UserAddressEditComponent } from '../useraddress/edit/useraddress-edit.component';
import { UserAddressComponent } from '../useraddress/useraddress.component';
import { UserCordinateDetailComponent } from '../usercordinate/detail/usercordinate-detail.component';
import { UserCordinateEditComponent } from '../usercordinate/edit/usercordinate-edit.component';
import { UserCordinateComponent } from '../usercordinate/usercordinate.component';
import { UserDeviceDetailComponent } from '../userdevice/detail/userdevice-detail.component';
import { UserDeviceComponent } from '../userdevice/userdevice.component';
import { UserDetailComponent } from '../users/detail/user-detail.component';
import { DriverEditComponent } from '../users/driver-edit/driver-edit.component';
import { UsersEditComponent } from '../users/edit/users-edit.component';
import { UsersComponent } from '../users/users.component';
import { LAYOUTROUTING } from './layout.routing';

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [
    LandingComponent,
    LayoutComponent,
    ChangePasswordComponent,
    AccountSettingsComponent,
    UsersComponent,
    UsersEditComponent,
    ImagePopUPModel,
    FileCropperComponent,
    FeedbackComponent,
    FeedbackDetailComponent,
    UserDeviceComponent,
    UserDeviceDetailComponent,
    MessageLogComponent,
    MessageLogEditComponent,
    MessageLogDetailComponent,
    NotificationComponent,
    NotificationEditComponent,
    NotificationDetailComponent,
    AppSettingComponent,
    AppSettingEditComponent,
    AppSettingDetailComponent,
    CountryComponent,
    CountryEditComponent,
    CountryDetailComponent,
    LanguageComponent,
    LanguageEditComponent,
    LanguageDetailComponent,
    StateComponent,
    StateEditComponent,
    StateDetailComponent,
    CityComponent,
    CityEditComponent,
    CityDetailComponent,
    UserAddressComponent,
    UserAddressEditComponent,
    UserAddressDetailComponent,
    DistrictComponent,
    DistrictEditComponent,
    DistrictDetailComponent,
    SubDistrictComponent,
    SubDistrictEditComponent,
    SubDistrictDetailComponent,
    ProvinceComponent,
    ProvinceEditComponent,
    ProvinceDetailComponent,
    UserCordinateComponent,
    UserCordinateEditComponent,
    UserCordinateDetailComponent,
    ServiceComponent,
    ServiceEditComponent,
    ServiceDetailComponent,
    ProductComponent,
    ProductEditComponent,
    ProductDetailComponent,
    DeliveryTypeComponent,
    DeliveryTypeEditComponent,
    DeliveryTypeDetailComponent,
    OrderComponent,
    OrderEditComponent,
    OrderDetailComponent,
    PaymentLogComponent,
    PaymentLogDetailComponent,
    PackingComponent,
    PackingEditComponent,
    PackingDetailComponent,
    DefaultMessageComponent,
    DefaultMessageEditComponent,
    DefaultMessageDetailComponent,
    CurrencyComponent,
    CurrencyEditComponent,
    CurrencyDetailComponent,
    PostalCodeComponent,
    PostalCodeEditComponent,
    PostalCodeDetailComponent,
    NotificationMasterComponent,
    NotificationMasterEditComponent,
    NotificationMasterDetailComponent,
    UserDetailComponent,
    DriverEditComponent,
    PromotionComponent,
    PromotionEditComponent,
    PromotionDetailComponent,
    BroadcastComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    EditorModule,
    FileUploadModule,
    MyDatePickerModule,
    ImageCropperModule,
    MaterialAppModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forChild(LAYOUTROUTING),
    DataTablesModule,
    NgSelectModule,
    NgxMaskModule.forRoot(maskConfig)
  ]
})
export class LayoutModule {
}
