import { Routes } from '@angular/router';
import { AuthGuard } from '../../shared/auth.guard';
import { AccountSettingsComponent } from '../account-settings/account-settings.component';
import { BroadcastComponent } from '../broadcast/broadcast.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { LandingComponent } from '../landing/landing.component';
import { PostalCodeEditComponent } from '../postalcode/edit/postalcode-edit.component';
import { PromotionDetailComponent } from '../promotion/detail/promotion-detail.component';
import { PromotionEditComponent } from '../promotion/edit/promotion-edit.component';
import { PromotionComponent } from '../promotion/promotion.component';
import { UserDetailComponent } from '../users/detail/user-detail.component';
import { DriverEditComponent } from '../users/driver-edit/driver-edit.component';
import { UsersEditComponent } from '../users/edit/users-edit.component';
import { UsersComponent } from '../users/users.component';
import { AppSettingComponent } from './../appsetting/appsetting.component';
import { AppSettingDetailComponent } from './../appsetting/detail/appsetting-detail.component';
import { AppSettingEditComponent } from './../appsetting/edit/appsetting-edit.component';
import { CityComponent } from './../city/city.component';
import { CityDetailComponent } from './../city/detail/city-detail.component';
import { CityEditComponent } from './../city/edit/city-edit.component';
import { CountryComponent } from './../country/country.component';
import { CountryDetailComponent } from './../country/detail/country-detail.component';
import { CountryEditComponent } from './../country/edit/country-edit.component';
import { CurrencyComponent } from './../currency/currency.component';
import { CurrencyDetailComponent } from './../currency/detail/currency-detail.component';
import { CurrencyEditComponent } from './../currency/edit/currency-edit.component';
import { DefaultMessageComponent } from './../defaultmessage/defaultmessage.component';
import { DefaultMessageDetailComponent } from './../defaultmessage/detail/defaultmessage-detail.component';
import { DefaultMessageEditComponent } from './../defaultmessage/edit/defaultmessage-edit.component';
import { DeliveryTypeComponent } from './../deliverytype/deliverytype.component';
import { DeliveryTypeDetailComponent } from './../deliverytype/detail/deliverytype-detail.component';
import { DeliveryTypeEditComponent } from './../deliverytype/edit/deliverytype-edit.component';
import { DistrictDetailComponent } from './../district/detail/district-detail.component';
import { DistrictComponent } from './../district/district.component';
import { DistrictEditComponent } from './../district/edit/district-edit.component';
import { FeedbackDetailComponent } from './../feedback/detail/feedback-detail.component';
import { FeedbackComponent } from './../feedback/feedback.component';
import { LanguageDetailComponent } from './../language/detail/language-detail.component';
import { LanguageEditComponent } from './../language/edit/language-edit.component';
import { LanguageComponent } from './../language/language.component';
import { NotificationMasterDetailComponent } from './../notificationmaster/detail/notificationmaster-detail.component';
import { NotificationMasterEditComponent } from './../notificationmaster/edit/notificationmaster-edit.component';
import { NotificationMasterComponent } from './../notificationmaster/notificationmaster.component';
import { OrderDetailComponent } from './../order/detail/order-detail.component';
import { OrderEditComponent } from './../order/edit/order-edit.component';
import { OrderComponent } from './../order/order.component';
import { PackingDetailComponent } from './../packing/detail/packing-detail.component';
import { PackingEditComponent } from './../packing/edit/packing-edit.component';
import { PackingComponent } from './../packing/packing.component';
import { PaymentLogDetailComponent } from './../paymentlog/detail/paymentlog-detail.component';
import { PaymentLogComponent } from './../paymentlog/paymentlog.component';
import { PostalCodeComponent } from './../postalcode/postalcode.component';
import { ProductDetailComponent } from './../product/detail/product-detail.component';
import { ProductEditComponent } from './../product/edit/product-edit.component';
import { ProductComponent } from './../product/product.component';
import { ProvinceDetailComponent } from './../province/detail/province-detail.component';
import { ProvinceEditComponent } from './../province/edit/province-edit.component';
import { ProvinceComponent } from './../province/province.component';
import { ServiceDetailComponent } from './../service/detail/service-detail.component';
import { ServiceEditComponent } from './../service/edit/service-edit.component';
import { ServiceComponent } from './../service/service.component';
import { StateDetailComponent } from './../state/detail/state-detail.component';
import { StateEditComponent } from './../state/edit/state-edit.component';
import { StateComponent } from './../state/state.component';
import { SubDistrictDetailComponent } from './../subdistrict/detail/subdistrict-detail.component';
import { SubDistrictEditComponent } from './../subdistrict/edit/subdistrict-edit.component';
import { SubDistrictComponent } from './../subdistrict/subdistrict.component';
import { LayoutComponent } from './layout.component';

export const LAYOUTROUTING: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      { path: '', component: LandingComponent },
      { path: 'change/password', component: ChangePasswordComponent },
      { path: 'account/settings', component: AccountSettingsComponent },
      { path: 'users', component: UsersComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      { path: 'user/edit/:id', component: UsersEditComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      { path: 'user/detail/:id', component: UserDetailComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      { path: 'customers', component: UsersComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      { path: 'drivers', component: UsersComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      { path: 'driver/edit/:id', component: DriverEditComponent, canActivate: [AuthGuard], data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] } },
      {
        path: 'feedback',
        component: FeedbackComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'feedback/detail/:id',
        component: FeedbackDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'app-setting',
        component: AppSettingComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'app-setting/edit/:id',
        component: AppSettingEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'app-setting/detail/:id',
        component: AppSettingDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'country',
        component: CountryComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'country/edit/:id',
        component: CountryEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'country/detail/:id',
        component: CountryDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'language',
        component: LanguageComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'language/edit/:id',
        component: LanguageEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'language/detail/:id',
        component: LanguageDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'state',
        component: StateComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'state/edit/:id',
        component: StateEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'state/detail/:id',
        component: StateDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'city',
        component: CityComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'city/edit/:id',
        component: CityEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'city/detail/:id',
        component: CityDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'district',
        component: DistrictComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'district/edit/:id',
        component: DistrictEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'district/detail/:id',
        component: DistrictDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'sub-district',
        component: SubDistrictComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'sub-district/edit/:id',
        component: SubDistrictEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'sub-district/detail/:id',
        component: SubDistrictDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'province',
        component: ProvinceComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'province/edit/:id',
        component: ProvinceEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'province/detail/:id',
        component: ProvinceDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'service',
        component: ServiceComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'service/edit/:id',
        component: ServiceEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'service/detail/:id',
        component: ServiceDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'product',
        component: ProductComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'product/edit/:id',
        component: ProductEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'product/detail/:id',
        component: ProductDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'delivery-type',
        component: DeliveryTypeComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'delivery-type/edit/:id',
        component: DeliveryTypeEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'delivery-type/detail/:id',
        component: DeliveryTypeDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'order',
        component: OrderComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] }
      },
      {
        path: 'order/edit/:id',
        component: OrderEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] }
      },
      {
        path: 'order/detail/:id',
        component: OrderDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN', 'ROLE_OPERATION'] }
      },
      {
        path: 'payment-log',
        component: PaymentLogComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'payment-log/detail/:id',
        component: PaymentLogDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'packing',
        component: PackingComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'packing/edit/:id',
        component: PackingEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'packing/detail/:id',
        component: PackingDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'default-message',
        component: DefaultMessageComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'default-message/edit/:id',
        component: DefaultMessageEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'default-message/detail/:id',
        component: DefaultMessageDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'currency',
        component: CurrencyComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'currency/edit/:id',
        component: CurrencyEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'currency/detail/:id',
        component: CurrencyDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'postal-code',
        component: PostalCodeComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'postal-code/edit/:id',
        component: PostalCodeEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'notification-master',
        component: NotificationMasterComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'notification-master/edit/:id',
        component: NotificationMasterEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'notification-master/detail/:id',
        component: NotificationMasterDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'promotion',
        component: PromotionComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'promotion/edit/:id',
        component: PromotionEditComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'promotion/detail/:id',
        component: PromotionDetailComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      },
      {
        path: 'broadcast',
        component: BroadcastComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_ADMIN'] }
      }
    ]
  },
];
