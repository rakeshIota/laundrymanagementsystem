import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.services';
import { CommonUtil } from '../../shared/common.util';

declare const $: any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  currentMenu: string;
  constructor(public commonUtil: CommonUtil, public authService: AuthService) { }

  ngOnInit() {
    $('#content').addClass('window-content-body');
    $('#content').removeClass('mobile-content-body');
    $('#main-container').removeClass('base-page-container');
    $('#main-container').addClass('landing-page-container');
  }

  ngOnDestroy() {
    $('#main-container').addClass('base-page-container');
    $('#main-container').removeClass('landing-page-container');
  }

  setCurrentMenu(type: string) {
    this.currentMenu = type;
  }
}
