import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { UploadService } from '../../../shared/upload.service';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';

import { Product } from '../../../models/product';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import { CommonUtil } from '../../../shared/common.util';
import { ProductManager } from '../product.manager';


import { CommonEventService } from '../../../shared/common.event.service';
import { Language } from '../../../models/language';
import { ProductLanguageMapping } from '../../../models/productlanguagemapping';
import { LanguageManager } from '../../language/language.manager';
import { Service } from '../../../models/service';
import { ProductPrice } from '../../../models/productprice';
import { ServiceManager } from '../../service/service.manager';

declare const $: any;

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})

export class ProductEditComponent extends BaseEditComponent implements OnInit {

  product: Product;
  productImageUploader: FileUploader;
  productLanguage: ProductLanguageMapping;
  onClickMappingValidation: boolean;
  onClickPriceValidation: boolean;
  languages: Language[];
  services: Service[];
  productPrice: ProductPrice;
  messageAlreadyShown: boolean;
  filters: any[];

  constructor(protected uploadService: UploadService, protected route: ActivatedRoute, protected productManager: ProductManager,
    protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
    protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
    public commonUtil: CommonUtil, private commonEventService: CommonEventService, private languageManager: LanguageManager,
    private serviceManager: ServiceManager) {
    super(productManager, commonService, toastService, loadingService, route, router, translateService);
    this.filters = [] as any[];
  }

  async ngOnInit() {
    this.product = new Product();
    this.setRecord(this.product);
    this.onClickMappingValidation = false;
    this.onClickPriceValidation = false;
    this.productLanguage = new ProductLanguageMapping();
    this.productPrice = new ProductPrice();
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.messageAlreadyShown = false;
    this.init();
    this.filters = await this.productManager.fetchProductFilters();
  }

  onFetchCompleted() {
    this.product = Product.fromResponse(this.record);
    this.setRecord(this.product);
    this.pluginIntialization();
  }

  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.services = await this.serviceManager.fetchAllData(null);
    this.productLanguage.languageId = this.languages[0].id;
  }

  pluginIntialization() {
    this.productImageUploader = this.initializeUploader(this.product.image, '.jpg,.jpeg,.png', 102400, 1, this.toastService);
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/product');
  }

  addLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.product.productLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.productLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('Product Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.productLanguage.languageId;
    });
    this.productLanguage.languageName = language[0].name;
    this.product.productLanguages.push(Object.assign({}, this.productLanguage));
    this.productLanguage = new ProductLanguageMapping();
    this.productLanguage.languageId = this.languages[0].id;
  }

  addPrice(valid: boolean) {
    this.onClickPriceValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.product.productPrices.filter((obj) => {
      return !obj.isDeleted && obj.serviceId === this.productPrice.serviceId;
    });
    if (hasAlreadyAdded.length > 0) {
      if (!this.messageAlreadyShown) {
        this.toastService.error('Price already added for services');
      }
      this.messageAlreadyShown = true;
      return;
    }
    const service = this.services.filter((obj) => {
      return obj.id === this.productPrice.serviceId;
    });
    this.productPrice.serviceName = service[0].name;
    this.product.productPrices.push(this.productPrice);
    this.productPrice = new ProductPrice();
  }

  removeLanguage(record: ProductLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: ProductLanguageMapping) {
    record.isDeleted = true;
  }

  removePrice(record: ProductPrice) {
    this.commonService.confirmation('Would you like to delete?', this.removePriceCallback.bind(this), record);
  }

  removePriceCallback(record: ProductPrice) {
    record.isDeleted = true;
  }

  async saveProduct(form) {
    if (this.product.productLanguages.length === 0) {
      this.toastService.error('Please add a language');
      return;
    }
    if (this.product.productPrices.length === 0) {
      this.toastService.error('Please add prices');
      return;
    }
    this.save(form);
  }
}
