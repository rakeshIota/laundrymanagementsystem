import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from 'src/app/services/loading.service';
import { RestResponse } from 'src/app/shared/auth.model';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseManager } from '../../config/base.manager';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductManager extends BaseManager {

  constructor(private productService: ProductService, protected loadingService: LoadingService, protected toastService: ToastService, private translateService: TranslateService) {
    super(productService, loadingService, toastService);
  }

  async fetchProductFilters(): Promise<any[]> {
    try {
      const response: RestResponse = await this.productService.getFilters();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      response.data.forEach(filter => {
        filter.translatedName = this.translateService.instant(`Product.${filter.value}`);
      });
      response.data.splice(response.data.findIndex(x => x.value === 'ALL'), 1);
      return response.data;
    } catch (e) {
      this.toastService.error(e.message);
    }
  }
}
