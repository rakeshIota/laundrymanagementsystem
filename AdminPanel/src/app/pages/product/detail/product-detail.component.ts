import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {Product} from '../../../models/product';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductManager} from '../product.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonEventService} from '../../../shared/common.event.service';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected productManager: ProductManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, private commonEventService: CommonEventService,
              private commonUtil: CommonUtil) {
    super(productManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new Product();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'Product';
    this.filterParam.relationId = this.record.id;
  }

  openFiles(imageUrl: string) {
    this.commonEventService.showImageOnClick(imageUrl);
  }
}
