import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestResponse } from 'src/app/shared/auth.model';
import { BaseService } from '../../config/base.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService {

  constructor(public http: HttpClient) {
    super(http, '/api/product', '/api/products');
  }

  productsByService(serviceId: string): Observable<RestResponse> {
    return this.getRecord(`/api/application/service/${serviceId}/products`);
  }

  getFilters(): Promise<RestResponse> {
    return this.saveRecord('/api/product/filters', null);
}
}

