import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { Feedback } from '../../../models/feedback';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbackManager } from '../feedback.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-feedback-detail',
  templateUrl: './feedback-detail.component.html',
  styleUrls: ['./feedback-detail.component.scss']
})
export class FeedbackDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected feedbackManager: FeedbackManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(feedbackManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new Feedback();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "Feedback";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
