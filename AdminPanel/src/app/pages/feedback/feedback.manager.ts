import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { FeedbackService } from './feedback.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class FeedbackManager extends BaseManager {

    constructor(private feedbackService: FeedbackService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(feedbackService, loadingService, toastService);
    }
}
