import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { DeliveryType } from '../../../models/deliverytype';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { DeliveryTypeManager } from '../deliverytype.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-deliverytype-edit',
  templateUrl: './deliverytype-edit.component.html',
  styleUrls: ['./deliverytype-edit.component.scss']
})

export class DeliveryTypeEditComponent extends BaseEditComponent implements OnInit {
  public deliveryType: DeliveryType;
  
  constructor(protected route: ActivatedRoute, protected deliverytypeManager: DeliveryTypeManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(deliverytypeManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.deliveryType = new DeliveryType();
  	this.deliveryType.isActive=true;   
    this.setRecord(this.deliveryType);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.deliveryType = DeliveryType.fromResponse(this.record);
    this.setRecord(this.deliveryType);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/delivery-type');
  }	
	  
	
}
