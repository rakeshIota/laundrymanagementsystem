import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { DeliveryTypeService } from './deliverytype.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class DeliveryTypeManager extends BaseManager {

    constructor(private deliveryTypeService: DeliveryTypeService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(deliveryTypeService, loadingService, toastService);
    }
}
