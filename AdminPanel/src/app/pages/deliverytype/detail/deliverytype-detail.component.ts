import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { DeliveryType } from '../../../models/deliverytype';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliveryTypeManager } from '../deliverytype.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-deliverytype-detail',
  templateUrl: './deliverytype-detail.component.html',
  styleUrls: ['./deliverytype-detail.component.scss']
})
export class DeliveryTypeDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected deliveryTypeManager: DeliveryTypeManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(deliveryTypeManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new DeliveryType();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "DeliveryType";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
