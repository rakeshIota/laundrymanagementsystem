import {Component, OnDestroy, OnInit} from '@angular/core';
import {BaseListComponent} from '../../config/base.list.component';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../shared/auth.services';
import {CommonService} from '../../shared/common.service';
import {ToastService} from '../../shared/toast.service';
import {DeliveryTypeManager} from './deliverytype.manager';
import {DeliveryType} from '../../models/deliverytype';
import {isNullOrUndefined} from 'util';
import {Router} from '@angular/router';
import {CommonUtil} from '../../shared/common.util';

declare const $: any;

@Component({
  selector: 'app-deliverytype',
  templateUrl: './deliverytype.component.html',
  styleUrls: ['./deliverytype.component.scss']
})
export class DeliveryTypeComponent extends BaseListComponent implements OnInit, OnDestroy {

  constructor(protected deliveryTypeManager: DeliveryTypeManager, protected toastService: ToastService, protected router: Router,
              protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService,
              public commonUtil: CommonUtil) {
    super(deliveryTypeManager, commonService, toastService, loadingService, router);
  }

  ngOnInit() {
    this.request.loadEditPage = false;
    this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
    this.records = new Array<DeliveryType>();
    this.init();
  }

  onItemSelection(record: any) {
    this.onAssociatedValueSelected(record);
  }

  onCancel() {
    this.request.loadEditPage = false;
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  onNewRecord() {
    if (!this.isPlusButton) {
      if (this.filterParam) {
        this.router.navigate(['/dashboard/delivery-type/edit/0'],
          {queryParams: {[this.filterParam.relationTable]: this.filterParam.relationId}});
      } else {
        this.router.navigate(['/dashboard/delivery-type/edit/0']);
      }
      return;
    }
    this.request.loadEditPage = true;
  }

  removeSuccess() {
    this.onCancel();
  }


  ngOnDestroy() {
    this.clean();
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#deliveryTypeDetailPage').appendTo('body').modal('show');
      $('#deliveryTypeDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }
}
