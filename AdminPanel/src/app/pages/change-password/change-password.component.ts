import { Component, OnInit } from '@angular/core';
import { ToastService } from '../../shared/toast.service';
import { AccountService } from '../../services/account.service';
import { RestResponse } from '../../shared/auth.model';
import { LoadingService } from '../../services/loading.service';
import { Profile } from '../../models/profile';
import { CommonUtil } from '../../shared/common.util';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  onClickValidation: boolean;
  data: Profile;

  constructor(private toastService: ToastService, private accountService: AccountService, private loadingService: LoadingService,
    public commonUtil: CommonUtil) {
    this.onClickValidation = false;
    this.data = new Profile();
  }

  ngOnInit() {
  }

  update(form) {
    this.onClickValidation = !form.valid;
    if (!form.valid) {
      return false;
    }
    if (!this.data.isValidChangePasswordRequest(form)) {
      return;
    }

    if (this.data.password !== this.data.confirmPassword) {
      this.toastService.error('Password doesn\'t matches');
      return;
    }

    this.loadingService.show();
    this.accountService.changePassword(this.data.forRequest())
      .then((data: RestResponse) => {
        this.loadingService.hide();
        if (!data.status) {
          this.toastService.error(data.message);
          return;
        }
        this.data = {} as any;
        this.toastService.success(data.message);
      }, (error) => {
        this.loadingService.hide();
        this.toastService.error(error.message);
      });
  }
}
