import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BroadcastFilterType, BroadcastMessage } from 'src/app/models/broadcast.message';
import { PostalCode } from 'src/app/models/postalcode';
import { LoadingService } from 'src/app/services/loading.service';
import { RestResponse } from 'src/app/shared/auth.model';
import { CommonUtil } from 'src/app/shared/common.util';
import { ToastService } from 'src/app/shared/toast.service';
import { isNullOrUndefined } from 'util';
import { PostalCodeManager } from '../postalcode/postalcode.manager';
import { BroadcastManager } from './broadcast.manager';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {
  customerTypes: BroadcastFilterType[];
  genders: BroadcastFilterType[];
  orderTypes: BroadcastFilterType[];
  postalCodes: PostalCode[];
  broadcastMessage: BroadcastMessage;
  onClickValidation: boolean;

  constructor(
    public commonUtil: CommonUtil,
    private postalCodeManager: PostalCodeManager,
    private toastService: ToastService,
    private translateService: TranslateService,
    private broadcastManager: BroadcastManager,
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.customerTypes = new Array<BroadcastFilterType>();
    this.genders = new Array<BroadcastFilterType>();
    this.postalCodes = new Array<PostalCode>();
    this.broadcastMessage = new BroadcastMessage();
    this.onClickValidation = false;
    this.customerTypes = this.broadcastManager.fetchCustomerTypes();
    this.genders = this.broadcastManager.fetchGenders();
    this.orderTypes = this.broadcastManager.fetchOrderTypes();
    this.fetchPostalCodes();
  }

  async fetchPostalCodes() {
    try {
      const response: RestResponse = await this.postalCodeManager.fetchAll(null);
      if (!response.status) {
        this.toastService.error(this.translateService.instant('ErrorCodeMessages.44002'));
        return;
      }
      this.postalCodes = response.data;
    } catch (error) {
      this.toastService.error(this.translateService.instant('ErrorCodeMessages.44002'));
    }
  }

  onPinCodeAddition(postalCode) {
    console.log(postalCode);
    this.postalCodes.push(postalCode);
  }

  onPinCodeRemoval(postalCode) {
    const itemIndex: number = this.postalCodes.findIndex(x => x === postalCode);
    if (itemIndex !== -1) {
      this.postalCodes.splice(itemIndex, 1);
    }
  }

  async send(isValid: boolean) {
    this.onClickValidation = !isValid;
    if (!isValid) {
      return false;
    }
    this.broadcastMessage.message = this.broadcastMessage.message.trim();
    if (!isNullOrUndefined(this.broadcastMessage.message) && this.broadcastMessage.message === '') {
      this.toastService.error(this.translateService.instant('ErrorCodeMessages.47007'));
      return;
    }
    try {
      this.loadingService.show();
      const response: RestResponse = await this.broadcastManager.sendBroadcast(this.broadcastMessage);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.ngOnInit();
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }
}
