import { Injectable } from '@angular/core';
import { Constant } from 'src/app/config/constants';
import { BroadcastFilterType, BroadcastMessage } from 'src/app/models/broadcast.message';
import { RestResponse } from 'src/app/shared/auth.model';
import { BroadcastService } from './broadcast.service';

@Injectable({
  providedIn: 'root'
})
export class BroadcastManager {
  constructor(
    private broadcastService: BroadcastService
  ) { }

  fetchCustomerTypes() {
    const customerTypes = [
      { name: 'All Customers', value: Constant.BROADCAST.CUSTOMER_TYPE.ALL_CUSTOMERS },
      { name: 'Tourist', value: Constant.BROADCAST.CUSTOMER_TYPE.TOURIST },
      { name: 'Resident', value: Constant.BROADCAST.CUSTOMER_TYPE.RESIDENT },
      { name: `Registered but without any Order`, value: Constant.BROADCAST.CUSTOMER_TYPE.WITHOUT_ORDER },
      { name: 'With selected postal codes', value: Constant.BROADCAST.CUSTOMER_TYPE.WITH_POSTAL_CODES },
    ] as BroadcastFilterType[];
    return customerTypes;
  }

  fetchGenders() {
    const genders = [
      { name: 'Male', value: Constant.BROADCAST.CUSTOMER_GENDER.MALE },
      { name: 'Female', value: Constant.BROADCAST.CUSTOMER_GENDER.FEMALE },
    ] as BroadcastFilterType[];
    return genders;
  }

  fetchOrderTypes() {
    const genders = [
      { name: 'Greater than', value: Constant.BROADCAST.CUSTOMER_ORDER_TYPE.GREATER_THAN },
      { name: 'Less than', value: Constant.BROADCAST.CUSTOMER_ORDER_TYPE.LESS_THAN },
      { name: 'Equal to', value: Constant.BROADCAST.CUSTOMER_ORDER_TYPE.EQUAL_TO },
    ] as BroadcastFilterType[];
    return genders;
  }

  sendBroadcast(broadcast: BroadcastMessage) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.broadcastService.send(broadcast);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }
}
