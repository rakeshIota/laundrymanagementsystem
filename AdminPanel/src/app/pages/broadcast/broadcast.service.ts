import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BroadcastMessage } from 'src/app/models/broadcast.message';
import { RestResponse } from 'src/app/shared/auth.model';
import { BaseService } from '../../config/base.service';

@Injectable({
  providedIn: 'root'
})
export class BroadcastService extends BaseService {

  constructor(public http: HttpClient) {
    super(http, '/api/broadcast', '/api/broadcasts');
  }

  send(broadcast: BroadcastMessage): Promise<RestResponse> {
    return this.saveRecord('/api/broadcast', broadcast);
  }
}

