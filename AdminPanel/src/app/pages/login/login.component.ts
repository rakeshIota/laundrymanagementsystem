import {Component, OnInit} from '@angular/core';
import {ToastService} from '../../shared/toast.service';
import {LoginService} from '../../services/login.service';
import {LocalStorageService} from 'angular-2-local-storage';
import {Router} from '@angular/router';
import {Login} from '../../models/login';
import {LoadingService} from '../../services/loading.service';
import {RestResponse} from '../../shared/auth.model';

declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  onClickValidation: boolean;
  data: Login;
  view: string;

  constructor(private toastService: ToastService, private loginService: LoginService, private router: Router,
              private localStorageService: LocalStorageService, private loadingService: LoadingService) {
  }

  ngOnInit() {
    this.onClickValidation = false;
    this.data = new Login();
    this.view = 'EMAILSLIDE';
  }

  async login(form) {
    this.onClickValidation = !form.valid;
    if (!form.valid) {
      return false;
    }
    if (!this.data.isValidLoginRequest(form)) {
      return;
    }
    try {
      this.loadingService.show();
      const data: RestResponse = await this.loginService.login(this.data);
      this.loadingService.hide();
      if (!data.status) {
        this.toastService.error(data.message);
        return;
      }
      const response = data.data;
      response.token.expires_at = new Date(response.token.expires).getTime();
      this.localStorageService.set('token', response.token);
      this.localStorageService.set('user', response.user);
      setTimeout(() => {
        this.router.navigate(['/dashboard/order']);
      });
    } catch (e) {
      this.loadingService.hide();
      this.toastService.error(e.message);
    }
  }

  async resetPassword(form) {
    this.onClickValidation = !form.valid;
    if (!form.valid) {
      return false;
    }
    if (!this.data.isValidForgotPasswordRequest(form)) {
      return;
    }
    try {
      this.loadingService.show();
      delete this.data.password;
      const response: RestResponse = await this.loginService.resetPassword(this.data);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.data.userName = undefined;
      this.toastService.success(response.message);
      this.view = 'EMAILSLIDE';
      this.data.email = null;
    } catch (e) {
      this.loadingService.hide();
      this.toastService.error(e.message);
    }
  }

  gotoForgotPassword() {
    this.view = 'FORGOTPASSWORD';
  }

  goToLogin() {
    this.view = 'EMAILSLIDE';
  }
}
