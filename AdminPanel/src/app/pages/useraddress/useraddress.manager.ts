import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { UserAddressService } from './useraddress.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class UserAddressManager extends BaseManager {

    constructor(private userAddressService: UserAddressService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(userAddressService, loadingService, toastService);
    }
}
