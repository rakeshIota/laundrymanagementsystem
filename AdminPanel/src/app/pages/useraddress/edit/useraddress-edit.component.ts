import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';

import {BaseEditComponent} from '../../../config/base.edit.component';
import {BaseModel} from '../../../config/base.model';

import {UserAddress} from '../../../models/useraddress';
import {LoadingService} from '../../../services/loading.service';

import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {UserAddressManager} from '../useraddress.manager';


import {CommonEventService} from '../../../shared/common.event.service';

declare const $: any;

@Component({
  selector: 'app-useraddress-edit',
  templateUrl: './useraddress-edit.component.html',
  styleUrls: ['./useraddress-edit.component.scss']
})

export class UserAddressEditComponent extends BaseEditComponent implements OnInit {
  public userAddress: UserAddress;

  constructor(protected route: ActivatedRoute, protected useraddressManager: UserAddressManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService
    , public commonUtil: CommonUtil) {
    super(useraddressManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.userAddress = new UserAddress();
    this.userAddress.isActive = true;
    this.setRecord(this.userAddress);


    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.userAddress = UserAddress.fromResponse(this.record);
    this.setRecord(this.userAddress);
  }


  async fetchAssociatedData() {
    this.afterFetchAssociatedCompleted();
  }

  afterFetchAssociatedCompleted() {
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/user-address');
  }


  checkConditionToReload(records: BaseModel[], selectedRecord: any) {
    if (!records.some(x => x.id === selectedRecord.id)) {
      this.fetchAssociatedData();
    }
  }

  onAssociatedValueSelected(selectedRecord: any, selectedField: any) {
    if (this.request.popupId) {
      $('#' + this.request.popupId).appendTo('body').modal('hide');
    }

  }
}
