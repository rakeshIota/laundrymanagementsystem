import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { UserAddress } from '../../../models/useraddress';
import { ActivatedRoute, Router } from '@angular/router';
import { UserAddressManager } from '../useraddress.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-useraddress-detail',
  templateUrl: './useraddress-detail.component.html',
  styleUrls: ['./useraddress-detail.component.scss']
})
export class UserAddressDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected userAddressManager: UserAddressManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(userAddressManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new UserAddress();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "UserAddress";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
