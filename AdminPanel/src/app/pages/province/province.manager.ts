import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { ProvinceService } from './province.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class ProvinceManager extends BaseManager {

    constructor(private provinceService: ProvinceService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(provinceService, loadingService, toastService);
    }
}
