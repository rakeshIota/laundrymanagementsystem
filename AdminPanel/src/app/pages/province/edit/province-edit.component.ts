import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {BaseEditComponent} from '../../../config/base.edit.component';
import {Province} from '../../../models/province';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {ProvinceManager} from '../province.manager';
import {LanguageManager} from '../../language/language.manager';
import {Language} from '../../../models/language';
import {ProvinceLanguageMapping} from '../../../models/provincelanguagemapping';

declare const $: any;

@Component({
  selector: 'app-province-edit',
  templateUrl: './province-edit.component.html',
  styleUrls: ['./province-edit.component.scss']
})

export class ProvinceEditComponent extends BaseEditComponent implements OnInit {
  province: Province;
  languages: Language[];
  provinceLanguage: ProvinceLanguageMapping;
  onClickMappingValidation: boolean;

  constructor(protected route: ActivatedRoute, protected provinceManager: ProvinceManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              private languageManager: LanguageManager, public commonUtil: CommonUtil) {
    super(provinceManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.province = new Province();
    this.setRecord(this.province);
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.languages = new Array<Language>();
    this.onClickMappingValidation = false;
    this.provinceLanguage = new ProvinceLanguageMapping();
    this.init();
  }

  onFetchCompleted() {
    this.province = Province.fromResponse(this.record);
    this.setRecord(this.province);
  }

  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.provinceLanguage.languageId = this.languages[0].id;
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/province');
  }

  addLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.province.provinceLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.provinceLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('City Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.provinceLanguage.languageId;
    });
    this.provinceLanguage.languageName = language[0].name;
    this.province.provinceLanguages.push(Object.assign({}, this.provinceLanguage));
    this.provinceLanguage = new ProvinceLanguageMapping();
    this.provinceLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: ProvinceLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: ProvinceLanguageMapping) {
    record.isDeleted = true;
  }
}
