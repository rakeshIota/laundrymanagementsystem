import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {Province} from '../../../models/province';
import {ActivatedRoute, Router} from '@angular/router';
import {ProvinceManager} from '../province.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-province-detail',
  templateUrl: './province-detail.component.html',
  styleUrls: ['./province-detail.component.scss']
})
export class ProvinceDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected provinceManager: ProvinceManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil) {
    super(provinceManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new Province();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'Province';
    this.filterParam.relationId = this.record.id;
  }

}
