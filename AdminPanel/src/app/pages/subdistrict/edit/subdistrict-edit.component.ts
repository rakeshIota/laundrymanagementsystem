import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';

import {BaseEditComponent} from '../../../config/base.edit.component';

import {SubDistrict} from '../../../models/subdistrict';
import {LoadingService} from '../../../services/loading.service';

import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {SubDistrictManager} from '../subdistrict.manager';


import {DistrictManager} from '../../district/district.manager';
import {District} from '../../../models/district';
import {Language} from '../../../models/language';
import {SubDistrictLanguageMapping} from '../../../models/subdistrictlanguagemapping';
import {LanguageManager} from '../../language/language.manager';

declare const $: any;

@Component({
  selector: 'app-subdistrict-edit',
  templateUrl: './subdistrict-edit.component.html',
  styleUrls: ['./subdistrict-edit.component.scss']
})

export class SubDistrictEditComponent extends BaseEditComponent implements OnInit {
  subDistrict: SubDistrict;
  districts: District[];
  subDistrictLanguage: SubDistrictLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected subdistrictManager: SubDistrictManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              private districtManager: DistrictManager, public commonUtil: CommonUtil, private languageManager: LanguageManager) {
    super(subdistrictManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.subDistrict = new SubDistrict();
    this.setRecord(this.subDistrict);
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.districts = new Array<District>();
    this.onClickMappingValidation = false;
    this.subDistrictLanguage = new SubDistrictLanguageMapping();
    this.init();
  }

  onFetchCompleted() {
    this.subDistrict = SubDistrict.fromResponse(this.record);
    this.setRecord(this.subDistrict);
  }


  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.districts = await this.districtManager.fetchAllData(null);
    this.subDistrictLanguage.languageId = this.languages[0].id;
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/sub-district');
  }


  addLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.subDistrict.subDistrictLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.subDistrictLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('City Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.subDistrictLanguage.languageId;
    });
    this.subDistrictLanguage.languageName = language[0].name;
    this.subDistrict.subDistrictLanguages.push(Object.assign({}, this.subDistrictLanguage));
    this.subDistrictLanguage = new SubDistrictLanguageMapping();
    this.subDistrictLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: SubDistrictLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: SubDistrictLanguageMapping) {
    record.isDeleted = true;
  }
}
