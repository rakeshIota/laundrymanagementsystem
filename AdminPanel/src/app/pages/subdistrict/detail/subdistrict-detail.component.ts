import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {SubDistrict} from '../../../models/subdistrict';
import {ActivatedRoute, Router} from '@angular/router';
import {SubDistrictManager} from '../subdistrict.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-subdistrict-detail',
  templateUrl: './subdistrict-detail.component.html',
  styleUrls: ['./subdistrict-detail.component.scss']
})
export class SubDistrictDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected subDistrictManager: SubDistrictManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil) {
    super(subDistrictManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new SubDistrict();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'SubDistrict';
    this.filterParam.relationId = this.record.id;
  }

}
