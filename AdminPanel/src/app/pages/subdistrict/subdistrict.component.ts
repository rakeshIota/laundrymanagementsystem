import { Component, OnDestroy, OnInit, Input, Output, ViewChild } from '@angular/core';
import { BaseListComponent } from '../../config/base.list.component';
import { LoadingService } from '../../services/loading.service';
import { AuthService } from '../../shared/auth.services';
import { CommonService } from '../../shared/common.service';
import { ToastService } from '../../shared/toast.service';
import { SubDistrictManager } from './subdistrict.manager';
import { SubDistrict } from '../../models/subdistrict';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import {CommonUtil} from '../../shared/common.util';

declare const $: any;

@Component({
  selector: 'app-subdistrict',
  templateUrl: './subdistrict.component.html',
  styleUrls: ['./subdistrict.component.scss']
})
export class SubDistrictComponent extends BaseListComponent implements OnInit, OnDestroy {

	constructor(protected subDistrictManager: SubDistrictManager, protected toastService: ToastService, 
  			  protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService, 
  			  protected router: Router,public commonUtil:CommonUtil ) {
    	super(subDistrictManager, commonService, toastService, loadingService, router);
  	}

	ngOnInit() {
		this.request.loadEditPage = false;
		this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
		this.records = new Array<SubDistrict>();
		this.init();
	}

	onItemSelection(record: any) {
		this.onAssociatedValueSelected(record);
	}

	onCancel() {
		this.request.loadEditPage = false;
		if (!isNullOrUndefined(this.dtElement.dtInstance)) {
			this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.destroy();
			});
		}
		this.init();
	}

	onNewRecord() {
	   if (!this.isPlusButton) {
		  if (this.filterParam){
		      this.router.navigate(['/dashboard/sub-district/edit/0'], { queryParams: { [this.filterParam.relationTable]: this.filterParam.relationId } });
		  } else {
		      this.router.navigate(['/dashboard/sub-district/edit/0']);
		  }
	      return;
	    }
	    this.request.loadEditPage = true;
	}

	removeSuccess() {
		this.onCancel();
	}
	
  	
	ngOnDestroy() {
		this.clean();
	}
	
	loadDetailPage(recordId) {
		this.selectedId = recordId;
		setTimeout(() => {
			$('#subDistrictDetailPage').appendTo('body').modal('show');
			$('#subDistrictDetailPage').on('hidden.bs.modal', () => {
				setTimeout(() => {
					this.selectedId = undefined;
				});
			});
		}, 500);
	}
}
