import { Injectable } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { RestResponse } from 'src/app/shared/auth.model';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseManager } from '../../config/base.manager';
import { SubDistrictService } from './subdistrict.service';

@Injectable({
  providedIn: 'root'
})
export class SubDistrictManager extends BaseManager {

  constructor(private subDistrictService: SubDistrictService, protected loadingService: LoadingService, protected toastService: ToastService) {
    super(subDistrictService, loadingService, toastService);
  }

  getSubDistricts(districtId) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.subDistrictService.fetchAll({ relationTable: 'DISTRICT', relationId: districtId });
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }
}
