import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../config/base.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationService extends BaseService {

    constructor(public http: HttpClient) {
        super(http, '/api/notification', '/api/notifications');
    }
}

