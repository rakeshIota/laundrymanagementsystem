import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { NotificationService } from './notification.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationManager extends BaseManager {

    constructor(private notificationService: NotificationService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(notificationService, loadingService, toastService);
    }
}
