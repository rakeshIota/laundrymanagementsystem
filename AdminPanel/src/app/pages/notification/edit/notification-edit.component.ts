import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { Notification } from '../../../models/notification';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { NotificationManager } from '../notification.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-notification-edit',
  templateUrl: './notification-edit.component.html',
  styleUrls: ['./notification-edit.component.scss']
})

export class NotificationEditComponent extends BaseEditComponent implements OnInit {
  public notification: Notification;
  
  constructor(protected route: ActivatedRoute, protected notificationManager: NotificationManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(notificationManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.notification = new Notification();
  	this.notification.isActive=true;   
    this.setRecord(this.notification);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.notification = Notification.fromResponse(this.record);
    this.setRecord(this.notification);
  }

  
  
  async fetchAssociatedData() {
    this.afterFetchAssociatedCompleted();
  }
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/notification');
  }	
	  
	
	checkConditionToReload(records: BaseModel[], selectedRecord: any){
		if (!records.some(x => x.id === selectedRecord.id)) {
			this.fetchAssociatedData();
		}
	}
	
	onAssociatedValueSelected(selectedRecord: any, selectedField: any) {	
	if(this.request.popupId){
		$('#'+this.request.popupId).appendTo('body').modal('hide');
	}
  	
	 }
}
