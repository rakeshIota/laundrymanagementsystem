import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BaseDetailComponent } from '../../../config/base.detail.component';
import { LoadingService } from '../../../services/loading.service';
import { AuthService } from '../../../shared/auth.services';
import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { PromotionManager } from '../promotion.manager';
import { Promotion } from 'src/app/models/promotion';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.component.html',
  styleUrls: ['./promotion-detail.component.scss']
})
export class PromotionDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected promotionManager: PromotionManager, protected toastService: ToastService,
    protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService,
    protected translateService: TranslateService) {
    super(promotionManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new Promotion();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'Promotion';
    this.filterParam.relationId = this.record.id;
  }

}
