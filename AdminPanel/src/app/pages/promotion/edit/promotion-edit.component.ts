import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { FileLikeObject, FileUploader } from 'ng2-file-upload';
import { Attachment } from 'src/app/models/attachment';
import { Promotion } from 'src/app/models/promotion';
import { RestResponse } from 'src/app/shared/auth.model';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { LoadingService } from '../../../services/loading.service';
import { AuthService } from '../../../shared/auth.services';
import { CommonService } from '../../../shared/common.service';
import { CommonUtil } from '../../../shared/common.util';
import { ToastService } from '../../../shared/toast.service';
import { PromotionManager } from '../promotion.manager';
import * as lodash from 'lodash';

declare const $: any;

@Component({
  selector: 'app-promotion-edit',
  templateUrl: './promotion-edit.component.html',
  styleUrls: ['./promotion-edit.component.scss']
})

export class PromotionEditComponent implements OnInit {
  public promotion: Promotion;
  onClickValidation: boolean;
  recordId: string;
  attachmentUploader: FileUploader;
  promotionImage: Attachment;
  loading: boolean;

  constructor(private route: ActivatedRoute, private promotionManager: PromotionManager,
    private toastService: ToastService, private loadingService: LoadingService, private router: Router,
    private commonService: CommonService, public authService: AuthService, private translateService: TranslateService
    , public commonUtil: CommonUtil) {
  }

  ngOnInit() {
    this.recordId = this.route.snapshot.paramMap.get('id');
    this.promotion = new Promotion();
    this.onClickValidation = false;
    this.pluginIntialization();
    this.fetch();
  }


  pluginIntialization() {
    this.attachmentUploader = this.initializeUploader(this.promotion.attachments, 'png,jpg,jpeg', 2 * 1024, 1);
  }

  async fetch() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.promotionManager.fetch(this.recordId);
      this.loadingService.hide();
      if (!response.status) {
        return;
      }
      this.promotion = response.data;
      if (!isNullOrUndefined(this.promotion.file) && this.promotion.file.length > 0) {
        this.promotionImage = this.promotion.file[0];
      }
    } catch (error) {
      this.loadingService.hide();
    }
  }

  initializeUploader(files, allowedExtensions: string, maxFileSize: number, aspectRatio: number) {
    const uploaderOptions = {
      url: environment.BaseApiUrl + '/api/file/group/items/upload',
      autoUpload: true,
      maxFileSize: maxFileSize * 1024,
      filters: []
    };
    if (allowedExtensions !== '') {
      uploaderOptions.filters.push({
        name: 'extension',
        fn: (item: any): boolean => {
          const fileExtension = item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase();
          return allowedExtensions.indexOf(fileExtension) !== -1;
        }
      });
    }

    const uploader = new FileUploader(uploaderOptions);
    uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });
    uploader.onBeforeUploadItem = (item => {
      this.loading = true;
      this.loadingService.show();
    });
    uploader.onWhenAddingFileFailed = (item: FileLikeObject, filter: any, options: any) => {
      this.loading = false;
      this.loadingService.hide();
      switch (filter.name) {
        case 'fileSize':
          this.toastService.error('Image size to too large');
          break;
        case 'extension':
          this.toastService.error('Only Image file allowed');
          break;
        default:
          this.toastService.error('Unknown error');
      }
    };
    uploader.onErrorItem = (fileItem, response) => {
      this.loading = false;
      this.loadingService.hide();
    };
    uploader.onSuccessItem = (fileItem, response) => {
      this.loading = false;
      this.loadingService.hide();
      const uploadResponse = JSON.parse(response);
      if (uploadResponse.length > 0) {
        const image = uploadResponse[0];
        image.isDeleted = false;
        if (isNullOrUndefined(this.promotion.attachments)) {
          this.promotion.attachments = new Array<any>();
        }
        this.promotion.attachments.push(image);
        this.processImages();
      }
    };
    return uploader;
  }

  processImages() {
    this.promotionImage = this.promotion.attachments.find(x => !x.isDeleted);
  }

  onSaveSuccess() {
    this.router.navigate(['/dashboard/promotion']);
  }

  async saveRecord(isValid) {
    this.onClickValidation = !isValid;
    if (!isValid) {
      return;
    }
    try {
      this.loadingService.show();
      this.addNewImage();
      const response: RestResponse = await this.promotionManager.update(this.promotion);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.onSaveSuccess();
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  addNewImage() {
    if (!isNullOrUndefined(this.promotion.attachments) && this.promotion.attachments.length > 0) {
      const previousImage: Attachment = lodash.cloneDeep(this.promotion.file[0]);
      previousImage.isDeleted = true;
      this.promotion.file = new Array<any>();
      this.promotion.file.push(previousImage);
      this.promotion.file.push(this.promotion.attachments[0]);
    }
  }

  openFileUploader() {
    $('#attachment').click();
  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }
}
