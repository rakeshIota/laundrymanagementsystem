import { Injectable } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseManager } from '../../config/base.manager';
import { PromotionService } from './promotion.service';

@Injectable({
  providedIn: 'root'
})
export class PromotionManager extends BaseManager {

  constructor(private promotionService: PromotionService, protected loadingService: LoadingService, protected toastService: ToastService) {
    super(promotionService, loadingService, toastService);
  }
}
