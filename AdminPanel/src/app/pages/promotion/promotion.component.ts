import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Promotion } from 'src/app/models/promotion';
import { isNullOrUndefined } from 'util';
import { BaseListComponent } from '../../config/base.list.component';
import { LoadingService } from '../../services/loading.service';
import { AuthService } from '../../shared/auth.services';
import { CommonService } from '../../shared/common.service';
import { CommonUtil } from '../../shared/common.util';
import { ToastService } from '../../shared/toast.service';
import { PromotionManager } from './promotion.manager';

declare const $: any;

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class PromotionComponent extends BaseListComponent implements OnInit, OnDestroy {

  constructor(protected promotionManager: PromotionManager, protected toastService: ToastService,
    protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService,
    protected router: Router, public commonUtil: CommonUtil) {
    super(promotionManager, commonService, toastService, loadingService, router);
  }

  ngOnInit() {
    this.request.loadEditPage = false;
    this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
    this.records = new Array<Promotion>();
    this.init();
  }

  onItemSelection(record: any) {
    this.onAssociatedValueSelected(record);
  }

  onCancel() {
    this.request.loadEditPage = false;
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  onNewRecord() {
    if (!this.isPlusButton) {
      if (this.filterParam) {
        this.router.navigate(['/dashboard/promotion/edit/0'], { queryParams: { [this.filterParam.relationTable]: this.filterParam.relationId } });
      } else {
        this.router.navigate(['/dashboard/promotion/edit/0']);
      }
      return;
    }
    this.request.loadEditPage = true;
  }

  removeSuccess() {
    this.onCancel();
  }

  goToLink(url: string) {
    if (url.includes('http')) {
      window.open(url, '_blank');
      return;
    }
    window.open('http://' + url, '_blank');
  }

  ngOnDestroy() {
    this.clean();
  }

  loadDetailPage(recordId) {
    this.selectedId = recordId;
    setTimeout(() => {
      $('#promotionDetailPage').appendTo('body').modal('show');
      $('#promotionDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }
}
