import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { PaymentLogService } from './paymentlog.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class PaymentLogManager extends BaseManager {

    constructor(private paymentLogService: PaymentLogService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(paymentLogService, loadingService, toastService);
    }
}
