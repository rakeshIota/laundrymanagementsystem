import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { PaymentLog } from '../../../models/paymentlog';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentLogManager } from '../paymentlog.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-paymentlog-detail',
  templateUrl: './paymentlog-detail.component.html',
  styleUrls: ['./paymentlog-detail.component.scss']
})
export class PaymentLogDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected paymentLogManager: PaymentLogManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(paymentLogManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new PaymentLog();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "PaymentLog";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
