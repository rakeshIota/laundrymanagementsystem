import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { UserDeviceService } from './userdevice.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class UserDeviceManager extends BaseManager {

    constructor(private userDeviceService: UserDeviceService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(userDeviceService, loadingService, toastService);
    }
}
