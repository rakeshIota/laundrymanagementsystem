import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { UserDevice } from '../../../models/userdevice';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDeviceManager } from '../userdevice.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-userdevice-detail',
  templateUrl: './userdevice-detail.component.html',
  styleUrls: ['./userdevice-detail.component.scss']
})
export class UserDeviceDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected userDeviceManager: UserDeviceManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(userDeviceManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new UserDevice();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "UserDevice";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
