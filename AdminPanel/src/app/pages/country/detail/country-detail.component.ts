import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {Country} from '../../../models/country';
import {ActivatedRoute, Router} from '@angular/router';
import {CountryManager} from '../country.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.scss']
})
export class CountryDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected countryManager: CountryManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil) {
    super(countryManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new Country();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'Country';
    this.filterParam.relationId = this.record.id;
  }
}
