import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { CountryService } from './country.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class CountryManager extends BaseManager {

    constructor(private countryService: CountryService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(countryService, loadingService, toastService);
    }
}
