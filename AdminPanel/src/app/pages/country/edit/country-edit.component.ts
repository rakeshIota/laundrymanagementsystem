import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';

import {BaseEditComponent} from '../../../config/base.edit.component';

import {Country} from '../../../models/country';
import {LoadingService} from '../../../services/loading.service';

import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {CountryManager} from '../country.manager';
import {CountryLanguageMapping} from '../../../models/countrylanguagemapping';
import {Language} from '../../../models/language';
import {LanguageManager} from '../../language/language.manager';

declare const $: any;

@Component({
  selector: 'app-country-edit',
  templateUrl: './country-edit.component.html',
  styleUrls: ['./country-edit.component.scss']
})

export class CountryEditComponent extends BaseEditComponent implements OnInit {
  country: Country;
  countryLanguage: CountryLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected countryManager: CountryManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              public commonUtil: CommonUtil, private languageManager: LanguageManager) {
    super(countryManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.country = new Country();
    this.country.isActive = true;
    this.setRecord(this.country);
    this.onClickMappingValidation = false;
    this.countryLanguage = new CountryLanguageMapping();
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
    this.country = Country.fromResponse(this.record);
    this.setRecord(this.country);
  }

  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.countryLanguage.languageId = this.languages[0].id;
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/country');
  }

  addCountryLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.country.countryLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.countryLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('Country Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.countryLanguage.languageId;
    });
    this.countryLanguage.languageName = language[0].name;
    this.country.countryLanguages.push(Object.assign({}, this.countryLanguage));
    this.countryLanguage = new CountryLanguageMapping();
    this.countryLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: CountryLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: CountryLanguageMapping) {
    record.isDeleted = true;
  }
}
