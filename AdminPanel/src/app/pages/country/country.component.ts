import { Component, OnDestroy, OnInit, Input, Output, ViewChild } from '@angular/core';
import { BaseListComponent } from '../../config/base.list.component';
import { LoadingService } from '../../services/loading.service';
import { AuthService } from '../../shared/auth.services';
import { CommonService } from '../../shared/common.service';
import { ToastService } from '../../shared/toast.service';
import { CountryManager } from './country.manager';
import { Country } from '../../models/country';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import {CommonUtil} from '../../shared/common.util';

declare const $: any;

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent extends BaseListComponent implements OnInit, OnDestroy {

	constructor(protected countryManager: CountryManager, protected toastService: ToastService, 
  			  protected loadingService: LoadingService, protected commonService: CommonService, public authService: AuthService, 
  			  protected router: Router,public commonUtil:CommonUtil ) {
    	super(countryManager, commonService, toastService, loadingService, router);
  	}

	ngOnInit() {
		this.request.loadEditPage = false;
		this.isPlusButton = !isNullOrUndefined(this.onAssociatedValueSelected);
		this.records = new Array<Country>();
		this.init();
	}

	onItemSelection(record: any) {
		this.onAssociatedValueSelected(record);
	}

	onCancel() {
		this.request.loadEditPage = false;
		if (!isNullOrUndefined(this.dtElement.dtInstance)) {
			this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.destroy();
			});
		}
		this.init();
	}

	onNewRecord() {
	   if (!this.isPlusButton) {
		  if (this.filterParam){
		      this.router.navigate(['/dashboard/country/edit/0'], { queryParams: { [this.filterParam.relationTable]: this.filterParam.relationId } });
		  } else {
		      this.router.navigate(['/dashboard/country/edit/0']);
		  }
	      return;
	    }
	    this.request.loadEditPage = true;
	}

	removeSuccess() {
		this.onCancel();
	}
	
  	
	ngOnDestroy() {
		this.clean();
	}
	
	loadDetailPage(recordId) {
		this.selectedId = recordId;
		setTimeout(() => {
			$('#countryDetailPage').appendTo('body').modal('show');
			$('#countryDetailPage').on('hidden.bs.modal', () => {
				setTimeout(() => {
					this.selectedId = undefined;
				});
			});
		}, 500);
	}
}
