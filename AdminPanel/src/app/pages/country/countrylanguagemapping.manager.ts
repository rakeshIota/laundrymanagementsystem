import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { CountryLanguageMappingService } from './countrylanguagemapping.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class CountryLanguageMappingManager extends BaseManager {

    constructor(private countryLanguageMappingService: CountryLanguageMappingService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(countryLanguageMappingService, loadingService, toastService);
    }
}
