import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { PackingService } from './packing.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class PackingManager extends BaseManager {

    constructor(private packingService: PackingService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(packingService, loadingService, toastService);
    }
}
