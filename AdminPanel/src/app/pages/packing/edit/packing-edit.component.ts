import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { Packing } from '../../../models/packing';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { PackingManager } from '../packing.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-packing-edit',
  templateUrl: './packing-edit.component.html',
  styleUrls: ['./packing-edit.component.scss']
})

export class PackingEditComponent extends BaseEditComponent implements OnInit {
  public packing: Packing;
  
  constructor(protected route: ActivatedRoute, protected packingManager: PackingManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(packingManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.packing = new Packing();
  	this.packing.isActive=true;   
    this.setRecord(this.packing);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.packing = Packing.fromResponse(this.record);
    this.setRecord(this.packing);
  }

  
  
  async fetchAssociatedData() {
    this.afterFetchAssociatedCompleted();
  }
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/packing');
  }	
	  
	
	checkConditionToReload(records: BaseModel[], selectedRecord: any){
		if (!records.some(x => x.id === selectedRecord.id)) {
			this.fetchAssociatedData();
		}
	}
	
	onAssociatedValueSelected(selectedRecord: any, selectedField: any) {	
	if(this.request.popupId){
		$('#'+this.request.popupId).appendTo('body').modal('hide');
	}
  	
	 }
}
