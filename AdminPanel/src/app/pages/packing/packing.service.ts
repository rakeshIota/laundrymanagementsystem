import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from '../../config/base.service';

@Injectable({
    providedIn: 'root'
})
export class PackingService extends BaseService {

    constructor(public http: HttpClient) {
        super(http, '/api/packing', '/api/packings');
    }
}

