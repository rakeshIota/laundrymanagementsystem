import { Component, OnInit } from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import { Packing } from '../../../models/packing';
import { ActivatedRoute, Router } from '@angular/router';
import { PackingManager } from '../packing.manager';
import { ToastService } from '../../../shared/toast.service';
import { LoadingService } from '../../../services/loading.service';
import { CommonService } from '../../../shared/common.service';
import { AuthService } from '../../../shared/auth.services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-packing-detail',
  templateUrl: './packing-detail.component.html',
  styleUrls: ['./packing-detail.component.scss']
})
export class PackingDetailComponent extends BaseDetailComponent implements OnInit {

	  constructor(protected route: ActivatedRoute, protected packingManager: PackingManager, protected toastService: ToastService,
	    		  protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService, public authService: AuthService, 
	    		  protected translateService: TranslateService ) {
	    	super(packingManager, commonService, toastService, loadingService, route, router, translateService);
	    	
	  }

	  ngOnInit() {
	  	this.record = new Packing();    
	    this.isDetailPage =  true;
	    this.init();
	  }
  
	  onFetchCompleted() { 
	    super.onFetchCompleted();
	    this.filterParam.relationTable = "Packing";
	    this.filterParam.relationId = this.record.id;
	  }
	  
}
