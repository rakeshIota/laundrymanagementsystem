import {HttpClient} from '@angular/common/http';
import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';
import {IMyDateModel} from 'mydatepicker';
import {Subject} from 'rxjs';
import {District} from 'src/app/models/district';
import {CustomerFilterOption, DataTablesResponse, OrderFilterOption} from 'src/app/models/order';
import {SubDistrict} from 'src/app/models/subdistrict';
import {RestResponse} from 'src/app/shared/auth.model';
import {environment} from 'src/environments/environment';
import {isNullOrUndefined} from 'util';
import {FilterParam} from '../../models/filterparam';
import {UserLocation, Users} from '../../models/users';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../shared/auth.services';
import {CommonService} from '../../shared/common.service';
import {CommonUtil} from '../../shared/common.util';
import {ToastService} from '../../shared/toast.service';
import {DistrictManager} from '../district/district.manager';
import {OrderManager} from '../order/order.manager';
import {SubDistrictManager} from '../subdistrict/subdistrict.manager';
import {UsersManager} from './users.manager';

declare const $: any;
declare const google: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy, AfterViewInit {
  filterParam: FilterParam;
  districts: District[];
  subDistricts: SubDistrict[];
  filterOption: OrderFilterOption;
  customerFilterOptions: CustomerFilterOption[];
  searchTerm: string;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtTrigger: Subject<any>;
  dtOptions: DataTables.Settings;
  hasDataLoad: boolean;
  records: Array<Users>;
  selectedId: string;
  request: any;
  showLocationModal: boolean;
  selectedCustomer: Users;
  showAdminNoteModal: boolean;
  selectedCustomerPreviousNote: string;
  pickerDate: IMyDateModel;
  authUser: Users;
  isPlusButton: boolean;

  constructor(protected usersManager: UsersManager, protected toastService: ToastService, public authService: AuthService,
              protected loadingService: LoadingService, protected commonService: CommonService, protected router: Router,
              public commonUtil: CommonUtil, private districtManager: DistrictManager, private subDistrictManager: SubDistrictManager,
              private http: HttpClient, private orderManager: OrderManager) {
    this.dtTrigger = new Subject();
  }

  ngOnInit() {
    this.init();
    this.isPlusButton = false; 
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 500);
  }

  init() {
    this.request = {} as any;
    this.filterParam = new FilterParam();
    this.selectedCustomer = new Users();
    this.showLocationModal = false;
    this.showAdminNoteModal = false;
    if (this.router.url === '/dashboard/users') {
      this.filterParam.relationTable = 'ADMIN';
      this.authUser = new Users();
      this.authUser = this.authService.getUser();
    } else if (this.router.url === '/dashboard/drivers') {
      this.filterParam.relationTable = 'DRIVER';
    } else if (this.router.url === '/dashboard/customers') {
      this.filterParam.relationTable = 'CUSTOMER';
      this.initializeCustomers();
    }
    this.fetchUsers();
  }

  onFetchCompleted() {
    this.records = this.records.sort((a: any, b: any) => {
      if (a.fullName < b.fullName) {
        return -1;
      }
      if (a.fullName > b.fullName) {
        return 1;
      }
      return 0;
    });
  }

  onCancel() {
    if (!isNullOrUndefined(this.dtElement.dtInstance)) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    this.init();
  }

  removeSuccess() {
    this.onCancel();
  }

  loadDetailPage(id) {
    this.selectedId = id;
    setTimeout(() => {
      $('#pluginDetailPage').appendTo('body').modal('show');
      $('#pluginDetailPage').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.selectedId = undefined;
        });
      });
    }, 500);
  }

  hasMoreAdmins(record: any) {
    if (record.roleName !== 'ROLE_ADMIN') {
      return true;
    }
    if (isNullOrUndefined(this.records)) {
      return true;
    }
    return record.adminCount > 1;
  }

  //#region CUSTOMERS
  async initializeCustomers() {
    this.filterOption = new OrderFilterOption();
    this.districts = new Array<District>();
    this.subDistricts = new Array<SubDistrict>();
    this.customerFilterOptions = Array<CustomerFilterOption>();
    this.customerFilterOptions = await this.orderManager.getFilterOptions();
    await this.getDistricts();
  }

  fetchUsers() {
    this.dtOptions = {
      order: [],
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: this.router.url === '/dashboard/users' || this.router.url === '/dashboard/drivers' || false,
      responsive: false,
      autoWidth: false,
      columns: [
        {name: 'fullName', data: 0},
        {name: 'nickName', data: 1},
        {data: 2, orderable: false},
        {data: 3, orderable: false},
        {data: 4, orderable: false},
        {data: 5, orderable: false},
        {name: 'lastOrderDate', data: 6},
      ],
      ajax: (dataTablesParameters: any, dataTableCallback) => {
        this.fetchRecords(dataTablesParameters, dataTableCallback);
        $('#fetchRecords').on('click', event => {
          this.fetchRecords(dataTablesParameters, dataTableCallback);
        });
      },
    };
  }

  async fetchRecords(dataTablesParameters, callback) {
    this.loadingService.show();
    await this.createParams(dataTablesParameters);
    await this.checkforFilters(dataTablesParameters);
    dataTablesParameters.relationTable = this.filterParam.relationTable;
    this.http.post<DataTablesResponse>(environment.BaseApiUrl + '/api/account/users', dataTablesParameters, {}).subscribe(resp => {
      if (!isNullOrUndefined(resp.data)) {
        this.records = resp.data;
        resp.recordsFiltered = resp.recordsTotal = resp.data.length > 0 ? resp.data[0].totalCount : 0;
        this.loadingService.hide();
        callback({recordsTotal: resp.recordsFiltered, recordsFiltered: resp.recordsFiltered, data: []});
      }
    });
  }

  async createParams(dataTablesParameters) {
    dataTablesParameters.offset = (dataTablesParameters.start / dataTablesParameters.length) + 1;
    dataTablesParameters.next = dataTablesParameters.length;
    dataTablesParameters.order.forEach(element => {
      dataTablesParameters.filterType = element.dir.toUpperCase();
      switch (element.column) {
        case 0:
          dataTablesParameters.filterOn = 'NAME';
          break;
        case 1:
          dataTablesParameters.filterOn = 'NICK_NAME';
          break;
        case 6:
          dataTablesParameters.filterOn = 'LAST_ORDER_DATE';
          break;
        default:
          dataTablesParameters.filterType = null;
          dataTablesParameters.filterOn = null;
          break;
      }
    });
    if (this.dtOptions.searching) {
      dataTablesParameters.searchText = dataTablesParameters.search.value.length > 0 ? dataTablesParameters.search.value.trim() : null;
    } else {
      dataTablesParameters.searchText = null;
    }
  }

  async getDistricts() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.districtManager.getDistricts();
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.districts = response.data;
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  async getSubDistricts(districtId) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.subDistrictManager.getSubDistricts(districtId);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.subDistricts = response.data;
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  async selectFilter(filterType: string) {
    switch (filterType) {
      case 'DISTRICT':
        if (!isNullOrUndefined(this.filterOption.districtId)) {
          await this.getSubDistricts(this.filterOption.districtId);
          this.rerender();
          return;
        }
        this.filterOption.subDistrictId = null;
        this.rerender();
        break;
      case 'SUB_DISTRICT':
        this.rerender();
        break;
      case 'CUSTOMER':
        if (isNullOrUndefined(this.filterOption.customerFilterType)) {
          this.searchTerm = undefined;
          this.pickerDate = undefined;
          this.rerender();
        }
        break;
    }
  }

  async checkforFilters(dataTablesParameters) {
    if (!isNullOrUndefined(this.filterOption)) {
      dataTablesParameters.district = this.filterOption.districtId;
      dataTablesParameters.subdistrict = this.filterOption.subDistrictId;
      dataTablesParameters.name = null;
      dataTablesParameters.email = null;
      dataTablesParameters.phoneNumber = null;
      dataTablesParameters.address = null;
      dataTablesParameters.registerDate = null;
      dataTablesParameters.lastOrderDate = null;
      if (!isNullOrUndefined(this.filterOption.customerFilterType)) {
        switch (this.filterOption.customerFilterType) {
          case 'FIRST_NAME':
            dataTablesParameters.firstName = this.searchTerm.trim();
            break;
          case 'LAST_NAME':
            dataTablesParameters.lastName = this.searchTerm.trim();
            break;
          case 'NICK_NAME':
            dataTablesParameters.nickName = this.searchTerm.trim();
            break;
          case 'EMAIL':
            dataTablesParameters.email = this.searchTerm.trim();
            break;
          case 'PHONE_NUMBER':
            dataTablesParameters.phoneNumber = this.searchTerm.trim();
            break;
          case 'ADDRESS':
            dataTablesParameters.address = this.searchTerm.trim();
            break;
          case 'REGISTERED_DATE':
            dataTablesParameters.registerDate = this.searchTerm.trim();
            break;
          case 'LAST_ORDER_DATE':
            dataTablesParameters.lastOrderDate = this.searchTerm.trim();
            break;
        }
      }
    }
    return dataTablesParameters;
  }

  //#endregion

  ngOnDestroy() {
    $('#pluginDetailPage').remove();
  }

  onFailureCallback() {
    $('#pluginDetailPage').appendTo('body').modal('hide');
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
      this.fetchUsers();
    });
  }

  search() {
    if (!isNullOrUndefined(this.searchTerm) && (this.searchTerm.trim() === '' || this.searchTerm.trim().length < 2)) {
      this.searchTerm = this.searchTerm.trim();
      return;
    }
    this.rerender();
  }

  remove(recordId: string) {
    this.commonService.confirmation('Would you like to delete?', this.removeCallback.bind(this), recordId, null);
  }

  async removeCallback(recordId: string) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.usersManager.remove(recordId);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      $('#fetchRecords').trigger('click');
    } catch (error) {
      this.toastService.error(error.message);
      this.loadingService.hide();
    }
  }

  async loadLocationMap(recordId: string) {
    try {
      const filterParam = new FilterParam();
      filterParam.relationId = recordId;
      filterParam.relationTable = 'User';
      this.loadingService.show();
      const response: RestResponse = await this.usersManager.getlocation(filterParam);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error('No location found for this driver!');
        return;
      }
      if (response.data.length === 0) {
        this.toastService.error('No location found for this driver!');
        return;
      }
      this.showLocation(response.data);
    } catch (error) {
      this.toastService.error('No location found for this driver!');
      this.loadingService.hide();
      return;
    }
  }

  showLocation(locations: UserLocation[]) {
    this.showLocationModal = true;
    setTimeout(() => {
      $('#locationMap').appendTo('body').modal('show');
      this.loadMap(locations);
      $('#locationMap').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showLocationModal = false;
        });
      });
    }, 500);
  }

  loadMap(locations: UserLocation[]) {
    const map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(15.2529179, 99.9488587),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoomControl: false,
      scaleControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      fullscreenControl: false
    });
    const infowindow = new google.maps.InfoWindow();
    if (locations.length > 0) {
      map.center = new google.maps.LatLng(locations[0].longitude, locations[0].latitude);
    }
    locations.forEach((location, index) => {
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.longitude, location.latitude),
        map
      });
      google.maps.event.addListener(marker, 'click', ((marker, i) => {
        return () => {
          infowindow.setContent(location.name);
          infowindow.open(map, marker);
        };
      })(marker, index));
      new google.maps.event.trigger(map, 'resize');
    });
  }

  changeNote(record: Users) {
    this.selectedCustomer = record;
    this.selectedCustomerPreviousNote = !isNullOrUndefined(record.adminNote) ? record.adminNote : undefined;
    this.showAdminNoteModal = true;
    setTimeout(() => {
      $('#adminNote').appendTo('body').modal('show');
      $('#adminNote').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showAdminNoteModal = false;
        });
      });
    }, 500);
  }

  async saveNote() {
    if (isNullOrUndefined(this.selectedCustomer.adminNote) || (!isNullOrUndefined(this.selectedCustomer.adminNote) && this.selectedCustomer.adminNote.trim() === '')) {
      this.toastService.error('Please enter notes!');
      return;
    }
    try {
      this.loadingService.show();
      const response: RestResponse = await this.usersManager.saveNotes({
        id: this.selectedCustomer.id,
        note: this.selectedCustomer.adminNote.trim()
      });
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      $('#adminNote').modal('hide');
      this.toastService.success(response.message);
      this.ngOnInit();
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  cancelAdminNote() {
    if (!isNullOrUndefined(this.selectedCustomerPreviousNote)) {
      this.selectedCustomer.adminNote = this.selectedCustomerPreviousNote;
    } else {
      this.selectedCustomer.adminNote = undefined;
    }
  }

  onPickerDateChanged(event: IMyDateModel) {
    this.searchTerm = `${event.date.month}/${event.date.day}/${event.date.year}`;
  }
}
