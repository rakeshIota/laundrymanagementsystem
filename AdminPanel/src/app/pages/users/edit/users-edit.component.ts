import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Users } from 'src/app/models/users';
import { UsersService } from 'src/app/services/users.service';
import { isNullOrUndefined } from 'util';
import { LoadingService } from '../../../services/loading.service';
import { RestResponse } from '../../../shared/auth.model';
import { CommonUtil } from '../../../shared/common.util';
import { ToastService } from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';

declare const $: any;

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss']
})
export class UsersEditComponent implements OnInit, AfterViewInit {

  user: Users;
  onClickValidation: boolean;
  roles: any[];
  request: any;
  positions: any[];
  emailPattern: string;
  constructor(private route: ActivatedRoute, private usersService: UsersService, private toastService: ToastService,
              private loadingService: LoadingService, private router: Router, public commonUtil: CommonUtil,
              private authService: AuthService) {
    this.user = new Users();
    this.request = {} as any;
    this.request.recordId = this.route.snapshot.paramMap.get('id');
    this.request.isNewRecord = true;
    this.request.onClickValidation = false;
    this.emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
  }

  async ngOnInit() {
    this.roles = new Array<any>();
    if(this.authService.hasRole(['ROLE_ADMIN'])){
      this.roles.push({ name: 'Admin', slug: 'ROLE_ADMIN' });
    }
    this.roles.push({ name: 'Operation Manager', slug: 'ROLE_OPERATION' });

    this.positions = new Array<any>();
    this.positions.push({ name: 'System Administrator', slug: 'ROLE_ADMIN' });
    this.positions.push({ name: 'Laundry Manager', slug: 'ROLE_OPERATION' });

    // await this.fetchRoles();
    if (this.request.recordId <= 0) {
      return;
    }
    await this.fetchUserDetail();
  }

  ngAfterViewInit(): void {
    $('[data-toggle="popover"]').popover({ html: true, trigger: 'hover' });
  }

  onRoleChange(role) {
    const selectedPostion = this.positions.find(x => x.slug === role);
    if (isNullOrUndefined(selectedPostion)) {
      return;
    }
    this.user.position = selectedPostion.name;
  }

  async fetchRoles() {
    try {
      const response: RestResponse = await this.usersService.GetAllRoles(null);
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.roles = response.data;
    } catch (e) {
      this.toastService.error(e.message);
    }
  }

  async fetchUserDetail() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.usersService.fetch(this.request.recordId).toPromise();
      if (!response.status) {
        this.loadingService.hide();
        this.toastService.error(response.message);
        return;
      }
      this.request.isNewRecord = false;
      this.user = response.data;
      this.user.password = 'A****@1**';
      this.user.confirmPassword = 'A****@1**';
      this.loadingService.hide();
    } catch (e) {
      this.toastService.error(e.message);
      this.loadingService.hide();
    }
  }

  save(valid) {
    if (!valid) {
      this.request.onClickValidation = true;
      return;
    }
    if (this.user.password !== this.user.confirmPassword) {
      this.toastService.error('Password does\'t match');
      return;
    }
    this.loadingService.show();
    const user = Object.assign({}, this.user);
    if (user.password === 'A****@1**') {
      user.confirmPassword = null;
      user.password = null;
    }
    this.request.isRequested = true;
    const method = this.request.isNewRecord ? 'save' : 'update';
    this.usersService[method](user)
      .then((resp) => {
        if (!resp.status) {
          this.loadingService.hide();
          this.toastService.error(resp.message);
          this.request.isRequested = false;
          return;
        }
        this.loadingService.hide();
        this.toastService.success(resp.message);
        this.router.navigateByUrl('/dashboard/users');
      }, (error) => {
        this.loadingService.hide();
        this.request.isRequested = false;
        this.toastService.error(error.message);
      });
  }
}
