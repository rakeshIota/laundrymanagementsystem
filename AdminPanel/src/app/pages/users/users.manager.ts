import { Injectable } from '@angular/core';
import { FilterParam } from 'src/app/models/filterparam';
import { LoadingService } from 'src/app/services/loading.service';
import { UsersService } from 'src/app/services/users.service';
import { RestResponse } from 'src/app/shared/auth.model';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseManager } from '../../config/base.manager';

@Injectable({
  providedIn: 'root'
})
export class UsersManager extends BaseManager {

  constructor(private usersService: UsersService, protected loadingService: LoadingService, protected toastService: ToastService) {
    super(usersService, loadingService, toastService);
  }

  fetchAllUsers(filterParam: FilterParam): Promise<RestResponse> {
    const promise = new Promise<RestResponse>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.usersService.fetchAllUsers(filterParam);

        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  fetchCustomer(userId: string): Promise<RestResponse> {
    const promise = new Promise<RestResponse>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.usersService.fetchCustomer(userId).toPromise();
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    }
    );
    return promise;
  }

  getlocation(filterParam: FilterParam) {
    const promise = new Promise<any>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.usersService.getDriverLocation(filterParam);
        if (!response.status) {
          resolve(response);
          return;
        }
        response.data = this.onFetchAllSuccess(response.data);
        resolve(response);
      } catch (error) {
        this.onFailure(error);
        reject(error);
      }
    });
    return promise;
  }

  saveNotes(data: any): Promise<RestResponse> {
    const promise = new Promise<RestResponse>(async (resolve, reject) => {
      try {
        const response: RestResponse = await this.usersService.saveNotes(data);
        if (!response.status) {
          resolve(response);
          return;
        }
        resolve(response);
      } catch (error) {
        reject(error);
      }
    }
    );
    return promise;
  }
}
