import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import * as lodash from 'lodash';
import {BaseDetailComponent} from 'src/app/config/base.detail.component';
import {Address} from 'src/app/models/address';
import {isNullOrUndefined} from 'util';
import {FilterParam} from '../../../models/filterparam';
import {Users} from '../../../models/users';
import {LoadingService} from '../../../services/loading.service';
import {RestResponse} from '../../../shared/auth.model';
import {AuthService} from '../../../shared/auth.services';
import {CommonService} from '../../../shared/common.service';
import {CommonUtil} from '../../../shared/common.util';
import {ToastService} from '../../../shared/toast.service';
import {UsersManager} from './../users.manager';

declare const $: any;

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent extends BaseDetailComponent implements OnInit {
  customer: Users;
  addressData: Address;
  showAdminNoteModal: boolean;
  previousCustomerNote: string;
  @Input()
  failure: () => void;

  constructor(protected route: ActivatedRoute, protected usersManager: UsersManager, protected toastService: ToastService,
              public authService: AuthService, protected loadingService: LoadingService, protected commonService: CommonService,
              protected router: Router, public commonUtil: CommonUtil, protected translateService: TranslateService) {
    super(usersManager, commonService, toastService, loadingService, route, router, translateService);
    this.filterParam = new FilterParam();
    this.record = new Users();
  }

  async ngOnInit() {
    this.isDetailPage = true;
    this.showAdminNoteModal = true;
    this.addressData = new Address();
    this.loadingService.show();
    this.customer = new Users();
    await this.fetchExistingCustomer();
    this.loadingService.hide();
  }

  async fetchExistingCustomer() {
    try {
      let recordId = this.recordId;
      if (isNullOrUndefined(recordId)) {
        recordId = this.route.snapshot.paramMap.get('id');
      }
      const response: RestResponse = await this.usersManager.fetchCustomer(recordId);
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.customer = response.data;
      if (!isNullOrUndefined(this.customer.addressdetails) && this.customer.addressdetails.length > 0) {
        this.addressData = this.customer.addressdetails.filter(x => !isNullOrUndefined(x.type)).find(x => x.type === 'PICKUP_DELIVERY');
      }
      this.previousCustomerNote = !isNullOrUndefined(this.customer.adminNote) ? lodash.cloneDeep(this.customer.adminNote) : undefined;
    } catch (error) {
      this.failure();
      this.toastService.error(error.message);
    }
  }

  changeNote() {
    this.showAdminNoteModal = true;
    setTimeout(() => {
      $('#adminNote').appendTo('body').modal('show');
      $('#adminNote').on('hidden.bs.modal', () => {
        setTimeout(() => {
          this.showAdminNoteModal = false;
        });
      });
    }, 500);
  }

  async saveNote() {
    if (isNullOrUndefined(this.customer.adminNote) || (!isNullOrUndefined(this.customer.adminNote) && this.customer.adminNote.trim() === '')) {
      this.toastService.error('Please enter notes!');
      return;
    }
    try {
      this.loadingService.show();
      const response: RestResponse = await this.usersManager.saveNotes({id: this.customer.id, note: this.customer.adminNote.trim()});
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      $('#adminNote').modal('hide');
      this.toastService.success(response.message);
      this.ngOnInit();
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  cancelAdminNote() {
    if (!isNullOrUndefined(this.previousCustomerNote)) {
      this.customer.adminNote = this.previousCustomerNote;
    } else {
      this.customer.adminNote = undefined;
    }
  }
}
