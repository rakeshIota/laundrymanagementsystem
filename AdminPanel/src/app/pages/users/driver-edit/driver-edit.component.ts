import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {IMyDpOptions} from 'mydatepicker';
import {FileLikeObject, FileUploader} from 'ng2-file-upload';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../environments/environment';
import {Constant} from '../../../config/constants';
import {Attachment} from '../../../models/attachment';
import {Users} from '../../../models/users';
import {LoadingService} from '../../../services/loading.service';
import {UsersService} from '../../../services/users.service';
import {RestResponse} from '../../../shared/auth.model';
import {CommonService} from '../../../shared/common.service';
import {CommonUtil} from '../../../shared/common.util';
import {ToastService} from '../../../shared/toast.service';

declare const $: any;

@Component({
  selector: 'app-driver-edit',
  templateUrl: './driver-edit.component.html',
  styleUrls: ['./driver-edit.component.scss']
})
export class DriverEditComponent implements OnInit, AfterViewInit {
  myDatePickerOptions: IMyDpOptions;
  driver: Users;
  onClickValidation: boolean;
  status: any[];
  request: any;
  attachmentUploader: FileUploader;
  selectedType: string;
  profilePic: Attachment;
  idCardFront: Attachment;
  idCardBack: Attachment;
  driverLicenceBack: Attachment;
  driverLicenceFront: Attachment;
  loading: boolean;
  nationalIdMast: string;
  emailPattern: string;

  constructor(private route: ActivatedRoute, private usersService: UsersService, private toastService: ToastService,
              private loadingService: LoadingService, private router: Router, public commonUtil: CommonUtil,
              private commonService: CommonService) {
    this.driver = new Users();
    this.driver.status = 'Active';
    this.request = {} as any;
    this.request.recordId = this.route.snapshot.paramMap.get('id');
    this.request.isNewRecord = true;
    this.request.onClickValidation = false;
    this.nationalIdMast = '0000000000000';
    this.emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
  }

  async ngOnInit() {
    const date = new Date();
    this.myDatePickerOptions = {
      dateFormat: Constant.MY_DATE_PICKER.DATE_TYPE,
      disableSince: {year: date.getFullYear(), month: (date.getMonth() + 1), day: date.getDate()},
    };
    this.status = new Array<any>();
    this.status.push({name: 'Active', slug: 'Active'});
    this.status.push({name: 'In-Active', slug: 'Inactive'});
    this.pluginIntialization();
    if (this.request.recordId <= 0) {
      return;
    }
    await this.fetchUserDetail();
  }

  ngAfterViewInit(): void {
    $('[data-toggle="popover"]').popover({html: true, trigger: 'hover'});
  }

  async fetchUserDetail() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.usersService.fetchDriver(this.request.recordId).toPromise();
      if (!response.status) {
        this.loadingService.hide();
        this.toastService.error(response.message);
        return;
      }
      this.request.isNewRecord = false;
      this.driver = response.data;
      this.driver.password = 'A****@1**';
      this.driver.confirmPassword = 'A****@1**';
      this.driver.dobCalendar = {date: moment(this.driver.DOB).format('DD/MM/YYYY'), jsdate: new Date(this.driver.DOB)};
      this.loadingService.hide();
      this.processImages();
    } catch (e) {
      this.loadingService.hide();
      this.toastService.error(e.message);
    }
  }

  pluginIntialization() {
    this.attachmentUploader = this.initializeUploader(this.driver.attachments, 'png,jpg,jpeg', 2 * 1024, 1);
  }

  initializeUploader(files, allowedExtensions: string, maxFileSize: number, aspectRatio: number) {
    const uploaderOptions = {
      url: environment.BaseApiUrl + '/api/file/group/items/upload',
      autoUpload: true,
      maxFileSize: maxFileSize * 1024,
      filters: []
    };
    if (allowedExtensions !== '') {
      uploaderOptions.filters.push({
        name: 'extension',
        fn: (item: any): boolean => {
          const fileExtension = item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase();
          return allowedExtensions.indexOf(fileExtension) !== -1;
        }
      });
    }

    const uploader = new FileUploader(uploaderOptions);
    uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });
    uploader.onBeforeUploadItem = (item => {
      this.loading = true;
      this.loadingService.show();
    });
    uploader.onWhenAddingFileFailed = (item: FileLikeObject, filter: any, options: any) => {
      this.loading = false;
      this.loadingService.hide();
      switch (filter.name) {
        case 'fileSize':
          this.toastService.error('Image size to too large');
          break;
        case 'extension':
          this.toastService.error('Only Image file allowed');
          break;
        default:
          this.toastService.error('Unknown error');
      }
    };
    uploader.onErrorItem = (fileItem, response) => {
      this.loading = false;
      this.loadingService.hide();
    };
    uploader.onSuccessItem = (fileItem, response) => {
      this.loading = false;
      this.loadingService.hide();
      const uploadResponse = JSON.parse(response);
      if (uploadResponse.length > 0) {
        const image = uploadResponse[0];
        image.isDeleted = false;
        if (isNullOrUndefined(this.driver.attachments)) {
          this.driver.attachments = new Array<any>();
        }
        image.type = this.selectedType;
        this.driver.attachments.push(image);
        this.processImages();
      }
    };
    return uploader;
  }

  removeFile(file) {
    this.commonService.confirmation('Would you like to delete?', this.removeFileCallback.bind(this), file);
  }

  removeFileCallback(file) {
    file.isDeleted = true;
    this.processImages();
  }

  processImages() {
    this.profilePic = this.driver.attachments.find(x => x.type === 'PROFILE_PIC' && !x.isDeleted);
    this.idCardFront = this.driver.attachments.find(x => x.type === 'ID_CARD_FRONT' && !x.isDeleted);
    this.idCardBack = this.driver.attachments.find(x => x.type === 'ID_CARD_BACK' && !x.isDeleted);
    this.driverLicenceFront = this.driver.attachments.find(x => x.type === 'DRIVER_LICENSE_FRONT' && !x.isDeleted);
    this.driverLicenceBack = this.driver.attachments.find(x => x.type === 'DRIVER_LICENSE_BACK' && !x.isDeleted);
  }

  openFileUploader(type) {
    this.selectedType = type;
    $('#attachment').click();
  }

  save(bol) {
    this.request.onClickValidation = !bol;
    if (!bol) {
      return;
    }
    if (this.driver.password !== this.driver.confirmPassword) {
      this.toastService.error('Password does\'t match');
      return;
    }
    this.processImages();
    if (isNullOrUndefined(this.profilePic)) {
      this.toastService.error('Profile Pic is required');
      return;
    }
    if (isNullOrUndefined(this.idCardFront)) {
      this.toastService.error('IdCard Front is required');
      return;
    }
    if (isNullOrUndefined(this.idCardBack)) {
      this.toastService.error('IdCard Back is required');
      return;
    }
    if (isNullOrUndefined(this.driverLicenceFront)) {
      this.toastService.error('DriverLicence Front is required');
      return;
    }
    if (isNullOrUndefined(this.driverLicenceBack)) {
      this.toastService.error('DriverLicence Back is required');
      return;
    }
    if (this.driver.password === 'A****@1**') {
      this.driver.password = null;
      this.driver.confirmPassword = null;
    }
    if (this.driver.status === 'Inactive') {
      this.driver.isActive = false;
    }
    if (this.driver.status === 'Active') {
      this.driver.isActive = true;
    }
    this.loadingService.show();
    const driver = Object.assign({}, this.driver);
    driver.dob = this.convertCalToDate(this.driver.dobCalendar);
    this.request.isRequested = true;
    driver.roleName = 'ROLE_DRIVER';
    const method = this.request.isNewRecord ? 'save' : 'update';
    this.usersService[method](driver)
      .then((resp) => {
        this.loadingService.hide();
        if (!resp.status) {
          this.toastService.error(resp.message);
          this.request.isRequested = false;
          return;
        }
        this.toastService.success(resp.message);
        this.router.navigateByUrl('/dashboard/drivers');
      }, (error) => {
        this.loadingService.hide();
        this.request.isRequested = false;
        this.toastService.error(error.message);
      });
  }

  convertCalToDate(val: any): Date {
    const date: Date = isNullOrUndefined(val) ? val : val.jsdate;
    date.setHours(12);
    date.setMinutes(0);
    date.setSeconds(0);
    return date;
  }
}
