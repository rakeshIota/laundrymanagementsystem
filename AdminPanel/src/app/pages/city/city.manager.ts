import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { CityService } from './city.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class CityManager extends BaseManager {

    constructor(private cityService: CityService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(cityService, loadingService, toastService);
    }
}
