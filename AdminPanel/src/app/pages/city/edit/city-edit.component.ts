import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {BaseEditComponent} from '../../../config/base.edit.component';
import {City} from '../../../models/city';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {ToastService} from '../../../shared/toast.service';
import {AuthService} from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import {CityManager} from '../city.manager';


import {StateManager} from '../../state/state.manager';
import {State} from '../../../models/state';
import {Language} from '../../../models/language';
import {CityLanguageMapping} from '../../../models/citylanguagemapping';
import {LanguageManager} from '../../language/language.manager';

declare const $: any;

@Component({
  selector: 'app-city-edit',
  templateUrl: './city-edit.component.html',
  styleUrls: ['./city-edit.component.scss']
})

export class CityEditComponent extends BaseEditComponent implements OnInit {
  city: City;
  states: State[];
  cityLanguage: CityLanguageMapping;
  onClickMappingValidation: boolean;
  languages: Language[];

  constructor(protected route: ActivatedRoute, protected cityManager: CityManager,
              protected toastService: ToastService, protected loadingService: LoadingService, protected router: Router,
              protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService,
              private stateManager: StateManager, public commonUtil: CommonUtil, private languageManager: LanguageManager) {
    super(cityManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
    this.city = new City();
    this.setRecord(this.city);
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.states = new Array<State>();
    this.onClickMappingValidation = false;
    this.cityLanguage = new CityLanguageMapping();
    this.init();
  }

  onFetchCompleted() {
    this.city = City.fromResponse(this.record);
    this.setRecord(this.city);
  }


  async fetchAssociatedData() {
    this.languages = await this.languageManager.fetchAllData(null);
    this.states = await this.stateManager.fetchAllData(null);
    this.cityLanguage.languageId = this.languages[0].id;
  }

  onSaveSuccess(data: any) {
    this.navigate('/dashboard/city');
  }

  addCityLanguage(valid: boolean) {
    this.onClickMappingValidation = !valid;
    if (!valid) {
      return;
    }
    const hasAlreadyAdded = this.city.cityLanguages.filter((obj) => {
      return !obj.isDeleted && obj.languageId === this.cityLanguage.languageId;
    });
    if (hasAlreadyAdded.length > 0) {
      this.toastService.error('City Already added');
      return;
    }
    const language = this.languages.filter((obj) => {
      return obj.id === this.cityLanguage.languageId;
    });
    this.cityLanguage.languageName = language[0].name;
    this.city.cityLanguages.push(Object.assign({}, this.cityLanguage));
    this.cityLanguage = new CityLanguageMapping();
    this.cityLanguage.languageId = this.languages[0].id;
  }

  removeLanguage(record: CityLanguageMapping) {
    this.commonService.confirmation('Would you like to delete?', this.removeLanguageCallback.bind(this), record);
  }

  removeLanguageCallback(record: CityLanguageMapping) {
    record.isDeleted = true;
  }
}
