import {Component, OnInit} from '@angular/core';
import {BaseDetailComponent} from '../../../config/base.detail.component';
import {AppSetting} from '../../../models/appsetting';
import {ActivatedRoute, Router} from '@angular/router';
import {AppSettingManager} from '../appsetting.manager';
import {ToastService} from '../../../shared/toast.service';
import {LoadingService} from '../../../services/loading.service';
import {CommonService} from '../../../shared/common.service';
import {AuthService} from '../../../shared/auth.services';
import {TranslateService} from '@ngx-translate/core';
import {CommonUtil} from '../../../shared/common.util';

@Component({
  selector: 'app-appsetting-detail',
  templateUrl: './appsetting-detail.component.html',
  styleUrls: ['./appsetting-detail.component.scss']
})
export class AppSettingDetailComponent extends BaseDetailComponent implements OnInit {

  constructor(protected route: ActivatedRoute, protected appSettingManager: AppSettingManager, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router, protected commonService: CommonService,
              public authService: AuthService, protected translateService: TranslateService, public commonUtil: CommonUtil) {
    super(appSettingManager, commonService, toastService, loadingService, route, router, translateService);

  }

  ngOnInit() {
    this.record = new AppSetting();
    this.isDetailPage = true;
    this.init();
  }

  onFetchCompleted() {
    super.onFetchCompleted();
    this.filterParam.relationTable = 'AppSetting';
    this.filterParam.relationId = this.record.id;
  }

}
