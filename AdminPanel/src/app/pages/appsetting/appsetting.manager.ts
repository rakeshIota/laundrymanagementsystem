import { Injectable } from '@angular/core';
import { BaseManager } from '../../config/base.manager';
import { AppSettingService } from './appsetting.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/shared/toast.service';

@Injectable({
    providedIn: 'root'
})
export class AppSettingManager extends BaseManager {

    constructor(private appSettingService: AppSettingService, protected loadingService: LoadingService, protected toastService: ToastService) {
        super(appSettingService, loadingService, toastService);
    }
}
