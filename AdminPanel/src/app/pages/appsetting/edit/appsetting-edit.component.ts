import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';

import { BaseEditComponent } from '../../../config/base.edit.component';
import { BaseModel } from '../../../config/base.model';

import { AppSetting } from '../../../models/appsetting';
import { LoadingService } from '../../../services/loading.service';

import { CommonService } from '../../../shared/common.service';
import { ToastService } from '../../../shared/toast.service';
import { AuthService } from '../../../shared/auth.services';
import {CommonUtil} from '../../../shared/common.util';
import { AppSettingManager } from '../appsetting.manager';


import { CommonEventService } from '../../../shared/common.event.service';
declare const $: any;

@Component({
  selector: 'app-appsetting-edit',
  templateUrl: './appsetting-edit.component.html',
  styleUrls: ['./appsetting-edit.component.scss']
})

export class AppSettingEditComponent extends BaseEditComponent implements OnInit {
  public appSetting: AppSetting;
  
  constructor(protected route: ActivatedRoute, protected appsettingManager: AppSettingManager, 
  			  protected toastService: ToastService,protected loadingService: LoadingService, protected router: Router, 
  			  protected commonService: CommonService, public authService: AuthService, protected translateService: TranslateService 
  			  
  			  ,public commonUtil:CommonUtil ) {
    	super(appsettingManager, commonService, toastService, loadingService, route, router, translateService);
  }

  ngOnInit() {
  	this.appSetting = new AppSetting();
  	this.appSetting.isActive=true;   
    this.setRecord(this.appSetting);
	
     
    this.isPlusButton = !isNullOrUndefined(this.onCancel);
    this.init();
  }

  onFetchCompleted() {
  	this.appSetting = AppSetting.fromResponse(this.record);
    this.setRecord(this.appSetting);
  }

  
  
  
  afterFetchAssociatedCompleted() {
	}
  
  onSaveSuccess(data: any) {
  	this.navigate('/dashboard/app-setting');
  }	
	  
	
}
