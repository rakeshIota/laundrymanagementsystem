import {Routes} from '@angular/router';
import {AuthGuard} from './shared/auth.guard';
import {LoginComponent} from './pages/login/login.component';
import {ForbiddenComponent} from './pages/forbidden/forbidden.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {RecoverPasswordComponent} from './pages/recover-password/recover-password.component';
import {RegisterSuccessComponent} from './pages/register/success/register-success.component';
import {RegisterFailureComponent} from './pages/register/failure/register-failure.component';

export const ROUTES: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', canActivate: [AuthGuard], component: LoginComponent, data: {roles: ['ROLE_ANONYMOUS']}},
  {path: 'register/success', component: RegisterSuccessComponent},
  {path: 'register/failure', component: RegisterFailureComponent},
  {
    path: 'account/recover/:ucode',
    canActivate: [AuthGuard],
    component: RecoverPasswordComponent,
    data: {roles: ['ROLE_ANONYMOUS']}
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/layout/layout.module').then(m => m.LayoutModule),
    canActivate: [AuthGuard],
    data: {roles: ['ROLE_ADMIN', 'ROLE_OPERATION']}
  },
  {path: '403', component: ForbiddenComponent},
  {path: '404', component: NotFoundComponent},
  {path: '**', component: NotFoundComponent}
];
