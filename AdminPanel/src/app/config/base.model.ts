import {isNullOrUndefined} from 'util';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';

export abstract class BaseModel {
  id: string;
  createdOn: Date;
  updatedOn: Date;
  isDeleted: boolean;
  isActive: boolean;
  createdBy: number;
  updatedBy: number;

  constructor() {
    this.isDeleted = false;
    this.isActive = true;
  }

  isNullOrUndefinedAndEmpty(name: string) {
    return isNullOrUndefined(name) || name.trim() === '';
  }

  abstract isValidateRequest(form: any, toastService: ToastService, translate: TranslateService);

  abstract forRequest();

  trimMe(val: string) {
    return isNullOrUndefined(val) ? val : val.trim();
  }

  convertCalToDate(val: any) {
    return isNullOrUndefined(val) ? val : val.jsdate;
  }
}
