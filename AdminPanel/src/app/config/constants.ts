export const Constant = {
  MY_DATE_PICKER: {
    DATE_TYPE: 'dd/mm/yyyy'
  },
  VIEW_USER_MAPPING: {
    COMMON_ACCESS: {
      Dashboard: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      UserPage: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AccountSettingPage: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    FEEDBACK_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    USERDEVICE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PASSWORDLOG_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    MESSAGELOG_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    NOTIFICATION_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    APPSETTING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    COUNTRY_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    COUNTRYLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    LANGUAGE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    STATE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    STATELANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CITY_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CITYLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    USERADDRESS_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DISTRICT_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DISTRICTLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    SUBDISTRICT_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    SUBDISTRICTLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PROVINCE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PROVINCELANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    USERCORDINATE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    SERVICE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    SERVICELANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PRODUCT_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PRODUCTLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PRODUCTPRICE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DELIVERYTYPE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DELIVERYTYPELANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CART_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CARTITEM_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CARTITEMSERVICEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    ORDER_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      }
    },
    ORDERITEM_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN', 'ROLE_OPERATION'],
      }
    },
    ORDERITEMSERVICEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    ORDERLOG_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PAYMENTLOG_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PACKING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PACKINGLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DEFAULTMESSAGE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    DEFAULTMESSAGELANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CURRENCY_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    CURRENCYLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    POSTALCODE_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    NOTIFICATIONMASTER_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    NOTIFICATIONMASTERLANGUAGEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    NOTIFICATIONMASTERKEYVALUEMAPPING_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    PROMOTION_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      AddButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      EditButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DeleteButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      },
      DetailButton: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    },
    BROADCAST_ACCESS: {
      Page: {
        SHOW_TO_ROLE: ['ROLE_ADMIN'],
        ENABLED_FOR_ROLE: ['ROLE_ADMIN'],
      }
    }
  },
  BROADCAST: {
    CUSTOMER_TYPE: {
      ALL_CUSTOMERS: 'ALL_CUSTOMERS',
      TOURIST: 'TOURIST',
      RESIDENT: 'RESIDENT',
      WITHOUT_ORDER: 'WITHOUT_ORDER',
      WITH_POSTAL_CODES: 'WITH_POSTAL_CODES'
    },
    CUSTOMER_GENDER: {
      BOTH: 'BOTH',
      MALE: 'MALE',
      FEMALE: 'FEMALE'
    },
    CUSTOMER_ORDER_TYPE: {
      GREATER_THAN: 'GREATER_THAN',
      LESS_THAN: 'LESS_THAN',
      EQUAL_TO: 'EQUAL_TO'
    }
  }
};

