import {LoadingService} from '../services/loading.service';
import {RestResponse} from '../shared/auth.model';
import {CommonService} from '../shared/common.service';
import {ToastService} from '../shared/toast.service';
import {BaseManager} from './base.manager';
import {Router} from '@angular/router';

export class BaseComponent {

  request: any;

  constructor(protected manager: BaseManager, protected commonService: CommonService, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router) {
    this.request = {} as any;
  }

  init() {

  }

  remove(id: string, path?: string) {
    this.commonService.confirmation('Would you like to delete?', this.removeCallback.bind(this), id, path);
  }

  async removeCallback(id: string, path?: string) {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.manager.remove(id);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.toastService.success(response.message);
      this.removeSuccess();
      if (path) {
        this.router.navigateByUrl(path);
      }
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  isImage(mime) {
    return (mime.toLowerCase() === 'jpg' || mime.toLowerCase() === 'png' || mime.toLowerCase() === 'jpeg');
  }

  clean() {
  }

  removeSuccess() {
  }
}
