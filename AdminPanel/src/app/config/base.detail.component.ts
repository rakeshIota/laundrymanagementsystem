import {Input, ViewChild} from '@angular/core';
import {BaseComponent} from './base.component';
import {BaseModel} from './base.model';
import {BaseManager} from './base.manager';
import {CommonService} from '../shared/common.service';
import {ToastService} from '../shared/toast.service';
import {LoadingService} from '../services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {RestResponse} from '../shared/auth.model';
import {FilterParam} from '../models/filterparam';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {isNullOrUndefined} from 'util';

declare const $: any;

export class BaseDetailComponent extends BaseComponent {

  @Input()
  recordId: string;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: any;
  dtTrigger: Subject<any>;

  request: any;
  record: BaseModel;
  isDetailPage: boolean;
  tabViewed: any[];
  tabView: string;
  filterParam: FilterParam;

  constructor(protected manager: BaseManager, protected commonService: CommonService, protected toastService: ToastService,
              protected loadingService: LoadingService, protected route: ActivatedRoute, protected router: Router,
              protected translateService: TranslateService) {
    super(manager, commonService, toastService, loadingService, router);
  }

  async init() {
    this.request = {} as any;
    this.tabViewed = new Array<any>();
    this.dtOptions = {
      order: [],
      responsive: true
    };
    this.dtTrigger = new Subject();
    this.filterParam = new FilterParam();
    if (this.isDetailPage) {
      this.tabView = 'DETAIL';
      this.tabViewed[this.tabView] = true;
    }
    this.loadingService.show();
    await this.fetchExistingRecord();
    this.loadingService.hide();
  }

  async fetchExistingRecord() {
    try {
      let recordId = this.recordId;
      if (isNullOrUndefined(recordId)) {
        recordId = this.route.snapshot.paramMap.get('id');
      }
      const response: RestResponse = await this.manager.fetch(recordId);
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.record = response.data;
      this.onFetchCompleted();
    } catch (error) {
      this.toastService.error(error.message);
    }
  }

  onChangeTab(tabType: string) {
    this.tabViewed[tabType] = true;
    this.tabView = tabType;
  }

  onFetchCompleted() {
  }
}
