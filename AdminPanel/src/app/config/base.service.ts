import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResourceWithId, RestResponse} from '../shared/auth.model';
import {HttpServiceRequests} from '../shared/http.service';
import {FilterParam} from '../models/filterparam';

export abstract class BaseService extends HttpServiceRequests<IResourceWithId>{

    constructor(public http: HttpClient, private entityBaseUrl: string, private entityPluralUrl: string) {
        super(http);
    }

    fetchAll(filterParam: FilterParam): Promise<RestResponse> {
        return this.getRecords(this.entityPluralUrl, filterParam);
    }

    fetch(id: string): Observable<RestResponse> {
        return this.getRecord(this.entityBaseUrl + '/' + id);
    }

    save(data: any): Promise<RestResponse> {
        return this.saveRecord(this.entityBaseUrl, data);
    }

    update(data: any): Promise<RestResponse> {
        return this.updateRecord(this.entityBaseUrl, data);
    }

    remove(id: string): Promise<RestResponse> {
        return this.removeRecord(this.entityBaseUrl + '/' + id);
    }
}
