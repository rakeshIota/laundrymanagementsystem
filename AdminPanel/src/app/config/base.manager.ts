import { FilterParam } from '../models/filterparam';
import { LoadingService } from '../services/loading.service';
import { RestResponse } from '../shared/auth.model';
import { ToastService } from '../shared/toast.service';
import { BaseService } from './base.service';

export class BaseManager {

    constructor(private service: BaseService, protected loadingService: LoadingService, protected toastService: ToastService) {

    }

    async fetchAllData(filterParam: FilterParam) {
        try {
            const response: RestResponse = await this.fetchAll(filterParam);
            if (!response.status) {
                this.toastService.error(response.message);
                return new Array<any>();
            }
            return response.data;
        } catch (error) {
            this.toastService.error(error.message);
            return new Array<any>();
        }
    }

    fetchAll(filterParam: FilterParam): Promise<RestResponse> {
        const promise = new Promise<RestResponse>(async (resolve, reject) => {
            try {
                const response: RestResponse = await this.service.fetchAll(filterParam);
                if (!response.status) {
                    resolve(response);
                    return;
                }
                response.data = this.onFetchAllSuccess(response.data);
                resolve(response);
            } catch (error) {
                this.onFailure(error);
                reject(error);
            }
        });
        return promise;
    }

    fetch(id: string): Promise<RestResponse> {
        const promise = new Promise<RestResponse>(async (resolve, reject) => {
            try {
                const response: RestResponse = await this.service.fetch(id).toPromise();
                if (!response.status) {
                    resolve(response);
                    return;
                }
                response.data = this.onFetchSuccess(response.data);
                resolve(response);
            } catch (error) {
                this.onFailure(error);
                reject(error);
            }
        });
        return promise;
    }

    save(data: any): Promise<RestResponse> {
        const promise = new Promise<RestResponse>(async (resolve, reject) => {
            try {
                const response: RestResponse = await this.service.save(data);
                if (!response.status) {
                    resolve(response);
                    return;
                }
                response.data = this.onSaveSuccess(response.data);
                resolve(response);
            } catch (error) {
                this.onFailure(error);
                reject(error);
            }
        });
        return promise;
    }

    update(data: any): Promise<RestResponse> {
        const promise = new Promise<RestResponse>(async (resolve, reject) => {
            try {
                const response: RestResponse = await this.service.update(data);
                if (!response.status) {
                    resolve(response);
                    return;
                }
                response.data = this.onUpdateSuccess(response.data);
                resolve(response);
            } catch (error) {
                this.onFailure(error);
                reject(error);
            }
        });
        return promise;
    }

    remove(id: string): Promise<RestResponse> {
        const promise = new Promise<RestResponse>(async (resolve, reject) => {
            try {
                const response: RestResponse = await this.service.remove(id);
                if (!response.status) {
                    resolve(response);
                    return;
                }
                response.data = this.onDeleteSuccess(response.data);
                resolve(response);
            } catch (error) {
                this.onFailure(error);
                reject(error);
            }
        });
        return promise;
    }

    onFetchAllSuccess(data: any) {
        return data;
    }

    onFetchSuccess(data: any) {
        return data;
    }

    onSaveSuccess(data: any) {
        return data;
    }

    onUpdateSuccess(data: any) {
        return data;
    }

    onDeleteSuccess(data: any) {
        return data;
    }

    onFailure(error: any) {
    }
}
