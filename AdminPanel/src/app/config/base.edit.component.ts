import {Input} from '@angular/core';
import {BaseComponent} from './base.component';
import {BaseModel} from './base.model';
import {BaseManager} from './base.manager';
import {CommonService} from '../shared/common.service';
import {ToastService} from '../shared/toast.service';
import {LoadingService} from '../services/loading.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RestResponse} from '../shared/auth.model';
import {FileLikeObject, FileUploader} from 'ng2-file-upload';
import {environment} from '../../environments/environment';
import {isNullOrUndefined} from 'util';
import {FilterParam} from '../models/filterparam';
import {TranslateService} from '@ngx-translate/core';
import {Attachment} from '../models/attachment';
import {FileItem} from 'ng2-file-upload/file-upload/file-item.class';

declare const $: any;

export class BaseEditComponent extends BaseComponent {

  @Input()
  public onCancel: () => void;

  request: any;
  record: BaseModel;

  isPlusButton: boolean;
  onClickValidation: boolean;
  isShowAssociated: boolean;
  filterParam: FilterParam;

  constructor(protected manager: BaseManager, protected commonService: CommonService, protected toastService: ToastService,
              protected loadingService: LoadingService, protected route: ActivatedRoute, protected router: Router,
              protected translateService: TranslateService) {
    super(manager, commonService, toastService, loadingService, router);
    this.isShowAssociated = false;
    this.filterParam = new FilterParam();
    this.request = {} as any;
  }

  async init() {
    this.request.recordId = this.route.snapshot.paramMap.get('id');
    this.request.isNewRecord = true;
    this.fetchAssociatedData();
    this.loadingService.show();
    if (this.request.recordId <= 0) {
      this.loadingService.hide();
      this.pluginIntialization();
      return;
    }
    await this.fetchExistingRecord();
    this.loadingService.hide();
  }

  async fetchExistingRecord() {
    try {
      const response: RestResponse = await this.manager.fetch(this.request.recordId);
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.record = response.data;
      this.request.isNewRecord = false;
      this.onFetchCompleted();
    } catch (error) {
      this.toastService.error(error.message);
    }
  }

  async save(form: any) {
    this.onClickValidation = !form.valid;
    if (!form.valid) {
      return;
    }
    if (!this.record.isValidateRequest(form, this.toastService, this.translateService)) {
      return;
    }
    try {
      this.loadingService.show();
      const method = this.request.isNewRecord ? 'save' : 'update';
      const response: RestResponse = await this.manager[method](this.record.forRequest());
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.onSaveSuccess(response.data);
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  navigate(path?: string) {
    if (!this.isPlusButton && !isNullOrUndefined(path)) {
      this.router.navigateByUrl(path);
      return;
    }
    if (!this.isPlusButton && isNullOrUndefined(path)) {
      window.history.back();
      return;
    }
    this.onCancel();
  }


  initializeUploader(files, allowedExtensions: string, maxFileSize: number, aspectRatio: number, toastService: ToastService) {
    const uploaderOptions = {
      url: environment.BaseApiUrl + '/api/file/group/items/upload',
      autoUpload: true,
      maxFileSize: maxFileSize * 1024,
      filters: []
    };
    if (allowedExtensions !== '') {
      uploaderOptions.filters.push({
        name: 'extension',
        fn: (item: any): boolean => {
          const fileExtension = item.name.slice(item.name.lastIndexOf('.') + 1).toLowerCase();
          return allowedExtensions.indexOf(fileExtension) !== -1;
        }
      });
    }
    const uploader = new FileUploader(uploaderOptions);
    uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });

    uploader.onWhenAddingFileFailed = (item: FileLikeObject, filter: any, options: any) => {
      switch (filter.name) {
        case 'fileSize':
          toastService.error('Image size to too large');
          break;
        case 'extension':
          toastService.error('Only Image file allowed');
          break;
        default:
          toastService.error('Unknown error');
      }
    };
    uploader.onSuccessItem = (fileItem, response) => {
      const uploadResponse = JSON.parse(response);
      if (uploadResponse.length > 0) {
        const image = uploadResponse[0];
        image.isDeleted = false;
        if (isNullOrUndefined(files)) {
          files = [] as any[];
        }
        files.push(image);
        setTimeout(() => {
          this.onUploadSuccess();
        }, 200);
      }
    };
    return uploader;
  }

  hasFileUploaded(files: Array<Attachment>) {
    if (!files || files.length === 0) {
      return true;
    }
    const result = files.filter((val) => {
      return !val.isDeleted;

    });
    return result.length === 0;
  }

  loadAssociatedPopup(popupId) {
    this.request.isShowAssociated = true;
    this.request.popupId = popupId;
    $('#' + popupId).appendTo('body').modal('show');
  }

  removeFile(file) {
    this.commonService.confirmation('Would you like to delete?', this.removeFileCallback.bind(this), file);
  }

  removeFileCallback(file) {
    file.isDeleted = true;
  }

  setRecord(inputRecord: BaseModel) {
    this.record = inputRecord;
  }

  fetchAssociatedData() {
  }

  onSaveSuccess(data: any) {
  }

  pluginIntialization() {
  }

  onFetchCompleted() {
  }

  onUploadSuccess() {
  }
}
