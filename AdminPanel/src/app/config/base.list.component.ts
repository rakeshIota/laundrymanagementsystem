import {Input, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {LoadingService} from '../services/loading.service';
import {RestResponse} from '../shared/auth.model';
import {CommonService} from '../shared/common.service';
import {ToastService} from '../shared/toast.service';
import {BaseComponent} from './base.component';
import {BaseModel} from './base.model';
import {BaseManager} from './base.manager';
import {FilterParam} from '../models/filterparam';
import {Router} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';

export class BaseListComponent extends BaseComponent {

  @Input()
  filterParam: FilterParam;
  @Input()
  onAssociatedValueSelected: (data: any) => void;
  @Input()
  isDetailPage: boolean;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtTrigger: Subject<any>;
  dtOptions: DataTables.Settings;

  records: BaseModel[];
  selectedId: string;
  hasDataLoad: boolean;
  isPlusButton: boolean;

  constructor(protected manager: BaseManager, protected commonService: CommonService, protected toastService: ToastService,
              protected loadingService: LoadingService, protected router: Router) {
    super(manager, commonService, toastService, loadingService, router);
    this.dtTrigger = new Subject();
    this.hasDataLoad = false;
    this.dtOptions = {
      order: [],
      responsive: true,
      processing: false
    } as DataTables.Settings;
  }

  async init() {
    try {
      this.loadingService.show();
      const response: RestResponse = await this.manager.fetchAll(this.filterParam);
      this.loadingService.hide();
      if (!response.status) {
        this.toastService.error(response.message);
        return;
      }
      this.records = response.data;
      this.onFetchCompleted();
      setTimeout(() => {
        this.dtTrigger.next();
      }, 500);
    } catch (error) {
      this.loadingService.hide();
      this.toastService.error(error.message);
    }
  }

  clean() {
    super.clean();
    this.dtTrigger.unsubscribe();
  }

  onFetchCompleted() {
    this.hasDataLoad = true;
  }
}
