import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class Notification extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	userId: number;
			  	message: string;
			  	languageId: number;
			  	type: string;
			  	token: string;
			  	status: string;
			  	response: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : Notification {
		const obj = new Notification();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.message = data.message;
		obj.languageId = data.languageId;
		obj.type = data.type;
		obj.token = data.token;
		obj.status = data.status;
		obj.response = data.response;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.message = this.trimMe(this.message);
				this.type = this.trimMe(this.type);
				this.token = this.trimMe(this.token);
				this.status = this.trimMe(this.status);
				this.response = this.trimMe(this.response);
        return this;
    }
}
