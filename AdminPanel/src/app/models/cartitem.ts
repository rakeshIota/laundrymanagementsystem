import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Product } from './product';
export class CartItem extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	cartId: number;
			  	productIdDetail: Product;
			  	productId: string;
				quantity: number;
			  	price: number;
			  	totalPrice: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : CartItem {
		const obj = new CartItem();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.cartId = data.cartId;
	  	obj.productIdDetail = data.productIdDetail;
	  	obj.productId = data.productId;
		obj.quantity = data.quantity;
		obj.price = data.price;
		obj.totalPrice = data.totalPrice;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
        return this;
    }
}
