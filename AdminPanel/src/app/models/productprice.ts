import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {ValidationService} from '../shared/validation.service';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {Service} from './service';
import {Product} from './product';

export class ProductPrice extends BaseModel {

  tenantId: number;
  slug: string;
  serviceIdDetail: Service;
  serviceId: string;
  productIdDetail: Product;
  productId: string;
  price: number;
  serviceName: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
  }

  static fromResponse(data: any): ProductPrice {
    const obj = new ProductPrice();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.serviceIdDetail = data.serviceIdDetail;
    obj.serviceId = data.serviceId;
    obj.productIdDetail = data.productIdDetail;
    obj.productId = data.productId;
    obj.price = data.price;
    obj.serviceName = data.serviceName;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    return this;
  }
}
