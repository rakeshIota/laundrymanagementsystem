import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {ValidationService} from '../shared/validation.service';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {DistrictLanguageMapping} from './districtlanguagemapping';

export class District extends BaseModel {

  tenantId: number;
  slug: string;
  districtLanguages: DistrictLanguageMapping[];
  name: string;
  cityName: string;
  cityId: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.districtLanguages = new Array<DistrictLanguageMapping>();
  }

  static fromResponse(data: any): District {
    const obj = new District();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.cityId = data.cityId;
    obj.cityName = data.cityName;
    obj.name = data.name;
    obj.districtLanguages = data.districtLanguages;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.districtLanguages = this.districtLanguages.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    return this;
  }
}
