import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {ServiceLanguageMapping} from './servicelanguagemapping';
import {Attachment} from './attachment';

export class Service extends BaseModel {

  tenantId: number;
  slug: string;
  name: string;
  serviceLanguages: ServiceLanguageMapping[];
  image: Attachment[];
  description: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.serviceLanguages = new Array<ServiceLanguageMapping>();
    this.image = new Array<Attachment>();
  }

  static fromResponse(data: any): Service {
    const obj = new Service();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.name = data.name;
    obj.serviceLanguages = data.serviceLanguages;
    obj.image = data.image;
    obj.description = data.description;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    const image = this.image.filter((obj) => {
      return !obj.isDeleted;
    });
    if (image.length === 0) {
      toastService.error('please add icon');
      return false;
    }
    return true;
  }

  forRequest() {
    this.serviceLanguages = this.serviceLanguages.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    this.name = this.trimMe(this.name);
    this.description = this.trimMe(this.description);
    return this;
  }
}
