import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class UserAddress extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	userId: number;
			  	adressLine1: string;
			  	adressLine2: string;
			  	districtId: number;
			  	subDistrictId: number;
			  	zipCode: string;
			  	isDefault: boolean;
			  	tag: string;
			  	provinceId: number;
			  	latitude: number;
			  	longitude: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
			this.isDefault=false;
    }
    
   static fromResponse(data: any) : UserAddress {
		const obj = new UserAddress();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.adressLine1 = data.adressLine1;
		obj.adressLine2 = data.adressLine2;
		obj.districtId = data.districtId;
		obj.subDistrictId = data.subDistrictId;
		obj.zipCode = data.zipCode;
		obj.isDefault = data.isDefault;
		obj.tag = data.tag;
		obj.provinceId = data.provinceId;
		obj.latitude = data.latitude;
		obj.longitude = data.longitude;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.tag)) {
            form.controls.tag.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.adressLine1 = this.trimMe(this.adressLine1);
				this.adressLine2 = this.trimMe(this.adressLine2);
				this.zipCode = this.trimMe(this.zipCode);
				this.tag = this.trimMe(this.tag);
        return this;
    }
}
