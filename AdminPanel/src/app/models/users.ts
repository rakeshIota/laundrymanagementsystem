import {isNullOrUndefined} from 'util';
import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {Address} from './address';

export class Users extends BaseModel {
  emailConfirmed: boolean;
  phoneNumber: string;
  userName: string;
  email: string;
  fullName: string;
  firstName: string;
  lastName: string;
  isActive: boolean;
  uniqueCode: string;
  isDeleted: boolean;
  roles: Array<string>;
  isFacebookConnected: boolean;
  isGoogleConnected: boolean;
  roleName: string;
  company: any;
  employeeId: string;
  position: string;
  password: string;
  confirmPassword: string;
  status: string;
  nationalId: string;
  dob: Date;
  DOB: Date;
  licenseId: string;
  bikeInformation: string;
  licencePlate: string;
  attachments: Array<any>;
  dobCalendar: any;
  staffId: string;
  lastOrder: string;
  nickName: string;
  lastOrderDate: Date;
  addressdetails: Address[];
  location: UserLocation;
  adminNote: string;
  profileimageUrl: string;

  constructor() {
    super();
    this.roles = new Array<string>();
    this.attachments = new Array<any>();
  }

  isValidAccountSettingRequest(form: any) {
    if (isNullOrUndefined(this.firstName) || this.firstName.trim() === '') {
      form.controls.firstName.setErrors({invalid: true});
      return false;
    }
    if (isNullOrUndefined(this.lastName) || this.lastName.trim() === '') {
      form.controls.lastName.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  forRequest() {
    this.userName = this.userName.trim();
    this.email = this.email.trim();
    this.fullName = this.fullName.trim();
    this.firstName = this.firstName.trim();
    this.lastName = this.lastName.trim();
    if (!isNullOrUndefined(this.uniqueCode)) {
      this.uniqueCode = this.uniqueCode.trim();
    }
    if (!isNullOrUndefined(this.company)) {
      this.company = this.company.trim();
    }
    if (!isNullOrUndefined(this.employeeId)) {
      this.employeeId = this.employeeId.trim();
    }
    if (!isNullOrUndefined(this.position)) {
      this.position = this.position.trim();
    }
    return this;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  static fromResponse(data: any): Users {
    const users = new Users();
    users.id = data.id;
    users.emailConfirmed = data.emailConfirmed;
    users.phoneNumber = data.phoneNumber;
    users.userName = data.userName;
    users.email = data.email;
    users.fullName = data.fullName;
    users.firstName = data.firstName;
    users.lastName = data.lastName;
    users.isActive = data.isActive;
    users.uniqueCode = data.uniqueCode;
    users.isDeleted = data.isDeleted;
    users.roles = data.roles;
    users.isGoogleConnected = data.isGoogleConnected;
    users.isFacebookConnected = data.isFacebookConnected;
    users.company = data.company;
    users.employeeId = data.employeeId;
    users.position = data.position;
    users.status = data.status;
    users.nationalId = data.nationalId;
    users.dob = data.dob;
    users.DOB = data.DOB;
    users.licenseId = data.licenseId;
    users.bikeInformation = data.bikeInformation;
    users.licencePlate = data.licencePlate;
    return users;
  }
}

export class UserLocation {
  id: string;
  latitude: number;
  longitude: number;
  name: string;
  userId: string;
}
