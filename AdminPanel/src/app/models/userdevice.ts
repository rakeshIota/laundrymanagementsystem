import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class UserDevice extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	userId: number;
			  	deviceId: string;
			  	notificationToken: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : UserDevice {
		const obj = new UserDevice();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.deviceId = data.deviceId;
		obj.notificationToken = data.notificationToken;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.deviceId = this.trimMe(this.deviceId);
				this.notificationToken = this.trimMe(this.notificationToken);
        return this;
    }
}
