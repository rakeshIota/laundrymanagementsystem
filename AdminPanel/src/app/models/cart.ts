import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class Cart extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	isLocked: boolean;
			  	customerId: number;
			  	status: string;
			  	tax: number;
			  	subTotal: number;
			  	totalPrice: number;
				totalItems: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
			this.isLocked=false;
    }
    
   static fromResponse(data: any) : Cart {
		const obj = new Cart();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.isLocked = data.isLocked;
		obj.customerId = data.customerId;
		obj.status = data.status;
		obj.tax = data.tax;
		obj.subTotal = data.subTotal;
		obj.totalPrice = data.totalPrice;
		obj.totalItems = data.totalItems;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.status)) {
            form.controls.status.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.status = this.trimMe(this.status);
        return this;
    }
}
