import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {Language} from './language';

export class CityLanguageMapping extends BaseModel {

  tenantId: number;
  slug: string;
  name: string;
  languageIdDetail: Language;
  languageId: string;
  cityId: number;
  languageName: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
  }

  static fromResponse(data: any): CityLanguageMapping {
    const obj = new CityLanguageMapping();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.name = data.name;
    obj.languageIdDetail = data.languageIdDetail;
    obj.languageId = data.languageId;
    obj.cityId = data.cityId;
    obj.languageName = data.languageName;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.name = this.trimMe(this.name);
    return this;
  }
}
