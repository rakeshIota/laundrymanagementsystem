import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class PaymentLog extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	userId: number;
			  	cardNumber: string;
			  	amount: number;
			  	orderId: number;
			  	transactionId: string;
			  	status: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : PaymentLog {
		const obj = new PaymentLog();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.cardNumber = data.cardNumber;
		obj.amount = data.amount;
		obj.orderId = data.orderId;
		obj.transactionId = data.transactionId;
		obj.status = data.status;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.transactionId)) {
            form.controls.transactionId.setErrors({ invalid: true });
            return false;
        }
		if (this.isNullOrUndefinedAndEmpty(this.status)) {
            form.controls.status.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.cardNumber = this.trimMe(this.cardNumber);
				this.transactionId = this.trimMe(this.transactionId);
				this.status = this.trimMe(this.status);
        return this;
    }
}
