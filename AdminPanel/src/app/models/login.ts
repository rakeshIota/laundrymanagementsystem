import {isNullOrUndefined} from 'util';

export class Login {
  userName: string;
  email: string;
  password: string;

  constructor() {
  }

  isValidLoginRequest(form: any) {
    if (isNullOrUndefined(this.userName) || this.userName.trim() === '') {
      form.controls.useEmail.setErrors({invalid: true});
      return false;
    }
    if (isNullOrUndefined(this.password) || this.password.trim() === '') {
      form.controls.userPassword.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  isValidForgotPasswordRequest(form: any) {
    if (isNullOrUndefined(this.userName) || this.userName.trim() === '') {
      form.controls.useEmail.setErrors({invalid: true});
      return false;
    }
    return true;
  }
}
