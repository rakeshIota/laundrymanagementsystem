import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Language } from './language';
export class DefaultMessageLanguageMapping extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	defaultMessageId: number;
			  	languageIdDetail: Language;
			  	languageId: string;
			  	message: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : DefaultMessageLanguageMapping {
		const obj = new DefaultMessageLanguageMapping();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.defaultMessageId = data.defaultMessageId;
	  	obj.languageIdDetail = data.languageIdDetail;
	  	obj.languageId = data.languageId;
		obj.message = data.message;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.message)) {
            form.controls.message.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.message = this.trimMe(this.message);
        return this;
    }
}
