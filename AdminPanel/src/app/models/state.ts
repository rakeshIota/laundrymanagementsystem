import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {Country} from './country';
import {StateLanguageMapping} from './statelanguagemapping';

export class State extends BaseModel {

  tenantId: number;
  slug: string;
  countryIdDetail: Country;
  countryId: string;
  stateLanguages: StateLanguageMapping[];
  name: string;
  countryName: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.stateLanguages = new Array<StateLanguageMapping>();
  }

  static fromResponse(data: any): State {
    const obj = new State();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.countryIdDetail = data.countryIdDetail;
    obj.countryId = data.countryId;
    obj.stateLanguages = data.stateLanguages;
    obj.name = data.name;
    obj.countryName = data.countryName;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.stateLanguages = this.stateLanguages.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    return this;
  }
}
