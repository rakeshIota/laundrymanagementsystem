import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class Packing extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	productId: number;
			  	serviceId: number;
			  	price: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : Packing {
		const obj = new Packing();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.productId = data.productId;
		obj.serviceId = data.serviceId;
		obj.price = data.price;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
        return this;
    }
}
