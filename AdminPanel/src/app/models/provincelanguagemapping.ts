import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';

export class ProvinceLanguageMapping extends BaseModel {

  tenantId: number;
  slug: string;
  languageId: string;
  provinceId: number;
  languageName: string;
  name: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
  }

  static fromResponse(data: any): ProvinceLanguageMapping {
    const obj = new ProvinceLanguageMapping();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.languageName = data.languageName;
    obj.languageId = data.languageId;
    obj.provinceId = data.provinceId;
    obj.name = data.name;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.name = this.trimMe(this.name);
    return this;
  }
}
