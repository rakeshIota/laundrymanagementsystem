import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class UserCordinate extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	userId: number;
			  	latitude: number;
			  	longitude: number;
			  	orderId: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : UserCordinate {
		const obj = new UserCordinate();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.latitude = data.latitude;
		obj.longitude = data.longitude;
		obj.orderId = data.orderId;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
        return this;
    }
}
