import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {OrderItem} from './orderitem';
import {Users} from './users';

export class Order extends BaseModel {

	tenantId: number;
	slug: string;
	userId: number;
	deliveryDate: Date;
	deliveryDateCalendar: any;
	deliveryType: string;
	totalItems: number;
	deliveryPrice: number;
	driverId: string;
	status: string;
	paymentType: string;
	paidAmount: number;
	tax: number;
	subTotal: number;
	totalPrice: number;
	adressId: number;
	expectedPickUpMin: number;
	expectedPickUpMax: number;
	expectedDeliveryMin: number;
	expectedDeliveryMax: number;
	cartId: number;
	driverDetails: any;
	pickupDriverDetail: any;
	customerDetails: Users;
	totalCount: number;
	deliveryAddress: any;
	paymentStatus: string;
	statusSlug: string;
	deliveryTypeSlug: string;
	paymentStatusSlug: string;
	paymentTypeSlug: string;
	collectedDate: Date;
	deliveredDate: Date;
	adminNote: string;
	paymentDate: Date;
	orderId: string;
	pickupDate: Date;
	pickupSlot: string;
	deliverySlot: string;
	orderItems: OrderItem[];
	logisticCharge: number;
	customerName: string;
	driverName: string;
	districtName: string;
	subDistrictName: string;
  change: number;
  laundryInstruction: string;

	constructor() {
		super();
		this.isDeleted = false;
		this.isActive = true;
	}

	static fromResponse(data: any): Order {
		const obj = new Order();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.userId = data.userId;
		obj.deliveryDate = data.deliveryDate;
		obj.deliveryDateCalendar = {date: moment(data.deliveryDate).format('DD/MM/YYYY')};
		obj.deliveryType = data.deliveryType;
		obj.totalItems = data.totalItems;
		obj.deliveryPrice = data.deliveryPrice;
		obj.driverId = data.driverId;
		obj.status = data.status;
		obj.paymentType = data.paymentType;
		obj.paidAmount = data.paidAmount;
		obj.tax = data.tax;
		obj.subTotal = data.subTotal;
		obj.totalPrice = data.totalPrice;
		obj.adressId = data.adressId;
		obj.expectedPickUpMin = data.expectedPickUpMin;
		obj.expectedPickUpMax = data.expectedPickUpMax;
		obj.expectedDeliveryMin = data.expectedDeliveryMin;
		obj.expectedDeliveryMax = data.expectedDeliveryMax;
		obj.cartId = data.cartId;
		obj.collectedDate = data.collectedDate;
		obj.deliveredDate = data.deliveredDate;
		obj.adminNote = data.adminNote;
		obj.paymentDate = data.paymentDate;
		obj.laundryInstruction = data.laundryInstruction;
		return obj;
	}

	isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.status)) {
			form.controls.status.setErrors({invalid: true});
			return false;
		}
		return true;
	}

	forRequest() {
		this.deliveryDate = this.convertCalToDate(this.deliveryDateCalendar);
		this.status = this.trimMe(this.status);
		this.paymentType = this.trimMe(this.paymentType);
		return this;
	}
}

export class DataTablesResponse {
	data: any[];
	next: number;
	recordsFiltered: number;
	recordsTotal: number;
	offset: number;
}

export class OrderStatus {
	name: string;
	status: string;
}


export class OrderFilterOption {
	districtId: string;
	subDistrictId: string;
	status: string;
	driverId: string;
	customerFilterType: string;
	deliveryType: string;
	relationTable: string;
}

export class CustomerFilterOption {
	name: string;
	filterType: string;
}
