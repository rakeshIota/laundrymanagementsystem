import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class AppSetting extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	key: string;
			  	value: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : AppSetting {
		const obj = new AppSetting();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.key = data.key;
		obj.value = data.value;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.key = this.trimMe(this.key);
				this.value = this.trimMe(this.value);
        return this;
    }
}
