import { PostalCode } from './postalcode';
import { IResourceWithId } from '../shared/auth.model';

export class BroadcastMessage implements IResourceWithId {
  id: number;
  customerType: string;
  customerGender: string;
  customerOrderType: string;
  orderCount: number;
  postalCodes: PostalCode[];
  message: string;
  constructor() {
    this.postalCodes = new Array<PostalCode>();
  }
}

export class BroadcastFilterType {
  name: string;
  value: string;
}
