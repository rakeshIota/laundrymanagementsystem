import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class CartItemServiceMapping extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	cartItemId: number;
			  	serviceId: number;
				quantity: number;
			  	price: number;
			  	subTotal: number;
			  	totalPrice: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : CartItemServiceMapping {
		const obj = new CartItemServiceMapping();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.cartItemId = data.cartItemId;
		obj.serviceId = data.serviceId;
		obj.quantity = data.quantity;
		obj.price = data.price;
		obj.subTotal = data.subTotal;
		obj.totalPrice = data.totalPrice;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
        return this;
    }
}
