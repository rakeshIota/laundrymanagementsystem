import {TranslateService} from '@ngx-translate/core';
import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';

export class DefaultMessage extends BaseModel {

  tenantId: number;
  slug: string;
  type: string;
  message: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = true;
  }

  static fromResponse(data: any): DefaultMessage {
    const obj = new DefaultMessage();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.type = data.type;
    obj.message = data.message;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    if (this.isNullOrUndefinedAndEmpty(this.message)) {
      form.controls.defaultMessageType.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  forRequest() {
    this.message = this.trimMe(this.message);
    return this;
  }
}
