import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {State} from './state';
import {CityLanguageMapping} from './citylanguagemapping';

export class City extends BaseModel {

  tenantId: number;
  slug: string;
  stateIdDetail: State;
  stateId: string;
  cityLanguages: CityLanguageMapping[];
  name: string;
  stateName: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.cityLanguages = new Array<CityLanguageMapping>();
  }

  static fromResponse(data: any): City {
    const obj = new City();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.stateIdDetail = data.stateIdDetail;
    obj.stateId = data.stateId;
    obj.cityLanguages = data.cityLanguages;
    obj.name = data.name;
    obj.stateName = data.stateName;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.cityLanguages = this.cityLanguages.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    return this;
  }
}
