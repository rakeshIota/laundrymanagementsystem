import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {ValidationService} from '../shared/validation.service';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {Language} from './language';

export class ServiceLanguageMapping extends BaseModel {

  tenantId: number;
  slug: string;
  name: string;
  languageIdDetail: Language;
  languageId: string;
  serviceId: number;
  languageName: string;
  description: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.languageId = '';
  }

  static fromResponse(data: any): ServiceLanguageMapping {
    const obj = new ServiceLanguageMapping();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.name = data.name;
    obj.languageIdDetail = data.languageIdDetail;
    obj.languageId = data.languageId;
    obj.serviceId = data.serviceId;
    obj.languageName = data.languageName;
    obj.description = data.description;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    if (this.isNullOrUndefinedAndEmpty(this.name)) {
      form.controls.name.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  forRequest() {
    this.name = this.trimMe(this.name);
    return this;
  }
}
