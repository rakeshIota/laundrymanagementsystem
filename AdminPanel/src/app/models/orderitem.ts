import { TranslateService } from '@ngx-translate/core';
import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { Product } from './product';
import { Service } from './service';
export class OrderItem extends BaseModel {

  tenantId: number;
  slug: string;
  orderId: number;
  productId: string;
  product: Product;
  serviceId: string;
  service: Service;
  quantity: number;
  isPacking: boolean;
  packingPrice: number;
  subTotal: number;
  totalPrice: number;
  packingId: number;
  productIsEditable: boolean;
  serviceIsEditable: boolean;
  quantityIsEditable: boolean;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = true;
    this.isPacking = false;
    this.productIsEditable = false;
    this.serviceIsEditable = false;
    this.quantityIsEditable = false;
  }

  static fromResponse(data: any): OrderItem {
    const obj = new OrderItem();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.orderId = data.orderId;
    obj.productId = data.productId;
    obj.quantity = data.quantity;
    obj.isPacking = data.isPacking;
    obj.packingPrice = data.packingPrice;
    obj.subTotal = data.subTotal;
    obj.totalPrice = data.totalPrice;
    obj.packingId = data.packingId;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    return this;
  }
}
