import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {ValidationService} from '../shared/validation.service';
import {TranslateService} from '@ngx-translate/core';
import * as moment from 'moment';
import {Language} from './language';

export class StateLanguageMapping extends BaseModel {
  tenantId: number;
  slug: string;
  name: string;
  languageIdDetail: Language;
  languageId: string;
  stateId: number;
  languageName: string;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.languageId = '';
  }

  static fromResponse(data: any): StateLanguageMapping {
    const obj = new StateLanguageMapping();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.name = data.name;
    obj.languageIdDetail = data.languageIdDetail;
    obj.languageId = data.languageId;
    obj.stateId = data.stateId;
    obj.languageName = data.languageName;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    return true;
  }

  forRequest() {
    this.name = this.trimMe(this.name);
    return this;
  }
}
