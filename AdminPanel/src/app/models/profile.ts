import {isNullOrUndefined} from 'util';

export class Profile {
  oldPassword: string;
  password: string;
  confirmPassword: string;

  constructor() {
  }

  isValidChangePasswordRequest(form: any) {
    if (isNullOrUndefined(this.oldPassword) || this.oldPassword.trim() === '') {
      form.controls.oldPassword.setErrors({invalid: true});
      return false;
    }
    if (isNullOrUndefined(this.password) || this.password.trim() === '') {
      form.controls.newPassword.setErrors({invalid: true});
      return false;
    }
    if (isNullOrUndefined(this.confirmPassword) || this.confirmPassword.trim() === '') {
      form.controls.confirmPassword.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  forRequest() {
    this.oldPassword = this.oldPassword.trim();
    this.password = this.password.trim();
    this.confirmPassword = this.confirmPassword.trim();
    return this;
  }
}
