import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class Feedback extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	orderId: number;
			  	type: string;
			  	message: string;
			  	rating: number;
			  	driverId: number;
			  	userId: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : Feedback {
		const obj = new Feedback();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.orderId = data.orderId;
		obj.type = data.type;
		obj.message = data.message;
		obj.rating = data.rating;
		obj.driverId = data.driverId;
		obj.userId = data.userId;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.type = this.trimMe(this.type);
				this.message = this.trimMe(this.message);
        return this;
    }
}
