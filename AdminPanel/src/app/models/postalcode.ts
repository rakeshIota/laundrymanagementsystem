import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class PostalCode extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	postalCode: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : PostalCode {
		const obj = new PostalCode();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.postalCode = data.postalCode;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.postalCode)) {
            form.controls.postalCode.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.postalCode = this.trimMe(this.postalCode);
        return this;
    }
}
