export class Address {
  buildingName: string;
  districtId: string;
  districtName: string;
  floor: string;
  houseNumber: string;
  id: string;
  latitude: number;
  longitude: number;
  note: string;
  phoneNo: string;
  postalCode: string;
  provinceId: string;
  provinceName: string;
  residenceType: string;
  streetNumber: string;
  subDistrictId: string;
  subDistrictName: string;
  alternatePhoneNo: string;
  type: string;
  unit: string;
}
