import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Language } from './language';
export class NotificationMasterLanguageMapping extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	languageIdDetail: Language;
			  	languageId: string;
			  	message: string;
			  	notificationMasterId: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : NotificationMasterLanguageMapping {
		const obj = new NotificationMasterLanguageMapping();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
	  	obj.languageIdDetail = data.languageIdDetail;
	  	obj.languageId = data.languageId;
		obj.message = data.message;
		obj.notificationMasterId = data.notificationMasterId;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.message = this.trimMe(this.message);
        return this;
    }
}
