import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { Language } from './language';
export class CurrencyLanguageMapping extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	currencyId: number;
			  	languageIdDetail: Language;
			  	languageId: string;
			  	name: string;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : CurrencyLanguageMapping {
		const obj = new CurrencyLanguageMapping();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.currencyId = data.currencyId;
	  	obj.languageIdDetail = data.languageIdDetail;
	  	obj.languageId = data.languageId;
		obj.name = data.name;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
		if (this.isNullOrUndefinedAndEmpty(this.name)) {
            form.controls.name.setErrors({ invalid: true });
            return false;
        }
        return true;
    }

    forRequest() {
				this.name = this.trimMe(this.name);
        return this;
    }
}
