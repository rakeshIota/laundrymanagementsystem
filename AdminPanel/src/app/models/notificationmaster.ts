import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class NotificationMaster extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	type: string;
			  	canBeDelete: boolean;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
			this.canBeDelete=false;
    }
    
   static fromResponse(data: any) : NotificationMaster {
		const obj = new NotificationMaster();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.type = data.type;
		obj.canBeDelete = data.canBeDelete;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
				this.type = this.trimMe(this.type);
        return this;
    }
}
