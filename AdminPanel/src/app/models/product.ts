import { BaseModel } from '../config/base.model';
import { Attachment } from './attachment';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { ProductLanguageMapping } from './productlanguagemapping';
import { ProductPrice } from './productprice';

export class Product extends BaseModel {

  tenantId: number;
  slug: string;
  image: Attachment[];
  gender: string;
  name: string;
  productLanguages: ProductLanguageMapping[];
  productPrices: ProductPrice[];
  price: number;
  rank: number;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
    this.image = new Array<Attachment>();
    this.productLanguages = new Array<ProductLanguageMapping>();
    this.gender = '';
    this.productPrices = new Array<ProductPrice>();
  }

  static fromResponse(data: any): Product {
    const obj = new Product();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.image = data.image;
    obj.gender = data.gender;
    obj.name = data.name;
    obj.productLanguages = data.productLanguages;
    obj.productPrices = data.productPrices;
    obj.rank = data.rank;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    if (this.isNullOrUndefinedAndEmpty(this.gender)) {
      form.controls.gender.setErrors({ invalid: true });
      return false;
    }
    const prices = this.productPrices.filter((obj) => {
      return !obj.isDeleted;
    });
    if (prices.length === 0) {
      toastService.error('please add price');
      return false;
    }
    const image = this.image.filter((obj) => {
      return !obj.isDeleted;
    });
    if (image.length === 0) {
      toastService.error('please add icon');
      return false;
    }
    return true;
  }

  forRequest() {
    this.productLanguages = this.productLanguages.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    this.productPrices = this.productPrices.filter((obj) => {
      obj.isActive = true;
      obj.isDeleted = obj.isDeleted ? true : false;
      return (obj.id && (obj.isDeleted || !obj.isDeleted)) || (!obj.id && !obj.isDeleted);
    });
    this.gender = this.trimMe(this.gender);
    this.name = this.trimMe(this.name);
    return this;
  }
}
