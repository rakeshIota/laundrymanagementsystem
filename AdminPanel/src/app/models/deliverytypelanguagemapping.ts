import {BaseModel} from '../config/base.model';
import {ToastService} from '../shared/toast.service';
import {TranslateService} from '@ngx-translate/core';
import {Language} from './language';

export class DeliveryTypeLanguageMapping extends BaseModel {

  tenantId: number;
  slug: string;
  name: string;
  languageIdDetail: Language;
  languageId: string;
  deliveryTypeId: number;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = false;
  }

  static fromResponse(data: any): DeliveryTypeLanguageMapping {
    const obj = new DeliveryTypeLanguageMapping();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.name = data.name;
    obj.languageIdDetail = data.languageIdDetail;
    obj.languageId = data.languageId;
    obj.deliveryTypeId = data.deliveryTypeId;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    if (this.isNullOrUndefinedAndEmpty(this.name)) {
      form.controls.name.setErrors({invalid: true});
      return false;
    }
    return true;
  }

  forRequest() {
    this.name = this.trimMe(this.name);
    return this;
  }
}
