import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
import { ValidationService } from '../shared/validation.service';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
export class OrderItemServiceMapping extends BaseModel {
	
				tenantId: number;
			  	slug: string;
			  	orderItemId: number;
			  	serviceId: number;
				quantity: number;
			  	price: number;
			  	subTotal: number;
			  	totalPrice: number;
	
    constructor() {
        super();
			this.isDeleted=false;
			this.isActive=true;
    }
    
   static fromResponse(data: any) : OrderItemServiceMapping {
		const obj = new OrderItemServiceMapping();
		obj.id = data.id;
		obj.tenantId = data.tenantId;
		obj.slug = data.slug;
		obj.createdBy = data.createdBy;
		obj.updatedBy = data.updatedBy;
		obj.createdOn = data.createdOn;
		obj.updatedOn = data.updatedOn;
		obj.isDeleted = data.isDeleted;
		obj.isActive = data.isActive;
		obj.orderItemId = data.orderItemId;
		obj.serviceId = data.serviceId;
		obj.quantity = data.quantity;
		obj.price = data.price;
		obj.subTotal = data.subTotal;
		obj.totalPrice = data.totalPrice;
		return obj;
	}

    isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
        return true;
    }

    forRequest() {
        return this;
    }
}
