export class OrderStats {
  totalOrder: TotalOrder;
  newOrder: NewOrder;
  collectionOrder: CollectionOrder;
  cleaningOrder: CleaningOrder;
  deliveredOrder: DeliveredOrder;
}

export class TotalOrder {
  totalOrderTitle: string;
  totalOrderCount: number;
  totalNormalOrderCount: number;
  totalSameDayOrderCount: number;
  totalExpressOrderCount: number;
}

export class NewOrder {
  newOrderTitle: string;
  newOrderTotalCount: number;
  newSameDayOrderCount: number;
  newExpressOrderCount: number;
  newNormalOrderCount: number;
}

export class CollectionOrder {
  collectionOrderTitle: string;
  collectionOrderTotalCount: number;
  collectionAssignedOrderCount: number;
  collectionCollectedOrderCount: number;
}

export class CleaningOrder {
  cleaningOrderTitle: string;
  cleaningOrderTotalCount: number;
  cleaningInprogressOrderCount: number;
  cleaningCompletedOrderCount: number;
}

export class DeliveredOrder {
  deliveryOrderTitle: string;
  deliveryOrderTotalCount: number;
}
