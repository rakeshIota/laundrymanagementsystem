import { TranslateService } from '@ngx-translate/core';
import { BaseModel } from '../config/base.model';
import { ToastService } from '../shared/toast.service';
export class Promotion extends BaseModel {
  tenantId: number;
  slug: string;
  title: string;
  url: string;
  attachments: Array<any>;
  file: Array<any>;

  constructor() {
    super();
    this.isDeleted = false;
    this.isActive = true;
  }

  static fromResponse(data: any): Promotion {
    const obj = new Promotion();
    obj.id = data.id;
    obj.tenantId = data.tenantId;
    obj.slug = data.slug;
    obj.createdBy = data.createdBy;
    obj.updatedBy = data.updatedBy;
    obj.createdOn = data.createdOn;
    obj.updatedOn = data.updatedOn;
    obj.isDeleted = data.isDeleted;
    obj.isActive = data.isActive;
    obj.title = data.title;
    obj.url = data.url;
    return obj;
  }

  isValidateRequest(form: any, toastService: ToastService, translate: TranslateService) {
    if (this.isNullOrUndefinedAndEmpty(this.title)) {
      form.controls.type.setErrors({ invalid: true });
      return false;
    }
    return true;
  }

  forRequest() {
    this.title = this.trimMe(this.title);
    return this;
  }
}
