import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpAuthInterceptor} from './shared/http.interceptor';
import {LocalStorageModule} from 'angular-2-local-storage';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './pages/login/login.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {ForbiddenComponent} from './pages/forbidden/forbidden.component';
import {RecoverPasswordComponent} from './pages/recover-password/recover-password.component';
import {DataTablesModule} from 'angular-datatables';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialAppModule} from './material.module';
import {MatNativeDateModule} from '@angular/material';
import {RegisterSuccessComponent} from './pages/register/success/register-success.component';
import {RegisterFailureComponent} from './pages/register/failure/register-failure.component';
import {NgxMaskModule} from 'ngx-mask';
import { ConnectionServiceModule } from 'ng-connection-service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    ForbiddenComponent,
    RecoverPasswordComponent,
    RegisterSuccessComponent,
    RegisterFailureComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialAppModule,
    MatNativeDateModule,
    LocalStorageModule.forRoot({
      prefix: 'site',
      storageType: 'localStorage'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    Ng4LoadingSpinnerModule.forRoot(),
    DataTablesModule,
    ConnectionServiceModule,
    NgxMaskModule.forRoot(),
    AngularFontAwesomeModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
