/****** Object:  UserDefinedFunction [dbo].[ListToTable]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ListToTable] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (number bigint NOT NULL) AS
BEGIN
   DECLARE @pos        bigint,
           @nextpos    bigint,
           @valuelen   bigint

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (number)
         VALUES (convert(int, substring(@list, @pos + 1, @valuelen)))
      SELECT @pos = @nextpos
   END
   RETURN
END
GO
/****** Object:  Table [dbo].[AppSetting]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppSetting](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Key] [nvarchar](100) NULL,
	[Value] [nvarchar](100) NULL,
 CONSTRAINT [PK_AppSetting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BroadCastMessage]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BroadCastMessage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Message] [nvarchar](max) NULL,
	[TotalUsers] [bigint] NULL,
	[Totalbatches] [bigint] NULL,
	[lastBatch] [bigint] NULL,
	[PendingUsers] [bigint] NULL,
	[UserType] [nvarchar](200) NULL,
	[postalCode] [nvarchar](max) NULL,
	[Gender] [nvarchar](20) NULL,
	[Relation] [nvarchar](20) NULL,
	[OrderCount] [bigint] NULL,
	[next] [int] NULL,
	[offset] [int] NULL,
	[Result] [varchar](max) NULL,
 CONSTRAINT [PK_BroadCastMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[Status] [nvarchar](30) NULL,
	[Tax] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[TotalItems] [int] NOT NULL,
	[DeliveryType] [nvarchar](100) NULL,
	[DeliveryDate] [datetimeoffset](7) NULL,
	[PickupDate] [datetimeoffset](7) NULL,
	[deliverySlot] [nvarchar](100) NULL,
	[pickupSlot] [nvarchar](100) NULL,
	[laundryInstruction] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CartItem]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CartId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[ServiceId] [bigint] NULL,
 CONSTRAINT [PK_CartItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CartItemServiceMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartItemServiceMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CartItemId] [bigint] NULL,
	[ServiceId] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_CartItemServiceMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[StateId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CityLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CityLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[LanguageId] [bigint] NOT NULL,
	[CityId] [bigint] NULL,
 CONSTRAINT [PK_CityLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CountryLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[LanguageId] [bigint] NOT NULL,
	[CountryId] [bigint] NOT NULL,
 CONSTRAINT [PK_CountryLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CurrencyLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CurrencyId] [bigint] NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_CurrencyLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DefaultMessage]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultMessage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[message] [nvarchar](max) NULL,
 CONSTRAINT [PK_DefaultMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DefaultMessageLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultMessageLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[DefaultMessageId] [bigint] NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_DefaultMessageLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryAddress]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryAddress](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NULL,
	[AddressLine1] [nvarchar](max) NULL,
	[AddressLine2] [nvarchar](max) NULL,
	[DistrictId] [bigint] NULL,
	[SubDistrictId] [bigint] NULL,
	[PostalCode] [nvarchar](40) NULL,
	[IsDefault] [bit] NULL,
	[Tag] [nvarchar](40) NULL,
	[ProvinceId] [bigint] NULL,
	[Latitude] [decimal](18, 6) NULL,
	[Longitude] [decimal](18, 6) NULL,
	[Type] [nvarchar](100) NULL,
	[HouseNumber] [nvarchar](100) NULL,
	[StreetNumber] [nvarchar](100) NULL,
	[Note] [nvarchar](max) NULL,
	[BuildingName] [nvarchar](100) NULL,
	[Floor] [nvarchar](100) NULL,
	[UnitNo] [nvarchar](100) NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[PhoneExt] [nvarchar](20) NULL,
	[ResidenceType] [nvarchar](20) NULL,
	[orderId] [bigint] NULL,
	[alterNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_DeliveryAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryType]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PriceType] [nvarchar](100) NULL,
	[Price] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_DeliveryType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliveryTypeLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliveryTypeLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[DeliveryTypeId] [bigint] NOT NULL,
 CONSTRAINT [PK_DeliveryTypeLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[CityId] [bigint] NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DistrictLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DistrictLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[LanguageId] [bigint] NOT NULL,
	[DistrictId] [bigint] NOT NULL,
 CONSTRAINT [PK_DistrictLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DriverLocation]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriverLocation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Longititude] [decimal](18, 6) NULL,
	[Latitude] [decimal](18, 6) NULL,
	[userId] [bigint] NULL,
 CONSTRAINT [PK_DriverLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DriverRideLogs]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DriverRideLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StartOn] [datetimeoffset](7) NULL,
	[CompleteOn] [datetimeoffset](7) NULL,
	[IsComplete] [bit] NOT NULL,
	[DriverId] [bigint] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Type] [nvarchar](100) NULL,
 CONSTRAINT [PK_DriverRideLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailConfiguration]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailConfiguration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConfigurationKey] [nvarchar](200) NULL,
	[ConfigurationValue] [nvarchar](max) NULL,
	[EmailSubject] [nchar](1000) NULL,
	[CreatedDate] [datetimeoffset](7) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[IsDeleted] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExceptionLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExceptionLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [nvarchar](1000) NOT NULL,
	[Message] [nvarchar](1000) NOT NULL,
	[StackTrace] [nvarchar](max) NOT NULL,
	[Uri] [nvarchar](1000) NOT NULL,
	[method] [nvarchar](50) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OrderId] [bigint] NULL,
	[Type] [nvarchar](100) NULL,
	[Message] [nvarchar](max) NULL,
	[Rating] [bigint] NULL,
	[DriverId] [bigint] NULL,
	[UserId] [bigint] NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileGroup]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[UpdatedOn] [datetimeoffset](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_Gallery] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileGroupItems]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileGroupItems](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[UpdatedOn] [datetimeoffset](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Filename] [nvarchar](256) NOT NULL,
	[MimeType] [nvarchar](256) NOT NULL,
	[Thumbnail] [nvarchar](256) NULL,
	[Size] [bigint] NULL,
	[Path] [nvarchar](256) NOT NULL,
	[OriginalName] [nvarchar](256) NULL,
	[OnServer] [nvarchar](256) NULL,
	[TypeId] [bigint] NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_AttachmentFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Language]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Language](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Code] [nvarchar](50) NULL,
 CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MessageLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NULL,
	[Message] [nvarchar](max) NULL,
	[LanguageId] [bigint] NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_MessageLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NULL,
	[Message] [nvarchar](max) NULL,
	[LanguageId] [bigint] NULL,
	[Type] [nvarchar](100) NULL,
	[Token] [nvarchar](100) NULL,
	[Status] [nvarchar](100) NULL,
	[Response] [nvarchar](max) NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationMaster]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationMaster](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Type] [nvarchar](100) NULL,
	[CanBeDelete] [bit] NOT NULL,
	[Message] [nvarchar](max) NULL,
 CONSTRAINT [PK_NotificationMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationMasterKeyValueMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationMasterKeyValueMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Key] [nvarchar](100) NULL,
	[Value] [nvarchar](100) NULL,
	[NotificationId] [bigint] NULL,
 CONSTRAINT [PK_NotificationMasterKeyValueMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationMasterLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationMasterLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[Message] [nvarchar](max) NULL,
	[NotificationMasterId] [bigint] NULL,
 CONSTRAINT [PK_NotificationMasterLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[DeliveryDate] [datetimeoffset](7) NOT NULL,
	[DeliveryType] [nvarchar](50) NULL,
	[TotalItems] [int] NOT NULL,
	[DeliveryPrice] [decimal](18, 2) NULL,
	[DriverId] [bigint] NULL,
	[Status] [nvarchar](30) NOT NULL,
	[PaymentType] [nvarchar](30) NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[Tax] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[ExpectedPickUpMin] [bigint] NULL,
	[ExpectedPickUpMax] [bigint] NULL,
	[ExpectedDeliveryMin] [bigint] NULL,
	[ExpectedDeliveryMax] [bigint] NULL,
	[CartId] [bigint] NULL,
	[Deliveryaddress] [nvarchar](max) NULL,
	[PickupDate] [datetimeoffset](7) NULL,
	[deliverySlot] [nvarchar](100) NULL,
	[pickupSlot] [nvarchar](100) NULL,
	[LogisticCharge] [decimal](18, 2) NULL,
	[discount] [decimal](18, 2) NULL,
	[paymentResponse] [nvarchar](100) NULL,
	[PaymentStatus] [nvarchar](100) NULL,
	[change] [decimal](18, 2) NULL,
	[note] [nvarchar](max) NULL,
	[AdminNote] [nvarchar](max) NULL,
	[paymentDate] [datetimeoffset](7) NULL,
	[collectedDate] [datetimeoffset](7) NULL,
	[deliveredDate] [datetimeoffset](7) NULL,
	[QRCode] [nvarchar](max) NULL,
	[laundryInstruction] [nvarchar](max) NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItem]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Quantity] [int] NULL,
	[IsPacking] [bit] NOT NULL,
	[PackingPrice] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PackingId] [bigint] NOT NULL,
	[ServiceId] [bigint] NULL,
 CONSTRAINT [PK_OrderItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItemServiceMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItemServiceMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OrderItemId] [bigint] NULL,
	[ServiceId] [bigint] NOT NULL,
	[Quantity] [int] NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_OrderItemServiceMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrderLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Packing]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Packing](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Packing] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PackingLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackingLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PackingId] [bigint] NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_PackingLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PasswordLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NULL,
	[Count] [int] NULL,
 CONSTRAINT [PK_PasswordLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[CardNumber] [nvarchar](20) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[TransactionId] [nvarchar](100) NOT NULL,
	[Status] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_PaymentLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostalCode]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostalCode](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PostalCode] [nvarchar](40) NOT NULL,
 CONSTRAINT [PK_PostalCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Gender] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Rank] [bigint] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
 CONSTRAINT [PK_ProductLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductPrice]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPrice](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Price] [decimal](18, 2) NULL,
 CONSTRAINT [PK_ProductPrice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promotion](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Title] [nvarchar](200) NULL,
	[url] [nvarchar](max) NULL,
 CONSTRAINT [PK_Promotion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Province] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProvinceLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProvinceLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ProvinceId] [bigint] NULL,
	[LanguageId] [bigint] NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_ProvinceLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PushNotification]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PushNotification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[UpdatedOn] [datetimeoffset](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Message] [nvarchar](256) NULL,
	[Type] [nvarchar](256) NULL,
	[IsArchive] [bit] NOT NULL,
	[TargetId] [bigint] NULL,
	[TargetType] [nvarchar](256) NULL,
	[UserId] [bigint] NULL,
	[IsSeen] [bit] NOT NULL,
	[UserRole] [nvarchar](20) NULL,
	[ArchivedBy] [bigint] NULL,
	[ArchiveDate] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_PushNotification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QrLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QrLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[QRCode] [nvarchar](max) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[TransactionId] [nvarchar](max) NULL,
	[Status] [nvarchar](100) NOT NULL,
	[result] [nvarchar](max) NULL,
	[AuthKey] [nvarchar](max) NULL,
	[txnCurrencyCode] [nvarchar](max) NULL,
	[loyaltyId] [nvarchar](max) NULL,
	[txnNo] [nvarchar](max) NULL,
	[additionalInfo] [nvarchar](max) NULL,
 CONSTRAINT [PK_QrLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rating]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[orderId] [bigint] NULL,
	[Quality] [bigint] NULL,
	[DeliveryService] [bigint] NULL,
	[Staff] [bigint] NULL,
	[Packaging] [bigint] NULL,
	[OverallSatisfaction] [bigint] NULL,
	[Feedback] [nvarchar](max) NULL,
 CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Service]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServiceLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LanguageId] [bigint] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_ServiceLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CountryId] [bigint] NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StateLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StateLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[LanguageId] [bigint] NOT NULL,
	[StateId] [bigint] NULL,
 CONSTRAINT [PK_StateLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubDistrict]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubDistrict](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[DistrictId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_SubDistrict] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubDistrictLanguageMapping]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubDistrictLanguageMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[LanguageId] [bigint] NOT NULL,
	[SubDistrictId] [bigint] NULL,
 CONSTRAINT [PK_SubDistrictLanguageMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tenant]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tenant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[Email] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
	[UpdatedOn] [datetimeoffset](7) NOT NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedBy] [bigint] NULL,
	[UniqueId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Tanant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadedFile]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadedFile](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](250) NULL,
	[FileUrl] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAddress]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAddress](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NULL,
	[AddressLine1] [nvarchar](max) NULL,
	[AddressLine2] [nvarchar](max) NULL,
	[DistrictId] [bigint] NULL,
	[SubDistrictId] [bigint] NULL,
	[PostalCode] [nvarchar](40) NULL,
	[IsDefault] [bit] NULL,
	[Tag] [nvarchar](40) NULL,
	[ProvinceId] [bigint] NULL,
	[Latitude] [decimal](18, 6) NULL,
	[Longitude] [decimal](18, 6) NULL,
	[Type] [nvarchar](100) NULL,
	[HouseNumber] [nvarchar](100) NULL,
	[StreetNumber] [nvarchar](100) NULL,
	[Note] [nvarchar](max) NULL,
	[BuildingName] [nvarchar](100) NULL,
	[Floor] [nvarchar](100) NULL,
	[UnitNo] [nvarchar](100) NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[PhoneExt] [nvarchar](20) NULL,
	[ResidenceType] [nvarchar](20) NULL,
	[alterNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_UserAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserClaims]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCordinate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCordinate](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[Slug] [nvarchar](255) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[UpdatedBy] [bigint] NOT NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[UpdatedOn] [datetimeoffset](7) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Latitude] [decimal](18, 2) NOT NULL,
	[Longitude] [decimal](18, 2) NOT NULL,
	[OrderId] [bigint] NULL,
 CONSTRAINT [PK_UserCordinate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDevice]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDevice](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceId] [nvarchar](250) NULL,
	[UserId] [bigint] NULL,
	[DeviceType] [nvarchar](50) NULL,
	[UserType] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeviceToken] [nvarchar](250) NULL,
	[CreatedOn] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLogins]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[ProfilePic] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[UniqueCode] [nvarchar](50) NULL,
	[IsFacebookConnected] [bit] NOT NULL,
	[IsGoogleConnected] [bit] NOT NULL,
	[FacebookId] [nvarchar](100) NULL,
	[GoogleId] [nvarchar](100) NULL,
	[TenantId] [int] NULL,
	[OTP] [bigint] NULL,
	[OTPValidTill] [datetime] NULL,
	[LanguageId] [bigint] NULL,
	[LicenseNo] [nvarchar](250) NULL,
	[Status] [nvarchar](100) NULL,
	[NickName] [nvarchar](100) NULL,
	[CountryCode] [nvarchar](10) NULL,
	[DOB] [datetime] NULL,
	[Gender] [varchar](20) NULL,
	[EmployeeId] [varchar](50) NULL,
	[Position] [varchar](50) NULL,
	[NationalId] [varchar](100) NULL,
	[LicenceId] [varchar](100) NULL,
	[BikeInformation] [varchar](100) NULL,
	[LicencePlate] [nvarchar](250) NULL,
	[CreatedOn] [datetimeoffset](7) NULL,
	[callStatus] [nvarchar](20) NULL,
	[adminNote] [varchar](max) NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTwilioCallingStatus]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTwilioCallingStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FromId] [bigint] NULL,
	[ToId] [bigint] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AppSetting] ADD  CONSTRAINT [DF_AppSetting_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[AppSetting] ADD  CONSTRAINT [DF_AppSetting_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[AppSetting] ADD  CONSTRAINT [DF_AppSetting_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AppSetting] ADD  CONSTRAINT [DF_AppSetting_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BroadCastMessage] ADD  CONSTRAINT [DF_BroadCastMessage_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[BroadCastMessage] ADD  CONSTRAINT [DF_BroadCastMessage_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[BroadCastMessage] ADD  CONSTRAINT [DF_BroadCastMessage_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BroadCastMessage] ADD  CONSTRAINT [DF_BroadCastMessage_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Cart] ADD  CONSTRAINT [DF_Cart_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Cart] ADD  CONSTRAINT [DF_Cart_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Cart] ADD  CONSTRAINT [DF_Cart_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Cart] ADD  CONSTRAINT [DF_Cart_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Cart] ADD  CONSTRAINT [DF_Cart_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[CartItem] ADD  CONSTRAINT [DF_CartItem_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CartItem] ADD  CONSTRAINT [DF_CartItem_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[CartItem] ADD  CONSTRAINT [DF_CartItem_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CartItem] ADD  CONSTRAINT [DF_CartItem_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CartItemServiceMapping] ADD  CONSTRAINT [DF_CartItemServiceMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CartItemServiceMapping] ADD  CONSTRAINT [DF_CartItemServiceMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[CartItemServiceMapping] ADD  CONSTRAINT [DF_CartItemServiceMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CartItemServiceMapping] ADD  CONSTRAINT [DF_CartItemServiceMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[City] ADD  CONSTRAINT [DF_City_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[City] ADD  CONSTRAINT [DF_City_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[City] ADD  CONSTRAINT [DF_City_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[City] ADD  CONSTRAINT [DF_City_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CityLanguageMapping] ADD  CONSTRAINT [DF_CityLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CityLanguageMapping] ADD  CONSTRAINT [DF_CityLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[CityLanguageMapping] ADD  CONSTRAINT [DF_CityLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CityLanguageMapping] ADD  CONSTRAINT [DF_CityLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CountryLanguageMapping] ADD  CONSTRAINT [DF_CountryLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CountryLanguageMapping] ADD  CONSTRAINT [DF_CountryLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[CountryLanguageMapping] ADD  CONSTRAINT [DF_CountryLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CountryLanguageMapping] ADD  CONSTRAINT [DF_CountryLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Currency] ADD  CONSTRAINT [DF_Currency_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CurrencyLanguageMapping] ADD  CONSTRAINT [DF_CurrencyLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[CurrencyLanguageMapping] ADD  CONSTRAINT [DF_CurrencyLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[CurrencyLanguageMapping] ADD  CONSTRAINT [DF_CurrencyLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CurrencyLanguageMapping] ADD  CONSTRAINT [DF_CurrencyLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DefaultMessage] ADD  CONSTRAINT [DF_DefaultMessage_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DefaultMessage] ADD  CONSTRAINT [DF_DefaultMessage_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DefaultMessage] ADD  CONSTRAINT [DF_DefaultMessage_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DefaultMessage] ADD  CONSTRAINT [DF_DefaultMessage_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DefaultMessageLanguageMapping] ADD  CONSTRAINT [DF_DefaultMessageLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DefaultMessageLanguageMapping] ADD  CONSTRAINT [DF_DefaultMessageLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DefaultMessageLanguageMapping] ADD  CONSTRAINT [DF_DefaultMessageLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DefaultMessageLanguageMapping] ADD  CONSTRAINT [DF_DefaultMessageLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DeliveryAddress] ADD  CONSTRAINT [DF_DeliveryAddress_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DeliveryAddress] ADD  CONSTRAINT [DF_DeliveryAddress_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DeliveryAddress] ADD  CONSTRAINT [DF_DeliveryAddress_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DeliveryAddress] ADD  CONSTRAINT [DF_DeliveryAddress_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DeliveryAddress] ADD  CONSTRAINT [DF_DeliveryAddress_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[DeliveryType] ADD  CONSTRAINT [DF_DeliveryType_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DeliveryType] ADD  CONSTRAINT [DF_DeliveryType_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DeliveryType] ADD  CONSTRAINT [DF_DeliveryType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DeliveryType] ADD  CONSTRAINT [DF_DeliveryType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DeliveryTypeLanguageMapping] ADD  CONSTRAINT [DF_DeliveryTypeLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DeliveryTypeLanguageMapping] ADD  CONSTRAINT [DF_DeliveryTypeLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DeliveryTypeLanguageMapping] ADD  CONSTRAINT [DF_DeliveryTypeLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DeliveryTypeLanguageMapping] ADD  CONSTRAINT [DF_DeliveryTypeLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[District] ADD  CONSTRAINT [DF_District_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[District] ADD  CONSTRAINT [DF_District_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[District] ADD  CONSTRAINT [DF_District_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[District] ADD  CONSTRAINT [DF_District_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DistrictLanguageMapping] ADD  CONSTRAINT [DF_DistrictLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DistrictLanguageMapping] ADD  CONSTRAINT [DF_DistrictLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DistrictLanguageMapping] ADD  CONSTRAINT [DF_DistrictLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DistrictLanguageMapping] ADD  CONSTRAINT [DF_DistrictLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DriverLocation] ADD  CONSTRAINT [DF_DriverLocation_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[DriverLocation] ADD  CONSTRAINT [DF_DriverLocation_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[DriverLocation] ADD  CONSTRAINT [DF_DriverLocation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DriverLocation] ADD  CONSTRAINT [DF_DriverLocation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[DriverRideLogs] ADD  CONSTRAINT [DF_DriverRideLogs_StartOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [StartOn]
GO
ALTER TABLE [dbo].[DriverRideLogs] ADD  CONSTRAINT [DF_DriverRideLogs_CompleteOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CompleteOn]
GO
ALTER TABLE [dbo].[DriverRideLogs] ADD  CONSTRAINT [DF_DriverRideLogs_IsComplete]  DEFAULT ((1)) FOR [IsComplete]
GO
ALTER TABLE [dbo].[EmailConfiguration] ADD  CONSTRAINT [DF_EmailConfiguration_CreatedDate]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[EmailConfiguration] ADD  CONSTRAINT [DF_EmailConfiguration_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ExceptionLog] ADD  CONSTRAINT [DF_ExceptionLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Feedback] ADD  CONSTRAINT [DF_Feedback_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Feedback] ADD  CONSTRAINT [DF_Feedback_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Feedback] ADD  CONSTRAINT [DF_Feedback_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Feedback] ADD  CONSTRAINT [DF_Feedback_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[FileGroup] ADD  CONSTRAINT [DF_Gallery_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[FileGroup] ADD  CONSTRAINT [DF_Gallery_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[FileGroup] ADD  CONSTRAINT [DF_Gallery_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[FileGroup] ADD  CONSTRAINT [DF_Gallery_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[FileGroupItems] ADD  CONSTRAINT [DF_AttachmentFile_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[FileGroupItems] ADD  CONSTRAINT [DF_AttachmentFile_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[FileGroupItems] ADD  CONSTRAINT [DF_AttachmentFile_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[FileGroupItems] ADD  CONSTRAINT [DF_AttachmentFile_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Language] ADD  CONSTRAINT [DF_Language_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[MessageLog] ADD  CONSTRAINT [DF_MessageLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[MessageLog] ADD  CONSTRAINT [DF_MessageLog_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[MessageLog] ADD  CONSTRAINT [DF_MessageLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[MessageLog] ADD  CONSTRAINT [DF_MessageLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Notification] ADD  CONSTRAINT [DF_Notification_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[NotificationMaster] ADD  CONSTRAINT [DF_NotificationMaster_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[NotificationMaster] ADD  CONSTRAINT [DF_NotificationMaster_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[NotificationMaster] ADD  CONSTRAINT [DF_NotificationMaster_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[NotificationMaster] ADD  CONSTRAINT [DF_NotificationMaster_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[NotificationMaster] ADD  CONSTRAINT [DF_NotificationMaster_CanBeDelete]  DEFAULT ((1)) FOR [CanBeDelete]
GO
ALTER TABLE [dbo].[NotificationMasterKeyValueMapping] ADD  CONSTRAINT [DF_NotificationMasterKeyValueMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[NotificationMasterKeyValueMapping] ADD  CONSTRAINT [DF_NotificationMasterKeyValueMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[NotificationMasterKeyValueMapping] ADD  CONSTRAINT [DF_NotificationMasterKeyValueMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[NotificationMasterKeyValueMapping] ADD  CONSTRAINT [DF_NotificationMasterKeyValueMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[NotificationMasterLanguageMapping] ADD  CONSTRAINT [DF_NotificationMasterLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[NotificationMasterLanguageMapping] ADD  CONSTRAINT [DF_NotificationMasterLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[NotificationMasterLanguageMapping] ADD  CONSTRAINT [DF_NotificationMasterLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[NotificationMasterLanguageMapping] ADD  CONSTRAINT [DF_NotificationMasterLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_DeliveryDate]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [DeliveryDate]
GO
ALTER TABLE [dbo].[OrderItem] ADD  CONSTRAINT [DF_OrderItem_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[OrderItem] ADD  CONSTRAINT [DF_OrderItem_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[OrderItem] ADD  CONSTRAINT [DF_OrderItem_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[OrderItem] ADD  CONSTRAINT [DF_OrderItem_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[OrderItem] ADD  CONSTRAINT [DF_OrderItem_IsPacking]  DEFAULT ((0)) FOR [IsPacking]
GO
ALTER TABLE [dbo].[OrderItemServiceMapping] ADD  CONSTRAINT [DF_OrderItemServiceMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[OrderItemServiceMapping] ADD  CONSTRAINT [DF_OrderItemServiceMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[OrderItemServiceMapping] ADD  CONSTRAINT [DF_OrderItemServiceMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[OrderItemServiceMapping] ADD  CONSTRAINT [DF_OrderItemServiceMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[OrderLog] ADD  CONSTRAINT [DF_OrderLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[OrderLog] ADD  CONSTRAINT [DF_OrderLog_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[OrderLog] ADD  CONSTRAINT [DF_OrderLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[OrderLog] ADD  CONSTRAINT [DF_OrderLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Packing] ADD  CONSTRAINT [DF_Packing_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Packing] ADD  CONSTRAINT [DF_Packing_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Packing] ADD  CONSTRAINT [DF_Packing_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Packing] ADD  CONSTRAINT [DF_Packing_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PackingLanguageMapping] ADD  CONSTRAINT [DF_PackingLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PackingLanguageMapping] ADD  CONSTRAINT [DF_PackingLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PackingLanguageMapping] ADD  CONSTRAINT [DF_PackingLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PackingLanguageMapping] ADD  CONSTRAINT [DF_PackingLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PasswordLog] ADD  CONSTRAINT [DF_PasswordLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PasswordLog] ADD  CONSTRAINT [DF_PasswordLog_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PasswordLog] ADD  CONSTRAINT [DF_PasswordLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PasswordLog] ADD  CONSTRAINT [DF_PasswordLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PaymentLog] ADD  CONSTRAINT [DF_PaymentLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PaymentLog] ADD  CONSTRAINT [DF_PaymentLog_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PaymentLog] ADD  CONSTRAINT [DF_PaymentLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PaymentLog] ADD  CONSTRAINT [DF_PaymentLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PostalCode] ADD  CONSTRAINT [DF_PostalCode_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PostalCode] ADD  CONSTRAINT [DF_PostalCode_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PostalCode] ADD  CONSTRAINT [DF_PostalCode_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PostalCode] ADD  CONSTRAINT [DF_PostalCode_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ProductLanguageMapping] ADD  CONSTRAINT [DF_ProductLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ProductLanguageMapping] ADD  CONSTRAINT [DF_ProductLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[ProductLanguageMapping] ADD  CONSTRAINT [DF_ProductLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProductLanguageMapping] ADD  CONSTRAINT [DF_ProductLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProductPrice] ADD  CONSTRAINT [DF_ProductPrice_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Promotion] ADD  CONSTRAINT [DF_Promotion_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Promotion] ADD  CONSTRAINT [DF_Promotion_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Promotion] ADD  CONSTRAINT [DF_Promotion_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Promotion] ADD  CONSTRAINT [DF_Promotion_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Province] ADD  CONSTRAINT [DF_Province_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Province] ADD  CONSTRAINT [DF_Province_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Province] ADD  CONSTRAINT [DF_Province_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Province] ADD  CONSTRAINT [DF_Province_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ProvinceLanguageMapping] ADD  CONSTRAINT [DF_ProvinceLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ProvinceLanguageMapping] ADD  CONSTRAINT [DF_ProvinceLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[ProvinceLanguageMapping] ADD  CONSTRAINT [DF_ProvinceLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProvinceLanguageMapping] ADD  CONSTRAINT [DF_ProvinceLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_IsArchive]  DEFAULT ((0)) FOR [IsArchive]
GO
ALTER TABLE [dbo].[PushNotification] ADD  CONSTRAINT [DF_PushNotification_IsSeen]  DEFAULT ((0)) FOR [IsSeen]
GO
ALTER TABLE [dbo].[QrLog] ADD  CONSTRAINT [DF_QrLog_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[QrLog] ADD  CONSTRAINT [DF_QrLog_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[QrLog] ADD  CONSTRAINT [DF_QrLog_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[QrLog] ADD  CONSTRAINT [DF_QrLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Rating] ADD  CONSTRAINT [DF_Rating_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Rating] ADD  CONSTRAINT [DF_Rating_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Rating] ADD  CONSTRAINT [DF_Rating_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Rating] ADD  CONSTRAINT [DF_Rating_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ServiceLanguageMapping] ADD  CONSTRAINT [DF_ServiceLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ServiceLanguageMapping] ADD  CONSTRAINT [DF_ServiceLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[ServiceLanguageMapping] ADD  CONSTRAINT [DF_ServiceLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ServiceLanguageMapping] ADD  CONSTRAINT [DF_ServiceLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[State] ADD  CONSTRAINT [DF_State_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[State] ADD  CONSTRAINT [DF_State_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[State] ADD  CONSTRAINT [DF_State_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[State] ADD  CONSTRAINT [DF_State_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[StateLanguageMapping] ADD  CONSTRAINT [DF_StateLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[StateLanguageMapping] ADD  CONSTRAINT [DF_StateLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[StateLanguageMapping] ADD  CONSTRAINT [DF_StateLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[StateLanguageMapping] ADD  CONSTRAINT [DF_StateLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubDistrict] ADD  CONSTRAINT [DF_SubDistrict_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[SubDistrict] ADD  CONSTRAINT [DF_SubDistrict_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[SubDistrict] ADD  CONSTRAINT [DF_SubDistrict_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SubDistrict] ADD  CONSTRAINT [DF_SubDistrict_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SubDistrictLanguageMapping] ADD  CONSTRAINT [DF_SubDistrictLanguageMapping_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[SubDistrictLanguageMapping] ADD  CONSTRAINT [DF_SubDistrictLanguageMapping_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[SubDistrictLanguageMapping] ADD  CONSTRAINT [DF_SubDistrictLanguageMapping_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SubDistrictLanguageMapping] ADD  CONSTRAINT [DF_SubDistrictLanguageMapping_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Tenant] ADD  CONSTRAINT [DF_Tanant_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Tenant] ADD  CONSTRAINT [DF_Tanant_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Tenant] ADD  CONSTRAINT [DF_Tanant_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Tenant] ADD  CONSTRAINT [DF_Tanant_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Tenant] ADD  CONSTRAINT [DF_Tenant_UniqueId]  DEFAULT (newid()) FOR [UniqueId]
GO
ALTER TABLE [dbo].[UploadedFile] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[UploadedFile] ADD  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[UserAddress] ADD  CONSTRAINT [DF_UserAddress_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[UserAddress] ADD  CONSTRAINT [DF_UserAddress_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[UserAddress] ADD  CONSTRAINT [DF_UserAddress_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[UserAddress] ADD  CONSTRAINT [DF_UserAddress_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[UserAddress] ADD  CONSTRAINT [DF_UserAddress_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[UserCordinate] ADD  CONSTRAINT [DF_UserCordinate_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[UserCordinate] ADD  CONSTRAINT [DF_UserCordinate_UpdatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[UserCordinate] ADD  CONSTRAINT [DF_UserCordinate_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[UserCordinate] ADD  CONSTRAINT [DF_UserCordinate_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[UserDevice] ADD  CONSTRAINT [DF_UserDevice_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsFacebookConnected]  DEFAULT ((0)) FOR [IsFacebookConnected]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsGoogleConnected]  DEFAULT ((0)) FOR [IsGoogleConnected]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Company_CreatedOn]  DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[UserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserClaims] CHECK CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[UserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogins] CHECK CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId]
GO
/****** Object:  StoredProcedure [dbo].[AllActiveOrderSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- select * from [order]        
CREATE PROCEDURE [dbo].[AllActiveOrderSelectApp](                
--declare          
@Id         BIGINT =null,                
@Slug     NVARCHAR(255)=NULL,                
@next   int = NULL,                
@offset  int = NULL,                
@RelationTable NVARCHAR(50)=NULL,                
@RelationId bigint=NULL,                
@TenantId   INT=NULL,                
@LanguageId  BIGINT=NULL,                
@userid      BIGINT=100,          
@IsClosed bit =null        
) AS           
BEGIN                
                
IF @next IS NULL                
BEGIN                
SET @next =100000                
SET @offset=1                
END                
                
  if(@IsClosed=0)        
  begin         
    set @IsClosed=null;        
  end        
        
             
          
 SELECT                
    R.[Id]                
               
                    
   ,                
    R.[CreatedOn]                
                       
                   
   ,                
    R.[Status] ,          
    PaymentStatus          
   ,      
   R.paymenttype,    
    
    R.[TotalItems]  ,              
    R.deliveryType,        
    R.PickupDate,  
 R.DeliveryDate,  
 R.pickupSlot,  
 R.deliverySlot,  
 R.TotalPrice,  
             
           
  R.Paymentresponse,          
            
  overall_count = COUNT(*) OVER(),                
           
 AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id           
           
  FOR XML AUTO,ROOT,ELEMENTs                
           
 )          
          
 from [order] R                  
 WHERE                 
 (                
  @RelationTable IS NULL                
 )            
         
        
        
        
 AND                
 (                
  @Id IS NULL                
  OR                
  R.[Id] = @Id                
 )                
 AND                
 (                
  @Slug IS NULL                
  OR                
  R.[Slug] = @Slug                
 )                
 AND                
 (                
  R.[IsDeleted] = 0                
 )                
 AND                
 (                
  @TenantId IS NULL                
  OR                
  R.[TenantId] = @TenantId                
 )                
 AND                
 (                
  @userid IS NULL                
   OR                
  R.userid =  @userid                
 )        
 and         
 (        
           
   R.status='AWAITING_COLLECTION'   
   or
   R.Status='ON_THE_WAY'
        
 )        
          
                
 Order by R.Id desc                
 OFFSET (@next*@offset)-@next ROWS                
    FETCH NEXT @next ROWS ONLY                
                
END                
                
                
                
---------------------------------------------------                
--------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[AllOrderSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 -- select * from [order]          
CREATE PROCEDURE [dbo].[AllOrderSelectApp](                  
--declare            
@Id         BIGINT =null,                  
@Slug     NVARCHAR(255)=NULL,                  
@next   int = NULL,                  
@offset  int = NULL,                  
@RelationTable NVARCHAR(50)=NULL,                  
@RelationId bigint=NULL,                  
@TenantId   INT=NULL,                  
@LanguageId  BIGINT=NULL,                  
@userid      BIGINT=100,            
@IsClosed bit =null          
) AS             
BEGIN                  
                  
IF @next IS NULL                  
BEGIN                  
SET @next =100000                  
SET @offset=1                  
END                  
                  
  if(@IsClosed=0)          
  begin           
    set @IsClosed=null;          
  end          
          
               
            
 SELECT                  
    R.[Id]                  
                 
                      
   ,                  
    R.[CreatedOn]                  
                         
                     
   ,                  
    R.[Status] ,            
    PaymentStatus            
   ,        
   R.paymenttype,      
      
    R.[TotalItems]  ,                
    R.deliveryType,          
    R.PickupDate,    
 R.DeliveryDate,    
 R.pickupSlot,    
 R.deliverySlot,    
 R.TotalPrice,    
               
             
  R.Paymentresponse,            
              
  overall_count = COUNT(*) OVER(),                  
             
 AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id             
             
  FOR XML AUTO,ROOT,ELEMENTs                  
             
 )            
            
 from [order] R                    
 WHERE                   
 (                  
  @RelationTable IS NULL                  
 )              
           
          
          
          
 AND                  
 (                  
  @Id IS NULL                  
  OR                  
  R.[Id] = @Id                  
 )                  
 AND                  
 (                  
  @Slug IS NULL                  
  OR                  
  R.[Slug] = @Slug                  
 )                  
 AND                  
 (                  
  R.[IsDeleted] = 0                  
 )                  
 AND                  
 (                  
  @TenantId IS NULL                  
  OR                  
  R.[TenantId] = @TenantId                  
 )                  
 AND                  
 (                  
  @userid IS NULL                  
   OR                  
  R.userid =  @userid                  
 )          
 and           
 (          
   @IsClosed is null          
   or           
   R.status='NEW'          
           
 )          
            
                  
ORDER BY 
R.createdon DESC,
(  
    CASE DeliveryType  
      
    WHEN 'SAMEDAY'  
    THEN 1  
 WHEN 'EXPRESS'  
    THEN 2  
      
    WHEN 'NORMAL'  
    THEN 3  
      
    END  
) ASC  
  
 OFFSET (@next*@offset)-@next ROWS                  
    FETCH NEXT @next ROWS ONLY                  
                  
END                  
                  
                  
                  
---------------------------------------------------                  
--------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[AppSettingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [AppSetting]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Key],[Value]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Key,@Value
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingUpdate

/***** Object:  StoredProcedure  [dbo].[AppSettingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[AppSettingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Key]
	 	,
	 		R.[Value]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [AppSetting] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CountrySelect

/***** Object:  StoredProcedure [dbo].[CountrySelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[AppSettingSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingSelectApp](
@Id	        BIGINT =NULL,
@next 		int = NULL,
@offset 	int = NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Key],
	 		R.[Value]
	FROM [AppSetting] R  
	WHERE 
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.[IsActive] =  1
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[AppSettingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [AppSetting]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Key] = ISNULL(@Key,[Key]),
			 	[Value] = ISNULL(@Value,[Value])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[AppSettingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingXMLSave]
 @AppSettingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Key)[1]', 'NVARCHAR') AS 'Key',
		NDS.DT.value('(Value)[1]', 'NVARCHAR') AS 'Value'
  FROM 
	@AppSettingXml.nodes('/root/AppSetting') AS NDS(DT)
   )
   MERGE INTO AppSetting R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Key] =ISNULL(x.[Key] ,R.[Key]),R.[Value] =ISNULL(x.[Value] ,R.[Value])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Key],
		[Value]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Key],x.[Value]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryInsert

/***** Object:  StoredProcedure [dbo].[CountryInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[BroadcastMsgInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BroadcastMsgInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL,
		    @Message nvarchar(max)=null,
	@TotalUsers bigint =null,
	@Totalbatches bigint =null,
	@lastBatch bigint =null,
	@PendingUsers bigint =null,
	@UserType nvarchar(200) =null,
	@postalCode nvarchar(max) =null,
	@Gender  nvarchar(20) =null,
	@Relation nvarchar(20) =null,
	@OrderCount bigint =null,
	@next  int null,
	@offset  int null

	--select * from [BroadCastMessage]
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [BroadCastMessage]
	  (
	   
       [TenantId]  ,
    [Slug]      ,
    [CreatedBy],
    [UpdatedBy] ,
   -- [CreatedOn] ,
   -- [UpdatedOn] ,
    [IsDeleted] ,
    [IsActive]  ,
    [Message]  ,
	[TotalUsers],
	[Totalbatches] ,
	[lastBatch] ,
	[PendingUsers] ,
	[UserType] ,
	[postalCode] ,
	[Gender] ,
	[Relation] ,
	[OrderCount] ,
	[next]  ,
	[offset]  
	  )
	  VALUES
	  ( 
	   @TenantId,
	   @Slug,
	   @CreatedBy,
	   @CreatedBy,

	   0,
	   @IsActive,			
    @Message ,
	@TotalUsers ,
	@Totalbatches ,
	@lastBatch ,
	@PendingUsers ,
	@UserType ,
	@postalCode,
	@Gender  ,
	@Relation ,
	@OrderCount ,
	@next  ,
	@offset  
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingUpdate

/***** Object:  StoredProcedure  [dbo].[AppSettingUpdate] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[BroadcastMsgUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BroadcastMsgUpdate](    
--  declare
  @Id   bigint=NULL,    
     
 @Message nvarchar(max)=null,    
 @TotalUsers bigint =null,    
 @Totalbatches bigint =null,    
 @lastBatch bigint =null,    
 @PendingUsers bigint =null,    
 @UserType nvarchar(200) =null,    
 @postalCode nvarchar(max) =null,    
 @Gender  nvarchar(20) =null,    
 @Relation nvarchar(20) =null,    
 @OrderCount bigint =null,
 @result nvarchar(max)=null,
 @next  int null,    
 @offset  int null    
    
 --select * from [BroadCastMessage]    
)    AS     
BEGIN    
SET NOCOUNT ON;    
  
 UPDATE BroadCastMessage  
 SET  
       
     [lastBatch] = ISNULL(@lastBatch,[lastBatch]),  
     [PendingUsers] = ISNULL(@PendingUsers,[PendingUsers]),  
     [next] = ISNULL(@next,[next]),  
     [offset] = ISNULL(@offset,[offset]),
	 [Result] =isnull(@result,[Result])
       
  WHERE  
  (  
   [Id]=@Id  
  )  
  
  
  
 END    
     
     
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : AppSettingUpdate    
    
/***** Object:  StoredProcedure  [dbo].[AppSettingUpdate] *****/    
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[BroadcastSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BroadcastSelect](  
--declare
@Id    BIGINT =NULL,  
@IsActive  bit  =NULL,  
@next   int  =NULL,  
@offset   int  =NULL,   
@freetext  NVARCHAR(20)=null,  
@UserId   BIGINT=NULL  
)  AS 
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =1000  
SET @offset=1  
END  
  
 SELECT  
  R.Id,  
 R.CreatedBy,  
 R.UpdatedBy,  
 R.CreatedOn,  
 R.UpdatedOn,  
 R.IsDeleted,  
 R.IsActive, 
 R.[Message],  
 R.[Totalusers],  
 R.[totalbatches],  
 R.lastbatch,  
 R.pendingUsers,  
 R.UserType,  
 R.postalcode,  
 R.Gender,  
 R.Relation,  
 R.orderCount,
 R.[Next],
 R.[offset],
 R.[result],
 overall_count = COUNT(*) OVER() 
     
  
 FROM Broadcastmessage R    
 WHERE   
 (  
  @Id IS NULL  
  OR  
  R.Id = @Id  
 )  
 AND  
 (  
  @IsActive IS NULL  
   OR  
  R.IsActive = @IsActive  
 ) 
 and
 (
  @freetext is null
  or 
  R.[message] like '%'+@freetext+'%'
  
 
 )
   
 AND  
  
 R.IsDeleted=0  
  
 Order by Id desc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END
GO
/****** Object:  StoredProcedure [dbo].[CartInsertApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartInsertApp](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @IsLocked    BIT=NULL,  
     @CustomerId    BIGINT=NULL,  
   @Status NVARCHAR(MAX)=NULL,  
   @Tax    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
   @TotalItems   INT=NULL,  
   @DeliveryType  NVARCHAR(100)=NULL , 

    @DeliveryDate DATETIMEOFFSET(7)=NULL,
    @PickupDate DATETIMEOFFSET(7)=NULL,
    @deliverySlot NVARCHAR(100)=NULL,  
   @pickupSlot NVARCHAR(100)=NULL
   
)  
AS   
BEGIN  
SET NOCOUNT ON;  
   INSERT INTO [Cart]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[IsLocked],[CustomerId],[Status],[Tax],[SubTotal],[TotalPrice],[TotalItems],[DeliveryType],[DeliveryDate],[PickupDate],[deliverySlot],[pickupSlot]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@IsLocked,@CustomerId,@Status,@Tax,@SubTotal,@TotalPrice,@TotalItems,@DeliveryType ,@DeliveryDate,@PickupDate,@deliverySlot,@pickupSlot 
   )  
 SELECT SCOPE_IDENTITY()  
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : CartUpdate  
  
/***** Object:  StoredProcedure  [dbo].[CartUpdate] *****/  
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[CartItemInsertApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemInsertApp](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@ServiceId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CartItem]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CartId],[ProductId],[Quantity],[Price],[TotalPrice],[ServiceId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CartId,@ProductId,@Quantity,@Price,@TotalPrice,@ServiceId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemUpdate

/***** Object:  StoredProcedure  [dbo].[CartItemUpdate] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[CartItemSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemSelectApp](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[CartId]
	 	,
	 		R.[ProductId]
	 	,
	 		R.[Quantity]
	 	,
	 		R.[Price]
	 	,
	 		R.[TotalPrice]
	 	,
			R.[ServiceId]
		,
	overall_count = COUNT(*) OVER()
	FROM [CartItem] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemServiceMappingSelect

/***** Object:  StoredProcedure [dbo].[CartItemServiceMappingSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[CartItemServiceMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CartItemServiceMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CartItemId],[ServiceId],[Quantity],[Price],[SubTotal],[TotalPrice]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CartItemId,@ServiceId,@Quantity,@Price,@SubTotal,@TotalPrice
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemServiceMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CartItemServiceMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CartItemServiceMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[CartItemId]
	 	,
	 		R.[ServiceId]
	 	,
	 		R.[Quantity]
	 	,
	 		R.[Price]
	 	,
	 		R.[SubTotal]
	 	,
	 		R.[TotalPrice]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [CartItemServiceMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderSelect

/***** Object:  StoredProcedure [dbo].[OrderSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CartItemServiceMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CartItemServiceMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CartItemId] = ISNULL(@CartItemId,[CartItemId]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[Quantity] = ISNULL(@Quantity,[Quantity]),
			 	[Price] = ISNULL(@Price,[Price]),
			 	[SubTotal] = ISNULL(@SubTotal,[SubTotal]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemServiceMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CartItemServiceMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingXMLSave]
 @CartItemServiceMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CartItemId)[1]', 'BIGINT') AS 'CartItemId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice'
  FROM 
	@CartItemServiceMappingXml.nodes('/root/CartItemServiceMapping') AS NDS(DT)
   )
   MERGE INTO CartItemServiceMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[CartItemId] =ISNULL(x.[CartItemId] ,R.[CartItemId]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),R.[Price] =ISNULL(x.[Price] ,R.[Price]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[CartItemId],
		[ServiceId],
		[Quantity],
		[Price],
		[SubTotal],
		[TotalPrice]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[CartItemId],x.[ServiceId],x.[Quantity],x.[Price],x.[SubTotal],x.[TotalPrice]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderInsert

/***** Object:  StoredProcedure [dbo].[OrderInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CartItemUpdateApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemUpdateApp]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@ServiceId   BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CartItem]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CartId] = ISNULL(@CartId,[CartId]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId]),
			 	[Quantity] = ISNULL(@Quantity,[Quantity]),
			 	[Price] = ISNULL(@Price,[Price]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),
				[ServiceId] =  ISNULL(@ServiceId,[ServiceId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemXMLSave
GO
/****** Object:  StoredProcedure [dbo].[CartItemUpdateByService]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemUpdateByService]    
 --declare
  @Id    BIGINT=NULL,      
     @TenantId   INT=NULL,      
     @Slug NVARCHAR(MAX)=NULL,      
     @UpdatedBy    BIGINT=NULL,      
     @IsDeleted    BIT=1,      
     @IsActive    BIT=NULL,      
     @CartId    BIGINT=NULL,      
     @ProductId    BIGINT=NULL,      
     @Quantity   INT=NULL,      
     @Price    DECIMAL(18,2)=NULL,      
     @TotalPrice    DECIMAL(18,2)=NULL,      
     @ServiceId   BIGINT=NULL      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
  declare @TotalItems bigint    
    
    UPDATE [CartItem]      
 SET      
     [Slug] = ISNULL(@Slug,[Slug]),      
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),      
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),       
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),      
     [IsActive] = ISNULL(@IsActive,[IsActive]),      
     [CartId] = ISNULL(@CartId,[CartId]),      
     [ProductId] = ISNULL(@ProductId,[ProductId]),      
     [Quantity] = ISNULL(@Quantity,[Quantity]),      
     [Price] = ISNULL(@Price,[Price]),      
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice])    
        
  WHERE      
  (      
   CartId=(select top 1 Id from cart where customerId= @Id  )    
  )      
  AND      
  (      
   ServiceId=@ServiceId    
  )      
    
  --select * from cart    
  select @TotalItems= isnull( sum(Quantity),0) from CartItem where cartid in(    
  select top 1 Id from cart where customerId= @Id) and isdeleted=0     
    
   declare @DeliveryType nvarchar(50)  
    if(@TotalItems=0)  
 begin   
   set @DeliveryType='NORMAL'  
   update cart set  DeliveryType = @DeliveryType where CustomerId=@Id    
 end  
  
  update cart set  [TotalItems] = ISNULL(@TotalItems,[TotalItems]) where CustomerId=@Id    
    
END     


--select * from cart where customerId=204
--select * from cartitem where cartid=153


      
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : CartItemXMLSave 

GO
/****** Object:  StoredProcedure [dbo].[CartItemXMLSaveApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemXMLSaveApp]
 @CartItemXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CartId)[1]', 'BIGINT') AS 'CartId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId'

		
  FROM 
	@CartItemXml.nodes('/ArrayOfCartItemModel/CartItemModel') AS NDS(DT)
   )
   MERGE INTO CartItem R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[CartId] =ISNULL(x.[CartId] ,R.[CartId]),
	R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),
	R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),
	R.[Price] =ISNULL(x.[Price] ,R.[Price]),
	R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),
	R.[ServiceId] =  ISNULL(x.[ServiceId],R.[ServiceId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		[IsActive],
		[CartId],
		[ProductId],
		[Quantity],
		[Price],
		[TotalPrice],
		[ServiceId]
	)
    VALUES(
		x.[TenantId],
		x.[Slug],
		x.[CreatedBy],
		x.[UpdatedBy],
		x.[IsActive],
		x.[CartId],
		x.[ProductId],
		x.[Quantity],
		x.[Price],
		x.[TotalPrice],
		x.[ServiceId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemServiceMappingInsert

/***** Object:  StoredProcedure [dbo].[CartItemServiceMappingInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[CartSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartSelectApp](        
--declare  
@Id         BIGINT =NULL,        
@Slug     NVARCHAR(255)=NULL,        
@next   int = NULL,        
@offset  int = NULL,        
@RelationTable NVARCHAR(50)=NULL,        
@RelationId bigint=NULL,        
@TenantId   INT=NULL,        
@LanguageId  BIGINT=NULL,        
@CustomerId      BIGINT=null        
)      AS  
BEGIN        
        
IF @next IS NULL        
BEGIN        
SET @next =100000        
SET @offset=1        
END        
        
 SELECT        
    R.[Id]        
   ,        
    R.[TenantId]        
   ,        
    R.[Slug]        
   ,        
    R.[CreatedBy]        
   ,        
    R.[UpdatedBy]        
   ,        
    R.[CreatedOn]        
   ,        
    R.[UpdatedOn]        
   ,        
    R.[IsDeleted]        
   ,        
    R.[IsActive]        
   ,        
    R.[IsLocked]        
   ,        
    R.[CustomerId]        
   ,        
    R.[Status]        
   ,        
    R.[Tax]        
   ,        
    R.[SubTotal]        
   ,        
    R.[TotalPrice]        
   ,        
    R.[TotalItems]        
   ,        
  R.[DeliveryType],        
      
  R.Deliverydate,      
  R.Pickupdate,      
  R.deliverySlot,      
  R.pickupSlot,      
  --select * FROM [Cart]      
      R.laundryInstruction,
 overall_count = COUNT(*) OVER(),        
 CartItemXml = (        
  select        
  cartitem.Id,        
  cartitem.ProductId,        
  cartitem.CartId,        
  cartitem.ServiceId,        
  --cartitem.TotalPrice,     
   pp.Price*cartitem.Quantity TotalPrice,     
  cartitem.Quantity,        
  product.Name  AS ProductName,        
  [service].Name  AS ServiceName,        
  (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  cartitem.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductNameL,        
  (SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  CartItem.ServiceId AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceNameL,        
  (SELECT TOP(1) [Path] FROM FileGroupItems FG WHERE FG.[TypeId] = cartitem.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0 ) AS [Path],        
  (SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ServiceId =  cartitem.ServiceId AND PP.ProductId =  cartitem.ProductId AND PP.[IsDeleted] = 0 ) AS Price        
          
        
  FROM         
  CartItem cartitem         
  INNER JOIN         
  Product product ON product.Id =  cartitem.ProductId        
  INNER JOIN        
  [Service] [service] ON [service].Id =  cartitem.ServiceId        
  left join     
  productprice pp on cartitem.productid =pp.productid and cartitem.serviceid=pp.serviceid        
  WHERE cartitem.CartId =  R.Id AND cartitem.IsDeleted = 0  and pp.IsDeleted=0      
  FOR XML AUTO,ROOT,ELEMENTs        
 )        
 FROM [Cart] R          
 WHERE         
 (        
  @RelationTable IS NULL        
 )        
 AND        
 (        
  @Id IS NULL        
  OR        
  R.[Id] = @Id        
 )        
 AND        
 (        
  @Slug IS NULL        
  OR        
  R.[Slug] = @Slug        
 )        
 AND        
 (        
  R.[IsDeleted] = 0        
 )        
 AND        
 (        
  @TenantId IS NULL        
  OR        
  R.[TenantId] = @TenantId        
 )        
 AND        
 (        
  @CustomerId IS NULL        
   OR        
  R.[CustomerId] =  @CustomerId        
 )        
        
 Order by R.Id desc        
 OFFSET (@next*@offset)-@next ROWS        
    FETCH NEXT @next ROWS ONLY        
        
END        
        
        
        
---------------------------------------------------        
---------------------------------------------------        
-- Procedure : CartItemSelect        
        
/***** Object:  StoredProcedure [dbo].[CartItemSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[CartUpdateApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartUpdateApp]        
     @Id    BIGINT=NULL,        
   @TenantId   INT=NULL,        
   @Slug NVARCHAR(MAX)=NULL,        
     @UpdatedBy    BIGINT=NULL,        
     @IsDeleted    BIT=NULL,        
     @IsActive    BIT=NULL,        
     @IsLocked    BIT=NULL,        
     @CustomerId    BIGINT=NULL,        
   @Status NVARCHAR(MAX)=NULL,        
   @Tax    DECIMAL(18,2)=NULL,        
   @SubTotal    DECIMAL(18,2)=NULL,        
   @TotalPrice    DECIMAL(18,2)=NULL,        
   @TotalItems   INT=NULL,        
   @DeliveryType  NVARCHAR(100)=NULL  ,      
      
   @DeliveryDate DATETIMEOFFSET(7)=NULL,      
    @PickupDate DATETIMEOFFSET(7)=NULL,      
    @deliverySlot NVARCHAR(100)=NULL,        
   @pickupSlot NVARCHAR(100)=NULL  ,
   @laundryInstruction Nvarchar(max)=null
AS        
BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
 if(@IsLocked=1)    
 begin     
   INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )      
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,@IsActive,@id,'CART IS LOCKED' )     
 end    
     
    if(@TotalItems=0)  
 begin   
   set @DeliveryType='NORMAL'  
 end  
  
    
    UPDATE [Cart]        
 SET        
     [Slug] = ISNULL(@Slug,[Slug]),        
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),        
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),         
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),        
     [IsActive] = ISNULL(@IsActive,[IsActive]),        
     [IsLocked] = ISNULL(@IsLocked,[IsLocked]),        
     [CustomerId] = ISNULL(@CustomerId,[CustomerId]),        
     [Status] = ISNULL(@Status,[Status]),        
     [Tax] = ISNULL(@Tax,[Tax]),        
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),        
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),        
     [TotalItems] = ISNULL(@TotalItems,[TotalItems]),        
    [DeliveryType]  =  ISNULL(@DeliveryType,[DeliveryType])  ,      
      
  DeliveryDate = ISNULL(@DeliveryDate,DeliveryDate),        
     PickupDate = ISNULL(@PickupDate,PickupDate),        
     deliverySlot = ISNULL(@deliverySlot,deliverySlot),        
    pickupSlot  =  ISNULL(@pickupSlot,pickupSlot),
	laundryInstruction=isnull(@laundryInstruction,laundryInstruction)
      
      
  WHERE        
  (        
   [Id]=@Id        
  )        
  AND        
  (        
  [TenantId] =  @TenantId        
  )        
END        
        
        
---------------------------------------------------        
---------------------------------------------------        
-- Procedure : CartXMLSave
GO
/****** Object:  StoredProcedure [dbo].[CartWithItemDelete]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartWithItemDelete]    
     @Id    BIGINT=NULL,    
   @TenantId   INT=NULL,    
   @Slug NVARCHAR(MAX)=NULL,    
     @UpdatedBy    BIGINT=NULL,    
     @IsDeleted    BIT=NULL,    
     @IsActive    BIT=NULL,    
     @IsLocked    BIT=NULL,    
     @CustomerId    BIGINT=NULL,    
   @Status NVARCHAR(MAX)=NULL,    
   @Tax    DECIMAL(18,2)=NULL,    
   @SubTotal    DECIMAL(18,2)=NULL,    
   @TotalPrice    DECIMAL(18,2)=NULL,    
   @TotalItems   INT=NULL,    
   @DeliveryType  NVARCHAR(100)=NULL    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
  if exists (select * from Cart where id=@Id  and IsLocked=1 )  
begin     
   delete from cartitem where CartId=@Id  
   delete from Cart where id=@Id     

       
   declare @note varchar(max)='cart Deleted'
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )  
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,1,@Id,@note)


end  
      
END    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : CartXMLSave
GO
/****** Object:  StoredProcedure [dbo].[CartXMLSaveApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartXMLSaveApp]
 @CartXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
	  	NDS.DT.value('(IsLocked)[1]', 'BIT') AS 'IsLocked',
		NDS.DT.value('(CustomerId)[1]', 'BIGINT') AS 'CustomerId',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(Tax)[1]', 'DECIMAL(18,2)') AS 'Tax',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		NDS.DT.value('(TotalItems)[1]', 'INT') AS 'TotalItems'
  FROM 
	@CartXml.nodes('/root/Cart') AS NDS(DT)
   )
   MERGE INTO Cart R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[IsLocked] =ISNULL(x.[IsLocked] ,R.[IsLocked]),R.[CustomerId] =ISNULL(x.[CustomerId] ,R.[CustomerId]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[Tax] =ISNULL(x.[Tax] ,R.[Tax]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),R.[TotalItems] =ISNULL(x.[TotalItems] ,R.[TotalItems])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[IsLocked],
		[CustomerId],
		[Status],
		[Tax],
		[SubTotal],
		[TotalPrice],
		[TotalItems]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[IsLocked],x.[CustomerId],x.[Status],x.[Tax],x.[SubTotal],x.[TotalPrice],x.[TotalItems]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemInsert

/***** Object:  StoredProcedure [dbo].[CartItemInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[CheckUserCallingDeviceStatus]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================      
-- Author:  <Author,,Name>      
-- Create date: <Create Date,,>      
-- Description: <Description,,>  select * from users     
-- =============================================      
CREATE PROCEDURE [dbo].[CheckUserCallingDeviceStatus]      
 -- Add the parameters for the stored procedure here      
 @FromUserId BIGINT = NULL,      
 @ToUserId BIGINT = NULL      
AS      
    
    
    
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
  If Exists (select * from UserDevice where UserId=@ToUserId  and IsDeleted =0
  )      
  BEGIN      
   Select CAST(0 AS BIT)       
  END       
  ELSE      
 BEGIN      
  Select CAST(1 AS BIT)       
 END       
      
END  
GO
/****** Object:  StoredProcedure [dbo].[CheckUserCallingStatus]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>  select * from users   
-- =============================================    
CREATE PROCEDURE [dbo].[CheckUserCallingStatus]    
 -- Add the parameters for the stored procedure here    
 @FromUserId BIGINT = NULL,    
 @ToUserId BIGINT = NULL    
AS    
  
  
  
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  If Exists (Select Id  from users     
  WHERE     
  (      
    (    
    id = @FromUserId   
    )    
   OR    
    (    
    id = @ToUserId   
    )    
  )    
   AND IsActive = 1  
   AND callStatus='IDLE'  
  )    
  BEGIN    
   Select CAST(0 AS BIT)     
  END     
  ELSE    
 BEGIN    
  Select CAST(1 AS BIT)     
 END     
    
END
GO
/****** Object:  StoredProcedure [dbo].[CheckUserCallingStatus_ori]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[CheckUserCallingStatus_ori]  
 -- Add the parameters for the stored procedure here  
 @FromUserId BIGINT = NULL,  
 @ToUserId BIGINT = NULL  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  If Exists (Select Id  from UserTwilioCallingStatus   
  WHERE   
  (    
    (  
    FromId = @FromUserId OR ToId = @FromUserId  
    )  
   OR  
    (  
    FromId = @ToUserId OR ToId = @ToUserId  
    )  
  )  
   AND IsActive = 1  
  )  
  BEGIN  
   Select CAST(1 AS BIT)   
  END   
  ELSE  
 BEGIN  
  Select CAST(0 AS BIT)   
 END   
  
END
GO
/****** Object:  StoredProcedure [dbo].[CityInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@StateId    BIGINT=NULL,
			@Name       NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [City]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[StateId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@StateId,
	   @Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CityUpdate

/***** Object:  StoredProcedure  [dbo].[CityUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CityLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@CityId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CityLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[CityId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@CityId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CityLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CityLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CityLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[CityId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [CityLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : UserAddressSelect

/***** Object:  StoredProcedure [dbo].[UserAddressSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CityLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@CityId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CityLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[CityId] = ISNULL(@CityId,[CityId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CityLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CityLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityLanguageMappingXMLSave]
 @CityLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(CityId)[1]', 'BIGINT') AS 'CityId'
  FROM 
  
	@CityLanguageMappingXml.nodes('/ArrayOfCityLanguageMappingModel/CityLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO CityLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[CityId] =ISNULL(x.[CityId] ,R.[CityId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[CityId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[CityId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : UserAddressInsert

/***** Object:  StoredProcedure [dbo].[UserAddressInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CitySelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CitySelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[StateId]
	 	,
		R.Name,
		S.Name AS StateName,

	overall_count = COUNT(*) OVER(),
	CityLanguageXml = (
		select
		citylanguagemapping.Id,
		citylanguagemapping.Name,
		citylanguagemapping.LanguageId,
		citylanguagemapping.CityId,
		[language].Name AS LanguageName

		FROM 
		CityLanguageMapping citylanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = citylanguagemapping.LanguageId
		WHERE citylanguagemapping.CityId =  R.Id AND citylanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [City] R  
	INNER JOIN [State] S ON S.[Id] = R.[StateId] AND S.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
			OR
		(
			@RelationTable = 'State'
			 AND
			 R.[StateId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CityLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[CityLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CityUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@StateId    BIGINT=NULL,
			@Name       NVARCHAR(100)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [City]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[StateId] = ISNULL(@StateId,[StateId]),
				[Name]    = ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CityXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CityXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityXMLSave]
 @CityXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(StateId)[1]', 'BIGINT') AS 'StateId'
  FROM 
	@CityXml.nodes('/root/City') AS NDS(DT)
   )
   MERGE INTO City R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[StateId] =ISNULL(x.[StateId] ,R.[StateId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[StateId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[StateId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CityLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[CityLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CompanySelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CompanySelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.Longititude
	 	,
	 		R.Latitude
	 	,
	 		R.userId
	 	,
	overall_count = COUNT(*) OVER()
	FROM [DriverLocation] R  
	WHERE 
	(
		@RelationTable IS NULL
		or
		(		
		@RelationTable = 'User'
		and
		R.userId =@RelationId
		)	
		
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyOwnerSelect

/***** Object:  StoredProcedure [dbo].[CompanyOwnerSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[CountryInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Country]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryUpdate

/***** Object:  StoredProcedure  [dbo].[CountryUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CountryLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@CountryId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CountryLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[CountryId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@CountryId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CountryLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CountryLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[CountryId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [CountryLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : LanguageSelect

/***** Object:  StoredProcedure [dbo].[LanguageSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CountryLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@CountryId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CountryLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[CountryId] = ISNULL(@CountryId,[CountryId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CountryLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryLanguageMappingXMLSave]
 @CountryLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(CountryId)[1]', 'BIGINT') AS 'CountryId'
  FROM 
  
	@CountryLanguageMappingXml.nodes('/ArrayOfCountryLanguageMappingModel/CountryLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO CountryLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[CountryId] =ISNULL(x.[CountryId] ,R.[CountryId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[CountryId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[CountryId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : LanguageInsert

/***** Object:  StoredProcedure [dbo].[LanguageInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CountrySelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountrySelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
		R.[Name],
	overall_count = COUNT(*) OVER(),
	CountryLanguageXml = (
		select
		countrylanguagemapping.Id,
		countrylanguagemapping.Name,
		countrylanguagemapping.LanguageId,
		countrylanguagemapping.CountryId,
		[language].Name AS LanguageName

		FROM 
		CountryLanguageMapping countrylanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = countrylanguagemapping.LanguageId
		WHERE countrylanguagemapping.CountryId =  R.Id AND countrylanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Country] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[CountryLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CountryUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Country]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
				[Name]     =  ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CountryXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CountryXMLSave]
 @CountryXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive'
  FROM 
	@CountryXml.nodes('/root/Country') AS NDS(DT)
   )
   MERGE INTO Country R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[CountryLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CurrencyInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Currency]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyUpdate

/***** Object:  StoredProcedure  [dbo].[CurrencyUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CurrencyId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CurrencyLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CurrencyId],[LanguageId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CurrencyId,@LanguageId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CurrencyLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[CurrencyId]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[Name]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [CurrencyLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
--@@@@NEW_SELECT_PROCEDURE@@@@
---------------------------------------------------
---------------------------------------------------

GO
/****** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CurrencyId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CurrencyLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CurrencyId] = ISNULL(@CurrencyId,[CurrencyId]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Name] = ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyLanguageMappingXMLSave]
 @CurrencyLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CurrencyId)[1]', 'BIGINT') AS 'CurrencyId',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name'
  FROM 
	@CurrencyLanguageMappingXml.nodes('/root/CurrencyLanguageMapping') AS NDS(DT)
   )
   MERGE INTO CurrencyLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[CurrencyId] =ISNULL(x.[CurrencyId] ,R.[CurrencyId]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Name] =ISNULL(x.[Name] ,R.[Name])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[CurrencyId],
		[LanguageId],
		[Name]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[CurrencyId],x.[LanguageId],x.[Name]
    );
END
---------------------------------------------------
---------------------------------------------------
--@@@@NEW_TABLE_PROCEDURE@@@@
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CurrencySelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencySelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Price]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Currency] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[CurrencyUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Price    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Currency]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Price] = ISNULL(@Price,[Price])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyXMLSave


GO
/****** Object:  StoredProcedure [dbo].[CurrencyXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyXMLSave]
 @CurrencyXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price'
  FROM 
	@CurrencyXml.nodes('/root/Currency') AS NDS(DT)
   )
   MERGE INTO Currency R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Price] =ISNULL(x.[Price] ,R.[Price])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Price]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Price]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[CustomerSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CustomerSelect]     
--declare  
@Id          BIGINT=NULL,    
@Role        NVARCHAR(20)=NULL,    
@next    INT = NULL,    
@offset   INT = NULL    
    
AS  
BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
SELECT     
       U.[Id],    
    U.[IsActive],    
    U.[IsDeleted],    
    U.[FirstName],    
    U.[LastName],    
    U.[Email],    
    U.[PhoneNumber],    
    U.[UserName],    
    U.[UniqueCode],    
    U.[TenantId],    
    R.[Name] AS RoleName,    
 case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'   
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER' end as Role   
      , overall_count = COUNT (*) OVER()     
  ,U.EmailConfirmed,U.PhoneNumberConfirmed,U.LanguageId,U.Status,U.NickName,U.CountryCode,U.DOB,U.Gender
  --select * from Users
  ,FileXml=(        
     SELECT         
      FG.[Id],        
      FG.[CreatedBy],        
      FG.[UpdatedBy],        
      FG.[CreatedOn],        
      FG.[UpdatedOn],        
      FG.[IsDeleted],        
      FG.[IsActive],        
      FG.[Filename],        
      FG.[MimeType],        
      FG.[Thumbnail],        
      FG.[Size],        
      FG.[Path],        
      FG.[OriginalName],        
      FG.[OnServer],        
      FG.[TypeId],        
      FG.[Type]        
      FROM FileGroupItems FG        
      WHERE FG.[TypeId] = U.[Id] AND FG.[Type]='USERS_PROFILEPIC' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0        
      FOR XML AUTO,ROOT,ELEMENTs        
    )
    
 FROM     
 Users U     
 INNER JOIN UserRoles UR ON U.Id = UR.UserId     
 INNER JOIN Roles R ON R.Id = UR.RoleId     
    
WHERE     
 (     
 @Id IS NULL     
 OR     
 U.Id=@Id     
 )     
    
 AND    
 (     
 @Role IS NULL     
 OR     
 R.Name = @Role    
 )     
    
 AND    
 (    
  U.IsDeleted = 0    
 )    
    
   ORDER BY U.Id DESC     
   OFFSET (@next*@offset)-@next ROWS    
   FETCH NEXT @next ROWS ONLY    
    
END    
---------------------------------------------------    
---------------------------------------------------     
     
SET ANSI_NULLS ON    
GO
/****** Object:  StoredProcedure [dbo].[CustomerWithAddressSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CustomerWithAddressSelect]                                   
--declare                                
@Id          BIGINT=NULL,                                  
@Role        NVARCHAR(20)=null,                                  
@next    INT = NULL,                                  
@offset   INT = NULL,                             
@RelationTable NVARCHAR(50)=NULL,                                
@RelationId bigint=NULL ,                 
@SortColumn nvarchar(200)=null  ,  
   @SortOrder            nvarchar(200)=null                                   
AS                                
BEGIN                                  
                                
  declare @temprole varchar(50)=null;                              
 if(@Role='ROLE_ADMIN')                              
 begin                               
  set  @temprole='ROLE_ADMIN';                              
  set @role=null;                              
 end                              
                              
IF @next IS NULL                                  
BEGIN                                  
SET @next =100000                                  
SET @offset=1                                  
END                                  
                                
SELECT                                   
    U.[Id],                                  
    U.[IsActive],                                  
    U.[IsDeleted],                                  
    U.[FirstName],                                  
    U.[LastName],                                  
    U.[Email],                                  
    U.[PhoneNumber],                                  
    U.[UserName],                                  
    U.[UniqueCode],                                  
    U.[TenantId],                    
    U.adminNote,               
    R.[Name] AS RoleName,                                  
    case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'                                 
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'                               
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'                               
   end as Role                                 
      ,                            
   U.Gender,                            
   U.EmployeeId,                            
   U.Position,                            
   U.NationalId,                            
   U.LicenceId,                            
   U.BikeInformation,                            
   U.LicencePlate,                            
   U.DOB,                            
    U.Status,                           
    U.NickName,                       
    U.EmailConfirmed,                    
                  
 U.PhoneNumberConfirmed,              
              
 U.languageid,              
 --select * from users               
              
 AddressXml=(                                
     SELECT                    
    R.[Id],                    
    R.[AddressLine1],                    
   R.[AddressLine2],                    
    R.[DistrictId],                    
    R.[SubDistrictId],                    
    R.[PostalCode],                    
    R.[Tag],                    
    R.[Latitude],                    
    R.[Longitude],                    
   R.[HouseNumber],                    
   R.[StreetNumber],                    
   R.[Note],                    
   R.[Type],                    
                    
                  
   R.[BuildingName] ,                  
   R.[Floor] ,                  
   R.[UnitNo] ,                  
   R.[PhoneNo] ,                  
   R.alterNumber ,                  
   R.[ResidenceType] ,                  
                  
                       
   D.Name  AS DistrictName,                    
   SD.Name  AS SubDistrictName,          
    PD.Name As ProvinceName,          
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =U.LanguageId ) AS DistrictNameL,                    
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =U.LanguageId  ) AS SubDistrictNameL            ,        
      (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =U.LanguageId ) AS ProvinceNameL                    
                  
                    
 FROM [UserAddress] R INNER JOIN                    
 [District] D ON D.Id =  R.[DistrictId]                    
 INNER JOIN                     
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId        
 INNER JOIN                     
 Province PD ON PD.Id =  R.ProvinceId        
        
        
 where R.userId=U.ID              
      FOR XML AUTO,ROOT,ELEMENTs                                
    )                 
 ,U.ProfilePic                
 ,FileXml=(                            
     SELECT                             
      FG.[Id],                            
      FG.[CreatedBy],                            
      FG.[UpdatedBy],                            
      FG.[CreatedOn],                            
      FG.[UpdatedOn],                            
      FG.[IsDeleted],                            
      FG.[IsActive],                            
      FG.[Filename],                            
      FG.[MimeType],                            
      FG.[Thumbnail],                        
      FG.[Size],                            
      FG.[Path],                            
      FG.[OriginalName],                            
      FG.[OnServer],                            
      FG.[TypeId],                
      FG.[Type]                            
      FROM FileGroupItems FG                            
      WHERE FG.[TypeId] = U.Id AND FG.[Type]='PROFILE_PIC' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0                            
      FOR XML AUTO,ROOT,ELEMENTs                            
    ) ,                  
                  
   overall_count = COUNT (*) OVER()                                   
                                
                                 
 FROM                                   
 Users U                                   
 INNER JOIN UserRoles UR ON U.Id = UR.UserId                                   
 INNER JOIN Roles R ON R.Id = UR.RoleId                                   
                                  
WHERE                                   
 (                                   
 @Id IS NULL                                   
 OR                                   
 U.Id=@Id                                   
 )                                   
                                  
 AND                                  
 (                                   
 @Role IS NULL                                   
 OR                                   
 R.Name = @Role                                  
 )                                   
                              
 AND                                  
 (                                   
 @temprole IS NULL                                   
  or                                                             
     R.[Name] in (select [name] from roles where id in (1,5))                              
                                   
 )                              
 and                     
U.isdeleted=0                    
              
     
   ORDER BY    
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='ASC')   THEN [FirstName]   END ASC,    
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='DESC')  THEN [FirstName]    END DESC,    
  
            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='ASC')   THEN NickName   END ASC,    
            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='DESC')  THEN NickName    END DESC,    
              
   CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='ASC')   THEN PhoneNumber   END ASC,    
   CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='DESC')  THEN PhoneNumber    END DESC,   
  
   CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='ASC')   THEN email   END ASC,    
            CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='DESC')  THEN email    END DESC,   
     
   CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='ASC')   THEN EmailConfirmed   END ASC,    
            CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='DESC')  THEN EmailConfirmed    END DESC,   
  
   --         CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='ASC')   THEN ordersCount   END ASC,    
   --         CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='DESC')  THEN ordersCount    END DESC,   
  
   --CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='ASC')   THEN lastOrderDate   END ASC,    
   --         CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='DESC')  THEN lastOrderDate    END DESC ,  
     
   CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,    
            CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC,   
  
   CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,    
            CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC,   
              
   CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN LicencePlate   END ASC,    
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN LicencePlate    END DESC,   
  
   CASE WHEN (@SortColumn = 'ID' AND @SortOrder='ASC')   THEN U.id   END ASC,    
            CASE WHEN (@SortColumn = 'ID' AND @SortOrder='DESC')  THEN U.id    END DESC,   
  
  
   CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN [STATUS]   END ASC,    
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN [STATUS]    END DESC  
                                  
 --  ORDER BY U.Id DESC   
     
  
  
   OFFSET (@next*@offset)-@next ROWS                                  
   FETCH NEXT @next ROWS ONLY                                  
                                  
END                                  
---------------------------------------------------                                  
---------------------------------------------------                                   
                                   
SET ANSI_NULLS ON  
GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageInsert](      
--  declare    
   @TenantId   INT=null,      
   @Slug NVARCHAR(MAX)=null,      
     @CreatedBy    BIGINT=null,      
     @IsActive    BIT=null,      
   @Type NVARCHAR(MAX)=null  ,    
   @message nvarchar(max)  =null  
)    AS       
BEGIN      
SET NOCOUNT ON;     
  if(@Type is null)
  begin 
   set @Type='defaultmessage'
  end
   INSERT INTO [DefaultMessage]      
   (      
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Type],[message]      
   )      
   VALUES      
   (       
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Type  ,@message    
   )      
 SELECT SCOPE_IDENTITY()      
 END      
       
       
  --select * from DefaultMessage    
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : DefaultMessageUpdate      
      
/***** Object:  StoredProcedure  [dbo].[DefaultMessageUpdate] *****/      
SET ANSI_NULLS ON      
GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DefaultMessageId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DefaultMessageLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[DefaultMessageId],[LanguageId],[Message]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@DefaultMessageId,@LanguageId,@Message
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[DefaultMessageLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[DefaultMessageId]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[Message]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [DefaultMessageLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencySelect

/***** Object:  StoredProcedure [dbo].[CurrencySelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DefaultMessageId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [DefaultMessageLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[DefaultMessageId] = ISNULL(@DefaultMessageId,[DefaultMessageId]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Message] = ISNULL(@Message,[Message])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingXMLSave]
 @DefaultMessageLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(DefaultMessageId)[1]', 'BIGINT') AS 'DefaultMessageId',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Message)[1]', 'NVARCHAR(MAX)') AS 'Message'
  FROM 
	@DefaultMessageLanguageMappingXml.nodes('/root/DefaultMessageLanguageMapping') AS NDS(DT)
   )
   MERGE INTO DefaultMessageLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[DefaultMessageId] =ISNULL(x.[DefaultMessageId] ,R.[DefaultMessageId]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Message] =ISNULL(x.[Message] ,R.[Message])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[DefaultMessageId],
		[LanguageId],
		[Message]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[DefaultMessageId],x.[LanguageId],x.[Message]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyInsert

/***** Object:  StoredProcedure [dbo].[CurrencyInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageSelect](    
@Id         BIGINT =NULL,    
@Slug     NVARCHAR(255)=NULL,    
@next   int = NULL,    
@offset  int = NULL,    
@RelationTable NVARCHAR(50)=NULL,    
@RelationId bigint=NULL,    
@TenantId   INT=NULL    
)    
AS BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
 SELECT    
    R.[Id]    
   ,    
    R.[TenantId]    
   ,    
    R.[Slug]    
   ,    
    R.[CreatedBy]    
   ,    
    R.[UpdatedBy]    
   ,    
    R.[CreatedOn]    
   ,    
    R.[UpdatedOn]    
   ,    
    R.[IsDeleted]    
   ,    
    R.[IsActive]    
   ,    
    R.[Type]    
 , R.[message]  
   ,    
 overall_count = COUNT(*) OVER()    
 FROM [DefaultMessage] R      
 WHERE     
 (    
  @RelationTable IS NULL    
 )    
 AND    
 (    
  @Id IS NULL    
  OR    
  R.[Id] = @Id    
 )    
 AND    
 (    
  @Slug IS NULL    
  OR    
  R.[Slug] = @Slug    
 )    
 AND    
 (    
  R.[IsDeleted] = 0    
 )    
 AND    
 (    
  @TenantId IS NULL    
  OR    
  R.[TenantId] = @TenantId    
 )    
    
 Order by R.Id desc    
 OFFSET (@next*@offset)-@next ROWS    
    FETCH NEXT @next ROWS ONLY    
    
END    
    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : DefaultMessageLanguageMappingSelect    
    
/***** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingSelect] ***/    
GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageUpdate]    
     @Id    BIGINT=NULL,    
   @TenantId   INT=NULL,    
   @Slug NVARCHAR(MAX)=NULL,    
     @UpdatedBy    BIGINT=NULL,    
     @IsDeleted    BIT=NULL,    
     @IsActive    BIT=NULL,    
   @Type NVARCHAR(MAX)=NULL ,  
   @message nvarchar(max)  
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    UPDATE [DefaultMessage]    
 SET    
     [Slug] = ISNULL(@Slug,[Slug]),    
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),    
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),     
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),    
     [IsActive] = ISNULL(@IsActive,[IsActive]),    
     [Type] = ISNULL(@Type,[Type])  ,  
  [message] = ISNULL(@message,[message])  
  WHERE    
  (    
   [Id]=@Id    
  )    
  AND    
  (    
  [TenantId] =  @TenantId    
  )    
END    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : DefaultMessageXMLSave    
    
GO
/****** Object:  StoredProcedure [dbo].[DefaultMessageXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DefaultMessageXMLSave]
 @DefaultMessageXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type'
  FROM 
	@DefaultMessageXml.nodes('/root/DefaultMessage') AS NDS(DT)
   )
   MERGE INTO DefaultMessage R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Type] =ISNULL(x.[Type] ,R.[Type])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Type]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Type]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[DefaultMessageLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DeliveryAddressInsertApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE PROCEDURE [dbo].[DeliveryAddressInsertApp](          
          
   @TenantId   INT=NULL,          
   @Slug NVARCHAR(MAX)=NULL,          
     @CreatedBy    BIGINT=NULL,          
     @IsActive    BIT=NULL,          
     @UserId    BIGINT=NULL,          
   @AddressLine1 NVARCHAR(MAX)=NULL,          
   @AddressLine2 NVARCHAR(MAX)=NULL,          
     @DistrictId    BIGINT=NULL,          
     @SubDistrictId    BIGINT=NULL,          
   @PostalCode NVARCHAR(MAX)=NULL,          
     @IsDefault    BIT=NULL,          
   @Tag NVARCHAR(MAX)=NULL,          
     @ProvinceId    BIGINT=NULL,          
   @Latitude    DECIMAL(18,6)=NULL,          
   @Longitude    DECIMAL(18,6)=NULL,          
   @Type        NVARCHAR(100)=NULL,          
   @HouseNumber NVARCHAR(100)=NULL,          
   @StreetNumber NVARCHAR(100)=NULL,          
   @Note         NVARCHAR(MAX)=NULL  ,        
        
   @BuildingName NVARCHAR(100)=NULL,         
   @Floor NVARCHAR(100)=NULL,         
   @UnitNo NVARCHAR(100)=NULL,         
   @PhoneNo NVARCHAR(100)=NULL,         
   @alterNumber NVARCHAR(100)=NULL,        
   @ResidenceType   NVARCHAR(100)=NULL ,    
   @OrderId bigint =null    
        
)          
AS           
BEGIN          
 declare @result decimal;  
SET NOCOUNT ON;          
   INSERT INTO [DeliveryAddress]          
   (          
    [TenantId],          
    [Slug],          
    [CreatedBy],          
    [UpdatedBy],          
    [IsActive],          
    [UserId],          
    [AddressLine1],          
    [AddressLine2],          
    [DistrictId],          
    [SubDistrictId],          
    [PostalCode],          
    [IsDefault],          
    [Tag],          
    [ProvinceId],          
    [Latitude],          
    [Longitude],          
    [Type],          
    [HouseNumber],          
    [StreetNumber],          
    [Note]  ,        
    [BuildingName] ,         
   [Floor] ,        
   [UnitNo] ,        
   [PhoneNo] ,        
   alterNumber ,        
   [ResidenceType]  ,    
   [OrderId]    
   )          
   VALUES          
   (           
    @TenantId,          
    @Slug,          
    @CreatedBy,          
    @CreatedBy,          
    @IsActive,          
    @UserId,          
    @AddressLine1,          
    @AddressLine2,          
    @DistrictId,          
    @SubDistrictId,          
    @PostalCode,          
    @IsDefault,          
    @Tag,          
    @ProvinceId,          
    @Latitude,          
    @Longitude,          
    @Type,          
    @HouseNumber,          
    @StreetNumber,          
    @Note  ,        
        
        
 @BuildingName ,         
   @Floor ,        
   @UnitNo ,        
   @PhoneNo ,        
   @alterNumber ,        
   @ResidenceType ,    
   @OrderId    
   )          
              
   
  SELECT @result=SCOPE_IDENTITY()      
   declare @Lognote varchar(max)='Delivery Address Saved for the order';  
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )    
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Lognote)  
    
 select cast (@result as decimal)  
  
  
 END          
           
           
          
---------------------------------------------------          
---------------------------------------------------          
-- Procedure : UserAddressUpdate          
          
/***** Object:  StoredProcedure  [dbo].[UserAddressUpdate] *****/          
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@PriceType NVARCHAR(MAX)=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DeliveryType]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PriceType],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PriceType,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeUpdate

/***** Object:  StoredProcedure  [dbo].[DeliveryTypeUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@DeliveryTypeId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DeliveryTypeLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[DeliveryTypeId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@DeliveryTypeId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[DeliveryTypeLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[DeliveryTypeId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [DeliveryTypeLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CartSelect

/***** Object:  StoredProcedure [dbo].[CartSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@DeliveryTypeId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [DeliveryTypeLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[DeliveryTypeId] = ISNULL(@DeliveryTypeId,[DeliveryTypeId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeLanguageMappingXMLSave]
 @DeliveryTypeLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(DeliveryTypeId)[1]', 'BIGINT') AS 'DeliveryTypeId'
  FROM 
	@DeliveryTypeLanguageMappingXml.nodes('/root/DeliveryTypeLanguageMapping') AS NDS(DT)
   )
   MERGE INTO DeliveryTypeLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[DeliveryTypeId] =ISNULL(x.[DeliveryTypeId] ,R.[DeliveryTypeId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[DeliveryTypeId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[DeliveryTypeId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CartInsert

/***** Object:  StoredProcedure [dbo].[CartInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[PriceType]
	 	,
	 		R.[Price]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [DeliveryType] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@PriceType NVARCHAR(MAX)=NULL,
			@Price    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [DeliveryType]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[PriceType] = ISNULL(@PriceType,[PriceType]),
			 	[Price] = ISNULL(@Price,[Price])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeXMLSave


GO
/****** Object:  StoredProcedure [dbo].[DeliveryTypeXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeliveryTypeXMLSave]
 @DeliveryTypeXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(PriceType)[1]', 'NVARCHAR') AS 'PriceType',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price'
  FROM 
	@DeliveryTypeXml.nodes('/root/DeliveryType') AS NDS(DT)
   )
   MERGE INTO DeliveryType R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[PriceType] =ISNULL(x.[PriceType] ,R.[PriceType]),R.[Price] =ISNULL(x.[Price] ,R.[Price])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[PriceType],
		[Price]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[PriceType],x.[Price]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[DeliveryTypeLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DistrictInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@CityId      BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [District]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[CityId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@CityId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictUpdate

/***** Object:  StoredProcedure  [dbo].[DistrictUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DistrictLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@DistrictId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DistrictLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[DistrictId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@DistrictId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[DistrictLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DistrictLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[DistrictId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [DistrictLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictSelect

/***** Object:  StoredProcedure [dbo].[SubDistrictSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[DistrictLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@DistrictId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [DistrictLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[DistrictId] = ISNULL(@DistrictId,[DistrictId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[DistrictLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictLanguageMappingXMLSave]
 @DistrictLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(DistrictId)[1]', 'BIGINT') AS 'DistrictId'
  FROM 
  
	@DistrictLanguageMappingXml.nodes('/ArrayOfDistrictLanguageMappingModel/DistrictLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO DistrictLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[DistrictId] =ISNULL(x.[DistrictId] ,R.[DistrictId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[DistrictId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[DistrictId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictInsert

/***** Object:  StoredProcedure [dbo].[SubDistrictInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DistrictSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
		R.CityId,
		R.[Name],
		C.Name AS CityName,
	overall_count = COUNT(*) OVER(),
	CityLanguageXml = (
		select
		districtlanguagemapping.Id,
		districtlanguagemapping.Name,
		districtlanguagemapping.LanguageId,
		districtlanguagemapping.DistrictId,
		[language].Name AS LanguageName

		FROM 
		DistrictLanguageMapping districtlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = districtlanguagemapping.LanguageId
		WHERE districtlanguagemapping.DistrictId =  R.Id AND districtlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [District] R  
	INNER JOIN [City] C ON C.[Id] = R.[CityId] AND C.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[DistrictLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[DistrictSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.Id AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictName
		
	FROM [District] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[DistrictLanguageMappingSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[DistrictUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@CityId      BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [District]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
				[Name]     =  ISNULL(@Name,[Name]),
				[CityId]   =  ISNULL(@CityId,[CityId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictXMLSave


GO
/****** Object:  StoredProcedure [dbo].[DistrictXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DistrictXMLSave]
 @DistrictXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive'
  FROM 
	@DistrictXml.nodes('/root/District') AS NDS(DT)
   )
   MERGE INTO District R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[DistrictLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[DriverActiveOrderSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                 
CREATE PROCEDURE [dbo].[DriverActiveOrderSelect](          
--select * from users where id=352    
--declare                                            
@Id         BIGINT =null,                                                  
@Slug     NVARCHAR(255)=NULL,                                                  
@next   int = NULL,                                                  
@offset  int = NULL,                                                  
@RelationTable NVARCHAR(50)=null,                                                  
@RelationId bigint=NULL,                                                  
@TenantId   INT=NULL,                                                  
@LanguageId  BIGINT=null,                                                  
@userid      BIGINT=null,                        --   --389 387 --2020-05-06 05:44:21.7062794 +00:00 --2020-05-07 05:44:21.7062794 +00:00                
@isCompleted bit =null,                                  
@DriverId  BIGINT =null,                                
@Date datetimeoffset(7)=null,                                
@type nvarchar(50)=null                                
                                 
) AS                                             
BEGIN                                                  
                                                  
IF @next IS NULL                                                  
BEGIN                                                  
SET @next =100000                                                  
SET @offset=1                                                  
END                                                  
     declare @InternalStatus nvarchar(50)=null                                             
                                  
                                     
  if(@isCompleted=1 and @type='ACTIVE')                                
  begin                                 
    set @RelationTable='ACTIVE-COMPLETE'                                
  end                                
     else if(@isCompleted=0 and @type='COMPLETE')                                
    begin                                 
     set @RelationTable='COMPLETE'                                
  end                                
  else if(@isCompleted=0 and @type='ACTIVE')                                
    begin                                 
     set @RelationTable='ACTIVE'                                
  end                        
  else                    
  begin                     
                       
     set @RelationTable='ACTIVE'                    
  end                     
                           
                      
                    
 SELECT                                                  
    o.[Id]                                                  
                                                 
                                                      
   ,                                                  
    o.[CreatedOn]                                                  
                                                         
                                                     
   ,                                                  
    o.[Status] ,                                            
    PaymentStatus                                            
   ,                                        
   o.paymenttype,                                      
                                      
    o.[TotalItems]  ,                                                
    o.deliveryType,                                          
    o.PickupDate,                                    
 o.DeliveryDate,                                    
 o.pickupSlot,                                    
 o.deliverySlot,                                    
 o.TotalPrice,                                    
    customerName=(select distinct FirstName+' '+LastName from users where id=o.UserId),               
    o.userid customerid,                                         
  o.Paymentresponse,                                            
                                           
  overall_count = COUNT(*) OVER(),                                                  
                                             
AddressXml=(                                                  
                                         
   SELECT                                                            
    R.[Id],            
    R.[AddressLine1],                                                            
   R.[AddressLine2],                                                            
    R.[DistrictId],                                                           
    R.[SubDistrictId],                                                            
    R.[PostalCode],                                   
    R.[Tag],                                                            
    R.[Latitude],                                                            
    R.[Longitude],                                
   R.[HouseNumber],                                                            
   R.[StreetNumber],                                                        
   R.[Note],                                                            
   R.[Type],                         
                                                            
                                                          
   R.[BuildingName] ,                                                          
   R.[Floor] ,                                        
   R.[UnitNo] ,                                                          
   R.[PhoneNo] ,                                                          
   R.[PhoneExt] ,                                                          
   R.[ResidenceType] ,                                                 
        -- AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id                                                     
                                                               
   D.Name  AS DistrictName,                                                            
   SD.Name  AS SubDistrictName,                               
    PD.Name As ProvinceName,                                
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,                                                            
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId  ) AS SubDistrictNameL  ,                                                          
     (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL                                        
                                                            
                                                            
 FROM deliveryaddress R INNER JOIN                                                            
 [District] D ON D.Id =  R.[DistrictId]                                                            
 INNER JOIN                                                             
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                                
 INNER JOIN                                         
 Province PD ON PD.Id =  R.ProvinceId                                 
                              
                              
 where R.orderid=o.ID                                                      
      FOR XML AUTO,ROOT,ELEMENTs                                                                        
    )    ,o.QRCode                                           
                                            
 from [order] o                                                    
 WHERE                           
 (                  
       @RelationTable IS NULL                                                              
    or                                                                  
       (      
           @RelationTable = 'ACTIVE'                                                                  
           and                                                                
     (                                                            
            o.[status]='AWAITING_COLLECTION'                                  
            or                                   
            o.[status]='ON_THE_WAY'                                  
            or                                   
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                       
   or                                   
            o.[status]='ASSIGNED_DRIVER'                       
            )                       
       )                                     
       or                                                                  
       (                                                                    
           @RelationTable = 'ACTIVE-COMPLETE'              
           and                                                                
            (                                                            
            o.[status]='AWAITING_COLLECTION'                                  
            or                                   
            o.[status]='ON_THE_WAY'                                  
            or                                   
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                                   
            or                                
            o.[Status]='DELIVERED'                       
   or                                   
            o.[status]='ASSIGNED_DRIVER'                      
          )                                                            
        )                                
       or                                                                  
       (                                                                    
           @RelationTable = 'COMPLETE'                                                                  
           and                         
            (                                                            
                                           
            o.[Status]='DELIVERED'                                
            )                                                            
       )                      
                      
                        
                    
   )                             
        and                                               
   o.[IsDeleted] = 0                          
   and                                
   (                                
   @DriverId is null                                
   or                                
   o.DriverId=@DriverId                                
   )                                
   AND                                                                  
   (                                                                  
      @Date IS NULL                                                                  
                          
     OR             
               
       cast (o.deliverydate as date)=cast(@Date as date)                   
     or                 
       cast (o.pickupdate as date)=cast(@Date as date)                 
                
   )  
   and   
   (  
       (o.status='ASSIGNED_DRIVER' or o.status='AWAITING_COLLECTION')  and cast (o.PickupDate as date)=cast(@Date as date)  
       or   
    (o.status='ASSIGNED_DRIVER_FOR_DELIVERY' or o.status='ON_THE_WAY')  and cast (o.deliverydate as date)=cast(@Date as date)  
   )  
  
  
   --and             
   --(            
   --  o.id not in (select id from [order] where status='ASSIGNED_DRIVER_FOR_DELIVERY' and cast (deliverydate as date)<cast(@Date as date))            
   --)     
   --and             
   --(            
   --  o.id not in (select id from [order] where status='ASSIGNED_DRIVER' and cast (deliverydate as date)=cast(@Date as date))            
   --)   
     
              --select distinct(status) from [order]  
           
          -- select pickupdate,deliverydate from [order] where id=182      
                                
                               
                                            
                                                  
 --Order by o.Id desc  
 order by
 CASE    WHEN  DeliveryType='SAMEDAY'  THEN 1  
         WHEN DeliveryType='EXPRESS'    THEN 2    
		 WHEN  DeliveryType= 'NORMAL'   THEN 3   END   ASC                      
               

 OFFSET (@next*@offset)-@next ROWS                                                  
    FETCH NEXT @next ROWS ONLY                                                  
                                                 
END                                                  
                                                  
  --select id from [order] where status='ASSIGNED_DRIVER_FOR_DELIVERY' and cast (deliverydate as date)<cast(@Date as date)                                                  
  --select id,deliverydate from [order] where status='ASSIGNED_DRIVER' and cast (deliverydate as date)=cast(@Date as date)      
 -- select @Date    
              --  select * from [Order] where id=393                      
 --         select distinct(status) from [order]--ASSIGNED_DRIVER    
---------------------------------------------------                                                  
--------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[DriverCompleteRidesSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  
-- ACTIVE-COMPLETE : Active order and completed order : Active                       
-- ACTIVE           : only active order for the driver    select count(*) from  DriverRideLogs where driverid=305       
                                                          -- select * from  DriverRideLogs where driverid=305      
                --select * from users where id=305  
CREATE PROCEDURE [dbo].[DriverCompleteRidesSelect](              
--declare                                  
@Id         BIGINT =null,                                        
@Slug     NVARCHAR(255)=NULL,                                        
@next   int = NULL,                                        
@offset  int = NULL,                                        
@RelationTable NVARCHAR(50)=null,                                        
@RelationId bigint=NULL,                                      
@TenantId   INT=NULL,                                        
@LanguageId  BIGINT=null,                                        
@userid      BIGINT=null,                            --2020-05-06 05:44:21.7062794 +00:00 --2020-05-07 05:44:21.7062794 +00:00      
@isCompleted bit =0,                        
@DriverId  BIGINT =null,                      
@Date datetimeoffset(7)=null,                      
@type nvarchar(50)=null                      
                       
) AS                                   
BEGIN                                        
                                        
IF @next IS NULL                                        
BEGIN                                        
SET @next =100000                                        
SET @offset=1                                        
END                                        
     declare @InternalStatus nvarchar(50)=null                                   
                        
                
            
          
 SELECT                                        
    o.[Id]                                        
                                       
                                            
   ,                                        
   Dl.CompleteOn  as[CreatedOn]                                     
                                               
                                           
   ,                                        
    o.[Status] ,                                  
    PaymentStatus                                  
   ,                              
   o.paymenttype,                            
                            
    o.[TotalItems]  ,                                      
    o.deliveryType,                                
    o.PickupDate,                          
 o.DeliveryDate,                          
 o.pickupSlot,                          
 o.deliverySlot,                          
 o.TotalPrice,                          
    customerName=(select distinct FirstName+' '+LastName from users where id=o.UserId),                                 
    o.userid customerid,                               
  o.Paymentresponse,                                  
                                    
  overall_count = COUNT(*) OVER(),                                        
                                   
AddressXml=(                                        
                                         
   SELECT                                                  
    R.[Id],                                                  
    R.[AddressLine1],                                                  
   R.[AddressLine2],                                                  
    R.[DistrictId],                                                 
    R.[SubDistrictId],                                                  
    R.[PostalCode],                         
    R.[Tag],                                                  
    R.[Latitude],                                                  
    R.[Longitude],                      
   R.[HouseNumber],                                                  
   R.[StreetNumber],               
   R.[Note],                                                  
   R.[Type],               
                                                  
                                                
   R.[BuildingName] ,                                                
   R.[Floor] ,                              
   R.[UnitNo] ,                                                
   R.[PhoneNo] ,                                                
   R.[PhoneExt] ,                                                
   R.[ResidenceType] ,                                       
        -- AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id                                           
                                                     
   D.Name  AS DistrictName,                                                  
   SD.Name  AS SubDistrictName,                     
    PD.Name As ProvinceName,                      
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,                                                  
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId  ) AS SubDistrictNameL  ,                                                
     (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL                              
    
                                                  
 FROM deliveryaddress R INNER JOIN                                                  
 [District] D ON D.Id =  R.[DistrictId]                                                  
 INNER JOIN                                                   
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                      
 INNER JOIN                               
 Province PD ON PD.Id =  R.ProvinceId                       
                    
                    
 where R.orderid=o.ID                                            
      FOR XML AUTO,ROOT,ELEMENTs                                                              
    )  ,  
 dl.Type as RideType  
                                 
 from [order] o   
 join   
 DriverRideLogs Dl on o.id=dl.OrderId  
 WHERE                 
                                    
   o.[IsDeleted] = 0                
   and                      
   (                      
   @DriverId is null                      
   or                      
   Dl.DriverId=@DriverId                      
   )                      
   AND                                                        
   (                                                        
      @Date IS NULL                                                        
                
     OR                                                        
       cast (o.deliverydate as date)=cast(@Date as date)       
     or       
       cast (o.pickupdate as date)=cast(@Date as date)       
      
   )                      
                      
                      
                     
                                  
                                        
 Order by o.Id desc                                        
 OFFSET (@next*@offset)-@next ROWS                                        
    FETCH NEXT @next ROWS ONLY                                        
                                        
END                                        
                                        
                                        
                            
---------------------------------------------------                                        
--------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[DriverLocationInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DriverLocationInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Longititude decimal(18,6)=NULL,
			@Latitude decimal(18,6)=NULL,
			@userId BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DriverLocation]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Longititude],[Latitude],[userId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Longititude,@Latitude,@userId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------

SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[DriverLocationSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DriverLocationSelect](  
--declare
@Id         BIGINT =NULL,  
@Slug     NVARCHAR(255)=NULL,  
@next   int = NULL,  
@offset  int = NULL,  
@RelationTable NVARCHAR(50)=NULL,  
@RelationId bigint=NULL,  
@TenantId   INT=NULL  
)  AS 
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =100000  
SET @offset=1  
END  
  
 SELECT  
    R.[Id]  
   ,  
    R.[TenantId]  
   ,  
    R.[Slug]  
   ,  
    R.[CreatedBy]  
   ,  
    R.[UpdatedBy]  
   ,  
    R.[CreatedOn]  
   ,  
    R.[UpdatedOn]  
   ,  
    R.[IsDeleted]  
   ,  
    R.[IsActive]  
   ,  
    R.Longititude  
   ,  
    R.Latitude  
   ,  
    R.userId  
   ,  
   U.[FirstName]+' '+[LastName] as [Name],
 overall_count = COUNT(*) OVER()  
 FROM [DriverLocation] R 
 inner join 
 users U on R.userid=U.id 
 WHERE   
 (  
  @RelationTable IS NULL  
  or  
  (    
  @RelationTable = 'User'  
  and  
  R.userId =@RelationId  
  )   
    
 )  
 AND  
 (  
  @Id IS NULL  
  OR  
  R.[Id] = @Id  
 )  
 AND  
 (  
  @Slug IS NULL  
  OR  
  R.[Slug] = @Slug  
 )  
 AND  
 (  
  R.[IsDeleted] = 0  
 )  
 AND  
 (  
  @TenantId IS NULL  
  OR  
  R.[TenantId] = @TenantId  
 )  
  
 Order by R.Id desc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END  
  
  
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : CompanyOwnerSelect  
  
/***** Object:  StoredProcedure [dbo].[CompanyOwnerSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[DriverLocationUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DriverLocationUpdate]          
  --declare        
  @Id    BIGINT=null,          
   @TenantId   INT=NULL,          
   @Slug NVARCHAR(MAX)=NULL,          
     @UpdatedBy    BIGINT=NULL,          
     @IsDeleted    BIT=NULL,          
     @IsActive    BIT=NULL,          
                
   @Longititude decimal(18,6)=NULL,          
   @Latitude decimal(18,6)=NULL,          
   @userId BIGINT=NULL          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
        if exists (select * from DriverLocation where userid=@userid and IsDeleted =0 and isactive=1)  
 begin  
    UPDATE [DriverLocation]          
 SET          
     [Slug] = ISNULL(@Slug,[Slug]),          
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),          
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),           
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),          
     [IsActive] = ISNULL(@IsActive,[IsActive]),          
    [Longititude]= ISNULL(@Longititude,[Longititude]),          
    [Latitude]= ISNULL(@Latitude,[Latitude]),          
    [userId]= ISNULL(@userId,[userId])          
               
  WHERE          
  (          
   [userId]=@userId          
  )          
  AND          
  (          
  [TenantId] =  @TenantId          
  )   
  end  
  else   
  begin   
  INSERT INTO [DriverLocation]    
   (    
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Longititude],[Latitude],[userId]    
   )    
   VALUES    
   (     
    @TenantId,@Slug,@UpdatedBy,@UpdatedBy,@IsActive,@Longititude,@Latitude,@userId    
   )    
  end  
  
END          
          
          
---------------------------------------------------          
---------------------------------------------------          
-- Procedure : CompanyXMLSave 
GO
/****** Object:  StoredProcedure [dbo].[DriverUserSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DriverUserSelect]                           
--declare                        
@Id          BIGINT=NULL,                          
@Role        NVARCHAR(20)=null,                          
@next    INT = NULL,                          
@offset   INT = NULL,                          
@freetext nvarchar(max)=null                        
AS                        
BEGIN                          
                        
  declare @temprole varchar(50)=null;                      
 if(@Role='ROLE_ADMIN')                      
 begin                       
  set  @temprole='ROLE_ADMIN';                      
  set @role=null;                      
 end                      
                      
IF @next IS NULL                          
BEGIN                          
SET @next =100000                          
SET @offset=1                          
END                          
                          
SELECT                           
       U.[Id],                          
    U.[IsActive],                          
    U.[IsDeleted],                          
    U.[FirstName],                          
    U.[LastName],                          
    U.[Email],                          
    U.[PhoneNumber],                          
    U.[UserName],                          
    U.[UniqueCode],                          
    U.[TenantId],                          
    R.[Name] AS RoleName,                          
    case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'                         
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'                       
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'                       
   end as Role                         
      ,                    
   U.Gender,                    
   U.EmployeeId,                    
   U.Position,                    
   U.NationalId,                    
   U.LicenceId,                    
   U.BikeInformation,                    
   U.LicencePlate,                    
   U.DOB,                    
    U.Status,                   
                       
                  
  FileXml=(                          
     SELECT                           
      FG.[Id],                          
      FG.[CreatedBy],                          
      FG.[UpdatedBy],                          
      FG.[CreatedOn],                          
      FG.[UpdatedOn],                          
      FG.[IsDeleted],                          
      FG.[IsActive],                          
      FG.[Filename],                          
      FG.[MimeType],                          
      FG.[Thumbnail],                          
      FG.[Size],                          
      FG.[Path],                          
      FG.[OriginalName],                          
      FG.[OnServer],                          
      FG.[TypeId],                          
      FG.[Type]                          
      FROM FileGroupItems FG                          
      WHERE FG.[TypeId] = U.[Id]  AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0                          
      FOR XML AUTO,ROOT,ELEMENTs                          
    )   ,                  
                 
     Order_Count=(select count(*) from [order] where driverid=U.id and ([status]='AWAITING_COLLECTION' or [status]='AWAITING_COLLECTION_DELIVERY'or [status]='ON_THE_WAY' or [status]='ASSIGNED_DRIVER' or [status]='ASSIGNED_DRIVER_FOR_DELIVERY')),              
                  
                  
   overall_count = COUNT (*) OVER()                           
                        
                          
 FROM                           
 Users U                           
 INNER JOIN UserRoles UR ON U.Id = UR.UserId                           
 INNER JOIN Roles R ON R.Id = UR.RoleId                           
                          
WHERE                           
 (                           
 @Id IS NULL                           
 OR                           
 U.Id=@Id                           
 )                           
                    
 AND                          
 (                           
                          
 R.Name ='ROLE_DRIVER'                          
 )                           
        
 AND                          
 (                           
 @temprole IS NULL                           
  or                                                     
     R.[Name] in (select [name] from roles where id in (1,5))                      
                           
 )           
  AND                          
 (                           
 @freetext IS NULL                           
  or                                                     
     U.FirstName like '%'+@freetext+'%'           
  or           
    U.Lastname like  '%'+@freetext+'%'          
  or           
    U.phonenumber like '%'+@freetext+'%'          
  or           
    U.email like '%'+@freetext+'%'          
  or           
    U.Position like '%'+@freetext+'%'          
 or           
    U.NationalId like '%'+@freetext+'%'          
 or           
    U.LicenceId like '%'+@freetext+'%'          
 or           
    U.LicencePlate like '%'+@freetext+'%'          
     or           
    U.BikeInformation like '%'+@freetext+'%'              
     or        
 U.FirstName+' '+U.LastName like '%'+@freetext+'%'         
 or           
    U.Status like '%'+@freetext+'%'          
 or           
    U.UserName like '%'+@freetext+'%'         
 or     
  U.EmployeeId like '%'+@freetext+'%'    
   --LicencePlate,  BikeInformation               
 )           
                          
 --AND                          
 --(                          
 -- U.IsDeleted = 0                          
 --)                          
                          
   ORDER BY U.Id DESC                           
   OFFSET (@next*@offset)-@next ROWS                          
   FETCH NEXT @next ROWS ONLY                          
                          
END                          
---------------------------------------------------                          
---------------------------------------------------                           
                           
SET ANSI_NULLS ON 
GO
/****** Object:  StoredProcedure [dbo].[EmailConfigurationSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EmailConfigurationSelect]
	@ConfigurationKey NVARCHAR(200)=NULL
AS
BEGIN
	SELECT * FROM EmailConfiguration WHERE ConfigurationKey=@ConfigurationKey AND IsDeleted=0
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackSelect

/***** Object:  StoredProcedure [dbo].[FeedbackSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[FeedbackInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FeedbackInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@Rating    BIGINT=NULL,
		  	@DriverId    BIGINT=NULL,
		  	@UserId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Feedback]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Type],[Message],[Rating],[DriverId],[UserId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Type,@Message,@Rating,@DriverId,@UserId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackUpdate

/***** Object:  StoredProcedure  [dbo].[FeedbackUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[FeedbackSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FeedbackSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[OrderId]
	 	,
	 		R.[Type]
	 	,
	 		R.[Message]
	 	,
	 		R.[Rating]
	 	,
	 		R.[DriverId]
	 	,
	 		R.[UserId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Feedback] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : UserDeviceSelect

/***** Object:  StoredProcedure [dbo].[UserDeviceSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[FeedbackUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FeedbackUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@Rating    BIGINT=NULL,
		  	@DriverId    BIGINT=NULL,
		  	@UserId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Feedback]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId]),
			 	[Type] = ISNULL(@Type,[Type]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[Rating] = ISNULL(@Rating,[Rating]),
			 	[DriverId] = ISNULL(@DriverId,[DriverId]),
			 	[UserId] = ISNULL(@UserId,[UserId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackXMLSave


GO
/****** Object:  StoredProcedure [dbo].[FeedbackXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FeedbackXMLSave]
 @FeedbackXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type',
		NDS.DT.value('(Message)[1]', 'NVARCHAR') AS 'Message',
		NDS.DT.value('(Rating)[1]', 'BIGINT') AS 'Rating',
		NDS.DT.value('(DriverId)[1]', 'BIGINT') AS 'DriverId',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId'
  FROM 
	@FeedbackXml.nodes('/root/Feedback') AS NDS(DT)
   )
   MERGE INTO Feedback R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),R.[Type] =ISNULL(x.[Type] ,R.[Type]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[Rating] =ISNULL(x.[Rating] ,R.[Rating]),R.[DriverId] =ISNULL(x.[DriverId] ,R.[DriverId]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[OrderId],
		[Type],
		[Message],
		[Rating],
		[DriverId],
		[UserId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[OrderId],x.[Type],x.[Message],x.[Rating],x.[DriverId],x.[UserId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : UserDeviceInsert

/***** Object:  StoredProcedure [dbo].[UserDeviceInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[FileGroupInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FileGroupInsert]
(
	  	@CreatedBy    BIGINT=NULL,
		@Name		NVARCHAR(250)=NULL,
		@Type		NVARCHAR(50)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [FileGroup]
	  (
	   CreatedBy,UpdatedBy,Name, [Type]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@CreatedBy,@Name, @Type
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[FileGroupItemInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FileGroupItemInsert]
	@CreatedBy		BIGINT=NULL,
	@FileName		NVARCHAR(250)=NULL,
	@MimeType		NVARCHAR(250)=NULL,
	@Thumbnail		NVARCHAR(250)=NULL,
	@Size			BIGINT=NULL,
	@Path			NVARCHAR(250)=NULL,
	@OriginalName	NVARCHAR(250)=NULL,
	@OnServer		NVARCHAR(250)=NULL,
	@TypeId			BIGINT=NULL,
	@Type			NVARCHAR(50)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO FileGroupItems
	(
		CreatedBy,
		UpdatedBy,		
		FileName,		
		MimeType,		
		Thumbnail,		
		Size,			
		Path,			
		OriginalName,	
		OnServer,		
		TypeId,			
		Type			
	)
	VALUES
	(
		@CreatedBy,
		@CreatedBy,		
		@FileName,		
		@MimeType,		
		@Thumbnail,	
		@Size,			
		@Path,			
		@OriginalName,	
		@OnServer,		
		@TypeId,			
		@Type			
	)
	SELECT SCOPE_IDENTITY()
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[FileGroupItemsDelete]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FileGroupItemsDelete]
	@Id			BIGINT=NULL,
	@UpdatedBy	BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
   UPDATE FileGroupItems 
   SET 
   IsDeleted=1,
   UpdatedBy=@UpdatedBy,
   UpdatedOn=switchoffset(sysdatetimeoffset(),'+00:00')
   WHERE  
   Id=@Id
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[FileGroupItemsInsertXml]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FileGroupItemsInsertXml](
	@UserId			BIGINT = NULL,
	@TypeId		BIGINT=NULL,
	@FileGroupItemsXml XML = NULL
)
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @FileGroupItems XML = NULL;
	SET @FileGroupItems = @FileGroupItemsXml;
	;WITH XmlData AS 
	(
		SELECT
			FileGroupItemsXml.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
			FileGroupItemsXml.DT.value('(Filename)[1]', 'nvarchar(256)') AS 'Filename',
			FileGroupItemsXml.DT.value('(MimeType)[1]', 'nvarchar(256)') AS 'MimeType',
			FileGroupItemsXml.DT.value('(Thumbnail)[1]', 'nvarchar(256)') AS 'Thumbnail',
			FileGroupItemsXml.DT.value('(Size)[1]', 'BIGINT') AS 'Size',
			FileGroupItemsXml.DT.value('(Path)[1]', 'nvarchar(256)') AS 'Path',
			FileGroupItemsXml.DT.value('(OriginalName)[1]', 'nvarchar(256)') AS 'OriginalName',
			FileGroupItemsXml.DT.value('(OnServer)[1]', 'nvarchar(256)') AS 'OnServer',
			FileGroupItemsXml.DT.value('(Type)[1]', 'nvarchar(50)') AS 'Type',
			FileGroupItemsXml.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted'
		FROM 
        @FileGroupItems.nodes('/ArrayOfFileGroupItemsModel/FileGroupItemsModel') AS FileGroupItemsXml(DT)
	 )
	 MERGE INTO FileGroupItems AF
	 USING XmlData x on AF.Id=x.Id
	 WHEN MATCHED 
		THEN UPDATE SET
			AF.UpdatedBy = @UserId,
			AF.Filename = x.Filename,
			AF.MimeType = x.MimeType,
			AF.Thumbnail = x.Thumbnail,
			AF.Size = x.Size,
			AF.Path = x.Path,
			AF.OriginalName = x.OriginalName,
			AF.OnServer = x.OnServer,
			AF.TypeId = @TypeId,
			AF.[Type]=x.[Type],
			AF.IsDeleted = x.[IsDeleted]
		
	WHEN NOT MATCHED 
	THEN 
	
      INSERT
	  (
	   CreatedBy,UpdatedBy,Filename,MimeType,Thumbnail,Size,Path,OriginalName,OnServer,TypeId, [Type]
	  )
      VALUES
	  ( 
	   @UserId,@UserId,x.Filename,x.MimeType,x.Thumbnail,x.Size,x.Path,x.OriginalName,x.OnServer,@TypeId, x.Type 
	  );
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[GetUserDeviceToken]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserDeviceToken] 
	-- Add the parameters for the stored procedure here
	@userId BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT 
			DeviceId,
			DeviceToken,
			DeviceType 
		FROM 
			UserDevice 
		WHERE 
		(
			@userId IS NULL
			OR
			UserId = @userId 
		)
END
GO
/****** Object:  StoredProcedure [dbo].[InsertExceptionLog]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertExceptionLog] (
	@Source        NVARCHAR(1000)=NULL,
	@Message       Nvarchar(1000)=NULL,
	@StackTrace    NVARCHAR(MAX)=NULL,
	@Uri           NVARCHAR(1000)=NULL,
	@method        NVARCHAR(50)=NULL,
	@CreatedBy     BIGINT=NULL
)
AS BEGIN
	INSERT INTO ExceptionLog
	(
		Source,
		Message,
		StackTrace,
		Uri,
		method,
		CreatedBy

	)
	Values
	(
		@Source,
		@Message,
		@StackTrace,
		@Uri,
		@method,
		@CreatedBy
	)
	SELECT SCOPE_IDENTITY()
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[LanguageInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LanguageInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
			@Code  NVARCHAR(30)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Language]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[Code]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@Code
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : LanguageUpdate

/***** Object:  StoredProcedure  [dbo].[LanguageUpdate] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[LanguageSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LanguageSelect](  
--declare
@Id         BIGINT =NULL,  
@Slug     NVARCHAR(255)=NULL,  
@next   int = NULL,  
@offset  int = NULL,  
@RelationTable NVARCHAR(50)=NULL,  
@RelationId bigint=NULL,  
@TenantId   INT=NULL  
)  AS 
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =100000  
SET @offset=1  
END  
  
 SELECT  
    R.[Id]  
   ,  
    R.[TenantId]  
   ,  
    R.[Slug]  
   ,  
    R.[CreatedBy]  
   ,  
    R.[UpdatedBy]  
   ,  
    R.[CreatedOn]  
   ,  
    R.[UpdatedOn]  
   ,  
    R.[IsDeleted]  
   ,  
    R.[IsActive]  
   ,  
    R.[Name]  
	,
	
	CASE WHEN R.id =1 THEN cast(1  as bit) Else cast(0  as bit) END AS IsDefault
   ,  
  R.[Code],  
 overall_count = COUNT(*) OVER()  
 FROM [Language] R    
 WHERE   
 (  
  @RelationTable IS NULL  
 )  
 AND  
 (  
  @Id IS NULL  
  OR  
  R.[Id] = @Id  
 )  
 AND  
 (  
  @Slug IS NULL  
  OR  
  R.[Slug] = @Slug  
 )  
 AND  
 (  
  R.[IsDeleted] = 0  
 )  
 AND  
 (  
  @TenantId IS NULL  
  OR  
  R.[TenantId] = @TenantId  
 )  
  
 Order by R.Id desc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END  
  
  
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : StateSelect  
  
/***** Object:  StoredProcedure [dbo].[StateSelect] ***/  


GO
/****** Object:  StoredProcedure [dbo].[LanguageSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LanguageSelectApp](
@Id	        BIGINT =NULL,
@next 		int = NULL,
@offset 	int = NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 	R.Id,
	 	R.[Name],
		R.[Code]
	FROM [Language] R  
	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.[IsActive] = 1
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : StateSelect

/***** Object:  StoredProcedure [dbo].[StateSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[LanguageUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LanguageUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
			@Code  NVARCHAR(30)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Language]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
				[Code]  =  ISNULL(@Code,[Code])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : LanguageXMLSave
GO
/****** Object:  StoredProcedure [dbo].[LanguageXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LanguageXMLSave]
 @LanguageXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(Code)[1]','NVARCHAR(30)')AS'Code'
  FROM 
	@LanguageXml.nodes('/root/Language') AS NDS(DT)
   )
   MERGE INTO Language R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[Name] =ISNULL(x.[Name] ,R.[Name]),
	R.[Code] =  ISNULL(x.[Code],R.[Code])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[Code]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[Code]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : StateInsert

/***** Object:  StoredProcedure [dbo].[StateInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[MessageLogInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MessageLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [MessageLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Message],[LanguageId],[Type]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Message,@LanguageId,@Type
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : MessageLogUpdate

/***** Object:  StoredProcedure  [dbo].[MessageLogUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[MessageLogSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MessageLogSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[Message]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[Type]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [MessageLog] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationSelect

/***** Object:  StoredProcedure [dbo].[NotificationSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[MessageLogUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MessageLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [MessageLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Type] = ISNULL(@Type,[Type])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : MessageLogXMLSave


GO
/****** Object:  StoredProcedure [dbo].[MessageLogXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MessageLogXMLSave]
 @MessageLogXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(Message)[1]', 'NVARCHAR') AS 'Message',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type'
  FROM 
	@MessageLogXml.nodes('/root/MessageLog') AS NDS(DT)
   )
   MERGE INTO MessageLog R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Type] =ISNULL(x.[Type] ,R.[Type])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[Message],
		[LanguageId],
		[Type]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[Message],x.[LanguageId],x.[Type]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationInsert

/***** Object:  StoredProcedure [dbo].[NotificationInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[NotificationInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Token NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Response NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Notification]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Message],[LanguageId],[Type],[Token],[Status],[Response]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Message,@LanguageId,@Type,@Token,@Status,@Response
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationUpdate

/***** Object:  StoredProcedure  [dbo].[NotificationUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Type NVARCHAR(MAX)=NULL,
		  	@CanBeDelete    BIT=NULL,
			@Message     NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMaster]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Type],[CanBeDelete],[Message]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Type,@CanBeDelete,@Message
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterKeyValueMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterKeyValueMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL,
		  	@NotificationId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMasterKeyValueMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Key],[Value],[NotificationId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Key,@Value,@NotificationId
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterKeyValueMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterKeyValueMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Key]
	 	,
	 		R.[Value]
	 	,
	 		R.[NotificationId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [NotificationMasterKeyValueMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterKeyValueMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterKeyValueMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL,
		  	@NotificationId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [NotificationMasterKeyValueMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Key] = ISNULL(@Key,[Key]),
			 	[Value] = ISNULL(@Value,[Value]),
			 	[NotificationId] = ISNULL(@NotificationId,[NotificationId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterKeyValueMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterKeyValueMappingXMLSave]
 @NotificationMasterKeyValueMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Key)[1]', 'NVARCHAR(100)') AS 'Key',
		NDS.DT.value('(Value)[1]', 'NVARCHAR(100)') AS 'Value',
		NDS.DT.value('(NotificationId)[1]', 'BIGINT') AS 'NotificationId'
  FROM 
	@NotificationMasterKeyValueMappingXml.nodes('/ArrayOfNotificationMasterKeyValueMappingModel/NotificationMasterKeyValueMappingModel') AS NDS(DT)
   )
   MERGE INTO NotificationMasterKeyValueMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Key] =ISNULL(x.[Key] ,R.[Key]),R.[Value] =ISNULL(x.[Value] ,R.[Value]),R.[NotificationId] =ISNULL(x.[NotificationId] ,R.[NotificationId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Key],
		[Value],
		[NotificationId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Key],x.[Value],x.[NotificationId]
    );
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		    @LanguageId BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@NotificationMasterId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMasterLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[LanguageId],[Message],[NotificationMasterId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@LanguageId,@Message,@NotificationMasterId
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[LanguageId],
	 		LanguageXml=(
			  SELECT 
			   *
			   FROM Language language 
			   WHERE language.Id = R.[LanguageId] AND language.IsActive = 1 AND language.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	 		R.[Message]
	 	,
	 		R.[NotificationMasterId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [NotificationMasterLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
		or
		(		
		@RelationTable = 'Language'
		and
		R.[LanguageId] = @RelationId
		)	
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@LanguageId   BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@NotificationMasterId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [NotificationMasterLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[NotificationMasterId] = ISNULL(@NotificationMasterId,[NotificationMasterId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingXMLSave]
 @NotificationMasterLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(MAX)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Message)[1]', 'NVARCHAR(MAX)') AS 'Message',
		NDS.DT.value('(NotificationMasterId)[1]', 'BIGINT') AS 'NotificationMasterId'
  FROM 
	@NotificationMasterLanguageMappingXml.nodes('/ArrayOfNotificationMasterLanguageMappingModel/NotificationMasterLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO NotificationMasterLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[NotificationMasterId] =ISNULL(x.[NotificationMasterId] ,R.[NotificationMasterId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[LanguageId],
		[Message],
		[NotificationMasterId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[LanguageId],x.[Message],x.[NotificationMasterId]
    );
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Type]
	 	,
	 		R.[CanBeDelete]
	 	,
			R.[Message],
	overall_count = COUNT(*) OVER(),
	NotificationMasterLanguageXml = (
		select
		notificationmasterlanguagemapping.Id,
		notificationmasterlanguagemapping.[Message],
		notificationmasterlanguagemapping.LanguageId,
		notificationmasterlanguagemapping.NotificationMasterId,
		[language].Name AS LanguageName

		FROM 
		NotificationMasterLanguageMapping notificationmasterlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = notificationmasterlanguagemapping.LanguageId
		WHERE notificationmasterlanguagemapping.NotificationMasterId =  R.Id AND notificationmasterlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	),
	NotificationMasterKeyValueXml = (
		select
		notificationmasterkeyvaluemapping.Id,
		notificationmasterkeyvaluemapping.[Key],
		notificationmasterkeyvaluemapping.Value,
		notificationmasterkeyvaluemapping.NotificationId
		

		FROM 
		NotificationMasterKeyValueMapping notificationmasterkeyvaluemapping 
		WHERE notificationmasterkeyvaluemapping.NotificationId =  R.Id AND notificationmasterkeyvaluemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [NotificationMaster] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Type NVARCHAR(MAX)=NULL,
		  	@CanBeDelete    BIT=NULL,
			@Message     NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [NotificationMaster]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Type] = ISNULL(@Type,[Type]),
			 	[CanBeDelete] = ISNULL(@CanBeDelete,[CanBeDelete]),
				[Message]     =  ISNULL(@Message,[Message])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationMasterXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationMasterXMLSave]
 @NotificationMasterXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type',
	  	NDS.DT.value('(CanBeDelete)[1]', 'BIT') AS 'CanBeDelete'
  FROM 
	@NotificationMasterXml.nodes('/root/NotificationMaster') AS NDS(DT)
   )
   MERGE INTO NotificationMaster R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Type] =ISNULL(x.[Type] ,R.[Type]),R.[CanBeDelete] =ISNULL(x.[CanBeDelete] ,R.[CanBeDelete])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Type],
		[CanBeDelete]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Type],x.[CanBeDelete]
    );
END
GO
/****** Object:  StoredProcedure [dbo].[NotificationSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[Message]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[Type]
	 	,
	 		R.[Token]
	 	,
	 		R.[Status]
	 	,
	 		R.[Response]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Notification] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingSelect

/***** Object:  StoredProcedure [dbo].[AppSettingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[NotificationSelectForOtherActiveUser]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationSelectForOtherActiveUser] 
	@UserId BIGINT = NULL,
	@DeviceId NVARCHAR(100) = NULL
AS

	SELECT (U.FirstName +' '+ U.LastName )as FullName,
	--TS.IsGroupChat,TS.IsGroupDiscussion,
	Devices=STUFF(
             (SELECT ',' + D.DeviceToken
              FROM UserDevice D
			  WHERE D.UserId = U.Id AND D.IsDeleted = 0 AND D.DeviceId <> @DeviceId
              FOR XML PATH (''))
             , 1, 1, '')
	 FROM Users U 
	--LEFT JOIN CustomerNotification TS ON TS.CustomerId = U.Id
	WHERE U.Id = @UserId AND U.IsDeleted = 0

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[NotificationUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Token NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Response NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Notification]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Type] = ISNULL(@Type,[Type]),
			 	[Token] = ISNULL(@Token,[Token]),
			 	[Status] = ISNULL(@Status,[Status]),
			 	[Response] = ISNULL(@Response,[Response])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationXMLSave


GO
/****** Object:  StoredProcedure [dbo].[NotificationXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NotificationXMLSave]
 @NotificationXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(Message)[1]', 'NVARCHAR') AS 'Message',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type',
		NDS.DT.value('(Token)[1]', 'NVARCHAR') AS 'Token',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(Response)[1]', 'NVARCHAR') AS 'Response'
  FROM 
	@NotificationXml.nodes('/root/Notification') AS NDS(DT)
   )
   MERGE INTO Notification R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Type] =ISNULL(x.[Type] ,R.[Type]),R.[Token] =ISNULL(x.[Token] ,R.[Token]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[Response] =ISNULL(x.[Response] ,R.[Response])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[Message],
		[LanguageId],
		[Type],
		[Token],
		[Status],
		[Response]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[Message],x.[LanguageId],x.[Type],x.[Token],x.[Status],x.[Response]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingInsert

/***** Object:  StoredProcedure [dbo].[AppSettingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderInsert](                    
                    
   @TenantId   INT=NULL,                    
   @Slug NVARCHAR(MAX)=NULL,                    
     @CreatedBy    BIGINT=NULL,                    
     @IsActive    BIT=NULL,                    
     @UserId    BIGINT=NULL,                    
   @DeliveryDate  DATETIMEOFFSET(7)=NULL,                    
     @DeliveryType    nvarchar(100)=NULL,                    
   @TotalItems   INT=NULL,                    
   @DeliveryPrice    DECIMAL(18,2)=NULL,                    
     @DriverId    BIGINT=NULL,                    
   @Status NVARCHAR(MAX)=NULL,                    
   @PaymentType NVARCHAR(MAX)=NULL,                    
   @PaidAmount    DECIMAL(18,2)=NULL,                    
   @Tax    DECIMAL(18,2)=NULL,                    
   @SubTotal    DECIMAL(18,2)=NULL,                    
   @TotalPrice    DECIMAL(18,2)=NULL,                    
     @AdressId    BIGINT=NULL,                    
     @ExpectedPickUpMin    BIGINT=NULL,                    
     @ExpectedPickUpMax    BIGINT=NULL,                    
     @ExpectedDeliveryMin    BIGINT=NULL,                    
     @ExpectedDeliveryMax    BIGINT=NULL,                    
     @CartId    BIGINT=NULL ,                  
  @Deliveryaddress nvarchar(max)=null  ,                
                
                
                   
    @PickupDate DATETIMEOFFSET(7)=NULL,                
    @deliverySlot NVARCHAR(100)=NULL,                  
   @pickupSlot NVARCHAR(100)=NULL  ,              
    @LogisticCharge DECIMAL(18,2)=NULL ,           
    @Discount     DECIMAL(18,2)=NULL ,          
 @paymentResponse nvarchar(100)=null    ,      
 @paymentStatus nvarchar(100)=null ,    
 @change DECIMAL(18,2)=NULL ,     
 @note nvarchar(max)=NULL,  
 @QrCode nvarchar(max)=NULL ,
 @laundryInstruction nvarchar(max)=NULL
)                    
AS                     
BEGIN                    
SET NOCOUNT ON;               
        
   declare @result decimal;        
        
        
        
   INSERT INTO   [Order]                    
   (                    
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[DeliveryDate],[DeliveryType],[TotalItems],        
 [DeliveryPrice],[DriverId],[Status],[PaymentType],[PaidAmount],[Tax],[SubTotal],[TotalPrice],[ExpectedPickUpMin],        
 [ExpectedPickUpMax],[ExpectedDeliveryMin],[ExpectedDeliveryMax],[CartId] ,[Deliveryaddress],[PickupDate],[deliverySlot],[pickupSlot],        
 LogisticCharge,Discount,paymentResponse,paymentstatus,change,note, QRCode ,laundryInstruction                
   )                    
   VALUES                    
   (                     
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@DeliveryDate,@DeliveryType,@TotalItems,        
 @DeliveryPrice,@DriverId,@Status,@PaymentType,@PaidAmount,@Tax,@SubTotal,@TotalPrice,@ExpectedPickUpMin,@ExpectedPickUpMax,        
 @ExpectedDeliveryMin,@ExpectedDeliveryMax,@CartId  ,@Deliveryaddress  ,@PickupDate,@deliverySlot,@pickupSlot  ,@LogisticCharge          
 ,@Discount ,@paymentResponse  , @paymentstatus   ,@change,@note,@QrCode,@laundryInstruction    
   )                    
        
        
        
 SELECT @result=SCOPE_IDENTITY()            
   declare @Lognote varchar(max)=cast( 'Cart id='+cast(@cartid as varchar(100))+' is convert to order order id='+cast(@result as varchar(100)) as varchar(max));        
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )          
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@result,@Lognote)        
          
 select cast (@result as decimal)        
 END                    
                     
                     
                    
---------------------------------------------------                    
---------------------------------------------------                    
-- Procedure : OrderUpdate                    
                    
/***** Object:  StoredProcedure  [dbo].[OrderUpdate] *****/                    
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[OrderItemInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemInsert](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @OrderId    BIGINT=NULL,  
     @ProductId    BIGINT=NULL,  
   @Quantity   INT=NULL,  
     @IsPacking    BIT=NULL,  
   @PackingPrice    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
     @PackingId    BIGINT=NULL  ,
	 @ServiceId bigint=null
)  
AS   
BEGIN  
SET NOCOUNT ON;  
   INSERT INTO [OrderItem]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[ProductId],[Quantity],[IsPacking],[PackingPrice],[SubTotal],[TotalPrice],[PackingId],[ServiceId]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@ProductId,@Quantity,@IsPacking,@PackingPrice,@SubTotal,@TotalPrice,@PackingId,@ServiceId
   )  
 SELECT SCOPE_IDENTITY()  
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : OrderItemUpdate  
  
/***** Object:  StoredProcedure  [dbo].[OrderItemUpdate] *****/  
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[OrderItemSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[OrderId]
	 	,
	 		R.[ProductId]
	 	,
	 		R.[Quantity]
	 	,
	 		R.[IsPacking]
	 	,
	 		R.[PackingPrice]
	 	,
	 		R.[SubTotal]
	 	,
	 		R.[TotalPrice]
	 	,
	 		R.[PackingId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [OrderItem] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemServiceMappingSelect

/***** Object:  StoredProcedure [dbo].[OrderItemServiceMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[OrderItemServiceMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemServiceMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderItemServiceMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderItemId],[ServiceId],[Quantity],[Price],[SubTotal],[TotalPrice]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderItemId,@ServiceId,@Quantity,@Price,@SubTotal,@TotalPrice
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemServiceMappingUpdate

/***** Object:  StoredProcedure  [dbo].[OrderItemServiceMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderItemServiceMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemServiceMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[OrderItemId]
	 	,
	 		R.[ServiceId]
	 	,
	 		R.[Quantity]
	 	,
	 		R.[Price]
	 	,
	 		R.[SubTotal]
	 	,
	 		R.[TotalPrice]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [OrderItemServiceMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderLogSelect

/***** Object:  StoredProcedure [dbo].[OrderLogSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[OrderItemServiceMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemServiceMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [OrderItemServiceMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[OrderItemId] = ISNULL(@OrderItemId,[OrderItemId]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[Quantity] = ISNULL(@Quantity,[Quantity]),
			 	[Price] = ISNULL(@Price,[Price]),
			 	[SubTotal] = ISNULL(@SubTotal,[SubTotal]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemServiceMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[OrderItemServiceMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemServiceMappingXMLSave]
 @OrderItemServiceMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(OrderItemId)[1]', 'BIGINT') AS 'OrderItemId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice'
  FROM 
	@OrderItemServiceMappingXml.nodes('/root/OrderItemServiceMapping') AS NDS(DT)
   )
   MERGE INTO OrderItemServiceMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[OrderItemId] =ISNULL(x.[OrderItemId] ,R.[OrderItemId]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),R.[Price] =ISNULL(x.[Price] ,R.[Price]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[OrderItemId],
		[ServiceId],
		[Quantity],
		[Price],
		[SubTotal],
		[TotalPrice]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[OrderItemId],x.[ServiceId],x.[Quantity],x.[Price],x.[SubTotal],x.[TotalPrice]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderLogInsert

/***** Object:  StoredProcedure [dbo].[OrderLogInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderItemUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemUpdate]  
     @Id    BIGINT=NULL,  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @UpdatedBy    BIGINT=NULL,  
     @IsDeleted    BIT=NULL,  
     @IsActive    BIT=NULL,  
     @OrderId    BIGINT=NULL,  
     @ProductId    BIGINT=NULL,  
   @Quantity   INT=NULL,  
     @IsPacking    BIT=NULL,  
   @PackingPrice    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
     @PackingId    BIGINT=NULL,
	 @ServiceId bigint =null
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    UPDATE [OrderItem]  
 SET  
     [Slug] = ISNULL(@Slug,[Slug]),  
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),  
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),   
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),  
     [IsActive] = ISNULL(@IsActive,[IsActive]),  
     [OrderId] = ISNULL(@OrderId,[OrderId]),  
     [ProductId] = ISNULL(@ProductId,[ProductId]),  
     [Quantity] = ISNULL(@Quantity,[Quantity]),  
     [IsPacking] = ISNULL(@IsPacking,[IsPacking]),  
     [PackingPrice] = ISNULL(@PackingPrice,[PackingPrice]),  
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),  
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),  
     [PackingId] = ISNULL(@PackingId,[PackingId]) ,
	 [ServiceId]=ISNULL(@ServiceId,[ServiceId])
  WHERE  
  (  
   [Id]=@Id  
  )  
  AND  
  (  
  [TenantId] =  @TenantId  
  )  
END  
  
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : OrderItemXMLSave  
GO
/****** Object:  StoredProcedure [dbo].[OrderItemXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemXMLSave]
 @OrderItemXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
	  	NDS.DT.value('(IsPacking)[1]', 'BIT') AS 'IsPacking',
		NDS.DT.value('(PackingPrice)[1]', 'DECIMAL(18,2)') AS 'PackingPrice',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		NDS.DT.value('(PackingId)[1]', 'BIGINT') AS 'PackingId'
  FROM 
	@OrderItemXml.nodes('/root/OrderItem') AS NDS(DT)
   )
   MERGE INTO OrderItem R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),R.[IsPacking] =ISNULL(x.[IsPacking] ,R.[IsPacking]),R.[PackingPrice] =ISNULL(x.[PackingPrice] ,R.[PackingPrice]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),R.[PackingId] =ISNULL(x.[PackingId] ,R.[PackingId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[OrderId],
		[ProductId],
		[Quantity],
		[IsPacking],
		[PackingPrice],
		[SubTotal],
		[TotalPrice],
		[PackingId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[OrderId],x.[ProductId],x.[Quantity],x.[IsPacking],x.[PackingPrice],x.[SubTotal],x.[TotalPrice],x.[PackingId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemServiceMappingInsert

/***** Object:  StoredProcedure [dbo].[OrderItemServiceMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderItemXMLSaveApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemXMLSaveApp]  
 @orderItemXml xml  
AS  
BEGIN 

  select * from CartItem
  select * from OrderItem
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 ;WITH XmlData AS   
 (  
  SELECT  
  NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',  
  NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',  
  NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',  
  NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',  
  NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',  
  NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',  
  NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',  

    NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',  
    NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',  

  NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',  
  NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',  
  NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',  

  NDS.DT.value('(subTotal)[1]', 'DECIMAL(18,2)') AS 'subTotal',  
  NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',  
  NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId'  
  
    
  FROM   
 @orderItemXml.nodes('/ArrayOfOrderItemModel/OrderItemModel') AS NDS(DT)  
   )  
   MERGE INTO OrderItem R  
   USING XmlData x on R.Id=x.Id  
   WHEN MATCHED   
   THEN UPDATE SET  
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),  
 R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),  
 R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),  
 R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),  
 R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),  
 R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),  
 R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),  
 R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),  
 R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),  
 R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),  
 R.[Subtotal] =ISNULL(x.[Subtotal] ,R.[Subtotal]),  
 R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),  
 R.[ServiceId] =  ISNULL(x.[ServiceId],R.[ServiceId])  
  WHEN NOT MATCHED   
  THEN   
    INSERT  
 (  
    
  [TenantId],  
  [Slug],  
  [CreatedBy],  
  [UpdatedBy],  
  [IsActive],  
  [Orderid],  
  [ProductId],  
  [Quantity],  
  [Subtotal],  
  [TotalPrice],  
  [ServiceId]  
 )  
    VALUES(  
  x.[TenantId],  
  x.[Slug],  
  x.[CreatedBy],  
  x.[UpdatedBy],  
  x.[IsActive],  
  x.[Orderid],  
  x.[ProductId],  
  x.[Quantity],  
  x.[Subtotal],  
  x.[TotalPrice],  
  x.[ServiceId]  
    );  
END  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : CartItemServiceMappingInsert  
  
/***** Object:  StoredProcedure [dbo].[CartItemServiceMappingInsert] *****/  
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[OrderList]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderList](                                                            
--declare                                                     
@Id         BIGINT =null,                                                            
@Slug     NVARCHAR(255)=NULL,                                                            
@next   int = NULL,                                                            
@offset  int = NULL,                                                            
@RelationTable NVARCHAR(50)=null,                                                            
@RelationId bigint=NULL,                                                            
@TenantId   INT=NULL,                                                            
@LanguageId  BIGINT=NULL,                                                            
@userid      BIGINT=null    ,                                                    
                                            
@DistrictId      BIGINT=NULL    ,                                               
@SubDistrictId      BIGINT=NULL    ,                                               
@DriverId      BIGINT=NULL    ,                                            
@Status      NVARCHAR(50)=  null  ,                                          
                                          
@Name      NVARCHAR(50)=  null  ,                                          
@email      NVARCHAR(50)=  null  ,                                          
@Phone      NVARCHAR(50)=  null  ,                            
@deliveryType Nvarchar(50)=null,                   
@SortColumn Nvarchar(50)=null,                
@SortOrder Nvarchar(50)=null                
                
)   AS                                                       
BEGIN                                                            
                     
   if(@SortColumn is null)            
   begin            
    --set @SortColumn='DEFAULT'            
 set @SortColumn='CREATED_ON'        
 set @SortOrder='DESC'      
   end            
            
IF @next IS NULL                                                            
BEGIN                                                            
SET @next =100000                                                            
SET @offset=1                                                            
END                                                            
                                                      
  if(@LanguageId=null)                                                    
  begin                                                     
    select @LanguageId=languageid from users where id=@userid                                                    
  end                                                    
                        
select *,COUNT(*) OVER() overall_count from (                
select                 
R.id,                
Customername =(select top 1 firstname+' '+lastname from users where id= R.userid),                
R.createdon ,                
R.PickupDate,                
R.DeliveryDate,                
r.DeliveryType,                 
districtname=(select D.Name from deliveryaddress DA INNER JOIN   [District] D ON D.Id =  DA.[DistrictId]   where orderid=R.id),                
Subdistrictname=(select D.Name from deliveryaddress DA INNER JOIN   [SubDistrict] D ON D.Id =  DA.SubDistrictId   where orderid=R.id),                
R.totalPrice,                
R.paymentType,                
R.paymentStatus,                
R.status,                
driverName=(select top 1 firstname+' '+lastname from users where id= R.driverid),                
R.driverid as DriverId,                
R.userId as CustomerId,                
R.pickupslot,              
R.deliverySlot,  
R.[change] 
 --overall_count = COUNT(*) OVER()                  
 from [order] R                
                 
 WHERE                            
 (                          
  @RelationTable IS NULL                                                                          
       or                                 
       (                                                                                
           @RelationTable = 'NEW_ORDERS'                                                                              
           and                                     
           (                                                                        
            R.[status]='NEW'                                              
                  
            )                                                                        
       )                           
                          
   or                                                                    
       (                                                                                
           @RelationTable = 'COLLECTING_ORDERS'                                                                              
           and                                                                            
           (                                                                        
            R.[status]='ASSIGNED_DRIVER'                                              
            or                                               
    R.[status]='AWAITING_COLLECTION'                                              
            or                                               
            R.[status]='COLLECTED'                                   
                                              
            )                                                                        
       )                          
    or                                                                              
       (                              
           @RelationTable = 'CLEANING_ORDERS'                                                                              
           and                                                                            
           (                                                                        
            R.[status]='CLEANING_IN_PROGRESS'                                              
            or                                               
            R.[status]='CLEANING_COMPLETED'                                              
                                              
                                 
            )                                                                        
       )                          
    or                                                                              
       (                                                                                
           @RelationTable = 'DELIVERY_ORDERS'                                                                              
           and                                                                            
           (                                                                        
            R.[status]='AWAITING_COLLECTION_DELIVERY'                                              
            or                                               
            R.[status]='ON_THE_WAY'                                              
                                              
                                              
            )                                                                        
       )                          
     or                                                                              
       (                                                                                
           @RelationTable = 'COLLECTING_ORDERS_ASSIGNED'                                                                              
           and                                                                            
           (                                                                        
            R.[status]='ASSIGNED_DRIVER'                                              
            or   
            R.[status]='AWAITING_COLLECTION'                                              
                                              
                                              
            )               
       )                          
    or                                                                              
       (                                                                                
 @RelationTable = 'TOTAL_ORDERS'                                                                              
           and                                                                            
           (                                                                        
            R.[status]='NEW'                                              
            or                                               
            R.[status]='ASSIGNED_DRIVER'                                              
            or                                               
            R.[status]='AWAITING_COLLECTION'                                   
             or                                               
            R.[status]='COLLECTED'                           
   or                                               
            R.[status]='CLEANING_IN_PROGRESS'                           
   or                           
            R.[status]='CLEANING_COMPLETED'                           
   or                                               
            R.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                           
   or                                               
            R.[status]='ON_THE_WAY'                           
            )                                                    
       )                          
 )                                                      
 AND                                                            
 (                                                            
  @Id IS NULL                                         
  OR                                                            
  R.[Id] = @Id                                                       
 )                                                            
 AND                                                            
 (                                                            
  @Slug IS NULL                                                            
  OR                                                            
  R.[Slug] = @Slug                        
 )                                                            
 AND                                                            
 (                                                            
  R.[IsDeleted] = 0                                                            
 )                                                            
 AND                                                            
 (                                                            
  @TenantId IS NULL                                                            
  OR                                              
  R.[TenantId] = @TenantId                                                            
 )                                                            
 AND                                                            
 (                                    
  @userid IS NULL                                                            
   OR                                                            
  R.userid =  @userid                                                            
 )                                                
                                              
  AND                                                            
 (                                                            
  @DriverId IS NULL                                                            
   OR                                                            
  R.driverid =  @DriverId     
 )                                             
  AND                                                            
 (                                                            
  @Status IS NULL                                           
   OR                                                            
  R.[Status] =  @Status                                                            
 )                                             
  AND                                                            
 (                                                            
  @DistrictId IS NULL                                                            
   OR                             
  R.id in(  select orderid from deliveryaddress where districtid=@DistrictId )                                                             
 )                                             
 AND                                                    
 (                                                            
  @SubDistrictId IS NULL                                                            
   OR                        
  R.id in(  select orderId from deliveryaddress where SubDistrictId=@SubDistrictId )                                                             
 )                                             
 -- AND                                                            
 --(                                                            
 -- @Name IS NULL                                                            
 --  OR                                                     
 -- o.userid in (select id from users where [FirstName] like '%'+@Name+'%' or LastName like '%'+@Name+'%' )                                                            
 --)                                               
   AND                                                            
 (                                                            
  @email IS NULL                                 
   OR                                        
  R.userid in (select id from users where [Email] like '%'+@email+'%')                                            
                                                
                                            
 )                                               
  AND                                                            
 (                                                            
  @Phone IS NULL                
   OR                                                            
  R.userid in (select id from users where phonenumber like '%'+@Phone+'%')                                               
   OR                                                            
  R.userid in (select userid from DeliveryAddress where PhoneNo like '%'+@Phone+'%')                                              
  OR                                                            
  R.userid in (select userid from DeliveryAddress where alterNumber like '%'+@Phone+'%')                            
 )                                  
 And                            
 (                            
  @deliveryType is null                            
  or                             
  R.deliveryType=@deliveryType                            
                             
 )    
      
     
       
   ) as CTE      
  where     
  (    
             @Name is null    
             or     
             customerName like '%'+@Name+'%'    
     
         )    
        
 --Order by o.Id desc       @SortColumn @SortOrder                
 ORDER BY                  
           -- CreatedOn desc,      
                                
           -- CASE    WHEN @SortColumn='DEFAULT' AND DeliveryType='SAMEDAY'  THEN 1   WHEN @SortColumn='DEFAULT' AND DeliveryType='EXPRESS'    THEN 2     WHEN @SortColumn='DEFAULT' AND DeliveryType= 'NORMAL'   THEN 3   END   ASC    ,                
                     
                           
                
            CASE WHEN (@SortColumn = 'ORDER_ID' AND @SortOrder='ASC')   THEN id   END ASC,   
            CASE WHEN (@SortColumn = 'ORDER_ID' AND @SortOrder='DESC')  THEN id    END DESC,                  
                
            CASE WHEN (@SortColumn = 'CUSTOMER_FULLNAME' AND @SortOrder='ASC')   THEN customerName   END ASC,                  
            CASE WHEN (@SortColumn = 'CUSTOMER_FULLNAME' AND @SortOrder='DESC')  THEN customerName    END DESC,                 
                    
          CASE WHEN (@SortColumn = 'CREATED_ON' AND @SortOrder='ASC')   THEN createdOn   END ASC,                  
            CASE WHEN (@SortColumn = 'CREATED_ON' AND @SortOrder='DESC')  THEN createdOn    END DESC,                
                   
   CASE WHEN (@SortColumn = 'PICKUP_DATE' AND @SortOrder='ASC')   THEN pickupdate   END ASC,                  
            CASE WHEN (@SortColumn = 'PICKUP_DATE' AND @SortOrder='DESC')  THEN pickupdate    END DESC ,                
                
   CASE WHEN (@SortColumn = 'DELIVERY_DATE' AND @SortOrder='ASC')   THEN DeliveryDate   END ASC,          
            CASE WHEN (@SortColumn = 'DELIVERY_DATE' AND @SortOrder='DESC')  THEN DeliveryDate    END DESC ,                
                
   CASE WHEN (@SortColumn = 'DELIVERY_TYPE' AND @SortOrder='ASC')   THEN DeliveryType   END ASC,                  
            CASE WHEN (@SortColumn = 'DELIVERY_TYPE' AND @SortOrder='DESC')  THEN DeliveryType    END DESC,                 
                
   CASE WHEN (@SortColumn = 'DISTRICT_NAME' AND @SortOrder='ASC')   THEN districtname    END ASC,                  
            CASE WHEN (@SortColumn = 'DISTRICT_NAME' AND @SortOrder='DESC')  THEN districtname     END DESC ,                
                
   CASE WHEN (@SortColumn = 'SUBDISTRICT_NAME' AND @SortOrder='ASC')   THEN  Subdistrictname   END ASC,                  
            CASE WHEN (@SortColumn = 'SUBDISTRICT_NAME' AND @SortOrder='DESC')  THEN  Subdistrictname    END DESC ,                
                
   CASE WHEN (@SortColumn = 'TOTAL_PRICE' AND @SortOrder='ASC')   THEN TotalPrice   END ASC,                  
            CASE WHEN (@SortColumn = 'TOTAL_PRICE' AND @SortOrder='DESC')  THEN TotalPrice    END DESC ,                
                
   CASE WHEN (@SortColumn = 'PAYMENT_STATUS' AND @SortOrder='ASC')   THEN PaymentStatus   END ASC,                  
            CASE WHEN (@SortColumn = 'PAYMENT_STATUS' AND @SortOrder='DESC')  THEN PaymentStatus    END DESC ,                
                
   CASE WHEN (@SortColumn = 'PAYMENT_TYPE' AND @SortOrder='ASC')   THEN PaymentType   END ASC,                  
            CASE WHEN (@SortColumn = 'PAYMENT_TYPE' AND @SortOrder='DESC')  THEN PaymentType    END DESC ,                
                
   CASE WHEN (@SortColumn = 'INVOICE_NO' AND @SortOrder='ASC')   THEN id   END ASC,                  
            CASE WHEN (@SortColumn = 'INVOICE_NO' AND @SortOrder='DESC')  THEN id    END DESC ,                
                
   CASE WHEN (@SortColumn = 'STATUS' AND @SortOrder='ASC')   THEN [status]   END ASC,                  
            CASE WHEN (@SortColumn = 'STATUS' AND @SortOrder='DESC')  THEN  [status]    END DESC ,                
                
   CASE WHEN (@SortColumn = 'DRIVER_FULLNAME' AND @SortOrder='ASC')   THEN   driverName  END ASC,                  
            CASE WHEN (@SortColumn = 'DRIVER_FULLNAME' AND @SortOrder='DESC')  THEN driverName    END DESC                 
           ,      
      CreatedOn desc      
                    
 OFFSET (@next*@offset)-@next ROWS                                                            
    FETCH NEXT @next ROWS ONLY                                                            
                                                            
END  
GO
/****** Object:  StoredProcedure [dbo].[OrderLogInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Note NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Note
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderLogUpdate

/***** Object:  StoredProcedure  [dbo].[OrderLogUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderLogSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderLogSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[OrderId]
	 	,
	 		R.[Note]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [OrderLog] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : PaymentLogSelect

/***** Object:  StoredProcedure [dbo].[PaymentLogSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[OrderLogUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Note NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [OrderLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId]),
			 	[Note] = ISNULL(@Note,[Note])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderLogXMLSave


GO
/****** Object:  StoredProcedure [dbo].[OrderLogXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderLogXMLSave]
 @OrderLogXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',
		NDS.DT.value('(Note)[1]', 'NVARCHAR') AS 'Note'
  FROM 
	@OrderLogXml.nodes('/root/OrderLog') AS NDS(DT)
   )
   MERGE INTO OrderLog R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),R.[Note] =ISNULL(x.[Note] ,R.[Note])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[OrderId],
		[Note]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[OrderId],x.[Note]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : PaymentLogInsert

/***** Object:  StoredProcedure [dbo].[PaymentLogInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[OrderPaymentUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderPaymentUpdate]                          
 --declare             
   @Id    BIGINT=NULL,                          
            @UpdatedBy    BIGINT=NULL,                                 
 @paymentStatus  nvarchar(50)=NULL ,                  
 @QrCode nvarchar(max)                  
AS                          
BEGIN                          
 -- SET NOCOUNT ON added to prevent extra result sets from                          
 -- interfering with SELECT statements.                          
 SET NOCOUNT ON;                          
                          
     if not exists (select * from [Order] where id=@Id and Status='DELIVERED')            
     begin            
    UPDATE [Order]                          
 SET                          
                             
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),      
     QrCode=isnull(@QrCode,QrCode)             ,  
  [paymentstatus]=isnull(@paymentStatus,@paymentStatus)  ,
    [paymentdate]= switchoffset(sysdatetimeoffset(),'+00:00')      
               
                
  WHERE                          
  (                          
   [Id]=@Id                          
  )                          
                
      
        
        
     declare @lognote varchar(max)='QR Code payment received and updated  '            
     INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )              
      VALUES (1,1,@UpdatedBy,@UpdatedBy,1,@Id,@lognote)           
          
  end            
END                          
                          
                          
---------------------------------------------------                          
---------------------------------------------------                          
-- Procedure : OrderXMLSave       
GO
/****** Object:  StoredProcedure [dbo].[OrderSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[DeliveryDate]
	 	,
	 		R.[DeliveryType]
	 	,
	 		R.[TotalItems]
	 	,
	 		R.[DeliveryPrice]
	 	,
	 		R.[DriverId]
	 	,
	 		R.[Status]
	 	,
	 		R.[PaymentType]
	 	,
	 		R.[PaidAmount]
	 	,
	 		R.[Tax]
	 	,
	 		R.[SubTotal]
	 	,
	 		R.[TotalPrice]
	 	
	 	,
	 		R.[ExpectedPickUpMin]
	 	,
	 		R.[ExpectedPickUpMax]
	 	,
	 		R.[ExpectedDeliveryMin]
	 	,
	 		R.[ExpectedDeliveryMax]
	 	,
	 		R.[CartId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Order] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemSelect

/***** Object:  StoredProcedure [dbo].[OrderItemSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[OrderSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--           select * from [users] where firstname='bhawana'                                             
CREATE PROCEDURE [dbo].[OrderSelectApp](                                                            
--declare                                                         
@Id         BIGINT =null,                                                                
@Slug     NVARCHAR(255)=NULL,                                                                
@next   int = NULL,                                                                
@offset  int = NULL,                                                                
@RelationTable NVARCHAR(50)=null,                                                                
@RelationId bigint=NULL,                                                                
@TenantId   INT=NULL,                                                                
@LanguageId  BIGINT=NULL,                                                                
@userid      BIGINT=null    ,                                                        
                                                
@DistrictId      BIGINT=NULL    ,                                                   
@SubDistrictId      BIGINT=NULL    ,                                                   
@DriverId      BIGINT=NULL    ,                                                
@Status      NVARCHAR(50)=  null  ,                                              
                                              
@Name      NVARCHAR(50)=  null  ,                                              
@email      NVARCHAR(50)=  null  ,                                              
@Phone      NVARCHAR(50)=  null  ,                                
@deliveryType Nvarchar(50)=null                                
)   AS                                                           
BEGIN                                                                
                                                                
IF @next IS NULL                                                                
BEGIN                                                                
SET @next =100000                                                                
SET @offset=1                                                                
END                                                                
     declare @ratingStatus bit=0;                                                     
  if(@LanguageId=null)                                                        
  begin                                                         
    select @LanguageId=languageid from users where id=@userid                                                        
  end                   
  if exists(select * from rating where orderid=@id)                
  begin                 
    set @ratingStatus=1                
  end                
                                                        
 SELECT                                                                
    o.[Id]                                                                
   ,                                                                
    o.[TenantId]                                                                
   ,                                                                
    o.[Slug]                                                                
   ,                                                                
    o.[CreatedBy]                                                                
   ,                                                                
    o.[UpdatedBy]                                                                
   ,                                                                
    o.[CreatedOn]                                                                
   ,                                                                
    o.[UpdatedOn]                                                                
   ,                           
    o.[IsDeleted]                                                                
   ,                                                                
    o.[IsActive]                             
                                                                  
   ,                                     
    o.Userid                                                                
   ,                                                   
    o.[Status]                                                                
   ,                                                       
    o.[Tax]                                                                
   ,                                                
    o.[SubTotal]                                                                
   ,                                      
    o.[TotalPrice]                                                                
   ,                                                                
    o.[TotalItems]                                                        
   ,                                                                
  o.[DeliveryType],                                     
                                                              
  o.Deliverydate,                                                              
  o.Pickupdate,                                                         
  o.deliverySlot,                                                              
  o.pickupSlot,                                                           
                                                  
  o.Paymentresponse,                                                          
  o.Discount,                                                          
      o.paymentstatus,                                                      
    o.LogisticCharge,                                                        
 o.paymentType,                                                    
 o.change,                                                    
 o.deliveryprice,                                        
 --o.adminnote,                      
 rating=@ratingStatus    ,          
        
 IsDelivered=0,        
 IsPickup=0      
         
         
 ,        
                      
 CASE                       
           WHEN O.AdminNote is null                      
           THEN (select adminNote from Users where id=O.userid)                      
           ELSE o.adminNote                      
       END AS adminnote,                      
                      
                      
o.paymentDate,                                      
o.collectedDate  ,                                      
                                      
 o.deliveredDate,                                      
 o.LaundryInstruction,                      
                       
                                      
                                      
                                      
  overall_count = COUNT(*) OVER(),                                                                
  OrderItemXml = (                                                                
  select                                                                
  OrderItem.Id,                                       
  OrderItem.ProductId,                                                                
  OrderItem.OrderId,                                                                
  OrderItem.ServiceId,                                                                
  OrderItem.TotalPrice,                                              
  OrderItem.Quantity,                                                      
  OrderItem.IsPacking,                                                     
  OrderItem.SubTotal,                                                     
  OrderItem.TotalPrice,                                                     
  OrderItem.PackingId,                                                    
  OrderItem.PackingPrice,                                         
  orderitem.IsDeleted,                                                    
  product.Name  AS ProductName,                                                                
  [service].Name  AS ServiceName,                                    
  (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  OrderItem.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductNameL,                    
  (SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  OrderItem.ServiceId AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceNameL,                                                                
  (SELECT TOP(1) [Path] FROM FileGroupItems FG WHERE FG.[TypeId] = OrderItem.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0 ) AS [Path],                                                                
  (SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ServiceId =  OrderItem.ServiceId AND PP.ProductId =  OrderItem.ProductId AND PP.[IsDeleted] = 0 ) AS Price                                                                
             
                                                                
  FROM                                                                 
  orderItem OrderItem                                               
  INNER JOIN                                                                 
  Product product ON product.Id =  OrderItem.ProductId                                                                
  INNER JOIN                                                                
  [Service] [service] ON [service].Id =  OrderItem.ServiceId                                                    
  left join                                                             
  productprice pp on OrderItem.productid =pp.productid and OrderItem.serviceid=pp.serviceid                                                                
  WHERE OrderItem.orderid =  o.Id AND OrderItem.IsDeleted = 0                                             
  FOR XML AUTO,ROOT,ELEMENTs                                                                
 )  ,                                                          
 AddressXml=(                                                        
                                                         
   SELECT                                                                  
    R.[Id],                                                                  
    R.[AddressLine1],                                                                  
   R.[AddressLine2],                                                                  
    R.[DistrictId],                                                       
    R.[SubDistrictId],                                                                  
    R.[PostalCode],                                                                  
    R.[Tag],                                                                  
    R.[Latitude],                                                                  
    R.[Longitude],                                                                  
   R.[HouseNumber],                                                                  
   R.[StreetNumber],                                                              
   R.[Note],                                                                  
   R.[Type],                                                                  
                                                                  
                                                                
   R.[BuildingName] ,                                                 
   R.[Floor] ,                                                                
   R.[UnitNo] ,                                                                
   R.[PhoneNo] ,                                                                
   R.[PhoneExt] ,                         
   R.[ResidenceType] ,                                                                
        -- AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id                                                           
                                                                     
   D.Name  AS DistrictName,                                                                  
   SD.Name  AS SubDistrictName,                                     
    PD.Name As ProvinceName,                                      
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,                                                                  
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId  ) AS SubDistrictNameL  ,                                                                
     (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL                                              
                                                                  
                                              
 FROM deliveryaddress R INNER JOIN                                                                  
 [District] D ON D.Id =  R.[DistrictId]                                                                  
 INNER JOIN                                                                   
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                                      
 INNER JOIN                                               
 Province PD ON PD.Id =  R.ProvinceId                                       
                                    
                                    
 where R.orderid=o.ID                                                            
      FOR XML AUTO,ROOT,ELEMENTs                                                           
    )  ,                                                  
 DriverXml=                                                  
 (                                                  
   select users.Id,Email,PhoneNumber,UserName,FirstName,LastName,BikeInformation,NationalId,LicenceId,LicencePlate,                  
   profilePic=(select top 1 path from filegroupitems where TypeId=o.driverid and [type]='PROFILE_PIC' and IsDeleted=0 and IsActive=1)                                   
   , DriverLocation.[Latitude],DriverLocation.[Longititude]                                   
  -- ,Latitude=(select [Latitude] from driverlocation  where userid=users.id)         select * from users where id=104                          
  -- ,Longititude=(select Longititude from driverlocation  where userid=users.id)                                  
                                     
   from users users left join DriverLocation DriverLocation on users.id=DriverLocation.userid where users.id=o.driverid                                                  
   --from users                                                
   FOR XML AUTO,ROOT,ELEMENTs                                                      
 )    ,              
              
  DeliveryDriverXml=                                                  
 (                                                  
   select users.Id,Email,PhoneNumber,UserName,FirstName,LastName,BikeInformation,NationalId,LicenceId,LicencePlate,                  
   profilePic=(select top 1 path from filegroupitems where TypeId=o.driverid and [type]='PROFILE_PIC' and IsDeleted=0 and IsActive=1)                                   
   , DriverLocation.[Latitude],DriverLocation.[Longititude]                                   
  -- ,Latitude=(select [Latitude] from driverlocation  where userid=users.id)         select * from users where id=104                          
  -- ,Longititude=(select Longititude from driverlocation  where userid=users.id)                                  
                                     
   from users users left join DriverLocation DriverLocation on users.id=DriverLocation.userid where users.id=(select top 1 DriverId from DriverRideLogs where OrderId=o.id and Type='DELIVERED')                                                 
   --from users                                                
  FOR XML AUTO,ROOT,ELEMENTs                                                      
 )    ,              
 PickUpDriverXml=                                                  
 (                                                  
   select users.Id,Email,PhoneNumber,UserName,FirstName,LastName,BikeInformation,NationalId,LicenceId,LicencePlate,                  
   profilePic=(select top 1 path from filegroupitems where TypeId=o.driverid and [type]='PROFILE_PIC' and IsDeleted=0 and IsActive=1)                                   
   , DriverLocation.[Latitude],DriverLocation.[Longititude]                                   
  -- ,Latitude=(select [Latitude] from driverlocation  where userid=users.id)         select * from users where id=104                          
  -- ,Longititude=(select Longititude from driverlocation  where userid=users.id)                                  
                                     
   from users users left join DriverLocation DriverLocation on users.id=DriverLocation.userid where users.id=(select top 1 DriverId from DriverRideLogs where OrderId=o.id and Type='PICKUP')                                                 
   --from users                                                
   FOR XML AUTO,ROOT,ELEMENTs                                          
 )    ,              
 CustomerXml=                                                
 (                                                
   select Id,Email,PhoneNumber,UserName,FirstName,LastName from users where id=o.UserId                                            
                                                 
   FOR XML AUTO,ROOT,ELEMENTs                                                    
 )   ,            
 O.qrcode  ,    
 activeorder=(select isnull(count(*),0) from [order] where DriverId=o.DriverId and ([Status]= 'AWAITING_COLLECTION' or [status]='ON_THE_WAY'))    
 --activeorder=0    
                                            
 FROM [order] o                                                                  
 WHERE                                
 (                              
  @RelationTable IS NULL                                                                              
       or                                                                      
       (                                                                                    
           @RelationTable = 'NEW_ORDERS'                                                                                  
           and                                                                                
           (                                                                            
            o.[status]='NEW'                                                  
                                                  
            )                                                                            
       )                               
                              
   or                                                                                  
       (                                                                                    
           @RelationTable = 'COLLECTING_ORDERS'                                                                                  
           and                                                                                
           (                                                                            
            o.[status]='ASSIGNED_DRIVER'                                                  
            or                                                   
         o.[status]='AWAITING_COLLECTION'                                                  
            or                                                   
            o.[status]='COLLECTED'                                       
                                                  
            )                                                                            
       )                              
    or                                                                                  
       (                                  
           @RelationTable = 'CLEANING_ORDERS'                                                                                  
           and                                                                                
           (                                                                            
            o.[status]='CLEANING_IN_PROGRESS'                                                  
            or                                                   
            o.[status]='CLEANING_COMPLETED'                                                  
                                                  
       
            )                                                                            
       )                              
    or                                                                                  
       (                          
           @RelationTable = 'DELIVERY_ORDERS'                                                                                  
           and                                                                                
           (                                                                            
    o.[status]='AWAITING_COLLECTION_DELIVERY'                                                  
            or                                                   
            o.[status]='ON_THE_WAY'                                                  
                                                  
                                                  
            )                                                                            
       )                              
     or                                                      
       (                                                                                    
@RelationTable = 'COLLECTING_ORDERS_ASSIGNED'                                                                                  
           and                                                                                
           (                                                                            
            o.[status]='ASSIGNED_DRIVER'                                                  
            or                                             
            o.[status]='AWAITING_COLLECTION'                                                  
                                                  
                                                  
            )                                                                            
       )                              
    or                                                               
       (                                                                                    
           @RelationTable = 'TOTAL_ORDERS'                                      
           and                                                                                
           (                                                                            
            o.[status]='NEW'                                                  
            or                                                   
            o.[status]='ASSIGNED_DRIVER'                                                  
            or                                                   
            o.[status]='AWAITING_COLLECTION'                                       
             or                                                   
            o.[status]='COLLECTED'                               
   or                                                   
            o.[status]='CLEANING_IN_PROGRESS'                               
   or                                                   
            o.[status]='CLEANING_COMPLETED'                               
   or                                                   
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                               
   or                                                   
            o.[status]='ON_THE_WAY'                               
            )                                                              
       )                              
 )                                                          
 AND                                                                
 (                                                                
  @Id IS NULL                                             
  OR                                                                
  o.[Id] = @Id                                                                
 )                                                                
 AND                                                                
 (                                                                
  @Slug IS NULL                                                                
  OR                                                                
  o.[Slug] = @Slug                            
 )                                                                
 AND                                                                
 (                                                                
  o.[IsDeleted] = 0                                       
 )                                                                
 AND                                                                
 (                                                                
  @TenantId IS NULL                                                                
  OR                                                  
  o.[TenantId] = @TenantId                                                                
 )                                                                
 AND                                                                
 (                                        
  @userid IS NULL                                                                
   OR                                                                
  o.userid =  @userid                             
 )                                                    
                                                  
  AND                                                 
 (                                                                
  @DriverId IS NULL                                                                
   OR                                                                
  o.driverid =  @DriverId                                                                
 )                                                 
  AND                                                                
 (                                                                
  @Status IS NULL                                               
   OR                                                                
  o.[Status] =  @Status                                 
 )                                                 
  AND                                                                
 (                                                                
  @DistrictId IS NULL                                                                
   OR                                                                
  o.id in(  select orderid from deliveryaddress where districtid=@DistrictId )                                                                 
 )                                                 
 AND                                                        
 (                                          
  @SubDistrictId IS NULL                                                    
   OR                                                                
  o.id in(  select orderId from deliveryaddress where SubDistrictId=@SubDistrictId )                                                        
 )                                                 
 -- AND                                                                
 --(                                                                
 -- @Name IS NULL                                                                
 --  OR                                                                
 -- o.userid in (select id from users where [FirstName] like '%'+@Name+'%' or LastName like '%'+@Name+'%' )                                                                
 --)                                                   
   AND                                                                
 (                                                                
  @email IS NULL                                                                
   OR                                            
  o.userid in (select id from users where [Email] like '%'+@email+'%')                                                
                                                    
                                                
 )                                                   
  AND                                                                
 (                                                                
  @Phone IS NULL                                    
   OR                                                                
  o.userid in (select id from users where phonenumber like '%'+@Phone+'%')                                                   
   OR                                                                
  o.userid in (select userid from DeliveryAddress where PhoneNo like '%'+@Phone+'%')                                                  
  OR                                                   
  o.userid in (select userid from DeliveryAddress where alterNumber like '%'+@Phone+'%')                                
 )                                      
 And                                
 (                                
  @deliveryType is null                                
  or                                 
  o.deliveryType=@deliveryType                                
                                 
 )                                
                                                
                                                
                                                
 --Order by o.Id desc                          
 ORDER BY                    
 o.CreatedOn desc,                    
 (                       
                    
    CASE DeliveryType                        
                            
    WHEN 'SAMEDAY'                        
    THEN 1                        
 WHEN 'EXPRESS'                        
    THEN 2                        
                            
    WHEN 'NORMAL'                        
    THEN 3                        
                            
    END              
) ASC                        
                        
                        
                        
 OFFSET (@next*@offset)-@next ROWS                                                                
    FETCH NEXT @next ROWS ONLY                                                                
                                                                
END                                        
                                                                
                                                                
                                                                
---------------------------------------------------                                                                
--------------------------------------------------- 
GO
/****** Object:  StoredProcedure [dbo].[OrderStatsSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderStatsSelect]
(  
@TenantId INT = NULL
)  
AS BEGIN  
SET NOCOUNT ON  
SELECT  
  
'Total Orders' AS TotalOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'NORMAL' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalNormalOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'SAMEDAY' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalSameDayOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'EXPRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalExpressOrderCount,  
 
'New Orders' AS NewOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'SAMEDAY' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewSameDayOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'EXPRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewExpressOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'NORMAL' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewNormalOrderCount,  
  
'Collecting' AS CollectionOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'ASSIGNED_DRIVER' OR [STATUS] = 'AWAITING_COLLECTION' OR [STATUS] = 'COLLECTED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'ASSIGNED_DRIVER' OR [STATUS] = 'AWAITING_COLLECTION') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionAssignedOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'COLLECTED' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionCollectedOrderCount,
  
'Cleaning' AS CleaningOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'CLEANING_IN_PROGRESS' OR [STATUS] = 'CLEANING_COMPLETED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'CLEANING_IN_PROGRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningInprogressOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'CLEANING_COMPLETED' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningCompletedOrderCount,  
  
'Delivery' AS DeliveryOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'AWAITING_COLLECTION_DELIVERY' OR [STATUS] = 'ON_THE_WAY') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS DeliveryOrderTotalCount

END

--SELECT [STATUS], DeliveryType From [Order] Where IsDeleted = 0 AND @TenantId IS NULL OR [TenantId] = @TenantId
GO
/****** Object:  StoredProcedure [dbo].[OrderStatusUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderStatusUpdate]                        
--declare      
  @Id    BIGINT=null,                
   @TenantId INT=null,              
   @UpdatedBy    BIGINT=null,                                         
   @UserId    BIGINT=NULL,                             
   @Status NVARCHAR(MAX)=null                             
AS                        
BEGIN                        
 -- SET NOCOUNT ON added to prevent extra result sets from                        
 -- interfering with SELECT statements.                        
 SET NOCOUNT ON;           
           
 declare @paymentType nvarchar(50),@paymentStatus nvarchar(30);        
 declare @deliveredDate datetimeoffset(7),@collectedDate datetimeoffset(7) ,@paymentDate datetimeoffset(7)=null    
 select @paymentType= paymenttype from [Order] where   id=@id          
          
  if((@paymentType='CASH') and (@Status='COLLECTED'))          
  begin           
     set @paymentStatus='PAID';     
     set @paymentDate=GETUTCDATE()  
  end          
  else          
  begin          
     set @paymentStatus= null;          
  end          
      
  if(@Status='COLLECTED')      
  begin       
   set  @collectedDate=GETUTCDATE()      
  end      
      
  if(@Status='DELIVERED')      
  begin       
   set  @deliveredDate=GETUTCDATE()      
  end      
          
       
      
 UPDATE [Order]                        
 SET               
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),                        
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),                                
     [PaymentStatus]=ISNULL(@paymentStatus,[PaymentStatus]),              
     [Status] = ISNULL(@Status,[Status])  ,      
  [deliveredDate]=ISNULL(@deliveredDate,[deliveredDate]),      
  [collectedDate]=isnull(@collectedDate,[collectedDate]),
  [paymentDate]=isnull(@paymentDate,[paymentdate])
  WHERE                        
  (                        
   [Id]=@Id                        
  )                        
  AND                        
  (                        
  [TenantId] =  @TenantId                        
  )                 
              
      
    
    
  if(@Status='COLLECTED')    
  begin    
    
   update [order] set DriverId=null where id=@Id        
    
   insert into DriverRideLogs ([StartOn],[CompleteOn],[IsComplete],[DriverId],[OrderId],[Type]) values             
   ( switchoffset(sysdatetimeoffset(),'+00:00'),switchoffset(sysdatetimeoffset(),'+00:00'),1,@updatedby,@id,'PICKUP')            
    
   end    
  if(@Status='DELIVERED')            
    insert into DriverRideLogs ([StartOn],[CompleteOn],[IsComplete],[DriverId],[OrderId],[Type]) values             
   ( switchoffset(sysdatetimeoffset(),'+00:00'),switchoffset(sysdatetimeoffset(),'+00:00'),1,@updatedby,@id,'DELIVERED')            
            
END                        
                        
                        
---------------------------------------------------                        
---------------------------------------------------                        
-- Procedure : OrderXMLSave  
  
GO
/****** Object:  StoredProcedure [dbo].[OrderUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderUpdate]                      
 --declare         
   @Id    BIGINT=NULL,                      
   @TenantId   INT=NULL,                      
   @Slug NVARCHAR(MAX)=NULL,                      
     @UpdatedBy    BIGINT=NULL,                      
     @IsDeleted    BIT=NULL,                      
     @IsActive    BIT=NULL,                      
     @UserId    BIGINT=NULL,                      
   @DeliveryDate  DATETIMEOFFSET(7)=NULL,                      
     @DeliveryType    nvarchar(100)=NULL,                      
   @TotalItems   INT=NULL,                      
   @DeliveryPrice    DECIMAL(18,2)=NULL,                      
     @DriverId    BIGINT=NULL,                      
   @Status NVARCHAR(MAX)=NULL,                      
   @PaymentType NVARCHAR(MAX)=NULL,                      
   @PaidAmount    DECIMAL(18,2)=NULL,                      
   @Tax    DECIMAL(18,2)=NULL,                      
   @SubTotal    DECIMAL(18,2)=NULL,                      
   @TotalPrice    DECIMAL(18,2)=NULL,                      
     @AdressId    BIGINT=NULL,                      
     @ExpectedPickUpMin    BIGINT=NULL,                      
     @ExpectedPickUpMax    BIGINT=NULL,                      
     @ExpectedDeliveryMin    BIGINT=NULL,                      
     @ExpectedDeliveryMax    BIGINT=NULL,                      
     @CartId    BIGINT=NULL ,                    
  @Deliveryaddress nvarchar(max)=null  ,                  
                  
                    
    @PickupDate DATETIMEOFFSET(7)=NULL,                  
    @deliverySlot NVARCHAR(100)=NULL,                    
   @pickupSlot NVARCHAR(100)=NULL  ,                
  @LogisticCharge DECIMAL(18,2)=NULL,                
  @Discount     DECIMAL(18,2)=NULL ,                
 @paymentResponse nvarchar(100)=null ,               
    @paymentStatus nvarchar(100)=null   ,            
 @change  DECIMAL(18,2)=NULL ,              
 @note nvarchar(max) ,           
    @AdminNote nvarchar(max)               
AS                      
BEGIN                      
 -- SET NOCOUNT ON added to prevent extra result sets from                      
 -- interfering with SELECT statements.                      
 SET NOCOUNT ON;                      
                      
     if not exists (select * from [Order] where id=@Id and Status='DELIVERED')        
     begin        
    UPDATE [Order]                      
 SET                      
     [Slug] = ISNULL(@Slug,[Slug]),                      
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),                      
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),                       
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),                      
     [IsActive] = ISNULL(@IsActive,[IsActive]),                      
     [UserId] = ISNULL(@UserId,[UserId]),                      
     [DeliveryDate] = ISNULL(@DeliveryDate,[DeliveryDate]),                      
     [DeliveryType] = ISNULL(@DeliveryType,[DeliveryType]),                      
     [TotalItems] = ISNULL(@TotalItems,[TotalItems]),                      
     [DeliveryPrice] = ISNULL(@DeliveryPrice,[DeliveryPrice]),                      
     [DriverId] = ISNULL(@DriverId,[DriverId]),                      
     [Status] = ISNULL(@Status,[Status]),                      
     [PaymentType] = ISNULL(@PaymentType,[PaymentType]),                      
     [PaidAmount] = ISNULL(@PaidAmount,[PaidAmount]),                      
     [Tax] = ISNULL(@Tax,[Tax]),                      
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),                      
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),                      
                      
     [ExpectedPickUpMin] = ISNULL(@ExpectedPickUpMin,[ExpectedPickUpMin]),                      
     [ExpectedPickUpMax] = ISNULL(@ExpectedPickUpMax,[ExpectedPickUpMax]),                      
     [ExpectedDeliveryMin] = ISNULL(@ExpectedDeliveryMin,[ExpectedDeliveryMin]),                      
     [ExpectedDeliveryMax] = ISNULL(@ExpectedDeliveryMax,[ExpectedDeliveryMax]),                      
     [CartId] = ISNULL(@CartId,[CartId]) ,                    
     [Deliveryaddress]=isnull(@Deliveryaddress,[Deliveryaddress])  ,                  
                  
                   
    PickupDate =isnull(@PickupDate,PickupDate)  ,                  
    deliverySlot=isnull(@deliverySlot,deliverySlot)  ,                   
    pickupSlot =isnull(@pickupSlot,pickupSlot) ,                   
    LogisticCharge=isnull(@LogisticCharge,LogisticCharge) ,                
  Discount=isnull(@Discount,Discount) ,                
  paymentResponse=isnull(@paymentResponse,paymentResponse) ,              
   paymentStatus=isnull(@paymentStatus,paymentStatus)  ,            
   change=isnull(@change,change)  ,            
note=isnull(@note,note)  ,          
    adminNote=isnull(@AdminNote,AdminNote)          
            
            
  WHERE                      
  (                      
   [Id]=@Id                      
  )                      
  AND                      
  (                      
  [TenantId] =  @TenantId                      
  )            
 if(@Status='COLLECTED')    
 begin     
   update [order] set DriverId=null,PaymentStatus='PAID',paymentDate= switchoffset(sysdatetimeoffset(),'+00:00') where id=@Id    
 end    
    
    
     declare @lognote varchar(max)='Order updated By Admin current staus id '+@status        
     INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )          
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,1,@Id,@lognote)       
      
  end        
END                      
                      
                      
---------------------------------------------------                      
---------------------------------------------------                      
-- Procedure : OrderXMLSave 
GO
/****** Object:  StoredProcedure [dbo].[OrderXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderXMLSave]
 @OrderXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(DeliveryDate)[1]', 'DATETIMEOFFSET(7)') AS 'DeliveryDate',
		NDS.DT.value('(DeliveryType)[1]', 'BIGINT') AS 'DeliveryType',
		NDS.DT.value('(TotalItems)[1]', 'INT') AS 'TotalItems',
		NDS.DT.value('(DeliveryPrice)[1]', 'DECIMAL(18,2)') AS 'DeliveryPrice',
		NDS.DT.value('(DriverId)[1]', 'BIGINT') AS 'DriverId',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(PaymentType)[1]', 'NVARCHAR') AS 'PaymentType',
		NDS.DT.value('(PaidAmount)[1]', 'DECIMAL(18,2)') AS 'PaidAmount',
		NDS.DT.value('(Tax)[1]', 'DECIMAL(18,2)') AS 'Tax',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		
		NDS.DT.value('(ExpectedPickUpMin)[1]', 'BIGINT') AS 'ExpectedPickUpMin',
		NDS.DT.value('(ExpectedPickUpMax)[1]', 'BIGINT') AS 'ExpectedPickUpMax',
		NDS.DT.value('(ExpectedDeliveryMin)[1]', 'BIGINT') AS 'ExpectedDeliveryMin',
		NDS.DT.value('(ExpectedDeliveryMax)[1]', 'BIGINT') AS 'ExpectedDeliveryMax',
		NDS.DT.value('(CartId)[1]', 'BIGINT') AS 'CartId'
  FROM 
	@OrderXml.nodes('/root/Order') AS NDS(DT)
   )
   MERGE INTO [Order] R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[DeliveryDate] =ISNULL(x.[DeliveryDate] ,R.[DeliveryDate]),R.[DeliveryType] =ISNULL(x.[DeliveryType] ,R.[DeliveryType]),R.[TotalItems] =ISNULL(x.[TotalItems] ,R.[TotalItems]),R.[DeliveryPrice] =ISNULL(x.[DeliveryPrice] ,R.[DeliveryPrice]),R.[DriverId] =ISNULL(x.[DriverId] ,R.[DriverId]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[PaymentType] =ISNULL(x.[PaymentType] ,R.[PaymentType]),R.[PaidAmount] =ISNULL(x.[PaidAmount] ,R.[PaidAmount]),R.[Tax] =ISNULL(x.[Tax] ,R.[Tax]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),R.[ExpectedPickUpMin] =ISNULL(x.[ExpectedPickUpMin] ,R.[ExpectedPickUpMin]),R.[ExpectedPickUpMax] =ISNULL(x.[ExpectedPickUpMax] ,R.[ExpectedPickUpMax]),R.[ExpectedDeliveryMin] =ISNULL(x.[ExpectedDeliveryMin] ,R.[ExpectedDeliveryMin]),R.[ExpectedDeliveryMax] =ISNULL(x.[ExpectedDeliveryMax] ,R.[ExpectedDeliveryMax]),R.[CartId] =ISNULL(x.[CartId] ,R.[CartId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[DeliveryDate],
		[DeliveryType],
		[TotalItems],
		[DeliveryPrice],
		[DriverId],
		[Status],
		[PaymentType],
		[PaidAmount],
		[Tax],
		[SubTotal],
		[TotalPrice],
		
		[ExpectedPickUpMin],
		[ExpectedPickUpMax],
		[ExpectedDeliveryMin],
		[ExpectedDeliveryMax],
		[CartId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[DeliveryDate],x.[DeliveryType],x.[TotalItems],x.[DeliveryPrice],x.[DriverId],x.[Status],x.[PaymentType],x.[PaidAmount],x.[Tax],x.[SubTotal],x.[TotalPrice],x.[ExpectedPickUpMin],x.[ExpectedPickUpMax],x.[ExpectedDeliveryMin],x.[ExpectedDeliveryMax],x.[CartId]
    );
END
GO
/****** Object:  StoredProcedure [dbo].[PackingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ProductId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Packing]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[ProductId],[ServiceId],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@ProductId,@ServiceId,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingUpdate

/***** Object:  StoredProcedure  [dbo].[PackingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PackingLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@PackingId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PackingLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PackingId],[LanguageId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PackingId,@LanguageId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[PackingLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PackingLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[PackingId]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[Name]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [PackingLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageSelect

/***** Object:  StoredProcedure [dbo].[DefaultMessageSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[PackingLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@PackingId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PackingLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[PackingId] = ISNULL(@PackingId,[PackingId]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Name] = ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[PackingLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingLanguageMappingXMLSave]
 @PackingLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(PackingId)[1]', 'BIGINT') AS 'PackingId',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name'
  FROM 
	@PackingLanguageMappingXml.nodes('/root/PackingLanguageMapping') AS NDS(DT)
   )
   MERGE INTO PackingLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[PackingId] =ISNULL(x.[PackingId] ,R.[PackingId]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Name] =ISNULL(x.[Name] ,R.[Name])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[PackingId],
		[LanguageId],
		[Name]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[PackingId],x.[LanguageId],x.[Name]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageInsert

/***** Object:  StoredProcedure [dbo].[DefaultMessageInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PackingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[ProductId]
	 	,
	 		R.[ServiceId]
	 	,
	 		R.[Price]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Packing] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[PackingLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[PackingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ProductId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Packing]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[Price] = ISNULL(@Price,[Price])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[PackingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingXMLSave]
 @PackingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price'
  FROM 
	@PackingXml.nodes('/root/Packing') AS NDS(DT)
   )
   MERGE INTO Packing R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[Price] =ISNULL(x.[Price] ,R.[Price])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[ProductId],
		[ServiceId],
		[Price]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[ProductId],x.[ServiceId],x.[Price]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[PackingLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PasswordLogInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PasswordLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Count   INT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PasswordLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Count]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Count
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : PasswordLogUpdate

/***** Object:  StoredProcedure  [dbo].[PasswordLogUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PasswordLogSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PasswordLogSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL,
@UserId     BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[Count]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [PasswordLog] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		@UserId IS NULL
			OR
		R.[UserId]  =  @UserId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : MessageLogSelect

/***** Object:  StoredProcedure [dbo].[MessageLogSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[PasswordLogUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PasswordLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Count   INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PasswordLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Count] = ISNULL(@Count,[Count])
	 WHERE
	 (
	  [Id]=@Id
		OR
		(
			@IsDeleted = 1
				AND
			[UserId] =  @UserId
		)
	 )
	 --AND
	 --(
		--[TenantId] =  @TenantId
	 --)
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : PasswordLogXMLSave
GO
/****** Object:  StoredProcedure [dbo].[PasswordLogXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PasswordLogXMLSave]
 @PasswordLogXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(Count)[1]', 'INT') AS 'Count'
  FROM 
	@PasswordLogXml.nodes('/root/PasswordLog') AS NDS(DT)
   )
   MERGE INTO PasswordLog R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[Count] =ISNULL(x.[Count] ,R.[Count])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[Count]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[Count]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : MessageLogInsert

/***** Object:  StoredProcedure [dbo].[MessageLogInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PaymentLogInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogInsert](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @UserId    BIGINT=NULL,  
   @CardNumber NVARCHAR(MAX)=NULL,  
   @Amount    DECIMAL(18,2)=NULL,  
     @OrderId    BIGINT=NULL,  
   @TransactionId NVARCHAR(MAX)=NULL,  
   @Status NVARCHAR(MAX)=NULL  
)  
AS   
BEGIN  
SET NOCOUNT ON;  
declare @result decimal;
   INSERT INTO [PaymentLog]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[CardNumber],[Amount],[OrderId],[TransactionId],[Status]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@CardNumber,@Amount,@OrderId,@TransactionId,@Status  
   )  
 


 SELECT @result=SCOPE_IDENTITY()   
 
   declare @note varchar(max)=cast( 'Order id='+cast(@OrderId as varchar(100))+' paylmentlog  id='+cast(@result as varchar(100)) as varchar(max));
     
	 INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )  
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@note)

	   select cast (@result as decimal) 
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : PaymentLogUpdate  
  
/***** Object:  StoredProcedure  [dbo].[PaymentLogUpdate] *****/  
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[PaymentLogSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[CardNumber]
	 	,
	 		R.[Amount]
	 	,
	 		R.[OrderId]
	 	,
	 		R.[TransactionId]
	 	,
	 		R.[Status]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [PaymentLog] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingSelect

/***** Object:  StoredProcedure [dbo].[PackingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[PaymentLogUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@CardNumber NVARCHAR(MAX)=NULL,
			@Amount    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL,
			@TransactionId NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PaymentLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[CardNumber] = ISNULL(@CardNumber,[CardNumber]),
			 	[Amount] = ISNULL(@Amount,[Amount]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId]),
			 	[TransactionId] = ISNULL(@TransactionId,[TransactionId]),
			 	[Status] = ISNULL(@Status,[Status])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : PaymentLogXMLSave


GO
/****** Object:  StoredProcedure [dbo].[PaymentLogXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogXMLSave]
 @PaymentLogXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(CardNumber)[1]', 'NVARCHAR') AS 'CardNumber',
		NDS.DT.value('(Amount)[1]', 'DECIMAL(18,2)') AS 'Amount',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',
		NDS.DT.value('(TransactionId)[1]', 'NVARCHAR') AS 'TransactionId',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status'
  FROM 
	@PaymentLogXml.nodes('/root/PaymentLog') AS NDS(DT)
   )
   MERGE INTO PaymentLog R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[CardNumber] =ISNULL(x.[CardNumber] ,R.[CardNumber]),R.[Amount] =ISNULL(x.[Amount] ,R.[Amount]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),R.[TransactionId] =ISNULL(x.[TransactionId] ,R.[TransactionId]),R.[Status] =ISNULL(x.[Status] ,R.[Status])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[CardNumber],
		[Amount],
		[OrderId],
		[TransactionId],
		[Status]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[CardNumber],x.[Amount],x.[OrderId],x.[TransactionId],x.[Status]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : PackingInsert

/***** Object:  StoredProcedure [dbo].[PackingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PostalCodeInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PostalCodeInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@PostalCode NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PostalCode]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PostalCode]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PostalCode
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[PostalCodeSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PostalCodeSelect](  
--declare
@Id         BIGINT =NULL,  
@Slug     NVARCHAR(255)=NULL,  
@next   int = NULL,  
@offset  int = NULL,  
@RelationTable NVARCHAR(50)=NULL,  
@RelationId bigint=NULL,  
@TenantId   INT=NULL  
)  AS
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =100000  
SET @offset=1  
END  
  
 SELECT  
    R.[Id]  
   ,  
    R.[TenantId]  
   ,  
    R.[Slug]  
   ,  
    R.[CreatedBy]  
   ,  
    R.[UpdatedBy]  
   ,  
    R.[CreatedOn]  
   ,  
    R.[UpdatedOn]  
   ,  
    R.[IsDeleted]  
   ,  
    R.[IsActive]  
   ,  
    R.[PostalCode]  
   ,  
 overall_count = COUNT(*) OVER()  
 FROM [PostalCode] R    
 WHERE   
 (  
  @RelationTable IS NULL  
 )  
 AND  
 (  
  @Id IS NULL  
  OR  
  R.[Id] = @Id  
 )  
 AND  
 (  
  @Slug IS NULL  
  OR  
  R.[Slug] = @Slug  
 )  
 AND  
 (  
  R.[IsDeleted] = 0  
 )  
 AND  
 (  
  @TenantId IS NULL  
  OR  
  R.[TenantId] = @TenantId  
 )  
  
 Order by R.PostalCode asc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END  
GO
/****** Object:  StoredProcedure [dbo].[PostalCodeUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PostalCodeUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@PostalCode NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PostalCode]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[PostalCode] = ISNULL(@PostalCode,[PostalCode])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[PostalCodeXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PostalCodeXMLSave]
 @PostalCodeXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(PostalCode)[1]', 'NVARCHAR') AS 'PostalCode'
  FROM 
	@PostalCodeXml.nodes('/root/PostalCode') AS NDS(DT)
   )
   MERGE INTO PostalCode R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[PostalCode] =ISNULL(x.[PostalCode] ,R.[PostalCode])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[PostalCode]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[PostalCode]
    );
END
GO
/****** Object:  StoredProcedure [dbo].[ProductInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductInsert](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
   @Gender NVARCHAR(MAX)=NULL,  
   @Name   NVARCHAR(100)=NULL ,
   @Rank  bigint =null
)  
AS   
BEGIN  
SET NOCOUNT ON;  
   INSERT INTO [Product]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Gender],[Name]  ,[Rank]
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Gender,@Name ,@Rank 
   )  
 SELECT SCOPE_IDENTITY()  
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : ProductUpdate  
  
/***** Object:  StoredProcedure  [dbo].[ProductUpdate] *****/  
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ProductLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProductLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[ProductId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@ProductId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[ProductLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ProductLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[ProductId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [ProductLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductPriceSelect

/***** Object:  StoredProcedure [dbo].[ProductPriceSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[ProductLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [ProductLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[ProductLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductLanguageMappingXMLSave]
 @ProductLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId'
  FROM 
  
	@ProductLanguageMappingXml.nodes('/ArrayOfProductLanguageMappingModel/ProductLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO ProductLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[ProductId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[ProductId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductPriceInsert

/***** Object:  StoredProcedure [dbo].[ProductPriceInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ProductPriceInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPriceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ServiceId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProductPrice]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[ServiceId],[ProductId],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@ServiceId,@ProductId,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductPriceUpdate

/***** Object:  StoredProcedure  [dbo].[ProductPriceUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ProductPriceSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPriceSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[ServiceId]
	 	,
	 		R.[ProductId]
	 	,
	 		R.[Price]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [ProductPrice] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeSelect

/***** Object:  StoredProcedure [dbo].[DeliveryTypeSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[ProductPriceUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPriceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ServiceId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [ProductPrice]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId]),
			 	[Price] = ISNULL(@Price,[Price])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductPriceXMLSave


GO
/****** Object:  StoredProcedure [dbo].[ProductPriceXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPriceXMLSave]
 @ProductPriceXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price'
  FROM 
  
	@ProductPriceXml.nodes('/ArrayOfProductPriceModel/ProductPriceModel') AS NDS(DT)
   )
   MERGE INTO ProductPrice R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),R.[Price] =ISNULL(x.[Price] ,R.[Price])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[ServiceId],
		[ProductId],
		[Price]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[ServiceId],x.[ProductId],x.[Price]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeInsert

/***** Object:  StoredProcedure [dbo].[DeliveryTypeInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ProductSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductSelect](      
--declare    
@Id         BIGINT =NULL,      
@Slug     NVARCHAR(255)=NULL,      
@next   int = NULL,      
@offset  int = NULL,      
@RelationTable NVARCHAR(50)=NULL,      
@RelationId bigint=NULL,      
@TenantId   INT=NULL      
)  AS    
BEGIN      
      
IF @next IS NULL      
BEGIN      
SET @next =100000      
SET @offset=1      
END      
      
 SELECT      
    R.[Id]      
   ,      
    R.[TenantId]      
   ,      
    R.[Slug]      
   ,      
    R.[CreatedBy]      
   ,      
    R.[UpdatedBy]      
   ,      
    R.[CreatedOn]      
   ,      
    R.[UpdatedOn]      
   ,      
    R.[IsDeleted]      
   ,      
    R.[IsActive]      
   ,      
    ImageXml=(      
     SELECT       
      FG.[Id],      
      FG.[CreatedBy],      
      FG.[UpdatedBy],      
      FG.[CreatedOn],      
      FG.[UpdatedOn],      
      FG.[IsDeleted],      
      FG.[IsActive],      
      FG.[Filename],      
      FG.[MimeType],      
      FG.[Thumbnail],      
      FG.[Size],      
      FG.[Path],      
      FG.[OriginalName],      
      FG.[OnServer],      
      FG.[TypeId],      
      FG.[Type]      
      FROM FileGroupItems FG      
      WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0      
      FOR XML AUTO,ROOT,ELEMENTs      
    )      
,      
    R.[Gender]      
   ,      
  R.[Name],      
 overall_count = COUNT(*) OVER(),      
 ProductLanguageXml = (      
  select      
  productlanguagemapping.Id,      
  productlanguagemapping.Name,      
  productlanguagemapping.LanguageId,      
  productlanguagemapping.ProductId,      
  [language].Name AS LanguageName      
      
  FROM       
  ProductLanguageMapping productlanguagemapping INNER JOIN       
  [Language] [language] ON [language].Id = productlanguagemapping.LanguageId      
  WHERE productlanguagemapping.ProductId =  R.Id AND productlanguagemapping.IsDeleted = 0      
  FOR XML AUTO,ROOT,ELEMENTs      
 ),      
 ProductPriceXml = (      
  select      
  productprice.Id,      
  productprice.Price,      
  productprice.ProductId,      
  productprice.ServiceId,      
  [service].Name AS ServiceName      
      
  FROM       
  ProductPrice productprice INNER JOIN       
  [Service] [service] ON [service].Id =  productprice.[ServiceId]      
  WHERE productprice.[ProductId] =  R.[Id] AND productprice.[IsDeleted] = 0  and [Service].[IsDeleted]=0    
  FOR XML AUTO,ROOT,ELEMENTs      
 ),R.[Rank]      
 FROM [Product] R        
 WHERE       
 (                                        
  @RelationTable IS NULL      
 )   
   
 AND      
 (      
  @Id IS NULL      
  OR      
  R.[Id] = @Id      
 )      
 AND      
 (      
  @Slug IS NULL      
  OR      
  R.[Slug] = @Slug      
 )      
 AND      
 (      
  R.[IsDeleted] = 0      
 )      
 AND      
 (      
  @TenantId IS NULL      
  OR      
  R.[TenantId] = @TenantId      
 )    
   
 and   
 R.id not in (select productid from ProductPrice productprice INNER JOIN       
  [Service] [service] ON [service].Id =  productprice.[ServiceId]      
  WHERE   [Service].[IsDeleted]=1)  
      
 Order by R.Id desc      
 OFFSET (@next*@offset)-@next ROWS      
    FETCH NEXT @next ROWS ONLY      
      
END      
      
      
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : ProductLanguageMappingSelect      
      
/***** Object:  StoredProcedure [dbo].[ProductLanguageMappingSelect] ***/      
      
    
  --select * from [Product]
GO
/****** Object:  StoredProcedure [dbo].[ProductSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProductSelectApp]
(
	@Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL,
	@Gender     NVARCHAR(30)=NULL,
	@ServiceId  BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

    SELECT
	R.[Id],
	R.[Name],
	R.[Gender],
	ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),

	(SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  R.Id AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductName,
	(SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ProductId =  R.Id AND PP.IsDeleted = 0 AND PP.ServiceId =@ServiceId ) AS Price

	FROM [Product] R

	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		@Gender  IS NULL
			OR
		R.[Gender] =  @Gender
	)
	AND
	(
		R.IsActive = 1
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END

GO
/****** Object:  StoredProcedure [dbo].[ProductUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductUpdate]  
     @Id    BIGINT=NULL,  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @UpdatedBy    BIGINT=NULL,  
     @IsDeleted    BIT=NULL,  
     @IsActive    BIT=NULL,  
   @Gender NVARCHAR(MAX)=NULL,  
   @Name   NVARCHAR(100)=NULL,
   @Rank Bigint=null
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    UPDATE [Product]  
 SET  
     [Slug] = ISNULL(@Slug,[Slug]),  
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),  
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),   
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),  
     [IsActive] = ISNULL(@IsActive,[IsActive]),  
     [Gender] = ISNULL(@Gender,[Gender]),  
    [Name] =  ISNULL(@Name,[Name]),
	[Rank]=Isnull(@Rank,[Rank])
  WHERE  
  (  
   [Id]=@Id  
  )  
  AND  
  (  
  [TenantId] =  @TenantId  
  )  
END  
  
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : ProductXMLSave
GO
/****** Object:  StoredProcedure [dbo].[ProductXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductXMLSave]
 @ProductXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Gender)[1]', 'NVARCHAR') AS 'Gender'
  FROM 
	@ProductXml.nodes('/root/Product') AS NDS(DT)
   )
   MERGE INTO Product R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Gender] =ISNULL(x.[Gender] ,R.[Gender])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Gender]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Gender]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[ProductLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PromotionInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PromotionInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,

		    @Title  NVARCHAR(MAX)=NULL,
			@url NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Promotion]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Title],[url]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Title,@url
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[PromotionSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PromotionSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.Title,
	 		R.[url]
			
	 	,
	 		FileXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='PROMOTION' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	 		
	 	
	overall_count = COUNT(*) OVER()
	FROM [Promotion] R  
	WHERE 
	
	
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[PromotionUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PromotionUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,

			 @Title  NVARCHAR(MAX)=NULL,
			 @url NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Promotion]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	
				[Title]=isnull(@Title,[Title]),
			 	[url] = ISNULL(@url,[url])
			 	


	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[ProvinceInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Province]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceUpdate

/***** Object:  StoredProcedure  [dbo].[ProvinceUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProvinceLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[ProvinceLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [ProvinceLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : UserCordinateSelect

/***** Object:  StoredProcedure [dbo].[UserCordinateSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [ProvinceLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceLanguageMappingXMLSave]
 @ProvinceLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(ProvinceId)[1]', 'BIGINT') AS 'ProvinceId',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name'
		
  FROM 
  
	@ProvinceLanguageMappingXml.nodes('/ArrayOfProvinceLanguageMappingModel/ProvinceLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO ProvinceLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,
	R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[ProvinceId] =  ISNULL(x.[ProvinceId], R.ProvinceId),
	R.[LanguageId]  =  ISNULL(x.LanguageId,R.LanguageId),
	R.[Name]     =  ISNULL(x.[Name],R.[Name])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		[IsActive],
		[ProvinceId],
		[LanguageId],
		[Name]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[ProvinceId],x.[LanguageId],x.[Name]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : UserCordinateInsert

/***** Object:  StoredProcedure [dbo].[UserCordinateInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ProvinceSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	overall_count = COUNT(*) OVER(),
	ProvinceLanguageXml = (
		select
		provincelanguagemapping.Id,
		provincelanguagemapping.Name,
		provincelanguagemapping.LanguageId,
		provincelanguagemapping.ProvinceId,
		[language].Name AS LanguageName

		FROM 
		ProvinceLanguageMapping provincelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = provincelanguagemapping.LanguageId
		WHERE provincelanguagemapping.ProvinceId =  R.Id AND provincelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Province] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[ProvinceSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 	R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM ProvinceLanguageMapping PL WHERE PL.ProvinceId =  R.Id AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProvinceName
	FROM [Province] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[ProvinceUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Province]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name])
			 	
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceXMLSave


GO
/****** Object:  StoredProcedure [dbo].[ProvinceXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceXMLSave]
 @ProvinceXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name'
  FROM 
	@ProvinceXml.nodes('/root/Province') AS NDS(DT)
   )
   MERGE INTO Province R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		[IsActive],
		[Name]
		
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[PushNotificationInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationInsert](

	  	@CreatedBy		BIGINT=NULL,
		@Title			NVARCHAR(150)=NULL,
		@Message		NVARCHAR(250)=NULL,
		@Type			NVARCHAR(100)=NULL,
		@TargetId		BIGINT=NULL,
		@TargetType		NVARCHAR(50),
		@UserId			BIGINT=NULL,
		@UserRole		NVARCHAR(50)
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO PushNotification
	  (
	   CreatedBy,
	   UpdatedBy,
	   Title,
	   [Message],
	   [Type], 
	   TargetId, 
	   TargetType, 
	   UserId, 
	   UserRole
	  )
	  VALUES
	  ( 
	   @CreatedBy,
	   @CreatedBy,
	   @Title,
	   @Message,
	   @Type, 
	   @TargetId, 
	   @TargetType, 
	   @UserId, 
	   @UserRole
	  )
	SELECT SCOPE_IDENTITY()
 END
GO
/****** Object:  StoredProcedure [dbo].[PushNotificationLogRead]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationLogRead]
 @Id BIGINT = NULL,
 @Type NVARCHAR(50)= NULL,
 @UserId BIGINT = NULL 

AS
	
 UPDATE [dbo].[PushNotification]
 SET
 IsSeen = 1,
 UpdatedOn = SWITCHOFFSET(SYSDATETIMEOFFSET(),'+00:00')
 WHERE
 (
  @Type IS NULL
  OR
  [Type] = @Type
 )
 AND
 (
  @UserId IS NULL
  OR
  UserId = @UserId
 )
 AND
 (
  Id <= @id
 )

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[PushNotificationRead]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationRead]
	  	@Ids			NVARCHAR(250)=NULL,
	  	@UpdatedBy		BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE PushNotification
	SET
	 UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy),	 		
	 UpdatedOn = switchoffset(sysdatetimeoffset(),'+00:00'),
	 IsSeen=1
	 WHERE
	 (
	  Id IN (SELECT number FROM ListToTable(@Ids))
	 )
END
GO
/****** Object:  StoredProcedure [dbo].[PushNotificationSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationSelect](
@Id				BIGINT	=NULL,
@IsActive		bit		=NULL,
@next			int		=NULL,
@offset			int		=NULL, 
@UserRole		NVARCHAR(20)=NULL,
@UserId			BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =1000
SET @offset=1
END

 SELECT
 	R.Id,
	R.CreatedBy,
	R.UpdatedBy,
	R.CreatedOn,
	R.UpdatedOn,
	R.IsDeleted,
	R.IsActive,
	R.Title,
	R.[Message],
	R.[Type],
	R.IsArchive,
	R.TargetId,
	R.TargetType,
	R.UserId,
	R.UserRole,
	R.IsSeen,
	R.ArchivedBy,
	R.ArchiveDate,
	overall_count = COUNT(*) OVER(),
    UnreadCount=
				(
					SELECT COUNT(1) FROM [PushNotification] L 
					WHERE 
						(
							@Id IS NULL
							OR
							L.Id = @Id
						)
						AND
						(
							@IsActive IS NULL
								OR
							L.IsActive = @IsActive
						)
						AND
						(
							@UserRole IS NULL
							OR
							L.UserRole=@UserRole
						)
						AND
						(
							@UserId IS NULL
							OR
							L.UserId=@UserId
						)
						AND
						L.IsDeleted=0
						AND
						L.IsSeen = 0
						)

	FROM PushNotification R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		@UserRole IS NULL
		OR
		R.UserRole=@UserRole
	)
	AND
	(
		@UserId IS NULL
		OR
		R.UserId=@UserId
	)
	AND

	R.IsDeleted=0

	Order by Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[PushNotificationSelectForOtherActiveUser]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationSelectForOtherActiveUser] 
	@UserId BIGINT = NULL,
	@DeviceId NVARCHAR(100) = NULL
AS

	SELECT (U.FirstName +' '+ U.LastName )as FullName,
	--TS.IsGroupChat,TS.IsGroupDiscussion,
	Devices=STUFF(  
             (SELECT ',' + D.DeviceToken
              FROM UserDevice D
			  WHERE D.UserId = U.Id AND D.IsDeleted = 0 AND D.DeviceId <> @DeviceId
              FOR XML PATH (''))
             , 1, 1, '')
	 FROM Users U 
	--LEFT JOIN CustomerNotification TS ON TS.CustomerId = U.Id
	WHERE U.Id = @UserId AND U.IsDeleted = 0

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[PushNotificationSelectForUser]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PushNotificationSelectForUser] 
	@UserId BIGINT
AS

	SELECT (U.FirstName +' '+ U.LastName )as FullName,
	--TS.IsGroupChat,TS.IsGroupDiscussion,
	Devices=STUFF(
             (SELECT ',' + D.DeviceToken
              FROM UserDevice D
			  WHERE D.UserId = U.Id AND D.IsDeleted = 0
              FOR XML PATH (''))
             , 1, 1, '')
	 FROM Users U 
	--LEFT JOIN CustomerNotification TS ON TS.CustomerId = U.Id
	WHERE U.Id = @UserId AND U.IsDeleted = 0

RETURN 0
GO
/****** Object:  StoredProcedure [dbo].[QrLogInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[QrLogInsert](      
      
   @TenantId   INT=NULL,      
   @Slug NVARCHAR(MAX)=NULL,      
   @CreatedBy    BIGINT=NULL,      
   @IsActive    BIT=NULL,      
   @UserId    BIGINT=NULL,      
   @QRcode NVARCHAR(MAX)=NULL,      
   @Amount    DECIMAL(18,2)=NULL,      
   @OrderId    BIGINT=NULL,      
   @TransactionId NVARCHAR(MAX)=NULL,      
   @Status NVARCHAR(MAX)=NULL,  
   @result nvarchar(max)=null,  
   @authKey nvarchar(max)=null,
   @txnCurrencyCode nvarchar(max)=null,
   @loyaltyId nvarchar(max)=null,
   @txnNo nvarchar(max)=null,
   @additionalInfo  nvarchar(max)=null



)      
AS       
BEGIN      
SET NOCOUNT ON;      
--declare @result decimal;    
   INSERT INTO [QrLog]      
   (      
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[QRCode],[Amount],[OrderId],[TransactionId],[Status],[Result],[AuthKey] 
	,[txnCurrencyCode],[loyaltyId],[txnNo],[additionalInfo]
   )      
   VALUES      
   (       
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@QRcode,@Amount,@OrderId,@TransactionId,@Status,@result,@Authkey
	,  @txnCurrencyCode,@loyaltyId,@txnNo,@additionalInfo
   )      
     
    
    
 SELECT @result=SCOPE_IDENTITY()       
     
   declare @note varchar(max)=cast( 'Order id='+cast(@OrderId as varchar(100))+' QRlog  id='+cast(@result as varchar(100)) as varchar(max));    
         
  INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )      
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@note)    
    
    select cast (@result as decimal)     
 END      
       
       
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : PaymentLogUpdate      
      
/***** Object:  StoredProcedure  [dbo].[PaymentLogUpdate] *****/      
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[RatingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RatingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			

			@orderId bigint=null
	 	,
	 		@Quality bigint =null
	 	,
	 		@DeliveryService bigint =null
			,
	 	    @Staff bigint =null,
	 		@Packaging  bigint =null
			,
			@OverallSatisfaction  bigint =null,
	 		@Feedback  nvarchar(max) =null
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [rating]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],orderid,quality,DeliveryService,Staff,Packaging,OverallSatisfaction,Feedback
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@orderId,@Quality,@DeliveryService,@Staff,@Packaging,@OverallSatisfaction,@Feedback
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyUpdate

/***** Object:  StoredProcedure  [dbo].[CompanyUpdate] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[RatingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RatingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]

			

	 	,
	 		R.[orderId]
	 	,
	 		R.[Quality]
	 	,
	 		R.[DeliveryService]
			,
	 		R.[Staff],
	 		R.[Packaging]
			,
			R.[OverallSatisfaction],
	 		R.[Feedback]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Rating] R  
	WHERE 
	(
		@RelationTable IS NULL
		or
		(		
		@RelationTable = 'Order'
		and
		R.orderid =@RelationId
		)	
		or
		(		
		@RelationTable = 'Users'
		and
		R.createdby =	@RelationId
		)	
			
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyOwnerSelect

/***** Object:  StoredProcedure [dbo].[CompanyOwnerSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[RatingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RatingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@orderId bigint=null
	 	    ,
	 		@Quality bigint =null
	     	,
	 		@DeliveryService bigint =null
			,
	 	    @Staff bigint =null,
	 		@Packaging  bigint =null
			,
			@OverallSatisfaction  bigint =null,
	 		@Feedback  nvarchar(max) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [rating]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	
				[orderid] = ISNULL(@orderId,orderid),
				[quality] = ISNULL(@Quality,quality),
				[DeliveryService] = ISNULL(@DeliveryService,DeliveryService),
				[Staff] = ISNULL(@Staff,Staff),
				[Packaging] = ISNULL(@Packaging,Packaging),
				[OverallSatisfaction] = ISNULL(@OverallSatisfaction,OverallSatisfaction),

				[Feedback] = ISNULL(@Feedback,Feedback)


	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyXMLSave
GO
/****** Object:  StoredProcedure [dbo].[RolesSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RolesSelect]                 
--declare       
@Id          BIGINT=null,                
@Role        NVARCHAR(20)=NULL,                
@next    INT = NULL,                
@offset   INT = NULL                
                
AS      
BEGIN                
                
IF @next IS NULL                
BEGIN                
SET @next =100000                
SET @offset=1                
END                
                
          
          
SELECT                 
      Id ,[Name],
	  case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'         
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'       
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'       
   end as Role  
	            
 from Roles  R             
                
WHERE                 
 (                 
 @Id IS NULL     
 OR                 
 R.Id=@Id                 
 )           
         
        
                
                  
                
               
                
   ORDER BY R.Id DESC                 
   OFFSET (@next*@offset)-@next ROWS                
   FETCH NEXT @next ROWS ONLY                
                
END   
GO
/****** Object:  StoredProcedure [dbo].[SelectUserProfileByUniqueCode]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectUserProfileByUniqueCode]
	@UniqueCode NVARCHAR(65)=NULL
AS
BEGIN
	
	SELECT 
	U.Email,
	U.Id,
	U.IsActive,
	U.PhoneNumber,
	U.UserName,
	U.firstName,
	U.lastName,
	U.ProfilePic,
	U.UniqueCode
	FROM Users U
	WHERE 
	U.IsDeleted=0
	AND
	(
		@UniqueCode IS NULL
		OR
		U.UniqueCode=@UniqueCode
	)
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ServiceInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@Description NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Service]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[Description]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@Description
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceUpdate

/***** Object:  StoredProcedure  [dbo].[ServiceUpdate] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ServiceLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ServiceLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[ServiceId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@ServiceId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[ServiceLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[ServiceLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[ServiceId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [ServiceLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductSelect

/***** Object:  StoredProcedure [dbo].[ProductSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[ServiceLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [ServiceLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[ServiceLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceLanguageMappingXMLSave]
 @ServiceLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(max)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Description)[1]', 'NVARCHAR(MAX)') AS 'Description'

		
  FROM 
	@ServiceLanguageMappingXml.nodes('/ArrayOfServiceLanguageMappingModel/ServiceLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO ServiceLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[Name] =ISNULL(x.[Name] ,R.[Name]),
	R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),
	R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),
	R.[Description] =  ISNULL(x.[Description],R.[Description])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[ServiceId],
		[Description]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[ServiceId],x.[Description]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductInsert

/***** Object:  StoredProcedure [dbo].[ProductInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[ServiceProductPriceSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceProductPriceSelect]  
--declare
  
 @LanguageId BIGINT=NULL,  
 @next   int = NULL,  
 @offset  int = NULL,  
 @TenantId   INT=NULL,  
 @Gender     NVARCHAR(30)=NULL,  
 @ServiceId  BIGINT=null  ,
 @productUd bigint=null 
AS
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =100000  
SET @offset=1  
END  
  
    SELECT  
 R.ProductId,  
 R.Price,  
 P.Name,  
 P.Gender,  
 ImageXml=(  
     SELECT   
      FG.[Id],  
      FG.[CreatedBy],  
      FG.[UpdatedBy],  
      FG.[CreatedOn],  
      FG.[UpdatedOn],  
      FG.[IsDeleted],  
      FG.[IsActive],  
      FG.[Filename],  
      FG.[MimeType],  
      FG.[Thumbnail],  
      FG.[Size],  
      FG.[Path],  
      FG.[OriginalName],  
      FG.[OnServer],  
      FG.[TypeId],  
      FG.[Type]  
      FROM FileGroupItems FG  
      WHERE FG.[TypeId] = R.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0  
      FOR XML AUTO,ROOT,ELEMENTs  
    ),  
  
 (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  R.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductName  
  
 FROM [ProductPrice] R INNER JOIN [Product] P ON   
 P.Id = R.ProductId AND P.IsDeleted = 0  AND P.IsActive = 1 AND (@Gender IS NULL OR P.Gender =  @Gender)   
 INNER JOIN [Service] S ON S.Id =  R.ServiceId AND S.IsDeleted = 0  
  
 WHERE  
 (  
  @ServiceId IS NULL  
  OR  
  R.ServiceId = @ServiceId  
 )  
  and
  (  
  @productUd IS NULL  
  OR  
  R.ProductId = @productUd  
 ) 
 AND  
 (  
  R.[IsDeleted] = 0  
 )  
 AND  
 (  
  @TenantId IS NULL  
  OR  
  R.[TenantId] = @TenantId  
 )  
   
 Order by R.Id desc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END
GO
/****** Object:  StoredProcedure [dbo].[ServiceProductSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceProductSelectApp]  (    
--declare    
      
 @LanguageId BIGINT=NULL,      
 @next   int = NULL,      
 @offset  int = NULL,      
 @TenantId   INT=NULL,      
 @Gender     NVARCHAR(30)=NULL,      
 @ServiceId  BIGINT=NULL      
)  AS    
BEGIN      
      
IF @next IS NULL      
BEGIN      
SET @next =100000      
SET @offset=1      
END      
      
    SELECT      
 R.ProductId,      
 R.Price,      
 P.Name,      
 P.Gender,      
 ImageXml=(      
     SELECT       
      FG.[Id],      
      FG.[CreatedBy],      
      FG.[UpdatedBy],      
      FG.[CreatedOn],      
      FG.[UpdatedOn],      
      FG.[IsDeleted],      
      FG.[IsActive],      
      FG.[Filename],      
      FG.[MimeType],      
      FG.[Thumbnail],      
      FG.[Size],      
      FG.[Path],      
      FG.[OriginalName],      
      FG.[OnServer],      
      FG.[TypeId],      
      FG.[Type]      
      FROM FileGroupItems FG      
      WHERE FG.[TypeId] = R.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0      
      FOR XML AUTO,ROOT,ELEMENTs      
    ),      
      
 (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  R.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductName      
      
 FROM [ProductPrice] R INNER JOIN [Product] P ON       
 P.Id = R.ProductId AND P.IsDeleted = 0  AND P.IsActive = 1 AND (@Gender IS NULL OR P.Gender =  @Gender)       
 INNER JOIN [Service] S ON S.Id =  R.ServiceId AND S.IsDeleted = 0      
      
 WHERE      
 (      
  @ServiceId IS NULL      
  OR      
  R.ServiceId = @ServiceId      
 )      
       
 AND      
 (      
  R.[IsDeleted] = 0      
 )      
 AND      
 (      
  @TenantId IS NULL      
  OR      
  R.[TenantId] = @TenantId      
 )   
 -- added on 17122020 to remove those item without service
 and     
 P.id not in (select productid from ProductPrice productprice INNER JOIN         
  [Service] [service] ON [service].Id =  productprice.[ServiceId]        
  WHERE   [Service].[IsDeleted]=1)    

   Order by P.[Rank] asc     
 --Order by R.Id desc      
 OFFSET (@next*@offset)-@next ROWS      
    FETCH NEXT @next ROWS ONLY      
      
END      
      
  
  
 --select * from product  
GO
/****** Object:  StoredProcedure [dbo].[ServiceSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive],
			R.[Name]
	 	,
		R.[Description],
	overall_count = COUNT(*) OVER(),
	 ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='SERVICE_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),
	ServiceLanguageXml = (
		select
		servicelanguagemapping.[Id],
		servicelanguagemapping.[Name],
		servicelanguagemapping.[LanguageId],
		servicelanguagemapping.[ServiceId],
		servicelanguagemapping.[Description],
		[language].Name AS LanguageName

		FROM 
		ServiceLanguageMapping servicelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = servicelanguagemapping.LanguageId
		WHERE servicelanguagemapping.ServiceId =  R.Id AND servicelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Service] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ServiceLanguageMappingSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[ServiceSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceSelectApp]
(
	@Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

    SELECT
	R.[Id],
	R.[Name],
	R.[Description],
	 ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='SERVICE_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),
	(SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  R.Id AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceName,
	(SELECT TOP(1) [Description] FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  R.Id AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS [ServiceDescription]

	FROM [Service] R

	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.IsActive = 1
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
GO
/****** Object:  StoredProcedure [dbo].[ServiceUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@Description NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Service]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
				[Name]     =  ISNULL(@Name,[Name]),
				[Description] =  ISNULL(@Description,[Description])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceXMLSave
GO
/****** Object:  StoredProcedure [dbo].[ServiceXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceXMLSave]
 @ServiceXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Description)[1]', 'NVARCHAR(MAX)') AS 'Description'

		
  FROM 
	@ServiceXml.nodes('/root/Service') AS NDS(DT)
   )
   MERGE INTO Service R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[Description] =ISNULL(x.[Description] ,R.[Description])

	
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Description]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Description]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[ServiceLanguageMappingInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[SetUserIdle]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>  select * from users   
-- =============================================    
create PROCEDURE [dbo].[SetUserIdle]    
 -- Add the parameters for the stored procedure here    
 @FromUserId BIGINT = NULL,    
 @ToUserId BIGINT = NULL    
AS    
  
  
  
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  update users set callStatus='IDLE' 
    
END
GO
/****** Object:  StoredProcedure [dbo].[StateInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CountryId    BIGINT=NULL,
			@Name        NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [State]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CountryId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CountryId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : StateUpdate

/***** Object:  StoredProcedure  [dbo].[StateUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[StateLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@StateId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [StateLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[StateId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@StateId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[StateLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[StateLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[StateId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [StateLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CitySelect

/***** Object:  StoredProcedure [dbo].[CitySelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[StateLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@StateId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [StateLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[StateId] = ISNULL(@StateId,[StateId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[StateLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateLanguageMappingXMLSave]
 @StateLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(StateId)[1]', 'BIGINT') AS 'StateId'
  FROM 
  
	@StateLanguageMappingXml.nodes('/ArrayOfStateLanguageMappingModel/StateLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO StateLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[StateId] =ISNULL(x.[StateId] ,R.[StateId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[StateId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[StateId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CityInsert

/***** Object:  StoredProcedure [dbo].[CityInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[StateSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[CountryId]
	 	,
		R.Name,
		C.Name As CountryName,
	overall_count = COUNT(*) OVER(),
	StateLanguageXml = (
		select
		statelanguagemapping.Id,
		statelanguagemapping.Name,
		statelanguagemapping.LanguageId,
		statelanguagemapping.StateId,
		[language].Name AS LanguageName

		FROM 
		StateLanguageMapping statelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = statelanguagemapping.LanguageId
		WHERE statelanguagemapping.StateId =  R.Id AND statelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [State] R  
	INNER JOIN Country C ON C.[Id] = R.[CountryId] AND C.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
		OR
		(
			@RelationTable ='Country'
			AND
			R.[CountryId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[StateLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[StateUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CountryId    BIGINT=NULL,
			@Name        NVARCHAR(100)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [State]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CountryId] = ISNULL(@CountryId,[CountryId]),
				[Name]      =  ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : StateXMLSave


GO
/****** Object:  StoredProcedure [dbo].[StateXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateXMLSave]
 @StateXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CountryId)[1]', 'BIGINT') AS 'CountryId'
  FROM 
	@StateXml.nodes('/root/State') AS NDS(DT)
   )
   MERGE INTO State R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[CountryId] =ISNULL(x.[CountryId] ,R.[CountryId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[CountryId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[CountryId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[StateLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DistrictId    BIGINT=NULL,
			@Name          NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [SubDistrict]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[DistrictId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@DistrictId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictUpdate

/***** Object:  StoredProcedure  [dbo].[SubDistrictUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@SubDistrictId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [SubDistrictLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[SubDistrictId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@SubDistrictId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[SubDistrictLanguageMappingUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	 		R.[LanguageId]
	 	,
	 		R.[SubDistrictId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [SubDistrictLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceSelect

/***** Object:  StoredProcedure [dbo].[ProvinceSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@SubDistrictId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [SubDistrictLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[SubDistrictId] = ISNULL(@SubDistrictId,[SubDistrictId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingXMLSave


GO
/****** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictLanguageMappingXMLSave]
 @SubDistrictLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(SubDistrictId)[1]', 'BIGINT') AS 'SubDistrictId'
  FROM 
  
	@SubDistrictLanguageMappingXml.nodes('/ArrayOfSubDistrictLanguageMappingModel/SubDistrictLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO SubDistrictLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[Name] =ISNULL(x.[Name] ,R.[Name]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[SubDistrictId] =ISNULL(x.[SubDistrictId] ,R.[SubDistrictId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[SubDistrictId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[SubDistrictId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceInsert

/***** Object:  StoredProcedure [dbo].[ProvinceInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[DistrictId]
	 	,
		R.[Name],
	overall_count = COUNT(*) OVER(),
	D.[Name] AS DistrictName,
	SubDistrictLanguageXml = (
		select
		subdistrictlanguagemapping.Id,
		subdistrictlanguagemapping.Name,
		subdistrictlanguagemapping.LanguageId,
		subdistrictlanguagemapping.SubDistrictId,
		[language].Name AS LanguageName

		FROM 
		SubDistrictLanguageMapping subdistrictlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = subdistrictlanguagemapping.LanguageId
		WHERE subdistrictlanguagemapping.SubDistrictId =  R.Id AND subdistrictlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [SubDistrict] R  
	INNER JOIN [District] D ON D.[Id] = R.[DistrictId] AND D.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
			OR

		(
			@RelationTable = 'District'
				AND
			R.[DistrictId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[SubDistrictSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL,
	@DistrictId  BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.Id AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId ) AS SubDistrictName
	
	FROM [SubDistrict] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		@DistrictId IS NULL
			OR
		R.[DistrictId] =  @DistrictId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[SubDistrictUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DistrictId    BIGINT=NULL,
			@Name          NVARCHAR(100)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [SubDistrict]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[DistrictId] = ISNULL(@DistrictId,[DistrictId]),
				[Name]       = ISNULL(@Name,[Name])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictXMLSave


GO
/****** Object:  StoredProcedure [dbo].[SubDistrictXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictXMLSave]
 @SubDistrictXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(DistrictId)[1]', 'BIGINT') AS 'DistrictId'
  FROM 
	@SubDistrictXml.nodes('/root/SubDistrict') AS NDS(DT)
   )
   MERGE INTO SubDistrict R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[DistrictId] =ISNULL(x.[DistrictId] ,R.[DistrictId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[DistrictId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[DistrictId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[TenantSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TenantSelect]
@UniqueId UNIQUEIDENTIFIER = NULL
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
Id,
Name,
[Address],
PhoneNo,
Email,
UniqueId
FROM 
Tenant 
WHERE 
(
@UniqueId IS NULL
OR
UniqueId=@UniqueId
)
AND
IsDeleted=0
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UpdateUserProfile]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUserProfile](
	@UserId          BIGINT = NULL,
	@FirstName       NVARCHAR(100) = NULL,
	@LastName        NVARCHAR(100) = NULL,
	@PhoneNumber     NVARCHAR(30) = NULL,
	@IsActive        BIT = NULL,
	@ProfileImageUrl	NVARCHAR(MAX)=NULL
)
AS
BEGIN
	UPDATE Users SET
	firstName       =     ISNULL(@FirstName,firstName),
	lastName        =     ISNULL(@LastName,lastName),
	PhoneNumber     =     ISNULL(@PhoneNumber,PhoneNumber),
	IsActive       =      ISNULL(@IsActive,IsActive),
	ProfilePic		=		ISNULL(@ProfileImageUrl, ProfilePic) 
WHERE

Id = @UserId

END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UploadedFileInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UploadedFileInsert]
(
@FileName NVARCHAR(250),
@FileUrl NVARCHAR(max)

)
AS BEGIN
SET NOCOUNT ON;
INSERT INTO UploadedFile

(
	FileName,
	FileUrl,
	IsDeleted,
	CreatedOn

)
VALUES
(
	@FileName,
	@FileUrl,
	0,
	switchoffset(sysdatetimeoffset(),'+00:00')

)

SELECT @@IDENTITY
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackInsert

/***** Object:  StoredProcedure [dbo].[FeedbackInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UserAddressInActivePostalCode]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressInActivePostalCode](        
--declare    
@Id         BIGINT =NULL,        
@next   int = NULL,        
@offset  int = NULL,        
@LanguageId BIGINT=NULL,        
@TenantId   INT=NULL,        
@Type       NVARCHAR(100)=NULL,           
@UserId     BIGINT=NULL         
)    AS     
BEGIN        
        
IF @next IS NULL        
BEGIN        
SET @next =100000        
SET @offset=1        
END        
        
 SELECT        
    R.[Id],        
    R.[AddressLine1],        
   R.[AddressLine2],        
    R.[DistrictId],        
    R.[SubDistrictId],        
    R.[PostalCode],        
    R.[Tag],        
    R.[Latitude],        
    R.[Longitude],        
   R.[HouseNumber],        
   R.[StreetNumber],        
   R.[Note],        
   R.[Type],        
        
      
   R.[BuildingName] ,      
   R.[Floor] ,      
   R.[UnitNo] ,      
   R.[PhoneNo] ,      
   R.alterNumber ,      
   R.[ResidenceType] ,      
      
         overall_count = COUNT(*) OVER(),        
   D.Name  AS DistrictName,        
   SD.Name  AS SubDistrictName,        
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,        
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId ) AS SubDistrictNameL        
        
        
 FROM [UserAddress] R INNER JOIN        
 [District] D ON D.Id =  R.[DistrictId]        
 INNER JOIN         
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId        
          
 WHERE         
 (        
  @Id IS NULL        
  OR        
  R.[Id] = @Id        
 )        
 AND        
 (        
  R.[IsDeleted] = 0        
 )        
 AND        
 (        
  @TenantId IS NULL        
  OR        
  R.[TenantId] = @TenantId        
 )        
 AND        
 (        
  @Type IS NULL        
   OR        
  R.[Type] =  @Type        
 )        
 And     
 (    
 @UserId is null    
 or     
 R.UserId=@UserId    
 and  
 r.PostalCode in (select PostalCode from postalcode where isactive=1 and isdeleted=0)  
 )    
        
 Order by R.Id desc        
 OFFSET (@next*@offset)-@next ROWS        
    FETCH NEXT @next ROWS ONLY        
        
END        
        
        
        
---------------------------------------------------        
---------------------------------------------------        
-- Procedure : DistrictSelect        
        
/***** Object:  StoredProcedure [dbo].[DistrictSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[UserAddressInsertApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressInsertApp](      
      
   @TenantId   INT=NULL,      
   @Slug NVARCHAR(MAX)=NULL,      
     @CreatedBy    BIGINT=NULL,      
     @IsActive    BIT=NULL,      
     @UserId    BIGINT=NULL,      
   @AddressLine1 NVARCHAR(MAX)=NULL,      
   @AddressLine2 NVARCHAR(MAX)=NULL,      
     @DistrictId    BIGINT=NULL,      
     @SubDistrictId    BIGINT=NULL,      
   @PostalCode NVARCHAR(MAX)=NULL,      
     @IsDefault    BIT=NULL,      
   @Tag NVARCHAR(MAX)=NULL,      
     @ProvinceId    BIGINT=NULL,      
   @Latitude    DECIMAL(18,6)=NULL,      
   @Longitude    DECIMAL(18,6)=NULL,      
   @Type        NVARCHAR(100)=NULL,      
   @HouseNumber NVARCHAR(100)=NULL,      
   @StreetNumber NVARCHAR(100)=NULL,      
   @Note         NVARCHAR(MAX)=NULL  ,    
    
   @BuildingName NVARCHAR(100)=NULL,     
   @Floor NVARCHAR(100)=NULL,     
   @UnitNo NVARCHAR(100)=NULL,     
   @PhoneNo NVARCHAR(100)=NULL,     
   @alterNumber NVARCHAR(100)=NULL,    
   @ResidenceType   NVARCHAR(100)=NULL    
    
)      
AS       
BEGIN      
SET NOCOUNT ON;      
   INSERT INTO [UserAddress]      
   (      
    [TenantId],      
    [Slug],      
    [CreatedBy],      
    [UpdatedBy],      
    [IsActive],      
    [UserId],      
    [AddressLine1],      
    [AddressLine2],      
    [DistrictId],      
    [SubDistrictId],      
    [PostalCode],      
    [IsDefault],      
    [Tag],      
    [ProvinceId],      
    [Latitude],      
    [Longitude],      
    [Type],      
    [HouseNumber],      
    [StreetNumber],      
    [Note]  ,    
    [BuildingName] ,     
   [Floor] ,    
   [UnitNo] ,    
   [PhoneNo] ,    
   alterNumber ,    
   [ResidenceType]       
   )      
   VALUES      
   (       
    @TenantId,      
    @Slug,      
    @CreatedBy,      
    @CreatedBy,      
    @IsActive,      
    @UserId,      
    @AddressLine1,      
    @AddressLine2,      
    @DistrictId,      
    @SubDistrictId,      
    @PostalCode,      
    @IsDefault,      
    @Tag,      
    @ProvinceId,      
    @Latitude,      
    @Longitude,      
    @Type,      
    @HouseNumber,      
    @StreetNumber,      
    @Note  ,    
    
    
 @BuildingName ,     
   @Floor ,    
   @UnitNo ,    
   @PhoneNo ,    
   @alterNumber ,    
   @ResidenceType       
   )      
      
 SELECT SCOPE_IDENTITY()      
 END      
       
       
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : UserAddressUpdate      
      
/***** Object:  StoredProcedure  [dbo].[UserAddressUpdate] *****/      
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[UserAddressSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[AddressLine1]
	 	,
	 		R.[AddressLine2]
	 	,
	 		R.[DistrictId]
	 	,
	 		R.[SubDistrictId]
	 	,
	 		R.[PostalCode]
	 	,
	 		R.[IsDefault]
	 	,
	 		R.[Tag]
	 	,
	 		R.[ProvinceId]
	 	,
	 		R.[Latitude]
	 	,
	 		R.[Longitude]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [UserAddress] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictSelect

/***** Object:  StoredProcedure [dbo].[DistrictSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[UserAddressSelectApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressSelectApp](          
--declare      
@Id         BIGINT =NULL,          
@next   int = NULL,          
@offset  int = NULL,          
@LanguageId BIGINT=NULL,          
@TenantId   INT=NULL,          
@Type       NVARCHAR(100)=NULL,             
@UserId     BIGINT=NULL           
)    AS       
BEGIN          
          
IF @next IS NULL          
BEGIN          
SET @next =100000          
SET @offset=1          
END          
          
 SELECT          
    R.[Id],          
    R.[AddressLine1],          
   R.[AddressLine2],          
    R.[DistrictId],          
    R.[SubDistrictId],          
    R.[PostalCode],          
    R.[Tag],          
    R.[Latitude],          
    R.[Longitude],          
   R.[HouseNumber],          
   R.[StreetNumber],          
   R.[Note],          
   R.[Type],          
        R.ProvinceId,  
   R.[BuildingName] ,        
   R.[Floor] ,        
   R.[UnitNo] ,        
   R.[PhoneNo] ,        
   R.alterNumber ,        
   R.[ResidenceType] ,        
        
         overall_count = COUNT(*) OVER(),          
   D.Name  AS DistrictName,          
   SD.Name  AS SubDistrictName,      
   PD.Name As ProvinceName,  
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,          
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId ) AS SubDistrictNameL   ,       
   (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL          
         
     
          
 FROM [UserAddress] R INNER JOIN          
 [District] D ON D.Id =  R.[DistrictId]          
 INNER JOIN           
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId          
 INNER JOIN           
 Province PD ON PD.Id =  R.ProvinceId      
            
 WHERE           
 (          
  @Id IS NULL          
  OR          
  R.[Id] = @Id          
 )          
 AND          
 (          
  R.[IsDeleted] = 0          
 )          
 AND          
 (          
  @TenantId IS NULL          
  OR          
  R.[TenantId] = @TenantId          
 )          
 AND          
 (          
  @Type IS NULL          
   OR          
  R.[Type] =  @Type          
 )          
 And       
 (      
 @UserId is null      
 or       
 R.UserId=@UserId      
       
 )      
          
 Order by R.Id desc          
 OFFSET (@next*@offset)-@next ROWS          
    FETCH NEXT @next ROWS ONLY          
          
END          
          
          
          
---------------------------------------------------          
---------------------------------------------------          
-- Procedure : DistrictSelect          
          
/***** Object:  StoredProcedure [dbo].[DistrictSelect] ***/
GO
/****** Object:  StoredProcedure [dbo].[UserAddressUpdateApp]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressUpdateApp]      
   @Id    BIGINT=NULL,      
   @TenantId   INT=NULL,      
   @Slug NVARCHAR(MAX)=NULL,      
    @UpdatedBy    BIGINT=NULL,      
   @IsDeleted    BIT=NULL,      
   @IsActive    BIT=NULL,      
   @UserId    BIGINT=NULL,      
   @AddressLine1 NVARCHAR(MAX)=NULL,      
   @AddressLine2 NVARCHAR(MAX)=NULL,      
    @DistrictId    BIGINT=NULL,      
    @SubDistrictId    BIGINT=NULL,      
    @PostalCode NVARCHAR(MAX)=NULL,      
    @IsDefault    BIT=NULL,      
    @Tag NVARCHAR(MAX)=NULL,      
     @ProvinceId    BIGINT=NULL,      
   @Latitude    DECIMAL(18,6)=NULL,      
   @Longitude    DECIMAL(18,6)=NULL,      
   @Type        NVARCHAR(100)=NULL,      
   @HouseNumber NVARCHAR(100)=NULL,      
   @StreetNumber NVARCHAR(100)=NULL,      
   @Note         NVARCHAR(MAX)=NULL  ,    
    
    
   @BuildingName NVARCHAR(100)=NULL,     
   @Floor NVARCHAR(100)=NULL,     
   @UnitNo NVARCHAR(100)=NULL,     
   @PhoneNo NVARCHAR(100)=NULL,     
   @alterNumber NVARCHAR(100)=NULL,    
   @ResidenceType   NVARCHAR(100)=NULL    
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    UPDATE [UserAddress]      
 SET      
     [Slug] = ISNULL(@Slug,[Slug]),      
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),      
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),       
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),      
     [IsActive] = ISNULL(@IsActive,[IsActive]),      
     [UserId] = ISNULL(@UserId,[UserId]),      
     [AddressLine1] = ISNULL(@AddressLine1,[AddressLine1]),      
     [AddressLine2] = ISNULL(@AddressLine2,[AddressLine2]),      
     [DistrictId] = ISNULL(@DistrictId,[DistrictId]),      
     [SubDistrictId] = ISNULL(@SubDistrictId,[SubDistrictId]),      
     [PostalCode] = ISNULL(@PostalCode,[PostalCode]),      
     [IsDefault] = ISNULL(@IsDefault,[IsDefault]),      
     [Tag] = ISNULL(@Tag,[Tag]),      
     [ProvinceId] = ISNULL(@ProvinceId,[ProvinceId]),      
     [Latitude] = ISNULL(@Latitude,[Latitude]),      
     [Longitude] = ISNULL(@Longitude,[Longitude]),      
    [Type]      = ISNULL(@Type,[Type]),      
    [HouseNumber] =  ISNULL(@HouseNumber,[HouseNumber]),      
    [StreetNumber]  =  ISNULL(@StreetNumber,[StreetNumber]),      
    [Note]        =  ISNULL(@Note,[Note]) ,     
    
 [BuildingName] =  ISNULL(@BuildingName,BuildingName) ,     
   [Floor] =  ISNULL(@Floor,[Floor]) ,     
   [UnitNo] =  ISNULL(@UnitNo,[UnitNo]) ,     
   [PhoneNo] =  ISNULL(@PhoneNo,[PhoneNo]) ,     
   alterNumber =  ISNULL(@alterNumber,alterNumber) ,     
   [ResidenceType] =  ISNULL(@ResidenceType,[ResidenceType])       
    
 WHERE      
  (      
   [Id]=@Id      
  )      
  AND      
  (      
  @UserId IS NULL      
   OR      
  [UserId] =  @UserId      
  )      
  AND      
  (      
  [TenantId] =  @TenantId      
  )      
END      
      
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : UserAddressXMLSave
GO
/****** Object:  StoredProcedure [dbo].[UserAddressXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressXMLSave]
 @UserAddressXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(255)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(AddressLine1)[1]', 'NVARCHAR(MAX)') AS 'AddressLine1',
		NDS.DT.value('(AddressLine2)[1]', 'NVARCHAR(MAX)') AS 'AddressLine2',
		NDS.DT.value('(DistrictId)[1]', 'BIGINT') AS 'DistrictId',
		NDS.DT.value('(SubDistrictId)[1]', 'BIGINT') AS 'SubDistrictId',
		NDS.DT.value('(PostalCode)[1]', 'NVARCHAR(40)') AS 'PostalCode',
	  	NDS.DT.value('(IsDefault)[1]', 'BIT') AS 'IsDefault',
		NDS.DT.value('(Tag)[1]', 'NVARCHAR(40)') AS 'Tag',
		NDS.DT.value('(ProvinceId)[1]', 'BIGINT') AS 'ProvinceId',
		NDS.DT.value('(Latitude)[1]', 'DECIMAL(18,2)') AS 'Latitude',
		NDS.DT.value('(Longitude)[1]', 'DECIMAL(18,2)') AS 'Longitude'
  FROM 
	@UserAddressXml.nodes('/root/UserAddress') AS NDS(DT)
   )
   MERGE INTO UserAddress R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),
	R.[AddressLine1] =ISNULL(x.[AddressLine1] ,R.[AddressLine1]),
	R.[AddressLine2] =ISNULL(x.[AddressLine2] ,R.[AddressLine2]),
	R.[DistrictId] =ISNULL(x.[DistrictId] ,R.[DistrictId]),
	R.[SubDistrictId] =ISNULL(x.[SubDistrictId] ,R.[SubDistrictId]),
	R.[PostalCode] =ISNULL(x.[PostalCode] ,R.[PostalCode]),
	R.[IsDefault] =ISNULL(x.[IsDefault] ,R.[IsDefault]),
	R.[Tag] =ISNULL(x.[Tag] ,R.[Tag]),
	R.[ProvinceId] =ISNULL(x.[ProvinceId] ,
	R.[ProvinceId]),R.[Latitude] =ISNULL(x.[Latitude] ,
	R.[Latitude]),R.[Longitude] =ISNULL(x.[Longitude] ,R.[Longitude])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[AddressLine1],
		[AddressLine2],
		[DistrictId],
		[SubDistrictId],
		[PostalCode],
		[IsDefault],
		[Tag],
		[ProvinceId],
		[Latitude],
		[Longitude]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[AddressLine1],x.[AddressLine2],x.[DistrictId],x.[SubDistrictId],x.[PostalCode],x.[IsDefault],x.[Tag],x.[ProvinceId],x.[Latitude],x.[Longitude]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictInsert

/***** Object:  StoredProcedure [dbo].[DistrictInsert] *****/
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[UserBasicInfoSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Adesh Mishra>
-- Create date: <25-02-2019>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserBasicInfoSelect] 
	-- Add the parameters for the stored procedure here  select * from users 
	--declare
	@UserIds NVARCHAR(500)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select 
	   U.[Id],
	   U.FirstName,
       U.[ProfilePic],
       RolesXml = 
	   (
	       SELECT R.Id,R.Name 
		   FROM Roles R INNER JOIN UserRoles UR ON R.Id = UR.RoleId 
		   WHERE UR.UserId = U.Id 
		   FOR XML AUTO,ROOT,ELEMENTs
		)
    
	  FROM Users U
	
	  WHERE
	  U.IsActive = 1
	  AND
	  U.IsDeleted = 0
	  AND 
	  U.Id IN (SELECT number FROM ListToTable(@UserIds))

	  --declare @id bigint ,@FirstName varchar(100), @ProfilePic varchar(max),@RolesXml varchar(max)

	  --select @id as Id ,@FirstName FirstName,@ProfilePic ProfilePic,@RolesXml RolesXml

END
GO
/****** Object:  StoredProcedure [dbo].[UserCallingStatusDeactivate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserCallingStatusDeactivate] 
	-- Add the parameters for the stored procedure here
	  @UserId BIGINT= NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Update UserTwilioCallingStatus 
	SET
	IsActive = 0 
	Where 
	FromId = @UserId OR ToId = @UserId

END
GO
/****** Object:  StoredProcedure [dbo].[UserCallingStatusUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UserCallingStatusUpdate]   
 -- Add the parameters for the stored procedure here  
 @FromUserId BIGINT = NULL,  
 @ToUserId BIGINT = NULL,  
 @IsActive BIT = NULL  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

 declare @callStatus varchar(20);
 if(@IsActive=1)
 begin 
 set @callStatus='BUSY'
 end
 else
 begin 
 set @callStatus='BUSY'

 end
  
    -- Insert statements for procedure here  
  IF EXISTS (Select Id from users Where id = @FromUserId and callStatus='BUSY')  
  BEGIN  
    Update users   
    SET   
    callStatus = 'IDLE'  
    WHERE   
    id = @FromUserId  
     
  END  
  ELSE  
  BEGIN  
   Update users   
    SET   
    callStatus = 'BUSY'  
    WHERE   
    id = @FromUserId 
  END  
   
   IF EXISTS (Select Id from users Where id = @ToUserId and callStatus='BUSY')  
  BEGIN  
    Update users   
    SET   
    callStatus = 'IDLE'  
    WHERE   
    id = @ToUserId  
     
  END  
  ELSE  
  BEGIN  
   Update users   
    SET   
    callStatus = 'BUSY'  
    WHERE   
    id = @ToUserId 
  END 


END
GO
/****** Object:  StoredProcedure [dbo].[UserCordinateInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserCordinateInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Latitude    DECIMAL(18,2)=NULL,
			@Longitude    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [UserCordinate]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Latitude],[Longitude],[OrderId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Latitude,@Longitude,@OrderId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : UserCordinateUpdate

/***** Object:  StoredProcedure  [dbo].[UserCordinateUpdate] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UserCordinateSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserCordinateSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[Latitude]
	 	,
	 		R.[Longitude]
	 	,
	 		R.[OrderId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [UserCordinate] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceSelect

/***** Object:  StoredProcedure [dbo].[ServiceSelect] ***/

GO
/****** Object:  StoredProcedure [dbo].[UserCordinateUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserCordinateUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Latitude    DECIMAL(18,2)=NULL,
			@Longitude    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [UserCordinate]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Latitude] = ISNULL(@Latitude,[Latitude]),
			 	[Longitude] = ISNULL(@Longitude,[Longitude]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : UserCordinateXMLSave


GO
/****** Object:  StoredProcedure [dbo].[UserCordinateXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserCordinateXMLSave]
 @UserCordinateXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(Latitude)[1]', 'DECIMAL(18,2)') AS 'Latitude',
		NDS.DT.value('(Longitude)[1]', 'DECIMAL(18,2)') AS 'Longitude',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId'
  FROM 
	@UserCordinateXml.nodes('/root/UserCordinate') AS NDS(DT)
   )
   MERGE INTO UserCordinate R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[Latitude] =ISNULL(x.[Latitude] ,R.[Latitude]),R.[Longitude] =ISNULL(x.[Longitude] ,R.[Longitude]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[Latitude],
		[Longitude],
		[OrderId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[Latitude],x.[Longitude],x.[OrderId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceInsert

/***** Object:  StoredProcedure [dbo].[ServiceInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UserDevicedelete]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserDevicedelete]          
 --declare    
 @DeviceId  NVARCHAR(250)=NULL        
         
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
           
  delete from UserDevice where DeviceId=@DeviceId        
     
        
END
GO
/****** Object:  StoredProcedure [dbo].[UserDeviceInsert]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserDeviceInsert]      
 --declare
 @DeviceId  NVARCHAR(250)=NULL,      
 @UserId   BIGINT=NULL,      
 @DeviceType  NVARCHAR(50)=NULL,      
 @UserType  NVARCHAR(50)=NULL,      
 @DeviceToken NVARCHAR(250)=NULL,      
 @IsDeleted  BIT=NULL      
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
       
 -- delete from UserDevice where DeviceId=@DeviceId and   
  
 IF NOT EXISTS(SELECT DeviceId FROM UserDevice WHERE DeviceId=@DeviceId And userid=@UserId AND IsDeleted = 0)      
 BEGIN      
     
  INSERT INTO UserDevice      
  (      
   DeviceId,      
   UserId,      
   DeviceType,      
   UserType,      
   DeviceToken,    
   IsDeleted    
  )      
  VALUES      
  (      
   @DeviceId,      
   @UserId,      
   @DeviceType,      
   @UserType,      
   @DeviceToken ,    
   0    
       
  )      
   select cast(1 as decimal)    
 END      
 ELSE      
 BEGIN      
  UPDATE UserDevice SET      
   DeviceType = ISNULL(@DeviceType, DeviceType),      
   UserType = ISNULL(@UserType, UserType),      
   DeviceToken = ISNULL(@DeviceToken, DeviceToken),      
   IsDeleted = ISNULL(@IsDeleted, IsDeleted),      
   UserId = ISNULL(@UserId,UserId)      
  WHERE       
   DeviceId=@DeviceId 
   and
   IsDeleted=0
   and 
   userid=@UserId
   
   
   --AND delete  from UserDevice     
   select cast(1 as decimal)    
 END      
END 
GO
/****** Object:  StoredProcedure [dbo].[UserDeviceSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserDeviceSelect]
	@UserId			BIGINT=NULL,
	@DeviceType		NVARCHAR(50)=NULL,
	@UserType		NVARCHAR(50)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM UserDevice
	WHERE
	IsDeleted=0
	AND
	(
		@UserId IS NULL
		OR
		UserId=@UserId
	)
	AND
	(
		@DeviceType IS NULL
		OR
		DeviceType=@DeviceType
	)
	AND
	(
		@UserType IS NULL
		OR
		UserType=@UserType
	)
END
GO
/****** Object:  StoredProcedure [dbo].[UserDeviceUpdate]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserDeviceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@DeviceId NVARCHAR(MAX)=NULL,
			@NotificationToken NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [UserDevice]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[DeviceId] = ISNULL(@DeviceId,[DeviceId]),
			 	[NotificationToken] = ISNULL(@NotificationToken,[NotificationToken])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : UserDeviceXMLSave


GO
/****** Object:  StoredProcedure [dbo].[UserDeviceXMLSave]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserDeviceXMLSave]
 @UserDeviceXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(DeviceId)[1]', 'NVARCHAR') AS 'DeviceId',
		NDS.DT.value('(NotificationToken)[1]', 'NVARCHAR') AS 'NotificationToken'
  FROM 
	@UserDeviceXml.nodes('/root/UserDevice') AS NDS(DT)
   )
   MERGE INTO UserDevice R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[DeviceId] =ISNULL(x.[DeviceId] ,R.[DeviceId]),R.[NotificationToken] =ISNULL(x.[NotificationToken] ,R.[NotificationToken])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[DeviceId],
		[NotificationToken]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[DeviceId],x.[NotificationToken]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : PasswordLogInsert

/***** Object:  StoredProcedure [dbo].[PasswordLogInsert] *****/
SET ANSI_NULLS ON

GO
/****** Object:  StoredProcedure [dbo].[UserSelect]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserSelect]                                
--select * from users                             
--declare                                          
@Id          BIGINT=NULL,                                            
@Role        NVARCHAR(20)=null,                                            
@next    INT = NULL,                                            
@offset   INT = NULL,                                     
                            
@firstName     NVARCHAR(max)=null,                              
@lastName     NVARCHAR(max)=null,                              
@PhoneNumber     NVARCHAR(max)=null,                              
@email    NVARCHAR(max)=null,                              
@address    NVARCHAR(max)=null,                             
                             
@districtid  BIGINT=NULL,                             
@subdistrictid  BIGINT=NULL,                            
@lastdate datetimeoffset=null,                            
@registerdate datetimeoffset=null,                        
    @nickName nvarchar(200)=null  ,                      
   @SortColumn nvarchar(200)=null  ,              
   @SortOrder            nvarchar(200)=null ,               
 @freetext nvarchar(max)=null                            
AS                                          
BEGIN       

         if(@SortColumn is null)
			   begin
			      set @SortColumn='ID'
				  set @SortOrder='DESC'
			   end                                  


  declare @temprole varchar(50)=null;                                        
 if(@Role='ROLE_ADMIN')                                        
 begin                                         
  set  @temprole='ROLE_ADMIN';                                        
  set @role=null;                                        
 end                                        
                                        
IF @next IS NULL                                            
BEGIN                                            
SET @next =100000                                            
SET @offset=1                                            
END                                            
   select * from (                                         
SELECT                                             
       U.[Id],                                            
    U.[IsActive],                                            
    U.[IsDeleted],                                            
    U.[FirstName],                                            
    U.[LastName],       
 U.FirstName+' '+U.LastName FullName,      
    U.[Email],                                            
    U.[PhoneNumber],                                            
    U.[UserName],                                            
    U.[UniqueCode],                                            
    U.[TenantId],                                
 u.[ProfilePic],                        
    RO.[Name] AS RoleName,                                            
    case when RO.[Name]='ROLE_ADMIN' then 'ADMIN' when RO.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'                                           
      when RO.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when RO.[Name]='ROLE_DRIVER' then 'DRIVER'                                         
   when RO.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'                                         
   end as Role                                           
      ,                                      
   U.Gender,                                      
   U.EmployeeId,                                      
   U.Position,                                      
   U.NationalId,                                      
   U.LicenceId,                                      
   U.BikeInformation,                                      
   U.LicencePlate,                                      
   U.DOB,                                      
    U.Status,                                     
    U.NickName,                 
 U.adminNote,                
 U.EmailConfirmed,                              
   overall_count = COUNT (*) OVER()   ,    
   admincount= (select count(*) from users u join userroles ur on u.id=ur.userid where ur.roleid=1 and u.IsDeleted=0),  
     
                ordersCount=( select count(*) from [order] where userid=U.id ) ,                    
 lastOrder=( select top 1 id from [order] where userid=U.id order by id desc) ,                          
 lastOrderDate=( select top 1 createdon from [order] where userid=U.id order by id desc)  ,                        
                        
  AddressXml=(                                                  
     SELECT                                      
    R.[Id],                                      
    R.[AddressLine1],                                      
   R.[AddressLine2],                                      
R.[DistrictId],                                      
    R.[SubDistrictId],                                      
    R.[PostalCode],                                      
    R.[Tag],                                   
    R.[Latitude],                                      
    R.[Longitude],                                      
   R.[HouseNumber],                                      
   R.[StreetNumber],                                      
   R.[Note],                                      
   R.[Type],                                      
                                      
                                    
   R.[BuildingName] ,                                    
   R.[Floor] ,                                    
   R.[UnitNo] ,                                    
   R.[PhoneNo] ,                                    
   R.alterNumber ,                                    
   R.[ResidenceType] ,                                    
   R.NOte,                                 
                        
   D.Name  AS DistrictName,                                      
   SD.Name  AS SubDistrictName,                            
    PD.Name As ProvinceName,                            
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =U.LanguageId ) AS DistrictNameL,                                      
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =U.LanguageId  ) AS SubDistrictNameL            ,                          
      (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =U.LanguageId ) AS ProvinceNameL                                  
                                     
                                      
 FROM [UserAddress] R INNER JOIN                                      
 [District] D ON D.Id =  R.[DistrictId]                                      
 INNER JOIN                                       
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                          
 INNER JOIN                                       
 Province PD ON PD.Id =  R.ProvinceId                          
                          
                          
 where R.userId=U.ID                                
      FOR XML AUTO,ROOT,ELEMENTs                                                  
    )                      
                       
 --U.ProfilePic                                  
 ,FileXml=(                                              
     SELECT                                               
      FG.[Id],                                              
      FG.[CreatedBy],                                              
      FG.[UpdatedBy],                                              
      FG.[CreatedOn],                                              
      FG.[UpdatedOn],                                              
      FG.[IsDeleted],                                              
      FG.[IsActive],                                              
      FG.[Filename],                                              
      FG.[MimeType],                                              
      FG.[Thumbnail],                                          
      FG.[Size],                                              
      FG.[Path],                                              
      FG.[OriginalName],                                              
      FG.[OnServer],                                              
      FG.[TypeId],                 
      FG.[Type]                                              
      FROM FileGroupItems FG                                              
      WHERE FG.[TypeId] = U.id AND FG.[Type]='USERS_PROFILEPIC' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0                      
      FOR XML AUTO,ROOT,ELEMENTs                                              
    )                      
                                           
 FROM                                             
 Users U                                             
 INNER JOIN UserRoles UR ON U.Id = UR.UserId                                             
 INNER JOIN Roles RO ON RO.Id = UR.RoleId                                             
                                            
WHERE                                             
 (                                             
 @Id IS NULL                                             
 OR                  
 U.Id=@Id                                             
 )                                             
                                            
 AND                                            
 (                                             
 @Role IS NULL                                             
 OR                                             
 RO.Name = @Role                                            
 )                                             
                                        
 AND                                            
 (                                             
 @temprole IS NULL                                             
  or                                                                       
     RO.[Name] in (select [name] from roles where id in (1,5))                                        
                                             
 )                            
 and                               
U.isdeleted=0                              
 AND                                            
 (                                             
 @firstName IS NULL                                             
 OR                                             
 U.FirstName = @firstName                                            
 )                            
  AND                                            
 (                                             
 @lastName IS NULL                                             
 OR                                             
 U.LastName = @lastName                                            
 )                            
 AND                                            
 (                                             
 @email IS NULL                                             
 OR                                             
 U.Email = @email                                            
 )                            
 AND                                            
 (                                             
 @address IS NULL                                             
 OR                                             
 U.id in ( select userid from UserAddress where HouseNumber like ''+@address+'' or StreetNumber like ''+@address+'' or [Floor] like ''+@address+''   )                                          
 )                            
 AND                                            
 (                                             
 @districtid IS NULL                                             
 OR                                             
 U.id in ( select userid from UserAddress where DistrictId=@districtid   )                                          
 )                            
 AND                                            
 (                               
 @subdistrictid IS NULL                                             
 OR                                             
 U.id in ( select userid from UserAddress where SubDistrictId=@subdistrictid   )                                          
 )                            
 AND                                  
 (                                             
 @lastdate IS NULL                                             
 OR         --select * from [Order]                            
 U.id in ( select userid from [Order] where  Cast([CreatedOn] As Date) = Cast(@lastdate As Date) )                                          
 )                     
 AND                                  
 (                                             
 @registerdate IS NULL                                             
 OR                                             
 U.id in ( select id from Users where Cast([CreatedOn] As Date) = Cast(@registerdate As Date)    )                                          
 )                    
 and                            
 (                            
 @PhoneNumber is null                            
 or                            
 u.PhoneNumber =@PhoneNumber                            
                             
 )                    
  and                            
 (                            
 @nickName is null                            
 or            
 u.NickName =@nickName                            
                             
 )         
 AND                        
 (                         
 @freetext IS NULL                         
  or                                                   
     U.FirstName like '%'+@freetext+'%'         
  or         
    U.Lastname like  '%'+@freetext+'%'        
  or         
    U.phonenumber like '%'+@freetext+'%'        
  or         
    U.email like '%'+@freetext+'%'        
  or         
    U.Position like '%'+@freetext+'%'        
 or         
    U.NationalId like '%'+@freetext+'%'        
 or         
    U.LicenceId like '%'+@freetext+'%'        
 or         
    U.LicencePlate like '%'+@freetext+'%'        
     or         
    U.BikeInformation like '%'+@freetext+'%'         
 or      
 U.FirstName+' '+U.LastName like '%'+@freetext+'%'        
     or      
   U.username like '%'+@freetext+'%'        
   --LicencePlate,  BikeInformation             
 )         
           
        )       as cte              
                          
        ORDER BY                
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='ASC')   THEN [FirstName]   END ASC,                
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='DESC')  THEN [FirstName]    END DESC,                
              
            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='ASC')   THEN NickName   END ASC,                
            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='DESC')  THEN NickName    END DESC,                
                          
   CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='ASC')   THEN PhoneNumber   END ASC,                
            CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='DESC')  THEN PhoneNumber    END DESC,               
              
   CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='ASC')   THEN email   END ASC,                
            CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='DESC')  THEN email    END DESC,               
                 
   CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='ASC')   THEN EmailConfirmed   END ASC,                
            CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='DESC')  THEN EmailConfirmed    END DESC,               
              
            CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='ASC')   THEN ordersCount   END ASC,                
            CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='DESC')  THEN ordersCount    END DESC,               
              
   CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='ASC')   THEN lastOrderDate   END ASC,                
            CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='DESC')  THEN lastOrderDate    END DESC ,              
                 
   CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,                
            CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC,               
              
   CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,                
            CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC,               
                 
   CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN LicencePlate   END ASC,                
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN LicencePlate    END DESC,               
              
   CASE WHEN (@SortColumn = 'ID' AND @SortOrder='ASC')   THEN id   END ASC,                
            CASE WHEN (@SortColumn = 'ID' AND @SortOrder='DESC')  THEN id    END DESC,               
              
              
   CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN [STATUS] END ASC,                
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN [STATUS]    END DESC              
   --ORDER BY U.Id DESC  STATUS              
                 
              
              
   OFFSET (@next*@offset)-@next ROWS                                            
   FETCH NEXT @next ROWS ONLY                                            
                                            
END                                            
---------------------------------------------------                                            
---------------------------------------------------                                             
                                             
SET ANSI_NULLS ON 
GO
/****** Object:  StoredProcedure [dbo].[UserSelectForBroadcastMsg]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserSelectForBroadcastMsg](            
      
--declare          
@UserType nvarchar(max)=null,            
@postalCode nvarchar(max)='',            
@Gender nvarchar(max)=null,            
@Relation  nvarchar(max)=null,            
@OrderCount int=null,            
@next   int = null,              
@offset  int =null            
            
) AS             
BEGIN              
              
IF @next IS NULL              
BEGIN              
SET @next =100000              
SET @offset=1              
END            
            
select             
U.id,U.firstname+' '+U.LastName  Name ,            
UD.DeviceId,            
UD.DeviceToken   ,          
overall_count = COUNT(*) OVER()           
           
from [users] U             
join userdevice UD on U.id=UD.userid             
join userroles ur on u.id=ur.userid            
where             
 (                                           
 @UserType IS NULL                                           
 OR   
 @UserType='ALL_CUSTOMERS'            
 and   
 U.Id in (select distinct UserId from UserDevice)    
  OR   
 @UserType='WITHOUT_ORDER'            
 and   
 U.Id in (select id from users u join UserRoles ur on u.id=ur.userid where u.id not in (select distinct userid from [order]) and u.isdeleted=0 and u.isactive=1 and ur.roleid=3)  
  
   
 )            
 and            
 (                                           
 @Gender IS NULL                                           
 OR                                           
 U.Gender=@Gender                                           
 )             
 --and            
 --(                                           
 -- @postalCode IS NULL                                           
 -- or                                           
 -- U.id in ( select userid from UserAddress where postalCode IN (SELECT number FROM ListToTable(@postalCode)))                                          
 --)            
 and            
 (            
 @Relation is null            
 or             
 @Relation='EQUAL_TO'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  = @OrderCount )            
 or            
 @Relation='GREATER_THAN'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  > @OrderCount )            
  or            
 @Relation='LESS_THAN'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  < @OrderCount )            
 )  
 and ur.roleid=3
 --and        
 --U.Id in (select distinct UserId from UserDevice)          
 and       
 UD.IsDeleted=0      
        
Order by U.Id desc          
 OFFSET (@next*@offset)-@next ROWS          
    FETCH NEXT @next ROWS ONLY          
          
 end
GO
/****** Object:  StoredProcedure [dbo].[UserSelectWithPostalForBroadcastMsg]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserSelectWithPostalForBroadcastMsg](            
      
--declare          
@UserType nvarchar(max)=null,            
@postalCode nvarchar(max)='',            
@Gender nvarchar(max)=null,            
@Relation  nvarchar(max)=null,            
@OrderCount int=10,            
@next   int = null,              
@offset  int =null            
            
) AS             
BEGIN              
              
IF @next IS NULL              
BEGIN              
SET @next =100000              
SET @offset=1              
END            
            
select             
U.id,U.firstname+' '+U.LastName  Name ,            
UD.DeviceId,            
UD.DeviceToken   ,          
overall_count = COUNT(*) OVER()           
            
from [users] U             
join userdevice UD on U.id=UD.userid             
  join userroles ur on u.id=ur.userid             
where             
 (                                           
 @UserType IS NULL                                           
 OR                                           
 U.Id in (select distinct UserId from UserDevice)                                           
 )            
 and            
 (                                           
 @Gender IS NULL                                           
 OR                                           
 U.Gender=@Gender                                           
 )             
 and            
 (                                           
  @postalCode IS NULL                                           
  or                                           
  U.id in ( select userid from UserAddress where postalCode IN (SELECT number FROM ListToTable(@postalCode)))                                          
 )            
 and            
 (            
 @Relation is null            
 or             
 @Relation='EQUAL_TO'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  = @OrderCount )            
 or            
 @Relation='GREATER_THAN'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  > @OrderCount )            
  or            
 @Relation='LESS_THAN'            
 and             
 U.id in (SELECT userid FROM [order] GROUP BY userid HAVING  COUNT(*)  < @OrderCount )            
 )         
 and        
 U.Id in (select distinct UserId from UserDevice)          
 and       
 UD.IsDeleted=0      
   and 
   ur.roleid=3
Order by U.Id desc          
 OFFSET (@next*@offset)-@next ROWS          
    FETCH NEXT @next ROWS ONLY          
          
 end
GO
/****** Object:  StoredProcedure [dbo].[ValidatePostalCode]    Script Date: 25-03-2021 17:10:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidatePostalCode](    
@Id         BIGINT =NULL,    
@Slug     NVARCHAR(255)=NULL,    
@next   int = NULL,    
@offset  int = NULL,    
@RelationTable NVARCHAR(50)=NULL,    
@RelationId bigint=NULL,    
@TenantId   INT=NULL,  
@PostalCode varchar(20)=null  
)    
AS BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
 SELECT    
    R.[Id]    
   ,    
    R.[TenantId]    
   ,    
    R.[Slug]    
   ,    
    R.[CreatedBy]    
   ,    
    R.[UpdatedBy]    
   ,    
    R.[CreatedOn]    
   ,    
    R.[UpdatedOn]    
   ,    
    R.[IsDeleted]    
   ,    
    R.[IsActive]    
   ,    
    R.[PostalCode]    
   ,    
 overall_count = COUNT(*) OVER()    
 FROM [PostalCode] R      
 WHERE     
 (    
  @RelationTable IS NULL    
 )    
 AND    
 (    
  @Id IS NULL    
  OR    
  R.[Id] = @Id    
 )    
 
 AND    
 (    
  @Slug IS NULL    
  OR    
  R.slug = @Slug    
 )    
 AND    
 (    
  R.[IsDeleted] = 0    
 )    
 AND    
 (    
  @TenantId IS NULL    
  OR    
  R.[TenantId] = @TenantId    
 )    
 And  
 (  
  @PostalCode IS NULL    
  OR   
  r.PostalCode=@PostalCode  
  
 )  
    
 Order by R.Id desc    
 OFFSET (@next*@offset)-@next ROWS    
    FETCH NEXT @next ROWS ONLY    
    
END 
GO
