import {Routes} from '@angular/router';
import {AuthGuard} from '../shared/auth/auth.guard';

export const ROUTES: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {
        path: 'language',
        loadChildren: './pages/language/language.module#LanguagePageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'login',
        loadChildren: './pages/authorization/login/login.module#LoginPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'registration',
        loadChildren: './pages/authorization/registration/registration.module#RegistrationPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'forgot-password',
        loadChildren: './pages/authorization/forgot-password/forgot-password.module#ForgotPasswordPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'otp-verification',
        loadChildren: './pages/authorization/otp-verification/otp-verification.module#OtpVerificationPageModule',
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'registration/success',
        loadChildren: './pages/authorization/success/success.module#SuccessPageModule',
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'confirm-account',
        loadChildren: './pages/authorization/email-verification/email-verification.module#EmailVerificationPageModule',
        data: {roles: ['ROLE_ANONYMOUS']}
    },
    {
        path: 'landing', loadChildren: './pages/landing/landing.module#LandingPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'account/settings',
        loadChildren: './pages/account-settings/account-settings.module#AccountSettingsPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'categories',
        loadChildren: './pages/category/service/service.module#ServicePageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'category/:categoryId/items',
        loadChildren: './pages/category/item/item.module#ItemPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'cart',
        loadChildren: './pages/cart/cart.module#CartPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'checkout',
        loadChildren: './pages/order/checkout/checkout.module#CheckoutPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/preview',
        loadChildren: './pages/order/preview/order-preview.module#OrderPreviewPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/payment/selection',
        loadChildren: './pages/order/payment-selection/payment-selection.module#PaymentSelectionPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/payment/card',
        loadChildren: './pages/order/payment/card/order-payment.module#OrderPaymentPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/payment/cash',
        loadChildren: './pages/order/payment/cash/order-cash-payment.module#OrderCashPaymentPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/payment/qr_code',
        loadChildren: './pages/order/payment/qrcode/order-qr-code-payment.module#OrderQrCodePaymentPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/:orderId/success',
        loadChildren: './pages/order/confirmation/order-confirmation.module#OrderConfirmationPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/failure',
        loadChildren: './pages/order/failure/order-failure.module#OrderFailurePageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'my/orders',
        loadChildren: './pages/order/my-orders/my-orders.module#MyOrdersPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'notification/order/:orderId/detail',
        loadChildren: './pages/order/detail/order-detail.module#OrderDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/:orderId/detail',
        loadChildren: './pages/order/detail/order-detail.module#OrderDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'place/order/:orderId/detail',
        loadChildren: './pages/order/detail/order-detail.module#OrderDetailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/:orderId/tracking',
        loadChildren: './pages/order/detail/order-tracking/order-tracking.module#OrderTrackingPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'order/:orderId/chat/:me/:from',
        loadChildren: './pages/chat/chat.module#ChatPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'user-settings',
        loadChildren: './pages/account-settings/user-settings/user-settings.module#UserSettingsPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'user-address',
        loadChildren: './pages/user-address/user-address.module#UserAddressPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'verify-password',
        loadChildren: './pages/account-settings/verify-password/verify-password.module#VerifyPasswordPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'verify-email',
        loadChildren: './pages/account-settings/verify-email/verify-email.module#VerifyEmailPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'change-phone-number',
        loadChildren: './pages/account-settings/change-phone-number/change-phone-number.module#ChangePhoneNumberPageModule',
        canActivate: [AuthGuard],
        data: {roles: ['ROLE_CUSTOMER']}
    },
    {
        path: 'reset-password',
        loadChildren: './pages/account-settings/verify-password/reset-password/reset-password.module#ResetPasswordPageModule'
    },
    {
        path: 'reset-successful',
        loadChildren: './pages/account-settings/verify-password/reset-successful/reset-successful.module#ResetSuccessfulPageModule'
    }
];
