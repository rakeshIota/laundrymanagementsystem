import { Component, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { EventService } from 'src/shared/event.service';
import { isNullOrUndefined } from 'util';
import { AuthService } from '../../../../shared/auth/auth.service';
import { ModalService } from '../../../../shared/modal.service';
import { Header } from '../../../models/header.model';
import { TwilioVoiceService } from '../../../services/twilio-voice.service';
import { TwilioService } from '../../../services/twilio.service';

declare const Twilio: any;

@Component({
    selector: 'app-calling',
    templateUrl: './calling.component.html',
    styleUrls: ['./calling.component.scss'],
})
export class CallingComponent implements OnInit, OnDestroy {
    @Input()
    data: any;
    speakerEnabled: boolean;
    callText: string;
    header: Header;
    time: number;
    timerInterval: any;
    targetCallerId: string;
    hasMakeCall: boolean;
    eventSubscribe: any;
    callerTargetId: string;
    hasCalledMade: boolean;

    constructor(private twilioService: TwilioService, public twilioVoiceService: TwilioVoiceService, private authService: AuthService,
        private modalService: ModalService, private platform: Platform, private eventService: EventService, private ngZone: NgZone) {
    }

    async ngOnInit() {
        this.header = new Header({
            title: 'PAGES.CALLING.HEADING',
            showBackButton: false,
            showMenu: false,
            hideCartIcon: true
        });
        if (this.twilioVoiceService.onCall) {
            this.initCallReceiver();
            this.processName();
            return;
        }
        this.twilioVoiceService.onCall = false;
        this.speakerEnabled = false;
        this.hasMakeCall = false;
        this.hasCalledMade = false;
        if (this.platform.is('cordova')) {
            Twilio.TwilioVoiceClient.setSpeaker('off');
            this.initCallReceiver();
        }
        this.registerEvents();
        if (this.data.type === 'NEW_CALL') {
            await this.makeCall();
        } else if (this.data.type === 'INCOMING_CALL') {
            await this.acceptIncomingCall();
        }
    }

    processName() {
        let callerId = this.data.from.from;
        callerId = callerId.replace('_1', ' ');
        callerId = callerId.split('_')[0].split(':')[1];
        const splitArray = this.data.from.from.split('_');
        this.callerTargetId = splitArray[splitArray.length - 1];
        callerId = this.capitalize(callerId.replace('-1', ' '));
        this.targetCallerId = callerId;
    }

    registerEvents() {
        if (!isNullOrUndefined(this.eventSubscribe)) {
            return;
        }
        this.eventSubscribe = this.eventService.event.subscribe((response: any) => {
            if (isNullOrUndefined(response)) {
                return;
            }
            if (response.key === 'twilio:call:answered' && this.hasCalledMade) {
                this.answeredCalled();
                this.hasCalledMade = false;
                if (!isNullOrUndefined(this.eventSubscribe)) {
                    this.eventSubscribe.unsubscribe();
                }
            }
        });
    }

    async acceptIncomingCall() {
        this.twilioVoiceService.callStatus = 'INCOMING';
        let callerId = this.data.from.from;
        callerId = callerId.replace('_1', ' ');
        callerId = callerId.split('_')[0].split(':')[1];
        const splitArray = this.data.from.from.split('_');
        this.callerTargetId = splitArray[splitArray.length - 1];
        callerId = this.capitalize(callerId.replace('-1', ' '));
        this.targetCallerId = callerId;
        this.callText = 'Incoming call from ' + callerId;
    }

    async makeCall() {
        this.speakerEnabled = false;
        this.twilioVoiceService.callStatus = 'CALLING';
        this.callText = 'Calling ' + this.data.to.fullName;
        this.targetCallerId = this.data.to.fullName;
        this.hasMakeCall = true;
        this.hasCalledMade = true;
        const callerId = this.data.to.fullName.replace(' ', '-1') + '_' + this.data.to.id;
        if (this.platform.is('cordova')) {
            Twilio.TwilioVoiceClient.setSpeaker('off');
            Twilio.TwilioVoiceClient.call(this.twilioVoiceService.twilioToken, callerId, this.data.to.fullName);
        }
    }

    capitalize(s) {
        return s.toLowerCase().replace(/\b./g, (a) => {
            return a.toUpperCase();
        });
    }

    initCallReceiver() {
        Twilio.TwilioVoiceClient.error((error) => {
            Twilio.TwilioVoiceClient.stopPlayingSound();
            this.hasMakeCall = false;
            this.twilioVoiceService.onCall = false;
            this.clearCallAndGoBack();
        });

        Twilio.TwilioVoiceClient.calldidconnect((call) => {
            Twilio.TwilioVoiceClient.stopPlayingSound();
            if (this.hasMakeCall) {
                console.log('Yes, Call has been make');
                this.hasMakeCall = false;
                return;
            }
            this.ngZone.run(() => {
                this.twilioVoiceService.callStatus = 'CONNECTED';
                this.callText = 'On Call ' + this.targetCallerId;
            });
        });

        Twilio.TwilioVoiceClient.calldiddisconnect((call) => {
            Twilio.TwilioVoiceClient.stopPlayingSound();
            this.hasMakeCall = false;
            this.twilioVoiceService.onCall = false;
            this.twilioVoiceService.callStatus = 'DISCONNECTED';
            this.callText = 'Call Ended ' + this.targetCallerId;
            this.clearCallAndGoBack();
        });

        Twilio.TwilioVoiceClient.callinvitecanceled((call) => {
            this.hasMakeCall = false;
            this.twilioVoiceService.onCall = false;
            Twilio.TwilioVoiceClient.stopPlayingSound();
            if (this.twilioVoiceService.callStatus !== 'CONNECTED') {
                this.clearCallAndGoBack();
            }
        });
    }

    handleTimeInterval() {
        this.time = Number(this.time) + 1;
    }

    async clearCallAndGoBack() {
        await this.clearCallStatus();
        this.close();
    }

    async clearCallStatus() {
        const callerId = this.data.type === 'INCOMING_CALL' ? Number(this.data.from.from.split('_')[1]) : this.data.to.id;
        const param = {
            toId: this.authService.getUser().id,
            fromId: callerId,
            status: 'IDLE'
        };
        await this.twilioService.updateCallStatus(param);
    }

    async acceptCall() {

        Twilio.TwilioVoiceClient.stopPlayingSound();
        this.twilioVoiceService.onCall = true;
        this.speakerEnabled = false;
        Twilio.TwilioVoiceClient.acceptCallInvite();
        Twilio.TwilioVoiceClient.setSpeaker('off');
        this.twilioVoiceService.callStatus = 'CONNECTED';
        this.callText = 'On Call ' + this.targetCallerId;
        await this.updateCurrentCallStatus('BUSY');
        const data = {} as any;
        data.callerId = this.callerTargetId;
        const response: any = await this.twilioService.answerCalled(data);
    }

    async answeredCalled() {
        this.ngZone.run(() => {
            this.callText = 'On Call ' + this.data.to.fullName;
            this.twilioVoiceService.onCall = true;
            this.twilioVoiceService.callStatus = 'CONNECTED';
        });
        Twilio.TwilioVoiceClient.stopPlayingSound();
        await this.updateCurrentCallStatus('BUSY');
    }

    async rejectCall() {
        this.twilioVoiceService.onCall = false;
        await this.updateCurrentCallStatus('IDLE');
        Twilio.TwilioVoiceClient.stopPlayingSound();
        Twilio.TwilioVoiceClient.rejectCallInvite();
        this.twilioVoiceService.callStatus = 'REJECTED';
        this.callText = 'Call Rejected';
        await this.clearCallAndGoBack();
    }

    async disconnectCall() {
        this.twilioVoiceService.onCall = false;
        if (this.platform.is('android')) {
            Twilio.TwilioVoiceClient.stopPlayingSound();
        }
        Twilio.TwilioVoiceClient.disconnect();
        this.twilioVoiceService.callStatus = 'DISCONNECTED';
        this.callText = 'Call Ended';
        if (this.data.type === 'INCOMING_CALL') {
            await this.clearCallAndGoBack();
        } else {
            await this.updateCurrentCallStatus('IDLE');
        }
    }

    async updateCurrentCallStatus(status: string) {
        const params = {
            toUserId: this.authService.getUser().id,
            callStatus: status
        };
        await this.twilioService.updateCallStatus(params);
    }

    toggleSpeaker() {
        Twilio.TwilioVoiceClient.setSpeaker(this.speakerEnabled ? 'off' : 'on');
        this.speakerEnabled = !this.speakerEnabled;
    }

    async close() {
        if (!isNullOrUndefined(this.eventSubscribe)) {
            this.eventSubscribe.unsubscribe();
        }
        if (this.platform.is('android')) {
            Twilio.TwilioVoiceClient.stopPlayingSound();
        }
        this.modalService.callingPopup = false;
        this.modalService.dismiss(true, null);
    }

    ngOnDestroy(): void {
        if (!isNullOrUndefined(this.eventSubscribe)) {
            this.eventSubscribe.unsubscribe();
        }
        if (this.platform.is('android')) {
            Twilio.TwilioVoiceClient.stopPlayingSound();
        }
    }

    getDisplayTime() {
        const min = Math.floor(this.time / 60);
        const second = Math.floor(this.time % 60);
        return (min >= 10 ? min : '0' + min) + ':' + (second >= 10 ? second : '0' + second);
    }
}
