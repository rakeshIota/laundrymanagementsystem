import {Component, OnInit, ViewChild} from '@angular/core';
import {Header} from '../../models/header.model';
import {ActivatedRoute} from '@angular/router';
import {IonContent} from '@ionic/angular';
import {TwilioService} from '../../services/twilio.service';
import {ToastService} from '../../../shared/toast.service';
import {RestResponse} from '../../models/authorization.model';
import {TwilioChatService} from '../../services/twilio-chat.service';
import * as moment from 'moment';
import {File} from '@ionic-native/file/ngx';
import {LocalStorageService} from '../../../shared/local.storage.service';
import {isNullOrUndefined} from 'util';
import {AuthService} from '../../../shared/auth/auth.service';
import {User} from '../../models/user.model';

declare const Twilio: any;
declare const window;

@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
    @ViewChild('scrollMe', {static: false}) chatScroll: IonContent;
    header: Header;
    orderId: string;
    userId: string;
    driverId: string;
    messageLoaded: boolean;
    isLoadingPreviousMessages: boolean;
    chatClient: any;
    activeChannel: any;
    channelMembersInfo: Array<any>;
    lastMessageUpdatedIndex: number;
    isTyping: boolean;
    indicatorText: string;
    hasPrevPage: boolean;
    channelMessages: Array<any>;
    lastMessageIndex: number;
    chatMessages: Array<any>;
    messageIndex: number;
    data: any;
    isKeyboardEnable: boolean;
    textArea: any;
    twiloCurrentUserId: number;
    groupUserIds: string;
    user: User;
    defaultMessages: Array<any>;

    constructor(private route: ActivatedRoute, private twilioService: TwilioService, private toastService: ToastService,
                private twilioChatService: TwilioChatService, private file: File, private localStorageService: LocalStorageService,
                private authService: AuthService) {
    }

    ngOnInit() {
        this.orderId = this.route.snapshot.paramMap.get('orderId');
        this.userId = this.route.snapshot.paramMap.get('me');
        this.driverId = this.route.snapshot.paramMap.get('from');
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'PAGES.CHAT.HEADING',
            showBackButton: true,
            parentLink: `/order/${this.orderId}/tracking`
        });
        this.init();
    }

    init() {
        this.data = {} as any;
        this.channelMessages = new Array<any>();
        this.channelMembersInfo = new Array<any>();
        this.isLoadingPreviousMessages = false;
        this.lastMessageIndex = 0;
        this.fetchDefaultMessages();
        this.fetchTwilioUserDetails();
    }

    async fetchDefaultMessages() {
        this.defaultMessages = new Array<any>();
        const response: RestResponse = await this.twilioService.fetchDefaultMessages();
        if (!response.status) {
            this.toastService.show(response.message);
            return;
        }
        this.defaultMessages = response.data;
    }

    async getPreviousMessagesOnScroll(event: any) {
        const scrollTop = parseInt(event.detail.scrollTop, 10);
        if (scrollTop < 50 && !this.isLoadingPreviousMessages && this.hasPrevPage) {
            this.isLoadingPreviousMessages = true;
            this.getPreviousMessages();
        }
    }

    async getPreviousMessages() {
        const prevMessageArray = new Array<any>();
        this.messageIndex = this.lastMessageIndex > 60 ? this.lastMessageIndex - 60 : 0;
        const messageLength = this.messageIndex === 0 ? (this.lastMessageIndex - 30) + 1 : 30;
        try {
            const messages = await this.activeChannel.getMessages(messageLength, this.messageIndex, 'forward');
            this.hasPrevPage = messages.hasPrevPage;
            messages.items.forEach(element => {
                element.state.messageTime = moment(element.state.timestamp).format('DD MMMM YYYY');
                prevMessageArray.push(element.state);
                this.lastMessageIndex = element.index;
            });
            if (messages.items.length < 30) {
                this.hasPrevPage = false;
            }
            if (prevMessageArray.length === 0) {
                return;
            }
            this.channelMessages = prevMessageArray.concat(this.channelMessages);
            this.renderCompleteMessages();
            this.isLoadingPreviousMessages = false;
        } catch (e) {

        }
    }

    async fetchTwilioUserDetails() {
        const isAccountExists = this.localStorageService.get('isTwilioAccountExists');
        if (!isNullOrUndefined(isAccountExists)) {
            this.initiateChatClient();
            return;
        }
        try {
            const response: RestResponse = await this.twilioService.fetchUser().toPromise();
            this.localStorageService.set('isTwilioAccountExists', 'true');
            this.localStorageService.set('twilioAccountDetails', JSON.stringify(response.data));
            this.initiateChatClient();
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    initiateChatClient() {
        try {
            if (this.chatClient) {
                this.generateChatChannelParams();
                return;
            }
            const device = 'device' + this.userId;
            this.twilioService.generateToken(device).subscribe((response) => {
                if (!response.status) {
                    return;
                }
                this.twiloCurrentUserId = Number(response.data.identity);
                this.twilioChatService.twilioToken = response.data.token;
                Twilio.Chat.Client.create(response.data.token).then(client => {
                    this.twilioChatService.chatClient = client;
                    this.chatClient = client;
                    this.generateChatChannelParams();
                });
            });
        } catch (e) {
            console.log('Error initiating chat client', e);
        }
    }

    generateChatChannelParams() {
        const to = this.driverId;
        const from = this.userId;
        const channelParams = {} as any;
        channelParams.uniqueName = from + '-' + to;
        channelParams.friendlyName = 'Chat between ' + this.userId + ' and ' + this.driverId;
        channelParams.isPrivate = false;
        this.getCurrentChatChannel(channelParams);
    }

    getCurrentChatChannel(channelParams: any) {
        try {
            this.chatClient.getChannelByUniqueName(channelParams.uniqueName)
                .then((channel) => {
                    this.joinExistingChannel(channel);
                })
                .catch((error) => {
                    this.createNewChannel(channelParams);
                    throw (error);
                });
        } catch (e) {
            console.log('Error getting channel with unique name', e);
        }
    }

    joinExistingChannel(myChannel: any) {
        myChannel.join().then((response) => {
            this.activeChannel = myChannel;
            this.getMembers();
        }).catch((err) => {
            this.activeChannel = myChannel;
            this.getMembers();
        });
    }

    createNewChannel(channelParams: any) {
        try {
            this.chatClient
                .createChannel(channelParams)
                .then((channel) => {
                    this.joinExistingChannel(channel);
                }).catch((error) => {
                throw (error);
            });
        } catch (e) {
            console.log(e);
        }
    }

    async getMembers() {
        try {
            const members = await this.activeChannel.getMembers();
            const userIds = [];
            members.forEach(element => {
                userIds.push({userId: Number(element.state.identity)});
            });
            this.groupUserIds = userIds.filter((obj) => {
                return Number(obj.userId) !== Number(this.twiloCurrentUserId);
            }).map((obj) => obj.userId).join(', ');
            const response: any = await this.twilioService.getChannelMember(userIds);
            if (response.status) {
                this.channelMembersInfo = response.data;
            }
            this.getChannelMessages();
        } catch (e) {
            console.log(e);
            this.toastService.show('Failed to read member from channel');
        }
    }

    async getChannelMessages() {
        this.registerChannelEvents();
        try {
            const messages = await this.activeChannel.getMessages();
            this.messageLoaded = true;
            this.hasPrevPage = messages.hasPrevPage;
            messages.items.forEach(element => {
                element.state.messageTime = moment(element.state.timestamp).format('DD MMMM YYYY');
                this.channelMessages.push(element.state);
                this.lastMessageIndex = element.index;
            });
            this.activeChannel.updateLastConsumedMessageIndex(this.lastMessageIndex);
            this.renderCompleteMessages();
        } catch (e) {
            this.toastService.show('Failed to message from channel');
        }
    }

    registerChannelEvents() {
        this.activeChannel.on('messageAdded', (message) => {
            this.activeChannel.updateLastConsumedMessageIndex(message.state.index);
            if (this.lastMessageUpdatedIndex === message.state.index) {
                return;
            }
            this.lastMessageUpdatedIndex = message.state.index;
            this.addMessage(message);
        });

        this.activeChannel.on('typingStarted', (member) => {
            this.isTyping = true;
            this.indicatorText = 'Typing...';
            this.scrollToBottom();
        });

        this.activeChannel.on('typingEnded', (member) => {
            this.isTyping = false;
            this.scrollToBottom();
        });
    }

    addMessage(message) {
        message.state.messageTime = moment(message.state.timestamp).format('DD MMMM YYYY');
        this.channelMessages.push(message.state);
        this.renderCompleteMessages();
    }

    async renderCompleteMessages() {
        this.channelMessages.forEach(element => {
            const memberIndex = this.channelMembersInfo.findIndex(x => x.id === element.author);
            if (memberIndex !== -1) {
                const member = this.channelMembersInfo[memberIndex];
                element.userId = Number(member.id);
                element.fullName = member.fullName;
                element.profileImage = member.profileImageUrl ? member.profileImageUrl : 'assets/images/default-user.png';
            }
        });

        this.channelMessages = this.channelMessages.map(m => {
            if (m.type === 'media') {
                return {
                    ...m,
                    mediaUx: {
                        isLoading: false,
                        url: null,
                        icon: this.twilioChatService.generateIcon(m.media.state.contentType),
                        contentType: m.media.state.contentType
                    }
                };
            }
            return {...m};
        });
        this.groupMessages();
        this.scrollToBottom();
        await this.mapLocalImages();
    }

    groupMessages() {
        const tempMessages = this.twilioChatService.groupBy(this.channelMessages, 'messageTime');

        this.chatMessages = tempMessages.filter((ele) => {
            ele.forEach((ele1) => {
                if (ele1.type === 'text' && ele1.body.lastIndexOf('{') >= 0) {
                    ele1.displayBody = '<a href="#">' + ele1.body.substring(0, ele1.body.lastIndexOf('{')) + '</a>';
                } else {
                    ele1.displayBody = ele1.body;
                }
            });
            return ele;
        });
    }

    scrollToBottom() {
        try {
            setTimeout(() => {
                this.chatScroll.scrollToBottom(500);
                this.isLoadingPreviousMessages = false;
            }, 500);
        } catch (err) {
            console.log(err);
        }
    }

    async mapLocalImages() {
        // mapping existing files with media messages
        for (let i = 0; i < this.channelMessages.length; i++) {
            const message = this.channelMessages[i];
            let fileUrl = null;
            let fileRelativePath = null;
            let fileName = message.sid;
            if (message.type === 'media' && message.media.state.filename && !message.url) {
                const fileNameArray = message.media.state.filename.split('.');
                fileName = message.sid + '.' + fileNameArray[fileNameArray.length - 1];
                try {
                    const response = await this.file.checkFile(this.file.dataDirectory + 'lotuslaundry/', fileName);
                    if (response) {
                        fileRelativePath = this.file.dataDirectory + 'lotuslaundry/' + fileName;
                    }
                    if (message.media.state.contentType.split('/')[0] === 'image') {
                        fileUrl = window.Ionic.WebView.convertFileSrc(this.file.dataDirectory + 'lotuslaundry/' + fileName);
                    }
                    message.mediaUx = {
                        isLoading: false,
                        url: fileUrl,
                        localUrl: fileRelativePath,
                        icon: this.twilioChatService.generateIcon(message.media.state.contentType),
                        contentType: message.media.state.contentType
                    };
                } catch (error) {
                }
            }
        }
    }

    onDisableKeyboard() {
        this.isKeyboardEnable = false;
    }

    onEnableKeyboard() {
        this.isKeyboardEnable = true;
    }

    typingHandler(event: any) {
        if (typeof event.target.value === 'string' && event.target.value.toString().trim() === '') {
            return;
        }
        /*this.textArea = document.getElementsByClassName('text-input')[0];
        if (event.target.scrollHeight < 50) {
            this.textArea.style.height = event.target.scrollHeight + 'px';
        }*/
        if (event.keyCode === 13) {
            this.sendMessage();
            return;
        }
        this.activeChannel.typing();
    }

    sendMessage() {
        if (isNullOrUndefined(this.data.message) || this.data.message.trim().length === 0) {
            return;
        }
        /*if (isNullOrUndefined(this.textArea) || isNullOrUndefined(this.data.message) || this.data.message.trim().length === 0) {
            return;
        }
        this.textArea.style.height = '25px';*/
        this.activeChannel.sendMessage(this.data.message);
        this.sendMessageNotification(this.data);
        this.data.message = '';
    }

    async sendMessageNotification(data) {
        if (!this.groupUserIds) {
            return;
        }
        const notificationObject = {
            message: data.message,
            title: this.user.fullName + ' Sent a Message',
            ids: this.driverId,
            targetId: this.orderId
        };
        // send message notification
        await this.twilioService.sendMessageNotification(notificationObject);
    }

    sendDefaultMessage(defaultMessage) {
        this.data = {} as any;
        this.data.message = defaultMessage.message;
        this.sendMessage();
    }
}
