import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../../../../shared/auth/auth.service';
import { CommonService } from '../../../../shared/common.service';
import { LoadingService } from '../../../../shared/loading.service';
import { ToastService } from '../../../../shared/toast.service';
import { RestResponse } from '../../../models/authorization.model';
import { Header } from '../../../models/header.model';
import { CartService } from '../../../services/cart.service';
import { DataService } from '../../../../shared/data.service';
import { UserService } from '../../../services/user.service';
import { Address } from '../../../models/user.model';
import { TranslateService } from '@ngx-translate/core';
import { NavigationExtras } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'app-order-preview',
    templateUrl: './order-preview.page.html',
    styleUrls: ['./order-preview.page.scss'],
})
export class OrderPreviewPage implements OnInit {
    user: any;
    header: Header;
    cart: any;
    appSettings: any;
    pickupDeliveryAddress: Address;
    logisticChargeShown: boolean;
    logisticChargeProcessed: boolean;

    constructor(private navController: NavController, private cartService: CartService, private loadingService: LoadingService,
        private toastService: ToastService, private commonService: CommonService, private authService: AuthService,
        private dataService: DataService, private userService: UserService, private translate: TranslateService) {
    }

    async ngOnInit() {
        this.appSettings = await this.dataService.getAppSettings();
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'ORDER_PREVIEW_PAGE_HEADER',
            showBackButton: true,
            parentLink: 'checkout'
        });
        await this.init();
        if (isNullOrUndefined(this.cart) || isNullOrUndefined(this.cart.cartItems)) {
            this.navController.navigateRoot('cart');
            return;
        }
        this.getUserAddresses();
        this.cart.uiCartItems = this.commonService.filterRecords(this.cart.cartItems);
    }

    async getUserAddresses() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.userService.getAddresses('PICKUP_DELIVERY').toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                this.loadingService.hideLoading(loading);
                return;
            }
            if (response.data.length > 0) {
                this.pickupDeliveryAddress = response.data.filter(x => x.type === 'PICKUP_DELIVERY')[0];
            }
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.toastService.show(e.message);
            this.loadingService.hideLoading(loading);
        }
    }

    async init() {
        this.cart = {} as any;
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.cartService.fetch().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                this.loadingService.hideLoading(loading);
                return;
            }
            this.cart = response.data;
            this.cart.pickupDate = new Date(this.cart.pickupDate);
            this.cart.deliveryDate = new Date(this.cart.deliveryDate);
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.toastService.show(e.message);
            this.loadingService.hideLoading(loading);
        }
    }

    getDeliveryFees() {
        const sameDayRate = this.appSettings.samedayDeliveryRate;
        const expressRate = this.appSettings.expressDeliveryRate;
        const normalRate = this.appSettings.normalDeliveryRate;
        if (this.cart.deliveryType.toLowerCase() === 'sameday') {
            return this.getSubtotal() * Number(sameDayRate);
        }
        if (this.cart.deliveryType.toLowerCase() === 'express') {
            return this.getSubtotal() * Number(expressRate);
        }
        if (this.cart.deliveryType.toLowerCase() === 'normal') {
            return this.getSubtotal() * Number(normalRate);
        }
        return 0.00;
    }

    getSubtotal() {
        let total = 0.00;
        this.cart.cartItems.forEach((item) => {
            total += item.quantity * item.price;
        });
        return total;
    }

    getTotal() {
        if (this.logisticChargeShown) {
            return this.getSubtotal() + this.getDeliveryFees() + this.getTax() + this.getLogisticCharge();
        }
        return this.getSubtotal() + this.getDeliveryFees() + this.getTax();
    }

    getTax() {
        return this.getSubtotal() * 0.0;
    }

    getLogisticCharge() {
        const total = this.getSubtotal() + this.getDeliveryFees() + this.getTax();
        if (Number(total) < Number(this.appSettings.minimumOrderAmount)) {
            return Number(this.appSettings.logisticCharge);
        }
        return 0.0;
    }

    async continue() {
        if (this.logisticChargeShown) {
            this.next();
            return;
        }
        if (this.getTotal() < this.appSettings.minimumOrderAmount) {
            const header = await this.translate.get('CONFIRMATION.MINIMUM_ORDER_AMOUNT.HEADER').toPromise();
            let body = await this.translate.get('CONFIRMATION.MINIMUM_ORDER_AMOUNT.BODY').toPromise();
            const confirmText = await this.translate.get('CONFIRMATION.MINIMUM_ORDER_AMOUNT.CONFIRM_BUTTON').toPromise();
            const cancelText = await this.translate.get('CONFIRMATION.MINIMUM_ORDER_AMOUNT.CANCEL_BUTTON').toPromise();
            body = body.replace('$amount', Number(this.appSettings.minimumOrderAmount).toFixed(2));
            await this.commonService.confirm(header, body, cancelText, confirmText, this.onNext.bind(this), null);
            return;
        }
        this.next();
    }

    async next() {
        this.cart.logisticChargeShown = this.logisticChargeShown;
        this.cart.total = this.getTotal();
        this.dataService.cartData = this.cart;
        this.dataService.addressData = this.pickupDeliveryAddress;
        await this.navController.navigateForward(['order/payment/selection']);
    }

    onNext() {
        this.logisticChargeShown = true;
    }
}