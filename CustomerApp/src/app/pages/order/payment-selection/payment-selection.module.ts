import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {PaymentSelectionPage} from './payment-selection.page';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CommonAppModule,
        TranslateModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: PaymentSelectionPage
        }])
    ],
    declarations: [PaymentSelectionPage]
})
export class PaymentSelectionPageModule {
}
