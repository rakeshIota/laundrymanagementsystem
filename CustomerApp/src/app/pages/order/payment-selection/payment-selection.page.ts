import {Component, OnInit} from '@angular/core';
import {Header} from '../../../models/header.model';
import {NavController} from '@ionic/angular';
import {CartService} from '../../../services/cart.service';
import {LoadingService} from '../../../../shared/loading.service';
import {ToastService} from '../../../../shared/toast.service';
import {CommonService} from '../../../../shared/common.service';
import {AuthService} from '../../../../shared/auth/auth.service';
import {OrderService} from '../../../services/order.service';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {DataService} from '../../../../shared/data.service';
import {isNullOrUndefined} from 'util';
import {Address} from '../../../models/user.model';

@Component({
    selector: 'app-payment-selection',
    templateUrl: './payment-selection.page.html',
    styleUrls: ['./payment-selection.page.scss'],
})
export class PaymentSelectionPage implements OnInit {
    header: Header;
    user: any;
    cart: any;
    appSettings: any;
    pickupDeliveryAddress: Address;

    constructor(private navController: NavController, private cartService: CartService, private loadingService: LoadingService,
                private toastService: ToastService, private commonService: CommonService, private authService: AuthService,
                private orderService: OrderService, private dataService: DataService, private route: ActivatedRoute,
                private router: Router) {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'ORDER_PAYMENT_PAGE_HEADER',
            showBackButton: true,
            parentLink: 'order/preview'
        });
    }

    ngOnInit() {
        this.init();
    }

    async init() {
        this.cart = this.dataService.cartData;
        this.pickupDeliveryAddress = this.dataService.addressData;
        if (isNullOrUndefined(this.cart)) {
            this.navController.navigateRoot('cart');
            return;
        }
        this.appSettings = await this.dataService.getAppSettings();
    }

    async placeOrder(type) {
        this.dataService.cartData = this.cart;
        this.dataService.addressData = this.pickupDeliveryAddress;
        await this.navController.navigateForward(['order/payment/' + type.toLowerCase()]);
    }

    hasPaymentActive(type) {
        if (isNullOrUndefined(this.appSettings)) {
            return false;
        }
        let availablePaymentTypes = this.appSettings.availablePaymentTypes;
        if (isNullOrUndefined(availablePaymentTypes)) {
            return false;
        }
        availablePaymentTypes = availablePaymentTypes.split(',');
        return availablePaymentTypes.indexOf(type) !== -1;
    }
}
