import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../../../../shared/loading.service';
import { ModalService } from '../../../../shared/modal.service';
import { ToastService } from '../../../../shared/toast.service';
import { RestResponse } from '../../../models/authorization.model';
import { OrderService } from '../../../services/order.service';

@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.scss'],
})
export class RatingComponent implements OnInit {
    @Input()
    data: any;
    onClickValidation: boolean;
    message: string;
    rating: any;

    constructor(private modalService: ModalService, private loadingService: LoadingService, private toastService: ToastService,
        private orderService: OrderService, private translateService: TranslateService) {
    }

    ngOnInit() {
        this.rating = {} as any;
    }

    onQualityChanged($event) {
        this.rating.quality = $event;
    }

    onDeliveryChanged($event) {
        this.rating.deliveryService = $event;
    }

    onStaffChanged($event) {
        this.rating.staff = $event;
    }

    onPackingChanged($event) {
        this.rating.packaging = $event;
    }

    onOverallChanged($event) {
        this.rating.overallSatisfaction = $event;
    }

    back(complete: boolean) {
        this.modalService.dismiss(complete, null);
    }

    async save() {
        if (!await this.isValidated()) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            this.rating.orderId = this.data.order.id;
            const response: RestResponse = await this.orderService.saveRating(this.rating);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.toastService.info(response.message);
            this.back(true);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async isValidated() {
        this.rating.feedbackEmpty = this.rating.feedback === null || this.rating.feedback === undefined || this.rating.feedback.trim() === '' || false;
        if (this.rating.quality === null || this.rating.quality === undefined) {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.QUALITY'));
            return false;
        }
        if (this.rating.deliveryService === null || this.rating.deliveryService === undefined) {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.DELIVERY'));
            return false;
        }
        if (this.rating.staff === null || this.rating.staff === undefined) {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.STAFF'));
            return false;
        }
        if (this.rating.packaging === null || this.rating.packaging === undefined) {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.PACKAGING'));
            return false;
        }
        if (this.rating.overallSatisfaction === null || this.rating.overallSatisfaction === undefined) {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.OVERALL'));
            return false;
        }
        if (this.rating.feedback === null || this.rating.feedback === undefined || this.rating.feedback.trim() === '') {
            this.toastService.show(this.translateService.instant('PAGES.RATE.ERROR.FEEDBACK'));
            return false;
        }
        return true;
    }

    onInputChange() {
        this.rating.feedbackEmpty = this.rating.feedback === null || this.rating.feedback === undefined || this.rating.feedback.trim() === '' || false;
    }
}
