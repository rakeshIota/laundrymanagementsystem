import {Component, OnInit} from '@angular/core';
import {Header} from '../../../../models/header.model';
import {NavController} from '@ionic/angular';
import {CartService} from '../../../../services/cart.service';
import {LoadingService} from '../../../../../shared/loading.service';
import {ToastService} from '../../../../../shared/toast.service';
import {CommonService} from '../../../../../shared/common.service';
import {AuthService} from '../../../../../shared/auth/auth.service';
import {OrderService} from '../../../../services/order.service';
import {RestResponse} from '../../../../models/authorization.model';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {isNullOrUndefined} from 'util';
import {UserService} from '../../../../services/user.service';
import {Address, User} from '../../../../models/user.model';
import {TermAndConditionComponent} from '../../../../components/term-and-condition/term-and-condition.component';
import {ModalService} from '../../../../../shared/modal.service';
import {DataService} from '../../../../../shared/data.service';

@Component({
    selector: 'app-order-payment',
    templateUrl: './order-payment.page.html',
    styleUrls: ['./order-payment.page.scss'],
})
export class OrderPaymentPage implements OnInit {
    header: Header;
    user: User;
    onClickValidation: boolean;
    payment: any;
    cart: any;
    pickupDeliveryAddress: Address;
    iAgreementAccepted: boolean;

    constructor(private navController: NavController, private cartService: CartService, private loadingService: LoadingService,
                private toastService: ToastService, private commonService: CommonService, private authService: AuthService,
                private orderService: OrderService, private route: ActivatedRoute, private router: Router,
                private userService: UserService, private modalService: ModalService, private dataService: DataService) {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'ORDER_PAYMENT_PAGE_HEADER',
            showBackButton: true,
            parentLink: 'order/payment/selection'
        });
    }

    ngOnInit() {
        this.cart = this.dataService.cartData;
        this.pickupDeliveryAddress = this.dataService.addressData;
        if (isNullOrUndefined(this.cart)) {
            this.navController.navigateRoot('cart');
        }
        this.payment = {} as any;
    }

    async makePayment(bol) {
        this.onClickValidation = !bol;
        if (!bol) {
            return;
        }
        if (!this.iAgreementAccepted) {
            this.onClickValidation = true;
            return;
        }
        await this.placeOrder();
    }

    async placeOrder() {
        const order = {} as any;
        order.deliveryAddress = this.pickupDeliveryAddress;
        order.paymentType = 'CARD';
        order.totalAmount = this.cart.total;
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.orderService.place(order);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.onOrderSuccess(response.data);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
            this.onPaymentFailure();
        }
    }

    async onOrderSuccess(data: any) {
        await this.navController.navigateRoot(`order/${data.orderId}/success?orderNumber=${data.orderNumber}`);
    }

    async onPaymentFailure() {
        const navigationExtras: NavigationExtras = {
            state: {
                message: 'Failure'
            }
        };
        await this.navController.navigateRoot(['order/failure'], navigationExtras);
    }

    async openTermPopup() {
        if (this.iAgreementAccepted) {
            return;
        }
        const eData = {} as any;
        this.modalService.addressPopup = true;
        const model = await this.modalService.presentModal(TermAndConditionComponent, {data: eData}, 'term-and-condition-popup');
        this.modalService.onComplete(model, this.onTermCallback.bind(this));
    }

    onTermCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.iAgreementAccepted = true;
    }
}
