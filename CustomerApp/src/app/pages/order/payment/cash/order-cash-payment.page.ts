import { Component, OnInit } from '@angular/core';
import { Header } from '../../../../models/header.model';
import { Address, User } from '../../../../models/user.model';
import { NavController } from '@ionic/angular';
import { LoadingService } from '../../../../../shared/loading.service';
import { ToastService } from '../../../../../shared/toast.service';
import { AuthService } from '../../../../../shared/auth/auth.service';
import { OrderService } from '../../../../services/order.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { RestResponse } from '../../../../models/authorization.model';
import { TermAndConditionComponent } from '../../../../components/term-and-condition/term-and-condition.component';
import { ModalService } from '../../../../../shared/modal.service';
import { DataService } from '../../../../../shared/data.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-order-cash-payment',
    templateUrl: './order-cash-payment.page.html',
    styleUrls: ['./order-cash-payment.page.scss'],
})
export class OrderCashPaymentPage implements OnInit {
    header: Header;
    user: User;
    onClickValidation: boolean;
    payment: any;
    cart: any;
    pickupDeliveryAddress: Address;
    iAgreementAccepted: boolean;
    changeValueInvalid: boolean;

    constructor(private navController: NavController, private loadingService: LoadingService, private toastService: ToastService,
        private authService: AuthService, private orderService: OrderService, private route: ActivatedRoute, private router: Router,
        private modalService: ModalService, private dataService: DataService, private translateService: TranslateService) {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'ORDER_PAYMENT_PAGE_HEADER',
            showBackButton: true,
            parentLink: 'order/payment/selection'
        });
        this.changeValueInvalid = false;
    }

    ngOnInit() {
        this.cart = this.dataService.cartData;
        this.pickupDeliveryAddress = this.dataService.addressData;
        if (isNullOrUndefined(this.cart)) {
            this.navController.navigateRoot('cart');
        }
        this.payment = {} as any;
    }

    async makePayment(bol) {
        this.onClickValidation = !bol;
        if (!bol) {
            return;
        }
        if (!this.iAgreementAccepted) {
            this.onClickValidation = true;
            return;
        }
        const hour = new Date().getHours();
        const min = new Date().getMinutes();
        const pickupDate = new Date(this.cart.pickupDate);
        const today = new Date();
        pickupDate.setHours(0, 0, 0, 0);
        today.setHours(0, 0, 0, 0);
        if (pickupDate.getTime() === today.getTime() && (this.cart.deliveryType === 'NORMAL' || this.cart.deliveryType === 'EXPRESS') && ((hour === 16 && min >= 30) || hour > 16)) {
            const pickupDateText = await this.translateService.get('PAGES.PAYMENT.ERROR.NORMAL_TIME_ERROR').toPromise();
            this.toastService.show(pickupDateText)
            return;
        }
        if (pickupDate.getTime() === today.getTime() && this.cart.deliveryType === 'SAMEDAY' && ((hour === 12 && min >= 30) || hour > 12)) {
            const pickupDateText = await this.translateService.get('PAGES.PAYMENT.ERROR.SAMEDAY_TIME_ERROR').toPromise();
            this.toastService.show(pickupDateText)
            return;
        }
        await this.placeOrder();
    }

    async placeOrder() {
        const order = {} as any;
        order.deliveryAddress = this.pickupDeliveryAddress;
        order.paymentType = 'CASH';
        order.totalAmount = this.cart.total;
        order.change = this.cart.change;
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.orderService.place(order);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.onOrderSuccess(response.data);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
            this.onPaymentFailure();
        }
    }

    async onOrderSuccess(data: any) {
        await this.navController.navigateRoot(`order/${data.orderId}/success?orderNumber=${data.orderNumber}`);
    }

    async onPaymentFailure() {
        const navigationExtras: NavigationExtras = {
            state: {
                message: 'Failure'
            }
        };
        await this.navController.navigateRoot(['order/failure'], navigationExtras);
    }

    async openTermPopup() {
        if (this.iAgreementAccepted) {
            return;
        }
        const eData = {} as any;
        this.modalService.addressPopup = true;
        const model = await this.modalService.presentModal(TermAndConditionComponent, { data: eData }, 'term-and-condition-popup');
        this.modalService.onComplete(model, this.onTermCallback.bind(this));
    }

    onTermCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.iAgreementAccepted = true;
    }

    onChangeValueChange() {
        if (this.cart.change < 0 || this.cart.change > 1000) {
            this.changeValueInvalid = true;
            return;
        }
        this.changeValueInvalid = false;
    }
}
