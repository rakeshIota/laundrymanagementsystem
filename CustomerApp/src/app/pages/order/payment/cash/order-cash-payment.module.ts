import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {OrderCashPaymentPage} from './order-cash-payment.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {BrMaskerModule} from 'br-mask';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: OrderCashPaymentPage
        }]),
        CommonAppModule,
        TranslateModule,
        BrMaskerModule,
    ],
    declarations: [OrderCashPaymentPage]
})
export class OrderCashPaymentPageModule {
}
