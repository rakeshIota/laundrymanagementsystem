import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {OrderQrCodePaymentPage} from './order-qr-code-payment.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: OrderQrCodePaymentPage
        }]),
        CommonAppModule,
        TranslateModule,
    ],
    declarations: [OrderQrCodePaymentPage]
})
export class OrderQrCodePaymentPageModule {
}
