import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-order-confirmation',
    templateUrl: './order-confirmation.page.html',
    styleUrls: ['./order-confirmation.page.scss'],
})
export class OrderConfirmationPage implements OnInit {
    orderNumber: string;

    constructor(private navController: NavController, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.orderNumber = params.orderNumber;
        });
    }

    async detail() {
        const orderId = this.route.snapshot.paramMap.get('orderId');
        await this.navController.navigateRoot(`place/order/${orderId}/detail`);
    }
}
