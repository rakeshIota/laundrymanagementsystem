import { Component, OnInit } from '@angular/core';
import { Header } from '../../../models/header.model';
import { RestResponse } from '../../../models/authorization.model';
import { CommonService } from '../../../../shared/common.service';
import { ToastService } from '../../../../shared/toast.service';
import { OrderService } from '../../../services/order.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AuthService } from '../../../../shared/auth/auth.service';
import { User } from '../../../models/user.model';
import { NavController } from '@ionic/angular';
import { ModalService } from '../../../../shared/modal.service';
import { RatingComponent } from '../rating/rating.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.page.html',
    styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {
    header: Header;
    order: any;
    orderId: string;
    user: User;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
        private route: ActivatedRoute, private authService: AuthService, private navController: NavController,
        private router: Router, private modalService: ModalService, private translateService: TranslateService) {

    }

    async ngOnInit() {
        this.route.queryParamMap.subscribe((params: any) => {
            this.beforeInit(params);
        });

    }

    async beforeInit(params) {
        let hasRootPage = false;
        let chat: boolean = params.params.chat;
        if (this.router.url.startsWith('/place/order/')) {
            hasRootPage = true;
        }
        if (this.router.url.startsWith('/notification/order/')) {
            hasRootPage = false;
        }
        this.header = new Header({
            title: 'PAGES.ORDER_DETAIL.HEADER_TEXT',
            showBackButton: !hasRootPage,
            showMenu: hasRootPage,
            parentLink: '/my/orders'
        });
        this.orderId = this.route.snapshot.paramMap.get('orderId');
        this.user = this.authService.getUser();
        await this.init();
        this.order.uiItems = this.commonService.filterRecords(this.order.orderItems);
        if (chat && chat.toString() === 'true') {
            await this.router.navigate([], {
                queryParams: {
                    completed: false,
                    chat: false
                }
            });
            this.tracking(this.order, true);
            chat = false;
            return;
        }
        if (this.order.status === 'DELIVERED' && !this.order.rating && !this.order.hasPopupShown) {
            this.openConfirmationRating();
            return;
        }
    }

    async init() {
        try {
            const response: RestResponse = await this.orderService.fetch(this.orderId).toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.order = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async tracking(order: any, iChat: boolean = false) {
        const navigationExtras: NavigationExtras = {
            state: {
                order: this.order,
                landing: false,
                chat: iChat
            }
        };
        await this.navController.navigateForward([`/order/${order.id}/tracking`], navigationExtras);
    }

    async rating() {
        const input = {} as any;
        input.order = this.order;
        const model = await this.modalService.presentModal(RatingComponent, { data: input });
        this.modalService.onComplete(model, this.onRatingCallback.bind(this));
    }

    async openConfirmationRating() {
        const header = await this.translateService.get('CONFIRMATION_HEADER').toPromise();
        const body = await this.translateService.get('CONFIRMATION_REVIEW_TEXT').toPromise();
        const cancelText = await this.translateService.get('CONFIRMATION_SKIP').toPromise();
        const discardText = await this.translateService.get('CONFIRMATION_YES').toPromise();
        return new Promise<boolean>((resolve, reject) => {
            this.commonService.confirmWithCallback(header, body,
                cancelText, discardText,
                () => {
                    this.order.hasPopupShown = true;
                    this.rating();
                    resolve(true);
                }, () => {
                    reject(false);
                });
        });
    }

    onRatingCallback(data) {
        this.order.rating = data.complete;
    }

    async paymentSuccess(order) {
        const data = {} as any;
        data.statusCode = '00';
        data.partnerTxnUid = 'LMS000000' + order.orderId;
        try {
            const response: RestResponse = await this.orderService.paymentSuccess(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.order.paymentStatus = 'PAID';
            this.toastService.info(response.message);
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async paymentFaliure(order) {
        const data = {} as any;
        data.statusCode = '01';
        data.partnerTxnUid = 'LMS000000' + order.orderId;
        try {
            const response: RestResponse = await this.orderService.paymentSuccess(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.toastService.info(response.message);
        } catch (e) {
            this.toastService.show(e.message);
        }
    }
}
