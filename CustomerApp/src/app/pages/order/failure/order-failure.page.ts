import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-order-failure',
    templateUrl: './order-failure.page.html',
    styleUrls: ['./order-failure.page.scss'],
})
export class OrderFailurePage implements OnInit {

    constructor(private navController: NavController) {
    }

    ngOnInit() {
    }

    async back() {
        await this.navController.navigateBack('order/preview');
    }
}
