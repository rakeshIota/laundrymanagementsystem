import { Component, OnInit } from '@angular/core';
import { Header } from '../../../models/header.model';
import { NavController } from '@ionic/angular';
import { CartService } from '../../../services/cart.service';
import { LoadingService } from '../../../../shared/loading.service';
import { ToastService } from '../../../../shared/toast.service';
import { CommonService } from '../../../../shared/common.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '../../../../shared/modal.service';
import { AuthService } from '../../../../shared/auth/auth.service';
import { isNullOrUndefined } from 'util';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { RestResponse } from '../../../models/authorization.model';
import { UserService } from '../../../services/user.service';
import { Address, User } from '../../../models/user.model';
import * as moment from 'moment';
import { DataService } from '../../../../shared/data.service';
import { forEach } from '@angular-devkit/schematics';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.page.html',
    styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
    user: User;
    header: Header;
    pickupSlots: Array<any>;
    deliverySlots: Array<any>;
    cart: any;
    request: any;
    pickupDeliveryAddress: Address;
    basicDetail: any;
    hasUserAddressOpen: boolean;

    constructor(private navController: NavController, private cartService: CartService, private loadingService: LoadingService,
        private toastService: ToastService, private commonService: CommonService, private translateService: TranslateService,
        private modalService: ModalService, private authService: AuthService, private route: ActivatedRoute,
        private router: Router, private userService: UserService, private dataService: DataService) {
    }

    async ngOnInit() {
        this.request = {} as any;
        this.hasUserAddressOpen = false;
        this.init();
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    this.user = this.authService.getUser();
                    if (this.hasUserAddressOpen) {
                        this.hasUserAddressOpen = false;
                        this.getUserAddresses();
                    }
                }
            });
    }

    initializationDateTime() {
        const date = new Date();
        if (this.cart.deliveryType === 'NORMAL') {
            date.setDate(date.getDate() + 1);
        } else if (this.cart.deliveryType === 'EXPRESS') {
            date.setDate(date.getDate() + 1);
        }
        this.initializationPickupDateMinAndMax(date, new Date());
    }

    initializationPickupDateMinAndMax(date: Date, maxDate: Date) {
        let day: any = date.getDate();
        const year = date.getFullYear();
        let month: any = date.getMonth() + 1;
        month = month > 9 ? month : '0' + month;
        day = day > 9 ? day : '0' + day;
        this.request.pickupMin = year + '-' + month + '-' + day;
        if (this.cart.deliveryType === 'NORMAL') {
            maxDate.setDate(maxDate.getDate() + 2);
        } else if (this.cart.deliveryType === 'EXPRESS') {
            maxDate.setDate(maxDate.getDate() + 1);
        }
        let mDay: any = maxDate.getDate();
        const mYear = maxDate.getFullYear();
        let mMonth: any = maxDate.getMonth() + 1;
        mMonth = mMonth > 9 ? mMonth : '0' + mMonth;
        mDay = mDay > 9 ? mDay : '0' + mDay;
        this.request.max = mYear + '-' + mMonth + '-' + mDay;
    }

    async init() {
        this.user = this.authService.getUser();
        this.header = new Header({
            title: 'ADDRESS_PAGE_HEADER',
            showBackButton: true,
            parentLink: 'cart'
        });
        this.cart = this.dataService.cartData;
        if (isNullOrUndefined(this.cart)) {
            this.navController.navigateRoot('cart');
            return;
        }
        const loading = this.loadingService.presentLoading();
        await this.getUserAddresses();
        // this.initializationDateTime();
        const today = new Date();
        this.deliverySlots = new Array<any>();
        this.cart.pickupDate = today.toISOString();
        this.onPickupDateChanged(this.cart.pickupDate);
        if (this.pickupSlots.length === 0) {
            today.setDate(today.getDate() + 1);
            this.cart.pickupDate = today.toISOString();
            this.onPickupDateChanged(this.cart.pickupDate);
            this.initializationPickupDateMinAndMax(today, today);
        } else {
            this.initializationPickupDateMinAndMax(today, today);
        }
        this.cart.pickupSlot = undefined;
        this.cart.deliverySlot = undefined;
        this.loadingService.hideLoading(loading);
        if (this.pickupSlots.length <= 0) {
            return;
        }
        this.cart.pickupSlot = this.pickupSlots[0].id;
        this.generateDeliverySlots();
        if (this.deliverySlots.length <= 0) {
            return;
        }
        this.cart.deliverySlot = this.deliverySlots[0].id;
    }

    generatePickupSlots(date: Date) {
        const month = (date.getMonth() + 1) <= 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
        const day = date.getDate() <= 9 ? '0' + date.getDate() : date.getDate();
        const startTime = '09:00:00 AM';
        let endTime = '04:59:00 PM';
        if (this.cart.deliveryType === 'NORMAL') {
            endTime = '04:59:00 PM';
        } else if (this.cart.deliveryType === 'EXPRESS') {
            endTime = '04:59:00 PM';
        } else if (this.cart.deliveryType === 'SAMEDAY') {
            endTime = '12:59:00 PM';
        }
        const startDate = `${date.getFullYear()}-${month}-${day} ${startTime}`;
        const endDate = `${date.getFullYear()}-${month}-${day} ${endTime}`;
        this.pickupSlots = this.intervals(startDate, endDate);
    }

    intervals(startString, endString) {
        const start = moment(startString, 'YYYY-MM-DD hh:mm a');
        const end = moment(endString, 'YYYY-MM-DD hh:mm a');
        start.minutes(Math.ceil(start.minutes() / 120) * 120);
        const result = [];
        const current = moment(start);
        const compareTime = moment(start);
        compareTime.add(90, 'minutes');
        while (current <= end) {
            const iFrom = current.format('hh:mm A');
            const hasPassedTime = compareTime.format('YYYY-MM-DD HH:mm') > moment().format('YYYY-MM-DD HH:mm');
            current.add(120, 'minutes');
            compareTime.add(120, 'minutes');
            const iEnd = current.format('hh:mm A');
            if (hasPassedTime) {
                result.push({ slug: `${iFrom} - ${iEnd}`, id: `${iFrom} - ${iEnd}` });
            }
        }
        return result;
    }

    async getUserAddresses() {
        try {
            const response: RestResponse = await this.userService.getAddresses('PICKUP_DELIVERY').toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            if (response.data.length > 0) {
                this.pickupDeliveryAddress = response.data.filter(x => x.type === 'PICKUP_DELIVERY')[0];
            }
            this.basicDetail = await this.loadUserDetail();
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async addAddress() {
        const address = new Address();
        address.type = 'PICKUP_DELIVERY';
        await this.navController.navigateForward('user-address', { queryParams: { address: JSON.stringify(address) } });
        this.hasUserAddressOpen = true;
    }

    async editAddress(address: Address) {
        await this.navController.navigateForward('user-address', { queryParams: { address: JSON.stringify(address) } });
        this.hasUserAddressOpen = true;
    }

    addressCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.getUserAddresses();
    }

    async continue() {
        const pickupDateText = await this.translateService.get('PAGES.CHECKOUT.ERROR.PICKUP_DATE_REQUIRED').toPromise();
        const pickupSLotText = await this.translateService.get('PAGES.CHECKOUT.ERROR.PICKUP_SLOT_REQUIRED').toPromise();
        const deliveryDateText = await this.translateService.get('PAGES.CHECKOUT.ERROR.DELIVERY_DATE_REQUIRED').toPromise();
        const deliverySlotText = await this.translateService.get('PAGES.CHECKOUT.ERROR.DELIVERY_SLOT_REQUIRED').toPromise();
        const pickupDeliveryText = await this.translateService.get('PAGES.CHECKOUT.ERROR.PICKUP_DELIVERY_REQUIRED').toPromise();
        if (isNullOrUndefined(this.cart.pickupDate)) {
            this.toastService.show(pickupDateText);
            return;
        }
        if (isNullOrUndefined(this.cart.pickupSlot)) {
            this.toastService.show(pickupSLotText);
            return;
        }
        if (isNullOrUndefined(this.cart.deliveryDate)) {
            this.toastService.show(deliveryDateText);
            return;
        }
        if (isNullOrUndefined(this.cart.deliverySlot)) {
            this.toastService.show(deliverySlotText);
            return;
        }
        if (isNullOrUndefined(this.pickupDeliveryAddress)) {
            this.toastService.show(pickupDeliveryText);
            return;
        }
        if (isNullOrUndefined(this.basicDetail)) {
            return;
        }
        if (!this.basicDetail.inServiceArea) {
            const headerText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_HEADER').toPromise();
            const bodyText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_ORDER_TEXT').toPromise();
            const noText = await this.translateService.get('CONFIRMATION_CANCEL_ORDER').toPromise();
            const yesText = await this.translateService.get('CONFIRMATION_CHANGE_ADDRESS').toPromise();
            this.commonService.confirm(headerText, bodyText,
                noText, yesText, this.onChangeAddress.bind(this), null);
            return;
        }
        this.update();
    }

    async onChangeAddress() {
        await this.navController.navigateForward('user-address', { queryParams: { address: JSON.stringify(this.pickupDeliveryAddress) } });
        this.hasUserAddressOpen = true;
    }

    async loadUserDetail() {
        try {
            const response: RestResponse = await this.userService.fetchBasicDetail();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            return response.data;
        } catch (e) {
            this.toastService.show(e.message);
            return null;
        }
    }

    onPickupSlotSelection(slot) {
        this.cart.pickupSlot = slot.id;
        this.generateDeliverySlots();
        if (this.deliverySlots.length <= 0) {
            return;
        }
        this.cart.deliverySlot = this.deliverySlots[0].id;
    }

    onDeliverySlotSelection(slot) {
        this.cart.deliverySlot = slot.id;
    }

    onPickupDateChanged(date: string) {
        const pickupDate = new Date(date);
        const deliveryDate = new Date(date);
        const deliveryMaxDate = new Date(date);
        if (this.cart.deliveryType === 'NORMAL') {
            deliveryDate.setDate(deliveryDate.getDate() + 2);
            deliveryMaxDate.setDate(deliveryMaxDate.getDate() + 6);
        } else if (this.cart.deliveryType === 'EXPRESS') {
            deliveryDate.setDate(deliveryDate.getDate() + 1);
            deliveryMaxDate.setDate(deliveryMaxDate.getDate() + 1);
        }
        let day: any = deliveryDate.getDate();
        const year = deliveryDate.getFullYear();
        let month: any = deliveryDate.getMonth() + 1;
        month = month > 9 ? month : '0' + month;
        day = day > 9 ? day : '0' + day;
        this.request.deliveryMin = year + '-' + month + '-' + day;

        let dmDay: any = deliveryMaxDate.getDate();
        const dmYear = deliveryMaxDate.getFullYear();
        let dmMonth: any = deliveryMaxDate.getMonth() + 1;
        dmMonth = dmMonth > 9 ? dmMonth : '0' + dmMonth;
        dmDay = dmDay > 9 ? dmDay : '0' + dmDay;
        this.request.deliveryMax = dmYear + '-' + dmMonth + '-' + dmDay;
        this.cart.deliveryDate = deliveryDate.toISOString();
        this.generatePickupSlots(pickupDate);
    }

    onDeliveryDateChanged(date: string) {

    }

    generateDeliverySlots() {
        this.deliverySlots = new Array<any>();
        if (this.cart.deliveryType === 'NORMAL') {
            this.deliverySlots.push({ id: '09:00 AM - 11:00 AM', name: '09:00 AM - 11:00 AM' });
            this.deliverySlots.push({ id: '11:00 AM - 01:00 PM', name: '11:00 AM - 01:00 PM' });
            this.deliverySlots.push({ id: '01:00 PM - 03:00 PM', name: '01:00 PM - 03:00 PM' });
            this.deliverySlots.push({ id: '03:00 PM - 05:00 PM', name: '03:00 PM - 05:00 PM' });
        } else if (this.cart.deliveryType === 'EXPRESS') {
            const deliverySlots = new Array();
            deliverySlots.push({ id: '09:00 AM - 11:00 AM', name: '09:00 AM - 11:00 AM' });
            deliverySlots.push({ id: '11:00 AM - 01:00 PM', name: '11:00 AM - 01:00 PM' });
            deliverySlots.push({ id: '01:00 PM - 03:00 PM', name: '01:00 PM - 03:00 PM' });
            deliverySlots.push({ id: '03:00 PM - 05:00 PM', name: '03:00 PM - 05:00 PM' });
            let index = -1;
            deliverySlots.forEach((slot, iIndex) => {
                const slotName = slot.name.split(' ').join();
                const cartSlotName = this.cart.pickupSlot.split(' ').join();
                if (slotName === cartSlotName) {
                    index = iIndex;
                }
            });
            index = index === 0 ? index : index - 1;
            if (index <= -1) {
                return;
            }
            for (let i = index; i < deliverySlots.length; i++) {
                this.deliverySlots.push(deliverySlots[i]);
            }
        } else if (this.cart.deliveryType === 'SAMEDAY') {
            this.deliverySlots.push({ id: '03:00 PM - 05:00 PM', name: '03:00 PM - 05:00 PM' });
        }
    }

    async update() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.cartService.updateDeliveryTime(this.cart);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            await this.navController.navigateForward('order/preview');
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }
}
