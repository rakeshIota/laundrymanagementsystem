import {Component, OnInit} from '@angular/core';
import {Header} from '../../../models/header.model';
import {RestResponse} from '../../../models/authorization.model';
import {ToastService} from '../../../../shared/toast.service';
import {CommonService} from '../../../../shared/common.service';
import {OrderService} from '../../../services/order.service';
import {NavController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {isNullOrUndefined} from 'util';
import {LoadingService} from '../../../../shared/loading.service';

@Component({
    selector: 'app-my-orders',
    templateUrl: './my-orders.page.html',
    styleUrls: ['./my-orders.page.scss'],
})
export class MyOrdersPage implements OnInit {
    header: Header;
    request: any;
    orders: Array<any>;
    allOrders: Array<any>;
    loading: boolean;

    constructor(private commonService: CommonService, private toastService: ToastService, private orderService: OrderService,
                private navController: NavController, private translateService: TranslateService,
                private loadingService: LoadingService) {
    }

    ngOnInit() {
        this.request = {} as any;
        this.request.status = 'ALL';
        this.header = new Header({
            title: 'ORDER_PAGE_HEADER',
            showBackButton: true,
            parentLink: '/'
        });
        this.loadOrders();
    }

    async loadOrders() {
        try {
            this.loading = true;
            this.orders = new Array<any>();
            const data = {} as any;
            const response: RestResponse = await this.orderService.myOrders(data);
            if (!response.status) {
                this.loading = false;
                this.toastService.show(response.message);
                return;
            }
            this.allOrders = response.data;
            this.orders = response.data;
            this.loading = false;
        } catch (e) {
            this.loading = false;
            this.toastService.show(e.message);
        }
    }

    async onStatusChanged(status) {
        if (isNullOrUndefined(this.allOrders) || isNullOrUndefined(status)) {
            return;
        }
        if (status === 'ALL') {
            this.orders = this.allOrders;
            return;
        }
        this.orders = this.allOrders.filter(x => x.status === status);
    }

    async detail(order) {
        await this.navController.navigateForward(`order/${order.id}/detail`);
    }

    cancel(event, order) {
        event.stopPropagation();
        event.preventDefault();
        this.cancelOrder(order);
        return false;
    }

    async cancelOrder(order) {
        const header = await this.translateService.get('CONFIRMATION.ORDER_CANCEL.HEADER').toPromise();
        let body = await this.translateService.get('CONFIRMATION.ORDER_CANCEL.BODY').toPromise();
        const confirm = await this.translateService.get('CONFIRMATION.ORDER_CANCEL.CONFIRM_BUTTON').toPromise();
        const cancel = await this.translateService.get('CONFIRMATION.ORDER_CANCEL.CANCEL_BUTTON').toPromise();
        body = body + order.orderId;
        await this.commonService.confirm(header, body, cancel, confirm, this.onCancelCallback.bind(this), order);
    }

    async onCancelCallback(order) {
        const loading = this.loadingService.presentLoading();
        try {
            const data = {} as any;
            const response: RestResponse = await this.orderService.cancelOrder(order.id);
            if (!response.status) {
                this.loadingService.hideLoading(loading);
                this.toastService.show(response.message);
                return;
            }
            await this.loadOrders();
            this.toastService.info(response.message);
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }
}
