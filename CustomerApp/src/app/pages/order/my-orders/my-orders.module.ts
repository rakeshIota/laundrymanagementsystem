import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {MyOrdersPage} from './my-orders.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: MyOrdersPage
        }]),
        CommonAppModule,
        TranslateModule
    ],
    declarations: [MyOrdersPage]
})
export class MyOrdersPageModule {
}
