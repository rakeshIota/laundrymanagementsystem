import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {BrMaskerModule} from 'br-mask';
import {CommonAppModule} from 'src/app/components/common.app.module';
import {RegistrationPage} from './registration.page';
import {PendingChangesGuard} from '../../../../shared/pending.guard';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{path: '', component: RegistrationPage, canDeactivate: [PendingChangesGuard]}]),
        ReactiveFormsModule,
        FontAwesomeModule,
        CommonAppModule,
        BrMaskerModule,
        TranslateModule
    ],
    declarations: [RegistrationPage]
})
export class RegistrationPageModule {
}
