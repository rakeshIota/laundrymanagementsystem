import {Component, HostListener, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {User} from 'src/app/models/user.model';
import {LoadingService} from 'src/shared/loading.service';
import {ModalService} from 'src/shared/modal.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../authorization.service';
import {Constants} from '../../../util/constants';
import {environment} from '../../../../environments/environment';
import {PopoverService} from '../../../../shared/popover.service';
import {isNullOrUndefined} from 'util';
import {TermAndConditionComponent} from '../../../components/term-and-condition/term-and-condition.component';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.page.html',
    styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
    user: User;
    copyUser: string;
    onClickValidationBasicForm: boolean;
    onClickValidationDetailForm: boolean;
    passwordPattern: string;
    view: string;
    actionSheetOptions: HTMLIonActionSheetElement;
    maxDate: string;
    mobileZeroError: boolean;

    constructor(private navController: NavController, private authorizationService: AuthorizationService, private toastService: ToastService,
                private loadingService: LoadingService, public popoverService: PopoverService, private menuCtrl: MenuController,
                private modalService: ModalService) {
    }

    ngOnInit() {
        this.menuCtrl.enable(false);
        const today = new Date();
        const month = today.getMonth() + 1;
        const date = today.getDate() - 1;
        const iMonth = month <= 9 ? '0' + month : month;
        const iDate = date <= 9 ? '0' + date : date;
        this.maxDate = today.getFullYear() + '-' + iMonth + '-' + iDate;
        this.view = 'BASIC_INFO';
        this.actionSheetOptions = {} as HTMLIonActionSheetElement;
        this.user = new User();
        this.user.countryCode = '+66';
        this.user.locality = 'RESIDENT';
        this.copyUser = JSON.stringify(this.user);
        this.onClickValidationBasicForm = false;
        this.onClickValidationDetailForm = false;
        this.passwordPattern = `^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$`;
    }

    async next(isValid) {
        this.onClickValidationBasicForm = !isValid;
        if (!isValid) {
            return;
        }
        this.view = 'DETAIL_INFO';
    }

    async register(isValid) {
        this.onClickValidationDetailForm = !isValid;
        this.mobileZeroError = false;
        if (!isValid) {
            return;
        }
        if (!isNullOrUndefined(this.user.mobileNo) && this.user.mobileNo.trim() !== '' && this.user.mobileNo.toString().startsWith('0')) {
            this.onClickValidationDetailForm = true;
            this.mobileZeroError = true;
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.register(this.user);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.copyUser = JSON.stringify(this.user);
            setTimeout(() => {
                this.postProcessed();
            });
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async postProcessed() {
        if (this.user.locality === Constants.TOURIST) {
            await this.navController.navigateRoot('registration/success', {
                queryParams: {
                    email: this.user.email,
                    type: 'REGISTER'
                }
            });
            return;
        }
        await this.navController.navigateRoot('otp-verification', {
            queryParams: {
                userMobileNo: this.user.mobileNo,
                userName: this.user.email,
                otpType: 'REGISTRATION'
            }
        });
    }

    async login() {
        await this.navController.navigateRoot('login');
    }

    openTermAndCondition() {
        window.open(environment.BaseApiUrl + '/term_and_condition.pdf', '_blank');
    }

    @HostListener('window:beforeunload', ['$event'])
    unSavedChanges($event: any) {
        if (this.hasChanged()) {
            $event.returnValue = true;
        }
    }

    hasChanged(): boolean {
        return this.copyUser !== JSON.stringify(this.user);
    }

    async openTermPopup() {
        if (this.user.agreementAccepted) {
            return;
        }
        this.modalService.addressPopup = true;
        const eData = {} as any;
        const model = await this.modalService.presentModal(TermAndConditionComponent, {data: eData}, 'term-and-condition-popup');
        this.modalService.onComplete(model, this.onTermCallback.bind(this));
    }

    onTermCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.user.agreementAccepted = true;
    }

    back() {
        this.view = 'BASIC_INFO';
    }
}
