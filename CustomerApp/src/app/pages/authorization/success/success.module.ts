import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {SuccessPage} from './success.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: SuccessPage
            }
        ]),
        CommonAppModule,
        TranslateModule,
        FontAwesomeModule
    ],
    declarations: [SuccessPage]
})
export class SuccessPageModule {
}
