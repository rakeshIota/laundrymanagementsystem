import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IResourceWithId, RestResponse} from 'src/app/models/authorization.model';
import {User, UserOtpVerification} from 'src/app/models/user.model';
import {HttpServiceRequests} from 'src/shared/http.service';

@Injectable({
    providedIn: 'root'
})

export class AuthorizationService extends HttpServiceRequests<IResourceWithId> {
    constructor(public http: HttpClient) {
        super(http);
    }

    register(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/register', user);
    }

    login(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/login', user);
    }

    forgotPassword(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/forgot-password', user);
    }

    verifyRegistrationOtp(otpDetails: UserOtpVerification): Promise<RestResponse> {
        return this.saveRecord('/app/customer/verify-otp', otpDetails);
    }

    verifyForgotPasswordOtp(otpDetails: UserOtpVerification): Promise<RestResponse> {
        return this.saveRecord('/app/customer/verify-resetOtp', otpDetails);
    }

    verifyPhoneOtp(otpDetails: UserOtpVerification): Promise<RestResponse> {
        return this.saveRecord('/api/phone/change/otp/verify', otpDetails);
    }

    verifyEmailOtp(otpDetails: UserOtpVerification): Promise<RestResponse> {
        return this.saveRecord('/app/customer/email/verify-otp', otpDetails);
    }

    verifyPassword(password): Promise<RestResponse> {
        return this.saveRecord('/api/validate/password', password);
    }

    verifyEmail(email): Promise<RestResponse> {
        return this.saveRecord('/app/customer/verify-email', email);
    }

    changePhoneNumber(user: User): Promise<RestResponse> {
        return this.saveRecord('/api/user/phone/change', user);
    }

    resetPassword(user: User): Promise<RestResponse> {
        return this.saveRecord('/app/customer/reset-password', user);
    }

    sendOTP(data: any) {
        return this.postUrl('/app/customer/resend-otp?userName=' + data.userName);
    }

    sendForgotOTP(data: any) {
        return this.postUrl('/app/customer/resend-emailotp?userName=' + data.userName);
    }

    requestEmailVerification() {
        return this.postUrl('/api/email/verification/send');
    }

    sendPhoneChangeOTP(data: any) {
        return this.saveRecord('/api/phone/change/otp/resend', data);
    }
}
