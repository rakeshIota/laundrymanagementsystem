import {Component, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {Language} from 'src/app/models/language.model';
import {User} from 'src/app/models/user.model';
import {LanguageService} from 'src/app/services/language.service';
import {AuthService} from 'src/shared/auth/auth.service';
import {LoadingService} from 'src/shared/loading.service';
import {LocalStorageService} from 'src/shared/local.storage.service';
import {ToastService} from 'src/shared/toast.service';
import {isNullOrUndefined} from 'util';
import {AuthorizationService} from '../authorization.service';
import {UserService} from '../../../services/user.service';
import {CommonService} from '../../../../shared/common.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
    user: User;
    onClickValidation: boolean;

    constructor(private loadingService: LoadingService, private authorizationService: AuthorizationService,
                private localStorageService: LocalStorageService, private toastService: ToastService, private authService: AuthService,
                private navController: NavController, private languageService: LanguageService, private menuCtrl: MenuController,
                private userService: UserService, private commonService: CommonService, private translate: TranslateService) {
    }

    ngOnInit() {
        this.menuCtrl.enable(false);
        this.user = new User();
        this.onClickValidation = false;
        this.user.userName = this.localStorageService.get('userName');
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.login(this.user);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.processToken(response.data);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async processToken(data: any) {
        data.token.expires_at = new Date(data.token.expires).getTime();
        this.localStorageService.set('userName', data.user.email);
        if (!this.user.rememberMe) {
            this.authService.user = data.user;
            this.authService.authToken = data.token;
            this.authService.roles = data.user.roles;
            await this.changeDefaultLanguge(data.user.languageId);
            await this.fetchMyDetail();
            this.navController.navigateRoot('landing');
            return;
        }
        this.localStorageService.set('token', JSON.stringify(data.token));
        this.localStorageService.set('user', JSON.stringify(data.user));
        this.localStorageService.set('roles', JSON.stringify(data.user.roles));
        await this.changeDefaultLanguge(data.user.languageId);
        await this.fetchMyDetail();
        this.navController.navigateRoot('landing');
    }

    async fetchMyDetail() {
        try {
            const response: RestResponse = await this.userService.fetchMyDetail().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            const user = response.data;
            if (isNullOrUndefined(user.addressdetails)) {
                user.addressdetails = new Array<any>();
            }
            user.pickupAddress = user.addressdetails.find(x => x.type === 'PICKUP_DELIVERY');
            delete user.addressdetails;
            this.authService.user = user;
            if (this.user.rememberMe) {
                this.localStorageService.set('user', JSON.stringify(user));
            }
            if (isNullOrUndefined(user.pickupAddress)) {
                this.openUserAccount(user);
            }
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async changeDefaultLanguge(languageId: string) {
        const language: Language = JSON.parse(this.localStorageService.get('DEFAULT_LANGUAGE'));
        if (!isNullOrUndefined(language) && language.id !== languageId) {
            try {
                await this.languageService.change(language.id);
            } catch (e) {
                this.toastService.show(e.message);
            }
        }
    }

    async openUserAccount(user: User) {
        const header = await this.translate.get('CONFIRMATION.ADDRESS.HEADER').toPromise();
        const body = await this.translate.get('CONFIRMATION.ADDRESS.BODY').toPromise();
        const confirmText = await this.translate.get('CONFIRMATION.ADDRESS.CONFIRM_BUTTON').toPromise();
        const cancelText = await this.translate.get('CONFIRMATION.ADDRESS.CANCEL_BUTTON').toPromise();
        this.commonService.confirm(header, body, cancelText, confirmText, this.onAddressCallback.bind(this), user);
    }

    async onAddressCallback(user: User) {
        this.commonService.addAddress = true;
        await this.navController.navigateForward('account/settings');
    }
}
