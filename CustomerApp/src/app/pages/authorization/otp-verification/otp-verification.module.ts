import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { BrMaskerModule } from 'br-mask';
import { CommonAppModule } from 'src/app/components/common.app.module';
import { OtpVerificationPage } from './otp-verification.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: OtpVerificationPage
            }
        ]),
        CommonAppModule,
        FontAwesomeModule,
        BrMaskerModule,
        TranslateModule
    ],
    declarations: [OtpVerificationPage]
})
export class OtpVerificationPageModule { }
