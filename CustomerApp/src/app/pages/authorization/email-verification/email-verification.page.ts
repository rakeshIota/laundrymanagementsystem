import {Component, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-email-verification',
    templateUrl: './email-verification.page.html',
    styleUrls: ['./email-verification.page.scss'],
})
export class EmailVerificationPage implements OnInit {

    uuid: string;

    constructor(private menuController: MenuController, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.menuController.enable(false);
        this.activatedRoute.queryParams.subscribe(params => {
            this.uuid = params.p;
        });
    }

}
