import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {EmailVerificationPage} from './email-verification.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: EmailVerificationPage
            }
        ]),
        CommonAppModule,
        TranslateModule,
        FontAwesomeModule
    ],
    declarations: [EmailVerificationPage]
})
export class EmailVerificationPageModule {
}
