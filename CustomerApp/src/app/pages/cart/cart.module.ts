import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {CartPage} from './cart.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: CartPage
            }
        ]),
        TranslateModule.forChild(),
        CommonAppModule
    ],
    declarations: [CartPage]
})
export class CartPageModule {
}
