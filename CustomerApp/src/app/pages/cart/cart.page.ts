import {Component, OnInit} from '@angular/core';
import {Header} from '../../models/header.model';
import {NavController} from '@ionic/angular';
import {LoadingService} from '../../../shared/loading.service';
import {ToastService} from '../../../shared/toast.service';
import {RestResponse} from '../../models/authorization.model';
import {CartService} from '../../services/cart.service';
import {CommonService} from '../../../shared/common.service';
import {TranslateService} from '@ngx-translate/core';
import {NavigationExtras} from '@angular/router';
import {isNullOrUndefined} from 'util';
import {DataService} from '../../../shared/data.service';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.page.html',
    styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
    header: Header;
    cart: any;
    selectedDeliveryType: string;
    appSettings: any;
    loading: boolean;
    type: string;

    constructor(private navController: NavController, private cartService: CartService, private loadingService: LoadingService,
                private toastService: ToastService, private commonService: CommonService, private translateService: TranslateService,
                private dataService: DataService) {
        this.header = new Header({
            title: 'BASKET_PAGE_HEADER',
            showMenu: true
        });
    }

    async ngOnInit() {
        this.appSettings = await this.dataService.getAppSettings();
        this.init();
    }

    async init() {
        await this.fetchCartDetail();
        if (isNullOrUndefined(this.cart)) {
            return;
        }
        this.cart.uiCartItems = this.commonService.filterRecords(this.cart.cartItems);
        this.selectedDeliveryType = this.cart.deliveryType;
    }

    async fetchCartDetail() {
        try {
            this.loading = true;
            const response: RestResponse = await this.cartService.fetch().toPromise();
            if (!response.status) {
                this.loading = false;
                this.toastService.show(response.message);
                return;
            }
            this.cart = response.data;
            this.loading = false;
        } catch (e) {
            this.loading = false;
            this.toastService.show(e.message);
        }
    }

    async decrease(cartItem) {
        if (cartItem.quantity <= 1) {
            return;
        }
        const quantity = cartItem.quantity - 1;
        cartItem.quantity = quantity;
        this.update(cartItem, quantity, false);
        this.dataService.decreaseCartItem();
    }

    async increase(cartItem) {
        const quantity = cartItem.quantity + 1;
        cartItem.quantity = quantity;
        this.update(cartItem, quantity, true);
        this.dataService.increaseCartItem();
    }

    async update(cartItem, quantity, add?) {
        try {
            const item = {} as any;
            item.productId = cartItem.product.id;
            item.serviceId = cartItem.service.id;
            item.quantity = quantity;
            const response: RestResponse = await this.cartService.update(item);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
        } catch (e) {
            cartItem.quantity = add ? cartItem.quantity - 1 : cartItem.quantity + 1;
            this.toastService.show(e.message);
        }
    }

    async removeItem(cartItem, index, cIndex) {
        const header = await this.translateService.get('CONFIRMATION_HEADER').toPromise();
        const body = await this.translateService.get('CONFIRMATION_TEXT').toPromise();
        const confirm = await this.translateService.get('CONFIRMATION_CONFIRM').toPromise();
        const cancel = await this.translateService.get('CONFIRMATION_CANCEL').toPromise();
        const data = {} as any;
        data.cartItem = cartItem;
        data.index = index;
        data.cIndex = cIndex;
        await this.commonService.confirm(header, body, cancel, confirm, this.removeItemCallback.bind(this), data);
    }

    async removeItemCallback(data) {
        this.dataService.decreaseCartItem(data.cartItem.quantity);
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.cartService.removeItem(data.cartItem.id);
            if (!response.status) {
                this.loadingService.hideLoading(loading);
                this.toastService.show(response.message);
                return;
            }
            this.cart.uiCartItems[data.index].splice(data.cIndex, 1);
            this.cart.cartItems = this.cart.cartItems.filter((cartItem) => {
                return cartItem.id !== data.cartItem.id;
            });
            this.cart.uiCartItems = this.commonService.filterRecords(this.cart.cartItems);
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    getDeliveryFees() {
        const sameDayRate = this.appSettings.samedayDeliveryRate;
        const expressRate = this.appSettings.expressDeliveryRate;
        const normalRate = this.appSettings.normalDeliveryRate;
        if (this.cart.deliveryType.toLowerCase() === 'sameday') {
            return this.getSubtotal() * Number(sameDayRate);
        }
        if (this.cart.deliveryType.toLowerCase() === 'express') {
            return this.getSubtotal() * Number(expressRate);
        }
        if (this.cart.deliveryType.toLowerCase() === 'normal') {
            return this.getSubtotal() * Number(normalRate);
        }
        return 0.00;
    }

    getSubtotal() {
        let total = 0.00;
        this.cart.cartItems.forEach((item) => {
            total += item.quantity * item.price;
        });
        return total;
    }

    getTotal() {
        return this.getSubtotal() + this.getDeliveryFees() + this.getTax();
    }

    getTax() {
        return this.getSubtotal() * 0.0;
    }

    async continue() {
        this.dataService.cartData = this.cart;
        await this.navController.navigateForward(['checkout']);
    }

    async onDeliveryChange(type: string) {
        if (type === 'NORMAL') {
            this.onDeliveryCallback(type);
        } else if (type === 'EXPRESS' || type === 'SAMEDAY') {
            const header = await this.translateService.get('CONFIRMATION.EXTRA_CHARGE.HEADER').toPromise();
            const body = await this.translateService.get('CONFIRMATION.EXTRA_CHARGE.BODY').toPromise();
            const confirmText = await this.translateService.get('CONFIRMATION.EXTRA_CHARGE.CONFIRM_BUTTON').toPromise();
            const cancelText = await this.translateService.get('CONFIRMATION.EXTRA_CHARGE.CANCEL_BUTTON').toPromise();
            this.commonService.confirmWithCallbackWithData(header, body, cancelText, confirmText, this.onDeliveryCallback.bind(this),
                this.onDeliveryCancelCallback.bind(this), type);
            return;
        }
    }

    onDeliveryCancelCallback() {
        this.selectedDeliveryType = this.cart.deliveryType;
    }

    async onDeliveryCallback(type) {
        const todayHr = new Date().getHours();
        const todayMin = new Date().getMinutes();
        if (type === 'SAMEDAY' && (((todayHr === 12 && todayMin >= 30) || (todayHr >= 13)) && ((todayHr === 16 && todayHr <= 30) || (todayHr < 16)))) {
            const header = await this.translateService.get('CONFIRMATION.EXPRESS.HEADER').toPromise();
            const body = await this.translateService.get('CONFIRMATION.EXPRESS.BODY').toPromise();
            const confirmText = await this.translateService.get('CONFIRMATION.EXPRESS.CONFIRM_BUTTON').toPromise();
            const cancelText = await this.translateService.get('CONFIRMATION.EXPRESS.CANCEL_BUTTON').toPromise();
            this.commonService.confirmWithCallback(header, body, cancelText, confirmText, this.expressDeliveryCallback.bind(this),
                this.normalDeliveryCallback.bind(this));
            return;
        }
        this.updateDelivery(type);
    }

    async updateDelivery(type) {
        try {
            const loading = this.loadingService.presentLoading();
            const data = {} as any;
            data.type = type;
            const response: RestResponse = await this.cartService.updateDelivery(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.cart.deliveryType = type;
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async expressDeliveryCallback() {
        this.selectedDeliveryType = 'EXPRESS';
        this.updateDelivery('EXPRESS');
    }

    async normalDeliveryCallback() {
        this.selectedDeliveryType = 'NORMAL';
        this.updateDelivery('NORMAL');
    }

    async loadItem(service) {
        const navigationExtras: NavigationExtras = {
            state: {
                name: service.name
            }
        };
        await this.navController.navigateRoot([`/category/${service.id}/items`], navigationExtras);
    }

    async removeServiceItem(index) {
        const header = await this.translateService.get('CONFIRMATION_HEADER').toPromise();
        const body = await this.translateService.get('CONFIRMATION_SERVICE_TEXT').toPromise();
        const confirm = await this.translateService.get('CONFIRMATION_CONFIRM').toPromise();
        const cancel = await this.translateService.get('CONFIRMATION_CANCEL').toPromise();
        const data = {} as any;
        data.index = index;
        await this.commonService.confirm(header, body, cancel, confirm, this.removeServiceCallback.bind(this), data);
    }

    async removeServiceCallback(data) {
        const serviceId = this.cart.uiCartItems[data.index][0].service.id;
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.cartService.removeService(serviceId);
            if (!response.status) {
                this.loadingService.hideLoading(loading);
                this.toastService.show(response.message);
                return;
            }
            this.cart.uiCartItems.splice(data.index, 1);
            this.cart.cartItems = this.cart.cartItems.filter((cartItem) => {
                return cartItem.service.id !== serviceId;
            });
            this.cart.uiCartItems = this.commonService.filterRecords(this.cart.cartItems);
            this.loadingService.hideLoading(loading);
            await this.dataService.fetchCartItemTotal();
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async openInfoPopup() {
        const header = await this.translateService.get('CONFIRMATION.INFO.HEADER').toPromise();
        const body = await this.translateService.get('CONFIRMATION.INFO.BODY').toPromise();
        const confirmText = await this.translateService.get('CONFIRMATION.INFO.CONFIRM_BUTTON').toPromise();
        this.commonService.info(header, body, confirmText, this.closeInfoPopup.bind(this));
    }

    async closeInfoPopup() {
    }
}
