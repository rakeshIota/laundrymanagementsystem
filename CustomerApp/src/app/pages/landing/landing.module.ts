import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {CommonAppModule} from 'src/app/components/common.app.module';
import {LandingPage} from './landing.page';
import {TranslateModule} from '@ngx-translate/core';
import {OrderModule} from 'ngx-order-pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: LandingPage
            }
        ]),
        TranslateModule.forChild(),
        CommonAppModule,
        OrderModule
    ],
    declarations: [LandingPage]
})
export class LandingPageModule {
}
