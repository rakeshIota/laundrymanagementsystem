import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Header} from 'src/app/models/header.model';
import {MenuController, NavController, Platform} from '@ionic/angular';
import {ProductService} from '../../services/product.service';
import {LoadingService} from '../../../shared/loading.service';
import {ToastService} from '../../../shared/toast.service';
import {environment} from '../../../environments/environment';
import {RestResponse} from '../../models/authorization.model';
import {OrderService} from '../../services/order.service';
import {User} from '../../models/user.model';
import {AuthService} from '../../../shared/auth/auth.service';
import {NavigationExtras} from '@angular/router';
import {PromotionService} from '../../services/promotion.service';
import {CommonService} from '../../../shared/common.service';
import {MapsAPILoader} from '@agm/core';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {TwilioVoiceService} from '../../services/twilio-voice.service';
import {ModalService} from '../../../shared/modal.service';
import {isNullOrUndefined} from 'util';
import {interval} from 'rxjs';
import {NetworkService} from '../../services/network.service';

declare const google: any;
declare const Twilio: any;

@Component({
    selector: 'app-landing',
    templateUrl: 'landing.page.html',
    styleUrls: ['landing.page.scss'],
})
export class LandingPage implements OnInit, OnDestroy {
    header: Header;
    slideOpts: any;
    orders: Array<any>;
    user: User;
    promotions: Array<any>;
    loading: boolean;
    orderInterval: any;
    @ViewChild('fab', {static: false}) fab: any;

    constructor(private navController: NavController, private productService: ProductService, private loadingService: LoadingService,
                private toastService: ToastService, private menuCtrl: MenuController, private orderService: OrderService,
                private authService: AuthService, private promotionService: PromotionService, private commonService: CommonService,
                private mapsAPILoader: MapsAPILoader, private androidPermissions: AndroidPermissions,
                private platform: Platform, private twilioVoiceService: TwilioVoiceService, private modalService: ModalService,
                private networkService: NetworkService) {
        this.header = new Header({
            title: '',
            showMenu: true
        });
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            height: 200
        };
    }

    async ngOnInit() {
        this.user = this.authService.getUser();
        this.menuCtrl.enable(true);
        this.loading = true;
        this.initCall();
        await this.mapsAPILoader.load();
        await this.fetchPromotions();
        this.orders = new Array<any>();
        await this.fetchActiveOrders();
        this.loading = false;
    }

    async fetchActiveOrders() {
        try {
            const data = {} as any;
            const response: RestResponse = await this.orderService.myActiveOrders(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.orders = new Array<any>();
            this.orders = response.data;
            this.autoRefreshProcess();
            if (this.orders.length > 1) {
                return;
            }
            this.orders.forEach((order) => {
                this.calculateDistance(order);
            });
            if (this.orders.length > 0 && this.commonService.hasTrackingPageLoaded) {
                this.commonService.hasTrackingPageLoaded = false;
                this.tracking(this.orders[0]);
                return;
            }
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    autoRefreshProcess() {
        console.log('Processing Auto Refresh');
        if (this.orders.length > 0 && isNullOrUndefined(this.orderInterval)) {
            this.orderInterval = interval(Number(60) * 1000)
                .subscribe((val) => {
                    this.fetchActiveOrders();
                });
        } else if (this.orders.length <= 0 && !isNullOrUndefined(this.orderInterval)) {
            this.orderInterval.unsubscribe();
        }
    }

    async fetchPromotions() {
        this.promotions = new Array<any>();
        try {
            const data = {} as any;
            const response: RestResponse = await this.promotionService.fetchAll(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.promotions = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    openSite(url) {
        window.open(environment.UiSiteUrl + url, '_system');
    }

    async tracking(iOrder: any) {
        const navigationExtras: NavigationExtras = {
            state: {
                order: iOrder,
                landing: true
            }
        };
        await this.navController.navigateForward([`/order/${iOrder.id}/tracking`], navigationExtras);
    }

    async loadLanding() {
        await this.navController.navigateForward([`/my/orders`]);
    }

    calculateDistance(order) {
        if (isNullOrUndefined(order.driverDetails) || isNullOrUndefined(order.deliveryAddress)) {
            order.time = 0.0;
            return;
        }
        const origin = new google.maps.LatLng(order.deliveryAddress.latitude, order.deliveryAddress.longitude);
        const destination = new google.maps.LatLng(order.driverDetails.latitude, order.driverDetails.longitude);
        const service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING',
            }, (response, status) => {
                order.time = undefined;
                if (response.rows.length <= 0) {
                    return;
                }
                const selectedElement = response.rows[0];
                if (selectedElement.elements.length <= 0) {
                    return;
                }
                order.time = selectedElement.elements[0];
            });

    }

    initCall() {
        this.twilioVoiceService.initializeTwilio();
    }

    async openLink(promotion: any) {
        window.open(promotion.url, '_system', 'location=yes');
    }

    ngOnDestroy(): void {
        console.log('Here');
        if (!isNullOrUndefined(this.orderInterval)) {
            this.orderInterval.unsubscribe();
        }
        if (!isNullOrUndefined(this.fab)) {
            this.fab.close();
        }
    }

    async openPage(url): Promise<void> {
        await this.navController.navigateRoot([url]);
    }
}
