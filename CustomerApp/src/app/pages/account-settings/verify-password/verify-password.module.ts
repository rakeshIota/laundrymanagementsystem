import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { VerifyPasswordPage } from './verify-password.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{ path: '', component: VerifyPasswordPage }]),
        ReactiveFormsModule,
        FontAwesomeModule,
        TranslateModule
    ],
    declarations: [VerifyPasswordPage]
})
export class VerifyPasswordPageModule { }
