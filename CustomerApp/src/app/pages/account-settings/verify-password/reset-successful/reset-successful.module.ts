import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ResetSuccessfulPage } from './reset-successful.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{ path: '', component: ResetSuccessfulPage }]),
        TranslateModule
    ],
    declarations: [ResetSuccessfulPage]
})
export class ResetSuccessfulPageModule { }
