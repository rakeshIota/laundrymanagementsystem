import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {NavController} from '@ionic/angular';
import {User} from 'src/app/models/user.model';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';
import {AuthorizationService} from '../../authorization/authorization.service';
import {RestResponse} from '../../../models/authorization.model';

@Component({
    selector: 'app-change-phone-number',
    templateUrl: './change-phone-number.page.html',
    styleUrls: ['./change-phone-number.page.scss'],
})
export class ChangePhoneNumberPage implements OnInit {
    user: User;
    onClickValidation: boolean;
    this;
    actionSheetOptions = {} as HTMLIonActionSheetElement;

    constructor(private navController: NavController, private authorizationService: AuthorizationService, private loadingService: LoadingService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.user = new User();
        this.user.countryCode = '+66';
        this.onClickValidation = false;
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.changePhoneNumber(this.user);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            await this.navController.navigateForward('otp-verification', {
                queryParams: {
                    countryCode: this.user.countryCode,
                    userMobileNo: this.user.mobileNo,
                    otpType: 'PHONE_NUMBER'
                }
            });
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    back() {
        this.navController.pop();
    }
}
