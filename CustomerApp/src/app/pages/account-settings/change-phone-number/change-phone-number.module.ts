import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { BrMaskerModule } from 'br-mask';
import { ChangePhoneNumberPage } from './change-phone-number.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{ path: '', component: ChangePhoneNumberPage }]),
        ReactiveFormsModule,
        FontAwesomeModule,
        TranslateModule,
        BrMaskerModule
    ],
    declarations: [ChangePhoneNumberPage]
})
export class ChangePhoneNumberPageModule { }
