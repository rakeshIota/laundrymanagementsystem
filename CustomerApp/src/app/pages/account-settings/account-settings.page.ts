import { Component, OnInit } from '@angular/core';
import { ActionSheetController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { RestResponse } from 'src/app/models/authorization.model';
import { Language } from 'src/app/models/language.model';
import { Address, User } from 'src/app/models/user.model';
import { LanguageService } from 'src/app/services/language.service';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/shared/auth/auth.service';
import { DataService } from 'src/shared/data.service';
import { LoadingService } from 'src/shared/loading.service';
import { LocalStorageService } from 'src/shared/local.storage.service';
import { ToastService } from 'src/shared/toast.service';
import { AuthorizationService } from '../authorization/authorization.service';
import { Camera } from '@ionic-native/camera/ngx';
import { UploadService } from '../../services/upload.service';
import { isNullOrUndefined } from 'util';
import { ModalService } from '../../../shared/modal.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CommonService } from '../../../shared/common.service';

@Component({
    selector: 'app-account-settings',
    templateUrl: './account-settings.page.html',
    styleUrls: ['./account-settings.page.scss'],
})
export class AccountSettingsPage implements OnInit {
    user: User;
    languages: Language[];
    nextOTP: number;
    timerInterval: any;
    pickupDeliveryAddresses: Array<Address>;
    hasChanged: boolean;
    hasUserAddressOpen: boolean;

    constructor(private navController: NavController, private dataService: DataService, private localStorageService: LocalStorageService,
        private loadingService: LoadingService, private userService: UserService, private toastService: ToastService,
        private languageService: LanguageService, private translateService: TranslateService, public authService: AuthService,
        private authorizationService: AuthorizationService, private actionSheetController: ActionSheetController,
        private camera: Camera, private uploadService: UploadService, private modalService: ModalService,
        private router: Router, private route: ActivatedRoute, private commonService: CommonService) {
    }

    ngOnInit() {
        this.hasUserAddressOpen = false;
        this.init();
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    this.user = this.authService.getUser();
                    if (!this.user.emailConfirmed) {
                        this.getUser();
                    }
                    if (this.hasUserAddressOpen) {
                        this.hasUserAddressOpen = false;
                        this.getUserAddresses();
                    }
                }
            });
        if (this.commonService.addAddress) {
            this.commonService.addAddress = false;
            this.addAddress('PICKUP_DELIVERY');
        }
    }

    async init() {
        this.nextOTP = 0;
        this.languages = [] as Language[];
        this.pickupDeliveryAddresses = new Array<Address>();
        this.getUserAddresses();
        this.updateAppSettings();
    }

    async updateAppSettings() {
        this.languages = await this.dataService.getLanguages();
        this.languages.map((x: Language) => {
            x.isSelected = x.id === this.user.languageId ? true : false;
            x.icon = x.id === this.user.languageId ? `../assets/icon/languages/${x.code}_white.svg` : `../assets/icon/languages/${x.code}.svg`;
        });
    }

    async getUser() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.userService.fetchMyDetail().toPromise();
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.user = response.data;
            this.authService.updateUserOnLocal(this.user);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async getUserAddresses() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.userService.getAddresses('PICKUP_DELIVERY').toPromise();
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            if (response.data.length > 0) {
                this.pickupDeliveryAddresses = response.data.filter(x => x.type === 'PICKUP_DELIVERY');
            }
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    onLanguageSelect(language: Language) {
        this.languages.map((x: Language) => {
            x.isSelected = language.code === x.code;
            x.icon = x.id === language.id ? `../assets/icon/languages/${x.code}_white.svg` : `../assets/icon/languages/${x.code}.svg`;
        });
        this.changeLanguage(language);
    }

    async changeLanguage(language: Language) {
        try {
            const response: RestResponse = await this.languageService.change(language.id);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.translateService.setDefaultLang(language.code);
            this.localStorageService.set('DEFAULT_LANGUAGE', JSON.stringify(language));
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async openUserSettings() {
        this.hasChanged = true;
        await this.navController.navigateForward('user-settings', { queryParams: { user: JSON.stringify(this.user) } });
    }

    async verifyPassword() {
        await this.navController.navigateForward('verify-password');
    }

    async verifyEmail() {
        await this.navController.navigateForward('verify-email');
    }

    async changePhoneNumber() {
        await this.navController.navigateForward('change-phone-number');
    }

    async back() {
        await this.navController.navigateRoot('landing');
    }

    async resend() {
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.authorizationService.requestEmailVerification();
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.nextOTP = 179;
            this.timerInterval = setInterval(this.handleTimeInterval.bind(this), 1000);
            this.toastService.info(response.message);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    handleTimeInterval() {
        this.nextOTP = Number(this.nextOTP) - 1;
        if (this.nextOTP > 0) {
            return;
        }
        if (this.timerInterval) {
            clearInterval(this.timerInterval);
        }
    }

    getDisplayTime() {
        const min = Math.floor(this.nextOTP / 60);
        const second = Math.floor(this.nextOTP % 60);
        return '0' + min + ':' + (second >= 10 ? second : '0' + second);
    }

    async openProfileImagePopup() {
        const takePhoto = await this.translateService.get('PAGES.ACCOUNT_SETTINGS.PROFILE_PIC.TAKE_PHOTO').toPromise();
        const photoGallery = await this.translateService.get('PAGES.ACCOUNT_SETTINGS.PROFILE_PIC.PHOTO_GALLERY').toPromise();
        const deletePhoto = await this.translateService.get('PAGES.ACCOUNT_SETTINGS.PROFILE_PIC.DELETE_PHOTO').toPromise();
        const cancel = await this.translateService.get('PAGES.ACCOUNT_SETTINGS.PROFILE_PIC.CANCEL').toPromise();
        const actionSheet = await this.actionSheetController.create({
            mode: 'ios',
            cssClass: 'profile-image-sheet',
            buttons: [{
                text: takePhoto,
                handler: this.openCamera.bind(this)
            }, {
                text: photoGallery,
                handler: this.openGallery.bind(this)
            }, {
                text: deletePhoto,
                handler: this.removeProfilePic.bind(this)
            }, {
                text: cancel,
                role: 'cancel'
            }]
        });
        await actionSheet.present();
    }

    async removeProfilePic() {
        if (isNullOrUndefined(this.user.profilePic)) {
            return;
        }
        const loading = this.loadingService.presentLoading();
        this.user.profilePic.isDeleted = true;
        await this.update(loading);
    }

    openCamera() {
        const galleryOptions = {
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            allowEdit: false,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(galleryOptions).then((imageData) => {
            const imageContent = 'data:image/jpeg;base64,' + imageData;
            const loading = this.loadingService.presentLoading();
            this.uploadFile(imageContent, loading);
        }, (err) => {
            this.toastService.show('Somethings went wrong while uploading. Please try again');
        });
    }

    openGallery() {
        const galleryOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            allowEdit: false,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(galleryOptions).then((imageData) => {
            const imageContent = 'data:image/jpeg;base64,' + imageData;
            const loading = this.loadingService.presentLoading();
            this.uploadFile(imageContent, loading);
        }, (err) => {
            this.toastService.show('Somethings went wrong while uploading. Please try again');
        });
    }

    uploadFile(imageContent, loading) {
        this.uploadService.uploadFile(imageContent)
            .then((res: any) => {
                if (res.responseCode !== 200) {
                    this.loadingService.hideLoading(loading);
                    this.toastService.show('Somethings went wrong while uploading. Please try again');
                    return;
                }
                const data = JSON.parse(res.response);
                const response = data[0] as any;
                this.user.profilePic = response;
                this.update(loading);
            }, (error) => {
                this.loadingService.hideLoading(loading);
                this.toastService.show('Somethings went wrong while uploading. Please try again');
            });
    }

    async update(loading) {
        try {
            const response: RestResponse = await this.userService.update(this.user);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.authService.updateUserOnLocal(this.user);
            setTimeout(() => {
                this.loadingService.hideLoading(loading);
            }, 1000);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async addAddress(type: string) {
        const address = new Address();
        address.type = type;
        await this.navController.navigateForward('user-address', { queryParams: { address: JSON.stringify(address) } });
        this.hasUserAddressOpen = true;
    }

    async editAddress(address: Address) {
        await this.navController.navigateForward('user-address', { queryParams: { address: JSON.stringify(address) } });
        this.hasUserAddressOpen = true;
    }

    addressCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.getUserAddresses();
    }
}
