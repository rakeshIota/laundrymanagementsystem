import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {RestResponse} from 'src/app/models/authorization.model';
import {User} from 'src/app/models/user.model';
import {UserService} from 'src/app/services/user.service';
import {AuthService} from 'src/shared/auth/auth.service';
import {CommonService} from 'src/shared/common.service';
import {LoadingService} from 'src/shared/loading.service';
import {ToastService} from 'src/shared/toast.service';

@Component({
    selector: 'app-user-settings',
    templateUrl: './user-settings.page.html',
    styleUrls: ['./user-settings.page.scss'],
})
export class UserSettingsPage implements OnInit {
    user: User;
    onClickValidation: boolean;
    maxDate: string;

    constructor(private userService: UserService, private loadingService: LoadingService, private toastService: ToastService,
                private navController: NavController, public commonService: CommonService, private activatedRoute: ActivatedRoute,
                private authService: AuthService) {
    }

    ngOnInit() {
        const today = new Date();
        const month = today.getMonth() + 1;
        const date = today.getDate() - 1;
        const iMonth = month <= 9 ? '0' + month : month;
        const iDate = date <= 9 ? '0' + date : date;
        this.maxDate = today.getFullYear() + '-' + iMonth + '-' + iDate;

        this.user = new User();
        this.init();
    }

    async init() {
        this.activatedRoute.queryParams.subscribe(params => {
            this.user = JSON.parse(params.user);
            this.user.dob = this.user.DOB;
        });
    }


    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        if (!isValid) {
            return;
        }
        const date = new Date(this.user.dob);
        date.setHours(12);
        date.setMinutes(0);
        date.setSeconds(0);
        this.user.dob = date;
        this.user.DOB = date;
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.userService.update(this.user);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.user.fullName = this.user.firstName + ' ' + this.user.lastName;
            this.authService.updateUserOnLocal(this.user);
            this.loadingService.hideLoading(loading);
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    back() {
        this.navController.pop();
    }
}
