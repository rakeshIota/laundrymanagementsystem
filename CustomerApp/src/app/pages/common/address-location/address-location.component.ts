import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, NgZone, OnInit, ViewChild} from '@angular/core';
import {ModalService} from '../../../../shared/modal.service';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {CommonService} from '../../../../shared/common.service';
import {ToastService} from '../../../../shared/toast.service';
import {AgmMap, AgmMarker, MapsAPILoader} from '@agm/core';
import {GeocodeService} from '../../../services/geocode.service';
import {Location} from '../../../dto/location';
import {Address} from '../../../models/user.model';
import {isNullOrUndefined} from 'util';
import {TranslateService} from '@ngx-translate/core';

declare const google: any;

@Component({
    selector: 'app-address-location',
    templateUrl: './address-location.component.html',
    styleUrls: ['./address-location.component.scss'],
})
export class AddressLocationComponent implements OnInit {
    searchText: string;
    @Input()
    data: Address;
    detail: any;
    geocoder: any;
    @ViewChild(AgmMap, {static: false})
    map: AgmMap;
    @ViewChild(AgmMap, {static: false})
    marker: AgmMarker;
    @ViewChild('search', {static: false}) searchElementRef: ElementRef;

    constructor(private modalService: ModalService, private diagnostic: Diagnostic, private locationAccuracy: LocationAccuracy,
                private location: Geolocation, private commonService: CommonService, private toastService: ToastService,
                private mapsAPILoader: MapsAPILoader, private geocodeService: GeocodeService, private ref: ChangeDetectorRef,
                private ngZone: NgZone, private translateService: TranslateService) {
    }

    async ngOnInit() {
        this.detail = {} as any;
        this.detail.zoom = 14;
        this.detail.isDefault = false;
        this.mapsAPILoader.load().then(() => {
            this.geocoder = new google.maps.Geocoder();
            this.init();
            this.fetchUserLocation();
        });
    }

    init() {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.setComponentRestrictions({'country': ['th', 'in']});
        autocomplete.addListener('place_changed', () => {
            this.ngZone.run(() => {
                const place = autocomplete.getPlace();
                this.detail.latitude = place.geometry.location.lat();
                this.detail.longitude = place.geometry.location.lng();
                this.ref.detectChanges();
            });
        });
    }

    async fetchUserLocation() {
        this.geocodeService.geocodeAddress(this.getUserAddress())
            .subscribe((location: Location) => {
                if (location.latitude === 0 || location.longitude === 0) {
                    this.detail.latitude = 13.753960;
                    this.detail.longitude = 100.502240;
                    this.detail.isDefault = true;
                    this.ref.detectChanges();
                    return;
                }
                this.detail.latitude = location.latitude;
                this.detail.longitude = location.longitude;
                this.detail.isDefault = false;
                this.ref.detectChanges();
            });
    }

    onMarkerDrag(event: any) {
        this.detail.latitude = event.coords.lat.toFixed(6);
        this.detail.longitude = event.coords.lng.toFixed(6);
        this.detail.isDefault = false;
    }

    async close() {
        this.modalService.addressLocationPopup = false;
        this.modalService.dismiss(false, null);
    }

    async process() {
        const errorText = await this.translateService.get('PAGES.COMMON.LOCATION_ERROR').toPromise();
        if (isNullOrUndefined(this.detail.latitude) || isNullOrUndefined(this.detail.longitude) || this.detail.isDefault) {
            this.toastService.show(errorText);
            return;
        }
        this.modalService.addressLocationPopup = false;
        this.modalService.dismiss(true, this.detail);
    }

    async getUserLocation() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            .then(() => {
                this.wait(500).then(() => this.fetchCurrentLocation(true));
            }, () => {
                this.wait(500).then(() => this.fetchCurrentLocation(false));
            });
    }

    wait(time) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, time);
        });
    }

    async fetchCurrentLocation(isGps) {
        const options = {
            enableHighAccuracy: isGps,
            timeout: 20000,
            maximumAge: 0
        } as any;
        try {
            const resp: any = await this.location.getCurrentPosition(options);
            const position = {} as any;
            position.lng = resp.coords.latitude;
            position.lat = resp.coords.longitude;
            this.detail.latitude = position.lat;
            this.detail.longitude = position.lng;
            this.detail.isDefault = false;
            this.ref.detectChanges();
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    getUserAddress(): string {
        return `${this.data.houseNumber} ${this.data.streetNumber} ${this.data.buildingName} ${this.data.floor} ${this.data.unit} ${this.data.subDistrictName} ${this.data.districtName} ${this.data.postalCode}`;
    }
}
