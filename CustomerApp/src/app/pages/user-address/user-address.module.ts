import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {UserAddressPage} from './user-address.page';
import {RouterModule} from '@angular/router';
import {PendingChangesGuard} from '../../../shared/pending.guard';
import {TranslateModule} from '@ngx-translate/core';
import {CommonAppModule} from '../../components/common.app.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrMaskerModule} from 'br-mask';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: UserAddressPage,
            canDeactivate: [PendingChangesGuard]
        }]),
        CommonAppModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        TranslateModule,
        BrMaskerModule
    ],
    declarations: [UserAddressPage]
})
export class UserAddressPageModule {
}
