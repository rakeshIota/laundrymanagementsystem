import {Component, Input, OnInit} from '@angular/core';
import {Address} from '../../models/user.model';
import {District} from '../../models/district.model';
import {SubDistrict} from '../../models/sub-district.model';
import {TranslateService} from '@ngx-translate/core';
import {ToastService} from '../../../shared/toast.service';
import {CommonService} from '../../../shared/common.service';
import {AddressService} from '../../services/address.service';
import {LoadingService} from '../../../shared/loading.service';
import {ModalService} from '../../../shared/modal.service';
import {isNullOrUndefined} from 'util';
import {RestResponse} from '../../models/authorization.model';
import {AddressLocationComponent} from '../common/address-location/address-location.component';
import {ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-user-address',
    templateUrl: './user-address.page.html',
    styleUrls: ['./user-address.page.scss'],
})
export class UserAddressPage implements OnInit {

    @Input()
    data: Address;
    onClickValidation: boolean;
    address: Address;
    actionSheetOptions: HTMLIonActionSheetElement;
    districts: Array<District>;
    subDistricts: Array<SubDistrict>;
    provinces: Array<any>;
    copyAddress: string;
    mobileZeroError: boolean;
    aMobileZeroError: boolean;

    constructor(private translateService: TranslateService, private toastService: ToastService, private commonService: CommonService,
                private addressService: AddressService, private loadingService: LoadingService, private modalService: ModalService,
                private activatedRoute: ActivatedRoute, private navController: NavController) {
        this.onClickValidation = false;
        this.address = new Address();
    }

    async ngOnInit() {
        this.actionSheetOptions = {} as HTMLIonActionSheetElement;
        const selectValue = await this.translateService.get('SELECT_LABEL').toPromise();
        this.actionSheetOptions.header = selectValue;
        this.activatedRoute.queryParams.subscribe(params => {
            const loading = this.loadingService.presentLoading();
            this.address = JSON.parse(params.address);
            this.init(loading);
        });
    }

    async init(loading: any) {
        if (isNullOrUndefined(this.address)) {
            return;
        }
        if (isNullOrUndefined(this.address.residenceType)) {
            this.address.residenceType = 'HOME';
        }
        await this.fetchDistricts();
        await this.fetchProvince();
        if (this.address.provinceId) {
            await this.onProvinceSelection(this.address.provinceId);
        }
        if (this.address.districtId) {
            await this.onDistrictSelection(this.address.districtId, false);
            await this.onSubDistrictSelection(this.address.subDistrictId);
        }
        this.copyAddress = JSON.stringify(this.address);
        this.loadingService.hideLoading(loading);
    }

    async fetchProvince() {
        try {
            this.provinces = new Array<any>();
            const response: RestResponse = await this.addressService.fetchProvince().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.provinces = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    onProvinceSelection(provinceId: string) {
        if (isNullOrUndefined(this.provinces)) {
            return;
        }
        const selectedProvince = this.provinces.find(x => x.id === provinceId);
        if (!isNullOrUndefined(selectedProvince)) {
            this.address.province = selectedProvince.name;
        }
    }

    async fetchDistricts() {
        try {
            this.districts = new Array<District>();
            const response: RestResponse = await this.addressService.fetchDistricts().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.districts = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async onDistrictSelection(districtId: string, ui: boolean) {
        try {
            const selectedDistrict = this.districts.find(x => x.id === districtId);
            if (!isNullOrUndefined(selectedDistrict)) {
                this.address.district = selectedDistrict.name;
            }
            if (ui) {
                this.address.subDistrict = undefined;
            }
            this.subDistricts = new Array<District>();
            const response: RestResponse = await this.addressService.fetchSubDistricts(districtId).toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.subDistricts = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async onSubDistrictSelection(subDistrictId: string) {
        const selectedSubDistrict = this.subDistricts.find(x => x.id === subDistrictId);
        if (!isNullOrUndefined(selectedSubDistrict)) {
            this.address.subDistrict = selectedSubDistrict.name;
        }
    }

    async onSubmit(isValid) {
        this.onClickValidation = !isValid;
        this.mobileZeroError = false;
        this.aMobileZeroError = false;
        if (!isValid) {
            return;
        }
        if (!isNullOrUndefined(this.address.alternatePhoneNo) && this.address.alternatePhoneNo.trim() !== ''
            && this.address.alternatePhoneNo.toString().startsWith('0')) {
            this.aMobileZeroError = true;
        }
        if (this.mobileZeroError || this.aMobileZeroError) {
            this.onClickValidation = true;
            return;
        }
        if (isNullOrUndefined(this.address.latitude) || isNullOrUndefined(this.address.longitude)) {
            const gpsLocationText = await this.translateService.get('PAGES.COMMON.GPS_LOCATION_REQUIRED').toPromise();
            this.toastService.info(gpsLocationText);
            return;
        }
        const loading = this.loadingService.presentLoading();
        const isValidPostalCode = await this.validatePostalCode();
        if (isValidPostalCode === 'FAILED') {
            this.loadingService.hideLoading(loading);
            return;
        }
        if (isValidPostalCode === 'OUT_OF_SERVICE') {
            this.loadingService.hideLoading(loading);
            const headerText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_HEADER').toPromise();
            const bodyText = await this.translateService.get('PAGES.COMMON.OUTSIDE_SERVICE_TEXT').toPromise();
            const noText = await this.translateService.get('CONFIRMATION_NO').toPromise();
            const yesText = await this.translateService.get('CONFIRMATION_YES').toPromise();
            this.commonService.confirm(headerText, bodyText,
                noText, yesText, this.onYesCallback.bind(this), null);
            return;
        }
        this.processAddress(loading);
    }

    onYesCallback() {
        const loading = this.loadingService.presentLoading();
        this.processAddress(loading);
    }

    async validatePostalCode(): Promise<string> {
        try {
            const response: RestResponse = await this.addressService.validatePostalCode(this.address.postalCode);
            if (!response.status) {
                return 'OUT_OF_SERVICE';
            }
            return 'VALID';
        } catch (e) {
            this.toastService.show(e.message);
            return 'FAILED';
        }
    }

    async processAddress(loading) {
        try {
            const method = this.address.id ? 'update' : 'save';
            const response: RestResponse = await this.addressService[method](this.address);
            this.loadingService.hideLoading(loading);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.copyAddress = JSON.stringify(this.address);
            this.navController.back();
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async close() {
        this.navController.back();
    }

    async openLocationPopup() {
        this.modalService.addressLocationPopup = true;
        const model = await this.modalService.presentModal(AddressLocationComponent, {data: this.address});
        this.modalService.onComplete(model, this.locationCallback.bind(this));
    }

    async locationCallback(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        this.address.longitude = data.data.longitude;
        this.address.latitude = data.data.latitude;
        const successText = await this.translateService.get('PAGES.COMMON.LOCATION_SUCCESS').toPromise();
        this.toastService.info(successText);
    }

    hasValidAddress() {
        if (this.address.residenceType === 'BUILDING') {
            return this.isNonEmpty(this.address.buildingName)
                && this.isNonEmpty(this.address.floor)
                && this.isNonEmpty(this.address.unit)
                && this.isNonEmpty(this.address.streetNumber)
                && this.isNonEmpty(this.address.districtId)
                && this.isNonEmpty(this.address.subDistrictId)
                && this.isNonEmpty(this.address.postalCode);
        }
        return this.isNonEmpty(this.address.houseNumber)
            && this.isNonEmpty(this.address.streetNumber)
            && this.isNonEmpty(this.address.districtId)
            && this.isNonEmpty(this.address.subDistrictId)
            && this.isNonEmpty(this.address.postalCode);
    }

    isNonEmpty(value) {
        return !isNullOrUndefined(value) && value.trim() !== '';
    }

    hasChanged(): boolean {
        return this.copyAddress !== JSON.stringify(this.address);
    }

    onResidenceChanged(type) {
        /*if (type === 'HOME') {
            this.address.buildingName = undefined;
            this.address.floor = undefined;
            this.address.unit = undefined;
            this.address.unit = undefined;
        } else if (type === 'BUILDING') {
            this.address.houseNumber = undefined;
        }*/
    }
}
