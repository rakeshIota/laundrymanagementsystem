import {Component, OnInit} from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import * as lodash from 'lodash';
import {Language} from 'src/app/models/language.model';
import {DataService} from 'src/shared/data.service';
import {LoadingService} from 'src/shared/loading.service';
import {LocalStorageService} from 'src/shared/local.storage.service';
import {isNullOrUndefined} from 'util';
import {AuthService} from '../../../shared/auth/auth.service';

@Component({
    selector: 'app-language',
    templateUrl: './language.page.html',
    styleUrls: ['./language.page.scss'],
})
export class LanguagePage implements OnInit {
    languages: Language[];

    constructor(private translate: TranslateService, private localStorageService: LocalStorageService, private navCtrl: NavController,
                private loadingService: LoadingService, private dataService: DataService, private authService: AuthService,
                private menuController: MenuController) {
    }

    ngOnInit() {
        if (this.authService.getToken() === null) {
            this.menuController.enable(false);
        }
        this.languages = [] as Language[];
        this.fetchAllLanguages();
    }

    async fetchAllLanguages() {
        const loading = this.loadingService.presentLoading();
        this.languages = await this.dataService.getLanguages();
        this.loadingService.hideLoading(loading);
    }

    onLanguageSelect(language: Language) {
        this.languages.map((x: Language) => {
            x.isSelected = language.code === x.code;
        });
    }

    proceed() {
        const language: Language = lodash.find(this.languages, {isSelected: true});
        if (isNullOrUndefined(language)) {
            alert('Please select any one language');
            return;
        }
        this.localStorageService.set('DEFAULT_LANGUAGE', JSON.stringify(language));
        this.translate.setDefaultLang(language.code);
        this.navCtrl.navigateRoot('login');
    }
}
