import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Header} from '../../../models/header.model';
import {MenuController, NavController} from '@ionic/angular';
import {ProductService} from '../../../services/product.service';
import {LoadingService} from '../../../../shared/loading.service';
import {ToastService} from '../../../../shared/toast.service';
import {RestResponse} from '../../../models/authorization.model';
import {UserService} from '../../../services/user.service';
import {TranslateService} from '@ngx-translate/core';
import {CommonService} from '../../../../shared/common.service';
import {NavigationExtras} from '@angular/router';
import {PromotionService} from '../../../services/promotion.service';
import {isNullOrUndefined} from 'util';

@Component({
    selector: 'app-service',
    templateUrl: './service.page.html',
    styleUrls: ['./service.page.scss'],
})
export class ServicePage implements OnInit, OnDestroy {
    header: Header;
    categories: Array<any>;
    basicDetail: any;
    slideOpts: any;
    promotions: Array<any>;
    @ViewChild('fab', {static: false}) fab: any;

    constructor(private navController: NavController, private productService: ProductService, private loadingService: LoadingService,
                private toastService: ToastService, private menuCtrl: MenuController, private userService: UserService,
                private translate: TranslateService, private commonService: CommonService, private promotionService: PromotionService) {
        this.header = new Header({
            title: '',
            showMenu: true
        });
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            height: 200
        };
    }

    async ngOnInit() {
        this.fetchPromotions();
        this.loadUserDetail();
        const loading = this.loadingService.presentLoading();
        await this.loadCategories();
        this.loadingService.hideLoading(loading);
    }

    async fetchPromotions() {
        this.promotions = new Array<any>();
        try {
            const data = {} as any;
            const response: RestResponse = await this.promotionService.fetchAll(data);
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.promotions = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async loadCategories() {
        this.categories = new Array<any>();
        try {
            const response: RestResponse = await this.productService.fetchCategories().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.categories = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async loadUserDetail() {
        try {
            const response: RestResponse = await this.userService.fetchBasicDetail();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.basicDetail = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async onCategorySelect(category) {
        if (!this.basicDetail.emailVerifed) {
            this.openEmailNotVerifiedPopup();
            return;
        }
        const navigationExtras: NavigationExtras = {
            state: {
                name: category.name
            }
        };
        await this.navController.navigateRoot([`/category/${category.id}/items`], navigationExtras);
    }

    async openEmailNotVerifiedPopup() {
        const header = await this.translate.get('CONFIRMATION.EMAIL_NOT_VERIFY.HEADER').toPromise();
        const body = await this.translate.get('CONFIRMATION.EMAIL_NOT_VERIFY.BODY').toPromise();
        const confirmText = await this.translate.get('CONFIRMATION.EMAIL_NOT_VERIFY.CONFIRM_BUTTON').toPromise();
        this.commonService.info(header, body, confirmText, this.onEmailVerifyCallback.bind(this), null);
    }

    async onEmailVerifyCallback() {
        this.navController.navigateRoot(`/account/settings`);
    }

    async openLink(promotion: any) {
        window.open(promotion.url, '_system', 'location=yes');
    }

    ngOnDestroy(): void {
        if (!isNullOrUndefined(this.fab)) {
            this.fab.close();
        }
    }
}
