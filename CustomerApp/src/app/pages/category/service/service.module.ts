import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ServicePage} from './service.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {OrderModule} from 'ngx-order-pipe';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([{
            path: '',
            component: ServicePage
        }]),
        TranslateModule.forChild(),
        CommonAppModule,
        OrderModule
    ],
    declarations: [ServicePage]
})
export class ServicePageModule {
}
