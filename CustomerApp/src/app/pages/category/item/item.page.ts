import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from '../../../../shared/data.service';
import { LoadingService } from '../../../../shared/loading.service';
import { ToastService } from '../../../../shared/toast.service';
import { RestResponse } from '../../../models/authorization.model';
import { Header } from '../../../models/header.model';
import { CartService } from '../../../services/cart.service';
import { ProductService } from '../../../services/product.service';

@Component({
    selector: 'app-item',
    templateUrl: './item.page.html',
    styleUrls: ['./item.page.scss'],
})
export class ItemPage implements OnInit {
    items: Array<any>;
    categoryId: string;
    header: Header;
    view: string;
    request: any;
    loading: boolean;
    categoryName: string;
    lastAddedDate: Date;
    filters: any[];

    constructor(private route: ActivatedRoute, private navController: NavController, private productService: ProductService,
        private loadingService: LoadingService, private toastService: ToastService, private cartService: CartService,
        private dataService: DataService, private router: Router, private translateService: TranslateService) {
        this.categoryId = this.route.snapshot.paramMap.get('categoryId');
        this.filters = [] as any[];
    }

    async ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.categoryName = this.router.getCurrentNavigation().extras.state.name;
                }
            });
        this.view = 'ADD';
        this.request = {} as any;
        this.request.type = 'ALL';
        this.header = new Header({
            title: 'SELECT_ITEM_PAGE_HEADER',
            showMenu: true
        });
        this.loadItems();
        this.filters = await this.dataService.fetchProductFilters();
    }

    async loadItems() {
        this.loading = true;
        this.items = new Array<any>();
        try {
            const response: RestResponse = await this.productService.fetchItemsByCategory(this.categoryId, this.request.type).toPromise();
            if (!response.status) {
                this.loading = false;
                this.toastService.show(response.message);
                return;
            }
            this.items = response.data;
            this.items.forEach((item) => {
                item.quantity = 0;
            });
            this.loading = false;
        } catch (e) {
            this.loading = false;
            this.toastService.show(e.message);
        }
    }

    decrease(item) {
        if (item.quantity <= 0) {
            return;
        }
        item.quantity = item.quantity - 1;
        this.lastAddedDate = new Date();
        this.isValid();
    }

    increase(item) {
        item.quantity = item.quantity + 1;
        this.lastAddedDate = new Date();
        this.isValid();
    }

    async back() {
        await this.navController.navigateRoot('categories');
    }

    async addToBasket() {
        const selectedItems = this.items.filter(x => x.quantity > 0);
        if (selectedItems.length <= 0) {
            this.toastService.show('Please select any item');
            return;
        }
        const items = [] as any;
        selectedItems.forEach((item) => {
            items.push({
                productId: item.id,
                quantity: item.quantity,
                serviceId: this.categoryId
            });
        });
        const loading = this.loadingService.presentLoading();
        try {
            const response: RestResponse = await this.cartService.add(items);
            if (!response.status) {
                this.loadingService.hideLoading(loading);
                this.toastService.show(response.message);
                return;
            }
            await this.dataService.fetchCartItemTotal();
            this.loadingService.hideLoading(loading);
            this.items.forEach((item) => {
                item.quantity = 0;
            });
            this.view = 'VIEW';
        } catch (e) {
            this.loadingService.hideLoading(loading);
            this.toastService.show(e.message);
        }
    }

    async viewBasket() {
        await this.navController.navigateRoot('cart');
    }

    isValid() {
        const selectedItems = this.items.filter(x => x.quantity > 0);
        if (selectedItems.length > 0) {
            this.view = 'ADD';
        }
        return selectedItems.length > 0;
    }

    async onTypeChanged(type) {
        this.request.type = type;
        await this.loadItems();
    }
}
