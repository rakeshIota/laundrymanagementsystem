import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {ItemPage} from './item.page';
import {RouterModule} from '@angular/router';
import {CommonAppModule} from '../../../components/common.app.module';
import {TranslateModule} from '@ngx-translate/core';
import {OrderModule} from 'ngx-order-pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: ItemPage
            }
        ]),
        TranslateModule.forChild(),
        CommonAppModule,
        OrderModule
    ],
    declarations: [ItemPage]
})
export class ItemPageModule {
}
