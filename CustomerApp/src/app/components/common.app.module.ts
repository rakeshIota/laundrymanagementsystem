import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {BrMaskerModule} from 'br-mask';
import {OrderModule} from 'ngx-order-pipe';
import {HeaderComponent} from 'src/app/components/header/header.component';
import {MustMatchDirective} from './must.match.directive';
import {PopoverComponent} from './popover/popover.component';
import {AddressLocationComponent} from '../pages/common/address-location/address-location.component';
import {AgmCoreModule} from '@agm/core';
import {TermAndConditionComponent} from './term-and-condition/term-and-condition.component';
import {CallingComponent} from '../pages/call/calling/calling.component';
import {LinkyModule} from 'angular-linky';
import {RatingComponent} from '../pages/order/rating/rating.component';
import {StarRatingModule} from 'ionic4-star-rating';

const pages: Array<any> = [
    HeaderComponent,
    PopoverComponent,
    AddressLocationComponent,
    CallingComponent,
    TermAndConditionComponent,
    RatingComponent
];

@NgModule({
    declarations: [pages, MustMatchDirective],
    imports: [
        FormsModule,
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        TranslateModule,
        OrderModule,
        BrMaskerModule,
        AgmCoreModule,
        LinkyModule,
        StarRatingModule
    ],
    exports: [pages, MustMatchDirective],
    entryComponents: [PopoverComponent]
})

export class CommonAppModule {

}
