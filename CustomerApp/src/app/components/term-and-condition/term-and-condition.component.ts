import {Component, OnInit} from '@angular/core';
import {ModalService} from '../../../shared/modal.service';

@Component({
    selector: 'app-term-and-condition',
    templateUrl: './term-and-condition.component.html',
    styleUrls: ['./term-and-condition.component.scss'],
})
export class TermAndConditionComponent implements OnInit {
    scrollDepthTriggered: boolean;

    constructor(private modalService: ModalService) {
        this.scrollDepthTriggered = false;
    }

    ngOnInit() {
    }

    async logScrolling($event) {
        // only send the event once
        if (this.scrollDepthTriggered) {
            return;
        }
        if ($event.target.localName !== 'ion-content') {
            return;
        }
        const scrollElement = await $event.target.getScrollElement();
        const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
        const currentScrollDepth = $event.detail.scrollTop;
        const targetPercent = 99;
        const triggerDepth = ((scrollHeight / 100) * targetPercent);
        if (currentScrollDepth > triggerDepth) {
            this.scrollDepthTriggered = true;
        }
    }

    accept() {
        this.modalService.dismiss(this.scrollDepthTriggered, null);
    }
}
