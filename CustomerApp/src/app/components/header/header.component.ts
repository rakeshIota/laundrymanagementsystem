import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavController} from '@ionic/angular';
import {Header} from 'src/app/models/header.model';
import {CartService} from '../../services/cart.service';
import {RestResponse} from '../../models/authorization.model';
import {ToastService} from '../../../shared/toast.service';
import {DataService} from '../../../shared/data.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
    @Input() header: Header;
    @Output() closeModalEmitter;


    constructor(private navController: NavController, public dataService: DataService) {
        this.closeModalEmitter = new EventEmitter<boolean>();
    }

    ngOnInit() {
        this.dataService.fetchCartItemTotal();
    }

    dismissModal() {
        this.closeModalEmitter.emit(true);
    }

    loadCart() {
        this.navController.navigateRoot('cart');
    }
}
