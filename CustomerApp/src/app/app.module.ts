import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fab} from '@fortawesome/free-brands-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Toast} from '@ionic-native/toast/ngx';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {BrMaskerModule} from 'br-mask';
import {OrderModule} from 'ngx-order-pipe';
import {CommonAppModule} from 'src/app/components/common.app.module';
import {HttpAuthInterceptor} from 'src/shared/http.interceptor';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Camera} from '@ionic-native/camera/ngx';
import {FileTransfer} from '@ionic-native/file-transfer/ngx';
import {AddressLocationComponent} from './pages/common/address-location/address-location.component';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {LocationAccuracy} from '@ionic-native/location-accuracy/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AgmCoreModule} from '@agm/core';
import {TermAndConditionComponent} from './components/term-and-condition/term-and-condition.component';
import {AgmDirectionModule} from 'agm-direction';
import {AppVersion} from '@ionic-native/app-version/ngx';
import {FirebaseX} from '@ionic-native/firebase-x/ngx';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';
import {Device} from '@ionic-native/device/ngx';
import {CallingComponent} from './pages/call/calling/calling.component';
import {UniqueDeviceID} from '@ionic-native/unique-device-id/ngx';
import {File} from '@ionic-native/file/ngx';
import {LinkyModule} from 'angular-linky';
import {RatingComponent} from './pages/order/rating/rating.component';
import {Network} from '@ionic-native/network/ngx';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [
        AddressLocationComponent,
        TermAndConditionComponent,
        CallingComponent,
        RatingComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        CommonAppModule,
        FontAwesomeModule,
        BrMaskerModule,
        OrderModule,
        LinkyModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD3brvWQZQHeqmYjNNyG5y0xhgWun6cpj4',
            libraries: ['places', 'geometry']
        }),
        AgmDirectionModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpAuthInterceptor,
            multi: true
        },
        Toast,
        Camera,
        Geolocation,
        Diagnostic,
        LocationAccuracy,
        AppVersion,
        FirebaseX,
        AndroidPermissions,
        Device,
        UniqueDeviceID,
        File,
        FileTransfer,
        Network,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(library: FaIconLibrary) {
        library.addIconPacks(fas);
        library.addIconPacks(far);
        library.addIconPacks(fab);
    }
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
