import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'src/shared/local.storage.service';
import { isNullOrUndefined } from 'util';
import { AuthService } from '../shared/auth/auth.service';
import { DataService } from '../shared/data.service';
import { EventService } from '../shared/event.service';
import { Language } from './models/language.model';
import { CommonService } from '../shared/common.service';
import { LoginPage } from './pages/authorization/login/login.page';
import { LandingPage } from './pages/landing/landing.page';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { OrderConfirmationPage } from './pages/order/confirmation/order-confirmation.page';
import { TwilioVoiceService } from './services/twilio-voice.service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { ModalService } from '../shared/modal.service';
import { CallingComponent } from './pages/call/calling/calling.component';
import { TwilioService } from './services/twilio.service';
import { ToastService } from '../shared/toast.service';
import { NetworkService } from './services/network.service';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { UserService } from './services/user.service';
import { LoadingService } from '../shared/loading.service';

declare const Twilio: any;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    @ViewChild(IonRouterOutlet, { static: false }) routerOutlet: IonRouterOutlet;
    exitShown: boolean;
    versionCode: any;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private eventService: EventService,
        private navController: NavController,
        private dataService: DataService,
        private translate: TranslateService,
        public authService: AuthService,
        private localStorageService: LocalStorageService,
        private commonService: CommonService,
        private appVersion: AppVersion,
        private twilioVoiceService: TwilioVoiceService,
        private firebaseX: FirebaseX,
        private modalService: ModalService,
        private twilioService: TwilioService,
        private toastService: ToastService,
        private networkService: NetworkService,
        private uniqueDeviceID: UniqueDeviceID,
        private androidPermissions: AndroidPermissions,
        private userService: UserService,
        private loadingService: LoadingService
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleLightContent();
            setTimeout(() => {
                this.splashScreen.hide();
            }, 1000);
            this.backButtonHandlerEvent();
            this.init();
            if (this.platform.is('cordova')) {
                this.initializeCallReceiver();
                this.initializeNotificationReceiver();
            }
        });
    }

    async init() {
        this.nextInit();
        this.versionCode = await this.appVersion.getVersionNumber();
        this.commonService.hasTrackingPageLoaded = true;
        this.dataService.init();
        this.networkService.init();
        this.eventService.event.subscribe((response: any) => {
            if (!isNullOrUndefined(response)) {
                this.navController.navigateRoot('login');
            }
        });
        const uuid: string = await this.uniqueDeviceID.get();
        await this.checkPermissions();
    }

    async nextInit() {
        const defaultLanguage: Language = JSON.parse(this.localStorageService.get('DEFAULT_LANGUAGE'));
        if (isNullOrUndefined(defaultLanguage)) {
            this.translate.setDefaultLang('en');
            await this.navController.navigateRoot('language');
            return;
        }
        this.translate.setDefaultLang(defaultLanguage.code);
    }

    async checkPermissions() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
            result => '',
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO)
        );
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.RECORD_AUDIO,
        this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    }

    initializeCallReceiver() {
        Twilio.TwilioVoiceClient.callinvitereceived((call) => {
            if (this.twilioVoiceService.onCall) {
                return;
            }
            this.twilioVoiceService.onCall = false;
            this.call(call);
        });
    }

    async call(call) {
        try {
            const input = {} as any;
            input.from = call;
            input.type = 'INCOMING_CALL';
            this.modalService.callingPopup = true;
            const model = await this.modalService.presentModal(CallingComponent, { data: input });
            this.modalService.onComplete(model, this.onCallDisconnect.bind(this));
        } catch (e) {
            console.log(e);
        }
    }

    async onCallDisconnect(data) {
        if (isNullOrUndefined(data) || !data.complete) {
            return;
        }
        const params = {} as any;
        params.toId = this.authService.getUser().id;
        params.status = 'IDLE';
        await this.twilioService.updateCallStatus(params);
    }

    initializeNotificationReceiver() {
        this.firebaseX.onMessageReceived().subscribe((resp) => {
            console.log(resp);
            if (resp.tap) {
                if (resp.type && ([
                    'ORDER_PLACED',
                    'ASSIGNED_DRIVER_FOR_COLLECTION',
                    'AWAITING_COLLECTION',
                    'COLLECTED',
                    'CLEANING_IN_PROGRESS',
                    'CLEANING_COMPLETED',
                    'ASSIGNED_DRIVER_FOR_DELIVERY',
                    'ON_THE_WAY',
                    'DELIVERED',
                    'CANCELLED',
                    'ORDER_MODIFIED_BY_OPERATION_MANAGER',
                    'ORDER_UPDATE'
                ].indexOf(resp.type) !== -1)) {
                    this.navController.navigateForward(`notification/order/${resp.targetId}/detail`);
                } else if (resp.type && (resp.type === 'NEW_DEVICE_LOGIN') && this.platform.is('android')) {
                    Twilio.TwilioVoiceClient.destroy();
                } else if (resp.type && (resp.type === 'ORDER_CHAT_NOTIFICATION')) {
                    this.navController.navigateForward([`order/${resp.targetId}/detail`], {
                        queryParams: {
                            chat: true
                        }
                    });
                } else if (resp.type && (resp.type === 'CALL_PICKED')) {
                    this.eventService.publish({ key: 'twilio:call:answered', value: resp.targetId });
                }
            } else {
                if (!isNullOrUndefined(resp.twi_message_type) && resp.twi_message_type === 'twilio.voice.call') {
                    console.log('Incoming Call');
                } else if (!isNullOrUndefined(resp.twi_message_type) && resp.twi_message_type === 'twilio.voice.cancel') {
                    if (this.twilioVoiceService.callStatus === 'INCOMING') {
                        this.modalService.dismiss(true, null);
                    }
                } else if (resp.type && (resp.type === 'ORDER_CHAT_NOTIFICATION')) {
                    this.navController.navigateForward([`order/${resp.targetId}/detail`], {
                        queryParams: {
                            chat: true
                        }
                    });
                } else if (resp.type && (resp.type === 'CALL_PICKED')) {
                    this.eventService.publish({ key: 'twilio:call:answered', value: resp.targetId });
                } else {
                    this.toastService.info(resp.body);
                }
            }
        });
    }

    backButtonHandlerEvent() {
        this.platform.backButton
            .subscribe((data) => {
                const isMainPage = this.routerOutlet.activatedRoute.component === LoginPage ||
                    this.routerOutlet.activatedRoute.component === LandingPage;
                const isOrderPayment = this.routerOutlet.activatedRoute.component === OrderConfirmationPage;
                if (this.routerOutlet && isMainPage && !this.modalService.callingPopup) {
                    if (!this.exitShown) {
                        this.backButtonHandler();
                    }
                } else if (isOrderPayment) {
                    this.navController.navigateRoot('/landing');
                } else if (this.modalService.addressPopup || this.modalService.addressLocationPopup) {
                    this.modalService.dismiss(false, null);
                } else if (this.modalService.callingPopup) {
                    if (!this.twilioVoiceService.onCall) {
                        this.modalService.dismiss(false, null);
                    }
                } else if (this.routerOutlet && !isMainPage) {
                    this.navController.back();
                } else {
                    this.routerOutlet.pop();
                }
            });
    }

    async openPage(page: string) {
        await this.navController.navigateForward(page);
    }

    async logout() {
        const header = await this.translate.get('CONFIRMATION_HEADER').toPromise();
        const body = await this.translate.get('CONFIRMATION_LOGOUT_TEXT').toPromise();
        const cancelText = await this.translate.get('CONFIRMATION_CANCEL').toPromise();
        const confirmText = await this.translate.get('CONFIRMATION_CONFIRM').toPromise();
        this.commonService.confirm(header, body,
            cancelText, confirmText, this.onLogoffCallback.bind(this), null);
    }

    async backButtonHandler() {
        this.exitShown = true;
        const header = await this.translate.get('CONFIRMATION_EXIT_HEADER').toPromise();
        const body = await this.translate.get('CONFIRMATION_EXIT_TEXT').toPromise();
        const cancelText = await this.translate.get('CONFIRMATION_CANCEL').toPromise();
        const confirmText = await this.translate.get('CONFIRMATION_CONFIRM').toPromise();
        this.commonService.confirmWithCallback(header, body, cancelText, confirmText,
            () => {
                setTimeout(() => {
                    navigator['app'].exitApp();
                }, 200);
            }, () => {
                this.exitShown = false;
            });
    }

    async onLogoffCallback() {
        this.commonService.hasTwilioInitialize = false;
        if (!isNullOrUndefined(Twilio) && !isNullOrUndefined(Twilio.TwilioVoiceClient)) {
            Twilio.TwilioVoiceClient.destroy();
        }
        const loading = this.loadingService.presentLoading();
        try {
            if (isNullOrUndefined(this.authService.deviceId)) {
                this.authService.clearUserDetail();
                this.loadingService.hideLoading(loading);
                this.navController.navigateRoot('login');
                return;
            }
            await this.userService.logout(this.authService.deviceId);
            this.authService.clearUserDetail();
            setTimeout(() => {
                this.loadingService.hideLoading(loading);
                this.navController.navigateRoot('login');
            }, 2000);
        } catch (e) {
            this.authService.clearUserDetail();
            setTimeout(() => {
                this.loadingService.hideLoading(loading);
                this.navController.navigateRoot('login');
            }, 2000);
        }
    }
}

