import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})
export class CartService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetch(): Observable<RestResponse> {
        return this.getRecords('/api/application/cart');
    }

    cartItems(): Observable<RestResponse> {
        return this.getRecord('/api/application/cart/item/count');
    }

    fetchBasicDetail() {
        return this.getRecords('/api/application/cart?BasicInfo=true');
    }

    add(cart: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/cart', cart);
    }

    update(cart: any): Promise<RestResponse> {
        return this.updateRecord('/api/application/cart', cart);
    }

    updateDelivery(data: any): Promise<RestResponse> {
        return this.updateRecord('/api/application/cart/update/delivery', data);
    }

    updateDeliveryTime(data: any): Promise<RestResponse> {
        return this.updateRecord('/api/application/cart/update/delivery/time', data);
    }

    lock(): Promise<RestResponse> {
        return this.postUrl('/api/application/cart/lock');
    }

    removeItem(id) {
        return this.removeRecord(`/api/application/cart/item/${id}`);
    }

    removeService(id) {
        return this.removeRecord(`/api/application/cart/service/${id}`);
    }
}
