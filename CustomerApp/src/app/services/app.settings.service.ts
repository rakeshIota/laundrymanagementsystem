import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})
export class AppSettingsService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetch(): Observable<RestResponse> {
        return this.getRecords('/app/application/settings');
    }
}
