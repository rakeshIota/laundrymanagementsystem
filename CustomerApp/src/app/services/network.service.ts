import {Injectable} from '@angular/core';
import {AlertController, Events} from '@ionic/angular';
import {Network} from '@ionic-native/network/ngx';
import {isNullOrUndefined} from 'util';

@Injectable({
    providedIn: 'root'
})
export class NetworkService {

    isNetworkAvailable: boolean;
    alertPopup: any;

    constructor(private network: Network, private events: Events, private alert: AlertController) {
        this.isNetworkAvailable = false;
    }

    init() {
        if (this.network.type !== null && this.network.type.toLowerCase() !== 'none') {
            this.isNetworkAvailable = true;
            setTimeout(() => {
                this.hidePopup();
            }, 500);
        }
        this.network.onDisconnect().subscribe((data) => {
            this.isNetworkAvailable = false;
            this.showNeedNetworkPopup();
        });
        this.network.onConnect().subscribe((data) => {
            setTimeout(() => {
                this.isNetworkAvailable = true;
                this.hidePopup();
            }, 500);
        });
    }

    async showNeedNetworkPopup() {
        const option = {
            header: 'Need Network',
            message: 'Your are current offline. Please connect to internet to use application',
            cssClass: 'custom-model',
            backdropDismiss: false,
            buttons: [
                {
                    text: 'DONE',
                    role: 'cancel',
                    handler: () => {
                        this.alertPopup.dismiss();
                    }
                }
            ]
        };
        this.alertPopup = await this.alert.create(option);
        await this.alertPopup.present();
    }

    hidePopup() {
        if (!isNullOrUndefined(this.alertPopup)) {
            this.alertPopup.dismiss();
            this.alertPopup = undefined;
        }
    }
}
