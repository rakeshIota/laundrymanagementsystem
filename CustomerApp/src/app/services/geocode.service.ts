import {Injectable} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {map, switchMap, tap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/observable/fromPromise';
import {Location} from '../dto/location';

declare var google: any;

@Injectable({
    providedIn: 'root'
})
export class GeocodeService {
    private geocoder: any;

    constructor(private mapLoader: MapsAPILoader) {
    }

    private initGeocoder() {
        this.geocoder = new google.maps.Geocoder();
    }

    private waitForMapsToLoad(): Observable<boolean> {
        if (!this.geocoder) {
            return fromPromise(this.mapLoader.load())
                .pipe(
                    tap(() => this.initGeocoder()),
                    map(() => true)
                );
        }
        return of(true);
    }

    geocodeAddress(location: string): Observable<Location> {
        console.log('Start geocoding!');
        // @ts-ignore
        return this.waitForMapsToLoad().pipe(
            switchMap(() => {
                return new Observable(observer => {
                    this.geocoder.geocode({address: location}, (results, status) => {
                        if (status === google.maps.GeocoderStatus.OK) {
                            console.log('Geocoding complete!');
                            const data = {} as any;
                            data.latitude = results[0].geometry.location.lat();
                            data.longitude = results[0].geometry.location.lng();
                            observer.next(data);
                        } else {
                            console.log('Error - ', results, ' & Status - ', status);
                            observer.next({latitude: 0, longitude: 0});
                        }
                        observer.complete();
                    });
                });
            })
        );
    }
}