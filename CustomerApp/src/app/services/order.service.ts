import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class OrderService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetch(orderId: string): Observable<RestResponse> {
        return this.getRecord(`/api/application/order/${orderId}`);
    }

    place(order: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/order/complete', order);
    }

    myOrders(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/my/orders', data);
    }

    myActiveOrders(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/application/my/home/orders', data);
    }

    cancelOrder(orderId: string) {
        return this.postUrl(`/api/application/order/${orderId}/cancel`);
    }

    driverLocation(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/driver/locations', data);
    }

    saveRating(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/rating', data);
    }

    paymentSuccess(data: any): Promise<RestResponse> {
        return this.saveRecord('/payment/received', data);
    }
}
