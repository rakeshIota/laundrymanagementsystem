import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpServiceRequests} from 'src/shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';
import {User} from '../models/user.model';

@Injectable({
    providedIn: 'root'
})

export class UserService extends HttpServiceRequests<IResourceWithId> {
    constructor(public http: HttpClient) {
        super(http);
    }

    fetch(): Observable<RestResponse> {
        return this.getRecord('/api/customer/detail');
    }

    fetchMyDetail(): Observable<RestResponse> {
        return this.getRecord('/api/application/my/details');
    }

    update(user: User): Promise<RestResponse> {
        return this.updateRecord('/api/customer/detail', user);
    }

    getAddresses(type: string) {
        return this.getRecords(`/api/application/address/${type}`);
    }

    fetchBasicDetail() {
        return this.postUrl(`/api/application/user/basic/detail`);
    }

    saveToken(token: any) {
        return this.saveRecord('/api/application/user/device', token);
    }

    logout(deviceId) {
        return this.postUrl(`/api/application/${deviceId}/logout`);
    }
}