import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpServiceRequests} from '../../shared/http.service';
import {IResourceWithId, RestResponse} from '../models/authorization.model';

@Injectable({
    providedIn: 'root'
})
export class PromotionService extends HttpServiceRequests<IResourceWithId> {

    constructor(public http: HttpClient) {
        super(http);
    }

    fetchAll(data: any): Promise<RestResponse> {
        return this.saveRecord('/api/promotions', data);
    }
}
