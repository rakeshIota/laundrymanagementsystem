import {HttpHeaders} from '@angular/common/http';

export const DEFAULT_STRING_VALUE = '';

const zone = -new Date().getTimezoneOffset();
export let environment = {
    production: true,
    BaseApiUrl: 'https://lotuslaundry.azurewebsites.net',
    // BaseApiUrl: 'http://lotuslaundry.iotasol.com',
    // BaseApiUrl: 'https://3775918a037a.ngrok.io',
    UiSiteUrl: 'https://lotuslaundry.azurewebsites.net',
    AppHeaders: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
        userTimeZone: zone.toString(),
    }),
};