import {HttpHeaders} from '@angular/common/http';

export const DEFAULT_STRING_VALUE = '';

const zone = -new Date().getTimezoneOffset();
export let environment = {
    production: false,
    // BaseApiUrl: 'https://9ca5abbe3cb7.ngrok.io',
    // BaseApiUrl: 'http://lotuslaundry.iotasol.com',
    // BaseApiUrl: 'http://localhost:61039',
    BaseApiUrl: 'https://lotuslaundry.azurewebsites.net',
    UiSiteUrl: 'https://lotuslaundry.azurewebsites.net',
    AppHeaders: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json',
        userTimeZone: zone.toString(),
    })
};