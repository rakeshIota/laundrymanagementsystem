import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {isNullOrUndefined} from 'util';

@Injectable({
    providedIn: 'root'
})

export class CommonService {
    hasTrackingPageLoaded: boolean;
    hasTwilioInitialize: boolean;
    addAddress: boolean;

    constructor(private alert: AlertController) {
    }

    async info(title, body, confirmButtonText, callbackConfirm, targetId?) {
        const options = {
            header: title,
            message: body,
            cssClass: 'info-alert',
            showBackdrop: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm(targetId);
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    async confirm(title, body, cancelButtonText, confirmButtonText, callbackConfirm, targetId) {
        const options = {
            header: title,
            message: body,
            showBackdrop: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: cancelButtonText,
                    role: 'cancel',
                    cssClass: 'disagreeBtn',
                    handler: () => {
                        alert.dismiss();
                    }
                },
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm(targetId);
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    async confirmWithCallback(title, body, cancelButtonText, confirmButtonText, callbackConfirm, callbackCancel) {
        const options = {
            header: title,
            message: body,
            showBackdrop: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: cancelButtonText,
                    role: 'cancel',
                    cssClass: 'disagreeBtn',
                    handler: () => {
                        alert.dismiss();
                        callbackCancel();
                    }
                },
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm();
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    async confirmWithCallbackWithData(title, body, cancelButtonText, confirmButtonText, callbackConfirm, callbackCancel, targetId) {
        const options = {
            header: title,
            message: body,
            showBackdrop: true,
            backdropDismiss: false,
            buttons: [
                {
                    text: cancelButtonText,
                    role: 'cancel',
                    cssClass: 'disagreeBtn',
                    handler: () => {
                        alert.dismiss();
                        callbackCancel();
                    }
                },
                {
                    text: confirmButtonText,
                    handler: () => {
                        alert.dismiss();
                        callbackConfirm(targetId);
                    }
                }
            ]
        };
        const alert: any = await this.alert.create(options);
        await alert.present();
        return alert;
    }

    locationPopup() {
        this.alert.create({
            header: 'Need Location',
            message: 'We need a your location for better exprience. Please change settings',
            cssClass: 'custom-model',
            buttons: ['Dismiss'],
        }).then(alert => alert.present());
    }

    filterRecords(inputRecords) {
        const temp = {};
        const filterRecords = inputRecords.reduce((records, record) => {
            if (temp.hasOwnProperty(record.service.id)) {
                records[temp[record.service.id]].push(record);
            } else {
                temp[record.service.id] = records.length;
                records.push([record]);
            }
            return records;
        }, []);
        return filterRecords;
    }
}
