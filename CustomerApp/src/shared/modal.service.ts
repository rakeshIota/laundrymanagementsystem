import {Injectable} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ModalService {
    popover: HTMLIonPopoverElement;
    addressLocationPopup: boolean;
    addressPopup: boolean;
    settingAddressPopup: boolean;
    callingPopup: boolean;

    constructor(public modalController: ModalController) {
        this.addressLocationPopup = false;
        this.addressPopup = false;
        this.settingAddressPopup = false;
    }

    async presentModal(inputComponent: any, data: any, iCssClass?: string) {
        const modal = await this.modalController.create({
            component: inputComponent,
            componentProps: data,
            cssClass: iCssClass,
            showBackdrop: true,
            backdropDismiss: false,
        });
        await modal.present();
        return modal;
    }

    dismiss(iComplete: boolean, iData: any) {
        this.modalController.dismiss({complete: iComplete, data: iData});
    }

    onComplete(modal, callback) {
        if (modal && modal !== null) {
            modal.onDidDismiss().then((data) => {
                callback(data.data);
            });
        }
    }
}
