import {CanDeactivate} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {CommonService} from './common.service';
import {TranslateService} from '@ngx-translate/core';

export interface ComponentCanDeactivate {
    hasChanged: () => boolean | Observable<boolean>;
}

@Injectable({
    providedIn: 'root'
})
export class PendingChangesGuard implements CanDeactivate<ComponentCanDeactivate> {
    constructor(private commonService: CommonService, private translateService: TranslateService) {
    }

    canDeactivate(component: ComponentCanDeactivate): Promise<boolean> | boolean {
        return this.handleRequest(component.hasChanged());
    }

    async handleRequest(hasChanged) {
        if (!hasChanged) {
            return true;
        }
        const header = await this.translateService.get('CONFIRMATION_HEADER').toPromise();
        const body = await this.translateService.get('CONFIRMATION_PENDING_TEXT').toPromise();
        const cancelText = await this.translateService.get('CONFIRMATION_NO').toPromise();
        const discardText = await this.translateService.get('CONFIRMATION_YES').toPromise();
        return new Promise<boolean>((resolve, reject) => {
            this.commonService.confirmWithCallback(header, body,
                cancelText, discardText,
                () => {
                    resolve(true);
                }, () => {
                    reject(false);
                });
        });
    }
}
