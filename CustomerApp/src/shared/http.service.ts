import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {IResourceWithId, RestResponse} from 'src/app/models/authorization.model';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class HttpServiceRequests<TResource extends IResourceWithId> {
    headers = environment.AppHeaders;

    constructor(public http: HttpClient) {
    }

    getRecords(path: string): Observable<RestResponse> {
        return this.http
            .get(environment.BaseApiUrl + path, {headers: environment.AppHeaders})
            .pipe(catchError(this.handleError));
    }

    getRecord(url: string): Observable<RestResponse> {
        return this.http
            .get(environment.BaseApiUrl + url, {headers: environment.AppHeaders})
            .pipe(catchError(this.handleError));
    }

    saveRecord(url: string, resource: TResource): Promise<RestResponse> {
        return this.http
            .post(environment.BaseApiUrl + url, resource, {headers: environment.AppHeaders})
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    updateRecord(url: string, resource: TResource): Promise<RestResponse> {
        return this.http
            .put(environment.BaseApiUrl + url, resource, {headers: environment.AppHeaders})
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    removeRecord(url: string): Promise<RestResponse> {
        return this.http
            .delete(environment.BaseApiUrl + url, {headers: environment.AppHeaders})
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    postUrl(url: string): Promise<RestResponse> {
        return this.http
            .post(environment.BaseApiUrl + url, {headers: environment.AppHeaders})
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    private handleError(error: HttpErrorResponse): Promise<any> {
        return Promise.reject(error.error);
    }
}
