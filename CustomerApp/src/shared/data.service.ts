import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Language } from 'src/app/models/language.model';
import { LanguageService } from 'src/app/services/language.service';
import { ProductService } from 'src/app/services/product.service';
import { isNullOrUndefined } from 'util';
import { RestResponse } from '../app/models/authorization.model';
import { Address } from '../app/models/user.model';
import { AppSettingsService } from '../app/services/app.settings.service';
import { CartService } from '../app/services/cart.service';
import { ToastService } from './toast.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    appSettings: Array<any>;
    languages: Language[];
    cartItemTotal: number;
    cartData: any;
    addressData: Address;

    constructor(private appSettingService: AppSettingsService, private toastService: ToastService, private languageService: LanguageService,
        private cartService: CartService, private productService: ProductService, private translateService: TranslateService) {
        this.languages = [] as Language[];
    }

    async init() {
        try {
            const response: RestResponse = await this.appSettingService.fetch().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.appSettings = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async getAppSettings() {
        if (!isNullOrUndefined(this.appSettings)) {
            return this.appSettings;
        }
        await this.init();
        return this.appSettings;
    }

    async fetchAllLanguages() {
        try {
            const response: RestResponse = await this.languageService.fetchAll().toPromise();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            this.languages = response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }

    async getLanguages() {
        await this.fetchAllLanguages();
        return this.languages;
    }

    async fetchCartItemTotal() {
        try {
            const response: RestResponse = await this.cartService.cartItems().toPromise();
            if (!response.status) {
                this.cartItemTotal = 0;
                return;
            }
            this.cartItemTotal = Number(response.data);
        } catch (e) {
            this.cartItemTotal = 0;
        }
    }

    increaseCartItem() {
        this.cartItemTotal = Number(this.cartItemTotal) + 1;
    }

    decreaseCartItem(count?: number) {
        if (isNullOrUndefined(count)) {
            this.cartItemTotal = Number(this.cartItemTotal) - 1;
            return;
        }
        this.cartItemTotal = Number(this.cartItemTotal) - count;
    }

    async fetchProductFilters(): Promise<any[]> {
        try {
            const response: RestResponse = await this.productService.getFilters();
            if (!response.status) {
                this.toastService.show(response.message);
                return;
            }
            response.data.forEach(filter => {
                filter.translatedName = this.translateService.instant(`ITEM.FILTER.${filter.value}`);
            });
            return response.data;
        } catch (e) {
            this.toastService.show(e.message);
        }
    }
}
