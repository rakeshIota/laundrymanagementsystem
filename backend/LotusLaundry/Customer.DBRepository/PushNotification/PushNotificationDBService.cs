﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.PushNotification
{
    public class PushNotificationDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public long? PushNotificationInsert(PushNotificationModel model)
        {
            using (var dbctx = DbContext)
            {
                 var target  = Convert.ToInt64(CryptoEngine.Decrypt(model.TargetId, KeyConstant.Key));
                //var target = Convert.ToInt64((model.TargetId));

                //      int target= Convert.ToInt32( CryptoEngine.Decrypt(FileType.ORDER + "_" + model.TargetId, KeyConstant.Key));
                var id = dbctx.PushNotificationInsert(model.CreatedBy, model.Title, model.Message, model.Type, target, model.TargetType, model.UserId, model.UserRole).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }
        }

        public void PushNotificationRead(PushNotificationModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PushNotificationLogRead(model.Id, model.Type, model.UserId);
            }
        }

        public List<PushNotificationModel> PushNotificationSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PushNotificationSelect(param.RecordId, param.IsActive, param.Next, param.Offset, param.UserRole, param.UserId).Select(x => new PushNotificationModel
                {
                    Id = x.Id,
                    Title = x.Title,
                    Message = x.Message,
                    IsSeen = x.IsSeen,
                    UserRole = x.UserRole,
                    ArchiveDate = x.ArchiveDate,
                    ArchivedBy = x.ArchivedBy,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    IsActive = x.IsActive,
                    IsArchive = x.IsArchive,
                    IsDeleted = x.IsDeleted,
                    overall_count = x.overall_count,
                    TargetId = x.TargetId != null? x.TargetId.ToString():null,
                    TargetType = x.TargetType,
                    Type = x.Type,
                    UpdatedBy = x.UpdatedBy,
                    UpdatedOn = x.UpdatedOn,
                    UserId = x.UserId,
                    UnreadCount = x.UnreadCount
                }).ToList();
            }
        }

        public List<Broadcastmodel> BroadcastSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.BroadcastSelect(param.RecordId, param.IsActive, param.Next, param.Offset, param.searchText, param.UserId).Select(x => new Broadcastmodel
                {
                    Id = x.Id,
                    
                    message = x.Message,
                    Totalbatche = x.totalbatches.GetValueOrDefault(),
                    lastBatch = x.lastbatch,
                    TotalUsers = x.Totalusers,
                    PendingUsers = x.pendingUsers,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    IsActive = x.IsActive,
                   
                    IsDeleted = x.IsDeleted,
                    OverallCount = x.overall_count.GetValueOrDefault(),

                    UpdatedBy = x.UpdatedBy,
                    UpdatedOn = x.UpdatedOn,
                   
                }).ToList();
            }
        }

        public PushNotificationSelectForUser_Result PushNotificationSelectForUser(long? userId)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PushNotificationSelectForUser(userId).FirstOrDefault();
            }
        }

        public PushNotificationSelectForOtherActiveUser_Result NotificationSelectForOtherActiveUser(long? userId, string deviceId)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PushNotificationSelectForOtherActiveUser(userId, deviceId).FirstOrDefault();
            }
        }

        public List<UserDevice> GetUserDevice(long? userId)
        {
            using (var dbctx = DbContext) 
            {
                return dbctx.GetUserDeviceToken(userId).Select(x => new UserDevice
                {
                    DeviceId = x.DeviceId,
                    DeviceToken = x.DeviceToken,
                    DeviceType = x.DeviceType

                }).ToList();
            }
        }

        //public List<PushNotificationModel> GetNotificationLogForAppDashboard(long? userId)
        //{
        //    using (var dbctx = DbContext)
        //    {
        //        return dbctx.GetNotificationLogForAppDashboard(userId).Select(x => new PushNotificationModel
        //        {
        //            Id = x.Id,
        //            Title = x.Title,
        //            Message = x.Message,
        //            IsSeen = x.IsSeen,
        //            UserRole = x.UserRole,
        //            ArchiveDate = x.ArchiveDate,
        //            ArchivedBy = x.ArchivedBy,
        //            CreatedBy = x.CreatedBy,
        //            CreatedOn = x.CreatedOn,
        //            IsActive = x.IsActive,
        //            IsArchive = x.IsArchive,
        //            IsDeleted = x.IsDeleted,
        //            TargetId = x.TargetId,
        //            TargetType = x.TargetType,
        //            Type = x.Type,
        //            UpdatedBy = x.UpdatedBy,
        //            UpdatedOn = x.UpdatedOn,
        //            UserId = x.UserId,
        //        }).ToList();
        //    }
        //}


        public long? BroadCastNotificationInsert(Broadcastmodel model)
        {
            using (var dbctx = DbContext)
            {
                
                //      int target= Convert.ToInt32( CryptoEngine.Decrypt(FileType.ORDER + "_" + model.TargetId, KeyConstant.Key));
                var id = dbctx.BroadcastMsgInsert(1, null, model.CreatedBy, model.IsActive, null,null, model.message, model.TotalUsers,model.Totalbatche,model.lastBatch,model.PendingUsers,model.UserType,model.postalCode,model.Gender,model.Relation,model.orderCount,model.next,model.offset).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }
        }

        public void BroadCastNotificationUpdate(Broadcastmodel model)
        {
            using (var dbctx = DbContext)
            { 

                //      int target= Convert.ToInt32( CryptoEngine.Decrypt(FileType.ORDER + "_" + model.TargetId, KeyConstant.Key));
                  dbctx.BroadcastMsgUpdate(model.Id, model.message, model.TotalUsers, model.Totalbatche, model.lastBatch, model.PendingUsers, model.UserType, model.postalCode, model.Gender, model.Relation, model.orderCount, model.Result,model.next, model.offset);
               
            }
        }


        public List<UsersGroupModel> BroadcastUsersSelect(BroadcastParam param)
        {


            using (var dbctx = DbContext)
            {
                return dbctx.UserSelectForBroadcastMsg(param.customerType, param.PostalCode, param.customerGender,param.customerOrderType, param.orderCount,param.next,param.offset).Select(x => new UsersGroupModel
                {

                    Id=x.id,
                    DeviceId=x.DeviceId,
                    DeiviceToken=x.DeviceToken,
                    TotalCount=x.overall_count


                }).ToList();
            }
        }
        public List<UsersGroupModel> BroadcastUsersSelectWithPostalCode(BroadcastParam param)
        {


            using (var dbctx = DbContext)
            {
                return dbctx.UserSelectWithPostalForBroadcastMsg(param.customerType, param.PostalCode, param.customerGender, param.customerOrderType, param.orderCount, param.next, param.offset).Select(x => new UsersGroupModel
                {

                    Id = x.id,
                    DeviceId = x.DeviceId,
                    DeiviceToken = x.DeviceToken,
                    TotalCount = x.overall_count


                }).ToList();
            }
        }
    }
}

