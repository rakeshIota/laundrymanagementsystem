﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository
{
    public partial class PushNotificationSelectForUser_Result
    {
        public List<string> DeviceList
        {
            get

            {
                if (string.IsNullOrEmpty(Devices))
                    return null;
                return Devices.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToList();
            }
        }
    }

    public partial class PushNotificationSelectForOtherActiveUser_Result
    {
        public List<string> OtherDeviceList
        {
            get
            {
                if (string.IsNullOrEmpty(Devices))
                    return null;
                return Devices.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .ToList();
            }
        }
    }
}
