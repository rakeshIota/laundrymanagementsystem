﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.Language
{
    public class LanguageCustomerDBService
    {

        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public List<LanguageModel> LanguageSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.LanguageSelectApp(param.RecordId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new LanguageModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Code = x.Code
                    }).ToList();
            }

        }
    }
}
