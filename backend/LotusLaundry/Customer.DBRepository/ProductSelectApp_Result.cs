//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Customer.DBRepository
{
    using System;
    
    public partial class ProductSelectApp_Result
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string ImageXml { get; set; }
        public string ProductName { get; set; }
        public Nullable<decimal> Price { get; set; }
    }
}
