﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.Product
{
    public class ProductCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public List<ProductModel> ProductSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProductSelectApp(param.RecordId, param.LanguageId, param.Next, param.Offset, param.TenantId, param.Gender, param.ServiceId).Select(x => new ProductModel
                {
                    Id = x.Id,
                    Name = x.ProductName != null ? x.ProductName : x.Name,
                    Gender = x.Gender,
                    Price = x.Price ?? 0,
                    ImageXml = x.ImageXml
                }).ToList();
            }

        }


        public List<ProductModel> ServiceProductSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ServiceProductSelectApp(param.LanguageId, param.Next, param.Offset, param.TenantId, param.Gender, param.ServiceId).Select(x => new ProductModel
                {
                    Id = x.ProductId,
                    Name = x.ProductName != null ? x.ProductName : x.Name,
                    Gender = x.Gender,
                    Price = x.Price ?? 0,
                    ImageXml = x.ImageXml
                }).ToList();
            }

        }

        public List<ProductModel> GetProductPrice(SearchParam param,long productid)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ServiceProductPriceSelect(param.LanguageId, param.Next, param.Offset, param.TenantId, param.Gender, param.ServiceId,productid).Select(x => new ProductModel
                {
                    Id = x.ProductId,
                    Name = x.ProductName != null ? x.ProductName : x.Name,
                    Gender = x.Gender,
                    Price = x.Price ?? 0,
                    ImageXml = x.ImageXml
                }).ToList();
            }

        }
    }
}
