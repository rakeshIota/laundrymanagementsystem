﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.SubDistrict
{
    public class SubDistrictCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }
        public List<SubDistrictModel> SubDistrictSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.SubDistrictSelectApp(param.RecordId, param.LanguageId, param.Next, param.Offset, param.TenantId, param.LRelationId
                    ).Select(x => new SubDistrictModel
                    {
                        Id = x.Id,
                        Name = x.SubDistrictName != null ? x.SubDistrictName : x.Name,
                    }).ToList();
            }
        }
    }
}
