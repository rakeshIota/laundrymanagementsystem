﻿using LotusLaundry.Domain.UserAddress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace Customer.DBRepository.UserAddress
{
    public class UserAddressDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public long UserAddressInsert(UserAddressModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.UserAddressInsertApp(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.UserId, model.AddressLine1, model.AddressLine2, model.DistrictId, model.SubDistrictId, model.PostalCode, model.IsDefault, model.Tag, model.ProvinceId, model.Latitude, model.Longitude, model.Type, model.HouseNumber, model.StreetNumber, model.Note,model.BuildingName,model.Floor,model.UnitNo,model.PhoneNo,model.alterNumber, model.ResidenceType).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        
        
        public void UserAddressUpdate(UserAddressModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserAddressUpdateApp(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.UserId, model.AddressLine1, model.AddressLine2, model.DistrictId, model.SubDistrictId, model.PostalCode, model.IsDefault, model.Tag, model.ProvinceId, model.Latitude, model.Longitude, model.Type, model.HouseNumber, model.StreetNumber, model.Note, model.BuildingName, model.Floor, model.UnitNo, model.PhoneNo, model.alterNumber, model.ResidenceType);
            }

        }
        public List<UserAddressModel> SelectUserAddress(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserAddressSelectApp(param.RecordId, param.Next, param.Offset, param.LanguageId, param.TenantId,param.Type,param.UserId).Select(x => new UserAddressModel
                {
                    Id = x.Id,
                    AddressLine1 = x.AddressLine1,
                    DistrictId = x.DistrictId,
                    SubDistrictId = x.SubDistrictId,
                    PostalCode = x.PostalCode,
                    Tag = x.Tag,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Type = x.Type,
                    StreetNumber = x.StreetNumber,
                    HouseNumber = x.HouseNumber,
                    Note = x.Note,
                    DistrictName = x.DistrictNameL != null ? x.DistrictNameL : x.DistrictName,
                    SubDistrictName = x.SubDistrictNameL != null ? x.SubDistrictNameL : x.SubDistrictName,
                    AddressLine2 = x.AddressLine2,

                    ResidenceType=x.ResidenceType,
                    BuildingName=x.BuildingName,
                    alterNumber = x.alterNumber,
                    PhoneNo=x.PhoneNo,
                    UnitNo=x.UnitNo,
                    Floor=x.Floor,
                    ProvinceId=x.ProvinceId,
                    ProvinceName= x.ProvinceNameL != null ? x.ProvinceNameL : x.ProvinceName,


                }).ToList();
            }
        }

        //SelectAddressInActivePostalCode

        public List<UserAddressModel> SelectAddressInActivePostalCode(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserAddressInActivePostalCode(param.RecordId, param.Next, param.Offset, param.LanguageId, param.TenantId, param.Type, param.UserId).Select(x => new UserAddressModel
                {
                    Id = x.Id,
                    AddressLine1 = x.AddressLine1,
                    DistrictId = x.DistrictId,
                    SubDistrictId = x.SubDistrictId,
                    PostalCode = x.PostalCode,
                    Tag = x.Tag,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Type = x.Type,
                    StreetNumber = x.StreetNumber,
                    HouseNumber = x.HouseNumber,
                    Note = x.Note,
                    DistrictName = x.DistrictNameL != null ? x.DistrictNameL : x.DistrictName,
                    SubDistrictName = x.SubDistrictNameL != null ? x.SubDistrictNameL : x.SubDistrictName,
                    AddressLine2 = x.AddressLine2,

                    ResidenceType = x.ResidenceType,
                    BuildingName = x.BuildingName,
                    alterNumber = x.alterNumber,
                    PhoneNo = x.PhoneNo,
                    UnitNo = x.UnitNo,
                    Floor = x.Floor,

                }).ToList();
            }
        }

        public long DeliveryAddressInsert(UserAddressModel model,Int32 orderid)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DeliveryAddressInsertApp(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.UserId, model.AddressLine1, model.AddressLine2, model.DistrictId, model.SubDistrictId, model.PostalCode, model.IsDefault, model.Tag, model.ProvinceId, model.Latitude, model.Longitude, model.Type, model.HouseNumber, model.StreetNumber, model.Note, model.BuildingName, model.Floor, model.UnitNo, model.PhoneNo, model.alterNumber, model.ResidenceType, orderid).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
    }
}
