﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.Service
{
    public class ServiceCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public List<ServiceModel> ServiceSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ServiceSelectApp(param.RecordId, param.LanguageId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new ServiceModel
                    {
                        Id = x.Id,
                        Name = x.ServiceName != null ? x.ServiceName : x.Name,
                        Description = x.ServiceDescription != null ? x.ServiceDescription :x.Description,
                        ImageXml =  x.ImageXml
                    }).ToList();
            }

        }
    }
}
