﻿using LotusLaundry.Domain.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using Customer.DBRepository;

namespace Customer.DBRepository.Cart
{
    public class CartDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public long CartInsert(CartModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CartInsertApp(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.IsLocked, model.CustomerId, model.Status, model.Tax, model.SubTotal, model.TotalPrice, model.TotalItems, model.DeliveryType,model.DeliveryDate,model.PickupDate,model.deliverySlot,model.pickupSlot).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CartUpdate(CartModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartUpdateApp(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.IsLocked, model.CustomerId, model.Status, model.Tax, model.SubTotal, model.TotalPrice, model.TotalItems, model.DeliveryType, model.DeliveryDate, model.PickupDate, model.deliverySlot, model.pickupSlot,model.laundryInstruction);
            }

        }


        public void CartWithItemDelete(CartModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartWithItemDelete(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.IsLocked, model.CustomerId, model.Status, model.Tax, model.SubTotal, model.TotalPrice, model.TotalItems, model.DeliveryType);
            }

        }


        public List<CartModel> SelectCart(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CartSelectApp(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId).Select(x => new CartModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    IsLocked = x.IsLocked,
                    CustomerId = x.CustomerId,
                    Status = x.Status,
                    Tax = x.Tax,
                    SubTotal = x.SubTotal,
                    TotalPrice = x.TotalPrice,
                    TotalItems = x.TotalItems,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    CartItmeXml = x.CartItemXml,
                    DeliveryType = x.DeliveryType,

                    DeliveryDate=x.Deliverydate,
                    deliverySlot=x.deliverySlot,
                    pickupSlot=x.pickupSlot,
                    PickupDate=x.Pickupdate,
                    laundryInstruction=x.laundryInstruction

                }).ToList();
            }
        }
    }
}
