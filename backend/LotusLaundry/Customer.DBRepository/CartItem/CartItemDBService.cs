﻿using LotusLaundry.Domain.CartItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace Customer.DBRepository.CartItem
{
    public class CartItemDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public long CartItemInsert(CartItemModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CartItemInsertApp(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.CartId, model.ProductId, model.Quantity, model.Price, model.TotalPrice, model.ServiceId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CartItemUpdate(CartItemModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartItemUpdateApp(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.CartId, model.ProductId, model.Quantity, model.Price, model.TotalPrice, model.ServiceId);
            }

        }
        //CartItemUpdateByService
        public void CartItemUpdateByService(CartItemModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartItemUpdateByService(model.UpdatedBy, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.CartId, model.ProductId, model.Quantity, model.Price, model.TotalPrice, model.ServiceId);
            }

        }
        public void CartItemXMLSaveApp(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartItemXMLSaveApp(xml);
            }

        }
        public List<CartItemModel> SelectCartItem(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CartItemSelectApp(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CartItemModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    CartId = x.CartId,
                    ProductId = x.ProductId,
                    Quantity = x.Quantity,
                    Price = x.Price,
                    TotalPrice = x.TotalPrice,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    ServiceId = x.ServiceId
                }).ToList();
            }
        }
    }
}
