﻿using Customer.DBRepository;
using LotusLaundry.Domain.UserTwilioCalling;
using System;
using System.Linq;

namespace LotusLaundry.DBRepository.UserTwilioCalling
{
    public class UserTwilioCallingDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public void UserCallingStatusUpdate(UserTwilioCallingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserCallingStatusUpdate(model.FromUserId, model.ToUserId, model.IsActive);
            }
        }

        public bool CheckUserCallingStatus(UserTwilioCallingModel model)
        {
            using (var dbctx = DbContext)
            {
                var isActive = dbctx.CheckUserCallingStatus(model.FromUserId, model.ToUserId).FirstOrDefault();
                return Convert.ToBoolean(isActive);
            }
        }

        //  CheckUserCallingDeviceStatus
        public bool CheckUserCallingDeviceStatus(UserTwilioCallingModel model)
        {
            using (var dbctx = DbContext)
            {
                var isActive = dbctx.CheckUserCallingDeviceStatus(model.FromUserId, model.ToUserId).FirstOrDefault();
                return Convert.ToBoolean(isActive);
            }
        }
        public void UserCallingStatusDeactivate(long? UserId)
        {
            using (var dbctx = DbContext)
            {
               dbctx.UserCallingStatusDeactivate(UserId);
            }
        }
    }
}
