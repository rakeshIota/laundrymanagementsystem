﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Rating;
using LotusLaundry.Domain.Location;
using Customer.DBRepository;

namespace Driver.DBRepository.Rating
{
    public class RatingDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public long RatingInsert(RatingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.RatingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Orderid,model.quality,model.DeliveryService,model.Staff,model.Packaging,model.OverallSatisfaction,model.Feedback).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void RatingUpdate(RatingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.RatingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive, model.Orderid, model.quality, model.DeliveryService, model.Staff, model.Packaging, model.OverallSatisfaction, model.Feedback);
            }

        }
        public List<RatingModel> SelectRating(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.RatingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new RatingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Orderid=x.orderId,
                            quality=x.Quality,
                            DeliveryService=x.DeliveryService,
                            Staff=x.Staff,
                            Packaging=x.Packaging,
                            OverallSatisfaction=x.OverallSatisfaction,
                            Feedback=x.Feedback,
                           

                    TotalCount = x.overall_count.GetValueOrDefault()
                }).ToList();
            }
        }

      
    }
}
