﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.AppSetting
{
   public class AppSettingCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public List<AppSettingModel> AppSettingSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.AppSettingSelectApp(param.RecordId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new AppSettingModel
                    {
                       Key =  x.Key,
                       Value = x.Value
                    }).ToList();
            }

        }
        
    }
}
