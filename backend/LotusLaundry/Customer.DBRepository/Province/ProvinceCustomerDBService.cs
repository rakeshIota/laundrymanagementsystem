﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.Province
{
    public class ProvinceCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }

        public List<ProvinceModel> ProvinceSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProvinceSelectApp(param.RecordId, param.LanguageId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new ProvinceModel
                    {
                        Id = x.Id,
                        Name = x.ProvinceName != null ? x.ProvinceName : x.Name
                    }).ToList();
            }

        }
    }
}
