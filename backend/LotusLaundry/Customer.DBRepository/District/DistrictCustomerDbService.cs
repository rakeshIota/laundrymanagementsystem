﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.DBRepository.District
{
    public class DistrictCustomerDBService
    {
        CustomerEntities DbContext { get { return new CustomerEntities(); } }
        public List<DistrictModel> DistrictSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DistrictSelectApp(param.RecordId, param.LanguageId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new DistrictModel
                    {
                        Id = x.Id,
                        Name = x.DistrictName != null ? x.DistrictName : x.Name,
                    }).ToList();
            }
        }
    }
}
