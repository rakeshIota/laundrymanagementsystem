using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;

using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Rating;

using Driver.Service.Rating;
using LotusLaundry.Framework.ViewModels.Driver.Rating;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class RatingController : ApiController
    { 
        private IRatingService _ratingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public RatingController(IRatingService ratingService, IExceptionService exceptionService)
        {
            _ratingService = ratingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Rating
        /// </summary>
        /// <returns></returns>
        [Route("ratings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost] 
        public IHttpActionResult GetAllRatings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var DRating = _ratingService.SelectRating(param).Select(x => x.ToViewModel());
            	return Ok(DRating.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Rating, (int)ErrorFunctionCode.Rating_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Rating_Select).ToString().ErrorResponse());
            }
        }




        /// <summary>
        /// Api use for get Rating by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("rating/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver, UserRole.Customer)]
        [HttpGet]
        public IHttpActionResult GetRating(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var Rating = _ratingService.SelectRating(param).FirstOrDefault().ToViewModel();
	            if (Rating.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(Rating.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Rating, (int)ErrorFunctionCode.Rating_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Rating_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save Rating
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("rating")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver,UserRole.Customer)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveRating(RatingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.IsActive = true;
                model.IsDeleted = false;
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _ratingService.RatingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.Rating + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Review Submitted - Thank you!"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Rating, (int)ErrorFunctionCode.Rating_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Rating_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update Rating
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("rating")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver, UserRole.Customer)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateRating(RatingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _ratingService.RatingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Review updated - Thank you! "));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Rating, (int)ErrorFunctionCode.Rating_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Rating_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete Rating by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("rating/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver, UserRole.Customer)]
        [HttpDelete]
        public IHttpActionResult DeleteRating(string id)
        {
        	try{
                RatingViewModel model = new RatingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _ratingService.RatingUpdate(model.ToModel());
	            return Ok("Rating Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Rating, (int)ErrorFunctionCode.Rating_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Rating_Delete).ToString().ErrorResponse());
            }
        }


       
    }
}

