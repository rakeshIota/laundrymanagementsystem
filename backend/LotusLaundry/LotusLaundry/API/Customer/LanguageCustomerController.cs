﻿using Customer.Service.Language;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.Language;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using LotusLaundry.Service.Language;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{

    public class LanguageCustomerController : ApiController
    {
        private ILanguageCustomerService _languageCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public LanguageCustomerController(ILanguageCustomerService languageCustomerService, IExceptionService exceptionService)
        {
            _languageCustomerService = languageCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }


        [Route("app/languages")]
        [HttpGet]
        public IHttpActionResult GetLanguages(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                var language = _languageCustomerService.SelectLanguage(param).Select(y => y.ToViewModel()).ToList();
                return Ok(language.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Select_customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Select_customer).ToString().ErrorResponse());
            }
        }

        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        [Route("api/language-change/{languageId}")]
        [CustomAuthorize(UserRole.Customer,UserRole.Driver)]
        [HttpPut]
        public IHttpActionResult ChangeLanguage(string languageId)
        {
            try
            {
              var user = UserManager.FindById(User.Identity.GetUserId<long>());
                user.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(languageId, KeyConstant.Key));
                UserManager.Update(user);
                return Ok("Language change successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Change);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Change).ToString().ErrorResponse());
            }
        }
    }
}
