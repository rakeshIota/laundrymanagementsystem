﻿using Customer.Service.Province;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.Province;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    public class ProvinceCustomerController : ApiController
    {

        private IProvinceCustomerService _provinceCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProvinceCustomerController(IProvinceCustomerService provinceCustomerService, IExceptionService exceptionService)
        {
            _provinceCustomerService = provinceCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("api/application/provinces")]
        [CustomAuthorize(UserRole.Customer, UserRole.Driver)]
        [HttpGet]
        public IHttpActionResult ProvinceSelect()
        {
            try
            {
                SearchParam param = new SearchParam();
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var provinces = _provinceCustomerService.ProvinceSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(provinces.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Province_Select_Customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Select_Customer).ToString().ErrorResponse());
            }
        }
    }
}
