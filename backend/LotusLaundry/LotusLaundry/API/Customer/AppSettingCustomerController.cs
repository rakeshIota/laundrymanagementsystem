﻿using Customer.Service.AppSetting;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.AppSetting;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    public class AppSettingCustomerController : ApiController
    {
        private IAppSettingCustomerService _appSettingCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public AppSettingCustomerController(IAppSettingCustomerService appSettingCustomerService, IExceptionService exceptionService)
        {
            _appSettingCustomerService = appSettingCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("app/settings")]
        [HttpGet]
        public IHttpActionResult GetAppSettings()
        {
            try 
            {
                SearchParam param = new SearchParam();
                //param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var services = _appSettingCustomerService.AppSettingSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(services.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.AppSetting_Select_customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Select_customer).ToString().ErrorResponse());
            }
        }

        [Route("app/application/settings")]
        [HttpGet]
        public IHttpActionResult GetApplicationSettings()
        {
            try
            {
                SearchParam param = new SearchParam();
              
                var services = _appSettingCustomerService.ApplicationSettingSelect(param).ApplicationToViewModel();

                return Ok(services.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.AppSetting_Select_customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Select_customer).ToString().ErrorResponse());
            }
        }




    }
}
