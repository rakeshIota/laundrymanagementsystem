﻿using Customer.Service.PushNotification;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.PushNotification;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api")]
    public class PushNotificationController : ApiController
    {
        private IPushNotificationService _pushNotificationService;
       
        public PushNotificationController(IPushNotificationService pushNotificationService)
        {
            _pushNotificationService = pushNotificationService;
            
        }

        [Route("pushnotification/log")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetPushNotificationLog([FromUri]SearchParam param)
        {
            param = param ?? new SearchParam();
            if (param.UserRole == UserRole.Customer)
            {
                param.UserId = User.Identity.GetUserId<long>();
            }
            var _notification = _pushNotificationService.PushNotificationSelect(param).Select(x => x.ToViewModel());
            return Ok(_notification.SuccessResponse());
        }

        [Route("pushnotification/log/admin")]
        [Authorize(Roles = UserRole.Admin)]
        [HttpGet]
        public IHttpActionResult GetNotificationLogAdmin([FromUri]SearchParam param)
        {
            var _notification = _pushNotificationService.PushNotificationSelect(param).Select(x => x.ToViewModel());
            return Ok(_notification.SuccessResponse());
        }

        [Route("pushnotification/is/read/update")]
        [Authorize]
        [HttpPut]
        public IHttpActionResult NotificationLogIsReadUpdate(PushNotificationViewModel model)
        {
            _pushNotificationService.PushNotificationRead(model.ToModel());
            return Ok("Notification read updated.".SuccessResponse());
        }

        [Route("pushnotification/testing")]
        [HttpPost]
        public IHttpActionResult NotificationTesting(PushNotificationViewModel model)
        {

            // _pushNotificationService.NotificationInsert(model.ToModel()); // save notification log
            /* send notification via FCM */
            var FCM_Model = new FCMNotificationModel();
            List<string> deviceToken = new List<string>();
            deviceToken.Add(model.TargetType);
            FCM_Model.DeviceToken = deviceToken;
            FCM_Model.Message = "testing testing";
            FCM_Model.Title = "testing testing";

            _pushNotificationService.SendNotificationByFCM(FCM_Model);
            return Ok("Notification Testing".SuccessResponse());
        }

        [Route("send/notification/user/chat")]
        [HttpPost]
        public IHttpActionResult SendNotificationChat(PushNotificationViewModel model)
        {
            if(model.Ids == null || model.Ids == "")
            {
                return Ok("Id not found".SuccessResponse());
            }

            List<string> UserIds = model.Ids.Split(',').ToList();

            //            List<long> UserIds = model.Ids.Split(',').Select(long.Parse).ToList();
            
            UserIds.ForEach(id =>
            {
                model.CreatedBy = model.UpdatedBy =  model.UserId = Convert.ToInt64(CryptoEngine.Decrypt(id.ToString(), KeyConstant.Key));
                model.TargetId = model.TargetId;
                model.TargetType = PushNotificationType.ORDER_CHAT_NOTIFICATION.ToString();  //ORDER_CHAT_N  orderid , customerid , driverid 
                //model.customerid=model.UserId
                model.orderid = model.TargetId;
                //model.Title = "Ask Betty";  orderchatnotifiaction
                _pushNotificationService.PushNotificationChatAsync(model.ToModel()); // save notification log
            });

            return Ok("Notification sent successfully.".SuccessResponse());
        }

        [Route("notification/activeUsre")]
        [HttpPost]
        public IHttpActionResult NotificationToActiveUser(PushNotificationViewModel model)
        {

            // _pushNotificationService.NotificationInsert(model.ToModel()); // save notification log
            /* send notification via FCM */

           
            var FCM_Model = new FCMNotificationModel();
            List<string> deviceToken = new List<string>();
            deviceToken.Add(model.TargetType);
            FCM_Model.DeviceToken = deviceToken;
            FCM_Model.Message = "testing testing";
            FCM_Model.Title = "testing testing";

            _pushNotificationService.SendNotificationByFCM(FCM_Model);
            return Ok("Notification Testing".SuccessResponse());
        }


        [Route("broadcast")]
        [HttpPost]
        public IHttpActionResult NotificationBroadcasting(BroadcastParam model)
        {

            // _pushNotificationService.NotificationInsert(model.ToModel()); // save notification log
            /* send notification via FCM */
            if(model.postalCodes != null)
            {
                if (model.postalCodes.Count <= 0)
                {
                    var result = _pushNotificationService.BroadcastNotificationByToGroup(model.customerType, model.postalCodes, model.customerGender, model.customerOrderType, model.orderCount, model.message);

                }
                else
                {
                    string postalcode = "";
                    int i = 0;
                    foreach (var item in model.postalCodes)
                    {
                        if (i == 0)
                        {
                            postalcode = postalcode + item;
                        }
                        else
                        {
                            postalcode = postalcode + "," + item;
                        }
                        i++;
                    }
                    var result = _pushNotificationService.BroadcastNotificationByPOSTALCODE(model.customerType, postalcode, model.customerGender, model.customerOrderType, model.orderCount, model.message);

                }
            }

          
            return Ok("Message Broadcasting Succesfully Completed ".SuccessResponse());
        }

        [Route("broadcast/log")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult GetBroadcastLog([FromUri]SearchParam param)
        {
            param = param ?? new SearchParam();
            if (param.UserRole == UserRole.Customer)
            {
                param.UserId = User.Identity.GetUserId<long>();
            }
            var _notification = _pushNotificationService.BroadcastMessageSelect(param).Select(x => x.ToViewModel());
            return Ok(_notification.SuccessResponse());
        }

        [Route("twilio/voice/answered")]
        [HttpPost]
        public IHttpActionResult SendCallNotificationChat(PushNotificationViewModel model)
        {
            if (model.callerId == null || model.callerId == "")
            {
                return Ok("Id not found".SuccessResponse());
            }

                
                model.CreatedBy = model.UpdatedBy = model.UserId = Convert.ToInt64(CryptoEngine.Decrypt(model.callerId.ToString(), KeyConstant.Key));
                model.TargetId = model.callerId;
                 model.Message = "Call Pickup";
                model.TargetType = "CALL_PICKED";  //ORDER_CHAT_N  orderid , customerid , driverid 
                //model.customerid=model.UserId
                model.orderid = model.TargetId;
                
                //model.Title = "Ask Betty";  orderchatnotifiaction
                _pushNotificationService.PushNotificationChatAsync(model.ToModel()); // save notification log
                                                                                     // });

                return Ok("Notification sent successfully.".SuccessResponse());
            
        }

    }
}
