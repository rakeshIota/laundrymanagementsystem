﻿using LotusLaundry.Domain.TwilioChat;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.TwilioChat;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Twilio;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;

namespace LotusLaundry.API.Customer
{
    [System.Web.Http.RoutePrefix("api")]
    public class TokenController : ApiController
    {
        private string accountSid = ConfigurationManager.AppSettings["TWILIOACCOUNTSID"];
        private string authToken = ConfigurationManager.AppSettings["TWILIOAUTHTOKEN"];
        private string chatServiceSid = ConfigurationManager.AppSettings["TwilioChatServiceSid"];
        private string apiKey = ConfigurationManager.AppSettings["TwilioApiKey"];
        private string apiSecret = ConfigurationManager.AppSettings["TwilioApiSecret"];

        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        [System.Web.Http.Route("twillio/get/users")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        public IHttpActionResult ListAllUsers()
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                var users = UserResource.Read(
                    pathServiceSid: chatServiceSid
                );

                return Ok(users.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }



        /// <summary>
        /// Create new twilio user
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.Route("twillio/create/user")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult CreateUser()
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                var identity = User.Identity.GetUserId<long>();

                var userDetails = UserManager.FindById(identity);

                var user = UserResource.Create(
                    identity: identity.ToString(),
                    friendlyName: userDetails.FirstName,
                    pathServiceSid: chatServiceSid
                //roleSid: "RLbdaf0d750eef4850b0787da960579178"
                );

                return Ok(user.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }


        [System.Web.Http.Route("twillio/get/user")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        public IHttpActionResult FetchUser()
        {
            try
            {

                TwilioClient.Init(accountSid, authToken);
                var identity = User.Identity.GetUserId<long>();

                var user = UserResource.Fetch(
                    pathServiceSid: chatServiceSid,
                    pathSid: identity.ToString()

                );

                return Ok(user.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("twillio/user/update")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult UpdateTwilioUserDetails(TwilioChatViewModel model)
        {
            try
            {
                var identity = User.Identity.GetUserId<long>();

                var userDetails = UserManager.FindById(identity);
                TwilioChatModel obj = new TwilioChatModel();
                obj.Name = userDetails.FirstName;

                TwilioClient.Init(accountSid, authToken);
                var user = UserResource.Update(
                    roleSid: model.RoleSid,
                    pathServiceSid: chatServiceSid,
                    pathSid: model.PathSid,
                    friendlyName: userDetails.FirstName,
                    attributes: JsonConvert.SerializeObject(obj)
                );

                return Ok(user.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("get/channel/members")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetChannelMembers(string channelSid)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                var members = MemberResource.Read(
                    pathServiceSid: chatServiceSid,
                    pathChannelSid: channelSid
                );

                return Ok(members.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }

        }


        [System.Web.Http.Route("get/channel")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetChannelDetails(string channelSid)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                var channelDetails = ChannelResource.Fetch(
                    pathServiceSid: chatServiceSid,
                    pathSid: channelSid
                );

                return Ok(channelDetails.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }

        }

        [System.Web.Http.Route("twilio/send/message")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult SendMessage(TwilioChatViewModel model)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                TwilioChatModel obj = new TwilioChatModel();
                obj.UserRole = model.UserRole;

                var message = MessageResource.Create(
                    body: model.Body,
                    attributes: JsonConvert.SerializeObject(obj),
                    pathServiceSid: chatServiceSid,
                    pathChannelSid: model.ChannelSid,
                    from: model.From
                );

                return Ok("Message Sent Successfulyy".SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        /// <summary>
        /// generating twilio token
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        [System.Web.Http.Route("get/token")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpGet]
        public IHttpActionResult getToken(string device)
        {
            try
            {
                // Create a random identity for the client
                var identity = User.Identity.GetUserId<long>();

                // Create an Chat grant for this token
                var grants = new HashSet<IGrant>
                {
                    new ChatGrant
                    {
                        EndpointId = $"TwilioChat:{identity}:{device}",
                        ServiceSid = chatServiceSid
                    }
                };

                // Create an Access Token generator
                var token = new Token(
                    accountSid,
                    apiKey,
                    apiSecret,
                    identity.ToString(),
                    grants: grants
                );

                return Ok(new
                {
                    identity = identity,
                    token = token.ToJwt(),
                    JsonRequestBehavior.AllowGet
                }.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("twilio/user/delete")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult TwilioUserDelete(TwilioChatViewModel model)
        {
            try
            {
                // Find your Account Sid and Token at twilio.com/console
                // DANGER! This is insecure. See http://twil.io/secure

                TwilioClient.Init(accountSid, authToken);

                UserResource.Delete(
                    pathServiceSid: "ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
                    pathSid: "USXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                );

                return Ok("User is Deleted Successfulyy".SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }


        [System.Web.Http.Route("twilio/add/user")]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult TwilioAddUser(TwilioChatViewModel model)
        {

            // Find your Account Sid and Token at twilio.com/console
            // DANGER! This is insecure. See http://twil.io/secure

            try
            {
                //const string AccountSid = accountSid;
                //const string AuthToken = authToken;
                //string identity = model.Identity;

                TwilioClient.Init(accountSid, authToken);

                var member = MemberResource.Create(
                    identity: model.Identity,
                    pathServiceSid: model.PathSid,
                    pathChannelSid: model.ChannelSid
                );

                return Ok("User is added on twilio.".SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }
    }
}
