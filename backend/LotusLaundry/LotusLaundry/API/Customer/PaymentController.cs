﻿using Customer.Service.Payment;
using Customer.Service.PushNotification;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Success;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Payment;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Domain.UserTwilioCalling;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.Order;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Service.UserTwilioCalling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using Twilio;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V2.Service;

namespace LotusLaundry.API.Customer
{
    public class PaymentController : ApiController
    {
        private string PaymentAuthKey = ConfigurationManager.AppSettings["PaymentAuthKey"];

        //private string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
        //private string chatServiceSid = ConfigurationManager.AppSettings["TwilioChatServiceSid"];
        //private string twilioApiKey = ConfigurationManager.AppSettings["TwilioApiKey"];
        //private string twilioApiSecret = ConfigurationManager.AppSettings["TwilioApiSecret"];
        //private string applicationSid = ConfigurationManager.AppSettings["TwilioVoiceAppSid"];
        //private string pushCredentialsSid = ConfigurationManager.AppSettings["PushCredentialSid"];
        //private string pushCredentialsSidIos = ConfigurationManager.AppSettings["PushCredentialSidIos"];
        private IOrderService _orderService;
        private IPaymentLogService _paymentlogService;
        IPaymentService _paymentService;
        private IPushNotificationService _pushNotificationService;
        public PaymentController(IPushNotificationService pushNotificationService, IPaymentLogService paymentLogService  ,IPaymentService paymentService, IOrderService orderService)
        {
            _paymentService = paymentService;
            _orderService = orderService;
            _paymentlogService = paymentLogService;
            _pushNotificationService = pushNotificationService;
        }

        [System.Web.Http.Route("payment/capability-token")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult generateCapabilityToken()
        {
            try
            {
                var authkey = _paymentService.GetElibilityToken(PaymentAuthKey);
                return Ok(authkey.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("payment/getnewqrcode")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult getNewQRCode(string Orderid)
        {
            try
            {
                //if(Orderid is null)
                //{
                //    return Ok("Please Enter Order Id".ErrorResponse());
                //}
               // var id = Convert.ToInt64(CryptoEngine.Decrypt(Orderid.ToString(), KeyConstant.Key));
                SearchParam param = new SearchParam();
                param.Id = Orderid;
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                decimal amount= order.TotalPrice.GetValueOrDefault();



                var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), amount);
                 _orderService.OrderPaymentUpdate(order.Id.GetValueOrDefault(), QrCode.qrCode.ToString(), "UNPAID", order.CreatedBy.GetValueOrDefault());

                return Ok(QrCode.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }


        [System.Web.Http.Route("payment/getqrcode")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult getQRCode(string Orderid)
        {
            try
            {
                //if (Orderid is null)
                //{
                //    return Ok("Please Enter Order Id".ErrorResponse());
                //}
                // var id = Convert.ToInt64(CryptoEngine.Decrypt(Orderid.ToString(), KeyConstant.Key));
                SearchParam param = new SearchParam();
                param.Id = Orderid;
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();

                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                decimal amount = order.TotalPrice.GetValueOrDefault();



                var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), amount);
                _orderService.OrderPaymentUpdate(order.Id.GetValueOrDefault(), QrCode.qrCode.ToString(), "UNPAID", order.CreatedBy.GetValueOrDefault());

                return Ok(QrCode.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }


        [System.Web.Http.Route("payment/cancelqrcode")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult cancelQRCode(string Orderid)
        {
            try
            {
                //if (Orderid is null)
                //{
                //    return Ok("Please Enter Order Id".ErrorResponse());
                //}
                // var id = Convert.ToInt64(CryptoEngine.Decrypt(Orderid.ToString(), KeyConstant.Key));
                //SearchParam param = new SearchParam();
                //param.Id = Orderid;
                //var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();

                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                decimal amount = 80.00M;



                var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), amount);
              //  _orderService.OrderPaymentUpdate(order.Id.GetValueOrDefault(), QrCode.qrCode.ToString(), "UNPAID", order.CreatedBy.GetValueOrDefault());

                return Ok(QrCode.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("payment/received")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult paymentReceived (PaymentQRcodeModel model)
        {
            try
            {
                if(model.statusCode=="00")
                {
                    var requestJson = JsonConvert.SerializeObject(model);
                    string orderid = model.partnerTxnUid.Substring(model.partnerTxnUid.Length - 6, 6);
                    model.output = requestJson.ToString();
                    model.orderid = Convert.ToInt64(orderid);
                    var response = _paymentlogService.QrLogInsert(model);
                    // model.partnerTxnUid = "LMS000000118001";


                    SearchParam param = new SearchParam();
                    param.Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid, KeyConstant.Key);
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault();

                    if(orders.PaymentType!="QRCODE")
                    {
                        return Ok("Fail".ErrorResponse());
                    }

                    // string myString = (model.partnerTxnUid.Length > 3) ? model.partnerTxnUid.Substring(model.partnerTxnUid.Length - 4, 4) : model.partnerTxnUid;

                    _orderService.OrderPaymentUpdate(Convert.ToInt64(orderid), null, "PAID",null);


                  

                    PushNotificationModel pmodel = new PushNotificationModel();
                    pmodel.CreatedBy = orders.UserId.GetValueOrDefault();
                    pmodel.IsDeleted = false;
                    pmodel.IsActive = true;
                    pmodel.UserId = orders.UserId.GetValueOrDefault();
                    pmodel.Type = PushNotificationType.ORDER_PAYMENT_COMPLETE.ToString(); ;
                    pmodel.Title = PushNotificationMSG.Title.ORDER_PAYMENT_COMPLETE.ToString(); ;
                    //pmodel.Message = PushNotificationMSG.message.ORDER_PAYMENT_COMPLETE.ToString(); ;
                    pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_PAYMENT_COMPLETE.ToString(), orders.Id.ToString().PadLeft(6, '0'));
                    pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orders.Id.ToString(), KeyConstant.Key);
                    //  pmodel.TargetId = orderid;
                    pmodel.UserRole = "CUSTOMER";
                    _pushNotificationService.PushNotificationInsert(pmodel);


                    PushNotificationModel dmodel = new PushNotificationModel();
                    dmodel.CreatedBy = orders.DriverDetails.FirstOrDefault().Id;
                    dmodel.IsDeleted = false;
                    dmodel.IsActive = true;
                    dmodel.UserId = orders.DriverDetails.FirstOrDefault().Id;
                    dmodel.Type = PushNotificationType.ORDER_PAYMENT_COMPLETE.ToString(); ;
                    dmodel.Title = PushNotificationMSG.Title.ORDER_PAYMENT_COMPLETE.ToString(); ;
                   // dmodel.Message = PushNotificationMSG.message.ORDER_PAYMENT_COMPLETE.ToString(); ;

               dmodel.Message=     string.Format(PushNotificationMSG.message.ORDER_PAYMENT_COMPLETE.ToString(), orders.Id.ToString().PadLeft(6, '0'));

                    dmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orders.Id.ToString(), KeyConstant.Key);
                    //  pmodel.TargetId = orderid;
                    dmodel.UserRole = "DRIVER";
                    _pushNotificationService.PushNotificationInsert(dmodel);



                }
                else
                {
                    var requestJson = JsonConvert.SerializeObject(model);
                    string orderid = model.partnerTxnUid.Substring(model.partnerTxnUid.Length - 6, 6);
                    model.output = requestJson.ToString();
                 
                    model.orderid = Convert.ToInt64(orderid);
                    var response = _paymentlogService.QrLogInsert(model);
                    SearchParam param = new SearchParam();
                    param.Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid, KeyConstant.Key);
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault();

                    PushNotificationModel pmodel = new PushNotificationModel();
                    pmodel.CreatedBy = orders.UserId.GetValueOrDefault();
                    pmodel.IsDeleted = false;
                    pmodel.IsActive = true;
                    pmodel.UserId = orders.UserId.GetValueOrDefault();
                    pmodel.Type = PushNotificationType.ORDER_PAYMENT_FAILURE.ToString(); ;
                    pmodel.Title = PushNotificationMSG.Title.ORDER_PAYMENT_FAILURE.ToString(); ;
                    //pmodel.Message = PushNotificationMSG.message.ORDER_PAYMENT_FAILURE.ToString(); ;
                    pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_PAYMENT_FAILURE.ToString(), orders.Id.ToString().PadLeft(6, '0'));


                    pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                    //  pmodel.TargetId = orderid;
                    pmodel.UserRole = "CUSTOMER";
                    _pushNotificationService.PushNotificationInsert(pmodel);


                    PushNotificationModel dmodel = new PushNotificationModel();
                    dmodel.CreatedBy = orders.DriverId;
                    dmodel.IsDeleted = false;
                    dmodel.IsActive = true;
                    dmodel.UserId = orders.DriverId.GetValueOrDefault();
                    pmodel.Type = PushNotificationType.ORDER_PAYMENT_FAILURE.ToString(); ;
                    pmodel.Title = PushNotificationMSG.Title.ORDER_PAYMENT_FAILURE.ToString(); ;
                    //pmodel.Message = PushNotificationMSG.message.ORDER_PAYMENT_FAILURE.ToString(); ;

                    pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_PAYMENT_FAILURE.ToString(), orders.Id.ToString().PadLeft(6, '0'));


                    dmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                    //  pmodel.TargetId = orderid;
                    dmodel.UserRole = "DRIVER";
                    _pushNotificationService.PushNotificationInsert(dmodel);

                    return Ok("Order Payment Failure".ErrorResponse());

                }


                return Ok("Order Payment Successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }


    }
}