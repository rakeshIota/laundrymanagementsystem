﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.UserTwilioCalling;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Service.UserTwilioCalling;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using Twilio;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V2.Service;

namespace LotusLaundry.API.Customer
{
    public class TwilioVoiceController : ApiController
    {
        private string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
        private string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
        private string chatServiceSid = ConfigurationManager.AppSettings["TwilioChatServiceSid"];
        private string twilioApiKey = ConfigurationManager.AppSettings["TwilioApiKey"];
        private string twilioApiSecret = ConfigurationManager.AppSettings["TwilioApiSecret"];
        private string applicationSid = ConfigurationManager.AppSettings["TwilioVoiceAppSid"];
        private string pushCredentialsSid = ConfigurationManager.AppSettings["PushCredentialSid"];
        private string pushCredentialsSidIos = ConfigurationManager.AppSettings["PushCredentialSidIos"];


        IUserCallingService _userCallingService;

        public TwilioVoiceController(IUserCallingService userCallingService)
        {
            _userCallingService = userCallingService;
        }

        [System.Web.Http.Route("twilio/voice/capability-token")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult generateCapabilityToken(string identity, string deviceType, bool allowIncoming = true)
         {
            try
            {
                var user = (identity.Split('_')[1]);
                //var userId = Convert.ToInt64(identity.Split('_')[1]);


                var userId =  Convert.ToInt64(CryptoEngine.Decrypt(user.ToString(), KeyConstant.Key)) ;

                _userCallingService.UserCallingStatusDeactivate(userId);
                // Create a Voice grant for this token
                var grant = new VoiceGrant();
                grant.OutgoingApplicationSid = applicationSid;
                grant.PushCredentialSid = deviceType == "iOS" ? pushCredentialsSidIos : pushCredentialsSid;
                // Optional: add to allow incoming calls
                grant.IncomingAllow = allowIncoming;
                var grants = new HashSet<IGrant>
                    {
                        { grant }
                    };

                // Create an Access Token generator
                var token = new Token(
                    accountSid,
                    twilioApiKey,
                    twilioApiSecret,
                    identity,
                    expiration: DateTime.Now.AddHours(23),
                    grants: grants);

                return Ok(new
                {
                    token = token.ToJwt(),
                    JsonRequestBehavior.AllowGet
                }.SuccessResponse());
            }
            catch (Exception ex)
            {
                var message = ex.Message ?? ex.InnerException.Message;
                return Ok(message.ErrorResponse());
            }
        }

        [System.Web.Http.Route("twilio/voice/call-status")]
        //[System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        public IHttpActionResult UpdateUserCalling(UserTwilioCallingModel model)
        {
            _userCallingService.UserCallingStatusUpdate(model);
            return Ok("Status Update Successfully".SuccessResponse());
        }

        [System.Web.Http.Route("twilio/voice/call-status-deactivate")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult UpdateUserCallingStatuDeactivate(long? userId)
        {
            _userCallingService.UserCallingStatusDeactivate(userId);
            return Ok("User calling status is Deactivate".SuccessResponse());
        }



        /*get all channels and delete */

        //[System.Web.Http.Route("twilio/channel/delete")]
        //[System.Web.Http.HttpGet]
        //public IHttpActionResult DeleteTwilioChannel()
        //{
        //    try
        //    {
        //        TwilioClient.Init(accountSid, authToken);

        //        var channels = ChannelResource.Read(pathServiceSid: chatServiceSid);

        //        foreach (var record in channels)
        //        {
        //            ChannelResource.Delete(pathServiceSid: chatServiceSid, pathSid: record.Sid);
        //            //return Ok("delete");
        //        }

        //        return Ok("Channel deleted from the Twilio.");
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok("Error :" + ex.Message);
        //    }

        //}
    }
}