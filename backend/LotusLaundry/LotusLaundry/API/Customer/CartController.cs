using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using Customer.Service.Cart;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.Cart;
using LotusLaundry.Framework.ViewModels.Customer.CartItem;
using LotusLaundry.Common.Extensions;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api/application")]
    public class CartController : ApiController
    {
        private ICartService _cartService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CartController(ICartService cartService, IExceptionService exceptionService)
        {
            _cartService = cartService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Cart
        /// </summary>
        /// <returns></returns>
        //[Route("Cart")]
        //[CustomAuthorize("ROLE_ADMIN")]
        //[HttpPost]
        //public IHttpActionResult GetAllCart(SearchParam param)
        //{
        //	try{
        //		param = param ?? new SearchParam();
        //    	var cart = _cartService.SelectCart(param).Select(x => x.ToViewModel()); ;
        //    	return Ok(cart.SuccessResponse());
        //    }catch(Exception ex){
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Select);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.Cart_Select).ToString().ErrorResponse());
        //    }
        //}

        /// <summary>
        /// Api use for get cart by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cart/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCart(string id)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;
                var cart = _cartService.SelectCart(param).FirstOrDefault().ToViewModel();
                if (cart.Id == null)
                {
                    int countOfItem = 0;
                    return Ok(countOfItem.SuccessResponse(""));
                    // return Ok("No record found".ErrorResponse());
                }
                return Ok(cart.SuccessResponse());

            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>



        [Route("cart")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpGet]
        [ValidateModelState]
        public IHttpActionResult GetCustomerCart()
        {
            try
            {
                SearchParam param = new SearchParam();
                param.UserId = User.Identity.GetUserId<long>();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                var cart = _cartService.SelectCart(param).FirstOrDefault().ToViewModel();
                if (cart.Id == null)
                {
                   // return Ok("No cart found".ErrorResponse());
                    string countOfItem = null;
                    return Ok(countOfItem.SuccessResponse("Cart does not exist"));
                }
                return Ok(cart.SuccessResponse());

            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Single_Select).ToString().ErrorResponse());
            }
        }

        [Route("cart")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCart(List<CartItemCustomerViewModel> model)
        {
            try
            {
                //set tenantid

                var cartItems = model.Select(y => y.ToModel()).ToList();
                var response = _cartService.CartItemInsert(cartItems, ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>());
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }

                return Ok("Cart item saved successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Insert).ToString().ErrorResponse());
            }
        }



        [Route("cart")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult CartItemUpdate(CartItemCustomerViewModel model)
        {
            try
            {
                //set tenantid
                var response = _cartService.CartItemUpdate(model.ServiceId, model.ProductId, ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>(), model.Quantity);
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }

                // get updated cart 
                SearchParam param = new SearchParam();
                param.UserId = User.Identity.GetUserId<long>();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                var cart = _cartService.SelectCart(param).FirstOrDefault().ToViewModel();
                if (cart.Id == null)
                {
                    return Ok("No record found".ErrorResponse());
                }
                return Ok(cart.SuccessResponse("Cart item updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.CartItem_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Update).ToString().ErrorResponse());
            }
        }


        /// <summary>
        /// Api use for update cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[Route("cart")]
        //[CustomAuthorize("ROLE_ADMIN")]
        //[HttpPut]
        //[ValidateModelState]
        //public IHttpActionResult UpdateCart(CartViewModel model)
        //{
        //	try{
        //		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        //		model.UpdatedBy = User.Identity.GetUserId<long>();
        //     _cartService.CartUpdate(model.ToModel());
        //     return Ok(model.Id.SuccessResponse("Cart updated successfully"));
        //    }
        //    catch (Exception ex)
        //    {
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Update);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.Cart_Update).ToString().ErrorResponse());
        //    }
        //}

        /// <summary>
        /// Api use for delete cart by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cart/item/{cartItemId}")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpDelete]
        public IHttpActionResult DeleteCartItem(string cartItemId)
        {
            try
            {
                //set tenantid
                var response = _cartService.CartItemDelete(cartItemId, ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>(), 0);
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }

                return Ok("Cart item deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.CartItem_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Delete).ToString().ErrorResponse());
            }
        }

        [Route("cart/update/delivery")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPut]
        public IHttpActionResult DeliveryType(SearchParam param)
        {
            try
            {
                //set tenantid
                var response = _cartService.CartDeliveryTypeUpdate(param.Type, ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>());
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }
                return Ok("Delivery type updated successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_DeliverType_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_DeliverType_Update).ToString().ErrorResponse());
            }
        }

        [Route("cart/item/count")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpGet]
        public IHttpActionResult GetCartItemQuantity()
        {
            try
            {
                //set tenantid
                var response = _cartService.GetCartItemQuantity(ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>());
                if (response.Status == false)
                {
                    //      return Ok(response.Message.ErrorResponse());
                    int countOfItem = 0;
                    return Ok(countOfItem.SuccessResponse(""));
                }
                return Ok(Convert.ToInt64(response.Message).SuccessResponse(""));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Item_Count);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Item_Count).ToString().ErrorResponse());
            }
        }


        /// 

        //cart/update/pickup/time
        //[Route("cart/update/pickup/time")]
        //[CustomAuthorize(UserRole.Customer)]
        //[HttpPut]
        //public IHttpActionResult PickupDateAndTime(SearchParam param)
        //{
        //    try
        //    {
        //        //set tenantid
        //        var response = _cartService.CartPickupDateAndTimeeUpdate( ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>(), param.pickupDate, param.pickupSlot);
        //        if (response.Status == false)
        //        {
        //            return Ok(response.Message.ErrorResponse());
        //        }
        //        return Ok("Pickup time updated successfully".SuccessResponse());
        //    }
        //    catch (Exception ex)
        //    {
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_DeliverType_Update);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.Cart_DeliverType_Update).ToString().ErrorResponse());
        //    }
        //}


        [Route("cart/update/delivery/time")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPut]
        public IHttpActionResult DeliveryDateAndTime(SearchParam param)
        {
            try
            {
                //set tenantid
                var response = _cartService.CartPickupDateAndTimeeUpdate( ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>(), param.pickupDate,param.deliveryDate, param.pickupSlot,param.deliverySlot,param.laundryInstruction);
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }
                return Ok("Delivery time updated successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_DeliverType_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_DeliverType_Update).ToString().ErrorResponse());
            }
        }


    }
}

