using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.Order;
using Customer.Service.Cart;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Service.Stripe;
using LotusLaundry.Service.OrderLog;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Framework.ViewModels.Customer.UserCardDetail;
using LotusLaundry.Facade;
using LotusLaundry.Service.OrderItem;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Service.Users;
using Customer.Service.UserAddress;
using Customer.Service.AppSetting;
using Customer.Service.PushNotification;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Common.Success;
using LotusLaundry.Framework.ViewModels.Customer.PushNotification;
using Twilio.Jwt.AccessToken;
using LotusLaundry.Core.GeneratePDF;
using Customer.Service.Payment;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api/application")]
    
    public class CustomerOrderController : ApiController
    {
        private IOrderService _orderService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        private ICartService _cartService;
        private IOrderLogService _orderlogService;
        private IPaymentLogService _paymentlogService;
        private IStripeService _stripeService;
        private OrderManager _orderManager;
        private IOrderItemService _orderitemService;
        private IEmailConfigurationService _emailConfiguration;
        private IUsersService _usersService;
        private IUserAddressService _useraddressService;
        private IAppSettingCustomerService _appSettingCustomerService;
        private IPushNotificationService _pushNotificationService;
        private IPaymentService _paymentService;
        public CustomerOrderController(IPaymentService paymentService,IPushNotificationService pushNotificationService,IAppSettingCustomerService appSettingCustomerService,IUserAddressService useraddressService, IUsersService usersService,IEmailConfigurationService emailConfiguration,IOrderItemService orderitemService,IPaymentLogService paymentlogService, ICartService cartService,IOrderService orderService, IExceptionService exceptionService, IStripeService stripeService, IOrderLogService orderlogService)
        {
            _appSettingCustomerService = appSettingCustomerService;
            _useraddressService = useraddressService;
            _usersService = usersService;
            _orderitemService = orderitemService;
            _paymentlogService = paymentlogService;
            _orderlogService = orderlogService;
            _cartService = cartService;
            _orderService = orderService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _stripeService = stripeService;
            _pushNotificationService = pushNotificationService;
            _paymentService = paymentService;
            _orderManager = new OrderManager(paymentService,pushNotificationService, appSettingCustomerService,useraddressService, usersService, emailConfiguration,orderitemService, paymentlogService, cartService, orderService, exceptionService, stripeService, orderlogService);
        }

        /// <summary>
        ///  Api use for get all Order
        /// </summary>
        /// <returns></returns>
        [Route("Orders")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetAllOrders(SearchParam param)
        {
        	try
            {
        		param = param ?? new SearchParam();
            	var orders = _orderService.SelectOrder(param).Select(x => x.ToViewModel()); ;
            	return Ok(orders.SuccessResponse());
            }
            catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }

       

        // ----------------new work
     

        [Route("order/complete")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPost]     

        public IHttpActionResult SaveMyOrder(UserCardDetailViewModel UserCardModel)
        {
            try
            {
                #region  ======= Request Validation ==========
                if (UserCardModel==null)
                {
                    return Ok(string.Format(ErrorMessage.OrderError.INAVLID_REQUEST).ErrorResponse());
                }
                if(UserCardModel.PaymentType==null)
                {
                    return Ok(string.Format(ErrorMessage.OrderError.INAVLID_PAYMENTTYPE).ErrorResponse());
                }
               
                if (UserCardModel.DeliveryAddress == null)
                {
                    return Ok(string.Format(ErrorMessage.OrderError.INAVLID_ADDRESS).ErrorResponse());
                }
                #endregion


                var id = User.Identity.GetUserId<long>();
                SearchParam param = new SearchParam();
                param.UserId = User.Identity.GetUserId<long>();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);

              
                var result = _orderManager.ProcessMyOrder(id, param, UserCardModel);


                if (result.Status)
                {

                    OrderResponseModel response = new OrderResponseModel();
                  
                    response.orderId = result.Message;
                    response.orderNumber = result.SecMessage.ToString().PadLeft(6, '0');                    
                    return Ok(response.SuccessResponse("Order save successfully"));
                }

                return Ok(result.ErrorResponse());


            }
            catch (Exception ex)
            {

                return Ok(ex.ErrorResponse());

            }
        }



        //api/application/my/orders
        [Route("my/home/orders")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetMyActiveOrders(SearchParam param)
        {
            try
            {
                var id = User.Identity.GetUserId<long>();
                param = param ?? new SearchParam();
                param.UserId = id;
                var orders = _orderService.SelectMyActiveOrder(param).Select(x => x.ToDetailsViewModel()); ;
              
                return Ok(orders.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }



        //api/application/my/orders
        [Route("my/orders")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetMyOrders(SearchParam param)
        {
            try
            {
                var id = User.Identity.GetUserId<long>();
                param = param ?? new SearchParam();
                param.UserId = id;
                var orders = _orderService.SelectMyOrder(param).Select(x => x.ToViewModel()); 
                return Ok(orders.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }

        [Route("order/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer,UserRole.Driver,UserRole.OPERATIONMANAGER)]
        [HttpGet]
        public IHttpActionResult GetMyOrderDetails(string id, string needDriverLoad = null)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;
                if (needDriverLoad == null || needDriverLoad=="false")
                {
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToBasicViewModel();
                    return Ok(orders.SuccessResponse());
                }
                else
                {
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToDetailsViewModel();
                    return Ok(orders.SuccessResponse());
                }
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }



        [Route("activeorder/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer, UserRole.Driver, UserRole.OPERATIONMANAGER)]
        [HttpGet]
        public IHttpActionResult GetMyOrderDetailswithActiveCount(string id, string needDriverLoad=null)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;
                if (needDriverLoad == null || needDriverLoad == "false")
                {
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToBasicViewModel();
                    return Ok(orders.SuccessResponse());
                }
                else
                {
                    var orders = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToDetailsViewModel();
                    return Ok(orders.SuccessResponse());
                }
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }


        [Route("order/{id}/cancel")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer,UserRole.OPERATIONMANAGER)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult CancelOrder(string  id)
        {
            try
            {
                var roles = ManageClaims.GetUserRole();
                var updatedBy = User.Identity.GetUserId<long>();
                var result = _orderManager.CancelOrder(id, updatedBy,roles.ToString());

                if(result.Status)
                {
                    
                    var data = result.Message;
                    return Ok(data.ToString().SuccessResponse());
                }

                return Ok(result.ErrorResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        [Route("invoice/{id}")]
       // [CustomAuthorize("ROLE_ADMIN", UserRole.Customer,UserRole.OPERATIONMANAGER)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult InvoiceOrder(string id)
        {
            try
            {
                var updatedBy = User.Identity.GetUserId<long>();
                var result = _orderManager.GetInvoice(id);

                if (result.Status)
                {

                    var data = result.Message;
                    return Ok(data.ToString().SuccessResponse());
                }

                return Ok(result.ErrorResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }


        


    }
}