﻿using Customer.Service.Product;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.Product;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    public class ProductCustomerController : ApiController
    {
        private IProductCustomerService _productCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProductCustomerController(IProductCustomerService productCustomerService, IExceptionService exceptionService)
        {
            _productCustomerService = productCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("api/application/service/{serviceId}/products")]
        [CustomAuthorize(UserRole.Customer, UserRole.Driver, UserRole.Admin, UserRole.OPERATIONMANAGER)]
        [HttpGet]
        public IHttpActionResult GetProductsByService(string serviceId, string type = null)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.Gender = type; //?? Gender.MALE.ToString();
                param.ServiceIdStr = serviceId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var products = _productCustomerService.ServiceProductSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(products.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Produt_Select_Customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Produt_Select_Customer).ToString().ErrorResponse());
            }
        }
    }
}
