using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using Customer.Service.UserAddress;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.Customer.UserAddress;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Facade;
using LotusLaundry.Framework.ViewModels.Users;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api/application")]
    public class UserAddressController : ApiController
    {
        private IUserAddressService _useraddressService;
        private IExceptionService _exceptionService;
        private UserAddressManager _userAddressManager;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public UserAddressController(IUserAddressService useraddressService, IExceptionService exceptionService)
        {
            _useraddressService = useraddressService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _userAddressManager = new UserAddressManager();
        }


        [Route("user/basic/detail")]
        
        [CustomAuthorize(UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetDeliveryAddresses(SearchParam param)
        {
            try
            {
                          
                UsersbasicViewModel usersAdressStatus = new UsersbasicViewModel();
                param = _userAddressManager.filterparam(param);
                usersAdressStatus = _userAddressManager.CheckUserAddressStatus();

                var useraddresses = _useraddressService.SelectAddressInActivePostalCode(param).Select(x => x.ToViewModel()); ;
                if (useraddresses == null)
                {
                    usersAdressStatus.InServiceArea = false;
                }
                if (useraddresses.Count() >0)
                { 
                        usersAdressStatus.InServiceArea = true;
                }
                
                return Ok(usersAdressStatus.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Select).ToString().ErrorResponse());
            }
        }


        [Route("useraddress")]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN, UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetAlluserAddresses(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
              
                // param = _userAddressManager.filterparam(param);                

                var useraddresses = _useraddressService.SelectUserAddress(param).Select(x => x.ToViewModel()); ;
                return Ok(useraddresses.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get useraddress by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("address/{type}")]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN, UserRole.Customer)]
        [HttpGet]
        public IHttpActionResult GetUserAddress(string type)
        {
        	try{
	        	SearchParam param = new SearchParam();
                param.Type = type;
                param.UserId = User.Identity.GetUserId<long>();
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
              
               

                var useraddresses = _useraddressService.SelectUserAddress(param).Select(x => x.ToViewModel()); ;
                return Ok(useraddresses.SuccessResponse());

            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save useraddress
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("address")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveUserAddress(CustomerAddressViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.CreatedBy = model.UpdatedBy =  model.UserId = User.Identity.GetUserId<long>();
                model.IsActive = true;
                var responseId = _useraddressService.UserAddressInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.USERADDRESS + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("User Address save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update useraddress
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("address")]
        [CustomAuthorize(UserRole.Customer)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateUserAddress(CustomerAddressViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy =  model.UserId = User.Identity.GetUserId<long>();
	            _useraddressService.UserAddressUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("User Address updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete useraddress by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[Route("useraddress/{id}")]
        //[CustomAuthorize(UserRole.Customer)]
        //[HttpDelete]
        //public IHttpActionResult DeleteUserAddress(string id)
        //{
        //	try{
        //     UserAddressViewModel model = new UserAddressViewModel();
        //     model.TenantId = ManageClaims.GetUserClaim().TenantId;
        //     model.Id = id;
        //     model.IsDeleted = true;
        //     model.UpdatedBy = User.Identity.GetUserId<long>();
        //     _useraddressService.UserAddressUpdate(model.ToModel());
        //     return Ok("UserAddress Deleted successfully".SuccessResponse());
        //    }
        //    catch (Exception ex)
        //    {
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Delete);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.UserAddress_Delete).ToString().ErrorResponse());
        //    }
        //}


        [Route("customeraddress")]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN, UserRole.Customer)]
        [HttpPost]
        public IHttpActionResult GetcustomerAddresses(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();

                // param = _userAddressManager.filterparam(param);                

                var useraddresses = _useraddressService.SelectCustomerAddress(param).Select(x => x.ToViewModel()); ;
                return Ok(useraddresses.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Select).ToString().ErrorResponse());
            }
        }

        //[Route("deliveryaddress")]
        //[CustomAuthorize(UserRole.Customer)]
        //[HttpPost]
        //[ValidateModelState]
        //public IHttpActionResult Savedeliveryaddress(CustomerAddressViewModel model,string OrderId)
        //{
        //    try
        //    {
        //        //set tenantid
        //        model.TenantId = ManageClaims.GetUserClaim().TenantId;
        //        model.CreatedBy = model.UpdatedBy = model.UserId = User.Identity.GetUserId<long>();
        //        model.IsActive = true;
        //        var responseId = _useraddressService.DeliveryAddressInsert(model.ToModel());

        //        return Ok(CryptoEngine.Encrypt(FileType.USERADDRESS + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("User Address save successfully"));
        //    }
        //    catch (Exception ex)
        //    {
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Insert);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.UserAddress_Insert).ToString().ErrorResponse());
        //    }
        //}

    }
}

