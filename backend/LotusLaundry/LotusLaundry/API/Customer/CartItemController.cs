using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.CartItem;
using Customer.Service.CartItem;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api")]
    public class CartItemController : ApiController
    {
        private ICartItemService _cartitemService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CartItemController(ICartItemService cartitemService, IExceptionService exceptionService)
        {
            _cartitemService = cartitemService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all CartItem
        /// </summary>
        /// <returns></returns>
        [Route("CartItems")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCartItems(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var cartitems = _cartitemService.SelectCartItem(param).Select(x =>x.ToViewModel()); ;
            	return Ok(cartitems.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get cartitem by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cartitem/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCartItem(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var cartitem = _cartitemService.SelectCartItem(param).FirstOrDefault().ToViewModel();
	            if (cartitem.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(cartitem.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save cartitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cartitem")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCartItem(CartItemViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _cartitemService.CartItemInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CARTITEM + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("CartItem save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update cartitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cartitem")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCartItem(CartItemViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartitemService.CartItemUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("CartItem updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete cartitem by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cartitem/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCartItem(string id)
        {
        	try{
	            CartItemViewModel model = new CartItemViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartitemService.CartItemUpdate(model.ToModel());
	            return Ok("CartItem Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Delete).ToString().ErrorResponse());
            }
        }


        [Route("application/cart/service/{id}")]
        [CustomAuthorize("ROLE_ADMIN",UserRole.Customer)]
        [HttpDelete]
        public IHttpActionResult DeleteCartItemByService(string id)
        {
            try
            {
                CartItemViewModel model = new CartItemViewModel();
                model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.serviceIdCode = id;
                model.IsDeleted = true;
                model.UpdatedBy = User.Identity.GetUserId<long>();
               
                _cartitemService.CartItemUpdateByService(model.ToModel());
                return Ok("CartItem Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItem, (int)ErrorFunctionCode.CartItem_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Delete).ToString().ErrorResponse());
            }
        }

    }
}

