﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Common.Success;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Common.Enums;
using LotusLaundry.Core.Mailer;
using LotusLaundry.Core.SMS;
using LotusLaundry.Models;
using LotusLaundry.Service.Users;
using Newtonsoft.Json;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Service.PasswordLog;
using Customer.Service.UserAddress;
using LotusLaundry.Domain;
using LotusLaundry.Framework.ViewModels.Customer.UserAddress;
using LotusLaundry.Framework.ViewModels.Customer.Users;
using Customer.Service.Customer;
using LotusLaundry.Facade;

using System.Configuration;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserDevice;
using LotusLaundry.Domain.UserDevice;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Extra;

namespace LotusLaundry.API.Customer
{

    public class AccountCustomerController : ApiController
    {

        #region Variable and other declarations 

        private ManageApplicationUserModel _manageApplicationUserModel;
        private IEmailConfigurationService _emailConfiguration;
        private IUsersService _usersService;
        private IPasswordLogService _passwordlogService;
        private IUserAddressService _useraddressService;
        private ICustomerService _customerService;
        private AccountManager _accountManager;
  


        public AccountCustomerController(IEmailConfigurationService emailConfiguration, ICustomerService customerService,
            IUsersService usersService, IPasswordLogService passwordlogService, IUserAddressService useraddressService)
        {
            _emailConfiguration = emailConfiguration;
            _manageApplicationUserModel = new ManageApplicationUserModel();
          
            _passwordlogService = passwordlogService;
            _useraddressService = useraddressService;
            _customerService = customerService;
            _accountManager = new AccountManager();
            _usersService = usersService;
          
        }

        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        #endregion


        [Route("app/customer/register")]
        [HttpPost]
        public IHttpActionResult RegisterUser([FromBody] UsersViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(ModelState.ErrorResponse());
                }

                

                if (UserManager.Users.Any(x => (x.Email.Equals(model.Email) ) && x.EmailConfirmed && !x.IsDeleted))
                {
                    if (UserManager.Users.Any(x => (x.PhoneNumber.Equals(model.MobileNo)) && x.EmailConfirmed && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.PHONE_ALREADY_EXISTS, model.MobileNo).ErrorResponse());
                    }
                    return Ok(string.Format(ErrorMessage.AccountError.EMAIL_ALREADY_REGISTERED, model.Email).ErrorResponse());
                }
                if (UserManager.Users.Any(x => (x.UserName.Equals(model.Email) && !x.EmailConfirmed && !x.IsDeleted)))
                {
                    var oldUser = UserManager.FindByName(model.Email);
                    oldUser.IsDeleted = true;
                    oldUser.Email = oldUser.UserName = oldUser.Email + "deleted" + CommonMethods.GetUniqueKey(6);
                    UserManager.Update(oldUser);
                }
                var user = _manageApplicationUserModel.UserViewModelToCustomerApplicationModel(model);


                user.OTP= _accountManager.GetOtp();


                user.OTP = _accountManager.GetOtp();
                user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);

                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                IdentityResult result = UserManager.Create(user);
                if (!result.Succeeded && result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        return Ok(error.ErrorResponse());
                    }
                }
                UserManager.AddToRole(user.Id, UserRole.Customer);
                

                if (model.Locality.ToUpper() == "RESIDENT")
                {
                    _passwordlogService.OtpLogInsert(user.Id);
                    if( _accountManager.IsAllowSms())
                    {
                        SendSms sms = new SendSms();
                        string msg =  user.OTP.ToString();
                        sms.SendMessage(model.MobileNo, msg, model.CountryCode);
                    }
                    
                    SendEmailForEmailConfirm(user);
                    return Ok(SuccessMessage.AccountSuccess.MOBILEANDEMAIL_VERIFY.SuccessResponse());
                }
                if (model.Locality.ToUpper() == "TOURIST")
                {
                    SendEmailForEmailConfirm(user);
                    return Ok(SuccessMessage.AccountSuccess.MOBILE_VERIFY.SuccessResponse());
                }

                return Ok(SuccessMessage.AccountSuccess.MOBILE_VERIFY.SuccessResponse());


            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        #region =====================   Verifictaions region =============
        [Route("app/customer/verify-otp")]
        [HttpPost]
        public IHttpActionResult VerifyOtp(UsersViewModel model)
        {
            try
            {

                var user = UserManager.FindByName(model.UserName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                
                if (model.OTP == user.OTP && user.OTPValidTill >= DateTime.UtcNow)
                {
                    user.PhoneNumberConfirmed = true;
                    user.IsActive = true;
                    user.OTPValidTill = DateTime.UtcNow.AddDays(-10);
                    UserManager.Update(user);
                    // SendEmailForEmailConfirm(user);
                    _passwordlogService.OtpLogDelete(user.Id);

                    return Ok(SuccessMessage.AccountSuccess.MOBILE_VERIFY_CONFIRM.SuccessResponse());
                }
                
                return Ok(ErrorMessage.AccountError.INVALID_OTP.ErrorResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        


        [Route("app/customer/verify-resetOtp")]
        [HttpPost]
        public IHttpActionResult VerifyResetOtp(UsersViewModel model)
        {
            try
            {

                var user = UserManager.FindByName(model.UserName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                //if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                //{
                //    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                //}
                
                if (model.OTP == user.OTP && user.OTPValidTill >= DateTime.UtcNow)
                {
                    user.PhoneNumberConfirmed = true;
                    user.IsActive = true;
                    user.OTPValidTill = DateTime.UtcNow.AddDays(-10);
                    UserManager.Update(user);
                    
                    string code = UserManager.GeneratePasswordResetToken(user.Id);
                    code = HttpUtility.UrlEncode(code);

                    UserCodeViewModel uc = new UserCodeViewModel();
                    uc.uniqueCode = user.UniqueCode;
                    uc.Code = code;

                    return Ok(uc.SuccessResponse());
                }

                return Ok(ErrorMessage.AccountError.INVALID_OTP.ErrorResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("app/customer/email/verify-otp")]
        [CustomAuthorize( UserRole.Customer,UserRole.Driver)]
        [HttpPost]
        public IHttpActionResult emailVerifyOtp(UsersViewModel model)
        {
            try
            {

                var user = UserManager.FindByName(model.UserName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }

                if (model.OTP == user.OTP && user.OTPValidTill >= DateTime.UtcNow)
                {
                    user.PhoneNumberConfirmed = true;
                    user.IsActive = true;
                    user.OTPValidTill = DateTime.UtcNow.AddDays(-10);
                    UserManager.Update(user);
                    // SendEmailForEmailConfirm(user);
                    _passwordlogService.OtpLogDelete(user.Id);

                    return Ok(SuccessMessage.AccountSuccess.MOBILE_VERIFY_CONFIRM.SuccessResponse());
                }

                return Ok(ErrorMessage.AccountError.INVALID_OTP.ErrorResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        [Route("app/customer/resend-otp")]
        [HttpPost]   
        public IHttpActionResult ResendOtp(string userName)
        {
            try 
            {

                var user = UserManager.FindByName(userName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
                }
                if (user.IsDeleted)
                {
                    return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
                }

                if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }

                //if (user.PhoneNumberConfirmed == true)
                //{
                //    return Ok(ErrorMessage.AccountError.PHONENUMBER_ALREADY_CONFIRM.ErrorResponse());
                //}

                var otpCount = _passwordlogService.OtpLogSelect(user.Id);
                  if (otpCount.Count() == 5)
                {
                    var date = DateTime.UtcNow - otpCount[0].CreatedOn;
                    if (date.Value.Minutes >= 30)
                    {
                        _passwordlogService.OtpLogDelete(user.Id);
                    }
                    else {
                        return Ok(ErrorMessage.AccountError.OTP_LIMIT.ErrorResponse());
                    }
                }


                user.OTP = _accountManager.GetOtp();

              
                user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);
                _passwordlogService.OtpLogInsert(user.Id);
                UserManager.Update(user);

                SendSms sms = new SendSms();
                string msg = "OTP =" + user.OTP.ToString();
                sms.SendMessage(user.PhoneNumber, msg,user.CountryCode);

                return Ok(SuccessMessage.AccountSuccess.RESEND_OTP.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        [Route("app/customer/resend-emailotp")]
        [HttpPost]
        public IHttpActionResult ResendEmailOtp(string userName)
        {
            try
            {

                var user = UserManager.FindByName(userName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
                }
                if (user.IsDeleted)
                {
                    return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
                }

                //if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                //{
                //    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                //}

                //if (user.PhoneNumberConfirmed == true)
                //{
                //    return Ok(ErrorMessage.AccountError.PHONENUMBER_ALREADY_CONFIRM.ErrorResponse());
                //}

                var otpCount = _passwordlogService.OtpLogSelect(user.Id);
                if (otpCount.Count() == 5)
                {
                    var date = DateTime.UtcNow - otpCount[0].CreatedOn;
                    if (date.Value.Minutes >= 30)
                    {
                        _passwordlogService.OtpLogDelete(user.Id);
                    }
                    else
                    {
                        return Ok(ErrorMessage.AccountError.OTP_LIMIT.ErrorResponse());
                    }
                }
                user.OTP = _accountManager.GetOtp();
                user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);
                _passwordlogService.OtpLogInsert(user.Id);
                UserManager.Update(user);

                //SendSms sms = new SendSms();
                //string msg = "OTP =" + user.OTP.ToString();
                //sms.SendMessage(user.PhoneNumber, msg);

                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.EmailOTPVerification.ToString());
                var emailBody = string.Format(configData.ConfigurationValue, user.FirstName, user.OTP);
                MailSender.SendEmail(user.Email, EmailConstants.OTP_VERIFICATION, emailBody).Wait();


                return Ok(SuccessMessage.AccountSuccess.RESEND_OTP.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }




//        [Route("app/customer/confirm-account")]
        [Route("register/confirm")]

        [HttpGet]
        public IHttpActionResult ConfirmAccount(string uniqueCode,string code)
        {
            try
            {
                
                code = code.Replace(" ", "+");
                code = System.Web.HttpUtility.UrlEncode(code);
                code = code.Replace("+", "%20");
                var callbackUrl = UrlConstants.baseUrl + "/register/failure";

                if (string.IsNullOrWhiteSpace(code))
                {
                    //return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                    return Redirect(callbackUrl);
                }
                var user = _usersService.SelectUserByUniqueCode(uniqueCode);
                if (user == null)
                {
                   // return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                    return Redirect(callbackUrl);
                }

                if (user.EmailConfirmed)
                {
                    return Redirect(callbackUrl);
                   // return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                }

                IdentityResult result = this.UserManager.ConfirmEmail(Convert.ToInt64(user.Id), HttpUtility.UrlDecode(code));
                if (result.Succeeded)
                {
                    var appUser = UserManager.FindByEmail(user.Email);
                    appUser.AccessFailedCount = 0;
                    appUser.EmailConfirmed = true;
                    appUser.IsActive = true;
                    UserManager.Update(appUser);

                    SendWelcomeEmail(appUser.Email, appUser.FirstName + " " + appUser.LastName);

                     callbackUrl = UrlConstants.baseUrl + "/register/success";
                    return Redirect(callbackUrl);
                }
                else
                {
                     callbackUrl = UrlConstants.baseUrl + "/register/failure";
                    return Redirect(callbackUrl);
                    //return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        #endregion

        

        #region ========================  LOGIN

        [Route("app/customer/login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login(UsersViewModel model)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
               
              var  userFound = UserManager.Find(model.UserName, model.Password);
            //var userFound1 = UserManager.Find(model.MobileNo, model.Password);
            
            if (userFound == null)
            {

                var userExist = UserManager.FindByName(model.UserName);
                if(userExist!=null)
                if(userExist.AccessFailedCount==5)
                {
                    
                    return Ok(ErrorMessage.AccountError.ACCOUNT_LOCK_INCORRECT.ErrorResponse());
                }
                if(userExist!=null)
                {
                    userExist.AccessFailedCount = userExist.AccessFailedCount + 1;
                    UserManager.Update(userExist);
                }
                return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
            }
            if (userFound.AccessFailedCount == 5)
            {
                return Ok(ErrorMessage.AccountError.ACCOUNT_LOCK_INCORRECT.ErrorResponse());
            }



            if (userFound.IsDeleted)
            {
                return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
            }

            if (UserManager.GetRoles(userFound.Id).FirstOrDefault() != UserRole.Customer)
            {
                return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
            }
          
            //if (userFound.EmailConfirmed != true)
            //{
            //    return Ok(ErrorMessage.AccountError.EMAIL_NOT_VeriFied.ErrorResponse());
            //}

            if (userFound.IsActive != true)
            {
                return Ok(ErrorMessage.AccountError.ACTIVATE.ErrorResponse());
            }
            _passwordlogService.OtpLogDelete(userFound.Id);
            UsersViewModel user = new UsersViewModel();
            user = _manageApplicationUserModel.modelApplicationUserToCustomerUserViewModel(userFound);
            user =_accountManager.addingProfilePic(user);



            return await GetLoginJson(user);
            //return await GetLoginJson(userFound);
        }


        [Route("app/driver/login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> driverLogin(UsersViewModel model)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
           
            var userFound = UserManager.Find(model.UserName, model.Password);
            //var userFound1 = UserManager.Find(model.MobileNo, model.Password);
            
            if (userFound == null)
            {

                var userExist = UserManager.FindByName(model.UserName);

                if (userExist != null)
                    if (userExist.AccessFailedCount == 5)
                    {

                    return Ok(ErrorMessage.AccountError.ACCOUNT_LOCK_INCORRECT.ErrorResponse());
                    }
                if (userExist != null)
                {
                    userExist.AccessFailedCount = userExist.AccessFailedCount + 1;
                    UserManager.Update(userExist);
                }
                return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
            }
            var u = UserManager.GetRoles(userFound.Id).FirstOrDefault();
            //if (UserManager.GetRoles(userFound.Id).FirstOrDefault() != UserRole.Driver)
            if(u!=UserRole.Driver)
            {
                return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
            }

            if (userFound.AccessFailedCount == 5)
            {
                return Ok(ErrorMessage.AccountError.ACCOUNT_LOCK_INCORRECT.ErrorResponse());
            }



            if (userFound.IsDeleted)
            {
                return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
            }

           

            //if (userFound.EmailConfirmed != true)
            //{
            //    return Ok(ErrorMessage.AccountError.EMAIL_NOT_VeriFied.ErrorResponse());
            //}

            if (userFound.IsActive != true)
            {
                return Ok(ErrorMessage.AccountError.ACTIVATE.ErrorResponse());
            }
            _passwordlogService.OtpLogDelete(userFound.Id);
            UsersViewModel user = new UsersViewModel();
            user = _manageApplicationUserModel.modelApplicationUserToCustomerUserViewModel(userFound);
            user = _accountManager.addingProfilePic(user);



            return await GetLoginJson(user);
            //return await GetLoginJson(userFound);
        }



        [Route("app/customer/forgot-password")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ForgotPassword(UsersViewModel model)
        {
            try
            {
                string host = HttpContext.Current.Request.Url.Host;
                var user = UserManager.FindByEmail(model.UserName);
                if (user == null) return Ok(ErrorMessage.AccountError.INVALID_EMAIL.ErrorResponse());

                user.OTP = _accountManager.GetOtp();
                user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);
                UserManager.Update(user);


                if (_accountManager.IsAllowSms())
                {
                    SendSms sms = new SendSms();
                    string msg = user.OTP.ToString();
                    sms.SendMessage(model.MobileNo, msg, model.CountryCode);
                }

                //string code = UserManager.GeneratePasswordResetToken(user.Id);
                //code = HttpUtility.UrlEncode(code);

                //var callbackUrl = "http://" + host + "/account/recover/" + user.UniqueCode + "?code=" + code;
                // var userData = _usersService.SelectUserByUniqueCode(user.UniqueCode);
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.ForgotPassword.ToString());
                var emailBody = string.Format(configData.ConfigurationValue, user.FirstName, user.OTP);
                MailSender.SendEmail(user.Email, EmailConstants.FORGOT_PASSWORD, emailBody).Wait();
                return Ok(SuccessMessage.AccountSuccess.FORGOT_PASSWORD_OTP.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public async Task<IHttpActionResult> GetLoginJson(ApplicationUser user)
        public async Task<IHttpActionResult> GetLoginJson(UsersViewModel user)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var token = await CreateToken(user.Email);
            if (string.IsNullOrEmpty(token.Error)) //if error is not found return token 
            {
                if (user != null)
                {
                    token.TokenData = new tokenData
                    {
                        Token = new tokenObj
                        {
                            Expires = DateTimeOffset.Now.AddDays(14),
                            AccessToken = token.AccessToken,
                            TokenType = token.TokenType
                        },
                        //User = _manageApplicationUserModel.modelApplicationUserToCustomerUserViewModel(user),
                        User=user,
                    };

                }
                SearchParam param = new SearchParam();

                var contactAddress =  _useraddressService.SelectUserAddress(param).FirstOrDefault().ToViewModel();
                return Ok(token.TokenData.SuccessResponse());
            }
            return Ok(token.ErrorDescription.ErrorResponse());
        }



        async Task<TokenViewModel> CreateToken(string email)
        {
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            IEnumerable<KeyValuePair<string, string>> postData = new Dictionary<string, string>
            {
                {"username",email},
                {"password","temp"},
                {"grant_type","password"}
            };
            var WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

            using (var httpClient = new HttpClient())
            {
                using (var content = new FormUrlEncodedContent(postData))
                {
                    content.Headers.Clear();
                    content.Headers.Add("Content-Type", WWW_FORM_URLENCODED);

                    content.Headers.Add("user-found", "true");
                   
                    HttpResponseMessage response = await httpClient.PostAsync(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/api/token", content);
             //       HttpResponseMessage response = await httpClient.PostAsync("http://lotuslaundry.azurewebsites.net" + "/api/token", content);

                    var respo = await response.Content.ReadAsStringAsync();
                    try
                    {
                        return JsonConvert.DeserializeObject<TokenViewModel>(respo);
                    }
                    catch (JsonReaderException ex)
                    {
                        return null;//throw new ("Internal Error Occured during Json Deserialization. " + respo, ex);
                    }
                }
            }
        }

        #endregion 




        [Route("api/customer/detail")]
        [HttpGet]
        [CustomAuthorize(UserRole.Customer)]
        public IHttpActionResult GetCustomer()
        {
            var userFound = UserManager.FindById(User.Identity.GetUserId<long>());
            var user = _manageApplicationUserModel.modelApplicationUserToCustomerDetail(userFound);
            _accountManager.addingProfilePic(user);
            return Ok(user.SuccessResponse());
        }

        [Route("api/application/my/details")]
        [HttpGet]
        [CustomAuthorize(UserRole.Customer,UserRole.Driver)]

        public IHttpActionResult Getcustomer()
        {
            try
            {
                var userFound = UserManager.FindById(User.Identity.GetUserId<long>());
                SearchParam param = new SearchParam();
                param.Id = CryptoEngine.Encrypt(FileType.USERS + "_" + userFound.Id.ToString(), KeyConstant.Key);              
                var user = _usersService.SelectCustomer(param).FirstOrDefault().ToViewModel();
                FileGroupItemsViewModel ProfileFile = new FileGroupItemsViewModel();
                ProfileFile.Path = user.ProfileImageUrl;
                foreach(var item in user.AllFile)
                {
                    if (item.Type == "PROFILE_PIC")
                    {
                        user.ProfileImageUrl = item.Path.ToString();
                    }
                }
                user.File = ProfileFile;
                if(user.File.Path==null)
                {
                    user.File = null;
                }
                if (user.ProfileImageUrl != null)
                {
                    FileGroupItemsViewModel ProfileFile1 = new FileGroupItemsViewModel();
                    ProfileFile1.Path = user.ProfileImageUrl;
                    user.File = ProfileFile1;
                }
                return Ok(user.SuccessResponse());

            }

            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }




        [Route("api/customer/detail")]
        [HttpPut]
        [CustomAuthorize(UserRole.Customer)]
        public IHttpActionResult UpdateCustomer(UsersViewModel model)
        {
            var user = UserManager.FindById(User.Identity.GetUserId<long>());

            if (model != null && user!=null )
            {
                model= _accountManager.filterUser(model , user);
            }
            model.CreatedBy = User.Identity.GetUserId<long>();
            if (user != null)
            {
                user.FirstName = model.FirstName ?? user.FirstName;
                user.LastName = model.LastName ?? user.FirstName;
                user.Email = model.Email ?? user.Email;
                user.NickName = model.NickName ?? user.NickName;
                user.DOB= model.DateOfbirth ?? user.DOB;
                user.Gender= model.Gender ?? user.Gender;

                user.ProfilePic = model.ProfileImageUrl ?? user.ProfilePic;

               
                
                UserManager.Update(user);


                try
                {
                    if (model.File != null)
                    {
                        string path = _customerService.ProfilePicInsert(model.ToModel());

                        if (model.File.IsDeleted == true)
                        {
                            user.ProfilePic = null;
                        }
                        else
                        {
                            user.ProfilePic = path;
                        }
                        UserManager.Update(user);
                    }
                    
                    
                }
                catch(Exception ex)
                {
                    string a = ex.Message.ToString();
                    return Ok("Debuger: File UPload file ");
                }

                return Ok(SuccessMessage.AccountSuccess.USER_DETAIL_UPDATE.SuccessResponse());
            }
            return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
        }

       
        // [AllowAnonymous]


        [Route("app/customer/reset-password")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ResetPassword(UsersViewModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.UniqueCode))
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                var user = UserManager.Users.Where(x => x.UniqueCode.Equals(model.UniqueCode)).FirstOrDefault();

               
                if (user == null)
                    return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());

                var userFoundWithPassword = UserManager.Find(user.UserName, model.Password);
                if (userFoundWithPassword != null)
                {
                    return Ok(SuccessMessage.AccountFailure.PASSWORD_SAME.ErrorResponse());
                }

                IdentityResult result = UserManager.ResetPassword(Convert.ToInt64(user.Id), HttpUtility.UrlDecode(model.Code), model.Password);
                if (result.Succeeded)
                {
                    user.AccessFailedCount = 0;
                    UserManager.Update(user);
                    return Ok(SuccessMessage.AccountSuccess.PASSWORD_CHANGED.SuccessResponse());
                }
                else
                {
                    return Ok(result.Errors.FirstOrDefault().ErrorResponse());
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        /// <summary>
        /// Change password api
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("app/customer/changePassword")]
        [CustomAuthorize]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(User.Identity.GetUserId<long>());

                var userFound = UserManager.Find(user.UserName, model.OldPassword);
                if (userFound == null)
                {
                    return Ok(SuccessMessage.AccountFailure.OLD_PASSWORD_NOT_MATCHED.ErrorResponse());
                }
                var userFoundWithPassword = UserManager.Find(user.UserName, model.Password);
                if (userFoundWithPassword !=null)
                {
                    return Ok(SuccessMessage.AccountFailure.PASSWORD_SAME.ErrorResponse());
                }
                if (user != null)
                {
                    user.IsActive = true;
                    user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                    await UserManager.UpdateAsync(user);
                   
                    return Ok(SuccessMessage.AccountSuccess.PASSWORD_CHANGED.SuccessResponse());
                }
                return Ok(SuccessMessage.AccountFailure.INVALID_USER.ErrorResponse());
            }
            catch (Exception ex)
            {

                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("api/validate/password")]
        [CustomAuthorize]
        [HttpPost]
        public async Task<IHttpActionResult> ValidateOldPassword(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(User.Identity.GetUserId<long>());

                var userFound = UserManager.Find(user.UserName, model.Password);
                if (userFound == null)
                {
                    return Ok("Password does not match".ErrorResponse());
                }
                if (user != null)
                {
                    UsersViewModel userData = new UsersViewModel();
                    string code = UserManager.GeneratePasswordResetToken(user.Id);
                    code = HttpUtility.UrlEncode(code);
                    string UniqueCode = user.UniqueCode;

                    userData.Code = code;
                    userData.UniqueCode = UniqueCode;
                    return Ok(userData.SuccessResponse());

                    //return Ok(SuccessMessage.AccountSuccess.PASSWORD_CHANGED.SuccessResponse());
                }
                return Ok("Invalid user please login.".ErrorResponse());
            }
            catch (Exception ex)
            {

                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("api/email/verification/send")]
        [CustomAuthorize]
        [HttpPost]
        public IHttpActionResult emailVerification( UsersViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(ModelState.ErrorResponse());
                }
                if(model==null)
                {
                    model = new UsersViewModel();
                }
                var curentUser = UserManager.FindById(User.Identity.GetUserId<long>());

                if (UserManager.Users.Any(x => (x.Email.Equals(curentUser.Email)) && x.EmailConfirmed && !x.IsDeleted))
                {
                    
                    return Ok(string.Format(ErrorMessage.AccountError.EMAIL_Already_VeriFied, curentUser.Email).ErrorResponse());
                }
                if (UserManager.Users.Any(x => (x.UserName.Equals(curentUser.Email) && !x.EmailConfirmed && !x.IsDeleted)))
                {


                    model.Email = curentUser.Email;
                    var user = _manageApplicationUserModel.UserViewModelToCustomerApplicationModel(model);
                    user.Id = curentUser.Id;
                    user.UniqueCode = curentUser.UniqueCode;
                    SendEmailForEmailConfirm(user);

                    
                    return Ok(string.Format(SuccessMessage.AccountSuccess.EMAIL_VERIFY_LINK, curentUser.Email).SuccessResponse());
                   
                }
               


              

                return Ok(SuccessMessage.AccountSuccess.EMAIL_VERIFY_LINK.SuccessResponse());


            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("api/user/phone/change")]
        [CustomAuthorize]
        [HttpPost]
        public IHttpActionResult phoneChange(UsersViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(ModelState.ErrorResponse());
                }
                
                var curentUser = UserManager.FindById(User.Identity.GetUserId<long>());
                model.PhoneNumber = model.MobileNo;
                if (UserManager.Users.Any(x => (x.Email.Equals(curentUser.Email)) &&(x.CountryCode.Equals(model.CountryCode)) && (x.PhoneNumber.Equals(model.PhoneNumber)) && x.PhoneNumberConfirmed && !x.IsDeleted))
                {

                    return Ok(string.Format(ErrorMessage.AccountError.PHONENUMBER_ALREADY_REGISTERED, model.PhoneNumber).ErrorResponse());
                }
                if (UserManager.Users.Any(x => (x.UserName.Equals(curentUser.Email)  && !x.IsDeleted)))
                {
                    //model.MobileNo = model.PhoneNumber;
                    //curentUser.PhoneNumber = model.PhoneNumber;
                    //curentUser.CountryCode = model.CountryCode;
                    //curentUser.PhoneNumberConfirmed = false;


                    curentUser.OTP = _accountManager.GetOtp();
                    curentUser.OTPValidTill = DateTime.UtcNow.AddMinutes(15);

                    UserManager.Update(curentUser);

                    
                    _passwordlogService.OtpLogInsert(curentUser.Id);
                    SendSms sms = new SendSms();
                    string msg = "OTP =" + curentUser.OTP.ToString();
                    sms.SendMessage(model.MobileNo, msg,model.CountryCode);

                   


                    return Ok(string.Format(SuccessMessage.AccountSuccess.MOBILE_VERIFY, curentUser.Email).SuccessResponse());

                }



                
                return Ok(string.Format(ErrorMessage.AccountError.INVALID,"").ErrorResponse());
                


            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        [Route("api/phone/change/otp/verify")]
        [HttpPost]
        public IHttpActionResult ChangePhoneVerifyOtp(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(User.Identity.GetUserId<long>());
                //var user = UserManager.FindByName(model.UserName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }

                if (model.OTP == user.OTP && user.OTPValidTill >= DateTime.UtcNow)
                {
                    model.PhoneNumber = model.MobileNo;
                    user.PhoneNumber = model.PhoneNumber;
                    user.CountryCode = model.CountryCode;
                    

                    user.PhoneNumberConfirmed = true;
                    user.IsActive = true;
                    user.OTPValidTill = DateTime.UtcNow.AddDays(-10);

                    UserManager.Update(user);

                    _passwordlogService.OtpLogDelete(user.Id);
                    // SendEmailForEmailConfirm(user);


                    return Ok(SuccessMessage.AccountSuccess.MOBILE_VERIFY_CONFIRM.SuccessResponse());
                }

                return Ok(ErrorMessage.AccountError.INVALID_OTP.ErrorResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("api/phone/change/otp/resend")]
        [HttpPost]
        public IHttpActionResult ChnagePhoneResendOtp(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(User.Identity.GetUserId<long>());
                //var user = UserManager.FindByName(userName);
                if (user == null)
                {
                    return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
                }
                if (user.IsDeleted)
                {
                    return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
                }

                if (UserManager.GetRoles(user.Id).FirstOrDefault() != UserRole.Customer)
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }

                //if (user.PhoneNumberConfirmed == true)
                //{
                //    return Ok(ErrorMessage.AccountError.PHONENUMBER_ALREADY_CONFIRM.ErrorResponse());
                //}

                var otpCount = _passwordlogService.OtpLogSelect(user.Id);
                if (otpCount.Count() == 5)
                {
                    var date = DateTime.UtcNow - otpCount[0].CreatedOn;
                    if (date.Value.Minutes >= 30)
                    {
                        _passwordlogService.OtpLogDelete(user.Id);
                    }
                    else
                    {
                        return Ok(ErrorMessage.AccountError.OTP_LIMIT.ErrorResponse());
                    }
                }
                user.OTP = _accountManager.GetOtp();
                user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);
                _passwordlogService.OtpLogInsert(user.Id);
                UserManager.Update(user);

                SendSms sms = new SendSms();
                string msg = "OTP =" + user.OTP.ToString();
                sms.SendMessage(model.MobileNo, msg, model.CountryCode);

                return Ok(SuccessMessage.AccountSuccess.RESEND_OTP.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        #region ====================================== Emails ==================

        public void SendWelcomeEmail(string toEmail, string fullName)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.WelcomeEmail.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, fullName);
            MailSender.SendEmail(toEmail, EmailConstants.WELOME_EMAIL, emailBody).Wait();
        }
        public void SendEmailForEmailConfirm(ApplicationUser model)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.EmailVerification.ToString());

            string code = UserManager.GenerateEmailConfirmationToken(model.Id);
            code = HttpUtility.UrlEncode(code);
            // var callbackUrl = UrlConstants.baseUrl + "/app/customer/confirm-account?uniqueCode=" + model.UniqueCode + "&code=" + code;

            //var callbackUrl = UrlConstants.baseUrl + "/register/confirm?uniqueCode=" + model.UniqueCode + "&code=" + code;
            var callbackUrl = UrlConstants.baseUrl + "/register/confirm?uniqueCode=" + model.UniqueCode + "&code=" + code;
            var emailBody = string.Format(configData.ConfigurationValue, model.FirstName + " " + model.LastName, callbackUrl);
            MailSender.SendEmail(model.Email, EmailConstants.CONFIRM_YOUR_ACCOUNT, emailBody).Wait();
        }
        #endregion


        [Route("api/application/user/device")]
       // [Authorize]
        [HttpPost]
        public IHttpActionResult UserDeviceSave(UserDeviceViewModel model)
        {
            try
            {
                model.UserId = User.Identity.GetUserId<long>();
                model.IsActive = true;
                model.IsDeleted = false;
                _usersService.UserDeviceSaveOrUpdate (model.ToModel());
                return Ok("User device save".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("user/device/delete")]
        [Authorize]
        [HttpDelete]
        public IHttpActionResult UserDeviceDelete(string deviceId)
        {
            try
            {
                var userId = User.Identity.GetUserId<long>();
                var userDeviceModel = new UserDeviceModel
                {
                    UserId = userId,
                    IsDeleted = true,
                    DeviceId = deviceId
                };

                _usersService.UserDeviceSaveOrUpdate(userDeviceModel);
                return Ok("User device is deleted".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        [Route("api/application/{deviceId}/logout")]
       // [Authorize]
        [HttpPost]
        public IHttpActionResult UserDeviceDeleteOnLogOut(string deviceId)
        {
            try
            {
                //var userId = User.Identity.GetUserId<long>();
                //var userDeviceModel = new UserDeviceModel
                //{
                //    UserId = userId,
                //    IsDeleted = true,
                //    DeviceId = deviceId
                //};

                _usersService.UserDeviceDelete(deviceId);
                return Ok("User device is deleted".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        //api/application/user/userId/call/status
        [Route("api/application/user/call/status")]
        [HttpPut]
       // [CustomAuthorize(UserRole.Customer,UserRole.Driver)]
        public IHttpActionResult UpdateCallStatus(CallStatusViewModel model)
        {
            if(model.toId==null && model.fromId==null)
            {
                return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
            }

            if (model.toId != null)
            {
                var userId = Convert.ToInt64(CryptoEngine.Decrypt(model.toId.ToString(), KeyConstant.Key));
                var user = UserManager.FindById(userId);
                user.callStatus = model.status;
                UserManager.Update(user);
            }
            if (model.fromId != null)
            {
                var fromUser_Id = Convert.ToInt64(CryptoEngine.Decrypt(model.fromId.ToString(), KeyConstant.Key));
                var fromUser = UserManager.FindById(fromUser_Id);
                fromUser.callStatus = model.status;
                UserManager.Update(fromUser);
            }
            return Ok(SuccessMessage.AccountSuccess.USER_DETAIL_UPDATE.SuccessResponse());
            
            
        }
        [Route("api/application/user/call/status/{id}")]
        [HttpGet]
      //  [CustomAuthorize(UserRole.Customer, UserRole.Driver)]
        public IHttpActionResult GetCallStatus(string Id)
        {
            UsersViewModel model = new UsersViewModel();
            model.Id = Id;
            var userId = Convert.ToInt64(CryptoEngine.Decrypt(Id.ToString(), KeyConstant.Key));
            var user = UserManager.FindById(userId);

           
            if (user != null)
            {

            
                return Ok(user.callStatus.SuccessResponse());
            }
            return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
        }

        [Route("api/idle/users")]
        [HttpGet]

        public IHttpActionResult SetUsersIdle(SearchParam param)
        {
            try
            {
                _usersService.SetUserIdle();

                return Ok("ok".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        
    }
}
