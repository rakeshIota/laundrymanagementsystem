﻿using Customer.Service.Service;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.Service;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    public class ServiceCustomerController : ApiController
    {
        private IServiceCustomerService _serviceCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ServiceCustomerController(IServiceCustomerService serviceCustomerService, IExceptionService exceptionService)
        {
            _serviceCustomerService = serviceCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("api/application/services")]
        [CustomAuthorize(UserRole.Customer, UserRole.Driver)]
        [HttpGet]
        public IHttpActionResult GetServices()
        {
            try
            {
                SearchParam param = new SearchParam();
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var services = _serviceCustomerService.ServiceSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(services.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Service_Select_Customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Select_Customer).ToString().ErrorResponse());
            }
        }
    }
}
