﻿using Customer.Service.District;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.District;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api/application")]
    public class DistrictCustomerController : ApiController
    {
        private IDistrictCustomerService _districtCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DistrictCustomerController(IDistrictCustomerService districtCustomerService, IExceptionService exceptionService)
        {
            _districtCustomerService = districtCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("districts")]
        [CustomAuthorize(UserRole.Customer, UserRole.Driver)]
        [HttpGet]
        public IHttpActionResult GetDistricts()
        {
            try
            {
                SearchParam param = new SearchParam();
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var districts = _districtCustomerService.DistrictSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(districts.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.District_Select_Customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Select_Customer).ToString().ErrorResponse());
            }
        }
    }
}
