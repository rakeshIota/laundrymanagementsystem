﻿using Customer.Service.SubDistrict;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Customer.SubDistrict;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LotusLaundry.API.Customer
{
    [RoutePrefix("api/application")]
    public class SubDistrictCustomerController : ApiController
    {
        private ISubDistrictCustomerService _subDistrictCustomerService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public SubDistrictCustomerController(ISubDistrictCustomerService subDistrictCustomerService, IExceptionService exceptionService)
        {
            _subDistrictCustomerService = subDistrictCustomerService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _exceptionService = exceptionService;
        }

        [Route("district/{districtId}/sub/districts")]
        [CustomAuthorize(UserRole.Customer, UserRole.Driver)]
        [HttpGet]
        public IHttpActionResult GetSubDistricts(string districtId)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.RelationId = districtId;
                var subDistricts = _subDistrictCustomerService.SubDistrictSelect(param).Select(y => y.ToViewModel()).ToList();
                return Ok(subDistricts.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.SubDistrict_Select_Customer);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Select_Customer).ToString().ErrorResponse());
            }
        }
    }
}
