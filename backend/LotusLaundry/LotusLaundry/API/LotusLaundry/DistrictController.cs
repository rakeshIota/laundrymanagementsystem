using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.District;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.District;
using LotusLaundry.Service.SubDistrict;
using LotusLaundry.Framework.ViewModels.Customer.SubDistrict;
using LotusLaundry.Facade;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class DistrictController : ApiController
    {
        private IDistrictService _districtService;
        private IExceptionService _exceptionService;
        private ISubDistrictService _subdistrictService;
        private DistrictManager _districtManager;

        private ManageApplicationUserModel _manageApplicationUserModel;
        public DistrictController(ISubDistrictService subdistrictService, IDistrictService districtService, IExceptionService exceptionService)
        {
            _subdistrictService = subdistrictService;
            _districtService = districtService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _districtManager = new DistrictManager(subdistrictService);
        }

        /// <summary>
        ///  Api use for get all District
        /// </summary>
        /// <returns></returns>
        [Route("Districts")]
        [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPost]
        public IHttpActionResult GetAllDistricts(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var districts = _districtService.SelectDistrict(param).Select(x => x.ToViewModel()); ;
            	return Ok(districts.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.District, (int)ErrorFunctionCode.District_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get district by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("district/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.OPERATIONMANAGER)]
        [HttpGet]
        public IHttpActionResult GetDistrict(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var district = _districtService.SelectDistrict(param).FirstOrDefault().ToViewModel();
	            if (district.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(district.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.District, (int)ErrorFunctionCode.District_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save district
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("district")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDistrict(DistrictViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _districtService.DistrictInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DISTRICT + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("District save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.District, (int)ErrorFunctionCode.District_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update district
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("district")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDistrict(DistrictViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
                _districtService.DistrictUpdate(model.ToModel());
                return Ok(model.Id.SuccessResponse("District updated successfully"));


            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.District, (int)ErrorFunctionCode.District_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete district by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("district/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDistrict(string id)
        {
        	try{
	            DistrictViewModel model = new DistrictViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();

                 //SearchParam param = new SearchParam();
                 //   param.RelationTable = "DISTRICT";
                 //   param.RelationId = model.Id;
                 //   var subdistrict = _subdistrictService.SelectSubDistrict(param).FirstOrDefault().ToViewModel();


                    if (!_districtManager.AllowTodelete(model.Id))
                    {
                       return Ok(((string)ErrorMessage.AccountError.DISTRICT_NOT_ALLOW_TO_DELETE).ToString().ErrorResponse());

                    }

                _districtService.DistrictUpdate(model.ToModel());
                return Ok("District Deleted successfully".SuccessResponse());

            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.District, (int)ErrorFunctionCode.District_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.District_Delete).ToString().ErrorResponse());
            }
        }
    }
}

