using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Service;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Service;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class ServiceController : ApiController
    {
        private IServiceService _serviceService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ServiceController(IServiceService serviceService, IExceptionService exceptionService)
        {
            _serviceService = serviceService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Service
        /// </summary>
        /// <returns></returns>
        [Route("Services")]
        [CustomAuthorize("ROLE_ADMIN", "ROLE_OPERATION")]
        [HttpPost]
        public IHttpActionResult GetAllServices(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                var services = _serviceService.SelectService(param).Select(x => x.ToViewModel());
                return Ok(services.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Service, (int)ErrorFunctionCode.Service_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get service by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("service/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetService(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var service = _serviceService.SelectService(param).FirstOrDefault().ToViewModel();
	            if (service.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(service.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Service, (int)ErrorFunctionCode.Service_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save service
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("service")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveService(ServiceViewModel model)
        {
        	try
            {
            	//set tenantid

            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _serviceService.ServiceInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.SERVICE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Service save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Service, (int)ErrorFunctionCode.Service_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update service
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("service")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateService(ServiceViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _serviceService.ServiceUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Service updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Service, (int)ErrorFunctionCode.Service_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete service by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("service/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteService(string id)
        {
        	try{
	            ServiceViewModel model = new ServiceViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _serviceService.ServiceUpdate(model.ToModel());
	            return Ok("Service Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Service, (int)ErrorFunctionCode.Service_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Service_Delete).ToString().ErrorResponse());
            }
        }
    }
}

