using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.PasswordLog;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.PasswordLog;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class PasswordLogController : ApiController
    {
        private IPasswordLogService _passwordlogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PasswordLogController(IPasswordLogService passwordlogService, IExceptionService exceptionService)
        {
            _passwordlogService = passwordlogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all PasswordLog
        /// </summary>
        /// <returns></returns>
        [Route("PasswordLogs")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPasswordLogs(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var passwordlogs = _passwordlogService.SelectPasswordLog(param).Select(x => x.ToViewModel()); ;
            	return Ok(passwordlogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PasswordLog, (int)ErrorFunctionCode.PasswordLog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PasswordLog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get passwordlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("passwordlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetPasswordLog(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var passwordlog = _passwordlogService.SelectPasswordLog(param).FirstOrDefault().ToViewModel();
	            if (passwordlog.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(passwordlog.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PasswordLog, (int)ErrorFunctionCode.PasswordLog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PasswordLog_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save passwordlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("passwordlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePasswordLog(PasswordLogViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _passwordlogService.PasswordLogInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PASSWORDLOG + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("PasswordLog save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PasswordLog, (int)ErrorFunctionCode.PasswordLog_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PasswordLog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update passwordlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("passwordlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePasswordLog(PasswordLogViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _passwordlogService.PasswordLogUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("PasswordLog updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PasswordLog, (int)ErrorFunctionCode.PasswordLog_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PasswordLog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete passwordlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("passwordlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePasswordLog(string id)
        {
        	try{
	            PasswordLogViewModel model = new PasswordLogViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _passwordlogService.PasswordLogUpdate(model.ToModel());
	            return Ok("PasswordLog Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PasswordLog, (int)ErrorFunctionCode.PasswordLog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PasswordLog_Delete).ToString().ErrorResponse());
            }
        }
    }
}

