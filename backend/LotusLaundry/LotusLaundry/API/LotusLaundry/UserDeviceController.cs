using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.UserDevice;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserDevice;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class UserDeviceController : ApiController
    {
        private IUserDeviceService _userdeviceService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public UserDeviceController(IUserDeviceService userdeviceService, IExceptionService exceptionService)
        {
            _userdeviceService = userdeviceService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all UserDevice
        /// </summary>
        /// <returns></returns>
        [Route("UserDevices")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllUserDevices(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var userdevices = _userdeviceService.SelectUserDevice(param).Select(x => x.ToViewModel()); ;
            	return Ok(userdevices.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserDevice, (int)ErrorFunctionCode.UserDevice_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserDevice_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get userdevice by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("userdevice/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetUserDevice(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var userdevice = _userdeviceService.SelectUserDevice(param).FirstOrDefault().ToViewModel();
	            if (userdevice.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(userdevice.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserDevice, (int)ErrorFunctionCode.UserDevice_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserDevice_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save userdevice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("userdevice")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveUserDevice(UserDeviceViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _userdeviceService.UserDeviceInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.USERDEVICE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("UserDevice save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserDevice, (int)ErrorFunctionCode.UserDevice_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserDevice_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update userdevice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("userdevice")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateUserDevice(UserDeviceViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _userdeviceService.UserDeviceUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("UserDevice updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserDevice, (int)ErrorFunctionCode.UserDevice_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserDevice_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete userdevice by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("userdevice/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteUserDevice(string id)
        {
        	try{
	            UserDeviceViewModel model = new UserDeviceViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _userdeviceService.UserDeviceUpdate(model.ToModel());
	            return Ok("UserDevice Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserDevice, (int)ErrorFunctionCode.UserDevice_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserDevice_Delete).ToString().ErrorResponse());
            }
        }
    }
}

