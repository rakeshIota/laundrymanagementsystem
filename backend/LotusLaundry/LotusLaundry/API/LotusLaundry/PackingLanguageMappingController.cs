using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.PackingLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.PackingLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class PackingLanguageMappingController : ApiController
    {
        private IPackingLanguageMappingService _packinglanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PackingLanguageMappingController(IPackingLanguageMappingService packinglanguagemappingService, IExceptionService exceptionService)
        {
            _packinglanguagemappingService = packinglanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all PackingLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("PackingLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPackingLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var packinglanguagemappings = _packinglanguagemappingService.SelectPackingLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(packinglanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PackingLanguageMapping, (int)ErrorFunctionCode.PackingLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PackingLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get packinglanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("packinglanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetPackingLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var packinglanguagemapping = _packinglanguagemappingService.SelectPackingLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (packinglanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(packinglanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PackingLanguageMapping, (int)ErrorFunctionCode.PackingLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PackingLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save packinglanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("packinglanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePackingLanguageMapping(PackingLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _packinglanguagemappingService.PackingLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PACKINGLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("PackingLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PackingLanguageMapping, (int)ErrorFunctionCode.PackingLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PackingLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update packinglanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("packinglanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePackingLanguageMapping(PackingLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _packinglanguagemappingService.PackingLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("PackingLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PackingLanguageMapping, (int)ErrorFunctionCode.PackingLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PackingLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete packinglanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("packinglanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePackingLanguageMapping(string id)
        {
        	try{
	            PackingLanguageMappingViewModel model = new PackingLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _packinglanguagemappingService.PackingLanguageMappingUpdate(model.ToModel());
	            return Ok("PackingLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PackingLanguageMapping, (int)ErrorFunctionCode.PackingLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PackingLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

