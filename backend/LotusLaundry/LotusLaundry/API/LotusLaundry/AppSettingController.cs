using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.AppSetting;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.LotusLaundry.ViewModels.AppSetting;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class AppSettingController : ApiController
    {
        private IAppSettingService _appsettingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public AppSettingController(IAppSettingService appsettingService, IExceptionService exceptionService)
        {
            _appsettingService = appsettingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all AppSetting
        /// </summary>
        /// <returns></returns>
        [Route("AppSettings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllAppSettings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var appsettings = _appsettingService.SelectAppSetting(param).Select(x =>x.ToViewModel()); ;
            	return Ok(appsettings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.AppSetting, (int)ErrorFunctionCode.AppSetting_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get appsetting by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("appsetting/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAppSetting(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var appsetting = _appsettingService.SelectAppSetting(param).FirstOrDefault().ToViewModel();
	            if (appsetting.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(appsetting.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.AppSetting, (int)ErrorFunctionCode.AppSetting_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save appsetting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("appsetting")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveAppSetting(AppSettingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _appsettingService.AppSettingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.APPSETTING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("AppSetting save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.AppSetting, (int)ErrorFunctionCode.AppSetting_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update appsetting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("appsetting")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateAppSetting(AppSettingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _appsettingService.AppSettingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("AppSetting updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.AppSetting, (int)ErrorFunctionCode.AppSetting_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete appsetting by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("appsetting/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteAppSetting(string id)
        {
        	try{
	            AppSettingViewModel model = new AppSettingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _appsettingService.AppSettingUpdate(model.ToModel());
	            return Ok("AppSetting Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.AppSetting, (int)ErrorFunctionCode.AppSetting_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.AppSetting_Delete).ToString().ErrorResponse());
            }
        }
    }
}

