using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;

using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Location;

using Driver.Service.Location;
using LotusLaundry.Framework.ViewModels.Driver.Location;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class LocationController : ApiController
    { 
        private ILocationService _locationService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public LocationController(ILocationService locationService, IExceptionService exceptionService)
        {
            _locationService = locationService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Location
        /// </summary>
        /// <returns></returns>
        [Route("driver/locations")]
        [CustomAuthorize("ROLE_ADMIN",UserRole.Customer,UserRole.OPERATIONMANAGER)]
        [HttpPost]  
        public IHttpActionResult GetAllDriverLocation(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var DLocation = _locationService.SelectLocationForParticularUsers(param).Select(x => x.ToViewModel());
            	return Ok(DLocation.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Select).ToString().ErrorResponse());
            }
        }

        //[Route("driver/Locations")]
        //[CustomAuthorize("ROLE_ADMIN")]
        //[HttpPost]
        //public IHttpActionResult GetDriverLocation(SearchParam param)
        //{
        //    try
        //    {
        //        param = param ?? new SearchParam();
               
        //        var DLocation = _locationService.SelectLocation(param).Select(x => x.ToViewModel());
        //        return Ok(DLocation.SuccessResponse());
        //    }
        //    catch (Exception ex)
        //    {
        //        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Select);
        //        _exceptionService.InsertLog(exception);
        //        return Ok(((int)ErrorFunctionCode.Location_Select).ToString().ErrorResponse());
        //    }
        //}


        /// <summary>
        /// Api use for get Location by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("driver/location/{id}")]
        //[CustomAuthorize("ROLE_ADMIN", UserRole.Driver,UserRole.Customer)]
        [HttpGet]
        public IHttpActionResult GetDriverLocation(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var Location = _locationService.SelectLocation(param).FirstOrDefault().ToViewModel();
	            if (Location.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(Location.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save Location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("driver/location")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver, UserRole.OPERATIONMANAGER)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDriverLocation(LocationViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _locationService.LocationInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.LOCATION + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Location save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update Locatione
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("driver/location")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver,UserRole.OPERATIONMANAGER)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDriverLocation(LocationViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
                model.IsActive = true;
                model.IsDeleted = false;
	            _locationService.LocationUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Location updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete Locatione by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("driver/location/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Driver)]
        [HttpDelete]
        public IHttpActionResult DeleteDriverLocation(string id)
        {
        	try{
                LocationViewModel model = new LocationViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _locationService.LocationUpdate(model.ToModel());
	            return Ok("Location Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Delete).ToString().ErrorResponse());
            }
        }


        [Route("test/driver/location/update/{id}")]
        //[CustomAuthorize("ROLE_ADMIN", UserRole.Driver)]  http://lotuslaundry.iotasol.com/app/test/driver/location/update
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDriverLocationTesting(string  id)
        {
            try
            {
                LocationViewModel model = new LocationViewModel();
                model.TenantId = 1;
                model.UpdatedBy = 1;
                model.Userid = id;
                model.Id = id;
                SearchParam param = new SearchParam();
                param.RelationTable = "User";
                param.RelationId = id;
                var DLocation = _locationService.SelectLocationForParticularUsers(param).Select(x => x.ToViewModel());

                var dl=  DLocation.FirstOrDefault();
                if(dl.Latitude.ToString()== "30.883995")
                {
                    //(30.880705, 75.840024)
                    model.Latitude = 30.880705m;
                    model.Longitude = 75.840024m;
                }
                else if (dl.Latitude.ToString() == "30.880705")
                {
                   // 30.875246, 75.844908)
                    model.Latitude = 30.875246m;
                    model.Longitude = 75.844908m;
                }
                else if (dl.Latitude.ToString() == "30.875246")
                {
                    // (30.870255, 75.843273)
                    model.Latitude = 30.870255m;
                    model.Longitude = 75.843273m;
                }
                else if (dl.Latitude.ToString() == "30.870255")
                {
                    //(30.868106, 75.835516)
                    model.Latitude = 30.868106m;
                    model.Longitude = 75.835516m;

                }
                else if (dl.Latitude.ToString() == "30.868106")
                {
                    //30.867989,75.831828
                    model.Latitude = 30.867989m;
                    model.Longitude = 75.831828m;
                }
                else
                {
                    //(30.883995, 75.831523)
                    model.Latitude = 30.883995m;
                    model.Longitude = 75.831523m;
                }



                _locationService.LocationUpdate(model.ToModel());
                return Ok(model.Id.SuccessResponse("Location updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Location, (int)ErrorFunctionCode.Location_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Location_Update).ToString().ErrorResponse());
            }
        }



    }
}

