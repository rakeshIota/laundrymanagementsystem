using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.DeliveryTypeLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.DeliveryTypeLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class DeliveryTypeLanguageMappingController : ApiController
    {
        private IDeliveryTypeLanguageMappingService _deliverytypelanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DeliveryTypeLanguageMappingController(IDeliveryTypeLanguageMappingService deliverytypelanguagemappingService, IExceptionService exceptionService)
        {
            _deliverytypelanguagemappingService = deliverytypelanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all DeliveryTypeLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("DeliveryTypeLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllDeliveryTypeLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var deliverytypelanguagemappings = _deliverytypelanguagemappingService.SelectDeliveryTypeLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(deliverytypelanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryTypeLanguageMapping, (int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get deliverytypelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("deliverytypelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetDeliveryTypeLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var deliverytypelanguagemapping = _deliverytypelanguagemappingService.SelectDeliveryTypeLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (deliverytypelanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(deliverytypelanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryTypeLanguageMapping, (int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save deliverytypelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("deliverytypelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDeliveryTypeLanguageMapping(DeliveryTypeLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _deliverytypelanguagemappingService.DeliveryTypeLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DELIVERYTYPELANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("DeliveryTypeLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryTypeLanguageMapping, (int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update deliverytypelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("deliverytypelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDeliveryTypeLanguageMapping(DeliveryTypeLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _deliverytypelanguagemappingService.DeliveryTypeLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("DeliveryTypeLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryTypeLanguageMapping, (int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete deliverytypelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("deliverytypelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDeliveryTypeLanguageMapping(string id)
        {
        	try{
	            DeliveryTypeLanguageMappingViewModel model = new DeliveryTypeLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _deliverytypelanguagemappingService.DeliveryTypeLanguageMappingUpdate(model.ToModel());
	            return Ok("DeliveryTypeLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryTypeLanguageMapping, (int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryTypeLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

