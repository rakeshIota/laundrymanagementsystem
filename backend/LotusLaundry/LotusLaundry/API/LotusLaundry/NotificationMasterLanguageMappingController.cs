using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.NotificationMasterLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class NotificationMasterLanguageMappingController : ApiController
    {
        private INotificationMasterLanguageMappingService _notificationmasterlanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public NotificationMasterLanguageMappingController(INotificationMasterLanguageMappingService notificationmasterlanguagemappingService, IExceptionService exceptionService)
        {
            _notificationmasterlanguagemappingService = notificationmasterlanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all NotificationMasterLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("NotificationMasterLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllNotificationMasterLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var notificationmasterlanguagemappings = _notificationmasterlanguagemappingService.SelectNotificationMasterLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(notificationmasterlanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterLanguageMapping, (int)ErrorFunctionCode.NotificationMasterLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get notificationmasterlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmasterlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetNotificationMasterLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var notificationmasterlanguagemapping = _notificationmasterlanguagemappingService.SelectNotificationMasterLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (notificationmasterlanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(notificationmasterlanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterLanguageMapping, (int)ErrorFunctionCode.NotificationMasterLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save notificationmasterlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmasterlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveNotificationMasterLanguageMapping(NotificationMasterLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _notificationmasterlanguagemappingService.NotificationMasterLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("NotificationMasterLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterLanguageMapping, (int)ErrorFunctionCode.NotificationMasterLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update notificationmasterlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmasterlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateNotificationMasterLanguageMapping(NotificationMasterLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterlanguagemappingService.NotificationMasterLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("NotificationMasterLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterLanguageMapping, (int)ErrorFunctionCode.NotificationMasterLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete notificationmasterlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmasterlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteNotificationMasterLanguageMapping(string id)
        {
        	try{
	            NotificationMasterLanguageMappingViewModel model = new NotificationMasterLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterlanguagemappingService.NotificationMasterLanguageMappingUpdate(model.ToModel());
	            return Ok("NotificationMasterLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterLanguageMapping, (int)ErrorFunctionCode.NotificationMasterLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

