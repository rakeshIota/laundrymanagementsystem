using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.DeliveryType;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.DeliveryType;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class DeliveryTypeController : ApiController
    {
        private IDeliveryTypeService _deliverytypeService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DeliveryTypeController(IDeliveryTypeService deliverytypeService, IExceptionService exceptionService)
        {
            _deliverytypeService = deliverytypeService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all DeliveryType
        /// </summary>
        /// <returns></returns>
        [Route("DeliveryType")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllDeliveryType(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var deliverytype = _deliverytypeService.SelectDeliveryType(param).Select(x => x.ToViewModel()); ;
            	return Ok(deliverytype.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryType, (int)ErrorFunctionCode.DeliveryType_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryType_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get deliverytype by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("deliverytype/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetDeliveryType(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var deliverytype = _deliverytypeService.SelectDeliveryType(param).FirstOrDefault().ToViewModel();
	            if (deliverytype.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(deliverytype.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryType, (int)ErrorFunctionCode.DeliveryType_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryType_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save deliverytype
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("deliverytype")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDeliveryType(DeliveryTypeViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _deliverytypeService.DeliveryTypeInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DELIVERYTYPE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("DeliveryType save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryType, (int)ErrorFunctionCode.DeliveryType_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryType_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update deliverytype
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("deliverytype")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDeliveryType(DeliveryTypeViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _deliverytypeService.DeliveryTypeUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("DeliveryType updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryType, (int)ErrorFunctionCode.DeliveryType_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryType_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete deliverytype by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("deliverytype/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDeliveryType(string id)
        {
        	try{
	            DeliveryTypeViewModel model = new DeliveryTypeViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _deliverytypeService.DeliveryTypeUpdate(model.ToModel());
	            return Ok("DeliveryType Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DeliveryType, (int)ErrorFunctionCode.DeliveryType_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DeliveryType_Delete).ToString().ErrorResponse());
            }
        }
    }
}

