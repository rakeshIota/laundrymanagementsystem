using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.DistrictLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.DistrictLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class DistrictLanguageMappingController : ApiController
    {
        private IDistrictLanguageMappingService _districtlanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DistrictLanguageMappingController(IDistrictLanguageMappingService districtlanguagemappingService, IExceptionService exceptionService)
        {
            _districtlanguagemappingService = districtlanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all DistrictLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("DistrictLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllDistrictLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var districtlanguagemappings = _districtlanguagemappingService.SelectDistrictLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(districtlanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DistrictLanguageMapping, (int)ErrorFunctionCode.DistrictLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DistrictLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get districtlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("districtlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetDistrictLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var districtlanguagemapping = _districtlanguagemappingService.SelectDistrictLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (districtlanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(districtlanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DistrictLanguageMapping, (int)ErrorFunctionCode.DistrictLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DistrictLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save districtlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("districtlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDistrictLanguageMapping(DistrictLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _districtlanguagemappingService.DistrictLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DISTRICTLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("DistrictLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DistrictLanguageMapping, (int)ErrorFunctionCode.DistrictLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DistrictLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update districtlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("districtlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDistrictLanguageMapping(DistrictLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _districtlanguagemappingService.DistrictLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("DistrictLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DistrictLanguageMapping, (int)ErrorFunctionCode.DistrictLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DistrictLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete districtlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("districtlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDistrictLanguageMapping(string id)
        {
        	try{
	            DistrictLanguageMappingViewModel model = new DistrictLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _districtlanguagemappingService.DistrictLanguageMappingUpdate(model.ToModel());
	            return Ok("DistrictLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DistrictLanguageMapping, (int)ErrorFunctionCode.DistrictLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DistrictLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

