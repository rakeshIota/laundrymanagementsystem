using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Country;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Facade;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class CountryController : ApiController
    {
        private CountryManager _CountryManager;
        private IStateService _stateService;
        private ICountryService _countryService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CountryController(IStateService stateService,ICountryService countryService, IExceptionService exceptionService)
        {
            _stateService = stateService;
            _countryService = countryService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _CountryManager = new CountryManager(stateService);
        }

        /// <summary>
        ///  Api use for get all Country
        /// </summary>
        /// <returns></returns>
        [Route("Countries")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCountries(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var countries = _countryService.SelectCountry(param).Select(x => x.ToViewModel()); ;
            	return Ok(countries.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Country, (int)ErrorFunctionCode.Country_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Country_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get country by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("country/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCountry(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var country = _countryService.SelectCountry(param).FirstOrDefault().ToViewModel();
	            if (country.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(country.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Country, (int)ErrorFunctionCode.Country_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Country_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save country
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("country")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCountry(CountryViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _countryService.CountryInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.COUNTRY + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Country save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Country, (int)ErrorFunctionCode.Country_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Country_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update country
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("country")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCountry(CountryViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _countryService.CountryUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Country updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Country, (int)ErrorFunctionCode.Country_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Country_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete country by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("country/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCountry(string id)
        {
        	try{
	            CountryViewModel model = new CountryViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();


                if (!_CountryManager.AllowTodelete(model.Id))
                {
                    return Ok(((string)ErrorMessage.AccountError.COUNTRY_NOT_ALLOW_TO_DELETE).ToString().ErrorResponse());

                }


                _countryService.CountryUpdate(model.ToModel());
	            return Ok("Country Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Country, (int)ErrorFunctionCode.Country_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Country_Delete).ToString().ErrorResponse());
            }
        }
    }
}

