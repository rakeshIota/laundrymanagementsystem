using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Product;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Product;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class ProductController : ApiController
    {
        private IProductService _productService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProductController(IProductService productService, IExceptionService exceptionService)
        {
            _productService = productService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Product
        /// </summary>
        /// <returns></returns>
        [Route("Products")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProducts(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var products = _productService.SelectProduct(param).Select(x => x.ToViewModel()); ;
            	return Ok(products.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("product/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetProduct(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var product = _productService.SelectProduct(param).FirstOrDefault().ToViewModel();
	            if (product.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(product.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("product")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveProduct(ProductViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.IsActive = true;
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _productService.ProductInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PRODUCT + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Product save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("product")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateProduct(ProductViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productService.ProductUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Product updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("product/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteProduct(string id)
        {
        	try{
	            ProductViewModel model = new ProductViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productService.ProductUpdate(model.ToModel());
	            return Ok("Product Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Delete).ToString().ErrorResponse());
            }
        }



        [Route("product/filters")]
        //[CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProductfilter(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();

                List<ProductFilterModel> model = new List<ProductFilterModel>();

                ProductFilterModel Fmodel = new ProductFilterModel();
                Fmodel.name = ProductFilterConstants.Allk;
                Fmodel.value = ProductFilterConstants.AllV;
                model.Add(Fmodel);
                Fmodel = new ProductFilterModel();
                Fmodel.name = ProductFilterConstants.Malek;
                Fmodel.value = ProductFilterConstants.MaleV;
                model.Add(Fmodel);
                Fmodel = new ProductFilterModel();
                Fmodel.name = ProductFilterConstants.Femalek;
                Fmodel.value = ProductFilterConstants.FemaleV;
                model.Add(Fmodel);
                Fmodel = new ProductFilterModel();
                Fmodel.name = ProductFilterConstants.childrenk;
                Fmodel.value = ProductFilterConstants.childrenv;
                model.Add(Fmodel);
                Fmodel = new ProductFilterModel();
                Fmodel.name = ProductFilterConstants.HomeAppliancesk;
                Fmodel.value = ProductFilterConstants.HomeAppliancesV;
                model.Add(Fmodel);

                //model.Add(ProductFilterConstants.Male);
                //model.Add(ProductFilterConstants.Female);
                //model.Add(ProductFilterConstants.Both);
                //model.Add(ProductFilterConstants.children);
                //model.Add(ProductFilterConstants.HomeAppliances);


                return Ok(model.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Product, (int)ErrorFunctionCode.Product_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Product_Select).ToString().ErrorResponse());
            }
        }


    }
}

