using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;

using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Location;
using LotusLaundry.Service.PostalCode;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class PostalCodeController : ApiController
    {
        private IPostalCodeService _postalcodeService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PostalCodeController(IPostalCodeService postalcodeService, IExceptionService exceptionService)
        {
            _postalcodeService = postalcodeService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all PostalCode
        /// </summary>
        /// <returns></returns>
        [Route("PostalCodes")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPostalCodes(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var postalcodes = _postalcodeService.SelectPostalCode(param).Select(x => x.ToViewModel());
            	return Ok(postalcodes.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get postalcode by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("postalcode/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetPostalCode(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var postalcode = _postalcodeService.SelectPostalCode(param).FirstOrDefault().ToViewModel();
	            if (postalcode.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(postalcode.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save postalcode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("postalcode")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePostalCode(PostalCodeViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _postalcodeService.PostalCodeInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.POSTALCODE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("PostalCode save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update postalcode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("postalcode")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePostalCode(PostalCodeViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _postalcodeService.PostalCodeUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("PostalCode updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete postalcode by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("postalcode/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePostalCode(string id)
        {
        	try{
	            PostalCodeViewModel model = new PostalCodeViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _postalcodeService.PostalCodeUpdate(model.ToModel());
	            return Ok("PostalCode Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Delete).ToString().ErrorResponse());
            }
        }


        [Route("application/validate/PostalCode")]
       // [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult validatePostalCode(SearchParam param)
        {
            try
            {
               
               
                var postalcode = _postalcodeService.ValidatePostalCode(param).FirstOrDefault().ToViewModel();
                var result = false;

                if (postalcode.Id == null)
                {
                    //result = "No record found";
                    return Ok(result.ErrorResponse());
                }
                if(postalcode.IsActive.GetValueOrDefault())
                {
                    return Ok(result.SuccessResponse());
                }
                else
                {
                    //result = "false";
                    return Ok(result.ErrorResponse());
                }

             

            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PostalCode, (int)ErrorFunctionCode.PostalCode_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PostalCode_Single_Select).ToString().ErrorResponse());
            }
        }
    }
}

