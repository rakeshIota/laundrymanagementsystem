using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.ProductPrice;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProductPrice;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class ProductPriceController : ApiController
    {
        private IProductPriceService _productpriceService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProductPriceController(IProductPriceService productpriceService, IExceptionService exceptionService)
        {
            _productpriceService = productpriceService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all ProductPrice
        /// </summary>
        /// <returns></returns>
        [Route("ProductPrices")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProductPrices(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var productprices = _productpriceService.SelectProductPrice(param).Select(x => x.ToViewModel()); ;
            	return Ok(productprices.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductPrice, (int)ErrorFunctionCode.ProductPrice_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductPrice_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get productprice by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("productprice/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetProductPrice(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var productprice = _productpriceService.SelectProductPrice(param).FirstOrDefault().ToViewModel();
	            if (productprice.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(productprice.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductPrice, (int)ErrorFunctionCode.ProductPrice_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductPrice_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save productprice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("productprice")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveProductPrice(ProductPriceViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _productpriceService.ProductPriceInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PRODUCTPRICE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("ProductPrice save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductPrice, (int)ErrorFunctionCode.ProductPrice_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductPrice_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update productprice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("productprice")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateProductPrice(ProductPriceViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productpriceService.ProductPriceUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("ProductPrice updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductPrice, (int)ErrorFunctionCode.ProductPrice_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductPrice_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete productprice by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("productprice/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteProductPrice(string id)
        {
        	try{
	            ProductPriceViewModel model = new ProductPriceViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productpriceService.ProductPriceUpdate(model.ToModel());
	            return Ok("ProductPrice Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductPrice, (int)ErrorFunctionCode.ProductPrice_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductPrice_Delete).ToString().ErrorResponse());
            }
        }
    }
}

