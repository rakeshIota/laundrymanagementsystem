
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Promotion;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Promotion;
using LotusLaundry.Service.Promotion;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class PromotionController : ApiController
    {
        private IPromotionService _promotionService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PromotionController(IPromotionService promotionService, IExceptionService exceptionService)
        {
            _promotionService = promotionService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all ProjectDocument
        /// </summary>
        /// <returns></returns>
        [Route("promotions")]
       // [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPromotions(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                var projectdocuments = _promotionService.SelectPromotion(param).Select(x => x.ToViewModel()); ;
            	return Ok(projectdocuments.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Promotion, (int)ErrorFunctionCode.Promotion_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Promotion_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get projectdocument by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("promotion/{id}")]
      //  [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult Getpromotion(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.Id = id;	            
	            var projectdocument = _promotionService.SelectPromotion(param).FirstOrDefault().ToViewModel();
	            if (projectdocument.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(projectdocument.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Promotion, (int)ErrorFunctionCode.Promotion_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Promotion_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save projectdocument
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("promotion")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePromotion(PromotionViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _promotionService.PromotionInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PROMOTION + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("ProjectDocument save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Promotion, (int)ErrorFunctionCode.Promotion_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Promotion_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update projectdocument
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("promotion")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePromotion(PromotionViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
                _promotionService.PromotionUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Promotion updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Promotion, (int)ErrorFunctionCode.Promotion_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Promotion_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete projectdocument by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("promotion/{id}/{Fileid}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePromotion(string id,string Fileid =null)
        {
        	try{
                PromotionViewModel model = new PromotionViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;

                
             

                model.Id = id;

                FileGroupItemsViewModel file = new FileGroupItemsViewModel();
                //file.Id= Fileid != null ? Convert.ToInt64(CryptoEngine.Decrypt(Fileid.ToString(), KeyConstant.Key)) : 0,
                file.Id = Fileid;
                file.IsDeleted = true;
                file.IsActive = false;
                file.CreatedBy= model.UpdatedBy = User.Identity.GetUserId<long>();
                List<FileGroupItemsViewModel> Files = new List<FileGroupItemsViewModel>();
                Files.Add(file);


                model.File = Files;
                
                // model.Id = id;
                model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _promotionService.PromotionUpdate(model.ToModel());
	            return Ok("Promotion Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Promotion, (int)ErrorFunctionCode.Promotion_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Promotion_Delete).ToString().ErrorResponse());
            }
        }
    }
}

