using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.CurrencyLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CurrencyLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class CurrencyLanguageMappingController : ApiController
    {
        private ICurrencyLanguageMappingService _currencylanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CurrencyLanguageMappingController(ICurrencyLanguageMappingService currencylanguagemappingService, IExceptionService exceptionService)
        {
            _currencylanguagemappingService = currencylanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all CurrencyLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("CurrencyLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCurrencyLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var currencylanguagemappings = _currencylanguagemappingService.SelectCurrencyLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(currencylanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CurrencyLanguageMapping, (int)ErrorFunctionCode.CurrencyLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CurrencyLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get currencylanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("currencylanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCurrencyLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var currencylanguagemapping = _currencylanguagemappingService.SelectCurrencyLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (currencylanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(currencylanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CurrencyLanguageMapping, (int)ErrorFunctionCode.CurrencyLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CurrencyLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save currencylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("currencylanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCurrencyLanguageMapping(CurrencyLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _currencylanguagemappingService.CurrencyLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CURRENCYLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("CurrencyLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CurrencyLanguageMapping, (int)ErrorFunctionCode.CurrencyLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CurrencyLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update currencylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("currencylanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCurrencyLanguageMapping(CurrencyLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _currencylanguagemappingService.CurrencyLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("CurrencyLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CurrencyLanguageMapping, (int)ErrorFunctionCode.CurrencyLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CurrencyLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete currencylanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("currencylanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCurrencyLanguageMapping(string id)
        {
        	try{
	            CurrencyLanguageMappingViewModel model = new CurrencyLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _currencylanguagemappingService.CurrencyLanguageMappingUpdate(model.ToModel());
	            return Ok("CurrencyLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CurrencyLanguageMapping, (int)ErrorFunctionCode.CurrencyLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CurrencyLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

