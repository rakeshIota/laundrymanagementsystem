using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Currency;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Currency;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class CurrencyController : ApiController
    {
        private ICurrencyService _currencyService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CurrencyController(ICurrencyService currencyService, IExceptionService exceptionService)
        {
            _currencyService = currencyService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Currency
        /// </summary>
        /// <returns></returns>
        [Route("Currency")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCurrency(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var currency = _currencyService.SelectCurrency(param).Select(x => x.ToViewModel()); ;
            	return Ok(currency.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Currency, (int)ErrorFunctionCode.Currency_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Currency_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get currency by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("currency/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCurrency(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var currency = _currencyService.SelectCurrency(param).FirstOrDefault().ToViewModel();
	            if (currency.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(currency.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Currency, (int)ErrorFunctionCode.Currency_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Currency_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save currency
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("currency")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCurrency(CurrencyViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _currencyService.CurrencyInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CURRENCY + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Currency save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Currency, (int)ErrorFunctionCode.Currency_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Currency_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update currency
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("currency")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCurrency(CurrencyViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _currencyService.CurrencyUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Currency updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Currency, (int)ErrorFunctionCode.Currency_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Currency_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete currency by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("currency/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCurrency(string id)
        {
        	try{
	            CurrencyViewModel model = new CurrencyViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _currencyService.CurrencyUpdate(model.ToModel());
	            return Ok("Currency Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Currency, (int)ErrorFunctionCode.Currency_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Currency_Delete).ToString().ErrorResponse());
            }
        }
    }
}

