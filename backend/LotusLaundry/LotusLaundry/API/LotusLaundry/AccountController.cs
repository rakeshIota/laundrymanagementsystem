﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using LotusLaundry.Models;
using LotusLaundry.Framework.ViewModels.Users;
using System.Threading.Tasks;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using Microsoft.AspNet.Identity;
using LotusLaundry.Service.Users;
using LotusLaundry.Common.Constants;
using LotusLaundry.Core.Mailer;
using LotusLaundry.Common.Success;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Providers;
using LotusLaundry.Domain;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Facade;
using Microsoft.AspNet.Identity.EntityFramework;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Extra;
using LotusLaundry.Service.Order;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private AccountManager _accountManager;
        private IUsersService _usersService;
        private IEmailConfigurationService _emailConfiguration;
        private ManageApplicationUserModel _manageApplicationUserModel;
        private IOrderService _orderService;
        public AccountController(IUsersService usersService, IEmailConfigurationService emailConfiguration, IOrderService orderService)
        {
            _usersService = usersService;
            _orderService = orderService;
            _emailConfiguration = emailConfiguration;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _accountManager = new AccountManager();
        }
        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("register")]
        [HttpPost]
        
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        public IHttpActionResult RegisterUser([FromBody] UsersViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(ModelState.ErrorResponse());
                }
                if (UserManager.Users.Any(x => x.Email.Equals(model.Email)  && !x.IsDeleted))
                {
                    return Ok(string.Format(ErrorMessage.AccountError.EMAIL_ALREADY_REGISTERED, model.Email).ErrorResponse());
                }
                if (UserManager.Users.Any(x => x.EmployeeId.Equals(model.employeeid) && !x.IsDeleted))
                {
                    return Ok(string.Format(ErrorMessage.AccountError.USER_ALREADY_REGISTERED, model.UserName).ErrorResponse());
                }

                if (model.RoleName == "ROLE_DRIVER")
                {
                    if (UserManager.Users.Any(x => x.NationalId.Equals(model.NationalId) && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.NATIONALID_ALREADY_REGISTERED, model.NationalId).ErrorResponse());
                    }
                    if (UserManager.Users.Any(x => x.LicenceId.Equals(model.LicenceId) && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.LICENCE_ALREADY_REGISTERED, model.LicenceId).ErrorResponse());
                    }

                    if (UserManager.Users.Any(x => x.PhoneNumber.Equals(model.PhoneNumber) && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.PHONE_ALREADY_EXISTS, model.LicenceId).ErrorResponse());
                    }

                    if (UserManager.Users.Any(x => x.BikeInformation.Equals(model.BikeInformation) && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.Bike_ALREADY_REGISTERED, model.BikeInformation).ErrorResponse());
                    }
                    if (UserManager.Users.Any(x => x.LicencePlate.Equals(model.LicencePlate) && !x.IsDeleted))
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.PlATE_ALREADY_REGISTERED, model.LicencePlate).ErrorResponse());
                    }
                }

                var user = _manageApplicationUserModel.UserViewModelToApplicationUserModel(model);
                if (model.RoleName == "ROLE_OPERATION" || model.RoleName == "ROLE_SUPER_ADMIN" || model.RoleName == "ROLE_ADMIN")
                {
                    user.EmailConfirmed = true;
                    user.IsActive = true;
                    user.UserName = model.employeeid;
                    user.Status = "Active";
                }
                if(model.RoleName== "ROLE_DRIVER")
                {
                    user.UserName = model.employeeid;
                }
                user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                IdentityResult result = UserManager.Create(user);

                if (model.AllFile != null && model.AllFile.Count>0)
                {
                    try
                    {
                        
                        user = UserManager.FindByEmail(model.Email);
                        model.Id =(CryptoEngine.Encrypt(FileType.USER + "_" + user.Id.ToString(), KeyConstant.Key));
                        string path = _usersService.FileInsert(model.ToModel());    

                    }
                    catch
                    {

                    }
                }


                if (!result.Succeeded && result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        return Ok(error.ErrorResponse());
                    }
                }
                UserManager.AddToRole(user.Id, model.RoleName);
                if (model.RoleName == "ROLE_OPERATION" || model.RoleName == "ROLE_SUPER_ADMIN" || model.RoleName == "ROLE_ADMIN")
                {
                   
                }
                else
                {
                    SendEmailForEmailConfirm(user);
                }
               // SendEmailForEmailConfirm(user.Id, user.Email, model.FirstName + " " + model.LastName, user.UniqueCode);
                return Ok(SuccessMessage.AccountSuccess.USER_REGISTERED.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

		[Route("users")]
		[HttpPost]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        public IHttpActionResult GetUsers(SearchParam param)
		{
		try
		{
                var roles = ManageClaims.GetUserRole();
                //SearchParam param = new SearchParam();
                param = _accountManager.filterparam(param);

                if( (param.RelationTable == "DRIVER"))
                {
                    var user = _usersService.SelectDeriver(param).Select(x => x.ToViewModel());
                    return Ok(user.SuccessResponse());
                }
                else
                {
                    var user = _usersService.SelectUser(param).Select(x => x.ToViewModel());


                    return Ok(user.SuccessResponse());
                }

		        
		       
		
		}
		catch (Exception ex)
		{
		return Ok(ex.Message.ErrorResponse());
		}
		}


        [Route("user/{id}")]
        [HttpGet]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]

        public IHttpActionResult GetUser([FromUri] SearchParam param)
        {
            try
            {

                var user = _usersService.SelectUser(param).FirstOrDefault().ToViewModel();
                return Ok(user.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("customer/{id}")]
        [HttpGet]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]

        public IHttpActionResult Getcustomer([FromUri] SearchParam param)
        {
            try
            {

                var user = _usersService.SelectCustomer(param).FirstOrDefault().ToViewModel();
                return Ok(user.SuccessResponse());

            }

            catch (Exception ex)
            { 
		     return Ok(ex.Message.ErrorResponse());
		     }
		}


        [Route("driver/{id}")]
        [HttpGet]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]

        public IHttpActionResult Getdriver([FromUri] SearchParam param)
         {
            try
            {

                var user = _usersService.SelectDeriver(param).FirstOrDefault().ToViewModel();
                return Ok(user.SuccessResponse());

            }

            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        [Route("update")]
		[HttpPut]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        public IHttpActionResult UpdateUserDetails(UsersViewModel model)
		{
		try
		{
		var user = UserManager.FindById(Convert.ToInt64(CryptoEngine.Decrypt(model.Id, KeyConstant.Key)));
              
                if (model.Email != user.Email)
                {
                  var AlreadyUser=  UserManager.FindByEmail(model.Email);
                    if(AlreadyUser!=null)
                    {
                        return Ok(string.Format(ErrorMessage.AccountError.EMAIL_ALREADY_REGISTERED, model.Email).ErrorResponse());
                    }
                }


                user   = _accountManager.ViewModelToApplicatioUser(model,user);

		        
		
		   UserManager.Update(user);

                if (model.AllFile != null)
                {
                    try
                    {

                        user = UserManager.FindByEmail(model.Email);
                        model.Id = (CryptoEngine.Encrypt(FileType.USER + "_" + user.Id.ToString(), KeyConstant.Key));
                        string path = _usersService.FileInsert(model.ToModel());

                    }
                    catch
                    {

                    }
                }


                return Ok("User details updated successfully.".SuccessResponse());
		}
		catch (Exception ex)
		{
		return Ok(ex.Message.ErrorResponse());
		}
		}
		
		
		[Route("user/{id}")]
		[HttpDelete]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        public IHttpActionResult DeleteUser([FromUri] SearchParam param)
		{
		try
		{
		var user = UserManager.FindById(Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key)));

                param = param ?? new SearchParam();
                param.driverId = user.Id;
                var orders = _orderService.SelectDriverActiveOrder(param);
                
                if(orders.Count>0)
                {
                    return Ok(ErrorMessage.AccountError.DRIVERHASODERTODELIVER.ErrorResponse());
                }

                user.IsDeleted = true;
                //	user.Email = user.UserName = user.Email + "deleted" + CommonMethods.GetUniqueKey(6);
        user.Status = "Deleted";
		UserManager.Update(user);
		return Ok(SuccessMessage.AccountSuccess.DELETED.SuccessResponse());
		}
		catch (Exception ex)
		{
		return Ok(ex.Message.ErrorResponse());
		}
		}
        [Route("app/register")]
        [HttpPost]
        public IHttpActionResult RegisterUserApp([FromBody] UsersViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(ModelState.ErrorResponse());
                }
                if (UserManager.Users.Any(x => x.Email.Equals(model.Email) && !x.IsDeleted))
                {
                    return Ok(string.Format(ErrorMessage.AccountError.EMAIL_ALREADY_REGISTERED, model.Email).ErrorResponse());
                }
                var user = _manageApplicationUserModel.UserViewModelToApplicationUserModel(model);
                IdentityResult result = UserManager.Create(user, model.Password);
                if (!result.Succeeded && result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        return Ok(error.ErrorResponse());
                    }
                }
                UserManager.AddToRole(user.Id, model.RoleName);

                SendEmailForEmailConfirm(user.Id, user.Email, model.FirstName + " " + model.LastName, user.UniqueCode);
                return Ok(SuccessMessage.AccountSuccess.USER_REGISTERED.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }
        /// <summary>
        /// Method for send email for email varification and password reset.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="toEmail"></param>
        /// <param name="fullName"></param>
        /// <param name="uniqueCode"></param>
        public void SendEmailForEmailConfirm(long userId, string toEmail, string fullName, string uniqueCode)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.EmailVerification.ToString());

            string code = UserManager.GenerateEmailConfirmationToken(userId);
            code = HttpUtility.UrlEncode(code);
            var callbackUrl = UrlConstants.baseUrl + "/reset-password?p=" + uniqueCode + "&code=" + code;
            var emailBody = string.Format(configData.ConfigurationValue, fullName, callbackUrl);
            MailSender.SendEmail(toEmail, EmailConstants.CONFIRM_YOUR_ACCOUNT, emailBody).Wait();
        }

        #region Login
        /// <summary>
        /// api use to login the app
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login(UsersViewModel model)
        {
            var userFound = UserManager.Find(model.UserName, model.Password);
            if (userFound == null)
            {
                return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
            }
            var r = userFound.Roles.FirstOrDefault();
            if (r.RoleId == 2 || r.RoleId == 3)
            {
                return Ok(ErrorMessage.AccountError.INCORRECT.ErrorResponse());
            }


            if (userFound.IsDeleted)
            {
                return Ok(ErrorMessage.AccountError.DELETED.ErrorResponse());
            }


            if (userFound.EmailConfirmed != true)
            {
                return Ok(ErrorMessage.AccountError.CONFIRM.ErrorResponse());
            }

            if (userFound.IsActive != true)
            {
                return Ok(ErrorMessage.AccountError.ACTIVATE.ErrorResponse());
            }
          
           

            return await GetLoginJson(userFound);
        }

        /// <summary>
        /// get login user detail 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> GetLoginJson(ApplicationUser user)
        {
            var token = await CreateToken(user.Email);
            if (string.IsNullOrEmpty(token.Error)) //if error is not found return token 
            {
                if (user != null)
                {
                    token.TokenData = new tokenData
                    {
                        Token = new tokenObj
                        {
                            Expires = DateTimeOffset.Now.AddDays(14),
                            AccessToken = token.AccessToken,
                            TokenType = token.TokenType
                        },
                        User = _manageApplicationUserModel.ApplicationUserModelToUserViewModel(user),
                    };

                }
                return Ok(token.TokenData.SuccessResponse());
            }
            return Ok(token.ErrorDescription.ErrorResponse());
        }


        public void TwoFactorAuthentication(ApplicationUser user)
        {
            user.OTP = CommonMethods.GetOTP();
            user.OTPValidTill = DateTime.UtcNow.AddMinutes(15);
            UserManager.Update(user);
            var fullName = user.FirstName + " " + user.LastName;
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.ForgotPassword.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, fullName, user.OTP);
            MailSender.SendEmail(user.Email, EmailConstants.CONFIRM_YOUR_ACCOUNT, emailBody).Wait();
        }

        #endregion

        #region Forgot-Password
        /// <summary>
        /// send mail for forget password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("forgot/password")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ForgotPassword(UsersViewModel model)
        {
            try
            {
                string host = HttpContext.Current.Request.Url.Host;
                var user = UserManager.FindByName(model.UserName);
         //       UserManager.findby
                if (user == null) return Ok(ErrorMessage.AccountError.INVALID_USERID.ErrorResponse());

                string code = UserManager.GeneratePasswordResetToken(user.Id);
                code = HttpUtility.UrlEncode(code);
                //account/recover/:ucode
                var callbackUrl = "http://" + host + "/account/recover/" + user.UniqueCode + "?code=" + code;
                // var userData = _usersService.SelectUserByUniqueCode(user.UniqueCode);
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.ForgotPasswordAdminPanel.ToString());
                var emailBody = string.Format(configData.ConfigurationValue, user.FirstName, callbackUrl);
                MailSender.SendEmail(user.Email, EmailConstants.FORGOT_PASSWORD, emailBody).Wait();
                return Ok(SuccessMessage.AccountSuccess.FORGOT_PASSWORD_LINK.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }
        /// <summary>
        /// api use for reset the password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("reset/password")]
        public IHttpActionResult ResetPassword(UsersViewModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.UniqueCode))
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                var user = UserManager.Users.Where(x => x.UniqueCode.Equals(model.UniqueCode)).FirstOrDefault();
                //_usersService.SelectUserByUniqueCode(model.UniqueCode);
                if (user == null)
                    return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                IdentityResult result = UserManager.ResetPassword(Convert.ToInt64(user.Id), HttpUtility.UrlDecode(model.Code), model.Password);
                if (result.Succeeded)
                {
                    return Ok(SuccessMessage.AccountSuccess.PASSWORD_CHANGED.SuccessResponse());
                }
                else
                {
                    return Ok(result.Errors.FirstOrDefault().ErrorResponse());
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        /// <summary>
        /// Email confirmation and password reset api
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("confirm/email")]
        public async Task<IHttpActionResult> ConfirmEmail(UsersViewModel model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.UniqueCode))
                {
                    return Ok(ErrorMessage.AccountError.INVALID.ErrorResponse());
                }
                var user = _usersService.SelectUserByUniqueCode(model.UniqueCode);
                if (user == null)
                    return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                IdentityResult result = await this.UserManager.ConfirmEmailAsync(Convert.ToInt64(user.Id), HttpUtility.UrlDecode(model.Code));
                if (result.Succeeded)
                {
                    var appUser = UserManager.FindByEmail(user.Email);
                    appUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                    appUser.IsActive = true;
                    await UserManager.UpdateAsync(appUser);
                    //string code = UserManager.GenerateEmailConfirmationToken(user.Id);
                    return Ok(SuccessMessage.AccountSuccess.PASSWORD_CHANGED.SuccessResponse());
                }
                else
                {
                    return Ok(ErrorMessage.AccountError.INVALID_LINK.ErrorResponse());
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        /// <summary>
        /// Change password api
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("changePassword")]
        [CustomAuthorize]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(User.Identity.GetUserId<long>());
              
                var userFound = UserManager.Find(user.UserName, model.OldPassword);
                if(userFound == null)
                {
                    return Ok("Old password does not match".ErrorResponse());
                }
                if (user != null)
                {
                    user.IsActive = true;
                    user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
                    await UserManager.UpdateAsync(user);
                    return Ok("Password has been changed successfully".SuccessResponse());
                }
                return Ok("Invalid user please login.".ErrorResponse());
            }
            catch (Exception ex)
            {
             
                return Ok(ex.Message.ErrorResponse());
            }
        }
        /// <summary>
        /// User details api
        /// </summary>
        /// <returns></returns>
        [Route("user")]
        [CustomAuthorize]
        [HttpGet]
        public IHttpActionResult GetUserDetail()
        {
            var userId = User.Identity.GetUserId<long>();
            var userProfile = UserManager.FindById(userId);
            if (userProfile != null)
            {

                var User = new UsersViewModel
                {
                    FirstName = userProfile.FirstName,
                    Roles = UserManager.GetRoles(userId).ToList(),
                    LastName = userProfile.LastName,
                    UserName = userProfile.Email,
                    FullName = userProfile.FirstName+" "+ userProfile.LastName,
                    Id = CryptoEngine.Encrypt(FileType.USERS + "_" + userId.ToString(), KeyConstant.Key),
                    ProfileImageUrl = userProfile.ProfilePic,
                    UniqueCode = userProfile.UniqueCode,
                    PhoneNumber = userProfile.PhoneNumber
                  
                };
                return Ok(User.SuccessResponse());
            }
            return Ok("Could Not Fetch Profile".ErrorResponse());
        }

        /// <summary>
        /// Update user details api
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("user")]
        [CustomAuthorize(UserRole.Admin, UserRole.SUPERADMIN,UserRole.OPERATIONMANAGER)]
        [HttpPut]
        public IHttpActionResult UpdateUserDetail(UsersViewModel model)
        {
            try
            {
                if (model == null)
                    return Ok("Invalid data".ErrorResponse());

                var user = UserManager.FindById(User.Identity.GetUserId<long>());
               // var user = UserManager.FindById(Convert.ToInt64(model.Id));
                var userEmailCheck = UserManager.FindByEmail(user.Email);
                if (userEmailCheck != null && userEmailCheck.Id != user.Id && !user.IsDeleted)
                {
                    return Ok(string.Format("{0} Email already registered with another account.", user.Email).ErrorResponse());
                }
                if (userEmailCheck != null && userEmailCheck.Id != user.Id && !user.IsDeleted)
                {
                    return Ok(string.Format("{0} Email already registered with another account.", user.Email).ErrorResponse());
                }
                if (userEmailCheck != null && userEmailCheck.EmployeeId != user.EmployeeId && !user.IsDeleted)
                {
                    return Ok(string.Format("{0} Employee Id already registered with another account.", user.Email).ErrorResponse());
                }

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
               

                UserManager.Update(user);
                if (model.AllFile != null)
                {
                    try
                    {

                        user = UserManager.FindByEmail(model.Email);
                        model.Id = (CryptoEngine.Encrypt(FileType.USER + "_" + user.Id.ToString(), KeyConstant.Key));
                        string path = _usersService.FileInsert(model.ToModel());

                    }
                    catch
                    {

                    }
                }
                return Ok("User details updated successfully.".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Create Token object for user
        /// </summary>
        /// <param name="user">Login model for User</param>
        /// <returns>Token View Model</returns>
        async Task<TokenViewModel> CreateToken(string email)
        {
            IEnumerable<KeyValuePair<string, string>> postData = new Dictionary<string, string>
            {
                {"username",email},
                {"password","temp"},
                {"grant_type","password"}
            };
            var WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

            using (var httpClient = new HttpClient())
            {
                using (var content = new FormUrlEncodedContent(postData))
                {
                    content.Headers.Clear();
                    content.Headers.Add("Content-Type", WWW_FORM_URLENCODED);

                    content.Headers.Add("user-found", "true");
                   HttpResponseMessage response = await httpClient.PostAsync(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/api/token", content);
              //      HttpResponseMessage response = await httpClient.PostAsync("http://lotuslaundry.azurewebsites.net" + "/api/token", content);

                    var respo = await response.Content.ReadAsStringAsync();
                    try
                    {
                        return JsonConvert.DeserializeObject<TokenViewModel>(respo);
                    }
                    catch (JsonReaderException ex)
                    {
                        return null;//throw new ("Internal Error Occured during Json Deserialization. " + respo, ex);
                    }
                }
            }
        }
        /// <summary>
        /// User activation and de-activation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("user/activate-deactivate")]
        [CustomAuthorize]
        [HttpPut]
        public IHttpActionResult UserActivateAndDeActivate(UsersViewModel model)
        {
            try
            {
                var appUser = UserManager.FindById(Convert.ToInt64(model.Id));
                var id = Convert.ToInt64(CryptoEngine.Decrypt(model.Id, KeyConstant.Key));
                if (Convert.ToBoolean(model.IsActive))
                {
                    appUser.IsActive = true;
                    UserManager.Update(appUser);
                    SendEmailForAccountActivation(id, appUser.Email, model.FullName);
                    return Ok(SuccessMessage.AccountSuccess.ACTIVATED.SuccessResponse());
                }
                else
                {
                    appUser.IsActive = false;
                    UserManager.Update(appUser);
                    SendEmailForAccountDeactivation(id, appUser.Email, model.FullName);
                    return Ok(SuccessMessage.AccountSuccess.DEACTIVATED.SuccessResponse());
                }
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }
        
        public void SendEmailForAccountActivation(long? userId, string toEmail, string fullName)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.AccountActivated.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, fullName);
            MailSender.SendEmail(toEmail, EmailConstants.ACCOUNT_ACTIVATED, emailBody).Wait();
        }
        
        public void SendEmailForAccountDeactivation(long? userId, string toEmail, string fullName)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.AccountDeactivated.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, fullName);
            MailSender.SendEmail(toEmail, EmailConstants.ACCOUNT_DEACTIVATED, emailBody).Wait();
        }

        [Route("user")]
        [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        [HttpDelete]
        public IHttpActionResult DeleteUser(UsersViewModel model)
        {
            try
            {
                var user = UserManager.FindById(Convert.ToInt64(model.Id));
                user.IsDeleted = true;
                //user.Email = user.UserName =  user.Email + "deleted" + CommonMethods.GetUniqueKey(6);
                UserManager.Update(user);
                return Ok(SuccessMessage.AccountSuccess.DELETED.SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }
        #endregion

        [AllowAnonymous]
        [HttpPost]
        [Route("user/social/register")]
        public async Task<IHttpActionResult> Register(UsersViewModel model)
        {
            string roleName = UserRole.Admin;
            #region Intializing ApplicationUser
            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                FacebookId = model.FacebookId,
                IsFacebookConnected = model.FacebookId != null ? true : false,
                GoogleId = model.GoogleId,
                IsGoogleConnected = model.GoogleId != null ? true : false,
                PhoneNumber = model.PhoneNumber,
                FirstName = model.FirstName,
                LastName = model.LastName
            };
            #endregion
            #region Social Login
            user.EmailConfirmed = true;
            var returnVal = await SaveExternalLoginInfo(user, roleName);

            if (returnVal.UserRegistered == UserRegistered.Failed) return Ok("Failed".ErrorResponse());

            return await GetLoginJson(returnVal.User);
            #endregion
        }



        [Route("Roles")]
        [HttpPost]
        // [CustomAuthorize]
        public IHttpActionResult GetAllRoles()
        {
            try
            {
                SearchParam param = new SearchParam();
                var user = _usersService.SelectAllRoles(param).Select(x => x.ToRoleViewModel());
                


                return Ok(user.SuccessResponse());

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        private async Task<UserDetails> SaveExternalLoginInfo(ApplicationUser user, string roleName)
        {
            var idenityresult = await UserManager.CreateAsync(user);
            UserRegistered UserRegistered;
            ApplicationUser userModel = null;
            if (!idenityresult.Succeeded)
            {
                UserRegistered = UserRegistered.AlreadyRegister;
                /*Check for user is already registered*/
                userModel = UserManager.FindByEmail(user.Email);
                if (userModel == null) UserRegistered = UserRegistered.Failed;

                if ((Convert.ToBoolean(userModel.IsFacebookConnected) && Convert.ToBoolean(user.IsFacebookConnected) && (userModel.FacebookId == user.FacebookId)) ||
                (Convert.ToBoolean(userModel.IsGoogleConnected) && Convert.ToBoolean(user.IsGoogleConnected) && (userModel.GoogleId == user.GoogleId)))
                {
                    UserRegistered = UserRegistered.AlreadyRegister;
                }
                else if (Convert.ToBoolean(user.IsGoogleConnected))
                {
                    userModel.IsGoogleConnected = user.IsGoogleConnected;
                    userModel.GoogleId = user.GoogleId;
                    await UserManager.UpdateAsync(userModel);
                }
                else if (Convert.ToBoolean(user.IsFacebookConnected))
                {
                    userModel.IsFacebookConnected = user.IsFacebookConnected;
                    userModel.FacebookId = user.FacebookId;
                    await UserManager.UpdateAsync(userModel);
                }
                else
                {
                    UserRegistered = UserRegistered.Failed;
                }
            }
            else
            {
                userModel = UserManager.FindByEmail(user.Email);
                UserRegistered = UserRegistered.NowRegitered;
                UserManager.AddToRole(user.Id, roleName);
            }
            return new UserDetails
            {
                User = userModel,
                UserRegistered = UserRegistered
            };
        }

        public void SendEmailForEmailConfirm(ApplicationUser model)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.EmailVerification.ToString());

            string code = UserManager.GenerateEmailConfirmationToken(model.Id);
            code = HttpUtility.UrlEncode(code);
            // var callbackUrl = UrlConstants.baseUrl + "/app/customer/confirm-account?uniqueCode=" + model.UniqueCode + "&code=" + code;

            var callbackUrl = UrlConstants.baseUrl + "/register/confirm?uniqueCode=" + model.UniqueCode + "&code=" + code;
            var emailBody = string.Format(configData.ConfigurationValue, model.FirstName + " " + model.LastName, callbackUrl);
            MailSender.SendEmail(model.Email, EmailConstants.CONFIRM_YOUR_ACCOUNT, emailBody).Wait();
        }

        public void SendWelcomeEmail(string toEmail, string fullName)
        {
            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.WelcomeEmail.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, fullName);
            MailSender.SendEmail(toEmail, EmailConstants.WELOME_EMAIL, emailBody).Wait();
        }

        [Route("admin/drivers")]
        [HttpPost]
        [CustomAuthorize]
        public IHttpActionResult GetDrivers(SearchParam param)
        {
            try
            {
                var roles = ManageClaims.GetUserRole();
                //SearchParam param = new SearchParam();
                param = _accountManager.filterparam(param);
                    var user = _usersService.SelectDeriver(param).Select(x => x.ToViewModel());

                    var OnlyActiveDriver=  user.Where(x => x.IsDeleted == false);
                    return Ok(OnlyActiveDriver.SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("admin/customerNote")]
        [HttpPut]
      //  [CustomAuthorize(UserRole.Admin, UserRole.OPERATIONMANAGER, UserRole.SUPERADMIN)]
        public IHttpActionResult UpdateCustomerNote(AdminNoteViewModel model)
        {
            try
            {
                var user = UserManager.FindById(Convert.ToInt64(CryptoEngine.Decrypt(model.Id, KeyConstant.Key)));

                // user = _accountManager.ViewModelToApplicatioUser(model, user);

                user.adminNote = model.note;

                UserManager.Update(user);

               


                return Ok("Customer Note updated successfully.".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("get/user/info")]
       // [Authorize]
        [HttpPost]
        public IHttpActionResult GetUserBasicInfo(List<SearchParam> param)
        {
            try
            {
                string UserIds = string.Empty;

                if (param.Count == 1)
                {
                    param.ForEach(x => { UserIds += x.UserId ; });
                }
                else
                {
                    param.ForEach(x => { UserIds += x.UserId + ","; });
                }
                //foreach (var item in param)
                //{
                //    UserIds += (CryptoEngine.Decrypt(item.Id.ToString(), KeyConstant.Key)) + ",";
                //}

                var userList = _usersService.UserBasicInfoSelect(UserIds).Select(x => x.TobasicViewModel());

                
                

                return Ok(userList.SuccessResponse());
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException.Message;
                return Ok(errorMessage.ErrorResponse());
            }
        }



    }

    

    public class UserDetails
    {
        public ApplicationUser User { get; set; }
        public UserRegistered UserRegistered { get; set; }
    }
}
