using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.NotificationMasterKeyValueMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterKeyValueMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class NotificationMasterKeyValueMappingController : ApiController
    {
        private INotificationMasterKeyValueMappingService _notificationmasterkeyvaluemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public NotificationMasterKeyValueMappingController(INotificationMasterKeyValueMappingService notificationmasterkeyvaluemappingService, IExceptionService exceptionService)
        {
            _notificationmasterkeyvaluemappingService = notificationmasterkeyvaluemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all NotificationMasterKeyValueMapping
        /// </summary>
        /// <returns></returns>
        [Route("NotificationMasterKeyValueMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllNotificationMasterKeyValueMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var notificationmasterkeyvaluemappings = _notificationmasterkeyvaluemappingService.SelectNotificationMasterKeyValueMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(notificationmasterkeyvaluemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterKeyValueMapping, (int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get notificationmasterkeyvaluemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmasterkeyvaluemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetNotificationMasterKeyValueMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var notificationmasterkeyvaluemapping = _notificationmasterkeyvaluemappingService.SelectNotificationMasterKeyValueMapping(param).FirstOrDefault().ToViewModel();
	            if (notificationmasterkeyvaluemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(notificationmasterkeyvaluemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterKeyValueMapping, (int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save notificationmasterkeyvaluemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmasterkeyvaluemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveNotificationMasterKeyValueMapping(NotificationMasterKeyValueMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _notificationmasterkeyvaluemappingService.NotificationMasterKeyValueMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERKEYVALUEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("NotificationMasterKeyValueMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterKeyValueMapping, (int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update notificationmasterkeyvaluemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmasterkeyvaluemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateNotificationMasterKeyValueMapping(NotificationMasterKeyValueMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterkeyvaluemappingService.NotificationMasterKeyValueMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("NotificationMasterKeyValueMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterKeyValueMapping, (int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete notificationmasterkeyvaluemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmasterkeyvaluemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteNotificationMasterKeyValueMapping(string id)
        {
        	try{
	            NotificationMasterKeyValueMappingViewModel model = new NotificationMasterKeyValueMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterkeyvaluemappingService.NotificationMasterKeyValueMappingUpdate(model.ToModel());
	            return Ok("NotificationMasterKeyValueMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMasterKeyValueMapping, (int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMasterKeyValueMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

