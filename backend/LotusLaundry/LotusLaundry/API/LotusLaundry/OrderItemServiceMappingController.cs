using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.OrderItemServiceMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItemServiceMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class OrderItemServiceMappingController : ApiController
    {
        private IOrderItemServiceMappingService _orderitemservicemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public OrderItemServiceMappingController(IOrderItemServiceMappingService orderitemservicemappingService, IExceptionService exceptionService)
        {
            _orderitemservicemappingService = orderitemservicemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all OrderItemServiceMapping
        /// </summary>
        /// <returns></returns>
        [Route("OrderItemServiceMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllOrderItemServiceMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var orderitemservicemappings = _orderitemservicemappingService.SelectOrderItemServiceMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(orderitemservicemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItemServiceMapping, (int)ErrorFunctionCode.OrderItemServiceMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItemServiceMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get orderitemservicemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderitemservicemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetOrderItemServiceMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var orderitemservicemapping = _orderitemservicemappingService.SelectOrderItemServiceMapping(param).FirstOrDefault().ToViewModel();
	            if (orderitemservicemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(orderitemservicemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItemServiceMapping, (int)ErrorFunctionCode.OrderItemServiceMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItemServiceMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save orderitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderitemservicemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveOrderItemServiceMapping(OrderItemServiceMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _orderitemservicemappingService.OrderItemServiceMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.ORDERITEMSERVICEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("OrderItemServiceMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItemServiceMapping, (int)ErrorFunctionCode.OrderItemServiceMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItemServiceMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update orderitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderitemservicemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrderItemServiceMapping(OrderItemServiceMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderitemservicemappingService.OrderItemServiceMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("OrderItemServiceMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItemServiceMapping, (int)ErrorFunctionCode.OrderItemServiceMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItemServiceMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete orderitemservicemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderitemservicemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteOrderItemServiceMapping(string id)
        {
        	try{
	            OrderItemServiceMappingViewModel model = new OrderItemServiceMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderitemservicemappingService.OrderItemServiceMappingUpdate(model.ToModel());
	            return Ok("OrderItemServiceMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItemServiceMapping, (int)ErrorFunctionCode.OrderItemServiceMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItemServiceMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

