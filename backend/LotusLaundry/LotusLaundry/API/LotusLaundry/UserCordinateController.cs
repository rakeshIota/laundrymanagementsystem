using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.UserCordinate;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserCordinate;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class UserCordinateController : ApiController
    {
        private IUserCordinateService _usercordinateService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public UserCordinateController(IUserCordinateService usercordinateService, IExceptionService exceptionService)
        {
            _usercordinateService = usercordinateService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all UserCordinate
        /// </summary>
        /// <returns></returns>
        [Route("UserCordinates")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllUserCordinates(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var usercordinates = _usercordinateService.SelectUserCordinate(param).Select(x => x.ToViewModel()); ;
            	return Ok(usercordinates.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserCordinate, (int)ErrorFunctionCode.UserCordinate_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserCordinate_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get usercordinate by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("usercordinate/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetUserCordinate(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var usercordinate = _usercordinateService.SelectUserCordinate(param).FirstOrDefault().ToViewModel();
	            if (usercordinate.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(usercordinate.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserCordinate, (int)ErrorFunctionCode.UserCordinate_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserCordinate_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save usercordinate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("usercordinate")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveUserCordinate(UserCordinateViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _usercordinateService.UserCordinateInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.USERCORDINATE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("UserCordinate save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserCordinate, (int)ErrorFunctionCode.UserCordinate_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserCordinate_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update usercordinate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("usercordinate")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateUserCordinate(UserCordinateViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _usercordinateService.UserCordinateUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("UserCordinate updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserCordinate, (int)ErrorFunctionCode.UserCordinate_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserCordinate_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete usercordinate by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("usercordinate/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteUserCordinate(string id)
        {
        	try{
	            UserCordinateViewModel model = new UserCordinateViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _usercordinateService.UserCordinateUpdate(model.ToModel());
	            return Ok("UserCordinate Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserCordinate, (int)ErrorFunctionCode.UserCordinate_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserCordinate_Delete).ToString().ErrorResponse());
            }
        }
    }
}

