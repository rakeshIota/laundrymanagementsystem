using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMaster;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.NotificationMaster;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.DataSecurity;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class NotificationMasterController : ApiController
    {
        private INotificationMasterService _notificationmasterService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public NotificationMasterController(INotificationMasterService notificationmasterService, IExceptionService exceptionService)
        {
            _notificationmasterService = notificationmasterService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all NotificationMaster
        /// </summary>
        /// <returns></returns>
        [Route("NotificationMasters")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllNotificationMasters(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var notificationmasters = _notificationmasterService.SelectNotificationMaster(param).Select(x => x.ToViewModel()); ;
            	return Ok(notificationmasters.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMaster, (int)ErrorFunctionCode.NotificationMaster_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMaster_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get notificationmaster by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmaster/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetNotificationMaster(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var notificationmaster = _notificationmasterService.SelectNotificationMaster(param).FirstOrDefault().ToViewModel();
	            if (notificationmaster.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(notificationmaster.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMaster, (int)ErrorFunctionCode.NotificationMaster_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMaster_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save notificationmaster
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmaster")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveNotificationMaster(NotificationMasterViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _notificationmasterService.NotificationMasterInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTER + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("NotificationMaster save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMaster, (int)ErrorFunctionCode.NotificationMaster_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMaster_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update notificationmaster
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notificationmaster")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateNotificationMaster(NotificationMasterViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterService.NotificationMasterUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("NotificationMaster updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMaster, (int)ErrorFunctionCode.NotificationMaster_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMaster_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete notificationmaster by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notificationmaster/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteNotificationMaster(string id)
        {
        	try{
	            NotificationMasterViewModel model = new NotificationMasterViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationmasterService.NotificationMasterUpdate(model.ToModel());
	            return Ok("NotificationMaster Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.NotificationMaster, (int)ErrorFunctionCode.NotificationMaster_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.NotificationMaster_Delete).ToString().ErrorResponse());
            }
        }
    }
}

