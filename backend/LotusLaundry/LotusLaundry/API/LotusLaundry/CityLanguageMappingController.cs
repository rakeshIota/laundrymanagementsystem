using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.CityLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CityLanguageMapping;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class CityLanguageMappingController : ApiController
    {
        private ICityLanguageMappingService _citylanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CityLanguageMappingController(ICityLanguageMappingService citylanguagemappingService, IExceptionService exceptionService)
        {
            _citylanguagemappingService = citylanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all CityLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("CityLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCityLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var citylanguagemappings = _citylanguagemappingService.SelectCityLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(citylanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CityLanguageMapping, (int)ErrorFunctionCode.CityLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CityLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get citylanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("citylanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCityLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var citylanguagemapping = _citylanguagemappingService.SelectCityLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (citylanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(citylanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CityLanguageMapping, (int)ErrorFunctionCode.CityLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CityLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save citylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("citylanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCityLanguageMapping(CityLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _citylanguagemappingService.CityLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CITYLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("CityLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CityLanguageMapping, (int)ErrorFunctionCode.CityLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CityLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update citylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("citylanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCityLanguageMapping(CityLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _citylanguagemappingService.CityLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("CityLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CityLanguageMapping, (int)ErrorFunctionCode.CityLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CityLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete citylanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("citylanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCityLanguageMapping(string id)
        {
        	try{
	            CityLanguageMappingViewModel model = new CityLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _citylanguagemappingService.CityLanguageMappingUpdate(model.ToModel());
	            return Ok("CityLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CityLanguageMapping, (int)ErrorFunctionCode.CityLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CityLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

