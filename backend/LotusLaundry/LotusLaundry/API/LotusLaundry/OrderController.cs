﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Order;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
using LotusLaundry.Common.Extensions;
using Customer.Service.PushNotification;
using LotusLaundry.Common.Success;
using LotusLaundry.Service.Users;
using LotusLaundry.Core.GeneratePDF;
using System.Globalization;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Domain.Payment;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Service.OrderLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Domain.Order;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Core.Mailer;
using LotusLaundry.Service.Configuration;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api")]
    public class OrderController : ApiController
    {
        private IOrderService _orderService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        private IPushNotificationService _pushNotificationService;
        private IUsersService _usersService;
        private IOrderLogService _orderlogService;
        private IPaymentLogService _paymentlogService;
        private IEmailConfigurationService _emailConfiguration;
        TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
        public OrderController(IPaymentLogService paymentlogService,IOrderLogService orderlogService,IUsersService usersService,IOrderService orderService, IExceptionService exceptionService, IPushNotificationService pushNotificationService, IEmailConfigurationService emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
            _paymentlogService = paymentlogService;
            _orderlogService = orderlogService;
            _usersService = usersService;
            _orderService = orderService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _pushNotificationService = pushNotificationService;
        }

        /// <summary>
        ///  Api use for get all Order
        /// </summary>
        /// <returns></returns>
        [Route("Orders")]
        [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPost]
        public IHttpActionResult GetAllOrders(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
                var orders = _orderService.SelectMyOrderDetails(param).Select(x => x.ToViewModel()); ;
             
            	return Ok(orders.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }


        [Route("ordersList")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.OPERATIONMANAGER)]
        [HttpPost]
        public IHttpActionResult GetAllOrderList(SearchParam param)
         {
            try
            {
                param = param ?? new SearchParam();
               
                var orders = _orderService.SelectOrderlist(param).Select(x => x.ToListViewModel());
                //if (param.name != null)
                //{
                //    param.name = txtInfo.ToTitleCase(param.name);
                //    var filterorder = orders.Where(x => x.customerName.Contains(param.name) || x.customerName.StartsWith(param.name) || x.customerName.EndsWith(param.name)).ToList();
                  

                //    //.Like(c.FullName, "%"+FirstName+"%,"+LastName)
                //    return Ok(filterorder.SuccessResponse());
                //}
                return Ok(orders.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }


        /// <summary>
        /// Api use for get order by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order/{id}")]
       [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpGet]
        public IHttpActionResult GetOrder(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToViewModel();
	            if (order.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(order.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Single_Select).ToString().ErrorResponse());
            }
        }


        //   

       





        /// <summary>
        /// Api use for update order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("admin/order")]
       [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrder(OrderViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderService.OrderUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Order updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }





        /// <summary>
        /// Api use for delete order by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order/{id}")]
       [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpDelete]
        public IHttpActionResult DeleteOrder(string id)
        {
        	try{
	            OrderViewModel model = new OrderViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderService.OrderUpdate(model.ToModel());
	            return Ok("Order Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Delete).ToString().ErrorResponse());
            }
        }

        /// <summary>
        ///  Api use for GET ORDER STATS
        /// </summary>
        /// <returns></returns>
        [Route("order/stats")]
       [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPost]
        public IHttpActionResult GetOrderStats(SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                var ordersStats = _orderService.SelectOrderStats(param).Select(x => x.ToViewModel()).FirstOrDefault();
                return Ok(ordersStats.SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString().ErrorResponse());
            }
        }



        /// <summary>
        /// Api use for update order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("assign/driver")]
        [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrderDriver(OrderbasicViewModel model)
        {
            try
            {
                string type="";
                string title="";
                string msg="";

                string typeDriver = "";
                string titleDriver = "";
                string msgDriver = "";

                SearchParam param = new SearchParam();
                param.Id = model.Id;
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault().ToViewModel();

                SearchParam paramuser = new SearchParam();
                paramuser.Id = model.DriverId;
                var user = _usersService.SelectUser(paramuser).FirstOrDefault().ToViewModel();

                model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.UpdatedBy = User.Identity.GetUserId<long>();
                if (model.Status == OrderConstants.NEW)
                {
                    model.Status = OrderConstants.ASSIGNED_DRIVER;                   
                    type = PushNotificationType.ASSIGNED_DRIVER_FOR_COLLECTION.ToString();
                    title = PushNotificationMSG.Title.DRIVER_ASSIGNED_FOR_COLLECTION.ToString();
                    msg = string.Format(PushNotificationMSG.message.DRIVER_ASSIGNED_FOR_COLLECTION.ToString(), user.FullName, user.LicencePlate, order.OrderId.ToString().PadLeft(6, '0'));

                    typeDriver = PushNotificationType.ASSIGNED_DRIVER_FOR_COLLECTION.ToString();
                    titleDriver = PushNotificationMSG.Title.ORDER_ASSIGNED_FOR_COLLECTION.ToString();
                    msgDriver = string.Format(PushNotificationMSG.message.ORDER_ASSIGNED_FOR_COLLECTION.ToString(),  order.OrderId.ToString().PadLeft(6, '0'), String.Format("{0:ddd, d MMM, yyyy}", order.PickupDate), model.pickupSlot);


                    //string.Format(configData.ConfigurationValue, model.FirstName + " " + model.LastName, callbackUrl);
                }
                if (model.Status == OrderConstants.CLEANING_COMPLETED)
                {
                    model.Status = OrderConstants.ASSIGNED_DRIVER_FOR_DELIVERY;

                  
                    type = PushNotificationType.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    title = PushNotificationMSG.Title.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    msg = string.Format(PushNotificationMSG.message.ASSIGNED_DRIVER_FOR_DELIVERY.ToString(), user.FullName, user.LicencePlate, order.OrderId.ToString().PadLeft(6, '0'));

                    typeDriver = PushNotificationType.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    titleDriver = PushNotificationMSG.Title.ORDER_ASSIGNED_FOR_DELIVERY.ToString();
                    msgDriver = string.Format(PushNotificationMSG.message.ORDER_ASSIGNED_FOR_DELIVERY.ToString(), order.OrderId.ToString().PadLeft(6, '0'), String.Format("{0:ddd, d MMM, yyyy}", order.PickupDate), order.pickupSlot);



                }
                if (model.Status==OrderConstants.ASSIGNED_DRIVER)
                {
                    model.Status = OrderConstants.ASSIGNED_DRIVER;

                    type = PushNotificationType.ASSIGNED_DRIVER_FOR_COLLECTION.ToString();
                    title = PushNotificationMSG.Title.DRIVER_ASSIGNED_FOR_COLLECTION.ToString();
                    msg = string.Format(PushNotificationMSG.message.DRIVER_ASSIGNED_FOR_COLLECTION.ToString(), user.FullName, user.LicencePlate, order.OrderId.ToString().PadLeft(6, '0'));

                    typeDriver = PushNotificationType.ASSIGNED_DRIVER_FOR_COLLECTION.ToString();
                    titleDriver = PushNotificationMSG.Title.ORDER_ASSIGNED_FOR_COLLECTION.ToString();
                    msgDriver = string.Format(PushNotificationMSG.message.ORDER_ASSIGNED_FOR_COLLECTION.ToString(), order.OrderId.ToString().PadLeft(6, '0'), String.Format("{0:ddd, d MMM, yyyy}", order.PickupDate),order.pickupSlot);

                }
                if (model.Status == OrderConstants.ASSIGNED_DRIVER_FOR_DELIVERY)
                {
                    model.Status = OrderConstants.ASSIGNED_DRIVER_FOR_DELIVERY;
                    type = PushNotificationType.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    title = PushNotificationMSG.Title.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    msg = string.Format(PushNotificationMSG.message.ASSIGNED_DRIVER_FOR_DELIVERY.ToString(), user.FullName, user.LicencePlate, order.OrderId.ToString().PadLeft(6, '0'));

                    typeDriver = PushNotificationType.ASSIGNED_DRIVER_FOR_DELIVERY.ToString();
                    titleDriver = PushNotificationMSG.Title.ORDER_ASSIGNED_FOR_DELIVERY.ToString();
                    msgDriver = string.Format(PushNotificationMSG.message.ORDER_ASSIGNED_FOR_DELIVERY.ToString(), order.OrderId.ToString().PadLeft(6, '0'), String.Format("{0:ddd, d MMM, yyyy}", order.DeliveryDate) , order.deliverySlot);

                }
                

                _orderService.OrderUpdate(model.ToBasicModel());
              
                var orderid = Convert.ToInt64(order.OrderId);
                var response = _orderService.OrderLogInsert(orderid, model.UpdatedBy, order.TenantId,"Diver Name "+user.FullName+""+  model.Status);
                //_pushNotificationService.SendNotificationByFCMToUser(orderid, order.UserId.GetValueOrDefault(), type, title, msg);
                PushNotificationModel pmodel = new PushNotificationModel();
                pmodel.CreatedBy = order.UserId.GetValueOrDefault();
                pmodel.IsDeleted = false;
                pmodel.IsActive = true;
                pmodel.UserId = order.UserId.GetValueOrDefault();
                pmodel.Type = type;
                pmodel.Title = title;
                pmodel.Message = msg;
                pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
              //  pmodel.TargetId = orderid;
                pmodel.UserRole = "CUSTOMER";
                _pushNotificationService.PushNotificationInsert(pmodel);

                if (typeDriver == PushNotificationType.ASSIGNED_DRIVER_FOR_DELIVERY.ToString() || typeDriver == PushNotificationType.ASSIGNED_DRIVER_FOR_COLLECTION.ToString())
                {
                    PushNotificationModel cModel = new PushNotificationModel();
                    cModel.CreatedBy = order.UserId.GetValueOrDefault();
                    cModel.IsDeleted = false;
                    cModel.IsActive = true;
                    cModel.UserId = Convert.ToInt64(CryptoEngine.Decrypt(model.DriverId, KeyConstant.Key));
                    cModel.Type = typeDriver;
                    cModel.Title = titleDriver;
                    cModel.Message = msgDriver;
                    cModel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                    //  pmodel.TargetId = orderid;
                    cModel.UserRole = "DRIVER";
                    _pushNotificationService.PushNotificationInsert(cModel);
                }


                return Ok(model.Id.SuccessResponse("Order updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        [Route("admin/order/orderId")]
       [CustomAuthorize("ROLE_ADMIN",UserRole.OPERATIONMANAGER)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrderStatus(OrderbasicViewModel model)
        {
            try
            {
                model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.UpdatedBy = User.Identity.GetUserId<long>();

                SearchParam param = new SearchParam();
                param.Id = model.Id;

                var order = _orderService.SelectOrderlist(param).FirstOrDefault();

                _orderService.OrderUpdate(model.ToBasicModel());

                
                
                string type = "";
                string title = "";
                string msg = "";
                var orderid = model.ToBasicModel().Id.ToString().PadLeft(6, '0');
                if (model.Status != null)
                {
                    //var response = _orderService.OrderLogInsert(order.Id, model.UpdatedBy, model.TenantId, order.Status);
                    
                    if (model.Status == OrderConstants.CLEANING_IN_PROGRESS)
                    {

                        type = PushNotificationType.CLEANING_IN_PROGRESS.ToString();
                        title = PushNotificationMSG.Title.CLEANING_IN_PROGRESS.ToString();
                        msg = string.Format(PushNotificationMSG.message.CLEANING_IN_PROGRESS.ToString(), orderid);
                    }
                    if (model.Status == OrderConstants.CLEANING_COMPLETED)
                    {

                        type = PushNotificationType.CLEANING_COMPLETED.ToString();
                        title = PushNotificationMSG.Title.CLEANING_COMPLETED.ToString();
                        msg = string.Format(PushNotificationMSG.message.CLEANING_COMPLETED.ToString(), orderid);
                    }
                    if (msg != "")
                    {
                        //_pushNotificationService.SendNotificationByFCMToUser(orderid, order.UserId.GetValueOrDefault(), type, title, msg);
                        PushNotificationModel pmodel = new PushNotificationModel();
                        pmodel.CreatedBy = model.UpdatedBy;
                        pmodel.IsDeleted = false;
                        pmodel.IsActive = true;
                        pmodel.UserId = order.UserId;
                        pmodel.Type = type;
                        pmodel.Title = title;
                        pmodel.Message = msg;
                        pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                        //pmodel.TargetId = orderid;
                        pmodel.UserRole = "CUSTOMER";
                        _pushNotificationService.PushNotificationInsert(pmodel);
                    }
                }
                if(model.PickupDate!=null)
                {
                    if(model.PickupDate!=order.PickupDate)
                    {
                        PushNotificationModel pmodel = new PushNotificationModel();
                        pmodel.CreatedBy = model.UpdatedBy;
                        pmodel.IsDeleted = false;
                        pmodel.IsActive = true;
                        pmodel.UserId = order.UserId;
                        pmodel.Type = PushNotificationType.ORDER_UPDATE.ToString(); 
                        pmodel.Title = PushNotificationMSG.Title.ORDER_UPDATE.ToString(); ;
                        pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_ITEM_PICKUP_UPDATE.ToString(), orderid);
                        pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                        //pmodel.TargetId = orderid;
                        pmodel.UserRole = "CUSTOMER";
                        _pushNotificationService.PushNotificationInsert(pmodel);


                        PushNotificationModel dmodel = new PushNotificationModel();
                        dmodel.CreatedBy = model.UpdatedBy;
                        dmodel.IsDeleted = false;
                        dmodel.IsActive = true;
                        dmodel.UserId = order.DriverId;
                        dmodel.Type = PushNotificationType.ORDER_UPDATE.ToString();
                        dmodel.Title = PushNotificationMSG.Title.ORDER_PICKUP_DATE_UPDATE.ToString(); ;
                        dmodel.Message = string.Format(PushNotificationMSG.message.ORDER_PICKUP_DATE_UPDATE.ToString(), orderid, String.Format("{0:ddd, MMM d, yyyy}", order.PickupDate) , order.pickupSlot);
                        dmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                        //pmodel.TargetId = orderid;
                        dmodel.UserRole = "DRIVER";
                        _pushNotificationService.PushNotificationInsert(dmodel);


                    }
                }
                if(model.DeliveryDate!=null)
                {

                    if (model.DeliveryDate != order.DeliveryDate)
                    {
                        PushNotificationModel pmodel = new PushNotificationModel();
                        pmodel.CreatedBy = model.UpdatedBy;
                        pmodel.IsDeleted = false;
                        pmodel.IsActive = true;
                        pmodel.UserId = order.UserId;
                        pmodel.Type = PushNotificationType.ORDER_UPDATE.ToString(); ;
                        pmodel.Title = PushNotificationMSG.Title.ORDER_UPDATE.ToString(); ;
                        pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_ITEM_DELIVERY_UPDATE.ToString(), orderid);
                        pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                        //pmodel.TargetId = orderid;
                        pmodel.UserRole = "CUSTOMER";
                        _pushNotificationService.PushNotificationInsert(pmodel);


                        PushNotificationModel dmodel = new PushNotificationModel();
                        dmodel.CreatedBy = model.UpdatedBy;
                        dmodel.IsDeleted = false;
                        dmodel.IsActive = true;
                        dmodel.UserId = order.DriverId;
                        dmodel.Type = PushNotificationType.ORDER_UPDATE.ToString();
                        dmodel.Title = PushNotificationMSG.Title.ORDER_DELIVERY_DATE_UPDATE.ToString(); ;
                        dmodel.Message = string.Format(PushNotificationMSG.message.ORDER_DELIVERY_DATE_UPDATE.ToString(), orderid, String.Format("{0:ddd, MMM d, yyyy}", order.DeliveryDate), order.deliverySlot);
                        dmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
                        //pmodel.TargetId = orderid;
                        dmodel.UserRole = "DRIVER";
                        _pushNotificationService.PushNotificationInsert(dmodel);
                    }
                }
                

                return Ok(model.Id.SuccessResponse("Order updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }



        [Route("admin/order/orderitem")]
        [CustomAuthorize(UserRole.Customer,UserRole.Admin,UserRole.SUPERADMIN,UserRole.OPERATIONMANAGER)]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult OrderItemUpdate(OrderItemViewModel model)
        {
            try
            {
               
                //set tenantid
                var response = _orderService.OrderItemUpdate(model.ServiceId, model.ProductId, ManageClaims.GetUserClaim().TenantId, User.Identity.GetUserId<long>(), model.Quantity);
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }

                // get updated cart 
                SearchParam param = new SearchParam();
                param.UserId = User.Identity.GetUserId<long>();
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                var Order = _orderService.SelectOrder(param).FirstOrDefault().ToViewModel();
                if (Order.Id == null)
                {
                    return Ok("No record found".ErrorResponse());
                }
                return Ok(Order.SuccessResponse("Order item updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.CartItem_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Update).ToString().ErrorResponse());
            }
        }


         [Route("admin/order/orderitems")]
        [CustomAuthorize(UserRole.Customer, UserRole.Admin, UserRole.SUPERADMIN, UserRole.OPERATIONMANAGER)]
        [HttpPut]
       // [ValidateModelState]
        public IHttpActionResult OrderUpdateWithItem(OrderViewModel model)
        {
            try
            {
               
                model.UpdatedBy = User.Identity.GetUserId<long>();
                //set tenantid
                var response = _orderService.OrderUpdateWithItem(model.ToModel());
               
                if (response.Status == false)
                {
                    return Ok(response.Message.ErrorResponse());
                }
               
                // get updated cart 
                SearchParam param = new SearchParam();
                param.Id = model.Id;
                param.TenantId = ManageClaims.GetUserClaim().TenantId;
                param.LanguageIdStr = CommonExtensions.GetValueFromHeader(Request, HeaderKey.Language);
                var _order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                var Order = _order.ToViewModel();
                var address = _order.DeliveryAddress;
                if (Order.Id == null)
                {
                    return Ok("No record found".ErrorResponse());
                }
                //var responseOrderLog = _orderService.OrderLogInsert(_order.Id, model.UpdatedBy, _order.TenantId, "order is updated by admin");
                //_pushNotificationService.SendNotificationByFCMToUser(orderid, order.UserId.GetValueOrDefault(), type, title, msg);
                
                string type = PushNotificationType.ORDER_UPDATE.ToString();
                string title = PushNotificationMSG.Title.ORDER_UPDATE.ToString();
                string msg = string.Format(PushNotificationMSG.message.ORDER_ITEM_UPDATE.ToString(),_order.Id.ToString().PadLeft(6, '0'));
                PushNotificationModel pmodel = new PushNotificationModel();
                pmodel.CreatedBy = model.UpdatedBy;
                pmodel.IsDeleted = false;
                pmodel.IsActive = true;
                pmodel.UserId = _order.UserId.GetValueOrDefault();
                pmodel.Type = type;
                pmodel.Title = title;
                pmodel.Message = msg;
                pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + _order.Id.ToString(), KeyConstant.Key);
               // pmodel.TargetId = _order.Id;
                pmodel.UserRole = "CUSTOMER";
                _pushNotificationService.PushNotificationInsert(pmodel);

                SendOrderConfirmEmail(_order);

                return Ok(Order.SuccessResponse("Order item updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.CartItem_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItem_Update).ToString().ErrorResponse());
            }
        }

        [Route("Orderpdf/{id}")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer, UserRole.OPERATIONMANAGER)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult OrderPDF(string id)
        {
            try
            {
                string timeIdStr = CommonExtensions.GetValueFromHeader(Request, "userTimeZone");
                int timegap = 0;
                if(timeIdStr!=null)
                {
                    timegap = Convert.ToInt32(timeIdStr);
                }


                var updatedBy = User.Identity.GetUserId<long>();
                SearchParam param = new SearchParam();
                param.Id = id;
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                var orderPdf = GenerateInvoice.GenerateOrderPdf(order, timegap);
                return Ok(orderPdf.SuccessResponse("Pdf downloaded successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        //api/notification/users?token=asdadas&message=asdasda&title=asdasdasdas
        [Route("notification/users")]
        [HttpGet]

        public IHttpActionResult testPushNotification(String token, string message, string title)
        {
            try
            {
                _pushNotificationService.TestSendNotificationByFCMToUser(token,"testTargetid", 100, "test", title, message);

                return Ok("ok".SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }

        [Route("order/{id}/refund")]
        [CustomAuthorize("ROLE_ADMIN", UserRole.Customer, UserRole.OPERATIONMANAGER)]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult RefundOrder(string id)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                var updatedBy = User.Identity.GetUserId<long>();
                SearchParam param = new SearchParam();
                param.Id = id;
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                PaymentResponseModel PaymentResult = new PaymentResponseModel();
                if (order.PaymentType == "card")
                {

                    PaymentResult = refundPayment();
                    if (PaymentResult.status == true)
                    {
                        result = PaymentLog(PaymentResult, order.UserId, order.TenantId, order.Id, order.TotalPrice);
                        result.Status = false;
                        result.Message = PaymentResult.Message;

                        return Ok(result.Message.ToString().SuccessResponse());
                    }
                   
                    if (result.Status == false)
                    {

                        result.Status = false;
                        result.Message = result.Message;
                        return Ok(result.Message.ErrorResponse());
                    }


                }



                var response = OrderLogInsert(order.Id, order.UserId, order.TenantId, OrderConstants.REFUND);




                if (result.Status)
                {

                    var data = result.Message;
                    return Ok(data.ToString().SuccessResponse());
                }

                return Ok(result.ErrorResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        public PaymentResponseModel refundPayment()
        {
            PaymentResponseModel model = new PaymentResponseModel();


            try
            {

                model.status = true;
                model.TransactionId = "13246";
                model.Message = "hard code complete";
                return model;
            }
            catch (Exception ex)
            {
                model.status = false;
                model.TransactionId = null;
                model.Message = "fail";
                return model;
            }

        }

        public ResponseModel OrderLogInsert(long? responseId, long? Userid, int? TenantId, string Note = null)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                OrderLogModel orderlog = new OrderLogModel();
                orderlog.OrderId = responseId;
                orderlog.CreatedBy = Userid;
                orderlog.TenantId = TenantId;
                orderlog.Note = Note;
                orderlog.IsActive = true;
                var response = _orderlogService.OrderLogInsert(orderlog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }

        public ResponseModel PaymentLog(PaymentResponseModel Transaction, long? Userid, int? TenantId, long? orderid, decimal? TotalAmount)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                PaymentLogModel paylog = new PaymentLogModel();

                paylog.CreatedBy = Userid;
                paylog.UserId = Userid;
                // paylog.CardNumber = UserCardModel.StripeCardId;
                paylog.Amount = TotalAmount;
                paylog.TenantId = TenantId;
                paylog.IsActive = true;
                paylog.OrderId = orderid;
                paylog.TransactionId = Transaction.Message.Contains("Error :") ? null : Transaction.TransactionId;
                paylog.Status = Transaction.status == true ? "Success" : "Fail";

                var response = _paymentlogService.PaymentLogInsert(paylog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }


        }

        public ResponseModel SendOrderConfirmEmail(OrderModel model)
        {
            var order = model.ToViewModel();
            UserAddressViewModel deliveryAddress = new UserAddressViewModel();
            deliveryAddress = order.DeliveryAddress;

            ResponseModel result = new ResponseModel();
            try
            {
                var userDetail = model.customerdetails;
                List<OrderItemModel> product = new List<OrderItemModel>();
                var orderid = model.Id.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.OrderEmail.ToString());

                string addressline1 = "", addressline2 = "", addressline3 = "", addressline4 = "", addressline5 = "";
                if (deliveryAddress.ResidenceType == "HOME")
                {
                    addressline1 = deliveryAddress.HouseNumber;
                    addressline2 = deliveryAddress.StreetNumber;


                    addressline3 = deliveryAddress.ProvinceName + "," + deliveryAddress.PostalCode;
                    addressline4 = "";
                    addressline5 = "";
                }
                if (deliveryAddress.ResidenceType == "BUILDING")
                {

                    addressline1 = deliveryAddress.BuildingName;
                    addressline2 = deliveryAddress.Floor + ", " + deliveryAddress.UnitNo + "," + deliveryAddress.StreetNumber;

                    addressline3 = deliveryAddress.ProvinceName + "," + deliveryAddress.PostalCode;
                    addressline4 = "";
                    addressline5 = "";
                    //addressline3 = addressline2 != null ? deliveryAddress.Floor + " " + deliveryAddress.UnitNo + " " + deliveryAddress.StreetNumber : deliveryAddress.StreetNumber;
                    //addressline4 = deliveryAddress.ProvinceName + " " + deliveryAddress.DistrictName;
                    //addressline5 = deliveryAddress.SubDistrictName + " " + deliveryAddress.PostalCode;
                }

                var name = txtInfo.ToTitleCase(userDetail.FirstName);
                string usesEmailBody = string.Format(configData.ConfigurationValue, name, orderid, convertListToTable(order), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1, addressline2, addressline3, addressline4);

                //string usesEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(order), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(order.DeliveryAddress));
                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.NewOrderRecevied.ToString());

                string AdminEmailBody = string.Format(configData.ConfigurationValue, name, orderid, convertListToTable(order), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1, addressline2, addressline3, addressline4);

                // MailSender.SendEmail(userDetail.Email, "Order Placed", usesEmailBody).Wait();
                string subject = string.Format(EmailConstants.ORDER_UPDATED, orderid);

                if (model.DeliveryType == "CREDIT_CARD" || model.DeliveryType == "QR_CODE")
                {
                    //foreach (var item in  model.OrderItems)                  
                    //{
                    //    OrderItemModel orderItem = new OrderItemModel();
                    //    orderItem.ProductName = item.ProductName;
                    //    orderItem.SubTotal = item.SubTotal;
                    //    orderItem.TotalPrice = item.TotalPrice;
                    //    orderItem.Quantity = item.Quantity;
                    //    product.Add(orderItem);
                    //}
                    ////todo setting admin mails 
                    //var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                  //  MailSender.SendEmailWithAttachment("admin", EmailConstants.ORDER_UPDATED, AdminEmailBody).Wait();
                }
                else
                {
                    foreach (var item in model.OrderItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.ProductName = item.ProductName;
                        orderItem.SubTotal = item.SubTotal;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);
                    }
                    var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                   // MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }

        public string convertListToTable(OrderViewModel model)
        {


            var deliveryType = model.DeliveryType;
            // string tbl = "<thead> <th style = 'width:25%'> Item Name </th><th style = 'width: 20%'> Price </th> <th style = 'width: 20%'> Quantity </th><th style = 'text-align: right; padding-right: 10px; width: 10%;'> Amount </th></thead> ";

            string tbl = "<thead style='color: #0000d4';> <th  scope='col' style = 'border:1px solid black'> Product </th><th scope='col' style = 'border:1px solid black'> Description </th> <th scope='col' style = 'border:1px solid black'> Price </th><th scope='col' style = 'border:1px solid black'> Quantity </th><th scope='col' style = 'border:1px solid black'> Total </th></thead><tbody style='color: #0000d4;'> ";

            foreach (var Item in model.OrderItems)
            {
                tbl += "<tr><td style = 'border:1px solid black'>" + Item.Product.Name + "</td><td style = 'border:1px solid black'>" + Item.Product.Name + "</td> <td style = 'border:1px solid black'>฿" + Item.SubTotal + "</td><td style = 'border:1px solid black'>" + Item.Quantity + "</td><td style = 'border:1px solid black'>฿" + Item.TotalPrice + "</td></tr>";
            }
            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";
            tbl += "</tbody>";
            return tbl;
        }



        public string convertListToString(UserAddressViewModel model)
        {

            //      string tbl =  model.BuildingName!=null?model.BuildingName:' ' + ' ' + model.StreetNumber != null ? model.StreetNumber : ' ' + ' ' + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode  + ' ';

            //string tbl1 = model.BuildingName != null ? model.BuildingName : ' '  + model.StreetNumber != null ? model.StreetNumber : ' '  + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode + ' ';
            string tbl = "";
            if (model.HouseNumber != null)
            {
                tbl = tbl + ' ' + model.HouseNumber;
            }
            if (model.BuildingName != null)
            {
                tbl = tbl + ' ' + model.BuildingName;
            }
            if (model.StreetNumber != null)
            {
                tbl = tbl + ',' + model.StreetNumber;
            }
            if (model.Floor != null)
            {
                tbl = tbl + ',' + model.Floor;
            }
            if (model.UnitNo != null)
            {
                tbl = tbl + ',' + model.UnitNo;
            }
            if (model.SubDistrictName != null)
            {
                tbl = tbl + ',' + model.SubDistrictName;
            }
            if (model.DistrictName != null)
            {
                tbl = tbl + ',' + model.DistrictName;
            }
            if (model.ProvinceName != null)
            {
                tbl = tbl + ',' + model.ProvinceName;
            }
            if (model.PostalCode != null)
            {
                tbl = tbl + ',' + model.PostalCode;
            }
            if (model.PhoneNo != null)
            {
                tbl = tbl + ',' + model.PhoneNo;
            }

            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";

            return tbl;
        } 




    }
}

