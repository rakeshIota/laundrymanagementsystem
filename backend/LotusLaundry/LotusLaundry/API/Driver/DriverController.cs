﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Order;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Common.Success;
using Customer.Service.PushNotification;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Domain.Order;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Core.GeneratePDF;
using LotusLaundry.Core.Mailer;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Service.Users;
using System.Globalization;

namespace LotusLaundry.API.LotusLaundry
{
    [RoutePrefix("api/application")]
    public class DriverController : ApiController
    {
        private IOrderService _orderService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        private IPushNotificationService _pushNotificationService;
        private IEmailConfigurationService _emailConfiguration;
        private IUsersService _usersService;
        TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
        public DriverController(IUsersService usersService, IPushNotificationService pushNotificationService,IOrderService orderService, IExceptionService exceptionService, IEmailConfigurationService emailConfiguration)
        {
            _usersService = usersService;
            _emailConfiguration = emailConfiguration;
            _pushNotificationService = pushNotificationService;
            _orderService = orderService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        [Route("driver/my/orders")]
        [CustomAuthorize(UserRole.Driver)]
        [HttpPost]
        public IHttpActionResult GetMyOrders(SearchParam param)
        {
            try
            {
                var id = User.Identity.GetUserId<long>();
               
                param = param ?? new SearchParam();
                
                param.driverId = id;
                var orders = _orderService.SelectDriverActiveOrder(param).Select(x => x.ToViewModel()); ;
                return Ok(orders.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }

        [Route("driver/my/rides")]
        [CustomAuthorize(UserRole.Driver)]
        [HttpPost]
        public IHttpActionResult GetMyCompleteRides(SearchParam param)
        {
            try
            {
                var id = User.Identity.GetUserId<long>();

                param = param ?? new SearchParam();

                param.driverId = id;
                var orders = _orderService.SelectDriverRides(param).Select(x => x.ToViewModel()); ;
                return Ok(orders.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }


        [Route("order/{id}/complete")]
        [CustomAuthorize(UserRole.Driver)]
        [HttpPut]
       
        public IHttpActionResult UpdateOrderStatus(string Id)
        {
            try
            {
                OrderbasicViewModel model = new OrderbasicViewModel();
                model.Id = Id;

                model.TenantId = ManageClaims.GetUserClaim().TenantId;
                model.UpdatedBy = User.Identity.GetUserId<long>();

                //test 
                string type = "";
                string title = "";
                string msg = "";
                SearchParam param = new SearchParam();
                param.RecordId = model.ToBasicModel().Id; 
                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                var orderdeatil = order.ToViewModel();
                var driver = order.DriverDetails.FirstOrDefault().ToViewModel();

                if (order.Status == OrderConstants.ASSIGNED_DRIVER)
                {
                    model.Status = OrderConstants.AWAITING_COLLECTION;
                    type = PushNotificationType.AWAITING_COLLECTION.ToString();
                    title = PushNotificationMSG.Title.AWAITING_COLLECTION.ToString();
                    msg = string.Format(PushNotificationMSG.message.AWAITING_COLLECTION.ToString(),driver.FullName,driver.LicencePlate, order.Id.ToString().PadLeft(6, '0'));

                   
                }
                if (order.Status == OrderConstants.AWAITING_COLLECTION)
                {
                    model.Status = OrderConstants.COLLECTED;
                    model.collectedDate = DateTimeOffset.UtcNow;
                    type = PushNotificationType.COLLECTED.ToString();
                    title = PushNotificationMSG.Title.COLLECTED.ToString();
                    msg = string.Format(PushNotificationMSG.message.COLLECTED.ToString(),order.Id.ToString().PadLeft(6, '0'));
                }


                if (order.Status == OrderConstants.ASSIGNED_DRIVER_FOR_DELIVERY)
                {
                    model.Status = OrderConstants.ON_THE_WAY;

                    type = PushNotificationType.ON_THE_WAY.ToString();
                    title = PushNotificationMSG.Title.ON_THE_WAY.ToString();
                    msg = string.Format(PushNotificationMSG.message.ON_THE_WAY.ToString(), driver.FullName, driver.LicencePlate,order.Id.ToString().PadLeft(6, '0'));
                    //SendOrderDeliverEmail(order);
                }
                if (order.Status == OrderConstants.ON_THE_WAY)
                {
                    model.Status = OrderConstants.DELIVERED;
                    
                    model.deliveredDate = DateTimeOffset.UtcNow;
                    type = PushNotificationType.DELIVERED.ToString();
                    title = PushNotificationMSG.Title.DELIVERED.ToString();
                    msg = string.Format(PushNotificationMSG.message.DELIVERED.ToString(), order.Id.ToString().PadLeft(6, '0'));
                    SendOrderDeliverEmail(orderdeatil,order.UserId, model.TenantId,order.Id.ToString().PadLeft(6, '0'));
                    
                }



                _orderService.OrderComplete(model.ToBasicModel());
                var response = _orderService.OrderLogInsert(order.Id, model.UpdatedBy, order.TenantId, "Diver Name " + driver.FullName + " change status to " + model.Status);
                //_pushNotificationService.SendNotificationByFCMToUser(orderid, order.UserId.GetValueOrDefault(), type, title, msg);
                PushNotificationModel pmodel = new PushNotificationModel();
                pmodel.CreatedBy = order.UserId.GetValueOrDefault();
                pmodel.IsDeleted = false;
                pmodel.IsActive = true;
                pmodel.UserId = order.UserId.GetValueOrDefault();
                pmodel.Type = type;
                pmodel.Title = title;
                pmodel.Message = msg;
                pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + order.Id.ToString(), KeyConstant.Key);
              //  pmodel.TargetId = order.Id;
                pmodel.UserRole = "CUSTOMER";
                _pushNotificationService.PushNotificationInsert(pmodel);


                return Ok(model.Id.SuccessResponse("Order updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        public ResponseModel SendOrderConfirmEmail(OrderModel model )
        {
            var order = model.ToViewModel();


            ResponseModel result = new ResponseModel();
            try
            {
                var userDetail = model.customerdetails;
                List<OrderItemModel> product = new List<OrderItemModel>();
                var orderid = model.Id.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.OrderEmail.ToString());
                string usesEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(order), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(order.DeliveryAddress));
                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.NewOrderRecevied.ToString());

                string AdminEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(order), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(order.DeliveryAddress));

                // MailSender.SendEmail(userDetail.Email, "Order Placed", usesEmailBody).Wait();
                string subject = string.Format(EmailConstants.ORDER_PlACED, orderid);

                if (model.DeliveryType == "CREDIT_CARD" || model.DeliveryType == "QR_CODE")
                {
                    //foreach (var item in  model.OrderItems)                  
                    //{
                    //    OrderItemModel orderItem = new OrderItemModel();
                    //    orderItem.ProductName = item.ProductName;
                    //    orderItem.SubTotal = item.SubTotal;
                    //    orderItem.TotalPrice = item.TotalPrice;
                    //    orderItem.Quantity = item.Quantity;
                    //    product.Add(orderItem);
                    //}
                    ////todo setting admin mails 
                    //var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                   // MailSender.SendEmailWithAttachment("adminmail", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
                else
                {
                    foreach (var item in model.OrderItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.ProductName = item.ProductName;
                        orderItem.SubTotal = item.SubTotal;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);
                    }
                    var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                   // MailSender.SendEmailWithAttachment("adminmailid", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }
        public ResponseModel SendOrderDeliverEmail1(OrderViewModel model)
        {
            var order = model;


            ResponseModel result = new ResponseModel();
            try
            {
                var userDetail = model.customerDetails;
                List<OrderItemModel> product = new List<OrderItemModel>();
                var orderid = model.Id.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.orderDelivered.ToString());
                string usesEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(order), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(order.DeliveryAddress));
                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.NewOrderRecevied.ToString());

                string AdminEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(order), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(order.DeliveryAddress));

                // MailSender.SendEmail(userDetail.Email, "Order Placed", usesEmailBody).Wait();
                string subject = string.Format(EmailConstants.ORDER_PlACED, orderid);

                if (model.DeliveryType == "CREDIT_CARD" || model.DeliveryType == "QR_CODE")
                {
                    //foreach (var item in  model.OrderItems)                  
                    //{
                    //    OrderItemModel orderItem = new OrderItemModel();
                    //    orderItem.ProductName = item.ProductName;
                    //    orderItem.SubTotal = item.SubTotal;
                    //    orderItem.TotalPrice = item.TotalPrice;
                    //    orderItem.Quantity = item.Quantity;
                    //    product.Add(orderItem);
                    //}
                    ////todo setting admin mails 
                    //var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                    //MailSender.SendEmailWithAttachment("adminmailid", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
                else
                {
                    foreach (var item in model.OrderItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                       // orderItem.ProductName = item.Product;
                        orderItem.SubTotal = item.SubTotal;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);
                    }
                   // var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, product, model.TotalPrice, model);
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody).Wait();
                   // MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }


        public ResponseModel SendOrderDeliverEmail(OrderViewModel model, long? userid, int? tenant, string orderid)
        {

            UserAddressViewModel deliveryAddress = model.DeliveryAddress;
          

            ResponseModel result = new ResponseModel();
            try
            {
                var userDetail = _usersService.SelectProfile(userid.GetValueOrDefault()).FirstOrDefault();

                List<OrderItemModel> product = new List<OrderItemModel>();


                orderid = orderid.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.orderDelivered.ToString());

                string addressline1 = "", addressline2 = "", addressline3 = "", addressline4 = "", addressline5 = "";
                if (deliveryAddress.ResidenceType == "HOME")
                {
                    addressline1 = deliveryAddress.HouseNumber;
                    addressline2 = deliveryAddress.StreetNumber;


                    addressline3 = deliveryAddress.ProvinceName + "," + deliveryAddress.PostalCode;
                    addressline4 = "";
                    addressline5 = "";
                }
                if (deliveryAddress.ResidenceType == "BUILDING")
                {

                    addressline1 = deliveryAddress.BuildingName;
                    addressline2 = deliveryAddress.Floor + ", " + deliveryAddress.UnitNo + "," + deliveryAddress.StreetNumber;

                    addressline3 = deliveryAddress.ProvinceName + "," + deliveryAddress.PostalCode;
                    addressline4 = "";
                    addressline5 = "";
                    //addressline3 = addressline2 != null ? deliveryAddress.Floor + " " + deliveryAddress.UnitNo + " " + deliveryAddress.StreetNumber : deliveryAddress.StreetNumber;
                    //addressline4 = deliveryAddress.ProvinceName + " " + deliveryAddress.DistrictName;
                    //addressline5 = deliveryAddress.SubDistrictName + " " + deliveryAddress.PostalCode;
                }
                //addressline1 = deliveryAddress.BuildingName != null ? deliveryAddress.BuildingName : deliveryAddress.HouseNumber;
                //addressline2 = deliveryAddress.Floor != null ? deliveryAddress.Floor : null;


                //addressline3 = addressline2 != null ? deliveryAddress.Floor + " " + deliveryAddress.UnitNo + " " + deliveryAddress.StreetNumber : deliveryAddress.StreetNumber;
                //addressline4 = deliveryAddress.ProvinceName + " " + deliveryAddress.DistrictName;
                //addressline5 = deliveryAddress.SubDistrictName + " " + deliveryAddress.PostalCode;

                string pickup = String.Format("{0:ddd, MMM d, yyyy}", model.PickupDate) + " " + model.pickupSlot;
                string delivery = String.Format("{0:ddd, MMM d, yyyy}", model.DeliveryDate) + " " + model.deliverySlot;


                var name = txtInfo.ToTitleCase(userDetail.FirstName);
                //string usesEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(model, carts), model.SubTotal,model.DeliveryType,model.TotalPrice,model.PaymentType, convertListToString(deliveryAddress));
                string usesEmailBody = string.Format(configData.ConfigurationValue, orderid, orderid, convertListToTable(model), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1, addressline2, addressline3, addressline4);


                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.orderDelivered.ToString());

                // string AdminEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(model, carts), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(deliveryAddress));
                string AdminEmailBody = string.Format(configData.ConfigurationValue, orderid, orderid, convertListToTable(model), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1, addressline2, addressline3, addressline4);

                // MailSender.SendEmail(userDetail.Email, "Order Placed", usesEmailBody).Wait();
                string subject = string.Format(EmailConstants.ORDER_DELIVERED, orderid);

                if (model.PaymentType == "CARD" || model.PaymentType == "QRCODE")
                {
                    foreach (var item in model.OrderItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.ProductName = item.Product.Name;
                        orderItem.SubTotal = item.SubTotal;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);

                    }
                    
                    //todo setting admin mails 
                    var invoice = GenerateInvoice.GenerateOrderInvoice(userDetail, product, model.TotalPrice, model.ToModel());
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody, invoice).Wait();
                  //  MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody, invoice).Wait();
                }

                else
                {
                    foreach (var item in model.OrderItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.ProductName = item.Product.Name;
                        orderItem.SubTotal = item.SubTotal;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);

                    }
                    var invoice = GenerateInvoice.GenerateOrderInvoice(userDetail, product, model.TotalPrice, model.ToModel());
              //      MailSender.SendEmail(userDetail.Email, subject, usesEmailBody).Wait();
                //    MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();

                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody, invoice).Wait();
                   // MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody, invoice).Wait();

                }





                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }


        public string convertListToTable(OrderViewModel model)
        {
            
          
            var deliveryType = model.DeliveryType;
            // string tbl = "<thead> <th style = 'width:25%'> Item Name </th><th style = 'width: 20%'> Price </th> <th style = 'width: 20%'> Quantity </th><th style = 'text-align: right; padding-right: 10px; width: 10%;'> Amount </th></thead> ";

            string tbl = "<thead style='color: #0000d4';> <th  scope='col' style = 'border:1px solid black'> Product </th><th scope='col' style = 'border:1px solid black'> Description </th> <th scope='col' style = 'border:1px solid black'> Price </th><th scope='col' style = 'border:1px solid black'> Quantity </th><th scope='col' style = 'border:1px solid black'> Total </th></thead><tbody style='color: #0000d4;'> ";

            foreach (var Item in model.OrderItems)
            {
                tbl += "<tr><td style = 'border:1px solid black'>" + Item.Product.Name + "</td><td style = 'border:1px solid black'>" + Item.Product.Name + "</td> <td style = 'border:1px solid black'>$" + Item.SubTotal + "</td><td style = 'border:1px solid black'>" + Item.Quantity + "</td><td style = 'border:1px solid black'>$" + Item.TotalPrice + "</td></tr>";
            }
            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";
            tbl += "</tbody>";
            return tbl;
        }



        public string convertListToString(UserAddressViewModel model)
        {

            //      string tbl =  model.BuildingName!=null?model.BuildingName:' ' + ' ' + model.StreetNumber != null ? model.StreetNumber : ' ' + ' ' + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode  + ' ';

            //string tbl1 = model.BuildingName != null ? model.BuildingName : ' '  + model.StreetNumber != null ? model.StreetNumber : ' '  + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode + ' ';
            string tbl = "";
            if (model.HouseNumber != null)
            {
                tbl = tbl + ' ' + model.HouseNumber;
            }
            if (model.BuildingName != null)
            {
                tbl = tbl + ' ' + model.BuildingName;
            }
            if (model.StreetNumber != null)
            {
                tbl = tbl + ',' + model.StreetNumber;
            }
            if (model.Floor != null)
            {
                tbl = tbl + ',' + model.Floor;
            }
            if (model.UnitNo != null)
            {
                tbl = tbl + ',' + model.UnitNo;
            }
            if (model.SubDistrictName != null)
            {
                tbl = tbl + ',' + model.SubDistrictName;
            }
            if (model.DistrictName != null)
            {
                tbl = tbl + ',' + model.DistrictName;
            }
            if (model.ProvinceName != null)
            {
                tbl = tbl + ',' + model.ProvinceName;
            }
            if (model.PostalCode != null)
            {
                tbl = tbl + ',' + model.PostalCode;
            }
            if (model.PhoneNo != null)
            {
                tbl = tbl + ',' + model.PhoneNo;
            }

            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";

            return tbl;
        }


    }
}

