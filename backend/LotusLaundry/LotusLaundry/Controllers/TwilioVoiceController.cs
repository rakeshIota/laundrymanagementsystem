﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain.UserTwilioCalling;
using LotusLaundry.Providers;
using LotusLaundry.Service.Exception;
using LotusLaundry.Service.Notification;
using LotusLaundry.Service.UserTwilioCalling;
using System;
using System.Configuration;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.TwiML;
using Twilio.TwiML.Voice;
using static Twilio.TwiML.Voice.Dial;

namespace LotusLaundry.Controllers
{
    public class TwilioVoiceController : Controller
    {
        private string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
        private string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
        private string chatServiceSid = ConfigurationManager.AppSettings["TwilioChatServiceSid"];
        private string apiKey = ConfigurationManager.AppSettings["TwilioApiKey"];
        private string apiSecret = ConfigurationManager.AppSettings["TwilioApiSecret"];
        private string applicationSid = ConfigurationManager.AppSettings["TwilioVoiceAppSid"];


        IUserCallingService _userCallingService;
        INotificationService _notificationService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;

        public TwilioVoiceController(IUserCallingService userCallingService, INotificationService notificationService, IExceptionService exceptionService)
        {
            _userCallingService = userCallingService;
            _notificationService = notificationService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        //public TwilioVoiceController()
        //{
        //    var _userService = EngineContext.Current.Resolve<IUserService>();
        //}

        [HttpPost]
        public ActionResult ClientToClient(VoiceRequest request)
        {
            var response = new VoiceResponse();

            if (!string.IsNullOrEmpty(request.To))
            {

                var model = new UserTwilioCallingModel();
                //model.ToUserId = Convert.ToInt64(request.To.Split('_')[1]);
                //model.FromUserId = Convert.ToInt64(request.From.Split('_')[1]);

                var ToUserId =(request.To.Split('_')[1]);
                var FromUserId = (request.From.Split('_')[1]);

                model.ToUserId = Convert.ToInt64(CryptoEngine.Decrypt(ToUserId.ToString(), KeyConstant.Key));
                model.FromUserId = Convert.ToInt64(CryptoEngine.Decrypt(FromUserId.ToString(), KeyConstant.Key));



                model.IsActive = true;
                
                var IsActive = _userCallingService.CheckUserCallingStatus(model);

                var IsDeviceActive = _userCallingService.CheckUserCallingDeviceStatus(model);


                if (Convert.ToBoolean(IsActive))
                {
                    response.Say("The user is currently busy in another call");
                }
                else if (Convert.ToBoolean(IsDeviceActive))
                {
                    response.Say("The user is currently logged out from the app");
                }
                else
                {
                    _userCallingService.UserCallingStatusUpdate(model);
                    var callerId = request.From;
                    // wrap the phone number or client name in the appropriate TwiML verb
                    // by checking if the number given has only digits and format symbols


                    callerId= callerId.Replace("-", "_");

                    var dial = new Dial(callerId: callerId);
                    var client = new Client(request.To);

                    //_notificationService.NotificationInsertAsync(new Domain.Notification.NotificationModel
                    //{
                    //    Title = "Calling",
                    //    Message = "User is Calling",
                    //    Type = NotificationType.CALLING,
                    //    TargetId = model.ToUserId,
                    //    TargetType = NotificationType.CALLING,
                    //    UserId = model.ToUserId,
                    //    CreatedBy = model.ToUserId,
                    //    UpdatedBy = model.ToUserId

                    //});

                    try
                    {
                        dial.Client(client);
                        dial.Timeout = 120;
                        dial.RingTone = RingToneEnum.In;
                        VoiceResponse respsss = response.Dial(dial);
                    }
                    catch (Exception ex)
                    {
                        var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Calling, (int)ErrorFunctionCode.User_Calling);
                        _exceptionService.InsertLog(exception);
                        //return Ok(((int)ErrorFunctionCode.User_Calling).ToString().ErrorResponse());
                    }
                }
            }
            else
            {
                response.Say("Thanks for calling at Lotus Laundry");
            }

            return Content(response.ToString(), "text/xml");
        }
    }
}