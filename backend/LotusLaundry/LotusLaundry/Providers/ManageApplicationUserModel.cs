﻿using LotusLaundry.Domain.Exception;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Domain.Users;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Framework.ViewModels.Upload;
using System.Globalization;

namespace LotusLaundry.Providers
{
    public class ManageApplicationUserModel
    {
        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        public ApplicationUser UserViewModelToApplicationUserModel(UsersViewModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.UserName!=null?model.UserName:model.Email,
                Email = model.Email,
                PhoneNumber = model.MobileNo!=null?model.MobileNo:model.PhoneNumber,
                PhoneNumberConfirmed = false,
                EmailConfirmed = false,
                IsActive = true,
                TwoFactorEnabled = model.RoleName == "" ? true : false,
                FirstName = model.FirstName,
                LastName = model.LastName,
                TenantId = ManageClaims.GetUserClaim().TenantId,

                EmployeeId = model.employeeid,
                Position = model.Position,

                DOB=model.DateOfbirth,
                LicenseNo=model.LicenseNo!=null?model.LicenseNo : model.LicenceId,
                
                NationalId=model.NationalId,
                LicenceId=model.LicenceId,
                BikeInformation=model.BikeInformation,
                LicencePlate=model.LicencePlate,
                CreatedOn=model.createdOn.GetValueOrDefault(),
               // Status= model.IsActive == true && model.IsDeleted==false ? "Active" : model.IsActive == false && model.IsDeleted == true ? "Deleted" : "Inactive",
                Status = model.IsDeleted == true ? "Deleted" : model.IsActive == false ? "Inactive" : "Active",
        };
            return user;
        }


        public ApplicationUser UserViewModelToCustomerApplicationModel(UsersViewModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.MobileNo,
                PhoneNumberConfirmed = false,
                EmailConfirmed = false,
                IsActive = false,
                TwoFactorEnabled = false,
                FirstName = model.FirstName,
                LastName = model.LastName,
                TenantId = 1,
                NickName = model.NickName,
                LanguageId = model.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(model.LanguageId.ToString(), KeyConstant.Key)) : 0,
                CountryCode = model.CountryCode,
                Gender = model.Gender,
                DOB = model.DateOfbirth,
                CreatedOn = DateTimeOffset.UtcNow,
                callStatus = "IDLE"

               
            };
            return user;
        }


        public UsersViewModel modelApplicationUserToCustomerUserViewModel(ApplicationUser user)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
            var userData = new UsersViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + user.Id.ToString(), KeyConstant.Key),
                FirstName = txtInfo.ToTitleCase(user.FirstName),
                LastName = txtInfo.ToTitleCase(user.LastName),
                UserName = user.Email,
                FullName = txtInfo.ToTitleCase(user.FirstName) + " " + txtInfo.ToTitleCase(user.LastName),
                Roles = UserManager.GetRoles(user.Id).ToList(),
                ProfileImageUrl = user.ProfilePic,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + user.LanguageId.ToString(), KeyConstant.Key),
                MobileNo = user.PhoneNumber,
                NickName = user.NickName,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                PhoneNumberConfirmed =  user.PhoneNumberConfirmed
               
            };
            //if (userData.ProfileImageUrl != null)
            //{
            //    FileGroupItemsViewModel ProfileFile = new FileGroupItemsViewModel();
            //    ProfileFile.Path = userData.ProfileImageUrl;
            //    userData.File = ProfileFile;
            //}
            return userData;
        }

        public UsersViewModel modelApplicationUserToCustomerDetail(ApplicationUser user)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
            var userData = new UsersViewModel
            {
                FirstName = txtInfo.ToTitleCase(user.FirstName),
                LastName = txtInfo.ToTitleCase(user.LastName),
                
                UserName = user.Email,
                FullName = txtInfo.ToTitleCase(user.FirstName) + " " + txtInfo.ToTitleCase(user.LastName),
                ProfileImageUrl = user.ProfilePic,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + user.LanguageId.ToString(), KeyConstant.Key),
                MobileNo = user.PhoneNumber,
                NickName = user.NickName,
                Email = user.Email,
                PhoneNumber =  user.PhoneNumber,
                PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                EmailConfirmed =  user.EmailConfirmed,
                DateOfbirth=user.DOB,
                Gender=user.Gender,
                

            };
            return userData;
        }

        public UsersViewModel ApplicationUserModelToUserViewModel(ApplicationUser user)
        {
            var userData = new UsersViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + user.Id.ToString(), KeyConstant.Key),
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.Email,
                FullName = user.FirstName + " " + user.LastName,
                Roles = UserManager.GetRoles(user.Id).ToList(),
                ProfileImageUrl = user.ProfilePic,
                OTP =  user.OTP,
                OTPValidTill = user.OTPValidTill,
                LicenseNo = user.LicenseNo,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + user.LanguageId.ToString(), KeyConstant.Key) 
               
            };
            return userData;
        }
        public ExceptionModel ExceptionToExceptionModel(Exception exception, int entityCode, int methodCode)
        {
            var _exception = new ExceptionModel
            {
                Message = exception.Message,
                Source = exception.Source,
                StackTrace = exception.StackTrace,
                EntityCode = entityCode,
                ResponseCode = methodCode,
                Uri = exception.Message,
                Method =  exception.Message
            };
            return _exception;
        }




    }
}
