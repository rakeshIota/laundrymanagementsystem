﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;

namespace LotusLaundry.Providers
{
    public class CustomAuthorize : AuthorizationFilterAttribute
    {
        private readonly string[] allowedroles;
        public CustomAuthorize(params string[] roles)
        {
            this.allowedroles = roles;
        }
        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            try
            {
                ClaimsIdentity claimsIdentity = System.Web.HttpContext.Current.User.Identity as ClaimsIdentity;
                var userIdClaim = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                var user = UserManager.FindById(Convert.ToInt64(userIdClaim.Value));
                if (user == null  || user.IsActive == false){
                    filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                if (this.allowedroles.Count() > 0) {
                    var roleName = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role);
                    if (!this.allowedroles.Contains(roleName.Value)) {
                        filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                    }
                }
            }
            catch (Exception ex)
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                return;
            }

            base.OnAuthorization(filterContext);
        }
    }

}
