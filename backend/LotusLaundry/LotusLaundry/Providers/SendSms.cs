﻿using LotusLaundry.Common.Success;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace LotusLaundry.Providers
{
    public class SendSms
    {
        public void SendMessage(string mobile = null, string content = null,string countryCode=null)
        {
            

             string accountSid = ConfigurationManager.AppSettings["TWILIOACCOUNTSID"];
             string authToken = ConfigurationManager.AppSettings["TWILIOAUTHTOKEN"];
             string phoneNumber = ConfigurationManager.AppSettings["FROM_TWILIO_PHONE_NUMBER"];
            

            content = string.Format(SuccessMessage.AccountSuccess.SEND_OTP_SMSCONTENT, content);

            

            TwilioClient.Init(accountSid, authToken);

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2
            var message = MessageResource.Create(
                body: content,
                from: new Twilio.Types.PhoneNumber(phoneNumber),
                to: new Twilio.Types.PhoneNumber(countryCode + mobile)
            );

            Console.WriteLine(message.Sid);
        }

    }
}