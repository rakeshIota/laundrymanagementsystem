﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using LotusLaundry.App_Start;
using Autofac.Integration.WebApi;
using LotusLaundry.Core.Infrastructure;
using System.Net.Http.Formatting;

namespace LotusLaundry
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var engine = EngineContext.Initialize(false);
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(engine.ContainerManager.Container);
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            GlobalConfiguration.Configuration.Formatters.Clear();

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter()
			{
			//supress all null value from json
			SerializerSettings = { NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore }
			});
            
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (this.Context.Request.Path.Contains("signalr/"))
            {
                this.Context.Response.AddHeader("Access-Control-Allow-Headers", "accept,origin,authorization,content-type");
            }
            //SqlDependency.Stop(connString);
            if (ReferenceEquals(null, HttpContext.Current.Request.Headers["Authorization"]))
            {
                var token = HttpContext.Current.Request.Params["a_t"];
                if (!string.IsNullOrEmpty(token))
                {
                    HttpContext.Current.Request.Headers.Add("Authorization", "Bearer " + token);
                }
            }
            //Response.Redirect(""),j
        }

        // this is for fixed the SSL error 
        void Session_Start(object sender, EventArgs e)
        {
           // System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

           
        }
    }
}
