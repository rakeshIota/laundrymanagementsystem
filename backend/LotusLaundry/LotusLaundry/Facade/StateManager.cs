﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Service.Country;
using LotusLaundry.Domain;
using LotusLaundry.Service.City;
using LotusLaundry.Framework.ViewModels.LotusLaundry.City;

namespace LotusLaundry.Facade
{
    public class StateManager
    {
        private ICityService _cityService;
       

        public StateManager(ICityService cityService)
        {
            // _countryService = countryService;
            _cityService = cityService;

        }

        public bool AllowTodelete(string id)
        {
            SearchParam param = new SearchParam();
            param.RelationTable = "State";
            param.RelationId = id;
            var city = _cityService.SelectCity(param).FirstOrDefault().ToViewModel();
            if (city.Id != null)
            {

                return false;

            }
            return true;
        }



    }
}