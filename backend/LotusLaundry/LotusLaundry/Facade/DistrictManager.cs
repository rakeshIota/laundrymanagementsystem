﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Service.Country;
using LotusLaundry.Domain;
using LotusLaundry.Service.SubDistrict;
using LotusLaundry.Framework.ViewModels.Customer.SubDistrict;

namespace LotusLaundry.Facade
{
    public class DistrictManager
    {
        //private ICityService _cityService;
        private ISubDistrictService _subdistrictService;

        public DistrictManager(ISubDistrictService subdistrictService)
        {
            // _countryService = countryService;
            _subdistrictService = subdistrictService;

        }

        public bool AllowTodelete(string id)
        {
            


            SearchParam param = new SearchParam();
            param.RelationTable = "DISTRICT";
            param.RelationId = id;
            var subdistrict = _subdistrictService.SelectSubDistrict(param).FirstOrDefault().ToViewModel();


            if (subdistrict.Id != null)
            {

                return false;

            }
            return true;
        }



    }
}