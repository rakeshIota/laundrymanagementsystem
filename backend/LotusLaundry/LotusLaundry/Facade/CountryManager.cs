﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Service.Country;
using LotusLaundry.Domain;

namespace LotusLaundry.Facade
{
    public class CountryManager
    {
        private IStateService _stateService;
        private ICountryService _countryService;

        public CountryManager( IStateService stateService)
        {
           // _countryService = countryService;
            _stateService = stateService;

        }

        public bool AllowTodelete(string id)
        {
            SearchParam param = new SearchParam();
            param.RelationId = id;
            param.RelationTable = "Country";
            var state = _stateService.SelectState(param).FirstOrDefault().ToViewModel();
            if (state.Id != null)
            {
                return false;

            }
            return true;
        }



    }
}