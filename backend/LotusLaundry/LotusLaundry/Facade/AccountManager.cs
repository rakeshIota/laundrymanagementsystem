﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Service.Country;
using LotusLaundry.Domain;
using LotusLaundry.Service.City;
using LotusLaundry.Framework.ViewModels.LotusLaundry.City;
using LotusLaundry.Framework.ViewModels.Customer;
using LotusLaundry.Models;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Providers;
using System.Configuration;
using LotusLaundry.Common.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using LotusLaundry.Framework.ViewModels.Users;

namespace LotusLaundry.Facade
{
    public class AccountManager
    {
        private ICityService _cityService;
        public readonly static string is_MobileSMS_ENABLE = ConfigurationManager.AppSettings["ISENABLEMOBILESMS"];
        private ManageApplicationUserModel _manageApplicationUserModel;
        public AccountManager()
        {
            
           

        }

        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public ApplicationUser ViewModelToApplicatioUser(UsersViewModel model,ApplicationUser user)
        {

            user.FirstName = model.FirstName != null ? model.FirstName : user.FirstName;
            user.LastName = model.LastName != null ? model.LastName : user.LastName;
            user.PhoneNumber = model.PhoneNumber != null ? model.PhoneNumber : user.PhoneNumber;
            user.IsActive = model.IsActive.GetValueOrDefault();
            user.EmployeeId = model.employeeid != null ? model.employeeid : user.EmployeeId;
            user.NationalId = model.NationalId != null ? model.NationalId : user.NationalId;
            user.DOB = model.DateOfbirth != null ? model.DateOfbirth : user.DOB;
            user.LicenceId = model.LicenceId != null ? model.LicenceId : user.LicenseNo;

            user.Email = model.Email != null ? model.Email : user.Email;
            user.BikeInformation = model.BikeInformation != null ? model.BikeInformation : user.BikeInformation;
            user.LicencePlate = model.LicencePlate != null ? model.LicencePlate : user.LicencePlate;
            user.Position = model.Position;
            user.PasswordHash = model.Password != null ? UserManager.PasswordHasher.HashPassword(model.Password) : user.PasswordHash;
            user.AccessFailedCount = model.Password != null ? 0 : user.AccessFailedCount;
            //user.Status = model.IsActive == true && model.IsDeleted == false ? "Active" : model.IsActive == false && model.IsDeleted == true ? "Deleted" : "Inactive";
            user.Status = model.IsDeleted == true  ? "Deleted" : model.IsActive == false ? "Inactive" : "Active";
            user.CreatedOn = model.createdOn.GetValueOrDefault();


            return user;
        }


        public SearchParam filterparam(SearchParam param)
        {
            if(param==null)
            {
                param = new SearchParam();
            }
           if(param.RelationTable=="CUSTOMER")
            {
                param.Role = "ROLE_CUSTOMER";
            }
           else if (param.RelationTable == "DRIVER")
            {
                param.Role = "ROLE_DRIVER";
            }
            else if (param.RelationTable == "ADMIN")
            {
                param.Role = "ROLE_ADMIN";
            }
            else 
            {
               
            }

            
            return param;
        }

        public UsersViewModel filterUser(UsersViewModel model, ApplicationUser user)
        {
            model.Id = CryptoEngine.Encrypt(FileType.USER + "_" + user.Id.ToString(), KeyConstant.Key);
            
            return model;

        }

        public UsersViewModel addingProfilePic(UsersViewModel model)
        {
            if (model.ProfileImageUrl != null)
            {
                FileGroupItemsViewModel ProfileFile = new FileGroupItemsViewModel();
                ProfileFile.Path = model.ProfileImageUrl;
                model.File = ProfileFile;
            }

            return model;

        }
        public UsersViewModel addingCustomerProfilePic(UsersViewModel model)
        {
            if (model.ProfileImageUrl != null)
            {
                FileGroupItemsViewModel ProfileFile = new FileGroupItemsViewModel();
                ProfileFile.Path = model.ProfileImageUrl;
                model.File = ProfileFile;
            }

            return model;

        }

        public bool comparePassword(string newPasword,string oldpassword)
        {
             newPasword = UserManager.PasswordHasher.HashPassword(newPasword);
            if(newPasword==oldpassword)
            {
                return true;
            }
            return false;
        }


        public UsersViewModel addingProfilePicToUser(ApplicationUser model)
        {
            _manageApplicationUserModel = new ManageApplicationUserModel();
             UsersViewModel user = new UsersViewModel();
             user= _manageApplicationUserModel.modelApplicationUserToCustomerUserViewModel(model);
              return user;

        }


        public long GetOtp()
        {
            long OTP = 123456;
            if (is_MobileSMS_ENABLE == "1")
            {
                OTP = CommonMethods.GetOTP();
            }
            else
            {
                OTP = 123456;// CommonMethods.GetOTP();
            }
            return OTP;
        }

        public Boolean IsAllowSms()
        {
           
            if (is_MobileSMS_ENABLE == "1")
            {
                return true;
            }
           
            return false;
        }

    }
}