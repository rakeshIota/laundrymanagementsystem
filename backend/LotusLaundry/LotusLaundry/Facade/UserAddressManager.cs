﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Country;
using LotusLaundry.Service.State;
using LotusLaundry.Framework.ViewModels.LotusLaundry.State;
using LotusLaundry.Service.Country;
using LotusLaundry.Domain;
using LotusLaundry.Service.City;
using LotusLaundry.Framework.ViewModels.LotusLaundry.City;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Web.Http;
using LotusLaundry.Framework.ViewModels.Users;

namespace LotusLaundry.Facade
{
    public class UserAddressManager : ApiController
    {

        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public UserAddressManager()
        {
           
           

        }

        public SearchParam filterparam(SearchParam param)
        {
            if(param==null)
            {
                param = new SearchParam();
            }
            if (param.UserId == null)
            {
                param.UserId = User.Identity.GetUserId<long>();
            }

                return param;
        }


        public UsersbasicViewModel CheckUserAddressStatus()
        {
            UsersbasicViewModel usersbasic = new UsersbasicViewModel();
            usersbasic.EmailConfirmed = false;
            usersbasic.PhoneVerified = false;
            usersbasic.InServiceArea = false;
            var currentUserUser = UserManager.FindById(Convert.ToInt64(User.Identity.GetUserId()));
            if (currentUserUser.EmailConfirmed==true && currentUserUser.IsDeleted==false)
            {
                usersbasic.EmailConfirmed = true;

            }
            if (currentUserUser.PhoneNumberConfirmed == true && currentUserUser.IsDeleted == false)
            {
                usersbasic.PhoneVerified = true;

            }

            return usersbasic;
        }

        public bool IsVerifiedEmail(SearchParam param)
        {
            //if (UserManager.Users.Any(x => (x.Email.Equals(model.Email)) && x.EmailConfirmed && !x.IsDeleted))
            if (UserManager.Users.Any(x => (x.Id.Equals(param.UserId)) && !x.IsDeleted && x.EmailConfirmed ))
            { 
                return true;
            }
            return false;
        }
        public bool IsVerifiedUserPhone(SearchParam param)
        {
            if (UserManager.Users.Any(x => (x.Id.Equals(param.UserId)) && !x.IsDeleted && x.PhoneNumberConfirmed))
            {
                return true;
            }
            return false;
        }





    }
}