﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Order;
using Customer.Service.Cart;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Service.Stripe;
using LotusLaundry.Service.OrderLog;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Framework.ViewModels.Customer.UserCardDetail;
using LotusLaundry.Facade;
using LotusLaundry.Service.OrderItem;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
using LotusLaundry.Domain.Cart;
using LotusLaundry.Domain.Payment;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Core.Mailer;
using LotusLaundry.Service.Users;
using Customer.Service.UserAddress;
using Customer.Service.AppSetting;
using LotusLaundry.Framework.ViewModels.Customer.Order;
using LotusLaundry.Domain.Order;
using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Core.GeneratePDF;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Domain.CartItem;
using LotusLaundry.Common.Success;
using LotusLaundry.Domain.PushNotification;
using Customer.Service.PushNotification;
using Twilio.Rest.Trunking.V1;
using System.Globalization;
using Customer.Service.Payment;

namespace LotusLaundry.Facade
{
    public class OrderManager
    {
        private IOrderItemService _orderitemService;
        private IOrderService _orderService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        private ICartService _cartService;
        private IOrderLogService _orderlogService;
        private IPaymentLogService _paymentlogService;
        private IStripeService _stripeService;
        private IEmailConfigurationService _emailConfiguration;
        private IUserAddressService _useraddressService;
        private IAppSettingCustomerService _appSettingCustomerService;
        private IPushNotificationService _pushNotificationService;
        private IUsersService _usersService;
        IPaymentService _paymentService;


        TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
        public OrderManager(IPaymentService paymentService, IPushNotificationService pushNotificationService, IAppSettingCustomerService appSettingCustomerService,IUserAddressService useraddressService, IUsersService usersService, IEmailConfigurationService emailConfiguration,IOrderItemService orderitemService,IPaymentLogService paymentlogService, ICartService cartService, IOrderService orderService, IExceptionService exceptionService, IStripeService stripeService, IOrderLogService orderlogService)
        {
            _paymentService = paymentService;
            _appSettingCustomerService = appSettingCustomerService;
            _useraddressService = useraddressService;
            _usersService = usersService;
            _emailConfiguration = emailConfiguration;
            _orderitemService = orderitemService;
            _paymentlogService = paymentlogService;
            _orderlogService = orderlogService;
            _cartService = cartService;
            _orderService = orderService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
            _stripeService = stripeService;
            _pushNotificationService = pushNotificationService;


        }
         
     

        public ResponseModel ProcessMyOrder(long id, SearchParam param, UserCardDetailViewModel UserCardModel)
        {
            #region       ============= common data for the operation  ===================
            ResponseModel result = new ResponseModel();
            OrderViewModel model = new OrderViewModel();
            PaymentQRcodeModel qrmodel = new PaymentQRcodeModel(); 
             CartModel cart = new CartModel();
           
            #endregion
            cart = Getcart(param);
            try
            {
                #region  common data from db 
                var TenantId = param.TenantId;
                var userid = id;
                UserCardModel.DeliveryAddress.CreatedBy = id;
                UserCardModel.DeliveryAddress.TenantId = TenantId;

                SearchParam Settingparam = new SearchParam();
                var services = _appSettingCustomerService.ApplicationSettingSelect(Settingparam);

                decimal sameDayRate = Convert.ToDecimal(services.SAMEDAY_DELIVERY_RATE);
                decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
                decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);
                decimal logisticCharge = Convert.ToDecimal(services.logisticCharge);
                decimal minimumOrderAmount = Convert.ToDecimal(services.minimumOrderAmount);


                //  minimumOrder

                #endregion 



                if (cart.IsLocked == true)
                {
                    result.Status = false;
                    result.Message = ErrorMessage.OrderError.CART_IN_PAYMENT_MODE;
                    return result;
                }
                if (cart.Id == null || cart.Id == 0)
                {
                    result.Status = false;
                    result.Message= ErrorMessage.OrderError.UNABLE_TO_ORDER;
                    return result; 
                }
                
                // getting total ammount
                model.SubTotal = getSubtotal(cart);            // cart.SubTotal;
                model.Discount = UserCardModel.Discount!=null? UserCardModel.Discount:0;
                model.Tax = Convert.ToDecimal(String.Format("{0:0.00}", getTax(model.SubTotal)));
                model.DeliveryPrice = getDeliveryFees(model, cart.DeliveryType,sameDayRate,expressRate,normalRate);
                model.LogisticCharge = getLogisticCharge(model.SubTotal, model.DeliveryPrice, model.Tax, logisticCharge, minimumOrderAmount);
                model.TotalPrice = getTotal(model.SubTotal, model.DeliveryPrice, model.Tax,model.LogisticCharge);
                
                if (UserCardModel.TotalAmount != model.TotalPrice)
                {
                    result.Status = false;
                    result.Message = ErrorMessage.OrderError.PRICE_UPGRADED;
                    return result;
                }

                Lockcart(cart);

                #region Payment process 

                PaymentResponseModel PaymentResult = new PaymentResponseModel();
                if (UserCardModel.PaymentType=="CARD")
                {
                     PaymentResult = MakePayment();
                    if (!PaymentResult.status == true)
                    {
                        UnLockcart(cart);
                        result.Status = false;
                        result.Message = PaymentResult.Message;

                        return result;
                    }
                    model.paymentDate = DateTimeOffset.UtcNow;
                }
                else if(UserCardModel.PaymentType == "QRCODE")
                {
                    var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                   
                    if (authkey.Result.access_token.ToString()==null)
                    {
                        UnLockcart(cart);
                        result.Status = false;
                        result.Message = ErrorMessage.OrderError.QR_CODE_NOT_GENERATED;
                        return result;
                    }
                    qrmodel.authkey = authkey.Result.access_token.ToString();
                   
                    //decimal amount = UserCardModel.TotalAmount.GetValueOrDefault();
                    decimal amount = 80.99M;
                    qrmodel.amount = amount;
                    var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), amount);
                    qrmodel.output = QrCode.ToString();
                    qrmodel.statusCode = "QRGenerated" ;

                    model.qrCode = QrCode.qrCode.ToString();
                    PaymentResult.status = false;
                }
                else
                {
                    PaymentResult.status = false;
                    model.change = UserCardModel.change;

                }

                #endregion


                model.paymentStatus = PaymentResult.status == true ? OrderConstants.PAYMENTPAIDSTATUS : OrderConstants.PAYMENTUNPAINSTATUS;

                int itemCount = 0;
                foreach(var item in cart.CartItems)
                {
                    itemCount = itemCount + item.Quantity.GetValueOrDefault();
                }


                // model.AdressId = UserCardModel.AddressId;
               
                model.PaymentType = UserCardModel.PaymentType;
                model.UserId = cart.CustomerId;
                model.CreatedBy = userid;
                model.UpdatedBy = userid;
                //model.TotalItems = cart.TotalItems;
                // model.TotalItems = cart.CartItems.Count();
                model.TotalItems = itemCount;

                model.Status = OrderStatus.NEW.ToString();
              
                model.DeliveryType = cart.DeliveryType;
                model.DeliveryDate = cart.DeliveryDate;// DateTimeOffset.UtcNow.AddDays(7);
                model.PickupDate = cart.PickupDate;
                model.pickupSlot = cart.pickupSlot;
                model.deliverySlot = cart.deliverySlot;
                model.TenantId = TenantId;
                model.IsActive = true;
                model.CartId = cart.Id;
                model.PickupDate = cart.PickupDate;
                model.deliverySlot = cart.deliverySlot;

                model.pickupSlot = cart.pickupSlot;
                model.Status = OrderConstants.NEW;
                model.paymentResponse = PaymentResult.status == true ? PaymentResult.Message : null;
                model.note = UserCardModel.note;
                model.laundryInstruction = cart.laundryInstruction;
              
                result =cartToOrder(model,cart);
               
                if(result.Status==false)
                {
                    UnLockcart(cart);
                    result.Status = false;
                    result.Message = result.Message;
                    return result;
                }
                var orderid = Convert.ToInt32( result.Message);

                if (UserCardModel.PaymentType == "CARD")
                {
                    result = PaymentLog(PaymentResult, userid, TenantId, orderid, UserCardModel.TotalAmount);
                    if (result.Status == false)
                    {
                        UnLockcart(cart);
                        result.Status = false;
                        result.Message = result.Message;
                        return result;
                    }
                }

                if (UserCardModel.PaymentType == "QRCODE")
                {

                    result = QRLog(qrmodel, userid, TenantId, orderid, UserCardModel.TotalAmount);
                    if (result.Status == false)
                    {
                        UnLockcart(cart);
                        result.Status = false;
                        result.Message = result.Message;
                        return result;
                    }
                }

                result = AddDeliveryAddress(UserCardModel.DeliveryAddress, orderid,userid);
                if (result.Status == false)
                {
                    UnLockcart(cart);
                    result.Status = false;
                    result.Message = result.Message;
                    return result;
                }
               // cartReadyToDelete(cart);
                deletecart(cart);

              

                result = SendOrderConfirmEmail(model,cart, UserCardModel.DeliveryAddress, userid, TenantId,orderid.ToString());
                if (result.Status == false)
                {
                    
                    result.Status = false;
                    result.Message = result.Message;
                    return result;
                }
                 var order = orderid.ToString().PadLeft(6, '0');
                string msg = string.Format(PushNotificationMSG.message.ORDER_PlACED.ToString(), order);
               // _pushNotificationService.SendNotificationByFCMToUser(orderid, model.UserId.GetValueOrDefault(), PushNotificationType.OrderPlaced.ToString(), PushNotificationMSG.Title.Order_placed.ToString(), msg);
                PushNotificationModel pmodel = new PushNotificationModel();
                pmodel.CreatedBy = model.UserId.GetValueOrDefault();
                pmodel.IsDeleted = false;
                pmodel.IsActive = true;
                pmodel.UserId= model.UserId.GetValueOrDefault();
                pmodel.Type = PushNotificationType.ORDER_PLACED.ToString();
                pmodel.Title = PushNotificationMSG.Title.Order_placed.ToString();
                pmodel.Message = msg;
                pmodel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + orderid.ToString(), KeyConstant.Key);
               // pmodel.TargetId=  model.UserId.GetValueOrDefault();
                pmodel.UserRole = "CUSTOMER";
                _pushNotificationService.PushNotificationInsert(pmodel);

                result.Status = true;
                result.Message= CryptoEngine.Encrypt(FileType.CART + "_" + orderid.ToString(), KeyConstant.Key);
                result.SecMessage = orderid.ToString();


                //if(UserCardModel.PaymentType=="QR")
                //{
                //    var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                //    var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), UserCardModel.TotalAmount.GetValueOrDefault());
                //    _orderService.OrderPaymentUpdate(orderid, QrCode.qrCode.ToString(), "UNPAID", UserCardModel.UpdatedBy.ToString());
                //}

                return result;

            }
            catch (Exception ex)
            {
                UnLockcart(cart);
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }
        }



        public ResponseModel CancelOrder(string id,long updatedby,string role)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;

                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
              
                long? driverid = 0;
                foreach(var item in order.DriverDetails)
                {
                    driverid=item.Id;
                }
                if (order.Status==OrderConstants.CANCELLED)
                {
                    result.Status = false;
                    result.Message = ErrorMessage.OrderError.ORDER_CANCELLED_ALREADY;
                    return result;
                }
                if (role == "ROLE_CUSTOMER")
                {
                    if (!IsAllowToCancel(id))
                    {


                        result.Status = false;
                        result.Message = ErrorMessage.OrderError.DRIVER_ASSIGNED;
                        return result;
                    }
                }
                order.Status = OrderConstants.CANCELLED;
                order.UpdatedBy = updatedby;
                _orderService.OrderStatusUpdate(order);
                //PaymentResponseModel PaymentResult = new PaymentResponseModel();
                //if (order.PaymentType=="card")
                //{
                   
                //    PaymentResult = refundPayment();
                //    if (!PaymentResult.status == true)
                //    {
                        
                //        result.Status = false;
                //        result.Message = PaymentResult.Message;

                //        return result;
                //    }
                //    result = PaymentLog(PaymentResult, order.UserId, order.TenantId , order.Id, order.TotalPrice);
                //    if (result.Status == false)
                //    {
                        
                //        result.Status = false;
                //        result.Message = result.Message;
                //        return result;
                //    }


                //}
                


                var response= OrderLogInsert(order.Id, order.UserId, order.TenantId, OrderConstants.CANCELLED);
                if(response.Status==false)
                {

                }

                string deliveryAddress = "";
                foreach (var item in order.DeliveryAddress)
                {
                     deliveryAddress = item.BuildingName != null ? item.BuildingName : ' ' + ' ' + item.StreetNumber != null ? item.StreetNumber : ' ' + ' ' + item.DistrictName != null ? item.DistrictName : ' ' + ' ' + item.SubDistrictName != null ? item.SubDistrictName : ' ' + ' ' + item.PostalCode + ' ';
                }

               
                result = SendOrderCancelConfirmEmail(order, deliveryAddress, order.UserId, order.TenantId, order.Id.ToString());
                if (result.Status == false)
                {

                    result.Status = false;
                    result.Message = result.Message;
                    return result;
                }
                string msg = "";
                PushNotificationModel pmodel = new PushNotificationModel();
                PushNotificationModel cModel = new PushNotificationModel();
                if (role == "ROLE_CUSTOMER")
                {
                    pmodel.Title= PushNotificationMSG.Title.ORDER_CANCEL_ByUSER.ToString();
                    pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_CANCEL_BYUser.ToString(), order.Id.ToString().PadLeft(6, '0'));

                    cModel.Type = PushNotificationType.CANCELLED.ToString();
                    cModel.Title = PushNotificationMSG.Title.ORDER_CANCEL_MANAGER.ToString();
                    cModel.Message = string.Format(PushNotificationMSG.message.ORDER_CANCEL_BYManager_Driver.ToString(), order.Id.ToString().PadLeft(6, '0'));


                }
                else
                {
                    pmodel.Title = PushNotificationMSG.Title.ORDER_CANCEL_MANAGER.ToString();
                    pmodel.Message = string.Format(PushNotificationMSG.message.ORDER_CANCEL_BYManager.ToString(), order.Id.ToString().PadLeft(6, '0'));

                    cModel.Type = PushNotificationType.CANCELLED.ToString();
                    cModel.Title = PushNotificationMSG.Title.ORDER_CANCEL_MANAGER.ToString();
                    cModel.Message = string.Format(PushNotificationMSG.message.ORDER_CANCEL_BYManager_Driver.ToString(), order.Id.ToString().PadLeft(6, '0'));


                }
                //_pushNotificationService.SendNotificationByFCMToUser(order.Id.GetValueOrDefault(),order.UserId.GetValueOrDefault(), PushNotificationType.CANCELLED.ToString(), PushNotificationMSG.Title.ORDER_CANCEL.ToString(), msg);

                pmodel.CreatedBy = order.UserId.GetValueOrDefault();
                pmodel.IsDeleted = false;
                pmodel.IsActive = true;
                pmodel.UserId = order.UserId.GetValueOrDefault();
                pmodel.Type = PushNotificationType.CANCELLED.ToString();   
                pmodel.TargetId=CryptoEngine.Encrypt(FileType.ORDER + "_" + order.Id.ToString(), KeyConstant.Key);
                //pmodel.TargetId = order.Id.GetValueOrDefault();
                pmodel.UserRole = "CUSTOMER";
                _pushNotificationService.PushNotificationInsert(pmodel);

                // pushnotifiaction to driver

               
                cModel.CreatedBy = order.UserId.GetValueOrDefault();
                cModel.IsDeleted = false;
                cModel.IsActive = true;
                //cModel.UserId = Convert.ToInt64(CryptoEngine.Decrypt(order.DriverId.ToString(), KeyConstant.Key));
                cModel.UserId = driverid;              
                cModel.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + order.Id.ToString(), KeyConstant.Key);              
                 cModel.UserRole = "DRIVER";
                _pushNotificationService.PushNotificationInsert(cModel);




                result.Status = true;
                result.Message = SuccessMessage.OrderSucces.ORDER_CANCEL;

                return result;
            }
            catch (Exception ex)
            {
               
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }
        }

        public bool IsAllowToCancel(string id)
        {
          
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;

               var order= _orderService.SelectMyOrder(param).FirstOrDefault();

                if (order.Status == "NEW")
                {
                    return true;
                }
                else
                {
                    return false;
                }    
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        

        public CartModel Getcart(SearchParam param)
        {            
                var carts = _cartService.SelectCart(param).FirstOrDefault();
                return carts;   
        }

        public void Lockcart(CartModel model)
        {
             model.IsLocked = true;
            _cartService.CartUpdate(model);
        }
        public void UnLockcart(CartModel model)
        {
            model.IsLocked = false;
            _cartService.CartUpdate(model);
        }

        public PaymentResponseModel MakePayment()
        {
            PaymentResponseModel model = new PaymentResponseModel();
            
           
            try
            {
                
                model.status = true;
                model.TransactionId = "13246";
                model.Message = "hard code complete";
                return model;
            }
            catch (Exception ex)
            {
                model.status = false;
                model.TransactionId = null;
                model.Message = "fail";
                return model;
            }

        }


        public PaymentResponseModel refundPayment()
        {
            PaymentResponseModel model = new PaymentResponseModel();


            try
            {

                model.status = true;
                model.TransactionId = "13246";
                model.Message = "hard code complete";
                return model;
            }
            catch (Exception ex)
            {
                model.status = false;
                model.TransactionId = null;
                model.Message = "fail";
                return model;
            }

        }

        public ResponseModel GenerateQRcode(PaymentResponseModel Transaction, long? Userid, int? TenantId, long? orderid, decimal? TotalAmount)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                decimal a = 80.99m;
                var authkey = _paymentService.GetElibilityToken(PaymentConstant.payKeys.BasicKey.ToString());
                var QrCode = _paymentService.GetQRCode(authkey.Result.access_token.ToString(), a);

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }


        }

        public ResponseModel PaymentLog(PaymentResponseModel Transaction, long? Userid, int? TenantId,long? orderid,decimal? TotalAmount)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                PaymentLogModel paylog = new PaymentLogModel();

                paylog.CreatedBy = Userid;
                paylog.UserId = Userid;
                // paylog.CardNumber = UserCardModel.StripeCardId;
                paylog.Amount = TotalAmount;
                paylog.TenantId = TenantId;
                paylog.IsActive = true;
                paylog.OrderId = orderid;
                paylog.TransactionId = Transaction.Message.Contains("Error :") ? null : Transaction.TransactionId;
                paylog.Status = Transaction.status == true ? "Success" : "Fail";
                
                var response = _paymentlogService.PaymentLogInsert(paylog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }
           

        }


        public ResponseModel QRLog(PaymentQRcodeModel Transaction, long? Userid, int? TenantId, long? orderid, decimal? TotalAmount)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                PaymentQRcodeModel paylog = new PaymentQRcodeModel();

                paylog.CreatedBy = Userid.GetValueOrDefault();
                paylog.userid = Userid.GetValueOrDefault();
                // paylog.CardNumber = UserCardModel.StripeCardId;
                paylog.amount = TotalAmount.GetValueOrDefault();
                paylog.tenantid = TenantId.GetValueOrDefault();
               
                paylog.orderid = orderid.GetValueOrDefault();
                paylog.partnerTxnUid = Transaction.partnerTxnUid;
                paylog.statusCode = Transaction.statusCode;
                paylog.errorDesc = Transaction.errorDesc;
                paylog.output = Transaction.output;
                
                var response = _paymentlogService.QrLogInsert(paylog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }


        }

        public ResponseModel cartToOrder(OrderViewModel model,CartModel cart)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                //OrderViewModel model = new OrderViewModel();
                // model.AdressId = UserCardModel.AddressId;
                //model.PaymentType = PaymentType;
                //model.UserId = carts.CustomerId;
                //model.CreatedBy = Userid;
                //model.UpdatedBy = Userid;
                //model.TotalItems = carts.CartItems[0].TotalCount;
                //model.Status = OrderStatus.PLACED.ToString();
                //model.SubTotal = carts.SubTotal;
                //// model.Tax = (UserCardModel.TotalAmount / Convert.ToInt32(UserCardModel.TaxPercentage));
                //model.Tax = Convert.ToDecimal(String.Format("{0:0.00}", carts.Tax));
                //model.TotalPrice = carts.TotalPrice;
                //model.DeliveryType = carts.DeliveryType;
                //model.DeliveryDate = DateTimeOffset.UtcNow.AddDays(7);
                //model.TenantId = TenantId;
                //model.IsActive = true;
                // model.DeliveryAddress = UserCardModel.DeliveryAddress;
                model.laundryInstruction = cart.laundryInstruction;
                var responseId = _orderService.OrderInsert(model.ToModel());



                OrderItemViewModel orderitem = new OrderItemViewModel();
                orderitem.CreatedBy = model.UserId;
                orderitem.UpdatedBy = model.UserId;
                orderitem.TenantId = model.TenantId;
                foreach (var i in cart.CartItems)
                {

                    orderitem.OrderId = responseId;
                    orderitem.ProductId = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + i.ProductId.ToString(), KeyConstant.Key);// i.ProductId;
                    orderitem.Quantity = i.Quantity;
                    orderitem.SubTotal = i.Price;
                    orderitem.TotalPrice = i.TotalPrice;
                    orderitem.ServiceId = CryptoEngine.Encrypt(FileType.SERVICE + "_" + i.ServiceId.ToString(), KeyConstant.Key); //i.ServiceId;
                    orderitem.IsActive = true;
                    orderitem.IsPacking = true;
                    orderitem.PackingPrice = 1;
                    orderitem.PackingId = 1;
                    //orderitem.TotalPrice=i.Quantity*i.s
                    var itemResponseId = _orderitemService.OrderItemInsert(orderitem.ToModel());

                }



                result.Status = true;
                result.Message = responseId.ToString();


                return result;
            }
            catch (Exception ex)
            {
                UnLockcart(cart);
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }


        }

      
        public ResponseModel AddDeliveryAddress(UserAddressViewModel model, Int32 OrderId,long userid)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                if(model.UserId==null)
                {
                    model.UserId = userid;
                }
                if(model.IsActive==null)
                {
                    model.IsActive = true;
                }
                 // private IUserAddressService _useraddressService;
                var response = _useraddressService.DeliveryAddressInsert(model.ToModel(), OrderId);
                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = ex.Message.ToString();

                return result;
            }
        }




        public ResponseModel OrderLogInsert(long? responseId, long? Userid, int? TenantId,string Note=null)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                OrderLogModel orderlog = new OrderLogModel();
                orderlog.OrderId = responseId;
                orderlog.CreatedBy = Userid;
                orderlog.TenantId = TenantId;
                orderlog.Note = Note;
                orderlog.IsActive = true;
                var response= _orderlogService.OrderLogInsert(orderlog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;
               

            }
        }

        public void deletecart(CartModel model)
        {

            _cartService.CartWithItemDelete(model);

        }


        public ResponseModel SendOrderConfirmEmail(OrderViewModel model,CartModel carts,UserAddressViewModel deliveryAddress,long userid,int? tenant,string orderid )
        {

         

            ResponseModel result = new ResponseModel();
            try
            {
                var userDetail = _usersService.SelectProfile(userid).FirstOrDefault();

                List<OrderItemModel> product = new List<OrderItemModel>();

                
                orderid = orderid.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.OrderEmail.ToString());

                string addressline1="", addressline2="", addressline3="", addressline4="", addressline5="";
                if (deliveryAddress.ResidenceType== "HOME")
                {
                     addressline1 = deliveryAddress.HouseNumber;
                     addressline2 = deliveryAddress.StreetNumber;


                     addressline3 = deliveryAddress.ProvinceName +","+ deliveryAddress.PostalCode;
                     addressline4 = "";
                     addressline5 = "";
                }
                if (deliveryAddress.ResidenceType == "BUILDING")
                {

                    addressline1 = deliveryAddress.BuildingName; 
                    addressline2 = deliveryAddress.Floor + ", " + deliveryAddress.UnitNo+","+ deliveryAddress.StreetNumber;
                   
                    addressline3 = deliveryAddress.ProvinceName +","+ deliveryAddress.PostalCode;
                    addressline4 = "";
                    addressline5 = "";
                    //addressline3 = addressline2 != null ? deliveryAddress.Floor + " " + deliveryAddress.UnitNo + " " + deliveryAddress.StreetNumber : deliveryAddress.StreetNumber;
                    //addressline4 = deliveryAddress.ProvinceName + " " + deliveryAddress.DistrictName;
                    //addressline5 = deliveryAddress.SubDistrictName + " " + deliveryAddress.PostalCode;
                }
                //addressline1 = deliveryAddress.BuildingName != null ? deliveryAddress.BuildingName : deliveryAddress.HouseNumber;
                //addressline2 = deliveryAddress.Floor != null ? deliveryAddress.Floor : null;


                //addressline3 = addressline2 != null ? deliveryAddress.Floor + " " + deliveryAddress.UnitNo + " " + deliveryAddress.StreetNumber : deliveryAddress.StreetNumber;
                //addressline4 = deliveryAddress.ProvinceName + " " + deliveryAddress.DistrictName;
                //addressline5 = deliveryAddress.SubDistrictName + " " + deliveryAddress.PostalCode;

                string pickup = String.Format("{0:ddd, MMM d, yyyy}", model.PickupDate)  + " " + model.pickupSlot;
                string delivery = String.Format("{0:ddd, MMM d, yyyy}", model.DeliveryDate)  + " " + model.deliverySlot;
               

                var name = txtInfo.ToTitleCase(userDetail.FirstName);
                //string usesEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(model, carts), model.SubTotal,model.DeliveryType,model.TotalPrice,model.PaymentType, convertListToString(deliveryAddress));
                string usesEmailBody = string.Format(configData.ConfigurationValue, name, orderid, convertListToTable(model, carts), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1,addressline2,addressline3,addressline4, pickup, delivery, "฿"+model.change);


                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.NewOrderRecevied.ToString());

                // string AdminEmailBody = string.Format(configData.ConfigurationValue, userDetail.FirstName, orderid, convertListToTable(model, carts), model.SubTotal, model.DeliveryType, model.TotalPrice, model.PaymentType, convertListToString(deliveryAddress));
                string AdminEmailBody = string.Format(configData.ConfigurationValue, name, orderid, convertListToTable(model, carts), "฿" + model.SubTotal, model.DeliveryType, "฿" + model.TotalPrice, model.PaymentType, name, addressline1, addressline2, addressline3, addressline4, model.PickupDate, model.DeliveryDate, "฿"+model.change);

                // MailSender.SendEmail(userDetail.Email, "Order Placed", usesEmailBody).Wait();
                string subject = string.Format(EmailConstants.ORDER_PlACED, orderid);

                if (model.DeliveryType == "CARD")
                {
                    foreach (CartItemModel item in carts.CartItems)
                    {
                        OrderItemModel orderItem = new OrderItemModel();
                        orderItem.ProductName = item.ProductName;
                        orderItem.SubTotal = item.Price;
                        orderItem.TotalPrice = item.TotalPrice;
                        orderItem.Quantity = item.Quantity;
                        product.Add(orderItem);

                    }
                    //todo setting admin mails 
                    var invoice = GenerateInvoice.GenerateOrderInvoice(userDetail, product, model.TotalPrice, model.ToModel());
                    MailSender.SendEmailWithAttachment(userDetail.Email, subject, usesEmailBody, invoice).Wait();
                   
                }

                else
                {
                    MailSender.SendEmail(userDetail.Email, subject, usesEmailBody).Wait();
                   // MailSender.SendEmailWithAttachment("admin", EmailConstants.RECEIVE_ORDER, AdminEmailBody).Wait();
                }
             


               

                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }


        public ResponseModel GetInvoice(string id)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                SearchParam param = new SearchParam();
                param.Id = id;

                var order = _orderService.SelectMyOrderDetails(param).FirstOrDefault();
                var userDetail = _usersService.SelectProfile(order.UserId.GetValueOrDefault()).FirstOrDefault();

                var invoice = GenerateInvoice.GenerateNewOrderInvoice(userDetail, order.OrderItems, order.TotalPrice, order);
                response.Status = true;
                response.Message = invoice;
                return response;
            }
            catch(Exception ex)
            {
                string msg = ex.Message.ToString();
                response.Status = false;
                return response;
            }
           
        }

        public ResponseModel SendOrderCancelConfirmEmail(OrderModel model, string  deliveryAddress, long? userid, int? tenant, string orderid)
        {
            ResponseModel result = new ResponseModel();
            try
            {
                orderid = orderid.ToString().PadLeft(6, '0');
                var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.OrderCancel.ToString());

                var userDetail = _usersService.SelectProfile(userid.GetValueOrDefault()).FirstOrDefault();
                string usesEmailBody = string.Format(configData.ConfigurationValue, orderid, orderid, convertOrderListToTable(model), '฿'+ model.SubTotal, model.DeliveryType, '฿'+model.TotalPrice, model.PaymentType, deliveryAddress);

                var adminConfigData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.OrderCancel.ToString());

                string AdminEmailBody = string.Format(configData.ConfigurationValue, orderid, orderid, convertOrderListToTable(model), '฿'+ model.SubTotal, model.DeliveryType, '฿'+model.TotalPrice, model.PaymentType, deliveryAddress);

                string subject = string.Format( EmailConstants.CANCEL_ORDER,orderid);

                MailSender.SendEmail(userDetail.Email, subject, usesEmailBody).Wait();
               // MailSender.SendEmail("admin", subject, AdminEmailBody).Wait();

                result.Status = true;
                result.Message = "Email Sent";
                return result;


            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message.ToString();
                return result;


            }
        }

        public decimal? getDeliveryFees(OrderViewModel model, string DeliveryType, decimal sameDayRate, decimal expressRate, decimal normalRate)
        {
            //SearchParam param = new SearchParam();
            //var services = _appSettingCustomerService.ApplicationSettingSelect(param);
            
            //decimal sameDayRate = Convert.ToDecimal( services.SAMEDAY_DELIVERY_RATE);
            //decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
            //decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);

            if (DeliveryType.ToLower() == "sameday")
            {
                return model.SubTotal * sameDayRate;
            }
            if (DeliveryType.ToLower() == "express")
            {

                return model.SubTotal * expressRate;
            }
            if (DeliveryType.ToLower() == "normal")
            {
                return model.SubTotal * normalRate;
            }
            return sameDayRate;
        }

        //public DateTimeOffset? getDeliveryDate(OrderViewModel model, string DeliveryType)
        //{
        //    SearchParam param = new SearchParam();
        //    var services = _appSettingCustomerService.ApplicationSettingSelect(param);

        //    decimal sameDayRate = Convert.ToDecimal(services.SAMEDAY_DELIVERY_RATE);
        //    decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
        //    decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);

        //    if (DeliveryType.ToLower() == "sameday")
        //    {
        //        return DateTimeOffset.UtcNow.AddDays(7);
        //    }
        //    if (DeliveryType.ToLower() == "express")
        //    {

        //        return model.SubTotal * expressRate;
        //    }
        //    if (DeliveryType.ToLower() == "normal")
        //    {
        //        return model.SubTotal * normalRate;
        //    }
        //    return sameDayRate;
        //}
       



        public decimal  getSubtotal(CartModel cart)
        {
            decimal total = 0.00m;

            foreach(var item in cart.CartItems)
            {
                //  total = (total + Convert.ToInt32(item.Quantity) * item.Price).GetValueOrDefault();
                total = (total +  item.TotalPrice).GetValueOrDefault();

            }

            return total;
        }

        public decimal? getTax(decimal? subtotal)
        {
            decimal? taxAmount = 0.0m;

            return subtotal * taxAmount;
        }


        public decimal? getTotal(decimal? subtotal, decimal? DeliveryFees, decimal? Tax,decimal? logisticCharges)
        {

            return subtotal + DeliveryFees  + Tax+ logisticCharges;
        }

        public decimal? getLogisticCharge(decimal? subtotal, decimal? DeliveryFees, decimal? Tax, decimal? logisticCharge,decimal? minimumOrder)
        {
            decimal? total = subtotal + DeliveryFees + Tax;



            if (total < minimumOrder)
            {
                return logisticCharge;
            }
            return 0;
        }



        public string convertListToTable(OrderViewModel order, CartModel cart)
        {
            var deliveryType = order.DeliveryType;
           // string tbl = "<thead> <th style = 'width:25%'> Item Name </th><th style = 'width: 20%'> Price </th> <th style = 'width: 20%'> Quantity </th><th style = 'text-align: right; padding-right: 10px; width: 10%;'> Amount </th></thead> ";

            string tbl = "<thead style='color: #0000d4';> <th  scope='col' style = 'border:1px solid black'> Product </th><th scope='col' style = 'border:1px solid black'> Description </th> <th scope='col' style = 'border:1px solid black'> Price </th><th scope='col' style = 'border:1px solid black'> Quantity </th><th scope='col' style = 'border:1px solid black'> Total </th></thead><tbody style='color: #0000d4;'> ";

            foreach (var cartItem in cart.CartItems)
            {
                tbl += "<tr><td style = 'border:1px solid black'>" + cartItem.ProductName + "</td><td style = 'border:1px solid black'>" + cartItem.ProductName + "</td> <td style = 'border:1px solid black;text-align:right'>฿" + cartItem.Price + "</td><td style = 'border:1px solid black;text-align:center'>" + cartItem.Quantity + "</td><td style = 'border:1px solid black;text-align:right'>฿" + cartItem.TotalPrice + "</td></tr>";
            }
            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";
            tbl += "</tbody>";
            return tbl;
        }

       

        public string convertListToString(UserAddressViewModel model)
        {

            //      string tbl =  model.BuildingName!=null?model.BuildingName:' ' + ' ' + model.StreetNumber != null ? model.StreetNumber : ' ' + ' ' + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode  + ' ';

            //string tbl1 = model.BuildingName != null ? model.BuildingName : ' '  + model.StreetNumber != null ? model.StreetNumber : ' '  + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode + ' ';
            string tbl="";
            if (model.HouseNumber != null)
            {
                tbl = tbl + ' ' + model.HouseNumber;
            }
            if (model.BuildingName != null)
            {
                tbl = tbl + ' ' + model.BuildingName;
            }
            if (model.StreetNumber != null)
            {
                tbl = tbl + ',' + model.StreetNumber;
            }
            if (model.Floor != null)
            {
                tbl = tbl + ',' + model.Floor;
            }
            if (model.UnitNo != null)
            {
                tbl = tbl + ',' + model.UnitNo;
            }
            if (model.SubDistrictName != null)
            {
                tbl = tbl + ',' + model.SubDistrictName;
            }
            if (model.DistrictName != null)
            {
                tbl = tbl + ',' + model.DistrictName;
            }
            if (model.ProvinceName != null)
            {
                tbl = tbl + ',' + model.ProvinceName;
            }
            if (model.PostalCode != null)
            {
                tbl = tbl + ',' + model.PostalCode;
            }
            if (model.PhoneNo != null)
            {
                tbl = tbl + ',' + model.PhoneNo;
            }

            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";

            return tbl;
        }

        public string convertAddressModelToString(UserAddressModel model)
        {

            string tbl = model.BuildingName != null ? model.BuildingName : ' ' + ' ' + model.StreetNumber != null ? model.StreetNumber : ' ' + ' ' + model.DistrictName != null ? model.DistrictName : ' ' + ' ' + model.SubDistrictName != null ? model.SubDistrictName : ' ' + ' ' + model.PostalCode + ' ';


            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";

            return tbl;
        }


        public string convertOrderListToTable(OrderModel order)
        {
            var deliveryType = order.DeliveryType;
            // string tbl = "<thead> <th style = 'width:25%'> Item Name </th><th style = 'width: 20%'> Price </th> <th style = 'width: 20%'> Quantity </th><th style = 'text-align: right; padding-right: 10px; width: 10%;'> Amount </th></thead> ";

            string tbl = "<thead style='color: #0000d4';> <th  scope='col' style = 'border:1px solid black'> Product </th><th scope='col' style = 'border:1px solid black'> Description </th> <th scope='col' style = 'border:1px solid black'> Price </th><th scope='col' style = 'border:1px solid black'> Quantity </th><th scope='col' style = 'border:1px solid black'> Total </th></thead><tbody style='color: #0000d4;'> ";

            foreach (var cartItem in order.OrderItems)
            {
                tbl += "<tr><td style = 'border:1px solid black'>" + cartItem.ProductName + "</td><td style = 'border:1px solid black'>" + cartItem.ProductName + "</td> <td style = 'border:1px solid black'>฿" + cartItem.TotalPrice + "</td><td style = 'border:1px solid black'>" + cartItem.Quantity + "</td><td style = 'border:1px solid black'>฿" + cartItem.TotalPrice*cartItem.Quantity + "</td></tr>";
            }
            //tbl += "<tr><td colspan='2'></td><td><b>Sub Total </b></td><td style= 'text-align: right; padding-right: 10px; width: 10%;'> $" + cart.CartItems[0].TotalPrice + " </td></tr> <tr><td colspan = '2' ></td><td><b> GST Amount(Included in original price) </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.Tax + " </td></tr><tr> <td colspan = '2'></td><td><b> Shipping charges  </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.DeliveryPrice + " </td></tr>  <tr><td colspan = '2' ></td><td><b> Grand Total </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>$" + order.TotalPrice + "</td ></tr><tr><td colspan = '2' ></td><td><b> Delivery Type </b></td><td style = 'text-align: right; padding-right: 10px; width: 10%;'>" + deliveryType + "</td ></tr> ";
            tbl += "</tbody>";
            return tbl;
        }


    }
}