﻿CREATE PROCEDURE [dbo].[CartItemUpdateApp]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@ServiceId   BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CartItem]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CartId] = ISNULL(@CartId,[CartId]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId]),
			 	[Quantity] = ISNULL(@Quantity,[Quantity]),
			 	[Price] = ISNULL(@Price,[Price]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),
				[ServiceId] =  ISNULL(@ServiceId,[ServiceId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemXMLSave