﻿-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>  select * from users   
-- =============================================    
CREATE PROCEDURE [dbo].[CheckUserCallingStatus]    
 -- Add the parameters for the stored procedure here    
 @FromUserId BIGINT = NULL,    
 @ToUserId BIGINT = NULL    
AS    
  
  
  
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  If Exists (Select Id  from users     
  WHERE     
  (      
    (    
    id = @FromUserId   
    )    
   OR    
    (    
    id = @ToUserId   
    )    
  )    
   AND IsActive = 1  
   AND callStatus='IDLE'  
  )    
  BEGIN    
   Select CAST(0 AS BIT)     
  END     
  ELSE    
 BEGIN    
  Select CAST(1 AS BIT)     
 END     
    
END