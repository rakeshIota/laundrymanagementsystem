﻿CREATE PROCEDURE [dbo].[UserDeviceInsert]  
 @DeviceId  NVARCHAR(250)=NULL,  
 @UserId   BIGINT=NULL,  
 @DeviceType  NVARCHAR(50)=NULL,  
 @UserType  NVARCHAR(50)=NULL,  
 @DeviceToken NVARCHAR(250)=NULL,  
 @IsDeleted  BIT=NULL  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
 IF NOT EXISTS(SELECT DeviceId FROM UserDevice WHERE DeviceId=@DeviceId AND IsDeleted = 0)  
 BEGIN  
 
  INSERT INTO UserDevice  
  (  
   DeviceId,  
   UserId,  
   DeviceType,  
   UserType,  
   DeviceToken,
   IsDeleted
  )  
  VALUES  
  (  
   @DeviceId,  
   @UserId,  
   @DeviceType,  
   @UserType,  
   @DeviceToken ,
   0
   
  )  
   select cast(1 as decimal)
 END  
 ELSE  
 BEGIN  
  UPDATE UserDevice SET  
   DeviceType = ISNULL(@DeviceType, DeviceType),  
   UserType = ISNULL(@UserType, UserType),  
   DeviceToken = ISNULL(@DeviceToken, DeviceToken),  
   IsDeleted = ISNULL(@IsDeleted, IsDeleted),  
   UserId = ISNULL(@UserId,UserId)  
  WHERE   
   DeviceId=@DeviceId  
   --AND delete  from UserDevice 
   select cast(1 as decimal)
 END  
END