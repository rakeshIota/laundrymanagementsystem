﻿CREATE PROCEDURE [dbo].[PushNotificationRead]
	  	@Ids			NVARCHAR(250)=NULL,
	  	@UpdatedBy		BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE PushNotification
	SET
	 UpdatedBy = ISNULL(@UpdatedBy,UpdatedBy),	 		
	 UpdatedOn = switchoffset(sysdatetimeoffset(),'+00:00'),
	 IsSeen=1
	 WHERE
	 (
	  Id IN (SELECT number FROM ListToTable(@Ids))
	 )
END