﻿CREATE PROCEDURE [dbo].[NotificationInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Token NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Response NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Notification]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Message],[LanguageId],[Type],[Token],[Status],[Response]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Message,@LanguageId,@Type,@Token,@Status,@Response
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationUpdate

/***** Object:  StoredProcedure  [dbo].[NotificationUpdate] *****/
SET ANSI_NULLS ON