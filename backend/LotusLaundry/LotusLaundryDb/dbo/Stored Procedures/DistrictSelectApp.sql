﻿CREATE PROCEDURE [dbo].[DistrictSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.Id AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictName
		
	FROM [District] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[DistrictLanguageMappingSelect] ***/