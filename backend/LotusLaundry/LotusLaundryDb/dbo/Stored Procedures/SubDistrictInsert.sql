﻿CREATE PROCEDURE [dbo].[SubDistrictInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DistrictId    BIGINT=NULL,
			@Name          NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [SubDistrict]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[DistrictId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@DistrictId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictUpdate

/***** Object:  StoredProcedure  [dbo].[SubDistrictUpdate] *****/
SET ANSI_NULLS ON