﻿CREATE PROCEDURE [dbo].[CartItemInsertApp](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@ServiceId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CartItem]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CartId],[ProductId],[Quantity],[Price],[TotalPrice],[ServiceId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CartId,@ProductId,@Quantity,@Price,@TotalPrice,@ServiceId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemUpdate

/***** Object:  StoredProcedure  [dbo].[CartItemUpdate] *****/
SET ANSI_NULLS ON