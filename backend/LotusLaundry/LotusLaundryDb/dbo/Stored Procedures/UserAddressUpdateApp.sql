﻿CREATE PROCEDURE [dbo].[UserAddressUpdateApp]      
   @Id    BIGINT=NULL,      
   @TenantId   INT=NULL,      
   @Slug NVARCHAR(MAX)=NULL,      
    @UpdatedBy    BIGINT=NULL,      
   @IsDeleted    BIT=NULL,      
   @IsActive    BIT=NULL,      
   @UserId    BIGINT=NULL,      
   @AddressLine1 NVARCHAR(MAX)=NULL,      
   @AddressLine2 NVARCHAR(MAX)=NULL,      
    @DistrictId    BIGINT=NULL,      
    @SubDistrictId    BIGINT=NULL,      
    @PostalCode NVARCHAR(MAX)=NULL,      
    @IsDefault    BIT=NULL,      
    @Tag NVARCHAR(MAX)=NULL,      
     @ProvinceId    BIGINT=NULL,      
   @Latitude    DECIMAL(18,6)=NULL,      
   @Longitude    DECIMAL(18,6)=NULL,      
   @Type        NVARCHAR(100)=NULL,      
   @HouseNumber NVARCHAR(100)=NULL,      
   @StreetNumber NVARCHAR(100)=NULL,      
   @Note         NVARCHAR(MAX)=NULL  ,    
    
    
   @BuildingName NVARCHAR(100)=NULL,     
   @Floor NVARCHAR(100)=NULL,     
   @UnitNo NVARCHAR(100)=NULL,     
   @PhoneNo NVARCHAR(100)=NULL,     
   @alterNumber NVARCHAR(100)=NULL,    
   @ResidenceType   NVARCHAR(100)=NULL    
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    UPDATE [UserAddress]      
 SET      
     [Slug] = ISNULL(@Slug,[Slug]),      
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),      
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),       
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),      
     [IsActive] = ISNULL(@IsActive,[IsActive]),      
     [UserId] = ISNULL(@UserId,[UserId]),      
     [AddressLine1] = ISNULL(@AddressLine1,[AddressLine1]),      
     [AddressLine2] = ISNULL(@AddressLine2,[AddressLine2]),      
     [DistrictId] = ISNULL(@DistrictId,[DistrictId]),      
     [SubDistrictId] = ISNULL(@SubDistrictId,[SubDistrictId]),      
     [PostalCode] = ISNULL(@PostalCode,[PostalCode]),      
     [IsDefault] = ISNULL(@IsDefault,[IsDefault]),      
     [Tag] = ISNULL(@Tag,[Tag]),      
     [ProvinceId] = ISNULL(@ProvinceId,[ProvinceId]),      
     [Latitude] = ISNULL(@Latitude,[Latitude]),      
     [Longitude] = ISNULL(@Longitude,[Longitude]),      
    [Type]      = ISNULL(@Type,[Type]),      
    [HouseNumber] =  ISNULL(@HouseNumber,[HouseNumber]),      
    [StreetNumber]  =  ISNULL(@StreetNumber,[StreetNumber]),      
    [Note]        =  ISNULL(@Note,[Note]) ,     
    
 [BuildingName] =  ISNULL(@BuildingName,BuildingName) ,     
   [Floor] =  ISNULL(@Floor,[Floor]) ,     
   [UnitNo] =  ISNULL(@UnitNo,[UnitNo]) ,     
   [PhoneNo] =  ISNULL(@PhoneNo,[PhoneNo]) ,     
   alterNumber =  ISNULL(@alterNumber,alterNumber) ,     
   [ResidenceType] =  ISNULL(@ResidenceType,[ResidenceType])       
    
 WHERE      
  (      
   [Id]=@Id      
  )      
  AND      
  (      
  @UserId IS NULL      
   OR      
  [UserId] =  @UserId      
  )      
  AND      
  (      
  [TenantId] =  @TenantId      
  )      
END      
      
      
---------------------------------------------------      
---------------------------------------------------      
-- Procedure : UserAddressXMLSave