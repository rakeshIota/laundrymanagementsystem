﻿CREATE PROCEDURE [dbo].[FeedbackXMLSave]
 @FeedbackXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(OrderId)[1]', 'BIGINT') AS 'OrderId',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type',
		NDS.DT.value('(Message)[1]', 'NVARCHAR') AS 'Message',
		NDS.DT.value('(Rating)[1]', 'BIGINT') AS 'Rating',
		NDS.DT.value('(DriverId)[1]', 'BIGINT') AS 'DriverId',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId'
  FROM 
	@FeedbackXml.nodes('/root/Feedback') AS NDS(DT)
   )
   MERGE INTO Feedback R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[OrderId] =ISNULL(x.[OrderId] ,R.[OrderId]),R.[Type] =ISNULL(x.[Type] ,R.[Type]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[Rating] =ISNULL(x.[Rating] ,R.[Rating]),R.[DriverId] =ISNULL(x.[DriverId] ,R.[DriverId]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[OrderId],
		[Type],
		[Message],
		[Rating],
		[DriverId],
		[UserId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[OrderId],x.[Type],x.[Message],x.[Rating],x.[DriverId],x.[UserId]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : UserDeviceInsert

/***** Object:  StoredProcedure [dbo].[UserDeviceInsert] *****/
SET ANSI_NULLS ON