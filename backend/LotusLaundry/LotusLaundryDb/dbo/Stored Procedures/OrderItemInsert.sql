﻿CREATE PROCEDURE [dbo].[OrderItemInsert](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @OrderId    BIGINT=NULL,  
     @ProductId    BIGINT=NULL,  
   @Quantity   INT=NULL,  
     @IsPacking    BIT=NULL,  
   @PackingPrice    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
     @PackingId    BIGINT=NULL  ,
	 @ServiceId bigint=null
)  
AS   
BEGIN  
SET NOCOUNT ON;  
   INSERT INTO [OrderItem]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[ProductId],[Quantity],[IsPacking],[PackingPrice],[SubTotal],[TotalPrice],[PackingId],[ServiceId]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@ProductId,@Quantity,@IsPacking,@PackingPrice,@SubTotal,@TotalPrice,@PackingId,@ServiceId
   )  
 SELECT SCOPE_IDENTITY()  
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : OrderItemUpdate  
  
/***** Object:  StoredProcedure  [dbo].[OrderItemUpdate] *****/  
SET ANSI_NULLS ON