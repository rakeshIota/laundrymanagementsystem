﻿CREATE PROCEDURE [dbo].[PasswordLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Count   INT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PasswordLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Count]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Count
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : PasswordLogUpdate

/***** Object:  StoredProcedure  [dbo].[PasswordLogUpdate] *****/
SET ANSI_NULLS ON