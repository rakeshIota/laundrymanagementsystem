﻿CREATE PROCEDURE [dbo].[PaymentLogInsert](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @UserId    BIGINT=NULL,  
   @CardNumber NVARCHAR(MAX)=NULL,  
   @Amount    DECIMAL(18,2)=NULL,  
     @OrderId    BIGINT=NULL,  
   @TransactionId NVARCHAR(MAX)=NULL,  
   @Status NVARCHAR(MAX)=NULL  
)  
AS   
BEGIN  
SET NOCOUNT ON;  
declare @result decimal;
   INSERT INTO [PaymentLog]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[CardNumber],[Amount],[OrderId],[TransactionId],[Status]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@CardNumber,@Amount,@OrderId,@TransactionId,@Status  
   )  
 


 SELECT @result=SCOPE_IDENTITY()   
 
   declare @note varchar(max)=cast( 'Order id='+cast(@OrderId as varchar(100))+' paylmentlog  id='+cast(@result as varchar(100)) as varchar(max));
     
	 INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )  
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@note)

	   select cast (@result as decimal) 
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : PaymentLogUpdate  
  
/***** Object:  StoredProcedure  [dbo].[PaymentLogUpdate] *****/  
SET ANSI_NULLS ON