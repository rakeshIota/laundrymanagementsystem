﻿CREATE PROCEDURE [dbo].[SubDistrictSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL,
	@DistrictId  BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.Id AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId ) AS SubDistrictName
	
	FROM [SubDistrict] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		@DistrictId IS NULL
			OR
		R.[DistrictId] =  @DistrictId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingSelect] ***/