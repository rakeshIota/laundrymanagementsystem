﻿CREATE PROCEDURE [dbo].[UserAddressInActivePostalCode](        
--declare    
@Id         BIGINT =NULL,        
@next   int = NULL,        
@offset  int = NULL,        
@LanguageId BIGINT=NULL,        
@TenantId   INT=NULL,        
@Type       NVARCHAR(100)=NULL,           
@UserId     BIGINT=NULL         
)    AS     
BEGIN        
        
IF @next IS NULL        
BEGIN        
SET @next =100000        
SET @offset=1        
END        
        
 SELECT        
    R.[Id],        
    R.[AddressLine1],        
   R.[AddressLine2],        
    R.[DistrictId],        
    R.[SubDistrictId],        
    R.[PostalCode],        
    R.[Tag],        
    R.[Latitude],        
    R.[Longitude],        
   R.[HouseNumber],        
   R.[StreetNumber],        
   R.[Note],        
   R.[Type],        
        
      
   R.[BuildingName] ,      
   R.[Floor] ,      
   R.[UnitNo] ,      
   R.[PhoneNo] ,      
   R.alterNumber ,      
   R.[ResidenceType] ,      
      
         overall_count = COUNT(*) OVER(),        
   D.Name  AS DistrictName,        
   SD.Name  AS SubDistrictName,        
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,        
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId ) AS SubDistrictNameL        
        
        
 FROM [UserAddress] R INNER JOIN        
 [District] D ON D.Id =  R.[DistrictId]        
 INNER JOIN         
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId        
          
 WHERE         
 (        
  @Id IS NULL        
  OR        
  R.[Id] = @Id        
 )        
 AND        
 (        
  R.[IsDeleted] = 0        
 )        
 AND        
 (        
  @TenantId IS NULL        
  OR        
  R.[TenantId] = @TenantId        
 )        
 AND        
 (        
  @Type IS NULL        
   OR        
  R.[Type] =  @Type        
 )        
 And     
 (    
 @UserId is null    
 or     
 R.UserId=@UserId    
 and  
 r.PostalCode in (select PostalCode from postalcode where isactive=1 and isdeleted=0)  
 )    
        
 Order by R.Id desc        
 OFFSET (@next*@offset)-@next ROWS        
    FETCH NEXT @next ROWS ONLY        
        
END        
        
        
        
---------------------------------------------------        
---------------------------------------------------        
-- Procedure : DistrictSelect        
        
/***** Object:  StoredProcedure [dbo].[DistrictSelect] ***/