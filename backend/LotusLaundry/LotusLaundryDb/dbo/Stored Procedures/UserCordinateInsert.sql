﻿CREATE PROCEDURE [dbo].[UserCordinateInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Latitude    DECIMAL(18,2)=NULL,
			@Longitude    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [UserCordinate]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Latitude],[Longitude],[OrderId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Latitude,@Longitude,@OrderId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : UserCordinateUpdate

/***** Object:  StoredProcedure  [dbo].[UserCordinateUpdate] *****/
SET ANSI_NULLS ON