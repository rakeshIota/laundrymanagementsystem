﻿CREATE PROCEDURE [dbo].[FeedbackUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@Rating    BIGINT=NULL,
		  	@DriverId    BIGINT=NULL,
		  	@UserId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Feedback]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId]),
			 	[Type] = ISNULL(@Type,[Type]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[Rating] = ISNULL(@Rating,[Rating]),
			 	[DriverId] = ISNULL(@DriverId,[DriverId]),
			 	[UserId] = ISNULL(@UserId,[UserId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackXMLSave