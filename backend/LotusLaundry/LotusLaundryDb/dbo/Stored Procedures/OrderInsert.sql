﻿CREATE PROCEDURE [dbo].[OrderInsert](                
                
   @TenantId   INT=NULL,                
   @Slug NVARCHAR(MAX)=NULL,                
     @CreatedBy    BIGINT=NULL,                
     @IsActive    BIT=NULL,                
     @UserId    BIGINT=NULL,                
   @DeliveryDate  DATETIMEOFFSET(7)=NULL,                
     @DeliveryType    nvarchar(100)=NULL,                
   @TotalItems   INT=NULL,                
   @DeliveryPrice    DECIMAL(18,2)=NULL,                
     @DriverId    BIGINT=NULL,                
   @Status NVARCHAR(MAX)=NULL,                
   @PaymentType NVARCHAR(MAX)=NULL,                
   @PaidAmount    DECIMAL(18,2)=NULL,                
   @Tax    DECIMAL(18,2)=NULL,                
   @SubTotal    DECIMAL(18,2)=NULL,                
   @TotalPrice    DECIMAL(18,2)=NULL,                
     @AdressId    BIGINT=NULL,                
     @ExpectedPickUpMin    BIGINT=NULL,                
     @ExpectedPickUpMax    BIGINT=NULL,                
     @ExpectedDeliveryMin    BIGINT=NULL,                
     @ExpectedDeliveryMax    BIGINT=NULL,                
     @CartId    BIGINT=NULL ,              
  @Deliveryaddress nvarchar(max)=null  ,            
            
            
               
    @PickupDate DATETIMEOFFSET(7)=NULL,            
    @deliverySlot NVARCHAR(100)=NULL,              
   @pickupSlot NVARCHAR(100)=NULL  ,          
    @LogisticCharge DECIMAL(18,2)=NULL ,       
    @Discount     DECIMAL(18,2)=NULL ,      
 @paymentResponse nvarchar(100)=null    ,  
 @paymentStatus nvarchar(100)=null ,
 @change DECIMAL(18,2)=NULL , 
 @note nvarchar(max)
)                
AS                 
BEGIN                
SET NOCOUNT ON;           
    
   declare @result decimal;    
    
    
    
   INSERT INTO   [Order]                
   (                
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[DeliveryDate],[DeliveryType],[TotalItems],    
 [DeliveryPrice],[DriverId],[Status],[PaymentType],[PaidAmount],[Tax],[SubTotal],[TotalPrice],[ExpectedPickUpMin],    
 [ExpectedPickUpMax],[ExpectedDeliveryMin],[ExpectedDeliveryMax],[CartId] ,[Deliveryaddress],[PickupDate],[deliverySlot],[pickupSlot],    
 LogisticCharge,Discount,paymentResponse,paymentstatus,change,note              
   )                
   VALUES                
   (                 
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@DeliveryDate,@DeliveryType,@TotalItems,    
 @DeliveryPrice,@DriverId,@Status,@PaymentType,@PaidAmount,@Tax,@SubTotal,@TotalPrice,@ExpectedPickUpMin,@ExpectedPickUpMax,    
 @ExpectedDeliveryMin,@ExpectedDeliveryMax,@CartId  ,@Deliveryaddress  ,@PickupDate,@deliverySlot,@pickupSlot  ,@LogisticCharge      
 ,@Discount ,@paymentResponse  , @paymentstatus   ,@change,@note
   )                
    
    
    
 SELECT @result=SCOPE_IDENTITY()        
   declare @Lognote varchar(max)=cast( 'Cart id='+cast(@cartid as varchar(100))+' is convert to order order id='+cast(@result as varchar(100)) as varchar(max));    
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )      
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@result,@Lognote)    
      
 select cast (@result as decimal)    
 END                
                 
                 
                
---------------------------------------------------                
---------------------------------------------------                
-- Procedure : OrderUpdate                
                
/***** Object:  StoredProcedure  [dbo].[OrderUpdate] *****/                
SET ANSI_NULLS ON