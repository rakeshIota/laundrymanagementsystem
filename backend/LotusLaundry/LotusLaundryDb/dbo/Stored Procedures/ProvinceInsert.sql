﻿CREATE PROCEDURE [dbo].[ProvinceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Province]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceUpdate

/***** Object:  StoredProcedure  [dbo].[ProvinceUpdate] *****/
SET ANSI_NULLS ON