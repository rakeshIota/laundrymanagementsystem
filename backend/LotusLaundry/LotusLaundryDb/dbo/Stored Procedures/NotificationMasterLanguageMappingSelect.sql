﻿CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[LanguageId],
	 		LanguageXml=(
			  SELECT 
			   *
			   FROM Language language 
			   WHERE language.Id = R.[LanguageId] AND language.IsActive = 1 AND language.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	 		R.[Message]
	 	,
	 		R.[NotificationMasterId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [NotificationMasterLanguageMapping] R  
	WHERE 
	(
		@RelationTable IS NULL
		or
		(		
		@RelationTable = 'Language'
		and
		R.[LanguageId] = @RelationId
		)	
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END