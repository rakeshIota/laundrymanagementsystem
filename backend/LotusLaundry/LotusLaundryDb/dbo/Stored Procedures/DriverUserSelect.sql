﻿CREATE PROCEDURE [dbo].[DriverUserSelect]               
--declare            
@Id          BIGINT=NULL,              
@Role        NVARCHAR(20)=null,              
@next    INT = NULL,              
@offset   INT = NULL              
              
AS            
BEGIN              
            
  declare @temprole varchar(50)=null;          
 if(@Role='ROLE_ADMIN')          
 begin           
  set  @temprole='ROLE_ADMIN';          
  set @role=null;          
 end          
          
IF @next IS NULL              
BEGIN              
SET @next =100000              
SET @offset=1              
END              
              
SELECT               
       U.[Id],              
    U.[IsActive],              
    U.[IsDeleted],              
    U.[FirstName],              
    U.[LastName],              
    U.[Email],              
    U.[PhoneNumber],              
    U.[UserName],              
    U.[UniqueCode],              
    U.[TenantId],              
    R.[Name] AS RoleName,              
    case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'             
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'           
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'           
   end as Role             
      ,        
   U.Gender,        
   U.EmployeeId,        
   U.Position,        
   U.NationalId,        
   U.LicenceId,        
   U.BikeInformation,        
   U.LicencePlate,        
   U.DOB,        
    U.Status,       
           
      
  FileXml=(              
     SELECT               
      FG.[Id],              
      FG.[CreatedBy],              
      FG.[UpdatedBy],              
      FG.[CreatedOn],              
      FG.[UpdatedOn],              
      FG.[IsDeleted],              
      FG.[IsActive],              
      FG.[Filename],              
      FG.[MimeType],              
      FG.[Thumbnail],              
      FG.[Size],              
      FG.[Path],              
      FG.[OriginalName],              
      FG.[OnServer],              
      FG.[TypeId],              
      FG.[Type]              
      FROM FileGroupItems FG              
      WHERE FG.[TypeId] = U.[Id]  AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0              
      FOR XML AUTO,ROOT,ELEMENTs              
    )   ,      
      
     Order_Count=(select count(*) from [order] where [status]='AWAITING_COLLECTION' or [status]='AWAITING_COLLECTION_DELIVERY'or [status]='ON_THE_WAY' and driverid=U.id),  
      
      
   overall_count = COUNT (*) OVER()               
            
              
 FROM               
 Users U               
 INNER JOIN UserRoles UR ON U.Id = UR.UserId               
 INNER JOIN Roles R ON R.Id = UR.RoleId               
              
WHERE               
 (               
 @Id IS NULL               
 OR               
 U.Id=@Id               
 )               
              
 AND              
 (               
              
 R.Name ='ROLE_DRIVER'              
 )               
          
 AND              
 (               
 @temprole IS NULL               
  or                                         
     R.[Name] in (select [name] from roles where id in (1,5))          
               
 )          
              
 --AND              
 --(              
 -- U.IsDeleted = 0              
 --)              
              
   ORDER BY U.Id DESC               
   OFFSET (@next*@offset)-@next ROWS              
   FETCH NEXT @next ROWS ONLY              
              
END              
---------------------------------------------------              
---------------------------------------------------               
               
SET ANSI_NULLS ON