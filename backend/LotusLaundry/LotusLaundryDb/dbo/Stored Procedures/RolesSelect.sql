﻿CREATE PROCEDURE [dbo].[RolesSelect]                 
--declare       
@Id          BIGINT=null,                
@Role        NVARCHAR(20)=NULL,                
@next    INT = NULL,                
@offset   INT = NULL                
                
AS      
BEGIN                
                
IF @next IS NULL                
BEGIN                
SET @next =100000                
SET @offset=1                
END                
                
          
          
SELECT                 
      Id ,[Name],
	  case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'         
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'       
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'       
   end as Role  
	            
 from Roles  R             
                
WHERE                 
 (                 
 @Id IS NULL     
 OR                 
 R.Id=@Id                 
 )           
         
        
                
                  
                
               
                
   ORDER BY R.Id DESC                 
   OFFSET (@next*@offset)-@next ROWS                
   FETCH NEXT @next ROWS ONLY                
                
END