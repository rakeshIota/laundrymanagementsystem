﻿CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DefaultMessageId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DefaultMessageLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[DefaultMessageId],[LanguageId],[Message]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@DefaultMessageId,@LanguageId,@Message
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[DefaultMessageLanguageMappingUpdate] *****/
SET ANSI_NULLS ON