﻿CREATE PROCEDURE [dbo].[FeedbackInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@Rating    BIGINT=NULL,
		  	@DriverId    BIGINT=NULL,
		  	@UserId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Feedback]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Type],[Message],[Rating],[DriverId],[UserId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Type,@Message,@Rating,@DriverId,@UserId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackUpdate

/***** Object:  StoredProcedure  [dbo].[FeedbackUpdate] *****/
SET ANSI_NULLS ON