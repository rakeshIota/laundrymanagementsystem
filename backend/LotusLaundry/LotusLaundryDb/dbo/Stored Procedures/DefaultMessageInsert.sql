﻿CREATE PROCEDURE [dbo].[DefaultMessageInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Type NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DefaultMessage]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Type]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Type
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageUpdate

/***** Object:  StoredProcedure  [dbo].[DefaultMessageUpdate] *****/
SET ANSI_NULLS ON