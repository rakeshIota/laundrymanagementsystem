﻿CREATE PROCEDURE [dbo].[PromotionInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,

		    @Title  NVARCHAR(MAX)=NULL,
			@url NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Promotion]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Title],[url]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Title,@url
	  )
	SELECT SCOPE_IDENTITY()
 END