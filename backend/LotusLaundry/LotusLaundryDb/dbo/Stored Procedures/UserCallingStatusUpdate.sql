﻿-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UserCallingStatusUpdate]   
 -- Add the parameters for the stored procedure here  
 @FromUserId BIGINT = NULL,  
 @ToUserId BIGINT = NULL,  
 @IsActive BIT = NULL  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  

 declare @callStatus varchar(20);
 if(@IsActive=1)
 begin 
 set @callStatus='BUSY'
 end
 else
 begin 
 set @callStatus='BUSY'

 end
  
    -- Insert statements for procedure here  
  IF EXISTS (Select Id from users Where id = @FromUserId and callStatus='BUSY')  
  BEGIN  
    Update users   
    SET   
    callStatus = 'IDLE'  
    WHERE   
    id = @FromUserId  
     
  END  
  ELSE  
  BEGIN  
   Update users   
    SET   
    callStatus = 'BUSY'  
    WHERE   
    id = @FromUserId 
  END  
   
   IF EXISTS (Select Id from users Where id = @ToUserId and callStatus='BUSY')  
  BEGIN  
    Update users   
    SET   
    callStatus = 'IDLE'  
    WHERE   
    id = @ToUserId  
     
  END  
  ELSE  
  BEGIN  
   Update users   
    SET   
    callStatus = 'BUSY'  
    WHERE   
    id = @ToUserId 
  END 


END