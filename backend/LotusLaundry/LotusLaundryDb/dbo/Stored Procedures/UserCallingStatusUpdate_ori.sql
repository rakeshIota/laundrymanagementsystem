﻿-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[UserCallingStatusUpdate_ori]   
 -- Add the parameters for the stored procedure here  
 @FromUserId BIGINT = NULL,  
 @ToUserId BIGINT = NULL,  
 @IsActive BIT = NULL  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  IF EXISTS (Select Id from UserTwilioCallingStatus Where FromId = @FromUserId AND ToId = @ToUserId)  
  BEGIN  
       Update UserTwilioCallingStatus   
    SET   
    IsActive = @IsActive  
    WHERE   
    FromId = @FromUserId  
    AND  
    ToId = @ToUserId   
  END  
  ELSE  
  BEGIN  
   INSERT INTO UserTwilioCallingStatus  
   (  
    FromId,  
    ToId,  
    IsActive   
   )  
   Values  
   (  
    @FromUserId,  
    @ToUserId,  
    @IsActive   
   )  
  END  
    
END