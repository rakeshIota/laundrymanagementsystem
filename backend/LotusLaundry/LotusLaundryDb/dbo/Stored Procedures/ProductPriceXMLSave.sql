﻿CREATE PROCEDURE [dbo].[ProductPriceXMLSave]
 @ProductPriceXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(100)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price'
  FROM 
  
	@ProductPriceXml.nodes('/ArrayOfProductPriceModel/ProductPriceModel') AS NDS(DT)
   )
   MERGE INTO ProductPrice R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),R.[Price] =ISNULL(x.[Price] ,R.[Price])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[ServiceId],
		[ProductId],
		[Price]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[ServiceId],x.[ProductId],x.[Price]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeInsert

/***** Object:  StoredProcedure [dbo].[DeliveryTypeInsert] *****/
SET ANSI_NULLS ON