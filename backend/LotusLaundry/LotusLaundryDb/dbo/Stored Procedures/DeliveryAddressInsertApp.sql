﻿    
CREATE PROCEDURE [dbo].[DeliveryAddressInsertApp](          
          
   @TenantId   INT=NULL,          
   @Slug NVARCHAR(MAX)=NULL,          
     @CreatedBy    BIGINT=NULL,          
     @IsActive    BIT=NULL,          
     @UserId    BIGINT=NULL,          
   @AddressLine1 NVARCHAR(MAX)=NULL,          
   @AddressLine2 NVARCHAR(MAX)=NULL,          
     @DistrictId    BIGINT=NULL,          
     @SubDistrictId    BIGINT=NULL,          
   @PostalCode NVARCHAR(MAX)=NULL,          
     @IsDefault    BIT=NULL,          
   @Tag NVARCHAR(MAX)=NULL,          
     @ProvinceId    BIGINT=NULL,          
   @Latitude    DECIMAL(18,6)=NULL,          
   @Longitude    DECIMAL(18,6)=NULL,          
   @Type        NVARCHAR(100)=NULL,          
   @HouseNumber NVARCHAR(100)=NULL,          
   @StreetNumber NVARCHAR(100)=NULL,          
   @Note         NVARCHAR(MAX)=NULL  ,        
        
   @BuildingName NVARCHAR(100)=NULL,         
   @Floor NVARCHAR(100)=NULL,         
   @UnitNo NVARCHAR(100)=NULL,         
   @PhoneNo NVARCHAR(100)=NULL,         
   @alterNumber NVARCHAR(100)=NULL,        
   @ResidenceType   NVARCHAR(100)=NULL ,    
   @OrderId bigint =null    
        
)          
AS           
BEGIN          
 declare @result decimal;  
SET NOCOUNT ON;          
   INSERT INTO [DeliveryAddress]          
   (          
    [TenantId],          
    [Slug],          
    [CreatedBy],          
    [UpdatedBy],          
    [IsActive],          
    [UserId],          
    [AddressLine1],          
    [AddressLine2],          
    [DistrictId],          
    [SubDistrictId],          
    [PostalCode],          
    [IsDefault],          
    [Tag],          
    [ProvinceId],          
    [Latitude],          
    [Longitude],          
    [Type],          
    [HouseNumber],          
    [StreetNumber],          
    [Note]  ,        
    [BuildingName] ,         
   [Floor] ,        
   [UnitNo] ,        
   [PhoneNo] ,        
   alterNumber ,        
   [ResidenceType]  ,    
   [OrderId]    
   )          
   VALUES          
   (           
    @TenantId,          
    @Slug,          
    @CreatedBy,          
    @CreatedBy,          
    @IsActive,          
    @UserId,          
    @AddressLine1,          
    @AddressLine2,          
    @DistrictId,          
    @SubDistrictId,          
    @PostalCode,          
    @IsDefault,          
    @Tag,          
    @ProvinceId,          
    @Latitude,          
    @Longitude,          
    @Type,          
    @HouseNumber,          
    @StreetNumber,          
    @Note  ,        
        
        
 @BuildingName ,         
   @Floor ,        
   @UnitNo ,        
   @PhoneNo ,        
   @alterNumber ,        
   @ResidenceType ,    
   @OrderId    
   )          
              
   
  SELECT @result=SCOPE_IDENTITY()      
   declare @Lognote varchar(max)='Delivery Address Saved for the order';  
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )    
      VALUES (@TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Lognote)  
    
 select cast (@result as decimal)  
  
  
 END          
           
           
          
---------------------------------------------------          
---------------------------------------------------          
-- Procedure : UserAddressUpdate          
          
/***** Object:  StoredProcedure  [dbo].[UserAddressUpdate] *****/          
SET ANSI_NULLS ON