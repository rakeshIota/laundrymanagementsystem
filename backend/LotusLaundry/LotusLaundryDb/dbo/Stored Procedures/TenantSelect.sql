﻿CREATE PROCEDURE [dbo].[TenantSelect]
@UniqueId UNIQUEIDENTIFIER = NULL
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
Id,
Name,
[Address],
PhoneNo,
Email,
UniqueId
FROM 
Tenant 
WHERE 
(
@UniqueId IS NULL
OR
UniqueId=@UniqueId
)
AND
IsDeleted=0
END
---------------------------------------------------
---------------------------------------------------
SET ANSI_NULLS ON