﻿CREATE PROCEDURE [dbo].[RatingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@orderId bigint=null
	 	    ,
	 		@Quality bigint =null
	     	,
	 		@DeliveryService bigint =null
			,
	 	    @Staff bigint =null,
	 		@Packaging  bigint =null
			,
			@OverallSatisfaction  bigint =null,
	 		@Feedback  nvarchar(max) =null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [rating]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	
				[orderid] = ISNULL(@orderId,orderid),
				[quality] = ISNULL(@Quality,quality),
				[DeliveryService] = ISNULL(@DeliveryService,DeliveryService),
				[Staff] = ISNULL(@Staff,Staff),
				[Packaging] = ISNULL(@Packaging,Packaging),
				[OverallSatisfaction] = ISNULL(@OverallSatisfaction,OverallSatisfaction),

				[Feedback] = ISNULL(@Feedback,Feedback)


	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyXMLSave