﻿CREATE PROCEDURE [dbo].[CityInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@StateId    BIGINT=NULL,
			@Name       NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [City]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[StateId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@StateId,
	   @Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CityUpdate

/***** Object:  StoredProcedure  [dbo].[CityUpdate] *****/
SET ANSI_NULLS ON