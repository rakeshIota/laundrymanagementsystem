﻿CREATE PROCEDURE [dbo].[StateInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CountryId    BIGINT=NULL,
			@Name        NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [State]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CountryId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CountryId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : StateUpdate

/***** Object:  StoredProcedure  [dbo].[StateUpdate] *****/
SET ANSI_NULLS ON