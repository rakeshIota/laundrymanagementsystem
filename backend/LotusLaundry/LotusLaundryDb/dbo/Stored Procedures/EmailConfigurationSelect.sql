﻿CREATE PROCEDURE [dbo].[EmailConfigurationSelect]
	@ConfigurationKey NVARCHAR(200)=NULL
AS
BEGIN
	SELECT * FROM EmailConfiguration WHERE ConfigurationKey=@ConfigurationKey AND IsDeleted=0
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : FeedbackSelect

/***** Object:  StoredProcedure [dbo].[FeedbackSelect] ***/