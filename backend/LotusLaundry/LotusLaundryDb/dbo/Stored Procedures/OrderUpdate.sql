﻿CREATE PROCEDURE [dbo].[OrderUpdate]                
 --declare   
   @Id    BIGINT=NULL,                
   @TenantId   INT=NULL,                
   @Slug NVARCHAR(MAX)=NULL,                
     @UpdatedBy    BIGINT=NULL,                
     @IsDeleted    BIT=NULL,                
     @IsActive    BIT=NULL,                
     @UserId    BIGINT=NULL,                
   @DeliveryDate  DATETIMEOFFSET(7)=NULL,                
     @DeliveryType    nvarchar(100)=NULL,                
   @TotalItems   INT=NULL,                
   @DeliveryPrice    DECIMAL(18,2)=NULL,                
     @DriverId    BIGINT=NULL,                
   @Status NVARCHAR(MAX)=NULL,                
   @PaymentType NVARCHAR(MAX)=NULL,                
   @PaidAmount    DECIMAL(18,2)=NULL,                
   @Tax    DECIMAL(18,2)=NULL,                
   @SubTotal    DECIMAL(18,2)=NULL,                
   @TotalPrice    DECIMAL(18,2)=NULL,                
     @AdressId    BIGINT=NULL,                
     @ExpectedPickUpMin    BIGINT=NULL,                
     @ExpectedPickUpMax    BIGINT=NULL,                
     @ExpectedDeliveryMin    BIGINT=NULL,                
     @ExpectedDeliveryMax    BIGINT=NULL,                
     @CartId    BIGINT=NULL ,              
  @Deliveryaddress nvarchar(max)=null  ,            
            
              
    @PickupDate DATETIMEOFFSET(7)=NULL,            
    @deliverySlot NVARCHAR(100)=NULL,              
   @pickupSlot NVARCHAR(100)=NULL  ,          
  @LogisticCharge DECIMAL(18,2)=NULL,          
  @Discount     DECIMAL(18,2)=NULL ,          
 @paymentResponse nvarchar(100)=null ,         
    @paymentStatus nvarchar(100)=null   ,      
 @change  DECIMAL(18,2)=NULL ,        
 @note nvarchar(max) ,     
    @AdminNote nvarchar(max)         
AS                
BEGIN                
 -- SET NOCOUNT ON added to prevent extra result sets from                
 -- interfering with SELECT statements.                
 SET NOCOUNT ON;                
                
     if not exists (select * from [Order] where id=@Id and Status='DELIVERED')  
     begin  
    UPDATE [Order]                
 SET                
     [Slug] = ISNULL(@Slug,[Slug]),                
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),                
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),                 
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),                
     [IsActive] = ISNULL(@IsActive,[IsActive]),                
     [UserId] = ISNULL(@UserId,[UserId]),                
     [DeliveryDate] = ISNULL(@DeliveryDate,[DeliveryDate]),                
     [DeliveryType] = ISNULL(@DeliveryType,[DeliveryType]),                
     [TotalItems] = ISNULL(@TotalItems,[TotalItems]),                
     [DeliveryPrice] = ISNULL(@DeliveryPrice,[DeliveryPrice]),                
     [DriverId] = ISNULL(@DriverId,[DriverId]),                
     [Status] = ISNULL(@Status,[Status]),                
     [PaymentType] = ISNULL(@PaymentType,[PaymentType]),                
     [PaidAmount] = ISNULL(@PaidAmount,[PaidAmount]),                
     [Tax] = ISNULL(@Tax,[Tax]),                
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),                
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),                
                
     [ExpectedPickUpMin] = ISNULL(@ExpectedPickUpMin,[ExpectedPickUpMin]),                
     [ExpectedPickUpMax] = ISNULL(@ExpectedPickUpMax,[ExpectedPickUpMax]),                
     [ExpectedDeliveryMin] = ISNULL(@ExpectedDeliveryMin,[ExpectedDeliveryMin]),                
     [ExpectedDeliveryMax] = ISNULL(@ExpectedDeliveryMax,[ExpectedDeliveryMax]),                
     [CartId] = ISNULL(@CartId,[CartId]) ,              
     [Deliveryaddress]=isnull(@Deliveryaddress,[Deliveryaddress])  ,            
            
             
    PickupDate =isnull(@PickupDate,PickupDate)  ,            
    deliverySlot=isnull(@deliverySlot,deliverySlot)  ,             
    pickupSlot =isnull(@pickupSlot,pickupSlot) ,             
    LogisticCharge=isnull(@LogisticCharge,LogisticCharge) ,          
  Discount=isnull(@Discount,Discount) ,          
  paymentResponse=isnull(@paymentResponse,paymentResponse) ,        
   paymentStatus=isnull(@paymentStatus,paymentStatus)  ,      
   change=isnull(@change,change)  ,      
note=isnull(@note,note)  ,    
    adminNote=isnull(@AdminNote,AdminNote)    
      
      
  WHERE                
  (                
   [Id]=@Id                
  )                
  AND                
  (                
  [TenantId] =  @TenantId                
  )      
  
     declare @lognote varchar(max)='Order updated By Admin current staus id '+@status  
     INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )    
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,1,@Id,@lognote) 

  end  
END                
                
                
---------------------------------------------------                
---------------------------------------------------                
-- Procedure : OrderXMLSave  