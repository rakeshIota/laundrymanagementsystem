﻿CREATE PROCEDURE [dbo].[ProductPriceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ServiceId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [ProductPrice]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[ProductId] = ISNULL(@ProductId,[ProductId]),
			 	[Price] = ISNULL(@Price,[Price])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductPriceXMLSave