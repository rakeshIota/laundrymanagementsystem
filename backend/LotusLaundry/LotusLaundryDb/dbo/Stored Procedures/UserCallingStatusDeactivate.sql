﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserCallingStatusDeactivate] 
	-- Add the parameters for the stored procedure here
	  @UserId BIGINT= NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Update UserTwilioCallingStatus 
	SET
	IsActive = 0 
	Where 
	FromId = @UserId OR ToId = @UserId

END