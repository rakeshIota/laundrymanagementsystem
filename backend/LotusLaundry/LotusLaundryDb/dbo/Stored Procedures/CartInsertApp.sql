﻿CREATE PROCEDURE [dbo].[CartInsertApp](  
  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @CreatedBy    BIGINT=NULL,  
     @IsActive    BIT=NULL,  
     @IsLocked    BIT=NULL,  
     @CustomerId    BIGINT=NULL,  
   @Status NVARCHAR(MAX)=NULL,  
   @Tax    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
   @TotalItems   INT=NULL,  
   @DeliveryType  NVARCHAR(100)=NULL , 

    @DeliveryDate DATETIMEOFFSET(7)=NULL,
    @PickupDate DATETIMEOFFSET(7)=NULL,
    @deliverySlot NVARCHAR(100)=NULL,  
   @pickupSlot NVARCHAR(100)=NULL
   
)  
AS   
BEGIN  
SET NOCOUNT ON;  
   INSERT INTO [Cart]  
   (  
    [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[IsLocked],[CustomerId],[Status],[Tax],[SubTotal],[TotalPrice],[TotalItems],[DeliveryType],[DeliveryDate],[PickupDate],[deliverySlot],[pickupSlot]  
   )  
   VALUES  
   (   
    @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@IsLocked,@CustomerId,@Status,@Tax,@SubTotal,@TotalPrice,@TotalItems,@DeliveryType ,@DeliveryDate,@PickupDate,@deliverySlot,@pickupSlot 
   )  
 SELECT SCOPE_IDENTITY()  
 END  
   
   
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : CartUpdate  
  
/***** Object:  StoredProcedure  [dbo].[CartUpdate] *****/  
SET ANSI_NULLS ON