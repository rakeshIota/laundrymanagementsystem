﻿CREATE PROCEDURE [dbo].[OrderLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Note NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Note
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderLogUpdate

/***** Object:  StoredProcedure  [dbo].[OrderLogUpdate] *****/
SET ANSI_NULLS ON