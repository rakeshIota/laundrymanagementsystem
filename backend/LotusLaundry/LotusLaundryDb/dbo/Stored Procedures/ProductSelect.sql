﻿CREATE PROCEDURE [dbo].[ProductSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	 		R.[Gender]
	 	,
		R.[Name],
	overall_count = COUNT(*) OVER(),
	ProductLanguageXml = (
		select
		productlanguagemapping.Id,
		productlanguagemapping.Name,
		productlanguagemapping.LanguageId,
		productlanguagemapping.ProductId,
		[language].Name AS LanguageName

		FROM 
		ProductLanguageMapping productlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = productlanguagemapping.LanguageId
		WHERE productlanguagemapping.ProductId =  R.Id AND productlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	),
	ProductPriceXml = (
		select
		productprice.Id,
		productprice.Price,
		productprice.ProductId,
		productprice.ServiceId,
		[service].Name AS ServiceName

		FROM 
		ProductPrice productprice INNER JOIN 
		[Service] [service] ON [service].Id =  productprice.[ServiceId]
		WHERE productprice.[ProductId] =  R.[Id] AND productprice.[IsDeleted] = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Product] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ProductLanguageMappingSelect] ***/