﻿CREATE PROCEDURE [dbo].[GetUserDeviceToken] 
	-- Add the parameters for the stored procedure here
	@userId BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT 
			DeviceId,
			DeviceToken,
			DeviceType 
		FROM 
			UserDevice 
		WHERE 
		(
			@userId IS NULL
			OR
			UserId = @userId 
		)
END