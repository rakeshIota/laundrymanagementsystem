﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserDeviceSelect]
	@UserId			BIGINT=NULL,
	@DeviceType		NVARCHAR(50)=NULL,
	@UserType		NVARCHAR(50)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM UserDevice
	WHERE
	IsDeleted=0
	AND
	(
		@UserId IS NULL
		OR
		UserId=@UserId
	)
	AND
	(
		@DeviceType IS NULL
		OR
		DeviceType=@DeviceType
	)
	AND
	(
		@UserType IS NULL
		OR
		UserType=@UserType
	)
END