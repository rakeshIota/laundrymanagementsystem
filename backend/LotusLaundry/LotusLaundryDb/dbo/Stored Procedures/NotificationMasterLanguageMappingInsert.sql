﻿CREATE PROCEDURE [dbo].[NotificationMasterLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		    @LanguageId BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@NotificationMasterId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMasterLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[LanguageId],[Message],[NotificationMasterId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@LanguageId,@Message,@NotificationMasterId
	  )
	SELECT SCOPE_IDENTITY()
 END