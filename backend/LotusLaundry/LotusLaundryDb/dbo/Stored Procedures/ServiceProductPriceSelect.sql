﻿CREATE PROCEDURE [dbo].[ServiceProductPriceSelect]  
--declare
  
 @LanguageId BIGINT=NULL,  
 @next   int = NULL,  
 @offset  int = NULL,  
 @TenantId   INT=NULL,  
 @Gender     NVARCHAR(30)=NULL,  
 @ServiceId  BIGINT=null  ,
 @productUd bigint=null 
AS
BEGIN  
  
IF @next IS NULL  
BEGIN  
SET @next =100000  
SET @offset=1  
END  
  
    SELECT  
 R.ProductId,  
 R.Price,  
 P.Name,  
 P.Gender,  
 ImageXml=(  
     SELECT   
      FG.[Id],  
      FG.[CreatedBy],  
      FG.[UpdatedBy],  
      FG.[CreatedOn],  
      FG.[UpdatedOn],  
      FG.[IsDeleted],  
      FG.[IsActive],  
      FG.[Filename],  
      FG.[MimeType],  
      FG.[Thumbnail],  
      FG.[Size],  
      FG.[Path],  
      FG.[OriginalName],  
      FG.[OnServer],  
      FG.[TypeId],  
      FG.[Type]  
      FROM FileGroupItems FG  
      WHERE FG.[TypeId] = R.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0  
      FOR XML AUTO,ROOT,ELEMENTs  
    ),  
  
 (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  R.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductName  
  
 FROM [ProductPrice] R INNER JOIN [Product] P ON   
 P.Id = R.ProductId AND P.IsDeleted = 0  AND P.IsActive = 1 AND (@Gender IS NULL OR P.Gender =  @Gender)   
 INNER JOIN [Service] S ON S.Id =  R.ServiceId AND S.IsDeleted = 0  
  
 WHERE  
 (  
  @ServiceId IS NULL  
  OR  
  R.ServiceId = @ServiceId  
 )  
  and
  (  
  @productUd IS NULL  
  OR  
  R.ProductId = @productUd  
 ) 
 AND  
 (  
  R.[IsDeleted] = 0  
 )  
 AND  
 (  
  @TenantId IS NULL  
  OR  
  R.[TenantId] = @TenantId  
 )  
   
 Order by R.Id desc  
 OFFSET (@next*@offset)-@next ROWS  
    FETCH NEXT @next ROWS ONLY  
  
END