﻿
CREATE PROCEDURE [dbo].[ProductSelectApp]
(
	@Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL,
	@Gender     NVARCHAR(30)=NULL,
	@ServiceId  BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

    SELECT
	R.[Id],
	R.[Name],
	R.[Gender],
	ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),

	(SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  R.Id AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductName,
	(SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ProductId =  R.Id AND PP.IsDeleted = 0 AND PP.ServiceId =@ServiceId ) AS Price

	FROM [Product] R

	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		@Gender  IS NULL
			OR
		R.[Gender] =  @Gender
	)
	AND
	(
		R.IsActive = 1
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END