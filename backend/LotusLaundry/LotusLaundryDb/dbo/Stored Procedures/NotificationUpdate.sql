﻿CREATE PROCEDURE [dbo].[NotificationUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL,
			@Token NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Response NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Notification]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Message] = ISNULL(@Message,[Message]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Type] = ISNULL(@Type,[Type]),
			 	[Token] = ISNULL(@Token,[Token]),
			 	[Status] = ISNULL(@Status,[Status]),
			 	[Response] = ISNULL(@Response,[Response])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : NotificationXMLSave