﻿CREATE PROCEDURE [dbo].[CartWithItemDelete]    
     @Id    BIGINT=NULL,    
   @TenantId   INT=NULL,    
   @Slug NVARCHAR(MAX)=NULL,    
     @UpdatedBy    BIGINT=NULL,    
     @IsDeleted    BIT=NULL,    
     @IsActive    BIT=NULL,    
     @IsLocked    BIT=NULL,    
     @CustomerId    BIGINT=NULL,    
   @Status NVARCHAR(MAX)=NULL,    
   @Tax    DECIMAL(18,2)=NULL,    
   @SubTotal    DECIMAL(18,2)=NULL,    
   @TotalPrice    DECIMAL(18,2)=NULL,    
   @TotalItems   INT=NULL,    
   @DeliveryType  NVARCHAR(100)=NULL    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
  if exists (select * from Cart where id=@Id  and IsLocked=1 )  
begin     
   delete from cartitem where CartId=@Id  
   delete from Cart where id=@Id     

       
   declare @note varchar(max)='cart Deleted'
      INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )  
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,1,@Id,@note)


end  
      
END    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : CartXMLSave