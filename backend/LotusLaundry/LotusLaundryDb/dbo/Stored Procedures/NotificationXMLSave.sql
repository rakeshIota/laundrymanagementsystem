﻿CREATE PROCEDURE [dbo].[NotificationXMLSave]
 @NotificationXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(Message)[1]', 'NVARCHAR') AS 'Message',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(Type)[1]', 'NVARCHAR') AS 'Type',
		NDS.DT.value('(Token)[1]', 'NVARCHAR') AS 'Token',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(Response)[1]', 'NVARCHAR') AS 'Response'
  FROM 
	@NotificationXml.nodes('/root/Notification') AS NDS(DT)
   )
   MERGE INTO Notification R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[Message] =ISNULL(x.[Message] ,R.[Message]),R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),R.[Type] =ISNULL(x.[Type] ,R.[Type]),R.[Token] =ISNULL(x.[Token] ,R.[Token]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[Response] =ISNULL(x.[Response] ,R.[Response])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[Message],
		[LanguageId],
		[Type],
		[Token],
		[Status],
		[Response]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[Message],x.[LanguageId],x.[Type],x.[Token],x.[Status],x.[Response]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingInsert

/***** Object:  StoredProcedure [dbo].[AppSettingInsert] *****/
SET ANSI_NULLS ON