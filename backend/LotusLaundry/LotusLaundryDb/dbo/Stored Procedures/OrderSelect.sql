﻿CREATE PROCEDURE [dbo].[OrderSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[UserId]
	 	,
	 		R.[DeliveryDate]
	 	,
	 		R.[DeliveryType]
	 	,
	 		R.[TotalItems]
	 	,
	 		R.[DeliveryPrice]
	 	,
	 		R.[DriverId]
	 	,
	 		R.[Status]
	 	,
	 		R.[PaymentType]
	 	,
	 		R.[PaidAmount]
	 	,
	 		R.[Tax]
	 	,
	 		R.[SubTotal]
	 	,
	 		R.[TotalPrice]
	 	
	 	,
	 		R.[ExpectedPickUpMin]
	 	,
	 		R.[ExpectedPickUpMax]
	 	,
	 		R.[ExpectedDeliveryMin]
	 	,
	 		R.[ExpectedDeliveryMax]
	 	,
	 		R.[CartId]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Order] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemSelect

/***** Object:  StoredProcedure [dbo].[OrderItemSelect] ***/