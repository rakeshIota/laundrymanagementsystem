﻿CREATE PROCEDURE [dbo].[DistrictSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
		R.CityId,
		R.[Name],
		C.Name AS CityName,
	overall_count = COUNT(*) OVER(),
	CityLanguageXml = (
		select
		districtlanguagemapping.Id,
		districtlanguagemapping.Name,
		districtlanguagemapping.LanguageId,
		districtlanguagemapping.DistrictId,
		[language].Name AS LanguageName

		FROM 
		DistrictLanguageMapping districtlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = districtlanguagemapping.LanguageId
		WHERE districtlanguagemapping.DistrictId =  R.Id AND districtlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [District] R  
	INNER JOIN [City] C ON C.[Id] = R.[CityId] AND C.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[DistrictLanguageMappingSelect] ***/