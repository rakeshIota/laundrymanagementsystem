﻿CREATE PROCEDURE [dbo].[AppSettingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [AppSetting]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Key],[Value]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Key,@Value
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : AppSettingUpdate

/***** Object:  StoredProcedure  [dbo].[AppSettingUpdate] *****/
SET ANSI_NULLS ON