﻿CREATE PROCEDURE [dbo].[ServiceLanguageMappingXMLSave]
 @ServiceLanguageMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR(max)') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR(100)') AS 'Name',
		NDS.DT.value('(LanguageId)[1]', 'BIGINT') AS 'LanguageId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Description)[1]', 'NVARCHAR(MAX)') AS 'Description'

		
  FROM 
	@ServiceLanguageMappingXml.nodes('/ArrayOfServiceLanguageMappingModel/ServiceLanguageMappingModel') AS NDS(DT)
   )
   MERGE INTO ServiceLanguageMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),
	R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),
	R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),
	R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),
	R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),
	R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),
	R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),
	R.[Name] =ISNULL(x.[Name] ,R.[Name]),
	R.[LanguageId] =ISNULL(x.[LanguageId] ,R.[LanguageId]),
	R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),
	R.[Description] =  ISNULL(x.[Description],R.[Description])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[Name],
		[LanguageId],
		[ServiceId],
		[Description]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[Name],x.[LanguageId],x.[ServiceId],x.[Description]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductInsert

/***** Object:  StoredProcedure [dbo].[ProductInsert] *****/
SET ANSI_NULLS ON