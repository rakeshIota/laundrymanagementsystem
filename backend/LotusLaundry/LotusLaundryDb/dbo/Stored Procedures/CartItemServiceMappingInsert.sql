﻿CREATE PROCEDURE [dbo].[CartItemServiceMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CartItemServiceMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CartItemId],[ServiceId],[Quantity],[Price],[SubTotal],[TotalPrice]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CartItemId,@ServiceId,@Quantity,@Price,@SubTotal,@TotalPrice
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemServiceMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CartItemServiceMappingUpdate] *****/
SET ANSI_NULLS ON