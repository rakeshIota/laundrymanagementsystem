﻿CREATE PROCEDURE [dbo].[DistrictLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@DistrictId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DistrictLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[DistrictId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@DistrictId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[DistrictLanguageMappingUpdate] *****/
SET ANSI_NULLS ON