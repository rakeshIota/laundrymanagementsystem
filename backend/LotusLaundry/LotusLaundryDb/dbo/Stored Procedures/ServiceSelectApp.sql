﻿CREATE PROCEDURE [dbo].[ServiceSelectApp]
(
	@Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

    SELECT
	R.[Id],
	R.[Name],
	R.[Description],
	 ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='SERVICE_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),
	(SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  R.Id AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceName,
	(SELECT TOP(1) [Description] FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  R.Id AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS [ServiceDescription]

	FROM [Service] R

	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.IsActive = 1
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END