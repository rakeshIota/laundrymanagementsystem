﻿CREATE PROCEDURE [dbo].[NotificationMasterSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Type]
	 	,
	 		R.[CanBeDelete]
	 	,
			R.[Message],
	overall_count = COUNT(*) OVER(),
	NotificationMasterLanguageXml = (
		select
		notificationmasterlanguagemapping.Id,
		notificationmasterlanguagemapping.[Message],
		notificationmasterlanguagemapping.LanguageId,
		notificationmasterlanguagemapping.NotificationMasterId,
		[language].Name AS LanguageName

		FROM 
		NotificationMasterLanguageMapping notificationmasterlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = notificationmasterlanguagemapping.LanguageId
		WHERE notificationmasterlanguagemapping.NotificationMasterId =  R.Id AND notificationmasterlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	),
	NotificationMasterKeyValueXml = (
		select
		notificationmasterkeyvaluemapping.Id,
		notificationmasterkeyvaluemapping.[Key],
		notificationmasterkeyvaluemapping.Value,
		notificationmasterkeyvaluemapping.NotificationId
		

		FROM 
		NotificationMasterKeyValueMapping notificationmasterkeyvaluemapping 
		WHERE notificationmasterkeyvaluemapping.NotificationId =  R.Id AND notificationmasterkeyvaluemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [NotificationMaster] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END