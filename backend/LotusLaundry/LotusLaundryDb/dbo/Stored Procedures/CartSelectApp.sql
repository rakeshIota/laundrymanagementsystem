﻿    
CREATE PROCEDURE [dbo].[CartSelectApp](    
@Id         BIGINT =NULL,    
@Slug     NVARCHAR(255)=NULL,    
@next   int = NULL,    
@offset  int = NULL,    
@RelationTable NVARCHAR(50)=NULL,    
@RelationId bigint=NULL,    
@TenantId   INT=NULL,    
@LanguageId  BIGINT=NULL,    
@CustomerId      BIGINT=NULL    
)    
AS BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
 SELECT    
    R.[Id]    
   ,    
    R.[TenantId]    
   ,    
    R.[Slug]    
   ,    
    R.[CreatedBy]    
   ,    
    R.[UpdatedBy]    
   ,    
    R.[CreatedOn]    
   ,    
    R.[UpdatedOn]    
   ,    
    R.[IsDeleted]    
   ,    
    R.[IsActive]    
   ,    
    R.[IsLocked]    
   ,    
    R.[CustomerId]    
   ,    
    R.[Status]    
   ,    
    R.[Tax]    
   ,    
    R.[SubTotal]    
   ,    
    R.[TotalPrice]    
   ,    
    R.[TotalItems]    
   ,    
  R.[DeliveryType],    
  
  R.Deliverydate,  
  R.Pickupdate,  
  R.deliverySlot,  
  R.pickupSlot,  
  --select * FROM [Cart]  
  
 overall_count = COUNT(*) OVER(),    
 CartItemXml = (    
  select    
  cartitem.Id,    
  cartitem.ProductId,    
  cartitem.CartId,    
  cartitem.ServiceId,    
  --cartitem.TotalPrice, 
   pp.Price*cartitem.Quantity TotalPrice, 
  cartitem.Quantity,    
  product.Name  AS ProductName,    
  [service].Name  AS ServiceName,    
  (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  cartitem.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductNameL,    
  (SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  CartItem.ServiceId AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceNameL,    
  (SELECT TOP(1) [Path] FROM FileGroupItems FG WHERE FG.[TypeId] = cartitem.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0 ) AS [Path],    
  (SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ServiceId =  cartitem.ServiceId AND PP.ProductId =  cartitem.ProductId AND PP.[IsDeleted] = 0 ) AS Price    
      
    
  FROM     
  CartItem cartitem     
  INNER JOIN     
  Product product ON product.Id =  cartitem.ProductId    
  INNER JOIN    
  [Service] [service] ON [service].Id =  cartitem.ServiceId    
  left join 
  productprice pp on cartitem.productid =pp.productid and cartitem.serviceid=pp.serviceid    
  WHERE cartitem.CartId =  R.Id AND cartitem.IsDeleted = 0    
  FOR XML AUTO,ROOT,ELEMENTs    
 )    
 FROM [Cart] R      
 WHERE     
 (    
  @RelationTable IS NULL    
 )    
 AND    
 (    
  @Id IS NULL    
  OR    
  R.[Id] = @Id    
 )    
 AND    
 (    
  @Slug IS NULL    
  OR    
  R.[Slug] = @Slug    
 )    
 AND    
 (    
  R.[IsDeleted] = 0    
 )    
 AND    
 (    
  @TenantId IS NULL    
  OR    
  R.[TenantId] = @TenantId    
 )    
 AND    
 (    
  @CustomerId IS NULL    
   OR    
  R.[CustomerId] =  @CustomerId    
 )    
    
 Order by R.Id desc    
 OFFSET (@next*@offset)-@next ROWS    
    FETCH NEXT @next ROWS ONLY    
    
END    
    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : CartItemSelect    
    
/***** Object:  StoredProcedure [dbo].[CartItemSelect] ***/