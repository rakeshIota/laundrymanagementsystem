﻿CREATE PROCEDURE [dbo].[OrderXMLSave]
 @OrderXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(DeliveryDate)[1]', 'DATETIMEOFFSET(7)') AS 'DeliveryDate',
		NDS.DT.value('(DeliveryType)[1]', 'BIGINT') AS 'DeliveryType',
		NDS.DT.value('(TotalItems)[1]', 'INT') AS 'TotalItems',
		NDS.DT.value('(DeliveryPrice)[1]', 'DECIMAL(18,2)') AS 'DeliveryPrice',
		NDS.DT.value('(DriverId)[1]', 'BIGINT') AS 'DriverId',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(PaymentType)[1]', 'NVARCHAR') AS 'PaymentType',
		NDS.DT.value('(PaidAmount)[1]', 'DECIMAL(18,2)') AS 'PaidAmount',
		NDS.DT.value('(Tax)[1]', 'DECIMAL(18,2)') AS 'Tax',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		
		NDS.DT.value('(ExpectedPickUpMin)[1]', 'BIGINT') AS 'ExpectedPickUpMin',
		NDS.DT.value('(ExpectedPickUpMax)[1]', 'BIGINT') AS 'ExpectedPickUpMax',
		NDS.DT.value('(ExpectedDeliveryMin)[1]', 'BIGINT') AS 'ExpectedDeliveryMin',
		NDS.DT.value('(ExpectedDeliveryMax)[1]', 'BIGINT') AS 'ExpectedDeliveryMax',
		NDS.DT.value('(CartId)[1]', 'BIGINT') AS 'CartId'
  FROM 
	@OrderXml.nodes('/root/Order') AS NDS(DT)
   )
   MERGE INTO [Order] R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[DeliveryDate] =ISNULL(x.[DeliveryDate] ,R.[DeliveryDate]),R.[DeliveryType] =ISNULL(x.[DeliveryType] ,R.[DeliveryType]),R.[TotalItems] =ISNULL(x.[TotalItems] ,R.[TotalItems]),R.[DeliveryPrice] =ISNULL(x.[DeliveryPrice] ,R.[DeliveryPrice]),R.[DriverId] =ISNULL(x.[DriverId] ,R.[DriverId]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[PaymentType] =ISNULL(x.[PaymentType] ,R.[PaymentType]),R.[PaidAmount] =ISNULL(x.[PaidAmount] ,R.[PaidAmount]),R.[Tax] =ISNULL(x.[Tax] ,R.[Tax]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),R.[ExpectedPickUpMin] =ISNULL(x.[ExpectedPickUpMin] ,R.[ExpectedPickUpMin]),R.[ExpectedPickUpMax] =ISNULL(x.[ExpectedPickUpMax] ,R.[ExpectedPickUpMax]),R.[ExpectedDeliveryMin] =ISNULL(x.[ExpectedDeliveryMin] ,R.[ExpectedDeliveryMin]),R.[ExpectedDeliveryMax] =ISNULL(x.[ExpectedDeliveryMax] ,R.[ExpectedDeliveryMax]),R.[CartId] =ISNULL(x.[CartId] ,R.[CartId])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[DeliveryDate],
		[DeliveryType],
		[TotalItems],
		[DeliveryPrice],
		[DriverId],
		[Status],
		[PaymentType],
		[PaidAmount],
		[Tax],
		[SubTotal],
		[TotalPrice],
		
		[ExpectedPickUpMin],
		[ExpectedPickUpMax],
		[ExpectedDeliveryMin],
		[ExpectedDeliveryMax],
		[CartId]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[DeliveryDate],x.[DeliveryType],x.[TotalItems],x.[DeliveryPrice],x.[DriverId],x.[Status],x.[PaymentType],x.[PaidAmount],x.[Tax],x.[SubTotal],x.[TotalPrice],x.[ExpectedPickUpMin],x.[ExpectedPickUpMax],x.[ExpectedDeliveryMin],x.[ExpectedDeliveryMax],x.[CartId]
    );
END