﻿CREATE PROCEDURE [dbo].[CustomerSelect]     
--declare  
@Id          BIGINT=NULL,    
@Role        NVARCHAR(20)=NULL,    
@next    INT = NULL,    
@offset   INT = NULL    
    
AS  
BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
SELECT     
       U.[Id],    
    U.[IsActive],    
    U.[IsDeleted],    
    U.[FirstName],    
    U.[LastName],    
    U.[Email],    
    U.[PhoneNumber],    
    U.[UserName],    
    U.[UniqueCode],    
    U.[TenantId],    
    R.[Name] AS RoleName,    
 case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'   
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER' end as Role   
      , overall_count = COUNT (*) OVER()     
  ,U.EmailConfirmed,U.PhoneNumberConfirmed,U.LanguageId,U.Status,U.NickName,U.CountryCode,U.DOB,U.Gender
  --select * from Users
  ,FileXml=(        
     SELECT         
      FG.[Id],        
      FG.[CreatedBy],        
      FG.[UpdatedBy],        
      FG.[CreatedOn],        
      FG.[UpdatedOn],        
      FG.[IsDeleted],        
      FG.[IsActive],        
      FG.[Filename],        
      FG.[MimeType],        
      FG.[Thumbnail],        
      FG.[Size],        
      FG.[Path],        
      FG.[OriginalName],        
      FG.[OnServer],        
      FG.[TypeId],        
      FG.[Type]        
      FROM FileGroupItems FG        
      WHERE FG.[TypeId] = U.[Id] AND FG.[Type]='USERS_PROFILEPIC' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0        
      FOR XML AUTO,ROOT,ELEMENTs        
    )
    
 FROM     
 Users U     
 INNER JOIN UserRoles UR ON U.Id = UR.UserId     
 INNER JOIN Roles R ON R.Id = UR.RoleId     
    
WHERE     
 (     
 @Id IS NULL     
 OR     
 U.Id=@Id     
 )     
    
 AND    
 (     
 @Role IS NULL     
 OR     
 R.Name = @Role    
 )     
    
 AND    
 (    
  U.IsDeleted = 0    
 )    
    
   ORDER BY U.Id DESC     
   OFFSET (@next*@offset)-@next ROWS    
   FETCH NEXT @next ROWS ONLY    
    
END    
---------------------------------------------------    
---------------------------------------------------     
     
SET ANSI_NULLS ON