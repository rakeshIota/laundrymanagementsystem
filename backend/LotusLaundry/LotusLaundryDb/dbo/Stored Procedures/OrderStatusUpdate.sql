﻿CREATE PROCEDURE [dbo].[OrderStatusUpdate]            
   @Id    BIGINT=NULL,    
   @TenantId INT=null,  
   @UpdatedBy    BIGINT=NULL,                             
   @UserId    BIGINT=NULL,                 
   @Status NVARCHAR(MAX)=NULL                 
AS            
BEGIN            
 -- SET NOCOUNT ON added to prevent extra result sets from            
 -- interfering with SELECT statements.            
 SET NOCOUNT ON;            
            
    UPDATE [Order]            
 SET            
       
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),            
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),                    
       
     [Status] = ISNULL(@Status,[Status])            
   
  WHERE            
  (            
   [Id]=@Id            
  )            
  AND            
  (            
  [TenantId] =  @TenantId            
  )     
  
  if(@Status='COLLECTED')
   insert into DriverRideLogs ([StartOn],[CompleteOn],[IsComplete],[DriverId],[OrderId],[Type]) values 
   ( switchoffset(sysdatetimeoffset(),'+00:00'),switchoffset(sysdatetimeoffset(),'+00:00'),1,@updatedby,@id,'PICKUP')
  if(@Status='DELIVERED')
    insert into DriverRideLogs ([StartOn],[CompleteOn],[IsComplete],[DriverId],[OrderId],[Type]) values 
   ( switchoffset(sysdatetimeoffset(),'+00:00'),switchoffset(sysdatetimeoffset(),'+00:00'),1,@updatedby,@id,'DELIVERED')

END            
            
            
---------------------------------------------------            
---------------------------------------------------            
-- Procedure : OrderXMLSave 