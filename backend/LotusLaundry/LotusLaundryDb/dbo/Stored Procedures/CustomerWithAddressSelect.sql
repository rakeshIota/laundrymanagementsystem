﻿CREATE PROCEDURE [dbo].[CustomerWithAddressSelect]                                 
--declare                              
@Id          BIGINT=NULL,                                
@Role        NVARCHAR(20)=null,                                
@next    INT = NULL,                                
@offset   INT = NULL,                           
@RelationTable NVARCHAR(50)=NULL,                              
@RelationId bigint=NULL ,               
@SortColumn nvarchar(200)=null  ,
   @SortOrder            nvarchar(200)=null                                 
AS                              
BEGIN                                
                              
  declare @temprole varchar(50)=null;                            
 if(@Role='ROLE_ADMIN')                            
 begin                             
  set  @temprole='ROLE_ADMIN';                            
  set @role=null;                            
 end                            
                            
IF @next IS NULL                                
BEGIN                                
SET @next =100000                                
SET @offset=1                                
END                                
                              
SELECT                                 
    U.[Id],                                
    U.[IsActive],                                
    U.[IsDeleted],                                
    U.[FirstName],                                
    U.[LastName],                                
    U.[Email],                                
    U.[PhoneNumber],                                
    U.[UserName],                                
    U.[UniqueCode],                                
    U.[TenantId],                  
    U.adminNote,             
    R.[Name] AS RoleName,                                
    case when R.[Name]='ROLE_ADMIN' then 'ADMIN' when R.[Name]='ROLE_SUPER_ADMIN' then 'SUPER ADMIN'                               
      when R.[Name]='ROLE_CUSTOMER' then 'CUSTOMER' when R.[Name]='ROLE_DRIVER' then 'DRIVER'                             
   when R.[Name]='ROLE_OPERATION' then 'OPERATION MANAGER'                             
   end as Role                               
      ,                          
   U.Gender,                          
   U.EmployeeId,                          
   U.Position,                          
   U.NationalId,                          
   U.LicenceId,                          
   U.BikeInformation,                          
   U.LicencePlate,                          
   U.DOB,                          
    U.Status,                         
    U.NickName,                     
    U.EmailConfirmed,                  
                
 U.PhoneNumberConfirmed,            
            
 U.languageid,            
 --select * from users             
            
 AddressXml=(                              
     SELECT                  
    R.[Id],                  
    R.[AddressLine1],                  
   R.[AddressLine2],                  
    R.[DistrictId],                  
    R.[SubDistrictId],                  
    R.[PostalCode],                  
    R.[Tag],                  
    R.[Latitude],                  
    R.[Longitude],                  
   R.[HouseNumber],                  
   R.[StreetNumber],                  
   R.[Note],                  
   R.[Type],                  
                  
                
   R.[BuildingName] ,                
   R.[Floor] ,                
   R.[UnitNo] ,                
   R.[PhoneNo] ,                
   R.alterNumber ,                
   R.[ResidenceType] ,                
                
                     
   D.Name  AS DistrictName,                  
   SD.Name  AS SubDistrictName,        
    PD.Name As ProvinceName,        
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =U.LanguageId ) AS DistrictNameL,                  
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =U.LanguageId  ) AS SubDistrictNameL            ,      
      (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =U.LanguageId ) AS ProvinceNameL                  
                
                  
 FROM [UserAddress] R INNER JOIN                  
 [District] D ON D.Id =  R.[DistrictId]                  
 INNER JOIN                   
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId      
 INNER JOIN                   
 Province PD ON PD.Id =  R.ProvinceId      
      
      
 where R.userId=U.ID            
      FOR XML AUTO,ROOT,ELEMENTs                              
    )               
 ,U.ProfilePic              
 ,FileXml=(                          
     SELECT                           
      FG.[Id],                          
      FG.[CreatedBy],                          
      FG.[UpdatedBy],                          
      FG.[CreatedOn],                          
      FG.[UpdatedOn],                          
      FG.[IsDeleted],                          
      FG.[IsActive],                          
      FG.[Filename],                          
      FG.[MimeType],                          
      FG.[Thumbnail],                      
      FG.[Size],                          
      FG.[Path],                          
      FG.[OriginalName],                          
      FG.[OnServer],                          
      FG.[TypeId],              
      FG.[Type]                          
      FROM FileGroupItems FG                          
      WHERE FG.[TypeId] = U.Id AND FG.[Type]='USERS_PROFILEPIC' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0                          
      FOR XML AUTO,ROOT,ELEMENTs                          
    ) ,                
                
   overall_count = COUNT (*) OVER()                                 
                              
                               
 FROM                                 
 Users U                                 
 INNER JOIN UserRoles UR ON U.Id = UR.UserId                                 
 INNER JOIN Roles R ON R.Id = UR.RoleId                                 
                                
WHERE                                 
 (                                 
 @Id IS NULL                                 
 OR                                 
 U.Id=@Id                                 
 )                                 
                                
 AND                                
 (                                 
 @Role IS NULL                                 
 OR                                 
 R.Name = @Role                                
 )                                 
                            
 AND                                
 (                                 
 @temprole IS NULL                                 
  or                                                           
     R.[Name] in (select [name] from roles where id in (1,5))                            
                                 
 )                            
 and                   
U.isdeleted=0                  
            
			
			ORDER BY  
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='ASC')   THEN [FirstName]   END ASC,  
            CASE WHEN (@SortColumn = 'Name' AND @SortOrder='DESC')  THEN [FirstName]    END DESC,  

            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='ASC')   THEN NickName   END ASC,  
            CASE WHEN (@SortColumn = 'NICK_NAME' AND @SortOrder='DESC')  THEN NickName    END DESC,  
            
			CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='ASC')   THEN PhoneNumber   END ASC,  
            CASE WHEN (@SortColumn = 'PHONE_NUMBER' AND @SortOrder='DESC')  THEN PhoneNumber    END DESC, 

			CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='ASC')   THEN email   END ASC,  
            CASE WHEN (@SortColumn = 'EMAIL' AND @SortOrder='DESC')  THEN email    END DESC, 
			
			CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='ASC')   THEN EmailConfirmed   END ASC,  
            CASE WHEN (@SortColumn = 'EMAIL_VERIFIED' AND @SortOrder='DESC')  THEN EmailConfirmed    END DESC, 

   --         CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='ASC')   THEN ordersCount   END ASC,  
   --         CASE WHEN (@SortColumn = 'ORDER' AND @SortOrder='DESC')  THEN ordersCount    END DESC, 

			--CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='ASC')   THEN lastOrderDate   END ASC,  
   --         CASE WHEN (@SortColumn = 'LAST_ORDER_DATE' AND @SortOrder='DESC')  THEN lastOrderDate    END DESC ,
			
			CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,  
            CASE WHEN (@SortColumn = 'EMPLOYEE_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC, 

			CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='ASC')   THEN EmployeeId   END ASC,  
            CASE WHEN (@SortColumn = 'STAFF_ID' AND @SortOrder='DESC')  THEN EmployeeId    END DESC, 
            
			CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN LicencePlate   END ASC,  
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN LicencePlate    END DESC, 

			CASE WHEN (@SortColumn = 'ID' AND @SortOrder='ASC')   THEN U.id   END ASC,  
            CASE WHEN (@SortColumn = 'ID' AND @SortOrder='DESC')  THEN U.id    END DESC, 


			CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='ASC')   THEN [STATUS]   END ASC,  
            CASE WHEN (@SortColumn = 'LICENSE_PLATE' AND @SortOrder='DESC')  THEN [STATUS]    END DESC
                                
 --  ORDER BY U.Id DESC 
   


   OFFSET (@next*@offset)-@next ROWS                                
   FETCH NEXT @next ROWS ONLY                                
                                
END                                
---------------------------------------------------                                
---------------------------------------------------                                 
                                 
SET ANSI_NULLS ON