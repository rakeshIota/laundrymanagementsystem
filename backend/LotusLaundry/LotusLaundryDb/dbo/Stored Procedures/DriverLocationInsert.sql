﻿CREATE PROCEDURE [dbo].[DriverLocationInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Longititude decimal(18,6)=NULL,
			@Latitude decimal(18,6)=NULL,
			@userId BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DriverLocation]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Longititude],[Latitude],[userId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Longititude,@Latitude,@userId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------

SET ANSI_NULLS ON