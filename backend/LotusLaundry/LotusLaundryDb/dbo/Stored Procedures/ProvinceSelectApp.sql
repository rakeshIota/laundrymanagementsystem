﻿CREATE PROCEDURE [dbo].[ProvinceSelectApp](
    @Id	        BIGINT =NULL,
	@LanguageId BIGINT=NULL,
	@next 		int = NULL,
	@offset 	int = NULL,
	@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 	R.[Id],
		    R.[Name],
			(SELECT TOP(1) NAME FROM ProvinceLanguageMapping PL WHERE PL.ProvinceId =  R.Id AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProvinceName
	FROM [Province] R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingSelect] ***/