﻿CREATE PROCEDURE [dbo].[ProductInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Gender NVARCHAR(MAX)=NULL,
			@Name   NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Product]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Gender],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Gender,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductUpdate

/***** Object:  StoredProcedure  [dbo].[ProductUpdate] *****/
SET ANSI_NULLS ON