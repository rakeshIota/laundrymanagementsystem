﻿CREATE PROCEDURE [dbo].[CartXMLSaveApp]
 @CartXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
	  	NDS.DT.value('(IsLocked)[1]', 'BIT') AS 'IsLocked',
		NDS.DT.value('(CustomerId)[1]', 'BIGINT') AS 'CustomerId',
		NDS.DT.value('(Status)[1]', 'NVARCHAR') AS 'Status',
		NDS.DT.value('(Tax)[1]', 'DECIMAL(18,2)') AS 'Tax',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice',
		NDS.DT.value('(TotalItems)[1]', 'INT') AS 'TotalItems'
  FROM 
	@CartXml.nodes('/root/Cart') AS NDS(DT)
   )
   MERGE INTO Cart R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[IsLocked] =ISNULL(x.[IsLocked] ,R.[IsLocked]),R.[CustomerId] =ISNULL(x.[CustomerId] ,R.[CustomerId]),R.[Status] =ISNULL(x.[Status] ,R.[Status]),R.[Tax] =ISNULL(x.[Tax] ,R.[Tax]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice]),R.[TotalItems] =ISNULL(x.[TotalItems] ,R.[TotalItems])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[IsLocked],
		[CustomerId],
		[Status],
		[Tax],
		[SubTotal],
		[TotalPrice],
		[TotalItems]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[IsLocked],x.[CustomerId],x.[Status],x.[Tax],x.[SubTotal],x.[TotalPrice],x.[TotalItems]
    );
END
---------------------------------------------------
---------------------------------------------------
-- Procedure : CartItemInsert

/***** Object:  StoredProcedure [dbo].[CartItemInsert] *****/
SET ANSI_NULLS ON