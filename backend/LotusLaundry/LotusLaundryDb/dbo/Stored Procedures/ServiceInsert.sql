﻿CREATE PROCEDURE [dbo].[ServiceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@Description NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Service]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[Description]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@Description
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceUpdate

/***** Object:  StoredProcedure  [dbo].[ServiceUpdate] *****/
SET ANSI_NULLS ON