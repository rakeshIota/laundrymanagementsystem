﻿CREATE PROCEDURE [dbo].[ServiceSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive],
			R.[Name]
	 	,
		R.[Description],
	overall_count = COUNT(*) OVER(),
	 ImageXml=(
			  SELECT 
			   FG.[Id],
			   FG.[CreatedBy],
			   FG.[UpdatedBy],
			   FG.[CreatedOn],
			   FG.[UpdatedOn],
			   FG.[IsDeleted],
			   FG.[IsActive],
			   FG.[Filename],
			   FG.[MimeType],
			   FG.[Thumbnail],
			   FG.[Size],
			   FG.[Path],
			   FG.[OriginalName],
			   FG.[OnServer],
			   FG.[TypeId],
			   FG.[Type]
			   FROM FileGroupItems FG
			   WHERE FG.[TypeId] = R.[Id] AND FG.[Type]='SERVICE_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 ),
	ServiceLanguageXml = (
		select
		servicelanguagemapping.[Id],
		servicelanguagemapping.[Name],
		servicelanguagemapping.[LanguageId],
		servicelanguagemapping.[ServiceId],
		servicelanguagemapping.[Description],
		[language].Name AS LanguageName

		FROM 
		ServiceLanguageMapping servicelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = servicelanguagemapping.LanguageId
		WHERE servicelanguagemapping.ServiceId =  R.Id AND servicelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Service] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ServiceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ServiceLanguageMappingSelect] ***/