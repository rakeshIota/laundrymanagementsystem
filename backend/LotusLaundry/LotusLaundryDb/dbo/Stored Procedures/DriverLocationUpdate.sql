﻿CREATE PROCEDURE [dbo].[DriverLocationUpdate]    
--  declare  
  @Id    BIGINT=null,    
   @TenantId   INT=NULL,    
   @Slug NVARCHAR(MAX)=NULL,    
     @UpdatedBy    BIGINT=NULL,    
     @IsDeleted    BIT=NULL,    
     @IsActive    BIT=NULL,    
          
   @Longititude decimal(18,6)=NULL,    
   @Latitude decimal(18,6)=NULL,    
   @userId BIGINT=NULL    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    UPDATE [DriverLocation]    
 SET    
     [Slug] = ISNULL(@Slug,[Slug]),    
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),    
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),     
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),    
     [IsActive] = ISNULL(@IsActive,[IsActive]),    
    [Longititude]= ISNULL(@Longititude,[Longititude]),    
    [Latitude]= ISNULL(@Latitude,[Latitude]),    
    [userId]= ISNULL(@userId,[userId])    
         
  WHERE    
  (    
   [userId]=@Id    
  )    
  AND    
  (    
  [TenantId] =  @TenantId    
  )    
END    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : CompanyXMLSave