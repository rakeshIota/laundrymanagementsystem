﻿CREATE PROCEDURE [dbo].[DeliveryTypeInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@PriceType NVARCHAR(MAX)=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [DeliveryType]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PriceType],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PriceType,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DeliveryTypeUpdate

/***** Object:  StoredProcedure  [dbo].[DeliveryTypeUpdate] *****/
SET ANSI_NULLS ON