﻿CREATE PROCEDURE [dbo].[NotificationMasterInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Type NVARCHAR(MAX)=NULL,
		  	@CanBeDelete    BIT=NULL,
			@Message     NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMaster]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Type],[CanBeDelete],[Message]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Type,@CanBeDelete,@Message
	  )
	SELECT SCOPE_IDENTITY()
 END