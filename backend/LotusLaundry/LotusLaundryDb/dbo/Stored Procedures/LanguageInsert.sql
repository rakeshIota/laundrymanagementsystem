﻿CREATE PROCEDURE [dbo].[LanguageInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
			@Code  NVARCHAR(30)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Language]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[Code]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@Code
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : LanguageUpdate

/***** Object:  StoredProcedure  [dbo].[LanguageUpdate] *****/
SET ANSI_NULLS ON