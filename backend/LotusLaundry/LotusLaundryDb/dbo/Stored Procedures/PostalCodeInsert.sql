﻿CREATE PROCEDURE [dbo].[PostalCodeInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@PostalCode NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PostalCode]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PostalCode]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PostalCode
	  )
	SELECT SCOPE_IDENTITY()
 END