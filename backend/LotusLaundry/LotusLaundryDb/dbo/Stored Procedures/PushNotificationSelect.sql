﻿CREATE PROCEDURE [dbo].[PushNotificationSelect](
@Id				BIGINT	=NULL,
@IsActive		bit		=NULL,
@next			int		=NULL,
@offset			int		=NULL, 
@UserRole		NVARCHAR(20)=NULL,
@UserId			BIGINT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =1000
SET @offset=1
END

 SELECT
 	R.Id,
	R.CreatedBy,
	R.UpdatedBy,
	R.CreatedOn,
	R.UpdatedOn,
	R.IsDeleted,
	R.IsActive,
	R.Title,
	R.[Message],
	R.[Type],
	R.IsArchive,
	R.TargetId,
	R.TargetType,
	R.UserId,
	R.UserRole,
	R.IsSeen,
	R.ArchivedBy,
	R.ArchiveDate,
	overall_count = COUNT(*) OVER(),
    UnreadCount=
				(
					SELECT COUNT(1) FROM [PushNotification] L 
					WHERE 
						(
							@Id IS NULL
							OR
							L.Id = @Id
						)
						AND
						(
							@IsActive IS NULL
								OR
							L.IsActive = @IsActive
						)
						AND
						(
							@UserRole IS NULL
							OR
							L.UserRole=@UserRole
						)
						AND
						(
							@UserId IS NULL
							OR
							L.UserId=@UserId
						)
						AND
						L.IsDeleted=0
						AND
						L.IsSeen = 0
						)

	FROM PushNotification R  
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		@UserRole IS NULL
		OR
		R.UserRole=@UserRole
	)
	AND
	(
		@UserId IS NULL
		OR
		R.UserId=@UserId
	)
	AND

	R.IsDeleted=0

	Order by Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END