﻿CREATE PROCEDURE [dbo].[RatingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			

			@orderId bigint=null
	 	,
	 		@Quality bigint =null
	 	,
	 		@DeliveryService bigint =null
			,
	 	    @Staff bigint =null,
	 		@Packaging  bigint =null
			,
			@OverallSatisfaction  bigint =null,
	 		@Feedback  nvarchar(max) =null
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [rating]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],orderid,quality,DeliveryService,Staff,Packaging,OverallSatisfaction,Feedback
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@orderId,@Quality,@DeliveryService,@Staff,@Packaging,@OverallSatisfaction,@Feedback
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CompanyUpdate

/***** Object:  StoredProcedure  [dbo].[CompanyUpdate] *****/
SET ANSI_NULLS ON