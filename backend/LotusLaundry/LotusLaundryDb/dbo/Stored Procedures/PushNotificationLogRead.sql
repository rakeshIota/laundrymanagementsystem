﻿CREATE PROCEDURE [dbo].[PushNotificationLogRead]
 @Id BIGINT = NULL,
 @Type NVARCHAR(50)= NULL,
 @UserId BIGINT = NULL 

AS
	
 UPDATE [dbo].[PushNotification]
 SET
 IsSeen = 1,
 UpdatedOn = SWITCHOFFSET(SYSDATETIMEOFFSET(),'+00:00')
 WHERE
 (
  @Type IS NULL
  OR
  [Type] = @Type
 )
 AND
 (
  @UserId IS NULL
  OR
  UserId = @UserId
 )
 AND
 (
  Id <= @id
 )

RETURN 0