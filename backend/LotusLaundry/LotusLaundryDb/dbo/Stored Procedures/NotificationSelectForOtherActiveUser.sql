﻿
CREATE PROCEDURE [dbo].[NotificationSelectForOtherActiveUser] 
	@UserId BIGINT = NULL,
	@DeviceId NVARCHAR(100) = NULL
AS

	SELECT (U.FirstName +' '+ U.LastName )as FullName,
	--TS.IsGroupChat,TS.IsGroupDiscussion,
	Devices=STUFF(
             (SELECT ',' + D.DeviceToken
              FROM UserDevice D
			  WHERE D.UserId = U.Id AND D.IsDeleted = 0 AND D.DeviceId <> @DeviceId
              FOR XML PATH (''))
             , 1, 1, '')
	 FROM Users U 
	--LEFT JOIN CustomerNotification TS ON TS.CustomerId = U.Id
	WHERE U.Id = @UserId AND U.IsDeleted = 0

RETURN 0