﻿CREATE PROCEDURE [dbo].[ProductLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProductLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[ProductId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@ProductId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : ProductLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[ProductLanguageMappingUpdate] *****/
SET ANSI_NULLS ON