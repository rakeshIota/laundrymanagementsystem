﻿CREATE PROCEDURE [dbo].[OrderItemServiceMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderItemServiceMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderItemId],[ServiceId],[Quantity],[Price],[SubTotal],[TotalPrice]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderItemId,@ServiceId,@Quantity,@Price,@SubTotal,@TotalPrice
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : OrderItemServiceMappingUpdate

/***** Object:  StoredProcedure  [dbo].[OrderItemServiceMappingUpdate] *****/
SET ANSI_NULLS ON