﻿-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>  select * from users   
-- =============================================    
create PROCEDURE [dbo].[SetUserIdle]    
 -- Add the parameters for the stored procedure here    
 @FromUserId BIGINT = NULL,    
 @ToUserId BIGINT = NULL    
AS    
  
  
  
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
    -- Insert statements for procedure here    
  update users set callStatus='IDLE' 
    
END