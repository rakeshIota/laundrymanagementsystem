﻿CREATE PROCEDURE [dbo].ValidatePostalCode(    
@Id         BIGINT =NULL,    
@Slug     NVARCHAR(255)=NULL,    
@next   int = NULL,    
@offset  int = NULL,    
@RelationTable NVARCHAR(50)=NULL,    
@RelationId bigint=NULL,    
@TenantId   INT=NULL,  
@PostalCode varchar(20)=null  
)    
AS BEGIN    
    
IF @next IS NULL    
BEGIN    
SET @next =100000    
SET @offset=1    
END    
    
 SELECT    
    R.[Id]    
   ,    
    R.[TenantId]    
   ,    
    R.[Slug]    
   ,    
    R.[CreatedBy]    
   ,    
    R.[UpdatedBy]    
   ,    
    R.[CreatedOn]    
   ,    
    R.[UpdatedOn]    
   ,    
    R.[IsDeleted]    
   ,    
    R.[IsActive]    
   ,    
    R.[PostalCode]    
   ,    
 overall_count = COUNT(*) OVER()    
 FROM [PostalCode] R      
 WHERE     
 (    
  @RelationTable IS NULL    
 )    
 AND    
 (    
  @Id IS NULL    
  OR    
  R.[Id] = @Id    
 )    
 
 AND    
 (    
  @Slug IS NULL    
  OR    
  R.slug = @Slug    
 )    
 AND    
 (    
  R.[IsDeleted] = 0    
 )    
 AND    
 (    
  @TenantId IS NULL    
  OR    
  R.[TenantId] = @TenantId    
 )    
 And  
 (  
  @PostalCode IS NULL    
  OR   
  r.PostalCode=@PostalCode  
  
 )  
    
 Order by R.Id desc    
 OFFSET (@next*@offset)-@next ROWS    
    FETCH NEXT @next ROWS ONLY    
    
END