﻿CREATE PROCEDURE [dbo].[StateSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[CountryId]
	 	,
		R.Name,
		C.Name As CountryName,
	overall_count = COUNT(*) OVER(),
	StateLanguageXml = (
		select
		statelanguagemapping.Id,
		statelanguagemapping.Name,
		statelanguagemapping.LanguageId,
		statelanguagemapping.StateId,
		[language].Name AS LanguageName

		FROM 
		StateLanguageMapping statelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = statelanguagemapping.LanguageId
		WHERE statelanguagemapping.StateId =  R.Id AND statelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [State] R  
	INNER JOIN Country C ON C.[Id] = R.[CountryId] AND C.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
		OR
		(
			@RelationTable ='Country'
			AND
			R.[CountryId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[StateLanguageMappingSelect] ***/