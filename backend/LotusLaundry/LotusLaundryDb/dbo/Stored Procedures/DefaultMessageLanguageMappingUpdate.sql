﻿CREATE PROCEDURE [dbo].[DefaultMessageLanguageMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DefaultMessageId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [DefaultMessageLanguageMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[DefaultMessageId] = ISNULL(@DefaultMessageId,[DefaultMessageId]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[Message] = ISNULL(@Message,[Message])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : DefaultMessageLanguageMappingXMLSave