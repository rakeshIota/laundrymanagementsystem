﻿CREATE PROCEDURE [dbo].[CartUpdateApp]    
     @Id    BIGINT=NULL,    
   @TenantId   INT=NULL,    
   @Slug NVARCHAR(MAX)=NULL,    
     @UpdatedBy    BIGINT=NULL,    
     @IsDeleted    BIT=NULL,    
     @IsActive    BIT=NULL,    
     @IsLocked    BIT=NULL,    
     @CustomerId    BIGINT=NULL,    
   @Status NVARCHAR(MAX)=NULL,    
   @Tax    DECIMAL(18,2)=NULL,    
   @SubTotal    DECIMAL(18,2)=NULL,    
   @TotalPrice    DECIMAL(18,2)=NULL,    
   @TotalItems   INT=NULL,    
   @DeliveryType  NVARCHAR(100)=NULL  ,  
  
   @DeliveryDate DATETIMEOFFSET(7)=NULL,  
    @PickupDate DATETIMEOFFSET(7)=NULL,  
    @deliverySlot NVARCHAR(100)=NULL,    
   @pickupSlot NVARCHAR(100)=NULL  
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
    
	if(@IsLocked=1)
	begin 
	  INSERT INTO [OrderLog] ([TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note] )  
      VALUES (@TenantId,@Slug,@UpdatedBy,@UpdatedBy,@IsActive,@id,'CART IS LOCKED' ) 
	end
	

    UPDATE [Cart]    
 SET    
     [Slug] = ISNULL(@Slug,[Slug]),    
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),    
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),     
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),    
     [IsActive] = ISNULL(@IsActive,[IsActive]),    
     [IsLocked] = ISNULL(@IsLocked,[IsLocked]),    
     [CustomerId] = ISNULL(@CustomerId,[CustomerId]),    
     [Status] = ISNULL(@Status,[Status]),    
     [Tax] = ISNULL(@Tax,[Tax]),    
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),    
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),    
     [TotalItems] = ISNULL(@TotalItems,[TotalItems]),    
    [DeliveryType]  =  ISNULL(@DeliveryType,[DeliveryType])  ,  
  
  DeliveryDate = ISNULL(@DeliveryDate,DeliveryDate),    
     PickupDate = ISNULL(@PickupDate,PickupDate),    
     deliverySlot = ISNULL(@deliverySlot,deliverySlot),    
    pickupSlot  =  ISNULL(@pickupSlot,pickupSlot)    
  
  
  WHERE    
  (    
   [Id]=@Id    
  )    
  AND    
  (    
  [TenantId] =  @TenantId    
  )    
END    
    
    
---------------------------------------------------    
---------------------------------------------------    
-- Procedure : CartXMLSave