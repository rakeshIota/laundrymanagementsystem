﻿
CREATE PROCEDURE [dbo].[PushNotificationInsert](

	  	@CreatedBy		BIGINT=NULL,
		@Title			NVARCHAR(150)=NULL,
		@Message		NVARCHAR(250)=NULL,
		@Type			NVARCHAR(100)=NULL,
		@TargetId		BIGINT=NULL,
		@TargetType		NVARCHAR(50),
		@UserId			BIGINT=NULL,
		@UserRole		NVARCHAR(50)
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO PushNotification
	  (
	   CreatedBy,
	   UpdatedBy,
	   Title,
	   [Message],
	   [Type], 
	   TargetId, 
	   TargetType, 
	   UserId, 
	   UserRole
	  )
	  VALUES
	  ( 
	   @CreatedBy,
	   @CreatedBy,
	   @Title,
	   @Message,
	   @Type, 
	   @TargetId, 
	   @TargetType, 
	   @UserId, 
	   @UserRole
	  )
	SELECT SCOPE_IDENTITY()
 END