﻿-- =============================================
-- Author:		<Adesh Mishra>
-- Create date: <25-02-2019>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserBasicInfoSelect] 
	-- Add the parameters for the stored procedure here  select * from users 
	--declare
	@UserIds NVARCHAR(500)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select 
	   U.[Id],
	   U.FirstName,
       U.[ProfilePic],
       RolesXml = 
	   (
	       SELECT R.Id,R.Name 
		   FROM Roles R INNER JOIN UserRoles UR ON R.Id = UR.RoleId 
		   WHERE UR.UserId = U.Id 
		   FOR XML AUTO,ROOT,ELEMENTs
		)
    
	  FROM Users U
	
	  WHERE
	  U.IsActive = 1
	  AND
	  U.IsDeleted = 0
	  AND 
	  U.Id IN (SELECT number FROM ListToTable(@UserIds))

	  --declare @id bigint ,@FirstName varchar(100), @ProfilePic varchar(max),@RolesXml varchar(max)

	  --select @id as Id ,@FirstName FirstName,@ProfilePic ProfilePic,@RolesXml RolesXml

END