﻿--           select * from [users] where firstname='bhawana'                             
CREATE PROCEDURE [dbo].[OrderSelectApp](                                                
 --declare                                         
@Id         BIGINT =null,                                                
@Slug     NVARCHAR(255)=NULL,                                                
@next   int = NULL,                                                
@offset  int = NULL,                                                
@RelationTable NVARCHAR(50)=null,                                                
@RelationId bigint=NULL,                                                
@TenantId   INT=NULL,                                                
@LanguageId  BIGINT=NULL,                                                
@userid      BIGINT=null    ,                                        
                                
@DistrictId      BIGINT=NULL    ,                                   
@SubDistrictId      BIGINT=NULL    ,                                   
@DriverId      BIGINT=NULL    ,                                
@Status      NVARCHAR(50)=  null  ,                              
                              
@Name      NVARCHAR(50)=  null  ,                              
@email      NVARCHAR(50)=  null  ,                              
@Phone      NVARCHAR(50)=  null  ,                
@deliveryType Nvarchar(50)=null                
)   AS                                           
BEGIN                                                
                                                
IF @next IS NULL                                                
BEGIN                                                
SET @next =100000                                                
SET @offset=1                                                
END                                                
     declare @ratingStatus bit=0;                                     
  if(@LanguageId=null)                                        
  begin                                         
    select @LanguageId=languageid from users where id=@userid                                        
  end   
  if exists(select * from rating where orderid=@id)
  begin 
    set @ratingStatus=1
  end
                                        
 SELECT                                                
    o.[Id]                                                
   ,                                                
    o.[TenantId]                                                
   ,                                                
    o.[Slug]                                                
   ,                                                
    o.[CreatedBy]                                                
   ,                                                
    o.[UpdatedBy]                                                
   ,                                                
    o.[CreatedOn]                                                
   ,                                                
    o.[UpdatedOn]                                                
   ,                                                
    o.[IsDeleted]                                                
   ,                                                
    o.[IsActive]                                                
                                                  
   ,                                                
    o.Userid                                                
   ,                                                
    o.[Status]                                                
   ,                                                
    o.[Tax]                                                
   ,                                                
    o.[SubTotal]                                                
   ,                                                
    o.[TotalPrice]                                                
   ,                                                
    o.[TotalItems]                                        
   ,                                                
  o.[DeliveryType],                     
                                              
  o.Deliverydate,                                              
  o.Pickupdate,                                         
  o.deliverySlot,                                              
  o.pickupSlot,                                           
                                  
  o.Paymentresponse,                                          
  o.Discount,                                          
      o.paymentstatus,                                      
    o.LogisticCharge,                                        
 o.paymentType,                                    
 o.change,                                    
 o.deliveryprice,                        
 --o.adminnote,      
 rating=@ratingStatus    , 
      
 CASE       
           WHEN O.AdminNote is null      
           THEN (select adminNote from Users where id=O.userid)      
           ELSE o.adminNote      
       END AS adminnote,      
      
      
o.paymentDate,                      
o.collectedDate  ,                      
                      
 o.deliveredDate,                      
       
       
                      
                      
                      
  overall_count = COUNT(*) OVER(),                                                
  OrderItemXml = (                                                
  select                                                
  OrderItem.Id,                       
  OrderItem.ProductId,                                                
  OrderItem.OrderId,                                                
  OrderItem.ServiceId,                                                
  OrderItem.TotalPrice,                              
  OrderItem.Quantity,                                      
  OrderItem.IsPacking,                                     
  OrderItem.SubTotal,                                     
  OrderItem.TotalPrice,                                     
  OrderItem.PackingId,                                    
  OrderItem.PackingPrice,                                    
  orderitem.IsDeleted,                                    
  product.Name  AS ProductName,                                                
  [service].Name  AS ServiceName,                               
  (SELECT TOP(1) NAME FROM ProductLanguageMapping PL WHERE PL.ProductId =  OrderItem.ProductId AND PL.IsDeleted = 0 AND PL.LanguageId =@LanguageId ) AS ProductNameL,                                                
  (SELECT TOP(1) NAME FROM ServiceLanguageMapping SL WHERE SL.ServiceId =  OrderItem.ServiceId AND SL.IsDeleted = 0 AND SL.LanguageId =@LanguageId ) AS ServiceNameL,                                                
  (SELECT TOP(1) [Path] FROM FileGroupItems FG WHERE FG.[TypeId] = OrderItem.ProductId AND FG.[Type]='PRODUCT_IMAGE' AND FG.[IsActive] = 1 AND FG.[IsDeleted] = 0 ) AS [Path],                                                
  (SELECT TOP(1) Price FROM ProductPrice PP WHERE PP.ServiceId =  OrderItem.ServiceId AND PP.ProductId =  OrderItem.ProductId AND PP.[IsDeleted] = 0 ) AS Price                                                
                                                  
                                                
  FROM                                                 
  orderItem OrderItem                               
  INNER JOIN                                                 
  Product product ON product.Id =  OrderItem.ProductId                                                
  INNER JOIN                                                
  [Service] [service] ON [service].Id =  OrderItem.ServiceId                                                
  left join                                             
  productprice pp on OrderItem.productid =pp.productid and OrderItem.serviceid=pp.serviceid                                                
  WHERE OrderItem.orderid =  o.Id AND OrderItem.IsDeleted = 0                             
  FOR XML AUTO,ROOT,ELEMENTs                                                
 )  ,                                          
 AddressXml=(                                        
                                         
   SELECT                                                  
    R.[Id],                                                  
    R.[AddressLine1],                                                  
   R.[AddressLine2],                                                  
    R.[DistrictId],                                       
    R.[SubDistrictId],                                                  
    R.[PostalCode],                                                  
    R.[Tag],                                                  
    R.[Latitude],                                                  
    R.[Longitude],                                                  
   R.[HouseNumber],                                                  
   R.[StreetNumber],                                              
   R.[Note],                                                  
   R.[Type],                                                  
                                                  
                                                
   R.[BuildingName] ,                                 
   R.[Floor] ,                                                
   R.[UnitNo] ,                                                
   R.[PhoneNo] ,                                                
   R.[PhoneExt] ,                                                
   R.[ResidenceType] ,                                                
        -- AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id                                           
                                                     
   D.Name  AS DistrictName,                                                  
   SD.Name  AS SubDistrictName,                     
    PD.Name As ProvinceName,                      
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,                                                  
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId  ) AS SubDistrictNameL  ,                                                
     (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL                              
                                                  
                                             
 FROM deliveryaddress R INNER JOIN                                                  
 [District] D ON D.Id =  R.[DistrictId]                                                  
 INNER JOIN                                                   
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                      
 INNER JOIN                               
 Province PD ON PD.Id =  R.ProvinceId                       
                    
                    
 where R.orderid=o.ID                                            
      FOR XML AUTO,ROOT,ELEMENTs                                                              
    )  ,                                  
 DriverXml=                                  
 (                                  
   select users.Id,Email,PhoneNumber,UserName,FirstName,LastName,BikeInformation,NationalId,LicenceId,LicencePlate,  
   profilePic=(select top 1 path from filegroupitems where TypeId=o.driverid and [type]='PROFILE_PIC' and IsDeleted=0 and IsActive=1)                   
   , DriverLocation.[Latitude],DriverLocation.[Longititude]                   
  -- ,Latitude=(select [Latitude] from driverlocation  where userid=users.id)         select * from users where id=104          
  -- ,Longititude=(select Longititude from driverlocation  where userid=users.id)                  
                     
   from users users left join DriverLocation DriverLocation on users.id=DriverLocation.userid where users.id=o.driverid                                  
   --from users                                
   FOR XML AUTO,ROOT,ELEMENTs                                      
 )    ,                              
 CustomerXml=                                
 (                                
   select Id,Email,PhoneNumber,UserName,FirstName,LastName from users where id=o.UserId                            
                                 
   FOR XML AUTO,ROOT,ELEMENTs                                    
 )                             
                                          
 FROM [order] o                                                  
 WHERE                
 (              
  @RelationTable IS NULL                                                              
       or                                                      
       (                                                                    
           @RelationTable = 'NEW_ORDERS'                                                                  
           and                                                                
           (                                                            
            o.[status]='NEW'                                  
                                  
            )                                                            
       )               
              
   or                                                                  
       (                                                                    
           @RelationTable = 'COLLECTING_ORDERS'                                                                  
           and                                                                
           (                                                            
            o.[status]='ASSIGNED_DRIVER'                                  
            or                                   
            o.[status]='AWAITING_COLLECTION'                                  
            or                                   
            o.[status]='COLLECTED'                       
                                  
            )                                                            
       )              
    or                                                                  
       (                  
           @RelationTable = 'CLEANING_ORDERS'                                                                  
           and                                                                
           (                                                            
            o.[status]='CLEANING_IN_PROGRESS'                                  
            or                                   
            o.[status]='CLEANING_COMPLETED'                                  
                                  
                     
            )                                                            
       )              
    or                                                                  
       (                                                                    
           @RelationTable = 'DELIVERY_ORDERS'                                                                  
           and                                                                
           (                                                            
            o.[status]='AWAITING_COLLECTION_DELIVERY'                                  
            or                                   
            o.[status]='ON_THE_WAY'                                  
                                  
                                  
            )                                                            
       )              
     or                                                                  
       (                                                                    
@RelationTable = 'COLLECTING_ORDERS_ASSIGNED'                                                                  
           and                                                                
           (                                                            
            o.[status]='ASSIGNED_DRIVER'                                  
            or                                   
            o.[status]='AWAITING_COLLECTION'                                  
                                  
                                  
            )                                                            
       )              
    or                                               
       (                                                                    
           @RelationTable = 'TOTAL_ORDERS'                                                                  
           and                                                                
           (                                                            
            o.[status]='NEW'                                  
            or                                   
            o.[status]='ASSIGNED_DRIVER'                                  
            or                                   
            o.[status]='AWAITING_COLLECTION'                       
             or                                   
            o.[status]='COLLECTED'               
   or                                   
            o.[status]='CLEANING_IN_PROGRESS'               
   or                                   
            o.[status]='CLEANING_COMPLETED'               
   or                                   
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'               
   or                                   
            o.[status]='ON_THE_WAY'               
            )                                                            
       )              
 )                                          
 AND                                                
 (                                                
  @Id IS NULL                             
  OR                                                
  o.[Id] = @Id                                                
 )                                                
 AND                                                
 (                                                
  @Slug IS NULL                                                
  OR                                                
  o.[Slug] = @Slug            
 )                                                
 AND                                                
 (                                                
  o.[IsDeleted] = 0                                                
 )                                                
 AND                                                
 (                                                
  @TenantId IS NULL                                                
  OR                                  
  o.[TenantId] = @TenantId                                                
 )                                                
 AND                                                
 (                        
  @userid IS NULL                                                
   OR                                                
  o.userid =  @userid                                                
 )                                    
                                  
  AND                                                
 (                                                
  @DriverId IS NULL                                                
   OR                                                
  o.driverid =  @DriverId                                                
 )                                 
  AND                                                
 (                                                
  @Status IS NULL                               
   OR                                                
  o.[Status] =  @Status                                              
 )                                 
  AND                                                
 (                                                
  @DistrictId IS NULL                                                
   OR                                                
  o.id in(  select orderid from deliveryaddress where districtid=@DistrictId )                                                 
 )                                 
 AND                                        
 (                                                
  @SubDistrictId IS NULL                                                
   OR                                                
  o.id in(  select orderId from deliveryaddress where SubDistrictId=@SubDistrictId )                                        
 )                                 
 -- AND                                                
 --(                                                
 -- @Name IS NULL                                                
 --  OR                                                
 -- o.userid in (select id from users where [FirstName] like '%'+@Name+'%' or LastName like '%'+@Name+'%' )                                                
 --)                                   
   AND                                                
 (                                                
  @email IS NULL                                                
   OR                            
  o.userid in (select id from users where [Email] like '%'+@email+'%')                                
                                    
                                
 )                                   
  AND                                                
 (                                                
  @Phone IS NULL                                                
   OR                                                
  o.userid in (select id from users where phonenumber like '%'+@Phone+'%')                                   
   OR                                                
  o.userid in (select userid from DeliveryAddress where PhoneNo like '%'+@Phone+'%')                                  
  OR                                                
  o.userid in (select userid from DeliveryAddress where alterNumber like '%'+@Phone+'%')                
 )                      
 And                
 (                
  @deliveryType is null                
  or                 
  o.deliveryType=@deliveryType                
                 
 )                
                                
                                
                                
 --Order by o.Id desc          
 ORDER BY    
 o.CreatedOn desc,    
 (       
    
    CASE DeliveryType        
            
    WHEN 'SAMEDAY'        
    THEN 1        
 WHEN 'EXPRESS'        
    THEN 2        
            
    WHEN 'NORMAL'        
    THEN 3        
            
    END        
) ASC        
        
        
        
 OFFSET (@next*@offset)-@next ROWS                                                
    FETCH NEXT @next ROWS ONLY                                                
                                                
END                                                
                                                
                                                
                                                
---------------------------------------------------                                                
--------------------------------------------------- 