﻿CREATE PROCEDURE [dbo].[MessageLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Message NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Type NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [MessageLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Message],[LanguageId],[Type]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Message,@LanguageId,@Type
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : MessageLogUpdate

/***** Object:  StoredProcedure  [dbo].[MessageLogUpdate] *****/
SET ANSI_NULLS ON