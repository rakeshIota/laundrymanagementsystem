﻿CREATE PROCEDURE [dbo].[CurrencyInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Currency]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CurrencyUpdate

/***** Object:  StoredProcedure  [dbo].[CurrencyUpdate] *****/
SET ANSI_NULLS ON