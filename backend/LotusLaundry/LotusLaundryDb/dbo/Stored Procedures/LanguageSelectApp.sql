﻿
CREATE PROCEDURE [dbo].[LanguageSelectApp](
@Id	        BIGINT =NULL,
@next 		int = NULL,
@offset 	int = NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 	R.Id,
	 	R.[Name],
		R.[Code]
	FROM [Language] R  
	WHERE
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.[IsActive] = 1
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : StateSelect

/***** Object:  StoredProcedure [dbo].[StateSelect] ***/