﻿-- ACTIVE-COMPLETE : Active order and completed order : Active                   
-- ACTIVE           : only active order for the driver                   
CREATE PROCEDURE [dbo].[DriverActiveOrderSelect](          
--declare                              
@Id         BIGINT =null,                                    
@Slug     NVARCHAR(255)=NULL,                                    
@next   int = NULL,                                    
@offset  int = NULL,                                    
@RelationTable NVARCHAR(50)=null,                                    
@RelationId bigint=NULL,                                    
@TenantId   INT=NULL,                                    
@LanguageId  BIGINT=null,                                    
@userid      BIGINT=null,                            --2020-05-06 05:44:21.7062794 +00:00 --2020-05-07 05:44:21.7062794 +00:00  
@isCompleted bit =0,                    
@DriverId  BIGINT =104,                  
@Date datetimeoffset(7)=null,                  
@type nvarchar(50)=null                  
                   
) AS                               
BEGIN                                    
                                    
IF @next IS NULL                                    
BEGIN                                    
SET @next =100000                                    
SET @offset=1                                    
END                                    
     declare @InternalStatus nvarchar(50)=null                               
                    
                       
  if(@isCompleted=1 and @type='ACTIVE')                  
  begin                   
    set @RelationTable='ACTIVE-COMPLETE'                  
  end                  
     else if(@isCompleted=0 and @type='COMPLETE')                  
    begin                   
     set @RelationTable='COMPLETE'                  
  end                  
  else if(@isCompleted=0 and @type='ACTIVE')                  
    begin                   
     set @RelationTable='ACTIVE'                  
  end          
  else      
  begin       
         
     set @RelationTable='ACTIVE'      
  end       
             
        
      
 SELECT                                    
    o.[Id]                                    
                                   
                                        
   ,                                    
    o.[CreatedOn]                                    
                                           
                                       
   ,                                    
    o.[Status] ,                              
    PaymentStatus                              
   ,                          
   o.paymenttype,                        
                        
    o.[TotalItems]  ,                                  
    o.deliveryType,                            
    o.PickupDate,                      
 o.DeliveryDate,                      
 o.pickupSlot,                      
 o.deliverySlot,                      
 o.TotalPrice,                      
    customerName=(select distinct FirstName+' '+LastName from users where id=o.UserId),                             
    o.userid customerid,                           
  o.Paymentresponse,                              
                                
  overall_count = COUNT(*) OVER(),                                    
                               
AddressXml=(                                    
                                     
   SELECT                                              
    R.[Id],                                              
    R.[AddressLine1],                                              
   R.[AddressLine2],                                              
    R.[DistrictId],                                             
    R.[SubDistrictId],                                              
    R.[PostalCode],                                              
    R.[Tag],                                              
    R.[Latitude],                                              
    R.[Longitude],                  
   R.[HouseNumber],                                              
   R.[StreetNumber],                                          
   R.[Note],                                              
   R.[Type],           
                                              
                                            
   R.[BuildingName] ,                                            
   R.[Floor] ,                          
   R.[UnitNo] ,                                            
   R.[PhoneNo] ,                                            
   R.[PhoneExt] ,                                            
   R.[ResidenceType] ,                                   
        -- AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id                                       
                                                 
   D.Name  AS DistrictName,                                              
   SD.Name  AS SubDistrictName,                 
    PD.Name As ProvinceName,                  
   (SELECT TOP(1) NAME FROM DistrictLanguageMapping DL WHERE DL.DistrictId =  R.DistrictId AND DL.IsDeleted = 0 AND DL.LanguageId =@LanguageId ) AS DistrictNameL,                                              
   (SELECT TOP(1) NAME FROM SubDistrictLanguageMapping SDL WHERE SDL.SubDistrictId =  R.SubDistrictId AND SDL.IsDeleted = 0 AND SDL.LanguageId =@LanguageId  ) AS SubDistrictNameL  ,                                            
     (SELECT TOP(1) NAME FROM ProvinceLanguageMapping PDL WHERE PDL.ProvinceId =  R.ProvinceId AND PDL.IsDeleted = 0 AND PDL.LanguageId =@LanguageId ) AS ProvinceNameL                          
                                              
                                              
 FROM deliveryaddress R INNER JOIN                                              
 [District] D ON D.Id =  R.[DistrictId]                                              
 INNER JOIN                                               
 [SubDistrict] SD ON SD.Id =  R.SubDistrictId                  
 INNER JOIN                           
 Province PD ON PD.Id =  R.ProvinceId                   
                
                
 where R.orderid=o.ID                                        
      FOR XML AUTO,ROOT,ELEMENTs                                                          
    )                              
                              
 from [order] o                                      
 WHERE             
 (          
       @RelationTable IS NULL                                                
    or                                                    
       (                                                      
           @RelationTable = 'ACTIVE'                                                    
           and                                                  
           (                                              
            o.[status]='AWAITING_COLLECTION'                    
            or                     
            o.[status]='ON_THE_WAY'                    
            or                     
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'         
   or                     
            o.[status]='ASSIGNED_DRIVER'         
            )                                              
       )                       
       or                                                    
       (                                                      
           @RelationTable = 'ACTIVE-COMPLETE'                                                    
           and                                                  
            (                                              
            o.[status]='AWAITING_COLLECTION'                    
            or                     
            o.[status]='ON_THE_WAY'                    
            or                     
            o.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                     
            or                  
            o.[Status]='DELIVERED'         
   or                     
            o.[status]='ASSIGNED_DRIVER'        
          )                                              
        )                  
       or                                                    
       (                                                      
           @RelationTable = 'COMPLETE'                                                    
           and           
            (                                              
                             
            o.[Status]='DELIVERED'                  
            )                                              
       )        
        
          
      
   )               
        and                                 
   o.[IsDeleted] = 0            
   and                  
   (                  
   @DriverId is null                  
   or                  
   o.DriverId=@DriverId                  
   )                  
   AND                                                    
   (                                                    
      @Date IS NULL                                                    
            
     OR                                                    
       cast (o.deliverydate as date)=cast(@Date as date)   
     or   
       cast (o.pickupdate as date)=cast(@Date as date)   
  
   )                  
                  
                  
                 
                              
                                    
 Order by o.Id desc                                    
 OFFSET (@next*@offset)-@next ROWS                                    
    FETCH NEXT @next ROWS ONLY                                    
                                    
END                                    
                                    
                                    
                        
---------------------------------------------------                                    
--------------------------------------------------- 