﻿CREATE PROCEDURE [dbo].[OrderItemUpdate]  
     @Id    BIGINT=NULL,  
   @TenantId   INT=NULL,  
   @Slug NVARCHAR(MAX)=NULL,  
     @UpdatedBy    BIGINT=NULL,  
     @IsDeleted    BIT=NULL,  
     @IsActive    BIT=NULL,  
     @OrderId    BIGINT=NULL,  
     @ProductId    BIGINT=NULL,  
   @Quantity   INT=NULL,  
     @IsPacking    BIT=NULL,  
   @PackingPrice    DECIMAL(18,2)=NULL,  
   @SubTotal    DECIMAL(18,2)=NULL,  
   @TotalPrice    DECIMAL(18,2)=NULL,  
     @PackingId    BIGINT=NULL,
	 @ServiceId bigint =null
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    UPDATE [OrderItem]  
 SET  
     [Slug] = ISNULL(@Slug,[Slug]),  
     [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),  
     [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),   
     [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),  
     [IsActive] = ISNULL(@IsActive,[IsActive]),  
     [OrderId] = ISNULL(@OrderId,[OrderId]),  
     [ProductId] = ISNULL(@ProductId,[ProductId]),  
     [Quantity] = ISNULL(@Quantity,[Quantity]),  
     [IsPacking] = ISNULL(@IsPacking,[IsPacking]),  
     [PackingPrice] = ISNULL(@PackingPrice,[PackingPrice]),  
     [SubTotal] = ISNULL(@SubTotal,[SubTotal]),  
     [TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),  
     [PackingId] = ISNULL(@PackingId,[PackingId]) ,
	 [ServiceId]=ISNULL(@ServiceId,[ServiceId])
  WHERE  
  (  
   [Id]=@Id  
  )  
  AND  
  (  
  [TenantId] =  @TenantId  
  )  
END  
  
  
---------------------------------------------------  
---------------------------------------------------  
-- Procedure : OrderItemXMLSave  