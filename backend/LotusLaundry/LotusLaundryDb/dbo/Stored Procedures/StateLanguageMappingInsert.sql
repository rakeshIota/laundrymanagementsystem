﻿CREATE PROCEDURE [dbo].[StateLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@StateId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [StateLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[StateId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@StateId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : StateLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[StateLanguageMappingUpdate] *****/
SET ANSI_NULLS ON