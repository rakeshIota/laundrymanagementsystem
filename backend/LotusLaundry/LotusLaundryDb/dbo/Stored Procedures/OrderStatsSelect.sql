﻿CREATE PROCEDURE [dbo].[OrderStatsSelect]
(  
@TenantId INT = NULL
)  
AS BEGIN  
SET NOCOUNT ON  
SELECT  
  
'Total Orders' AS TotalOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'NORMAL' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalNormalOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'SAMEDAY' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalSameDayOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] != 'DELIVERED' AND [STATUS] != 'CANCELLED') AND [DeliveryType] = 'EXPRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS TotalExpressOrderCount,  
 
'New Orders' AS NewOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'SAMEDAY' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewSameDayOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'EXPRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewExpressOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'NEW' AND [DeliveryType] = 'NORMAL' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS NewNormalOrderCount,  
  
'Collecting' AS CollectionOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'ASSIGNED_DRIVER' OR [STATUS] = 'AWAITING_COLLECTION' OR [STATUS] = 'COLLECTED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'ASSIGNED_DRIVER' OR [STATUS] = 'AWAITING_COLLECTION') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionAssignedOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'COLLECTED' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CollectionCollectedOrderCount,
  
'Cleaning' AS CleaningOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'CLEANING_IN_PROGRESS' OR [STATUS] = 'CLEANING_COMPLETED') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningOrderTotalCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'CLEANING_IN_PROGRESS' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningInprogressOrderCount,  
(SELECT COUNT(ID) FROM [ORDER] WHERE [STATUS] = 'CLEANING_COMPLETED' AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS CleaningCompletedOrderCount,  
  
'Delivery' AS DeliveryOrderTitle,
(SELECT COUNT(ID) FROM [ORDER] WHERE ([STATUS] = 'AWAITING_COLLECTION_DELIVERY' OR [STATUS] = 'ON_THE_WAY') AND IsDeleted = 0 AND (@TenantId IS NULL OR [TenantId] = @TenantId)) AS DeliveryOrderTotalCount

END

--SELECT [STATUS], DeliveryType From [Order] Where IsDeleted = 0 AND @TenantId IS NULL OR [TenantId] = @TenantId