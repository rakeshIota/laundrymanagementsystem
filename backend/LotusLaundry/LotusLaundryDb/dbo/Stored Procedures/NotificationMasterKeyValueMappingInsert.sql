﻿CREATE PROCEDURE [dbo].[NotificationMasterKeyValueMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL,
		  	@NotificationId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [NotificationMasterKeyValueMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Key],[Value],[NotificationId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Key,@Value,@NotificationId
	  )
	SELECT SCOPE_IDENTITY()
 END