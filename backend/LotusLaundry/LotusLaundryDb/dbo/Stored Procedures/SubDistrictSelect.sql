﻿CREATE PROCEDURE [dbo].[SubDistrictSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[DistrictId]
	 	,
		R.[Name],
	overall_count = COUNT(*) OVER(),
	D.[Name] AS DistrictName,
	SubDistrictLanguageXml = (
		select
		subdistrictlanguagemapping.Id,
		subdistrictlanguagemapping.Name,
		subdistrictlanguagemapping.LanguageId,
		subdistrictlanguagemapping.SubDistrictId,
		[language].Name AS LanguageName

		FROM 
		SubDistrictLanguageMapping subdistrictlanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = subdistrictlanguagemapping.LanguageId
		WHERE subdistrictlanguagemapping.SubDistrictId =  R.Id AND subdistrictlanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [SubDistrict] R  
	INNER JOIN [District] D ON D.[Id] = R.[DistrictId] AND D.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
			OR

		(
			@RelationTable = 'District'
				AND
			R.[DistrictId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[SubDistrictLanguageMappingSelect] ***/