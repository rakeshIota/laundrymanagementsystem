﻿CREATE PROCEDURE [dbo].[CitySelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[StateId]
	 	,
		R.Name,
		S.Name AS StateName,

	overall_count = COUNT(*) OVER(),
	CityLanguageXml = (
		select
		citylanguagemapping.Id,
		citylanguagemapping.Name,
		citylanguagemapping.LanguageId,
		citylanguagemapping.CityId,
		[language].Name AS LanguageName

		FROM 
		CityLanguageMapping citylanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = citylanguagemapping.LanguageId
		WHERE citylanguagemapping.CityId =  R.Id AND citylanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [City] R  
	INNER JOIN [State] S ON S.[Id] = R.[StateId] AND S.IsDeleted = 0
	WHERE 
	(
		@RelationTable IS NULL
			OR
		(
			@RelationTable = 'State'
			 AND
			 R.[StateId] =  @RelationId
		)
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : CityLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[CityLanguageMappingSelect] ***/