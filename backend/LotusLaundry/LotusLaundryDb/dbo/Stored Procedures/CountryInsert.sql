﻿CREATE PROCEDURE [dbo].[CountryInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Country]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : CountryUpdate

/***** Object:  StoredProcedure  [dbo].[CountryUpdate] *****/
SET ANSI_NULLS ON