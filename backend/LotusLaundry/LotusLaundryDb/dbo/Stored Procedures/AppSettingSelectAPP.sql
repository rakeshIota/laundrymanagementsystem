﻿CREATE PROCEDURE [dbo].[AppSettingSelectApp](
@Id	        BIGINT =NULL,
@next 		int = NULL,
@offset 	int = NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Key],
	 		R.[Value]
	FROM [AppSetting] R  
	WHERE 
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)
	AND
	(
		R.[IsActive] =  1
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END