﻿ -- select * from [order]          
CREATE PROCEDURE [dbo].[AllOrderSelectApp](                  
--declare            
@Id         BIGINT =null,                  
@Slug     NVARCHAR(255)=NULL,                  
@next   int = NULL,                  
@offset  int = NULL,                  
@RelationTable NVARCHAR(50)=NULL,                  
@RelationId bigint=NULL,                  
@TenantId   INT=NULL,                  
@LanguageId  BIGINT=NULL,                  
@userid      BIGINT=100,            
@IsClosed bit =null          
) AS             
BEGIN                  
                  
IF @next IS NULL                  
BEGIN                  
SET @next =100000                  
SET @offset=1                  
END                  
                  
  if(@IsClosed=0)          
  begin           
    set @IsClosed=null;          
  end          
          
               
            
 SELECT                  
    R.[Id]                  
                 
                      
   ,                  
    R.[CreatedOn]                  
                         
                     
   ,                  
    R.[Status] ,            
    PaymentStatus            
   ,        
   R.paymenttype,      
      
    R.[TotalItems]  ,                
    R.deliveryType,          
    R.PickupDate,    
 R.DeliveryDate,    
 R.pickupSlot,    
 R.deliverySlot,    
 R.TotalPrice,    
               
             
  R.Paymentresponse,            
              
  overall_count = COUNT(*) OVER(),                  
             
 AddressXml=( select * from deliveryaddress DA where DA.orderid=R.id             
             
  FOR XML AUTO,ROOT,ELEMENTs                  
             
 )            
            
 from [order] R                    
 WHERE                   
 (                  
  @RelationTable IS NULL                  
 )              
           
          
          
          
 AND                  
 (                  
  @Id IS NULL                  
  OR                  
  R.[Id] = @Id                  
 )                  
 AND                  
 (                  
  @Slug IS NULL                  
  OR                  
  R.[Slug] = @Slug                  
 )                  
 AND                  
 (                  
  R.[IsDeleted] = 0                  
 )                  
 AND                  
 (                  
  @TenantId IS NULL                  
  OR                  
  R.[TenantId] = @TenantId                  
 )                  
 AND                  
 (                  
  @userid IS NULL                  
   OR                  
  R.userid =  @userid                  
 )          
 and           
 (          
   @IsClosed is null          
   or           
   R.status='NEW'          
           
 )          
            
                  
ORDER BY 
R.createdon DESC,
(  
    CASE DeliveryType  
      
    WHEN 'SAMEDAY'  
    THEN 1  
 WHEN 'EXPRESS'  
    THEN 2  
      
    WHEN 'NORMAL'  
    THEN 3  
      
    END  
) ASC  
  
 OFFSET (@next*@offset)-@next ROWS                  
    FETCH NEXT @next ROWS ONLY                  
                  
END                  
                  
                  
                  
---------------------------------------------------                  
--------------------------------------------------- 