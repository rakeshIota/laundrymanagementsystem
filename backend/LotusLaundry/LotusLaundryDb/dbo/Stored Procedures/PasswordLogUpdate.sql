﻿CREATE PROCEDURE [dbo].[PasswordLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Count   INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PasswordLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[Count] = ISNULL(@Count,[Count])
	 WHERE
	 (
	  [Id]=@Id
		OR
		(
			@IsDeleted = 1
				AND
			[UserId] =  @UserId
		)
	 )
	 --AND
	 --(
		--[TenantId] =  @TenantId
	 --)
END


---------------------------------------------------
---------------------------------------------------
-- Procedure : PasswordLogXMLSave