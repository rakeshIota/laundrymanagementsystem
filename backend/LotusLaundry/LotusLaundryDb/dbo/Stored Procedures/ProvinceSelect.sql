﻿CREATE PROCEDURE [dbo].[ProvinceSelect](
@Id	        BIGINT =NULL,
@Slug	   	NVARCHAR(255)=NULL,
@next 		int = NULL,
@offset 	int = NULL,
@RelationTable NVARCHAR(50)=NULL,
@RelationId bigint=NULL,
@TenantId   INT=NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[TenantId]
	 	,
	 		R.[Slug]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	overall_count = COUNT(*) OVER(),
	ProvinceLanguageXml = (
		select
		provincelanguagemapping.Id,
		provincelanguagemapping.Name,
		provincelanguagemapping.LanguageId,
		provincelanguagemapping.ProvinceId,
		[language].Name AS LanguageName

		FROM 
		ProvinceLanguageMapping provincelanguagemapping INNER JOIN 
		[Language] [language] ON [language].Id = provincelanguagemapping.LanguageId
		WHERE provincelanguagemapping.ProvinceId =  R.Id AND provincelanguagemapping.IsDeleted = 0
		FOR XML AUTO,ROOT,ELEMENTs
	)
	FROM [Province] R  
	WHERE 
	(
		@RelationTable IS NULL
	)
	AND
	(
		@Id IS NULL
		OR
		R.[Id] = @Id
	)
	AND
	(
		@Slug IS NULL
		OR
		R.[Slug] = @Slug
	)
	AND
	(
		R.[IsDeleted] = 0
	)
	AND
	(
		@TenantId IS NULL
		OR
		R.[TenantId] = @TenantId
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END



---------------------------------------------------
---------------------------------------------------
-- Procedure : ProvinceLanguageMappingSelect

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingSelect] ***/