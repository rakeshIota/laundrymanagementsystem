﻿CREATE PROCEDURE [dbo].[OrderList](                                                      
 --declare                                               
@Id         BIGINT =null,                                                      
@Slug     NVARCHAR(255)=NULL,                                                      
@next   int = NULL,                                                      
@offset  int = NULL,                                                      
@RelationTable NVARCHAR(50)=null,                                                      
@RelationId bigint=NULL,                                                      
@TenantId   INT=NULL,                                                      
@LanguageId  BIGINT=NULL,                                                      
@userid      BIGINT=null    ,                                              
                                      
@DistrictId      BIGINT=NULL    ,                                         
@SubDistrictId      BIGINT=NULL    ,                                         
@DriverId      BIGINT=NULL    ,                                      
@Status      NVARCHAR(50)=  null  ,                                    
                                    
@Name      NVARCHAR(50)=  null  ,                                    
@email      NVARCHAR(50)=  null  ,                                    
@Phone      NVARCHAR(50)=  null  ,                      
@deliveryType Nvarchar(50)=null,             
@SortColumn Nvarchar(50)=null,          
@SortOrder Nvarchar(50)=null          
          
)   AS                                                 
BEGIN                                                      
               
   if(@SortColumn is null)      
   begin      
    --set @SortColumn='DEFAULT'      
	set @SortColumn='CREATED_ON'  
	set @SortOrder='DESC'
   end      
      
IF @next IS NULL                                                      
BEGIN                                                      
SET @next =100000                                                      
SET @offset=1                                                      
END                                                      
                                                
  if(@LanguageId=null)                                              
  begin                                               
    select @LanguageId=languageid from users where id=@userid                                              
  end                                              
                  
select * from (          
select           
R.id,          
Customername =(select top 1 firstname+' '+lastname from users where id= R.userid),          
R.createdon ,          
R.PickupDate,          
R.DeliveryDate,          
r.DeliveryType,           
districtname=(select D.Name from deliveryaddress DA INNER JOIN   [District] D ON D.Id =  DA.[DistrictId]   where orderid=R.id),          
Subdistrictname=(select D.Name from deliveryaddress DA INNER JOIN   [SubDistrict] D ON D.Id =  DA.SubDistrictId   where orderid=R.id),          
R.totalPrice,          
R.paymentType,          
R.paymentStatus,          
R.status,          
driverName=(select top 1 firstname+' '+lastname from users where id= R.driverid),          
R.driverid as DriverId,          
R.userId as CustomerId,          
R.pickupslot,        
R.deliverySlot,        
 overall_count = COUNT(*) OVER()            
 from [order] R          
           
 WHERE                      
 (                    
  @RelationTable IS NULL                                                                    
       or                                                            
       (                                                                          
           @RelationTable = 'NEW_ORDERS'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='NEW'                                        
            
            )                                                                  
       )                     
                    
   or                                                              
       (                                                                          
           @RelationTable = 'COLLECTING_ORDERS'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='ASSIGNED_DRIVER'                                        
            or                                         
    R.[status]='AWAITING_COLLECTION'                                        
            or                                         
            R.[status]='COLLECTED'                             
                                        
            )                                                                  
       )                    
    or                                                                        
       (                        
           @RelationTable = 'CLEANING_ORDERS'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='CLEANING_IN_PROGRESS'                                        
            or                                         
            R.[status]='CLEANING_COMPLETED'                                        
                                        
                           
            )                                                                  
       )                    
    or                                                                        
       (                                                                          
           @RelationTable = 'DELIVERY_ORDERS'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='AWAITING_COLLECTION_DELIVERY'                                        
            or                                         
            R.[status]='ON_THE_WAY'                                        
                                        
                                        
            )                                                                  
       )                    
     or                                                                        
       (                                                                          
           @RelationTable = 'COLLECTING_ORDERS_ASSIGNED'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='ASSIGNED_DRIVER'                                        
            or                                         
            R.[status]='AWAITING_COLLECTION'                                        
                                        
                                        
            )                                                                  
       )                    
    or                                                                        
       (                                                                          
           @RelationTable = 'TOTAL_ORDERS'                                                                        
           and                                                                      
           (                                                                  
            R.[status]='NEW'                                        
            or                                         
            R.[status]='ASSIGNED_DRIVER'                                        
            or                                         
            R.[status]='AWAITING_COLLECTION'                             
             or                                         
            R.[status]='COLLECTED'                     
   or                                         
            R.[status]='CLEANING_IN_PROGRESS'                     
   or                     
            R.[status]='CLEANING_COMPLETED'                     
   or                                         
            R.[status]='ASSIGNED_DRIVER_FOR_DELIVERY'                     
   or                                         
            R.[status]='ON_THE_WAY'                     
            )                                              
       )                    
 )                                                
 AND                                                      
 (                                                      
  @Id IS NULL                                   
  OR                                                      
  R.[Id] = @Id                                                 
 )                                                      
 AND                                                      
 (                                                      
  @Slug IS NULL                                                      
  OR                                                      
  R.[Slug] = @Slug                  
 )                                                      
 AND                                                      
 (                                                      
  R.[IsDeleted] = 0                                                      
 )                                                      
 AND                                                      
 (                                                      
  @TenantId IS NULL                                                      
  OR                                        
  R.[TenantId] = @TenantId                                                      
 )                                                      
 AND                                                      
 (                              
  @userid IS NULL                                                      
   OR                                                      
  R.userid =  @userid                                                      
 )                                          
                                        
  AND                                                      
 (                                                      
  @DriverId IS NULL                                                      
   OR                                                      
  R.driverid =  @DriverId                                                      
 )                                       
  AND                                                      
 (                                                      
  @Status IS NULL                                     
   OR                                                      
  R.[Status] =  @Status                                                      
 )                                       
  AND                                                      
 (                                                      
  @DistrictId IS NULL                                                      
   OR                                                      
  R.id in(  select orderid from deliveryaddress where districtid=@DistrictId )                                                       
 )                                       
 AND                                              
 (                                                      
  @SubDistrictId IS NULL                                                      
   OR                  
  R.id in(  select orderId from deliveryaddress where SubDistrictId=@SubDistrictId )                                                       
 )                                       
 -- AND                                                      
 --(                                                      
 -- @Name IS NULL                                                      
 --  OR                                               
 -- o.userid in (select id from users where [FirstName] like '%'+@Name+'%' or LastName like '%'+@Name+'%' )                                                      
 --)                                         
   AND                                                      
 (                                                      
  @email IS NULL                           
   OR                                  
  R.userid in (select id from users where [Email] like '%'+@email+'%')                                      
                                          
                                      
 )                                         
  AND                                                      
 (                                                      
  @Phone IS NULL          
   OR                                                      
  R.userid in (select id from users where phonenumber like '%'+@Phone+'%')                                         
   OR                                                      
  R.userid in (select userid from DeliveryAddress where PhoneNo like '%'+@Phone+'%')                                        
  OR                                                      
  R.userid in (select userid from DeliveryAddress where alterNumber like '%'+@Phone+'%')                      
 )                            
 And                      
 (                      
  @deliveryType is null                      
  or                       
  R.deliveryType=@deliveryType                      
                       
 )            
           ) as CTE                          
 --Order by o.Id desc       @SortColumn @SortOrder          
 ORDER BY            
           -- CreatedOn desc,
                          
           -- CASE    WHEN @SortColumn='DEFAULT' AND DeliveryType='SAMEDAY'  THEN 1   WHEN @SortColumn='DEFAULT' AND DeliveryType='EXPRESS'    THEN 2     WHEN @SortColumn='DEFAULT' AND DeliveryType= 'NORMAL'   THEN 3   END   ASC    ,          
               
                     
          
            CASE WHEN (@SortColumn = 'ORDER_ID' AND @SortOrder='ASC')   THEN id   END ASC,            
            CASE WHEN (@SortColumn = 'ORDER_ID' AND @SortOrder='DESC')  THEN id    END DESC,            
          
            CASE WHEN (@SortColumn = 'CUSTOMER_FULLNAME' AND @SortOrder='ASC')   THEN customerName   END ASC,            
            CASE WHEN (@SortColumn = 'CUSTOMER_FULLNAME' AND @SortOrder='DESC')  THEN customerName    END DESC,           
              
          CASE WHEN (@SortColumn = 'CREATED_ON' AND @SortOrder='ASC')   THEN createdOn   END ASC,            
            CASE WHEN (@SortColumn = 'CREATED_ON' AND @SortOrder='DESC')  THEN createdOn    END DESC,          
             
   CASE WHEN (@SortColumn = 'PICKUP_DATE' AND @SortOrder='ASC')   THEN pickupdate   END ASC,            
            CASE WHEN (@SortColumn = 'PICKUP_DATE' AND @SortOrder='DESC')  THEN pickupdate    END DESC ,          
          
   CASE WHEN (@SortColumn = 'DELIVERY_DATE' AND @SortOrder='ASC')   THEN DeliveryDate   END ASC,            
            CASE WHEN (@SortColumn = 'DELIVERY_DATE' AND @SortOrder='DESC')  THEN DeliveryDate    END DESC ,          
          
   CASE WHEN (@SortColumn = 'DELIVERY_TYPE' AND @SortOrder='ASC')   THEN DeliveryType   END ASC,            
            CASE WHEN (@SortColumn = 'DELIVERY_TYPE' AND @SortOrder='DESC')  THEN DeliveryType    END DESC,           
          
   CASE WHEN (@SortColumn = 'DISTRICT_NAME' AND @SortOrder='ASC')   THEN districtname    END ASC,            
            CASE WHEN (@SortColumn = 'DISTRICT_NAME' AND @SortOrder='DESC')  THEN districtname     END DESC ,          
          
   CASE WHEN (@SortColumn = 'SUBDISTRICT_NAME' AND @SortOrder='ASC')   THEN  Subdistrictname   END ASC,            
            CASE WHEN (@SortColumn = 'SUBDISTRICT_NAME' AND @SortOrder='DESC')  THEN  Subdistrictname    END DESC ,          
          
   CASE WHEN (@SortColumn = 'TOTAL_PRICE' AND @SortOrder='ASC')   THEN TotalPrice   END ASC,            
            CASE WHEN (@SortColumn = 'TOTAL_PRICE' AND @SortOrder='DESC')  THEN TotalPrice    END DESC ,          
          
   CASE WHEN (@SortColumn = 'PAYMENT_STATUS' AND @SortOrder='ASC')   THEN PaymentStatus   END ASC,            
            CASE WHEN (@SortColumn = 'PAYMENT_STATUS' AND @SortOrder='DESC')  THEN PaymentStatus    END DESC ,          
          
   CASE WHEN (@SortColumn = 'PAYMENT_TYPE' AND @SortOrder='ASC')   THEN PaymentType   END ASC,            
            CASE WHEN (@SortColumn = 'PAYMENT_TYPE' AND @SortOrder='DESC')  THEN PaymentType    END DESC ,          
          
   CASE WHEN (@SortColumn = 'INVOICE_NO' AND @SortOrder='ASC')   THEN id   END ASC,            
            CASE WHEN (@SortColumn = 'INVOICE_NO' AND @SortOrder='DESC')  THEN id    END DESC ,          
          
   CASE WHEN (@SortColumn = 'STATUS' AND @SortOrder='ASC')   THEN [status]   END ASC,            
            CASE WHEN (@SortColumn = 'STATUS' AND @SortOrder='DESC')  THEN  [status]    END DESC ,          
          
   CASE WHEN (@SortColumn = 'DRIVER_FULLNAME' AND @SortOrder='ASC')   THEN driverName   END ASC,            
            CASE WHEN (@SortColumn = 'DRIVER_FULLNAME' AND @SortOrder='DESC')  THEN driverName    END DESC           
           ,
      CreatedOn desc
              
 OFFSET (@next*@offset)-@next ROWS                                                      
    FETCH NEXT @next ROWS ONLY                                                      
                                                      
END