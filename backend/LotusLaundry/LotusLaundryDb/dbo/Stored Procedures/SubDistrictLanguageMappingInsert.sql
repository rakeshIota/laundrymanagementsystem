﻿CREATE PROCEDURE [dbo].[SubDistrictLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@SubDistrictId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [SubDistrictLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[LanguageId],[SubDistrictId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@LanguageId,@SubDistrictId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : SubDistrictLanguageMappingUpdate

/***** Object:  StoredProcedure  [dbo].[SubDistrictLanguageMappingUpdate] *****/
SET ANSI_NULLS ON