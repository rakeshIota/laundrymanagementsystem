﻿CREATE PROCEDURE [dbo].[DistrictInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Name        NVARCHAR(100)=NULL,
			@CityId      BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [District]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Name],[CityId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Name,@CityId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 

---------------------------------------------------
---------------------------------------------------
-- Procedure : DistrictUpdate

/***** Object:  StoredProcedure  [dbo].[DistrictUpdate] *****/
SET ANSI_NULLS ON