﻿CREATE TABLE [dbo].[DefaultMessage] (
    [Id]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]  INT                NOT NULL,
    [Slug]      NVARCHAR (255)     NULL,
    [CreatedBy] BIGINT             NOT NULL,
    [UpdatedBy] BIGINT             NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_DefaultMessage_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_DefaultMessage_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted] BIT                CONSTRAINT [DF_DefaultMessage_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT                CONSTRAINT [DF_DefaultMessage_IsActive] DEFAULT ((1)) NOT NULL,
    [Type]      NVARCHAR (100)     NOT NULL,
    CONSTRAINT [PK_DefaultMessage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

