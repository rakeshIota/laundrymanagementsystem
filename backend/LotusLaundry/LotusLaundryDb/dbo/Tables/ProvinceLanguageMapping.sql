﻿CREATE TABLE [dbo].[ProvinceLanguageMapping] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]   INT                NOT NULL,
    [Slug]       NVARCHAR (255)     NULL,
    [CreatedBy]  BIGINT             NOT NULL,
    [UpdatedBy]  BIGINT             NOT NULL,
    [CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_ProvinceLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_ProvinceLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]  BIT                CONSTRAINT [DF_ProvinceLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]   BIT                CONSTRAINT [DF_ProvinceLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [ProvinceId] BIGINT             NULL,
    [LanguageId] BIGINT             NULL,
    [Name]       NVARCHAR (100)     NULL,
    CONSTRAINT [PK_ProvinceLanguageMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

