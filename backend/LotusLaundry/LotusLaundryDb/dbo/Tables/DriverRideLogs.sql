﻿CREATE TABLE [dbo].[DriverRideLogs] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [StartOn]    DATETIMEOFFSET (7) CONSTRAINT [DF_DriverRideLogs_StartOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [CompleteOn] DATETIMEOFFSET (7) CONSTRAINT [DF_DriverRideLogs_CompleteOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsComplete] BIT                CONSTRAINT [DF_DriverRideLogs_IsComplete] DEFAULT ((1)) NOT NULL,
    [DriverId]   BIGINT             NOT NULL,
    [OrderId]    BIGINT             NOT NULL,
    [Type]       NVARCHAR (100)     NULL,
    CONSTRAINT [PK_DriverRideLogs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

