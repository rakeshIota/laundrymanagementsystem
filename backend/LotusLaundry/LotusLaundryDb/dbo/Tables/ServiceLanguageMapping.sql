﻿CREATE TABLE [dbo].[ServiceLanguageMapping] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]    INT                NOT NULL,
    [Slug]        NVARCHAR (255)     NULL,
    [CreatedBy]   BIGINT             NOT NULL,
    [UpdatedBy]   BIGINT             NOT NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_ServiceLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_ServiceLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]   BIT                CONSTRAINT [DF_ServiceLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_ServiceLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (100)     NOT NULL,
    [LanguageId]  BIGINT             NOT NULL,
    [ServiceId]   BIGINT             NOT NULL,
    [Description] NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_ServiceLanguageMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

