﻿CREATE TABLE [dbo].[Notification] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]   INT                NOT NULL,
    [Slug]       NVARCHAR (255)     NULL,
    [CreatedBy]  BIGINT             NOT NULL,
    [UpdatedBy]  BIGINT             NOT NULL,
    [CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Notification_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Notification_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]  BIT                CONSTRAINT [DF_Notification_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]   BIT                CONSTRAINT [DF_Notification_IsActive] DEFAULT ((1)) NOT NULL,
    [UserId]     BIGINT             NULL,
    [Message]    NVARCHAR (MAX)     NULL,
    [LanguageId] BIGINT             NULL,
    [Type]       NVARCHAR (100)     NULL,
    [Token]      NVARCHAR (100)     NULL,
    [Status]     NVARCHAR (100)     NULL,
    [Response]   NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED ([Id] ASC)
);

