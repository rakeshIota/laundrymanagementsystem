﻿CREATE TABLE [dbo].[NotificationMasterKeyValueMapping] (
    [Id]             BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]       INT                NOT NULL,
    [Slug]           NVARCHAR (255)     NULL,
    [CreatedBy]      BIGINT             NOT NULL,
    [UpdatedBy]      BIGINT             NOT NULL,
    [CreatedOn]      DATETIMEOFFSET (7) CONSTRAINT [DF_NotificationMasterKeyValueMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]      DATETIMEOFFSET (7) CONSTRAINT [DF_NotificationMasterKeyValueMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]      BIT                CONSTRAINT [DF_NotificationMasterKeyValueMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]       BIT                CONSTRAINT [DF_NotificationMasterKeyValueMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [Key]            NVARCHAR (100)     NULL,
    [Value]          NVARCHAR (100)     NULL,
    [NotificationId] BIGINT             NULL,
    CONSTRAINT [PK_NotificationMasterKeyValueMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

