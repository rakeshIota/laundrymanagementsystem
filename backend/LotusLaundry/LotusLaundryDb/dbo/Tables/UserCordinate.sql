﻿CREATE TABLE [dbo].[UserCordinate] (
    [Id]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]  INT                NOT NULL,
    [Slug]      NVARCHAR (255)     NULL,
    [CreatedBy] BIGINT             NOT NULL,
    [UpdatedBy] BIGINT             NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_UserCordinate_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_UserCordinate_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted] BIT                CONSTRAINT [DF_UserCordinate_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT                CONSTRAINT [DF_UserCordinate_IsActive] DEFAULT ((1)) NOT NULL,
    [UserId]    BIGINT             NOT NULL,
    [Latitude]  DECIMAL (18, 2)    NOT NULL,
    [Longitude] DECIMAL (18, 2)    NOT NULL,
    [OrderId]   BIGINT             NULL,
    CONSTRAINT [PK_UserCordinate] PRIMARY KEY CLUSTERED ([Id] ASC)
);

