﻿CREATE TABLE [dbo].[DriverLocation] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]    INT                NOT NULL,
    [Slug]        NVARCHAR (255)     NULL,
    [CreatedBy]   BIGINT             NOT NULL,
    [UpdatedBy]   BIGINT             NOT NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_DriverLocation_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_DriverLocation_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]   BIT                CONSTRAINT [DF_DriverLocation_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_DriverLocation_IsActive] DEFAULT ((1)) NOT NULL,
    [Longititude] DECIMAL (18, 6)    NULL,
    [Latitude]    DECIMAL (18, 6)    NULL,
    [userId]      BIGINT             NULL,
    CONSTRAINT [PK_DriverLocation] PRIMARY KEY CLUSTERED ([Id] ASC)
);

