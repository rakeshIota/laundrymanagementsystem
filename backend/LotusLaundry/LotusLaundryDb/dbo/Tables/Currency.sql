﻿CREATE TABLE [dbo].[Currency] (
    [Id]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]  INT                NOT NULL,
    [Slug]      NVARCHAR (255)     NULL,
    [CreatedBy] BIGINT             NOT NULL,
    [UpdatedBy] BIGINT             NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_Currency_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_Currency_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted] BIT                CONSTRAINT [DF_Currency_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT                CONSTRAINT [DF_Currency_IsActive] DEFAULT ((1)) NOT NULL,
    [Price]     DECIMAL (18, 2)    NOT NULL,
    CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED ([Id] ASC)
);

