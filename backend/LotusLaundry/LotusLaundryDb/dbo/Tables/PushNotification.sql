﻿CREATE TABLE [dbo].[PushNotification] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [CreatedBy]   BIGINT             NULL,
    [UpdatedBy]   BIGINT             NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_PushNotification_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [UpdatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_PushNotification_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [IsDeleted]   BIT                CONSTRAINT [DF_PushNotification_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_PushNotification_IsActive] DEFAULT ((1)) NOT NULL,
    [Title]       NVARCHAR (256)     NULL,
    [Message]     NVARCHAR (256)     NULL,
    [Type]        NVARCHAR (256)     NULL,
    [IsArchive]   BIT                CONSTRAINT [DF_PushNotification_IsArchive] DEFAULT ((0)) NOT NULL,
    [TargetId]    BIGINT             NULL,
    [TargetType]  NVARCHAR (256)     NULL,
    [UserId]      BIGINT             NULL,
    [IsSeen]      BIT                CONSTRAINT [DF_PushNotification_IsSeen] DEFAULT ((0)) NOT NULL,
    [UserRole]    NVARCHAR (20)      NULL,
    [ArchivedBy]  BIGINT             NULL,
    [ArchiveDate] DATETIMEOFFSET (7) NULL,
    CONSTRAINT [PK_PushNotification] PRIMARY KEY CLUSTERED ([Id] ASC)
);

