﻿CREATE TABLE [dbo].[CurrencyLanguageMapping] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]   INT                NOT NULL,
    [Slug]       NVARCHAR (255)     NULL,
    [CreatedBy]  BIGINT             NOT NULL,
    [UpdatedBy]  BIGINT             NOT NULL,
    [CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CurrencyLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CurrencyLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]  BIT                CONSTRAINT [DF_CurrencyLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]   BIT                CONSTRAINT [DF_CurrencyLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [CurrencyId] BIGINT             NOT NULL,
    [LanguageId] BIGINT             NOT NULL,
    [Name]       NVARCHAR (100)     NOT NULL,
    CONSTRAINT [PK_CurrencyLanguageMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

