﻿CREATE TABLE [dbo].[Tenant] (
    [Id]        INT                IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (250)     NULL,
    [Address]   NVARCHAR (250)     NULL,
    [PhoneNo]   NVARCHAR (20)      NULL,
    [Email]     NVARCHAR (50)      NULL,
    [IsDeleted] BIT                CONSTRAINT [DF_Tanant_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT                CONSTRAINT [DF_Tanant_IsActive] DEFAULT ((1)) NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_Tanant_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [UpdatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_Tanant_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    [CreatedBy] BIGINT             NULL,
    [UpdatedBy] BIGINT             NULL,
    [UniqueId]  UNIQUEIDENTIFIER   CONSTRAINT [DF_Tenant_UniqueId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_Tanant] PRIMARY KEY CLUSTERED ([Id] ASC)
);

