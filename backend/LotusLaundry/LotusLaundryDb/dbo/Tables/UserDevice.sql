﻿CREATE TABLE [dbo].[UserDevice] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [DeviceId]    NVARCHAR (250)     NULL,
    [UserId]      BIGINT             NULL,
    [DeviceType]  NVARCHAR (50)      NULL,
    [UserType]    NVARCHAR (50)      NULL,
    [IsDeleted]   BIT                NOT NULL,
    [DeviceToken] NVARCHAR (250)     NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_UserDevice_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
    CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED ([Id] ASC)
);



