﻿CREATE TABLE [dbo].[Service] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]    INT                NOT NULL,
    [Slug]        NVARCHAR (255)     NULL,
    [CreatedBy]   BIGINT             NOT NULL,
    [UpdatedBy]   BIGINT             NOT NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_Service_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_Service_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]   BIT                CONSTRAINT [DF_Service_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_Service_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (100)     NULL,
    [Description] NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([Id] ASC)
);

