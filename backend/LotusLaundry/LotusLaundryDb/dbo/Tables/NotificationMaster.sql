﻿CREATE TABLE [dbo].[NotificationMaster] (
    [Id]          BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]    INT                NOT NULL,
    [Slug]        NVARCHAR (255)     NULL,
    [CreatedBy]   BIGINT             NOT NULL,
    [UpdatedBy]   BIGINT             NOT NULL,
    [CreatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_NotificationMaster_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]   DATETIMEOFFSET (7) CONSTRAINT [DF_NotificationMaster_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]   BIT                CONSTRAINT [DF_NotificationMaster_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]    BIT                CONSTRAINT [DF_NotificationMaster_IsActive] DEFAULT ((1)) NOT NULL,
    [Type]        NVARCHAR (100)     NULL,
    [CanBeDelete] BIT                CONSTRAINT [DF_NotificationMaster_CanBeDelete] DEFAULT ((1)) NOT NULL,
    [Message]     NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_NotificationMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

