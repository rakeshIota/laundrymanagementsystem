﻿CREATE TABLE [dbo].[UserTwilioCallingStatus] (
    [Id]       BIGINT IDENTITY (1, 1) NOT NULL,
    [FromId]   BIGINT NULL,
    [ToId]     BIGINT NULL,
    [IsActive] BIT    NULL
);

