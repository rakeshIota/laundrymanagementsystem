﻿CREATE TABLE [dbo].[Rating] (
    [Id]                  BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]            INT                NOT NULL,
    [Slug]                NVARCHAR (255)     NULL,
    [CreatedBy]           BIGINT             NOT NULL,
    [UpdatedBy]           BIGINT             NOT NULL,
    [CreatedOn]           DATETIMEOFFSET (7) CONSTRAINT [DF_Rating_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]           DATETIMEOFFSET (7) CONSTRAINT [DF_Rating_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]           BIT                CONSTRAINT [DF_Rating_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]            BIT                CONSTRAINT [DF_Rating_IsActive] DEFAULT ((1)) NOT NULL,
    [orderId]             BIGINT             NULL,
    [Quality]             BIGINT             NULL,
    [DeliveryService]     BIGINT             NULL,
    [Staff]               BIGINT             NULL,
    [Packaging]           BIGINT             NULL,
    [OverallSatisfaction] BIGINT             NULL,
    [Feedback]            NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED ([Id] ASC)
);

