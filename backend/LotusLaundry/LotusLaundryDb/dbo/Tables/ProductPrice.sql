﻿CREATE TABLE [dbo].[ProductPrice] (
    [Id]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]  INT                NOT NULL,
    [Slug]      NVARCHAR (255)     NULL,
    [CreatedBy] BIGINT             NOT NULL,
    [UpdatedBy] BIGINT             NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_ProductPrice_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_ProductPrice_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted] BIT                CONSTRAINT [DF_ProductPrice_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]  BIT                CONSTRAINT [DF_ProductPrice_IsActive] DEFAULT ((1)) NOT NULL,
    [ServiceId] BIGINT             NOT NULL,
    [ProductId] BIGINT             NOT NULL,
    [Price]     DECIMAL (18, 2)    NULL,
    CONSTRAINT [PK_ProductPrice] PRIMARY KEY CLUSTERED ([Id] ASC)
);

