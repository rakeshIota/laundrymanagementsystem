﻿CREATE TABLE [dbo].[DefaultMessageLanguageMapping] (
    [Id]               BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]         INT                NOT NULL,
    [Slug]             NVARCHAR (255)     NULL,
    [CreatedBy]        BIGINT             NOT NULL,
    [UpdatedBy]        BIGINT             NOT NULL,
    [CreatedOn]        DATETIMEOFFSET (7) CONSTRAINT [DF_DefaultMessageLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]        DATETIMEOFFSET (7) CONSTRAINT [DF_DefaultMessageLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]        BIT                CONSTRAINT [DF_DefaultMessageLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]         BIT                CONSTRAINT [DF_DefaultMessageLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [DefaultMessageId] BIGINT             NOT NULL,
    [LanguageId]       BIGINT             NOT NULL,
    [Message]          NVARCHAR (MAX)     NOT NULL,
    CONSTRAINT [PK_DefaultMessageLanguageMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

