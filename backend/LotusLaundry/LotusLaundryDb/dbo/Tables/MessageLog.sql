﻿CREATE TABLE [dbo].[MessageLog] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]   INT                NOT NULL,
    [Slug]       NVARCHAR (255)     NULL,
    [CreatedBy]  BIGINT             NOT NULL,
    [UpdatedBy]  BIGINT             NOT NULL,
    [CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_MessageLog_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_MessageLog_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]  BIT                CONSTRAINT [DF_MessageLog_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]   BIT                CONSTRAINT [DF_MessageLog_IsActive] DEFAULT ((1)) NOT NULL,
    [UserId]     BIGINT             NULL,
    [Message]    NVARCHAR (MAX)     NULL,
    [LanguageId] BIGINT             NULL,
    [Type]       NVARCHAR (50)      NULL,
    CONSTRAINT [PK_MessageLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

