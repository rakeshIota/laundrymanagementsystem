﻿CREATE TABLE [dbo].[PackingLanguageMapping] (
    [Id]         BIGINT             IDENTITY (1, 1) NOT NULL,
    [TenantId]   INT                NOT NULL,
    [Slug]       NVARCHAR (255)     NULL,
    [CreatedBy]  BIGINT             NOT NULL,
    [UpdatedBy]  BIGINT             NOT NULL,
    [CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PackingLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PackingLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]  BIT                CONSTRAINT [DF_PackingLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]   BIT                CONSTRAINT [DF_PackingLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
    [PackingId]  BIGINT             NOT NULL,
    [LanguageId] BIGINT             NOT NULL,
    [Name]       NVARCHAR (100)     NOT NULL,
    CONSTRAINT [PK_PackingLanguageMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

