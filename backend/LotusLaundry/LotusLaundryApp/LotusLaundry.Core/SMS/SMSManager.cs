﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace LotusLaundry.Core.SMS
{
    class SMSManager
    {
        public static string SendSMS(string toMobileNumber, string smsBody)
        {
            TwilioClient.Init(ConfigurationManager.AppSettings["TWILIOACCOUNTSID"], ConfigurationManager.AppSettings["TWILIOAUTHTOKEN"]);
            string fromPhoneNumber = ConfigurationManager.AppSettings["FROM_TWILIO_PHONE_NUMBER"];
            if (toMobileNumber != "7889153789")
            {
                toMobileNumber = "+1" + toMobileNumber;
            }
            else
            {
                toMobileNumber = "+91" + toMobileNumber;
            }

            var message = MessageResource.Create(body: smsBody,
               //from: new Twilio.Types.PhoneNumber("+15017122661"),
               //to: new Twilio.Types.PhoneNumber(MobileNumber)
               from: new Twilio.Types.PhoneNumber(fromPhoneNumber),
               to: new Twilio.Types.PhoneNumber(toMobileNumber));
            return message.Sid;
        }
    }
}
