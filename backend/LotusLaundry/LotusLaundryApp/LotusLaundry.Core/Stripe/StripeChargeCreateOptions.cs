﻿namespace LotusLaundry.Core.Stripe
{
    internal class StripeChargeCreateOptions
    {
        public StripeChargeCreateOptions()
        {
        }

        public string ReceiptEmail { get; set; }
        public int Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public string SourceTokenOrExistingSourceId { get; set; }
        public string Customer { get; internal set; }
        public string Source { get; internal set; }
    }
}