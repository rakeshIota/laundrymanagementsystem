﻿using LotusLaundry.Core.Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

using LotusLaundry.Domain.Stripe;
using Stripe;
//using LotusLaundry.Domain.Stripe;

namespace LotusLaundry.Core.Stripe
{
    public class StripePaymentService
    {
        public StripeResponseModel MakePayment(string description, string email, string stripeTokenId, decimal? amount)
        {
            StripeConfiguration.SetApiKey(StripeInfo.STRIPEAPIKEY);
            try
            {

                //string key = getKeys();
                //StripeConfiguration.ApiKey = key;

                //Amount = Math.Round(Convert.ToDecimal(Amount), 2, MidpointRounding.AwayFromZero);
                //var options = new ChargeCreateOptions();
                ////myCharge.Amount = (int)(model.Amount * 100);

                //options.Amount = Convert.ToInt32(Amount * 100);
                //options.Currency = CurrencyCode;
                //options.Description = "One Time Payment- Product Create";
                ////options.ReceiptEmail = model.Email;

                //options.Customer = stripeCustomerId;
                //options.Source = stripeCardId;



                //var service = new ChargeService();
                //Charge charge = service.Create(options);
                return new StripeResponseModel
                {
                    Status = false,
                    Message = ""
                };
            }
            catch (Exception ex)
            {
                return new StripeResponseModel
                {
                    Status = false,
                    Message = ex.Message.ToString()
                };
            }
        }


        public static string getKeys()
        {
            string key = "";

            string mode = StripeInfo.is_Live_Mode;

            if (mode == "1")
            {
                key = StripeInfo.STRIPEAPIKEY;
            }
            else
            {
                key = StripeInfo.STRIPEAPITESTKEY;
            }
            return key;
        }
    }
    internal class StripeInfo
    {
        public readonly static string STRIPEPUBLISHABLEKEY = ConfigurationManager.AppSettings["STRIPEPUBLISHABLEKEY"];
        public readonly static string STRIPEAPIKEY = ConfigurationManager.AppSettings["STRIPEAPIKEY"];

        public readonly static string STRIPEPUBLISHABLETESTKEY = ConfigurationManager.AppSettings["STRIPEPUBLISHABLETESTKEY"];
        public readonly static string STRIPEAPITESTKEY = ConfigurationManager.AppSettings["STRIPEAPITESTKEY"];


        public readonly static string is_Live_Mode = ConfigurationManager.AppSettings["is_Live_Mode"];

    }
}
