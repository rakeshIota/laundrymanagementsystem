﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace LotusLaundry.Core.PushNotification
{
    public static class PushNotificationManager
    {
        private class PushNotificationMessage
        {
            public string Title;
            public string Message;
            public string ItemId;
        }

        public static async Task<string> SendNotificationAsync(List<string> deviceRegIds, string message, string title, string targetId, string type)
        {

            string SERVER_API_KEY = "AAAAq-hZlzA:APA91bFCLP4DZSfh-YbQZzTrZo2_dx2KifKkwLEifJCcgmNfJ1rVTE74dJ-SH9Gbg_Km2F1Jh5WlUEIONtS5QJdd25jGZmFgHmamdLeaa9q1NjPwxfwDh-10GR8M3fphXf4yhbkH75ZV";
            var SENDER_ID = "12";
            string regIds = string.Join("\",\"", deviceRegIds);

            PushNotificationMessage notificationMessage = new PushNotificationMessage();
            notificationMessage.Title = title;
            notificationMessage.Message = message;
            notificationMessage.ItemId = (targetId);

            var value = new JavaScriptSerializer().Serialize(notificationMessage);

            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            var postData = new
            {
                registration_ids = deviceRegIds,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = message,
                    title = title,
                    targetId = targetId,
                    sound = "default",
                    //soundname = "customsound",
                    alert = true,
                    contentavailable = 1,
                    badge = "1"
                },
                data = new
                {
                    targetId = targetId,
                    title = title,
                    message = message,
                    type = type,
                    //soundname = "customsound",
                },
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);
            tRequest.ContentLength = byteArray.Length;
            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();
            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }
    }
}
