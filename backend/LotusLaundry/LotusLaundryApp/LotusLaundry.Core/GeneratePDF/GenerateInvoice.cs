﻿

using LotusLaundry.Domain.Order;

using LotusLaundry.Domain.Users;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Domain.UserAddress;
using System.Configuration;
using System.Globalization;

namespace LotusLaundry.Core.GeneratePDF
{
    public static class GenerateInvoice
    {

        public static class Cultures
        {
            public static readonly CultureInfo UnitedKingdom =
                CultureInfo.GetCultureInfo("th-TH");
        }
        public static string GenerateOrderInvoice(UsersModel user, List<OrderItemModel> product, decimal? payableAmount, OrderModel order)
        {

            using (System.IO.MemoryStream memoryStream = new MemoryStream())
            {
                Document document = new Document();
                document.SetMargins(-50, -50, 20, 20);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                document.SetMargins(-50, -50, 20, 20);

                PdfPTable spacetop = new PdfPTable(3);
                spacetop.SpacingAfter = 20;
                document.Add(spacetop);

                string order_id = order.Id.ToString().PadLeft(6, '0');

                decimal? productAmount = payableAmount;
                int? taxPercentage = 10;  // todo 
                // productAmount = product.IsABNLookup == true ? (productAmount - product.AsicPrice) : productAmount;
                productAmount = decimal.Round(Convert.ToDecimal(productAmount), 2, MidpointRounding.AwayFromZero);

                decimal? gst = (productAmount / 11);

                gst = decimal.Round(Convert.ToDecimal(gst), 2, MidpointRounding.AwayFromZero);


                decimal? productAmountWithoutGst = productAmount - gst;


                decimal? creditAmount = (order.TotalPrice != 0 && order.TotalPrice != null) ? order.TotalPrice : 0;

                productAmountWithoutGst = decimal.Round(Convert.ToDecimal(productAmountWithoutGst), 2, MidpointRounding.AwayFromZero);


                decimal? subTotal = productAmountWithoutGst;
               
                //subTotal = product.IsABNLookup == true ? (subTotal + product.AsicPrice) : subTotal;
                subTotal = decimal.Round(Convert.ToDecimal(subTotal), 2, MidpointRounding.AwayFromZero);


                string invoiceDate = String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn);

                decimal? productTotal = gst + subTotal;

               // BaseColor customColor = WebColors.GetRGBColor("#91ca3d");
                BaseColor customColor = WebColors.GetRGBColor("#4cc2ba");


                PdfPTable header = new PdfPTable(3);
                header.DefaultCell.Border = Rectangle.NO_BORDER;


                //add logo to table 
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/logo.png");
               // jpg.ScaleAbsolute(100f, 30f);
                jpg.ScaleToFit(100f, 30f);
                PdfPCell logo = new PdfPCell(jpg);
                //companyPhoneNumber.HorizontalAlignment = Element.ALIGN_RIGHT;
                logo.Border = Rectangle.NO_BORDER;
                logo.PaddingTop = 20f;
                logo.Colspan = 2;
                header.AddCell(logo);
                //add logo to table 

                //add company detail
                PdfPTable CompanyDetail = new PdfPTable(1);
                CompanyDetail.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell CompanyABN = new PdfPCell(new Phrase("25 Oriental Towers ", new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyABN.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN);

                PdfPCell CompanyABN1 = new PdfPCell(new Phrase("Ekamai Road,Bangkok 10110", new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN1.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyABN1.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN1);

                //PdfPCell CompanyABN2 = new PdfPCell(new Phrase("9 Sherwood Road", new Font(Font.FontFamily.HELVETICA, 10F)));
                //CompanyABN2.HorizontalAlignment = Element.ALIGN_LEFT;
                //CompanyABN2.Border = Rectangle.NO_BORDER;
                //CompanyDetail.AddCell(CompanyABN2);

                //PdfPCell CompanyABN3 = new PdfPCell(new Phrase("Toowong Qld 4066", new Font(Font.FontFamily.HELVETICA, 10F)));
                //CompanyABN3.HorizontalAlignment = Element.ALIGN_LEFT;
                //CompanyABN3.Border = Rectangle.NO_BORDER;
                //CompanyDetail.AddCell(CompanyABN3);

                //PdfPCell CompanyPhoneNumber = new PdfPCell(new Phrase("P: 1300 XXX XXX", new Font(Font.FontFamily.HELVETICA, 10F)));
                //CompanyPhoneNumber.HorizontalAlignment = Element.ALIGN_LEFT;
                //CompanyPhoneNumber.Border = Rectangle.NO_BORDER;
                //CompanyDetail.AddCell(CompanyPhoneNumber);


                PdfPCell CompanyEmail = new PdfPCell(new Phrase("E: info@lotuslaundry.com", new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyEmail.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyEmail.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyEmail);




                PdfPCell companyDetailCell = new PdfPCell(CompanyDetail);
                companyDetailCell.PaddingTop = 20;
                companyDetailCell.Border = Rectangle.NO_BORDER;
                header.AddCell(companyDetailCell);
                document.Add(header);
                //add company detail


                PdfPTable CustomerDetail = new PdfPTable(1);
                CustomerDetail.DefaultCell.Border = Rectangle.NO_BORDER;
                CustomerDetail.PaddingTop = 20;

                PdfPCell TaxInvoice = new PdfPCell(new Phrase("TAX INVOICE", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                TaxInvoice.HorizontalAlignment = Element.ALIGN_LEFT;
                TaxInvoice.Border = Rectangle.NO_BORDER;
                CustomerDetail.AddCell(TaxInvoice);

                PdfPCell CustomerName = new PdfPCell(new Phrase(user.FirstName + " " + user.LastName, new Font(Font.FontFamily.HELVETICA, 10F)));
                CustomerName.HorizontalAlignment = Element.ALIGN_LEFT;
                CustomerName.Border = Rectangle.NO_BORDER;
                CustomerName.PaddingTop = 10;
                CustomerDetail.AddCell(CustomerName);

                PdfPCell CustomerAddressLine1 = new PdfPCell(new Phrase(user.Address, new Font(Font.FontFamily.HELVETICA, 10F)));
                CustomerAddressLine1.HorizontalAlignment = Element.ALIGN_LEFT;
                CustomerAddressLine1.Border = Rectangle.NO_BORDER;
                CustomerDetail.AddCell(CustomerAddressLine1);


                PdfPCell CustomerEmail = new PdfPCell(new Phrase(user.Email, new Font(Font.FontFamily.HELVETICA, 10F)));
                CustomerEmail.HorizontalAlignment = Element.ALIGN_LEFT;
                CustomerEmail.Border = Rectangle.NO_BORDER;
                CustomerDetail.AddCell(CustomerEmail);


                PdfPCell InvoiceNumber = new PdfPCell(new Phrase("Invoice Number: " + order_id, new Font(Font.FontFamily.HELVETICA, 10F)));
                InvoiceNumber.HorizontalAlignment = Element.ALIGN_LEFT;
                InvoiceNumber.Border = Rectangle.NO_BORDER;
                InvoiceNumber.PaddingTop = 10;
                CustomerDetail.AddCell(InvoiceNumber);

                PdfPCell InvoiceDate = new PdfPCell(new Phrase("Invoice Date: " + String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn), new Font(Font.FontFamily.HELVETICA, 10F)));
                InvoiceDate.HorizontalAlignment = Element.ALIGN_LEFT;
                InvoiceDate.Border = Rectangle.NO_BORDER;
                InvoiceDate.PaddingTop = InvoiceDate.PaddingBottom = 10;
                CustomerDetail.AddCell(InvoiceDate);

                document.Add(CustomerDetail);

                //test code
                PdfPTable ProductHeaderDetail = new PdfPTable(11);
                ProductHeaderDetail.DefaultCell.Border = Rectangle.TOP_BORDER;

                AddTableHeader(ProductHeaderDetail, "Product Name", 3, Element.ALIGN_LEFT, customColor);
                AddTableHeader(ProductHeaderDetail, "Quantity", 3, Element.ALIGN_CENTER, customColor);
                AddTableHeader(ProductHeaderDetail, "Price", 3, Element.ALIGN_RIGHT, customColor);
                AddTableHeader(ProductHeaderDetail, "Total", 3, Element.ALIGN_CENTER, customColor);

                document.Add(ProductHeaderDetail);
                //end 

                PdfPTable ProductDetail = new PdfPTable(11);
               // ProductDetail.DefaultCell.Border = Rectangle.NO_BORDER;

                ProductDetail.DefaultCell.Border = Rectangle.TOP_BORDER;

                // AddTableHeader(ProductDetail, "Product Name", 3, Element.ALIGN_LEFT, customColor);
                // AddTableHeader(ProductDetail, "Quantity", 3, Element.ALIGN_CENTER, customColor);
                // AddTableHeader(ProductDetail, "Price", 3, Element.ALIGN_RIGHT, customColor);


                //AddTableHeader(ProductDetail, "Total", 3, Element.ALIGN_CENTER, customColor);




                foreach ( var item in product)
                {
                    AddProductCell(ProductDetail, "" + item.ProductName, 3, Element.ALIGN_LEFT);
                    AddProductCell(ProductDetail, "" + item.Quantity, 3, Element.ALIGN_CENTER);
                    AddProductCell(ProductDetail, string.Format("{0:n1}", item.SubTotal.ToString()), 3, Element.ALIGN_RIGHT);
                    AddProductCell(ProductDetail, String.Format("{0:n1}", item.TotalPrice.ToString().ToString()), 3, Element.ALIGN_CENTER);
                    document.Add(ProductDetail);
                }
                

                ///End Product Listing

                ///Product Cost
                PdfPTable ProductCost = new PdfPTable(9);
                ProductCost.SpacingBefore = 10;
                PdfPCell ProductCostBlank = new PdfPCell();
                ProductCostBlank.HorizontalAlignment = Element.ALIGN_LEFT;
                ProductCostBlank.Border = Rectangle.NO_BORDER;
                ProductCostBlank.Colspan = 4;
                ProductCostBlank.PaddingTop = 10;
                ProductCost.AddCell(ProductCostBlank);

                PdfPCell ProductSubTotal = new PdfPCell(new Phrase("Subtotal", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductSubTotal.BorderColor = customColor;
                ProductSubTotal.Border = Rectangle.TOP_BORDER;
                ProductSubTotal.Colspan = 3;
                ProductSubTotal.PaddingTop = 10;
                ProductCost.AddCell(ProductSubTotal);


                PdfPCell ProductSubTotalAmount = new PdfPCell(new Phrase(  order.SubTotal.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));

                ProductSubTotalAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductSubTotalAmount.Border = Rectangle.TOP_BORDER;
                ProductSubTotalAmount.BorderColor = customColor;
                ProductSubTotalAmount.Colspan = 2;
                ProductSubTotalAmount.PaddingTop = ProductSubTotalAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductSubTotalAmount);



                ProductCost.AddCell(ProductCostBlank);
                PdfPCell ProductGST = new PdfPCell(new Phrase("TAX", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductGST.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductGST.Border = Rectangle.NO_BORDER;
                ProductGST.Colspan = 3;
                ProductGST.PaddingTop = 10;
                ProductCost.AddCell(ProductGST);


                PdfPCell ProductGSTAmount = new PdfPCell(new Phrase( order.Tax.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductGSTAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductGSTAmount.Border = Rectangle.NO_BORDER;
                ProductGSTAmount.Colspan = 2;
                ProductGSTAmount.PaddingTop = ProductGSTAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductGSTAmount);


                ProductCost.AddCell(ProductCostBlank);
                PdfPCell ProductTotal = new PdfPCell(new Phrase("Total", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductTotal.Border = Rectangle.NO_BORDER;
                ProductTotal.Colspan = 3;
                ProductTotal.PaddingTop = ProductTotal.PaddingBottom = 10;
                ProductCost.AddCell(ProductTotal);


                PdfPCell ProductTotalAmount = new PdfPCell(new Phrase( productAmount.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductTotalAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductTotalAmount.Border = Rectangle.NO_BORDER;
                ProductTotalAmount.Colspan = 2;
                ProductTotalAmount.PaddingTop = ProductTotalAmount.PaddingBottom = ProductTotalAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductTotalAmount);





                ProductCost.AddCell(ProductCostBlank);

                

                PdfPCell BlankLine = new PdfPCell();
                BlankLine.Border = Rectangle.TOP_BORDER;
                BlankLine.BorderColor = customColor;
                BlankLine.Colspan = 9;
                ProductCost.AddCell(BlankLine);

                document.Add(ProductCost);
                ///Product Cost End
                ///Payment Type Start
                PdfPTable PaidType = new PdfPTable(7);
                PaidType.SpacingBefore = 10;
                PaidType.AddCell(ProductCostBlank);
                PdfPCell PaidInFullText = new PdfPCell(new Phrase("PAID IN FULL", new Font(Font.FontFamily.HELVETICA, 16F)));
                PaidInFullText.HorizontalAlignment = Element.ALIGN_CENTER;
                PaidInFullText.Border = Rectangle.NO_BORDER;
                PaidInFullText.BackgroundColor = customColor;
                PaidInFullText.Colspan = 4;
                PaidInFullText.PaddingBottom = 5;
                PaidType.AddCell(PaidInFullText);
                document.Add(PaidType);
                ///Payment Type End

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                string base64String = Convert.ToBase64String(bytes);
                memoryStream.Close();
                return base64String;

            }
        }


        public static string GenerateNewOrderInvoice(UsersModel user, List<OrderItemModel> product, decimal? payableAmount, OrderModel order)
        {

            using (System.IO.MemoryStream memoryStream = new MemoryStream())
            {


                 order.DeliveryAddress.FirstOrDefault();
               

                Document document = new Document();
                document.SetMargins(-50, -50, 20, 20);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                document.SetMargins(-50, -50, 20, 20);

                PdfPTable spacetop = new PdfPTable(3);
                spacetop.SpacingAfter = 20;
                document.Add(spacetop);

                string order_id = order.Id.ToString().PadLeft(6, '0');

                decimal? productAmount = payableAmount;
                int? taxPercentage = 10;  // todo 
                // productAmount = product.IsABNLookup == true ? (productAmount - product.AsicPrice) : productAmount;
                productAmount = decimal.Round(Convert.ToDecimal(productAmount), 2, MidpointRounding.AwayFromZero);

                UserAddressModel PickAndDeliveryAddress = new UserAddressModel();
                PickAndDeliveryAddress = order.DeliveryAddress.FirstOrDefault();

                var addressline1 = PickAndDeliveryAddress.BuildingName != null ? PickAndDeliveryAddress.BuildingName : PickAndDeliveryAddress.HouseNumber;
                var addressline2 = PickAndDeliveryAddress.Floor != null ? PickAndDeliveryAddress.Floor : null;


                var addressline3 = addressline2 != null ? PickAndDeliveryAddress.Floor + " " + PickAndDeliveryAddress.UnitNo + " " + PickAndDeliveryAddress.StreetNumber : PickAndDeliveryAddress.StreetNumber;
                var addressline4 = PickAndDeliveryAddress.ProvinceName + " " + PickAndDeliveryAddress.DistrictName;
                var addressline5 = PickAndDeliveryAddress.SubDistrictName + " " + PickAndDeliveryAddress.PostalCode;





                decimal? gst = (productAmount / 11);

                gst = decimal.Round(Convert.ToDecimal(gst), 2, MidpointRounding.AwayFromZero);


                decimal? productAmountWithoutGst = productAmount - gst;


                decimal? creditAmount = (order.TotalPrice != 0 && order.TotalPrice != null) ? order.TotalPrice : 0;

                productAmountWithoutGst = decimal.Round(Convert.ToDecimal(productAmountWithoutGst), 2, MidpointRounding.AwayFromZero);


                decimal? subTotal = productAmountWithoutGst;

                //subTotal = product.IsABNLookup == true ? (subTotal + product.AsicPrice) : subTotal;
                subTotal = decimal.Round(Convert.ToDecimal(subTotal), 2, MidpointRounding.AwayFromZero);


                string invoiceDate = String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn);

                decimal? productTotal = gst + subTotal;

                // BaseColor customColor = WebColors.GetRGBColor("#91ca3d");
                //BaseColor customColor = WebColors.GetRGBColor("#4cc2ba");
                BaseColor customColor = WebColors.GetRGBColor("#FFFFFF");



                PdfPTable header = new PdfPTable(3);
                header.DefaultCell.Border = Rectangle.NO_BORDER;


                //add logo to table 
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/logo.png");
                 //jpg.ScaleAbsolute(100f, 50f);
              
                jpg.ScaleToFit(150f, 100f);
                PdfPCell logo = new PdfPCell(jpg);
                //companyPhoneNumber.HorizontalAlignment = Element.ALIGN_RIGHT;
                logo.Border = Rectangle.NO_BORDER;
                logo.PaddingTop = 20f;
                logo.Colspan = 2;
                header.AddCell(logo);

                
                PdfPTable CompanyDetail = new PdfPTable(1);
                CompanyDetail.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell CompanyABN = new PdfPCell(new Phrase("Invoice Number", new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyABN.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN);

                PdfPCell CompanyABN1 = new PdfPCell(new Phrase("#"+order.Id, new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN1.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyABN1.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN1);

                PdfPCell CompanyEmail = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn), new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyEmail.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyEmail.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyEmail);




                PdfPCell companyDetailCell = new PdfPCell(CompanyDetail);
                companyDetailCell.PaddingTop = 20;
                companyDetailCell.Border = Rectangle.NO_BORDER;
                header.AddCell(companyDetailCell);
                document.Add(header);
                //add company detail
                PdfPTable spaceMid12 = new PdfPTable(1);
                spaceMid12.SpacingAfter = 120;
                document.Add(spaceMid12);

                PdfPTable CustomerDetail = new PdfPTable(3);
                CustomerDetail.DefaultCell.Border = Rectangle.NO_BORDER;
                CustomerDetail.PaddingTop = 20;

                PdfPCell TaxInvoice = new PdfPCell(new Phrase("Congratulation! Your Order is Completed!", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD,BaseColor.BLUE )));
                TaxInvoice.HorizontalAlignment = Element.ALIGN_CENTER;
                TaxInvoice.Border = Rectangle.NO_BORDER;
                TaxInvoice.Colspan = 3;
                CustomerDetail.AddCell(TaxInvoice);


                document.Add(CustomerDetail);


                Paragraph p1 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.WHITE, Element.ALIGN_LEFT, 1)));
                document.Add(p1);

                document.Add(Chunk.NEWLINE);
             

                #region // new address line 


                PdfPTable AddressDiv = new PdfPTable(4);
                AddressDiv.DefaultCell.Border = Rectangle.NO_BORDER;
                AddressDiv.PaddingTop = 20;
                AddressDiv.DefaultCell.BorderColor = BaseColor.WHITE;

                PdfPCell cell1 = new PdfPCell(new Phrase("pickup", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                cell1.Colspan = 2;
                cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                cell1.Border = Rectangle.NO_BORDER;
                AddressDiv.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell(new Phrase("Delivery", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                cell2.Colspan = 2;
                cell2.Border = Rectangle.NO_BORDER;
                cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell2);

                PdfPCell cell3 = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.PickupDate), new Font(Font.FontFamily.HELVETICA, 8F)));
                cell3.Colspan = 2;
                cell3.Border = Rectangle.NO_BORDER;
                cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell3);

                PdfPCell cell4 = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.DeliveryDate, new Font(Font.FontFamily.HELVETICA, 8F))));
                cell4.Colspan = 2;
                cell4.Border = Rectangle.NO_BORDER;
                cell4.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell4);
                
                PdfPCell cell5 = new PdfPCell(new Phrase(addressline1, new Font(Font.FontFamily.HELVETICA, 8F)));

                cell5.Colspan = 2;
                cell5.Border = Rectangle.NO_BORDER;
                cell5.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell5);

                PdfPCell cell6 = new PdfPCell(new Phrase(addressline1, new Font(Font.FontFamily.HELVETICA, 8F)));
                cell6.Colspan = 2;
                cell6.Border = Rectangle.NO_BORDER;
                cell6.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell6);


                PdfPCell cell7 = new PdfPCell(new Phrase(addressline3, new Font(Font.FontFamily.HELVETICA, 8F)));

                cell7.Colspan = 2;
                cell7.Border = Rectangle.NO_BORDER;
                cell7.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell7);

                PdfPCell cell8 = new PdfPCell(new Phrase(addressline3, new Font(Font.FontFamily.HELVETICA, 8F)));
                cell8.Colspan = 2;
                cell8.Border = Rectangle.NO_BORDER;
                cell8.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell8);


                PdfPCell cell9 = new PdfPCell(new Phrase(addressline3, new Font(Font.FontFamily.HELVETICA, 8F)));

                cell9.Colspan = 2;
                cell9.Border = Rectangle.NO_BORDER;
                cell9.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell9);

                PdfPCell cell10 = new PdfPCell(new Phrase(addressline3, new Font(Font.FontFamily.HELVETICA, 8F)));
                cell10.Colspan = 2;
                cell10.Border = Rectangle.NO_BORDER;
                cell10.HorizontalAlignment = Element.ALIGN_CENTER;
                AddressDiv.AddCell(cell10);


                document.Add(AddressDiv);

                document.Add(Chunk.NEWLINE);
             


                #endregion

                //test code
                PdfPTable ProductHeaderDetail = new PdfPTable(11);
                ProductHeaderDetail.DefaultCell.Border = Rectangle.TOP_BORDER;
              
                AddTableHeader(ProductHeaderDetail, "Product Name", 3, Element.ALIGN_CENTER, customColor);
                AddTableHeader(ProductHeaderDetail, "Quantity", 3, Element.ALIGN_CENTER, customColor);
                AddTableHeader(ProductHeaderDetail, "Price", 3, Element.ALIGN_CENTER, customColor);
                AddTableHeader(ProductHeaderDetail, "Total", 3, Element.ALIGN_CENTER, customColor);

                document.Add(ProductHeaderDetail);

                Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                document.Add(p);
                
               

                //end 

                PdfPTable ProductDetail = new PdfPTable(11);
                // ProductDetail.DefaultCell.Border = Rectangle.NO_BORDER;

                ProductDetail.DefaultCell.Border = Rectangle.TOP_BORDER;

                // AddTableHeader(ProductDetail, "Product Name", 3, Element.ALIGN_LEFT, customColor);
                // AddTableHeader(ProductDetail, "Quantity", 3, Element.ALIGN_CENTER, customColor);
                // AddTableHeader(ProductDetail, "Price", 3, Element.ALIGN_RIGHT, customColor);


                //AddTableHeader(ProductDetail, "Total", 3, Element.ALIGN_CENTER, customColor);

               


                foreach (var item in product)
                {
                    AddProductCell(ProductDetail, "" + item.ProductName, 3, Element.ALIGN_CENTER);
                    AddProductCell(ProductDetail, "" + item.Quantity, 3, Element.ALIGN_CENTER);
                    AddProductCell(ProductDetail, "฿"+ string.Format("{0:n1}", item.SubTotal.ToString()), 3, Element.ALIGN_CENTER);
                    AddProductCell(ProductDetail, "฿" + String.Format("{0:n1}", item.TotalPrice.ToString().ToString()), 3, Element.ALIGN_CENTER);
                    
                }
                document.Add(ProductDetail);

                Paragraph p3 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                document.Add(p3);
                ///End Product Listing

                ///Product Cost
                PdfPTable ProductCost = new PdfPTable(9);
                ProductCost.SpacingBefore = 10;
                PdfPCell ProductCostBlank = new PdfPCell();
                ProductCostBlank.HorizontalAlignment = Element.ALIGN_LEFT;
                ProductCostBlank.Border = Rectangle.NO_BORDER;
                ProductCostBlank.Colspan = 4;
                ProductCostBlank.PaddingTop = 10;
                ProductCost.AddCell(ProductCostBlank);

                PdfPCell ProductSubTotal = new PdfPCell(new Phrase("Subtotal", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductSubTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductSubTotal.BorderColor = customColor;
                ProductSubTotal.Border = Rectangle.TOP_BORDER;
                ProductSubTotal.Colspan = 3;
                ProductSubTotal.PaddingTop = 10;
                ProductCost.AddCell(ProductSubTotal);


                PdfPCell ProductSubTotalAmount = new PdfPCell(new Phrase(order.SubTotal.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));

                ProductSubTotalAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductSubTotalAmount.Border = Rectangle.TOP_BORDER;
                ProductSubTotalAmount.BorderColor = customColor;
                ProductSubTotalAmount.Colspan = 2;
                ProductSubTotalAmount.PaddingTop = ProductSubTotalAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductSubTotalAmount);



                ProductCost.AddCell(ProductCostBlank);
                PdfPCell ProductGST = new PdfPCell(new Phrase("TAX", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductGST.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductGST.Border = Rectangle.NO_BORDER;
                ProductGST.Colspan = 3;
                ProductGST.PaddingTop = 10;
                ProductCost.AddCell(ProductGST);


                PdfPCell ProductGSTAmount = new PdfPCell(new Phrase(order.Tax.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductGSTAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductGSTAmount.Border = Rectangle.NO_BORDER;
                ProductGSTAmount.Colspan = 2;
                ProductGSTAmount.PaddingTop = ProductGSTAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductGSTAmount);


                ProductCost.AddCell(ProductCostBlank);
                PdfPCell ProductTotal = new PdfPCell(new Phrase("Total", new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductTotal.HorizontalAlignment = Element.ALIGN_CENTER;
                ProductTotal.Border = Rectangle.NO_BORDER;
                ProductTotal.Colspan = 3;
                ProductTotal.PaddingTop = ProductTotal.PaddingBottom = 10;
                ProductCost.AddCell(ProductTotal);


                PdfPCell ProductTotalAmount = new PdfPCell(new Phrase(productAmount.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                ProductTotalAmount.HorizontalAlignment = Element.ALIGN_RIGHT;
                ProductTotalAmount.Border = Rectangle.NO_BORDER;
                ProductTotalAmount.Colspan = 2;
                ProductTotalAmount.PaddingTop = ProductTotalAmount.PaddingBottom = ProductTotalAmount.PaddingLeft = 10;
                ProductCost.AddCell(ProductTotalAmount);





                ProductCost.AddCell(ProductCostBlank);



                PdfPCell BlankLine = new PdfPCell();
                BlankLine.Border = Rectangle.TOP_BORDER;
                BlankLine.BorderColor = customColor;
                BlankLine.Colspan = 9;
                ProductCost.AddCell(BlankLine);

                document.Add(ProductCost);
                ///Product Cost End
                ///Payment Type Start
                PdfPTable PaidType = new PdfPTable(7);
                PaidType.SpacingBefore = 10;
                PaidType.AddCell(ProductCostBlank);
                PdfPCell PaidInFullText = new PdfPCell(new Phrase("Your Credit Card ending xxx123 was charged", new Font(Font.FontFamily.HELVETICA, 8F)));
                PaidInFullText.HorizontalAlignment = Element.ALIGN_CENTER;
                PaidInFullText.Border = Rectangle.NO_BORDER;
                PaidInFullText.BackgroundColor = customColor;
                PaidInFullText.Colspan = 4;
                PaidInFullText.PaddingBottom = 5;
                PaidType.AddCell(PaidInFullText);
                document.Add(PaidType);


                PdfPTable Footer = new PdfPTable(2);
                Footer.SpacingBefore = 10;
                Footer.AddCell(ProductCostBlank);
                PdfPCell CompanyAddress = new PdfPCell(new Phrase("Your Credit Card ending xxx123 was charged", new Font(Font.FontFamily.HELVETICA, 8F)));
                CompanyAddress.HorizontalAlignment = Element.ALIGN_CENTER;
                CompanyAddress.Border = Rectangle.NO_BORDER;
                CompanyAddress.BackgroundColor = customColor;
                CompanyAddress.Colspan = 4;
                CompanyAddress.PaddingBottom = 5;
                PaidType.AddCell(CompanyAddress);
                document.Add(Footer);



                PdfPTable table2 = new PdfPTable(3);
                table2.TotalWidth = document.PageSize.Width - document.LeftMargin;
                table2.AddCell("cell Value 1");
                table2.AddCell("Cell Value 2 ");
                table2.WriteSelectedRows(0, -1, document.LeftMargin + 200, document.PageSize.Height - 30, writer.DirectContent);




                PdfPTable CompanyAddressDiv = new PdfPTable(2);
                CompanyAddressDiv.DefaultCell.Border = Rectangle.NO_BORDER;
                CompanyAddressDiv.PaddingTop = 20;
                CompanyAddressDiv.DefaultCell.BorderColor = BaseColor.WHITE;

                PdfPCell Fcell1 = new PdfPCell(new Phrase("Lotus Laundary", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                Fcell1.Colspan = 2;
                Fcell1.HorizontalAlignment = Element.ALIGN_LEFT;
                Fcell1.Border = Rectangle.NO_BORDER;
                CompanyAddressDiv.AddCell(Fcell1);

                PdfPCell Fcell2 = new PdfPCell(new Phrase("Address", new Font(Font.FontFamily.HELVETICA, 10F, Font.BOLD)));
                Fcell2.Colspan = 2;
                Fcell2.Border = Rectangle.NO_BORDER;
                Fcell2.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell2);

                PdfPCell Fcell3 = new PdfPCell(new Phrase("01 Soi Daeng Udam ,Sukhumvati Soi 33 Rd Klongton", new Font(Font.FontFamily.HELVETICA, 8F)));
                Fcell3.Colspan = 2;
                Fcell3.Border = Rectangle.NO_BORDER;
                Fcell3.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell3);

                PdfPCell Fcell4 = new PdfPCell(new Phrase("Nua Wattana", new Font(Font.FontFamily.HELVETICA, 8F)));
                Fcell4.Colspan = 2;
                Fcell4.Border = Rectangle.NO_BORDER;
                Fcell4.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell4);

                PdfPCell Fcell5 = new PdfPCell(new Phrase("Bangkok 10110 Thailand", new Font(Font.FontFamily.HELVETICA, 8F)));

                Fcell5.Colspan = 2;
                Fcell5.Border = Rectangle.NO_BORDER;
                Fcell5.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell5);

                PdfPCell Fcell6 = new PdfPCell(new Phrase("Telephone :+66(2)6100111", new Font(Font.FontFamily.HELVETICA, 8F)));

                Fcell6.Colspan = 2;
                Fcell6.Border = Rectangle.NO_BORDER;
                Fcell6.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell6);

                PdfPCell Fcell7 = new PdfPCell(new Phrase("Fax :+66(2)2621700 ", new Font(Font.FontFamily.HELVETICA, 8F)));

                Fcell7.Colspan = 2;
                Fcell7.Border = Rectangle.NO_BORDER;
                Fcell7.HorizontalAlignment = Element.ALIGN_LEFT;
                CompanyAddressDiv.AddCell(Fcell7);
                document.Add(CompanyAddressDiv);



                // add footer code working but only show singl row


                //PdfPTable tbfooter = new PdfPTable(1);
                //tbfooter.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                //tbfooter.DefaultCell.Border = 0;

                //PdfPCell newcell = new PdfPCell(new Phrase("Lotus Laundary", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;
                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("Address", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("01 Soi Daeng Udam ,Sukhumvati Soi 33 Rd Klongton", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("Nua Wattana", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("Bangkok 10110 Thailand", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("Telephone :+66(2)6100111", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
                //newcell = new PdfPCell(new Phrase("Fax :+66(2)2621700", new Font(Font.FontFamily.HELVETICA, 12F, Font.BOLD)));
                //newcell.HorizontalAlignment = Element.ALIGN_CENTER;

                //tbfooter.AddCell(newcell);
               
                
                //float[] widths1 = new float[] {20f};
               
                //tbfooter.SetWidths(widths1);
                //tbfooter.FooterRows = 7;
                //tbfooter.WriteSelectedRows(3, -1, document.LeftMargin, writer.PageSize.GetBottom(document.BottomMargin), writer.DirectContent);

                //end


                document.Close();
                byte[] bytes = memoryStream.ToArray();
                string base64String = Convert.ToBase64String(bytes);
                memoryStream.Close();
                return base64String;

            }
        }

        public static string GenerateOrderPdf( OrderModel order , int timegap)
        {
            CultureInfo FrCulture = new CultureInfo("th-TH");
            using (System.IO.MemoryStream memoryStream = new MemoryStream())
            {


                var info = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                // DateTimeOffset localServerTime = DateTimeOffset.Now;
                DateTimeOffset localTime = order.CreatedOn.GetValueOrDefault().AddMinutes(timegap);


                string TitleOfOrderPage = ConfigurationManager.AppSettings["TitleOfOrderPage"];

                BaseColor customColor = WebColors.GetRGBColor("#FFFFFF");
                BaseColor blackColor = WebColors.GetRGBColor("#000000");


                Document document = new Document();
                document.SetMargins(-50, -50, 20, 20);
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();
                document.SetMargins(-50, -50, 20, 20);

                PdfPTable spacetop = new PdfPTable(3);
                spacetop.SpacingAfter = 20;
                document.Add(spacetop);

                string order_id = order.Id.ToString().PadLeft(6, '0');
                string DeliveryType = order.DeliveryType;
                if (order.DeliveryType== "NORMAL")
                {
                    DeliveryType = "Standard";
                }
                if(order.DeliveryType== "SAMEDAY")
                {
                    DeliveryType = "SAME DAY";
                }
               
               

                UserAddressModel PickAndDeliveryAddress = new UserAddressModel();
                PickAndDeliveryAddress = order.DeliveryAddress.FirstOrDefault();

                var addressline1 = PickAndDeliveryAddress.BuildingName != null ? PickAndDeliveryAddress.BuildingName : PickAndDeliveryAddress.HouseNumber;
                var addressline2 = PickAndDeliveryAddress.Floor != null ? PickAndDeliveryAddress.Floor : null;


                var addressline3 = addressline2 != null ? PickAndDeliveryAddress.Floor + " " + PickAndDeliveryAddress.UnitNo + " " + PickAndDeliveryAddress.StreetNumber : PickAndDeliveryAddress.StreetNumber;
                var addressline4 = PickAndDeliveryAddress.ProvinceName + " " + PickAndDeliveryAddress.DistrictName;
                var addressline5 = PickAndDeliveryAddress.SubDistrictName + " " + PickAndDeliveryAddress.PostalCode;
                var note = PickAndDeliveryAddress.Note;

                var CustomerAddress = addressline1 +" "+ addressline2 +" "+ addressline3 +" "+ addressline4 +" "+ addressline5;

                var driver = order.DriverDetails.FirstOrDefault();
                var collctedDriver = order.PickupDriverDetails.FirstOrDefault();

                //test for title and logo

                PdfPTable header = new PdfPTable(3);
                header.DefaultCell.Border = Rectangle.NO_BORDER;

              //  iTextSharp.text.Image symbol = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/symbol.png");

                iTextSharp.text.Image symbol = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/bill.png");
                // iTextSharp.text.Image symbol = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/bhatt.svg");
                symbol.ScaleToFit(9f, 8f);
                PdfPCell symbollogo = new PdfPCell(symbol);
                //add logo to table 
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance("https://lotuslaundry.azurewebsites.net/assets/images/logo.png");
                //jpg.ScaleAbsolute(100f, 50f);

                jpg.ScaleToFit(90f, 40f);
                PdfPCell logo = new PdfPCell(jpg);
                //companyPhoneNumber.HorizontalAlignment = Element.ALIGN_RIGHT;
                logo.Border = Rectangle.NO_BORDER;
                logo.PaddingTop = 20f;
                logo.Colspan = 1;
                header.AddCell(logo);

              


                PdfPTable spacetop12 = new PdfPTable(3);
                spacetop.SpacingAfter = 20;
                document.Add(spacetop12);


                //title 

                PdfPTable title = new PdfPTable(1);
                title.DefaultCell.Border = Rectangle.NO_BORDER;

                

                PdfPCell titleABNABN1 = new PdfPCell(new Phrase(TitleOfOrderPage, new Font(Font.FontFamily.HELVETICA, 10F)));
                titleABNABN1.HorizontalAlignment = Element.ALIGN_CENTER;
                titleABNABN1.Border = Rectangle.NO_BORDER;
                title.AddCell(titleABNABN1);

               

                PdfPCell titleCell = new PdfPCell(title);
                titleCell.PaddingTop = 20;
                titleCell.Border = Rectangle.NO_BORDER;
                header.AddCell(titleCell);

               
                //end title



                PdfPTable CompanyDetail = new PdfPTable(1);
                CompanyDetail.DefaultCell.Border = Rectangle.NO_BORDER;

                PdfPCell CompanyABN = new PdfPCell(new Phrase("Invoice Number", new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyABN.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN);

                PdfPCell CompanyABN1 = new PdfPCell(new Phrase("#"+order.Id.ToString().PadLeft(6, '0'), new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyABN1.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyABN1.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyABN1);

                PdfPCell CompanyEmail = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn), new Font(Font.FontFamily.HELVETICA, 10F)));
                CompanyEmail.HorizontalAlignment = Element.ALIGN_RIGHT;
                CompanyEmail.Border = Rectangle.NO_BORDER;
                CompanyDetail.AddCell(CompanyEmail);

                PdfPCell companyDetailCell = new PdfPCell(CompanyDetail);
                companyDetailCell.PaddingTop = 20;
                companyDetailCell.Border = Rectangle.NO_BORDER;
                header.AddCell(companyDetailCell);


                document.Add(header);
                PdfPTable spacetop1 = new PdfPTable(3);
                spacetop1.SpacingAfter = 20;
                document.Add(spacetop1);

                //end test for title and logo
                // adding an emplty line

                //PdfPTable tableBlank = new PdfPTable(1);

                //PdfPCell cell1 = new PdfPCell(new Phrase(" "));
                //tableBlank.AddCell(cell1);
                //cell1 = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                //tableBlank.AddCell(cell1);
                //cell1 = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                //tableBlank.AddCell(cell1);
                //cell1 = new PdfPCell(new Phrase(" " , new Font(Font.FontFamily.HELVETICA, 10F)));
                //cell1.Colspan = 2;
                //tableBlank.AddCell(cell1);
                //document.Add(tableBlank);

                //


                PdfPTable table12 = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Border = Rectangle.NO_BORDER;
                table12.AddCell(cell);

                cell = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 15F)));
                cell.Border = Rectangle.NO_BORDER;
                table12.AddCell(cell);

                cell = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Border = Rectangle.NO_BORDER;
                table12.AddCell(cell);

                document.Add(table12);







                PdfPTable table = new PdfPTable(5);

                 cell = new PdfPCell(new Phrase("Customer"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.customerdetails.FirstName, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Note To Customer"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.adminNote, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Order Id"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.Id.ToString().PadLeft(6, '0'), new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Order Date"));
                table.AddCell(cell);
               // cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.CreatedOn), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy hh:mm:ss tt}", localTime), new Font(Font.FontFamily.HELVETICA, 10F)));

                //String.Format("{0:d/M/yyyy HH:mm:ss}", dt);
                cell.Colspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Delivery Type"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(DeliveryType, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 4;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Status"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.Status, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 4;
               
                table.AddCell(cell);




                cell = new PdfPCell(new Phrase("Collection Driver"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(collctedDriver.FirstName, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(collctedDriver.NationalId, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(collctedDriver.LicencePlate, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Collection Date"));
                table.AddCell(cell);
                //cell = new PdfPCell(new Phrase(order.PickupDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.PickupDate), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.pickupSlot, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Collection Address"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(CustomerAddress, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Customer's Note", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(note, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Delivery Driver"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(driver.FirstName, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(driver.NationalId, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(driver.LicencePlate, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Delivery Date"));
                table.AddCell(cell);
                //cell = new PdfPCell(new Phrase(order.PickupDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy}", order.deliveredDate), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.pickupSlot, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Delivery Address"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(CustomerAddress, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Customer's Note", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 1;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(note, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase("Payment"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.PaymentType, new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Payment Status"));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase(order.paymentStatus, new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);

                //cell = new PdfPCell(new Phrase(""));
                //table.AddCell(cell);
                //cell = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 10F)));
                //table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Invoice No."));
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("#"+order.Id.ToString().PadLeft(6, '0'), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Payment date"));
                table.AddCell(cell);
                //cell = new PdfPCell(new Phrase(order.paymentDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy hh:mm:ss tt}", order.paymentDate.GetValueOrDefault().AddMinutes(timegap)), new Font(Font.FontFamily.HELVETICA, 10F)));
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase("Collected At"));
                table.AddCell(cell);
               // cell = new PdfPCell(new Phrase(order.collectedDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy hh:mm:ss tt}", order.collectedDate.GetValueOrDefault().AddMinutes(timegap)), new Font(Font.FontFamily.HELVETICA, 10F)));

                cell.Colspan = 2;
                table.AddCell(cell);
                cell = new PdfPCell(new Phrase("Delivered On"));
                table.AddCell(cell);
               // cell = new PdfPCell(new Phrase(order.DeliveryDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(String.Format("{0:ddd, MMM d, yyyy hh:mm:ss tt}", order.DeliveryDate.GetValueOrDefault().AddMinutes(timegap)), new Font(Font.FontFamily.HELVETICA, 10F)));

                table.AddCell(cell);

                //new 
                if(order.laundryInstruction==null)
                {
                    order.laundryInstruction = " ";
                }
                cell = new PdfPCell(new Phrase("Laundry Instruction"));
                table.AddCell(cell);
                // cell = new PdfPCell(new Phrase(order.collectedDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(  order.laundryInstruction.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));

                cell.Colspan = 4;
                table.AddCell(cell); 


                //cell = new PdfPCell(new Phrase("Change Required"));
                //table.AddCell(cell);


                //symbollogo.Border = Rectangle.TOP_BORDER;
                //symbollogo.Border = Rectangle.BOTTOM_BORDER;
                //symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                //table.AddCell(symbollogo);
             
                //cell = new PdfPCell(new Phrase(order.change.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
               
                //cell.Border = 0;
                //cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                //cell.HorizontalAlignment = PdfPCell.ALIGN_MIDDLE;
                //cell.Colspan = 2;
                //table.AddCell(cell);


                //test 
                cell = new PdfPCell(new Phrase("Change Required"));
                table.AddCell(cell);
                // cell = new PdfPCell(new Phrase(order.collectedDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));



                symbollogo.Border = Rectangle.TOP_BORDER;
                symbollogo.Border = Rectangle.BOTTOM_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table.AddCell(symbollogo);
                // cell = new PdfPCell(new Phrase(order.DeliveryDate.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                cell = new PdfPCell(new Phrase(order.change.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));

                cell.Border = 0;
                cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                cell.Colspan = 3;
                table.AddCell(cell);
                //test end 



                document.Add(table);



                //tets 

               





                PdfPTable table2 = new PdfPTable(1);
                cell = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Border = Rectangle.NO_BORDER;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase("Order Items", new Font(Font.FontFamily.HELVETICA, 15F)));
                cell.Border = Rectangle.NO_BORDER;
                table2.AddCell(cell);

                cell = new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Border = Rectangle.NO_BORDER;
                table2.AddCell(cell);

                document.Add(table2);

                PdfPTable table3 = new PdfPTable(12);
                cell = new PdfPCell(new Phrase("Product Name", new Font(Font.FontFamily.HELVETICA, 12F)));
                cell.Colspan = 3;
                cell.Padding = 5;
                table3.AddCell(cell);

                cell=new PdfPCell(new Phrase("Quantity", new Font(Font.FontFamily.HELVETICA, 12F)));
                cell.Colspan = 3;
                cell.Padding = 5;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Price", new Font(Font.FontFamily.HELVETICA, 12F)));
                cell.Colspan = 3;
                cell.Padding = 5;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table3.AddCell(cell);

                cell = new PdfPCell(new Phrase("Total", new Font(Font.FontFamily.HELVETICA, 12F)));
                cell.Colspan = 3;
                cell.Padding = 5;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                table3.AddCell(cell);

                document.Add(table3);


                PdfPTable table4 = new PdfPTable(12);

                foreach (var item in order.OrderItems)
                {
                    cell=new PdfPCell(new Phrase(item.ProductName, new Font(Font.FontFamily.HELVETICA, 10F)));
                    cell.Colspan = 3;
                    table4.AddCell(cell);

                    cell = new PdfPCell(new Phrase(item.Quantity.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                    cell.Colspan = 3;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    table4.AddCell(cell);

                   // var a=item.SubTotal.ToString("C", culture);
                      var a =  @String.Format(new CultureInfo("th-TH"), "{0:C}", item.SubTotal);


                    symbollogo.Border = Rectangle.TOP_BORDER;

                    symbollogo.Border = Rectangle.BOTTOM_BORDER;
                    symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                   symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    table4.AddCell(symbollogo);

                    cell = new PdfPCell(new Phrase(a.ToString(), new Font(Font.FontFamily.HELVETICA, 10F)));
                    cell.Colspan = 2;
                    cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                    cell.Border = 0;
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    table4.AddCell(cell);

                    symbollogo.Border = Rectangle.TOP_BORDER;

                    symbollogo.Border = Rectangle.BOTTOM_BORDER;
                    symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                   symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                    table4.AddCell(symbollogo);

                    cell = new PdfPCell(new Phrase( String.Format("{0:n1}", item.TotalPrice.ToString()), new Font(Font.FontFamily.HELVETICA, 10F)));
                    cell.Colspan = 2;
                    cell.HorizontalAlignment= PdfPCell.ALIGN_LEFT;
                    cell.Border = 0;
                    cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;

                    table4.AddCell(cell);

                }

          

                cell = new PdfPCell(new Phrase("Subtotal", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 9;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                table4.AddCell(cell);


                //decimal parsed = decimal.Parse(order.SubTotal.ToString(), CultureInfo.InvariantCulture);
                //CultureInfo hindi = new CultureInfo("th-TH");
                //string subtotal = string.Format(hindi, "{0:c}", parsed);

                symbollogo.Border = Rectangle.TOP_BORDER;

                symbollogo.Border = Rectangle.BOTTOM_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
               symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table4.AddCell(symbollogo);


                var b = @String.Format(new CultureInfo("th-TH"), "{0:C}", order.SubTotal);
                //cell = new PdfPCell(new Phrase( String.Format("{0:n1}", subtotal, new Font(Font.FontFamily.SYMBOL, 10F))));
               
                Font Calibri = FontFactory.GetFont("Calibri", 16, Font.BOLDITALIC);
                //cell = new PdfPCell(new Phrase(subtotal.ToString(), Calibri, 10F)));
                cell = new PdfPCell(new Phrase( String.Format("{0:n1}", order.SubTotal.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                cell.Border = 0;
                cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;


                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase(DeliveryType+" Charges ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 9;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                //cell.Border = 0;
                //cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;


                table4.AddCell(cell);

                symbollogo.Border = Rectangle.TOP_BORDER;

                symbollogo.Border = Rectangle.BOTTOM_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table4.AddCell(symbollogo);

                cell = new PdfPCell(new Phrase("฿" + String.Format("{0:n1}", order.DeliveryPrice.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                cell.Border = 0;
                cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;

                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Logistic Charges ", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 9;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                table4.AddCell(cell);



                // cell = new PdfPCell(new Phrase("฿" + String.Format("{0:n1}", order.LogisticCharge.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));

                //cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                symbollogo.Border = Rectangle.TOP_BORDER;

                symbollogo.Border = Rectangle.BOTTOM_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
               symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table4.AddCell(symbollogo);

                cell = new PdfPCell(new Phrase(String.Format("{0:n1}", order.LogisticCharge.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));
                cell.Colspan = 2;

                //cell.Border = Rectangle.TOP_BORDER;               
                //cell.Border = Rectangle.RIGHT_BORDER;
                //cell.Border = Rectangle.BOTTOM_BORDER;

                cell.Border = 0;
                cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                table4.AddCell(cell);

                cell = new PdfPCell(new Phrase("Tax", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 9;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                table4.AddCell(cell);


              //  symbollogo.Border = Rectangle.NO_BORDER;


                symbollogo.Border = Rectangle.TOP_BORDER;

                symbollogo.Border = Rectangle.BOTTOM_BORDER;
               // symbollogo.Border = Rectangle.RIGHT_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
               symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table4.AddCell(symbollogo);

                cell = new PdfPCell(new Phrase(String.Format("{0:n1}", order.Tax.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));
                cell.Colspan = 2;
                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                //cell.Border = Rectangle.TOP_BORDER;               
                //cell.Border = Rectangle.RIGHT_BORDER;
                //cell.Border = Rectangle.BOTTOM_BORDER;

                cell.Border = 0;
                cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;

                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                table4.AddCell(cell);


                cell = new PdfPCell(new Phrase("Grand Total", new Font(Font.FontFamily.HELVETICA, 10F)));
                cell.Colspan = 9;
                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                table4.AddCell(cell);

                symbollogo.Border = Rectangle.TOP_BORDER;

                symbollogo.Border = Rectangle.BOTTOM_BORDER;
                symbollogo.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                symbollogo.VerticalAlignment = PdfPCell.ALIGN_MIDDLE;
                table4.AddCell(symbollogo);

                cell = new PdfPCell(new Phrase(String.Format("{0:n1}", order.TotalPrice.ToString(), new Font(Font.FontFamily.HELVETICA, 10F))));
                cell.Colspan = 2;
                // cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;

                // cell.setBorder(PdfPCell.NO_BORDER);
                //  cell.setCellEvent(new DottedCell(PdfPCell.RIGHT | PdfPCell.BOTTOM))



                //cell.Border = Rectangle.TOP_BORDER;              
                //cell.Border = Rectangle.BOTTOM_BORDER;

                cell.Border = 0;
                
                 cell.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                cell.BorderColorLeft = BaseColor.WHITE;
                table4.AddCell(cell);

                
                document.Add(table4);
                

                ///Product Cost
               

                document.Close();
                byte[] bytes = memoryStream.ToArray();
                string base64String = Convert.ToBase64String(bytes);
                memoryStream.Close();
                return base64String;

            }
        }



        public static void AddProductCell(PdfPTable table, string value, int colspan, int alignMent)
        {
            PdfPCell ProductCell = new PdfPCell(new Phrase(value, new Font(Font.FontFamily.HELVETICA, 10F)));
            ProductCell.HorizontalAlignment = alignMent;
            ProductCell.Border = Rectangle.NO_BORDER;
            ProductCell.Padding = 8;
            ProductCell.Colspan = colspan;
            ProductCell.PaddingTop = 0;
            table.AddCell(ProductCell);

        }

        public static void AddTableHeader(PdfPTable table, string value, int colspan, int alignMent, BaseColor customColor)
        {
            PdfPCell ProductCell = new PdfPCell(new Phrase(value, new Font(Font.FontFamily.HELVETICA, 10F)));
            ProductCell.HorizontalAlignment = alignMent;
            ProductCell.Border = Rectangle.NO_BORDER;
            ProductCell.BackgroundColor = customColor;
            ProductCell.Padding = 8;
            ProductCell.Colspan = colspan;
            table.AddCell(ProductCell);

        }


       

    }
}