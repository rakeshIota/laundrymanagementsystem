﻿using LotusLaundry.Domain.BankAccount;

namespace LotusLaundry.Domain.Payment
{
    public class PaymentQRcodeModel
    {
        public long orderid { get; set; }

        public decimal amount { get; set; }

        public long userid { get; set; }

        public long CreatedBy { get; set; }

        public int tenantid { get; set; }


        public string authkey { get; set; }

        public string partnerTxnUid { get; set; }
        public string statusCode { get; set; }
        public string errorCode { get; set; }
        public string errorDesc  { get; set; }

        public string accountName { get; set; }

        public string qrCode { get; set; }

        public string merchantId { get; set; }

        public decimal txnAmount { get; set; }

        public decimal txnCurrencyCode { get; set; }
        public string  loyaltyId { get; set; }

        public string txnNo { get; set; }
        
        public string additionalInfo { get; set; }

        public string output { get; set; }














    }

}
  
