﻿using LotusLaundry.Domain.BankAccount;

namespace LotusLaundry.Domain.Payment
{
    public class PaymentResponseModel
    {
        
        public string jsonInput { get; set; }
        public string Message { get; set; }
        public string TransactionId { get; set; }
        public bool status { get; set; }
        
    }

}
  
