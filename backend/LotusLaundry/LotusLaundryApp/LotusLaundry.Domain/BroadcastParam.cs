﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain
{
    public class BroadcastParam
    {
        public string Id { get; set; }

        public List<string> postalCodes { get; set; }

        public string customerType { get; set; }

        public string customerGender { get; set; }

        public string customerOrderType { get; set; }

        public int orderCount { get; set; }

        public string message { get; set; }


   
        public int? TenantId { get; set; }
   
        public string Slug { get; set; }
   
        public long? CreatedBy { get; set; }
       
        public long? UpdatedBy { get; set; }
       
        public DateTimeOffset? CreatedOn { get; set; }
     
        public DateTimeOffset? UpdatedOn { get; set; }
     
   
        public bool? IsDeleted { get; set; }
    
        public bool? IsActive { get; set; }

        public int? next { get; set; }
        public int? offset { get; set; }

        public string PostalCode { get; set; }

    }
}
