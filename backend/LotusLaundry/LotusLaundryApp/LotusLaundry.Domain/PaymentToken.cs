﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain
{
    public class PaymentToken
    {
       
        public string access_token { get; set; }

       
        public string client_id { get; set; }

       
        public int expires_in { get; set; }

      
        public string RefreshToken { get; set; }

        public string scope { get; set; }

        public string developeremail { get; set; }



        public string status { get; set; }


        public string token_type { get; set; }


    }
}
