﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.PushNotification
{
    public class FCMNotificationModel
    {
        public List<string> DeviceToken { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public string TargetId { get; set; }
        public string Type { get; set; }
    }


    public class PushNotificationModel
    {
        public long Id { get; set; }
        public string Ids { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public System.DateTimeOffset UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public bool IsArchive { get; set; }
        public string TargetId { get; set; }
        public string TargetType { get; set; }
        public Nullable<long> UserId { get; set; }
        public string UserRole { get; set; }

        public string orderid { get; set; }
        public string driverid { get; set; }
        public string customerid { get; set; }



        public bool IsSeen { get; set; }
        public Nullable<long> ArchivedBy { get; set; }
        public Nullable<System.DateTimeOffset> ArchiveDate { get; set; }
        public Nullable<int> overall_count { get; set; }
        public long? FromUserId { get; set; }
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
        public Nullable<long> ToUserId { get; set; }
        public int? UnreadCount { get; set; }
        public string DeviceId { get; set; }
    }
    public class UserDevice
    {
        public string DeviceId { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
    }
    public class PushNotification
    {
        [JsonProperty("registration_ids")]
        public string[] TargetDevice { get; set; }
        [JsonProperty("notification")]
        public NotiMessage NotificationMsg { get; set; }
        [JsonProperty("data")]
        public dynamic Data { get { return _data; } set { _data = value; } }
        dynamic _data;
        public string priority { get { return "high"; } }
    }

    //public class NotificationLogModel
    //{
    //    /*
    //    id = model.Id, //notification_id
    //                      notificationObjId = model.NotificationObjectId,//notification_object_id
    //                      notiFrom = (int?)model.NotificationFrom,
    //                      action = (int?)model.ActionPerformed,
    //                      title = 
    //       */
    //    [JsonProperty("id")]
    //    public long Id { get; set; }
    //    [JsonProperty("fromUserId")]
    //    public long? FromUserId { get; set; }
    //    /// <summary>
    //    /// From user Full Name
    //    /// </summary>
    //    [JsonProperty("fromUserName")]
    //    public string FromUserName { get; set; }

    //    [JsonProperty("toUserId")]
    //    public long? ToUserId { get; set; }
    //    /// <summary>
    //    /// ToUser Full Name
    //    /// </summary>
    //    [JsonProperty("toUserName")]
    //    public string ToUserName { get; set; }
    //    [JsonProperty("action")]
    //    public NotiActionPerformed? ActionPerformed { get; set; }

    //    [JsonProperty("actionPerformedDesc")]
    //    public string ActionPerformedDesc { get { return ActionPerformed.GetDescription(); } }
    //    [JsonProperty("createdDt")]
    //    public System.DateTimeOffset CreatedDate { get; set; }
    //    [JsonProperty("isRead")]
    //    public bool IsRead { get; set; }
    //    [JsonProperty("notiFrom")]
    //    public NotificationFrom? NotificationFrom { get; set; }
    //    [JsonProperty("notificationFromDesc")]
    //    public string NotificationFromDesc { get { return NotificationFrom.GetDescription(); } }
    //    [JsonProperty("notificationObjId")]
    //    public long? NotificationObjectId { get; set; }
    //    [JsonProperty("optionalDetails")]
    //    public string OptionalDetails { get; set; }
    //    [JsonProperty("overall_count")]
    //    public int? overall_count { get; set; }
    //    //public bool? TIsGroupChat { get; set; }
    //    //public bool? TIsGroupDiscussion { get; set; }
    //    //public bool? FIsGroupChat { get; set; }
    //    //public bool? FIsGroupDiscussion { get; set; }
    //    [JsonProperty("title")]
    //    public string Description { get; set; }
    //    [JsonProperty("unreadCount")]
    //    public int? UnreadCount { get; set; }
    //    [JsonProperty("storeId")]
    //    public long? StoreId { get; set; }
    //    [JsonProperty("companyId")]
    //    public long? CompanyId { get; set; }
    //    [JsonProperty("fromUser")]
    //    public UserModel FromUser { get; set; }
    //    [JsonProperty("toUser")]
    //    public UserModel ToUser { get; set; }
    //    [JsonProperty("type")]
    //    public string Type { get; set; }
    //    [JsonProperty("createdBy")]
    //    public long? createdBy { get; set; }
    //}
    public class NotiMessage
    {
        [JsonProperty("body")]
        public string Body { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        public string sound { get { return "default"; } }
        public bool alert { get { return true; } }
        [JsonProperty("content-available")]
        public int ContentAvailable { get { return 1; } }
        //public string click_action { get { return "FCM_PLUGIN_ACTIVITY"; } }
        public string icon { get { return "icon"; } }
        public string iconColor { get { return "grey"; } }

        //iconColor: "grey"
        //public string badge { get { return "0"; } }
        public string badge { get; set; }
        public bool vibrate { get { return true; } }
    }

}
