﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.CartItem
{
    public class CartItemModel
    { 
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public long? CartId { get; set; }
        public long? ProductId { get; set; }
        public int? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? TotalPrice { get; set; }
        public int TotalCount { get; set; }
        public long? ServiceId { get; set; }
        public string ProductName { get; set; }
        public string ServiceName { get; set; }
        public string Icon { get; set; }
    }
}
