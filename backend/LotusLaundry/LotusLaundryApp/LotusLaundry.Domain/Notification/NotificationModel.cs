﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.Notification
{
    public class NotificationModel
    {
			  	public long? Id { get; set; }
				public int? TenantId { get; set; }
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public long? UserId { get; set; }
			  	public string Message { get; set; }
			  	public long? LanguageId { get; set; }
			  	public string Type { get; set; }
			  	public string Token { get; set; }
			  	public string Status { get; set; }
			  	public string Response { get; set; }
		public int TotalCount { get; set; }
    }
}
