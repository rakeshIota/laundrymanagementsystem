﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.TwilioChat
{
    public class TwilioChatModel
    {
        public string Name { get; set; }
        public string UserRole { get; set; }
    }
}
