﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain
{
    public class SearchParam
    {
        public string Id { get; set; }
        public long? UserId { get; set; }
        public long? ToUserId { get; set; }
        public long? FromUserId { get; set; }
        public string Type { get; set; }
        public int? Next { get; set; }
        public int? Offset { get; set; }
        public bool? IsActive { get; set; }
        public DateTimeOffset? AppointmentDate { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public DateTimeOffset? ToDayDate { get; set; }
        public long? RecordId { get; set; }
        public string Slug { get; set; }
        public string FileType { get; set; }
        public string RelationTable { get; set; }
        public string RelationId { get; set; }
        public long? LRelationId { get; set; }
        public int? TenantId { get; set; }
        public string Role { get; set; }
        public string LanguageIdStr { get; set; }
        public long? LanguageId { get; set; }
        public string Gender { get; set; }
        public string ServiceIdStr { get; set; }
        public long? ServiceId { get; set; }

        public string postalCode { get; set; }      
        public DateTimeOffset? deliveryDate { get; set; }      
        public DateTimeOffset? pickupDate { get; set; }
        public string deliverySlot { get; set; }     
        public string pickupSlot { get; set; }
        public bool? IsClose { get; set; }

        public long? districtId { get; set; }
        public long? subdistrictId { get; set; }
        public long? driverId { get; set; }

        public string district { get; set; }
        public string subdistrict { get; set; }
        public string driver { get; set; }

        public string status { get; set; }

        public string name { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }


        public string firstName { get; set; }
        public string lastName { get; set; }

        public DateTimeOffset? lastOrderDate { get; set; }
        public DateTimeOffset? registerDate { get; set; }

        public string address { get; set; }

        public DateTimeOffset? date { get; set; }
        public bool isCompleted { get; set; }

        public List<string> drivers { get; set; }

        public string UserRole { get; set; }
        public string orderId { get; set; }

        public string deliveryType { get; set; }

        public string nickName { get; set; }

        public string filterOn { get; set; }

        public string filterType { get; set; }

        public string searchText { get; set; }

        public string laundryInstruction { get; set; }

    }
}
