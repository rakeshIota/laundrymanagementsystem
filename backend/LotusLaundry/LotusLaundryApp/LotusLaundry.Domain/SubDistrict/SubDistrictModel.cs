﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.SubDistrictLanguageMapping;

namespace LotusLaundry.Domain.SubDistrict
{
    public class SubDistrictModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public long? DistrictId { get; set; }
        public int TotalCount { get; set; }
        public string Name { get; set; }
        public string DistrictName { get; set; }
        public string SubDistrictLanguageXml { get; set; }
        public List<SubDistrictLanguageMappingModel> SubDistrictLanguages { get; set; }
    }
}
