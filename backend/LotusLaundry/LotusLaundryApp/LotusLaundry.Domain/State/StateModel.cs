﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.StateLanguageMapping;

namespace LotusLaundry.Domain.State
{
    public class StateModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public long? CountryId { get; set; }
        public int TotalCount { get; set; }
        public string Name { get; set; }
        public string StateLanguageXml { get; set; }
        public List<StateLanguageMappingModel> StateLanguages { get; set; }
        public string CountryName { get; set; }
    }
}
