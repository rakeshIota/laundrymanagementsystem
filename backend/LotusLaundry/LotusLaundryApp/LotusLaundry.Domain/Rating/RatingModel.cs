﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.Rating
{
    public class RatingModel
    { 
			  	public long? Id { get; set; }
				public int? TenantId { get; set; }
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public long? Orderid { get; set; }
		public long? quality { get; set; }
		public long? DeliveryService { get; set; }
		public long? Staff { get; set; }
		public long? Packaging { get; set; }
		public long? OverallSatisfaction { get; set; }
		public string Feedback { get; set; }







		public int TotalCount { get; set; }
    }
}
