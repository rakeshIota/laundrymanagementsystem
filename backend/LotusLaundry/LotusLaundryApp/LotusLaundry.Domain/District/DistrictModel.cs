﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.DistrictLanguageMapping;

namespace LotusLaundry.Domain.District
{
    public class DistrictModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public int TotalCount { get; set; }
        public string Name { get; set; }
        public string CityName { get; set; }
        public string DistrictLanguageXml { get; set; }
        public List<DistrictLanguageMappingModel> DistrictLanguages { get; set; }
        public long? CityId { get; set; }
    }
}
