﻿using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.UserAddress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.Users
{
    public class UsersModel
    {
        public long? Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }

        public string UserName { get; set; }
        public string FullName { get; set; }
        public string UserProfileEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileImageUrl { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public string MobileNo { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int? Pin { get; set; }
        public string Address { get; set; }
        public string UniqueCode { get; set; }

        public string Password { get; set; }
        public string Code { get; set; }
        public bool IsDeleted { get; set; }
        public string Base64String { get; set; }
        public string OldPassword { get; set; }
        public IList<string> Roles { get; set; }
        public bool? IsFacebookConnected { get; set; }
        public bool? IsGoogleConnected { get; set; }
        public string FacebookId { get; set; }
        public string GoogleId { get; set; }
        public int? TenantId { get; set; }

        public string Role { get; set; }
        // public object File { get; set; }


        public string NickName { get; set; }

        public string CountryCode { get; set; }

        public DateTime? DOB { get; set; }

        public long? LanguageId { get; set; }

        public FileGroupItemsModel File { get; set; }

        public List< FileGroupItemsModel> AllFile { get; set; }


        public string employeeid { get; set; }

       
        public string Position { get; set; }

        
        public string NationalId { get; set; }
        
        public string LicenceId { get; set; }
      
        public string BikeInformation { get; set; }
     
        public string LicencePlate { get; set; }

        
        public string status { get; set; }

        public string FileXml { get; set; }

        public string userName { get; set; }

        public bool PhoneConfirmed { get; set; }
        public List< UserAddressModel> AddressDetail { get; set; }

        public string addressXml { get; set; }

        public long? workLoad { get; set; }

        public long? orderId { get; set; }

        public int ordersCount { get; set; }

        public DateTimeOffset? lastOrderDate { get; set; }


        public decimal? Latitude { get; set; }
        public decimal? Longititude { get; set; }

        public string adminNote { get; set; }


        public string RolesXml { get; set; }

        public int TotalCount { get; set; }

        public int adminCount { get; set; }


    }




}
