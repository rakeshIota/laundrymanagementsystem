﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.CartItem;

namespace LotusLaundry.Domain.Cart
{
    public class CartModel 
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsLocked { get; set; }
        public long? CustomerId { get; set; }
        public string Status { get; set; }
        public decimal? Tax { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? TotalItems { get; set; }
        public int TotalCount { get; set; }

        public string CartItmeXml { get; set; }
        public List<CartItemModel> CartItems { get; set; }
        public string DeliveryType { get; set; }


        public DateTimeOffset? DeliveryDate { get; set; }
        public DateTimeOffset? PickupDate { get; set; }

        public string deliverySlot { get; set; }
        public string pickupSlot { get; set; }

        public string laundryInstruction { get; set; }

    }
}
