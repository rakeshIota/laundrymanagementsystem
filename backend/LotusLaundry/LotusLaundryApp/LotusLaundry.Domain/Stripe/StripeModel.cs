﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.Stripe
{
    public class StripeModel
    {
        public long? StripeId { get; set; }
        public int Amount { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string StripeTokenId { get; set; }
        public string PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanCurrency { get; set; }
        public int LastFour { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string HolderName { get; set; }
        public string PlanType { get; set; }
    }
}
