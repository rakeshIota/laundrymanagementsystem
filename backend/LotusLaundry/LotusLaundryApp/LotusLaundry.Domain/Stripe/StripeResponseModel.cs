﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.Stripe
{
    public class StripeResponseModel
    {
        public bool Status { get; set; }
        public string CustomerId { get; set; }
        public string PlanId { get; set; }
        public string Message { get; set; }
        public long? LastFour { get; set; }
       public string currency { get; set; }
        public decimal Amount { get; set; }
        public long? OrderId { get; set; }
        public long? CreatedBy { get; set; }
        public string StripeResponse { get; set; }
        public string StripeTokenId { get; set; }

    }
}
