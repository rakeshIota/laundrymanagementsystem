﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.ProductPrice;
using LotusLaundry.Domain.ProductLanguageMapping;

namespace LotusLaundry.Domain.Product
{
    public class ProductModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string ImageXml { get; set; }
        public List<FileGroupItemsModel> Image { get; set; }
        public string Gender { get; set; }
        public int TotalCount { get; set; }
        public string ProductLanguageXml { get; set; }
        public string ProductPriceXml { get; set; }
        public string Name { get; set; }
        public List<ProductPriceModel> ProductPrices { get; set; }
        public List<ProductLanguageMappingModel> ProductLanguages { get; set; }
        public decimal? Price { get; set; }

        public long Rank { get; set; }

    }
}
