﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Domain.Users;
//using LotusLaundry.Domain.UserAddress;

namespace LotusLaundry.Domain.Order
{
    public class OrderModel 
    {
			  	public long? Id { get; set; }
				public int? TenantId { get; set; }
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public long? UserId { get; set; }
			  	public DateTimeOffset?  DeliveryDate { get; set; }			
			  	public string DeliveryType { get; set; }
				public int? TotalItems { get; set; }
			  	public decimal? DeliveryPrice { get; set; }
			  	public long? DriverId { get; set; }
			  	public string Status { get; set; }
			  	public string PaymentType { get; set; }
			  	public decimal? PaidAmount { get; set; }
			  	public decimal? Tax { get; set; }
			  	public decimal? SubTotal { get; set; }
			  	public decimal? TotalPrice { get; set; }
			  	public long? AdressId { get; set; }
			  	public long? ExpectedPickUpMin { get; set; }
			  	public long? ExpectedPickUpMax { get; set; }
			  	public long? ExpectedDeliveryMin { get; set; }
			  	public long? ExpectedDeliveryMax { get; set; }

        public string OrderItemXml { get; set; }
        public string DeliveryAddressXml { get; set; }
        public List<OrderItemModel> OrderItems { get; set; }

        public List<UsersModel> DriverDetails { get; set; }

        public string DriverXml { get; set; }

		public string adminNote { get; set; }
		public decimal? LogisticCharge { get; set; }
         

        public List<UserAddressModel> DeliveryAddress { get; set; }

		//public UserAddressModel DeliveryAddress { get; set; }

		public DateTimeOffset? PickupDate { get; set; }

  
        public string deliverySlot { get; set; }


        public string pickupSlot { get; set; }

        public decimal? Discount { get; set; }
        public string paymentResponse { get; set; }


        public decimal? change { get; set; }
        public string note { get; set; }

        public string paymentStatus { get; set; }

		public string customerXml { get; set; }

		public string customerName { get; set; }

		public long? customerId { get; set; }

		

		public string driverName { get; set; }
		public  UsersModel customerdetails { get; set; }
        public long? CartId { get; set; }


		public DateTimeOffset? paymentDate { get; set; }
		public DateTimeOffset? collectedDate { get; set; }
		public DateTimeOffset? deliveredDate { get; set; }

		public string districtName { get; set; }
		public string subDistrictName { get; set; }
		public string PickupDriverName { get; set; }
		public string DeliveryDriverName { get; set; }

		public List<UsersModel> DeliveryDriverDetails { get; set; }
		public List<UsersModel> PickupDriverDetails { get; set; }

		public string RideType { get; set; }

		public bool? isRating { get; set; }
		public string qrCode { get; set; }

		//public string laundaryInstruction  { get; set; }



		public string laundryInstruction { get; set; }

		public int Isdelivered { get; set; }
		public int IsPickup { get; set; }


		public int TotalCount { get; set; }

		public int activeOrders { get; set; }
		//
	}
}
