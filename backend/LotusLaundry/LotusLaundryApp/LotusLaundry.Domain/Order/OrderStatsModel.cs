﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.Order
{
    public class OrderStatsModel
    {
        public string TotalOrderTitle { get; set; }
        public long? TotalOrderCount { get; set; }
        public long? TotalNormalOrderCount { get; set; }
        public long? TotalSameDayOrderCount { get; set; }
        public long? TotalExpressOrderCount { get; set; }

        public string NewOrderTitle { get; set; }
        public long? NewOrderTotalCount { get; set; }
        public long? NewSameDayOrderCount { get; set; }
        public long? NewExpressOrderCount { get; set; }
        public long? NewNormalOrderCount { get; set; }

        public string CollectionOrderTitle { get; set; }
        public long? CollectionOrderTotalCount { get; set; }
        public long? CollectionAssignedOrderCount { get; set; }
        public long? CollectionCollectedOrderCount { get; set; }

        public string CleaningOrderTitle { get; set; }
        public long? CleaningOrderTotalCount { get; set; }
        public long? CleaningInprogressOrderCount { get; set; }
        public long? CleaningCompletedOrderCount { get; set; }

        public string DeliveryOrderTitle { get; set; }
        public long? DeliveryOrderTotalCount { get; set; }
    }
}
