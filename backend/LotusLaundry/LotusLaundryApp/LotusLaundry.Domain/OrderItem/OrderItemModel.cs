﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.OrderItem
{
    public class OrderItemModel
    {
			  	public long? Id { get; set; }
				public int? TenantId { get; set; } 
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public long? OrderId { get; set; }
			  	public long? ProductId { get; set; }
				public int? Quantity { get; set; }
			  	public bool? IsPacking { get; set; }
			  	public decimal? PackingPrice { get; set; }
			  	public decimal? SubTotal { get; set; }
			  	public decimal? TotalPrice { get; set; }
			  	public long? PackingId { get; set; }

        public string ProductName { get; set; }
        public string ServiceName { get; set; }

        public long? ServiceId { get; set; }
        public string Icon { get; set; }
        public int TotalCount { get; set; }
    }
}
