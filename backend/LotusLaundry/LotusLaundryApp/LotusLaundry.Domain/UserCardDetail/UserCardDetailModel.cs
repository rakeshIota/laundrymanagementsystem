﻿using System;
using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Domain.Users;

namespace LotusLaundry.Domain.UserCardDetail
{
    public class UserCardDetailModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }


        public string Brand { get; set; }
        public string LastFourDigits { get; set; }       
        public int TotalCount { get; set; }
        public decimal? TotalAmount { get; set; }

        public UserAddressModel DeliveryAddress { get; set; }       
        public decimal? TaxAmount { get; set; }
        public String CurrencyCode { get; set; }      
        public String PaymentType { get; set; }

    }
}
