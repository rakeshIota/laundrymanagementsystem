﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain
{
    public class BaseEntity
    {
        public long? Id { get; set; }
        public CreatedUserModel CreatedUser { get; set; }
        public UpdatedUserModel UpdatedUser { get; set; }
    }
    /// <summary>
    /// It represent the common auditable field for a record
    /// </summary>
    /// 
    public class CreatedUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public long? UserId { get; set; }
    }
    public class UpdatedUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public long? UserId { get; set; }
    }
    public class AuditModel
    {
        public DateTimeOffset? OnDate { get; set; }
        public string ByUser { get; set; }
        public long? UserId { get; set; }

    }

    public class ResponseModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }

        public string SecMessage { get; set; }
    }

    public class OrderResponseModel
    {
       
        public string orderId { get; set; }

        public string orderNumber { get; set; }
    }


    public class QrMOdel
    {

        public string partnerTxnUid { get; set; }

        public string partnerId { get; set; }

        public string partnerSecret { get; set; }
        public string merchantId { get; set; }
        public string qrType { get; set; }
        public decimal txnAmount { get; set; }
        public string txnCurrencyCode { get; set; }
        public string reference1 { get; set; }
        public string reference2 { get; set; }
        public string reference3 { get; set; }
        public string reference4 { get; set; }
        public string terminalId { get; set; }
        public string origPartnerTxnUid { get; set; }

        public string status { get; set; }



        public DateTime requestDt { get; set; }      
}

    public class QrForGenarateModel
    {

        public string partnerTxnUid { get; set; }

        public string partnerId { get; set; }

        public string partnerSecret { get; set; }
        public string merchantId { get; set; }
        public string qrType { get; set; }
        public decimal txnAmount { get; set; }
        public string txnCurrencyCode { get; set; }
        public string reference1 { get; set; }
        public string reference2 { get; set; }
        public string reference3 { get; set; }
        public string reference4 { get; set; }
        //public string terminalId { get; set; }
        //public string origPartnerTxnUid { get; set; }

        //public string status { get; set; }



        public DateTime requestDt { get; set; }
    }

    public class ProductFilterModel
    {
        
        public string name { get; set; }

        public string value { get; set; }


    }

}


