﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.CountryLanguageMapping;

namespace LotusLaundry.Domain.Country
{
    public class CountryModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public int TotalCount { get; set; }
        public string Name { get; set; }
        public List<CountryLanguageMappingModel> CountryLanguages { get; set; }
        public string CountryLanguageXml { get; set; }
    }
}
