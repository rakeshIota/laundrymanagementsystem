﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.Feedback
{
    public class FeedbackModel
    {
			  	public long? Id { get; set; }
				public int? TenantId { get; set; }
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public long? OrderId { get; set; }
			  	public string Type { get; set; }
			  	public string Message { get; set; }
			  	public long? Rating { get; set; }
			  	public long? DriverId { get; set; }
			  	public long? UserId { get; set; }
		public int TotalCount { get; set; }
    }
}
