﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using LotusLaundry.Domain.NotificationMasterLanguageMapping;

namespace LotusLaundry.Domain.NotificationMaster
{
    public class NotificationMasterModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string Type { get; set; }
        public bool? CanBeDelete { get; set; }
        public int TotalCount { get; set; }
        public string Message { get; set; }
        public string NotificationMasterKeyValueXml { get; set; }
        public List<NotificationMasterKeyValueMappingModel> NotificationMasterKeyValues { get; set; }
        public string NotificationMasterLanguageXml { get; set; }
        public List<NotificationMasterLanguageMappingModel> NotificationMasterLanguages { get; set; }
    }
}
