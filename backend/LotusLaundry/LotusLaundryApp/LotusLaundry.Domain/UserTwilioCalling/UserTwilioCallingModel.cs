﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.UserTwilioCalling
{
    public class UserTwilioCallingModel
    {

        public long? Id { get; set; }
        public long? FromUserId { get; set; }
        public long? ToUserId { get; set; }
        public bool? IsActive { get; set; }
    }
}
