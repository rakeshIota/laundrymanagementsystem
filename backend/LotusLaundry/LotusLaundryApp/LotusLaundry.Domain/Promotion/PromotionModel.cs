﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;


namespace LotusLaundry.Domain.Promotion
{
    public class PromotionModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }

        public string title { get; set; }
       


        public string FileXml { get; set; }
        public List<FileGroupItemsModel> File { get; set; }

        public string url { get; set; }

        

        public int TotalCount { get; set; }











    }
}
