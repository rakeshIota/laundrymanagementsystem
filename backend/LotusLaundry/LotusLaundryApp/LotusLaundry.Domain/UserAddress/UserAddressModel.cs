﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.UserAddress
{
    public class UserAddressModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public long? UserId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long? DistrictId { get; set; }
        public long? SubDistrictId { get; set; }
        public string PostalCode  { get; set; }
        public bool? IsDefault { get; set; }
        public string Tag { get; set; }
        public string ProvinceName { get; set; }

        public string ProvinceNameL { get; set; }
        public long? ProvinceId { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int TotalCount { get; set; }
        public string HouseNumber { get; set; }
        public string StreetNumber { get; set; }
        public string Note { get; set; }
        public string Type { get; set; }
        public string DistrictName { get; set; }

        public string DistrictNameL { get; set; }
        public string SubDistrictName { get; set; }

        public string SubDistrictNameL { get; set; }
        public string BuildingName { get; set; }

 
        public string Floor { get; set; }

 
        public string UnitNo { get; set; }

   
        public string PhoneNo { get; set; }
         
    
        public string alterNumber { get; set; }

     
        public string ResidenceType { get; set; }
    }
}
