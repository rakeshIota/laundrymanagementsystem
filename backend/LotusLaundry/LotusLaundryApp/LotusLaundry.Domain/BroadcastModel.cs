﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain
{
    public class Broadcastmodel
    {
        public long? Id { get; set; }

        public List<string> postalCodes { get; set; }

        public string customerType { get; set; }

        public string customerGender { get; set; }

        public string customerOrderType { get; set; }

        public long orderCount { get; set; }

        public string message { get; set; }


   
        public int? TenantId { get; set; }
   
        public string Slug { get; set; }
   
        public long? CreatedBy { get; set; }
       
        public long? UpdatedBy { get; set; }
       
        public DateTimeOffset? CreatedOn { get; set; }
     
        public DateTimeOffset? UpdatedOn { get; set; }
     
   
        public bool? IsDeleted { get; set; }
    
        public bool? IsActive { get; set; }

        public long? TotalUsers { get; set; }

        public long? Totalbatche { get; set; }

        public long? lastBatch { get; set; }

        public long? PendingUsers { get; set; }

        public string UserType { get; set; }

        public string postalCode { get; set; }

        public string Gender { get; set; }

        public string Relation { get; set; }

        public string Result { get; set; }

        public long OrderCount { get; set; }

        public int OverallCount { get; set; }


        public int next { get; set; }

        public int offset { get; set; }










       

    }
}
