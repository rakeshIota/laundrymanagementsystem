﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Domain.BankAccount
{
    public class BankAccountModel
    {
        public long Id { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string Currency { get; set; }
        public string Country { get; set; }
        public string AccountHolderType { get; set; }
        public Nullable<long> ExpertId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public System.DateTimeOffset UpdatedOn { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public bool? IsVoid { get; set; }
        public string BankAccountToken { get; set; }
        public string BankAccountStripeId { get; set; }
    }
}
