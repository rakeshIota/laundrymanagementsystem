﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;

namespace LotusLaundry.Domain.AppSetting
{
    public class AppSettingModel
    {
			  	public long? Id { get; set; }
				public int? TenantId { get; set; }
			  	public string Slug { get; set; }
			  	public long? CreatedBy { get; set; }
			  	public long? UpdatedBy { get; set; }
			  	public DateTimeOffset?  CreatedOn { get; set; }			
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			  	public bool? IsDeleted { get; set; }
			  	public bool? IsActive { get; set; }
			  	public string Key { get; set; }
			  	public string Value { get; set; }
		public int TotalCount { get; set; }
    }

    public class ApplicationSettingModel
    {

        
        public string SAMEDAY_DELIVERY_RATE { get; set; }

      
        public string EXPRESS_DELIVERY_RATE { get; set; }

        
        public string NORMAL_DELIVERY_RATE { get; set; }
     
        public string GoogleKeyRefresh { get; set; }

        public string timeSlot { get; set; }


        
        public string sameDayEndTime { get; set; }

        
        public string expressEndTime { get; set; }

       
        public string normalEndTime { get; set; }

      
        public string deliveryStartTime { get; set; }

        public string minimumOrderAmount { get; set; }


        public string logisticCharge { get; set; }

        public string availablePaymentTypes { get; set; }

        public string driverLocationInterval { get; set; }
        //
        public string driverOrderInterval { get; set; }



    }
}
