﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.Language;

namespace LotusLaundry.Domain.NotificationMasterLanguageMapping
{
    public class NotificationMasterLanguageMappingModel
    {
        public long? Id { get; set; }
        public int? TenantId { get; set; }
        public string Slug { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string LanguageXml { get; set; }

        public long? LanguageId { get; set; }
        public LanguageModel LanguageIdDetail { get; set; }
        public string Message { get; set; }
        public long? NotificationMasterId { get; set; }
        public int TotalCount { get; set; }
        public string LanguageName { get; set; }
    }
}
