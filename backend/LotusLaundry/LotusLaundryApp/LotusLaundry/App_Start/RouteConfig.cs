﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LotusLaundry
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
           
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "login",
                url: "login",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "404",
                url: "404",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "403",
                url: "403",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
              name: "dashboard",
              url: "dashboard",
              defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	        );	
	        
	        
	        routes.MapRoute(
              name: "account_recover",
              url: "account/recover/{ucode}",
              defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	        );	
	        
            routes.MapRoute(
                name: "changePassword",
                url: "dashboard/change/password",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "recoverPassword",
                url: "recoverPassword",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "users",
                url: "dashboard/users",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "useredit",
                url: "dashboard/user/edit/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "accountSettings",
                url: "dashboard/account/settings",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            
            
				routes.MapRoute(
	                name: "UsersList",
	                url: "dashboard/users",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UsersEdit",
	                url: "dashboard/users/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UsersDetail",
	                url: "dashboard/users/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "FeedbackList",
	                url: "dashboard/feedback",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "FeedbackEdit",
	                url: "dashboard/feedback/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "FeedbackDetail",
	                url: "dashboard/feedback/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "UserDeviceList",
	                url: "dashboard/user-device",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserDeviceEdit",
	                url: "dashboard/user-device/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserDeviceDetail",
	                url: "dashboard/user-device/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "PasswordLogList",
	                url: "dashboard/password-log",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PasswordLogEdit",
	                url: "dashboard/password-log/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PasswordLogDetail",
	                url: "dashboard/password-log/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "MessageLogList",
	                url: "dashboard/message-log",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "MessageLogEdit",
	                url: "dashboard/message-log/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "MessageLogDetail",
	                url: "dashboard/message-log/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "NotificationList",
	                url: "dashboard/notification",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "NotificationEdit",
	                url: "dashboard/notification/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "NotificationDetail",
	                url: "dashboard/notification/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "AppSettingList",
	                url: "dashboard/app-setting",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "AppSettingEdit",
	                url: "dashboard/app-setting/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "AppSettingDetail",
	                url: "dashboard/app-setting/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CountryList",
	                url: "dashboard/country",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CountryEdit",
	                url: "dashboard/country/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CountryDetail",
	                url: "dashboard/country/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CountryLanguageMappingList",
	                url: "dashboard/country-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CountryLanguageMappingEdit",
	                url: "dashboard/country-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CountryLanguageMappingDetail",
	                url: "dashboard/country-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "LanguageList",
	                url: "dashboard/language",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "LanguageEdit",
	                url: "dashboard/language/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "LanguageDetail",
	                url: "dashboard/language/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "StateList",
	                url: "dashboard/state",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "StateEdit",
	                url: "dashboard/state/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "StateDetail",
	                url: "dashboard/state/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "StateLanguageMappingList",
	                url: "dashboard/state-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "StateLanguageMappingEdit",
	                url: "dashboard/state-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "StateLanguageMappingDetail",
	                url: "dashboard/state-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CityList",
	                url: "dashboard/city",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CityEdit",
	                url: "dashboard/city/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CityDetail",
	                url: "dashboard/city/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CityLanguageMappingList",
	                url: "dashboard/city-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CityLanguageMappingEdit",
	                url: "dashboard/city-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CityLanguageMappingDetail",
	                url: "dashboard/city-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "UserAddressList",
	                url: "dashboard/user-address",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserAddressEdit",
	                url: "dashboard/user-address/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserAddressDetail",
	                url: "dashboard/user-address/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DistrictList",
	                url: "dashboard/district",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DistrictEdit",
	                url: "dashboard/district/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DistrictDetail",
	                url: "dashboard/district/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DistrictLanguageMappingList",
	                url: "dashboard/district-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DistrictLanguageMappingEdit",
	                url: "dashboard/district-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DistrictLanguageMappingDetail",
	                url: "dashboard/district-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "SubDistrictList",
	                url: "dashboard/sub-district",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "SubDistrictEdit",
	                url: "dashboard/sub-district/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "SubDistrictDetail",
	                url: "dashboard/sub-district/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "SubDistrictLanguageMappingList",
	                url: "dashboard/sub-district-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "SubDistrictLanguageMappingEdit",
	                url: "dashboard/sub-district-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "SubDistrictLanguageMappingDetail",
	                url: "dashboard/sub-district-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ProvinceList",
	                url: "dashboard/province",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProvinceEdit",
	                url: "dashboard/province/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProvinceDetail",
	                url: "dashboard/province/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ProvinceLanguageMappingList",
	                url: "dashboard/province-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProvinceLanguageMappingEdit",
	                url: "dashboard/province-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProvinceLanguageMappingDetail",
	                url: "dashboard/province-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "UserCordinateList",
	                url: "dashboard/user-cordinate",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserCordinateEdit",
	                url: "dashboard/user-cordinate/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "UserCordinateDetail",
	                url: "dashboard/user-cordinate/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ServiceList",
	                url: "dashboard/service",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ServiceEdit",
	                url: "dashboard/service/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ServiceDetail",
	                url: "dashboard/service/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ServiceLanguageMappingList",
	                url: "dashboard/service-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ServiceLanguageMappingEdit",
	                url: "dashboard/service-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ServiceLanguageMappingDetail",
	                url: "dashboard/service-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ProductList",
	                url: "dashboard/product",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductEdit",
	                url: "dashboard/product/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductDetail",
	                url: "dashboard/product/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ProductLanguageMappingList",
	                url: "dashboard/product-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductLanguageMappingEdit",
	                url: "dashboard/product-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductLanguageMappingDetail",
	                url: "dashboard/product-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "ProductPriceList",
	                url: "dashboard/product-price",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductPriceEdit",
	                url: "dashboard/product-price/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "ProductPriceDetail",
	                url: "dashboard/product-price/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DeliveryTypeList",
	                url: "dashboard/delivery-type",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DeliveryTypeEdit",
	                url: "dashboard/delivery-type/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DeliveryTypeDetail",
	                url: "dashboard/delivery-type/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DeliveryTypeLanguageMappingList",
	                url: "dashboard/delivery-type-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DeliveryTypeLanguageMappingEdit",
	                url: "dashboard/delivery-type-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DeliveryTypeLanguageMappingDetail",
	                url: "dashboard/delivery-type-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CartList",
	                url: "dashboard/cart",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartEdit",
	                url: "dashboard/cart/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartDetail",
	                url: "dashboard/cart/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CartItemList",
	                url: "dashboard/cart-item",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartItemEdit",
	                url: "dashboard/cart-item/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartItemDetail",
	                url: "dashboard/cart-item/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CartItemServiceMappingList",
	                url: "dashboard/cart-item-service-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartItemServiceMappingEdit",
	                url: "dashboard/cart-item-service-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CartItemServiceMappingDetail",
	                url: "dashboard/cart-item-service-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "OrderList",
	                url: "dashboard/order",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderEdit",
	                url: "dashboard/order/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderDetail",
	                url: "dashboard/order/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "OrderItemList",
	                url: "dashboard/order-item",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderItemEdit",
	                url: "dashboard/order-item/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderItemDetail",
	                url: "dashboard/order-item/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "OrderItemServiceMappingList",
	                url: "dashboard/order-item-service-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderItemServiceMappingEdit",
	                url: "dashboard/order-item-service-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderItemServiceMappingDetail",
	                url: "dashboard/order-item-service-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "OrderLogList",
	                url: "dashboard/order-log",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderLogEdit",
	                url: "dashboard/order-log/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "OrderLogDetail",
	                url: "dashboard/order-log/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "PaymentLogList",
	                url: "dashboard/payment-log",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PaymentLogEdit",
	                url: "dashboard/payment-log/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PaymentLogDetail",
	                url: "dashboard/payment-log/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "PackingList",
	                url: "dashboard/packing",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PackingEdit",
	                url: "dashboard/packing/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PackingDetail",
	                url: "dashboard/packing/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "PackingLanguageMappingList",
	                url: "dashboard/packing-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PackingLanguageMappingEdit",
	                url: "dashboard/packing-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "PackingLanguageMappingDetail",
	                url: "dashboard/packing-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DefaultMessageList",
	                url: "dashboard/default-message",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DefaultMessageEdit",
	                url: "dashboard/default-message/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DefaultMessageDetail",
	                url: "dashboard/default-message/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "DefaultMessageLanguageMappingList",
	                url: "dashboard/default-message-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DefaultMessageLanguageMappingEdit",
	                url: "dashboard/default-message-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "DefaultMessageLanguageMappingDetail",
	                url: "dashboard/default-message-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CurrencyList",
	                url: "dashboard/currency",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CurrencyEdit",
	                url: "dashboard/currency/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CurrencyDetail",
	                url: "dashboard/currency/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
				routes.MapRoute(
	                name: "CurrencyLanguageMappingList",
	                url: "dashboard/currency-language-mapping",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CurrencyLanguageMappingEdit",
	                url: "dashboard/currency-language-mapping/edit/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
	            routes.MapRoute(
	                name: "CurrencyLanguageMappingDetail",
	                url: "dashboard/currency-language-mapping/detail/{id}",
	                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
	            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
