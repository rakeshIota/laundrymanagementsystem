﻿using LotusLaundry.Domain.Exception;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Providers
{
    public class ManageApplicationUserModel
    {
        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        public ApplicationUser UserViewModelToApplicationUserModel(UsersViewModel model)
        {
            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.MobileNo,
                PhoneNumberConfirmed = false,
                EmailConfirmed = false,
                IsActive = true,
                TwoFactorEnabled = model.RoleName == "" ? true : false,
                FirstName = model.FirstName,
                LastName = model.LastName,
                TenantId = ManageClaims.GetUserClaim().TenantId,
            };
            return user;
        }

        public UsersViewModel ApplicationUserModelToUserViewModel(ApplicationUser user)
        {
            var userData = new UsersViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + user.Id.ToString(), KeyConstant.Key),
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.Email,
                FullName = user.FirstName + " " + user.LastName,
                Roles = UserManager.GetRoles(user.Id).ToList(),
                ProfileImageUrl = user.ProfilePic,
                UniqueCode = user.UniqueCode,
                IsActive = user.IsActive,
                IsDeleted = user.IsDeleted,
                EmailConfirmed = user.EmailConfirmed
            };
            return userData;
        }
        public ExceptionModel ExceptionToExceptionModel(Exception exception, int entityCode, int methodCode)
        {
            var _exception = new ExceptionModel
            {
                Message = exception.Message,
                Source = exception.Source,
                StackTrace = exception.StackTrace,
                EntityCode = entityCode,
                ResponseCode = methodCode,
            };
            return _exception;
        }
    }
}
