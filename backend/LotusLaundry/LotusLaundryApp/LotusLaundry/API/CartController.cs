using LotusLaundry.Framework.ViewModels.Cart;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Cart;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class CartController : ApiController
    {
        private ICartService _cartService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CartController(ICartService cartService, IExceptionService exceptionService)
        {
            _cartService = cartService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Cart
        /// </summary>
        /// <returns></returns>
        [Route("Cart")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCart(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var cart = _cartService.SelectCart(param).Select(x => x.ToViewModel()); ;
            	return Ok(cart.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get cart by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cart/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCart(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var cart = _cartService.SelectCart(param).FirstOrDefault().ToViewModel();
	            if (cart.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(cart.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cart")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCart(CartViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _cartService.CartInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CART + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Cart save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cart")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCart(CartViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartService.CartUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Cart updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete cart by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cart/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCart(string id)
        {
        	try{
	            CartViewModel model = new CartViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartService.CartUpdate(model.ToModel());
	            return Ok("Cart Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Cart, (int)ErrorFunctionCode.Cart_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Cart_Delete).ToString().ErrorResponse());
            }
        }
    }
}

