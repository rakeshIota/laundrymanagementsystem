using LotusLaundry.Framework.ViewModels.ServiceLanguageMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.ServiceLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class ServiceLanguageMappingController : ApiController
    {
        private IServiceLanguageMappingService _servicelanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ServiceLanguageMappingController(IServiceLanguageMappingService servicelanguagemappingService, IExceptionService exceptionService)
        {
            _servicelanguagemappingService = servicelanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all ServiceLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("ServiceLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllServiceLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var servicelanguagemappings = _servicelanguagemappingService.SelectServiceLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(servicelanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ServiceLanguageMapping, (int)ErrorFunctionCode.ServiceLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ServiceLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get servicelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("servicelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetServiceLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var servicelanguagemapping = _servicelanguagemappingService.SelectServiceLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (servicelanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(servicelanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ServiceLanguageMapping, (int)ErrorFunctionCode.ServiceLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ServiceLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save servicelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("servicelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveServiceLanguageMapping(ServiceLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _servicelanguagemappingService.ServiceLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.SERVICELANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("ServiceLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ServiceLanguageMapping, (int)ErrorFunctionCode.ServiceLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ServiceLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update servicelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("servicelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateServiceLanguageMapping(ServiceLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _servicelanguagemappingService.ServiceLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("ServiceLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ServiceLanguageMapping, (int)ErrorFunctionCode.ServiceLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ServiceLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete servicelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("servicelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteServiceLanguageMapping(string id)
        {
        	try{
	            ServiceLanguageMappingViewModel model = new ServiceLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _servicelanguagemappingService.ServiceLanguageMappingUpdate(model.ToModel());
	            return Ok("ServiceLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ServiceLanguageMapping, (int)ErrorFunctionCode.ServiceLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ServiceLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

