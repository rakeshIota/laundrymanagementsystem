using LotusLaundry.Framework.ViewModels.SubDistrict;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.SubDistrict;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class SubDistrictController : ApiController
    {
        private ISubDistrictService _subdistrictService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public SubDistrictController(ISubDistrictService subdistrictService, IExceptionService exceptionService)
        {
            _subdistrictService = subdistrictService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all SubDistrict
        /// </summary>
        /// <returns></returns>
        [Route("SubDistricts")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllSubDistricts(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var subdistricts = _subdistrictService.SelectSubDistrict(param).Select(x => x.ToViewModel()); ;
            	return Ok(subdistricts.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrict, (int)ErrorFunctionCode.SubDistrict_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get subdistrict by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("subdistrict/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetSubDistrict(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var subdistrict = _subdistrictService.SelectSubDistrict(param).FirstOrDefault().ToViewModel();
	            if (subdistrict.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(subdistrict.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrict, (int)ErrorFunctionCode.SubDistrict_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save subdistrict
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("subdistrict")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveSubDistrict(SubDistrictViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _subdistrictService.SubDistrictInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("SubDistrict save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrict, (int)ErrorFunctionCode.SubDistrict_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update subdistrict
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("subdistrict")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateSubDistrict(SubDistrictViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _subdistrictService.SubDistrictUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("SubDistrict updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrict, (int)ErrorFunctionCode.SubDistrict_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete subdistrict by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("subdistrict/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteSubDistrict(string id)
        {
        	try{
	            SubDistrictViewModel model = new SubDistrictViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _subdistrictService.SubDistrictUpdate(model.ToModel());
	            return Ok("SubDistrict Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrict, (int)ErrorFunctionCode.SubDistrict_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrict_Delete).ToString().ErrorResponse());
            }
        }
    }
}

