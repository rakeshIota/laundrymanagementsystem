﻿using LotusLaundry.Domain.Image;
using LotusLaundry.Service.ImageUpload;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Service.Upload;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class FileUploadController : ApiController
    {
        private IFileGroupService _fileGroupService;
        private IFileUploadService _fileUploadService;
        private IImageService _imageService;
        private string _uploadPath;
        private readonly string _imageRootPath;
        private readonly string _imageOption;
        private readonly MultipartFormDataStreamProvider _streamProvider;
        public FileUploadController(IFileGroupService fileGroupService, IFileUploadService fileUploadService, IImageService imageService)
        {
            //_imageRootPath = ConfigurationManager.AppSettings["ImageRootPath"];
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _fileGroupService = fileGroupService;
            _fileUploadService = fileUploadService;
            _imageService = imageService;
            _uploadPath = CommonMethods.CreateDirectory();
            _streamProvider = new MultipartFormDataStreamProvider(_uploadPath);
        }

        

        [Route("file/group/items/upload")]
        [HttpPost]
        public async Task<IEnumerable<FileGroupItemsViewModel>> FileGroupItemsAdd(HttpRequestMessage request)
        {
            var provider = new PhotoMultipartFormDataStreamProvider(_uploadPath);
            /* file upload in folder */
            if (_imageOption == FileType.TYPE_FOLDER)
            {
                await request.Content.ReadAsMultipartAsync(provider);
                var bodylengthForFolder = request.Content.Headers.ContentLength;
                return _fileUploadService.ProcessDocs(provider.FileData, Convert.ToInt32(bodylengthForFolder)).Select(x => x.ToViewModel()).ToList();
            }

            /* file upload on cloud */

            var filesReadToProvider = await request.Content.ReadAsMultipartAsync();
            var bodylengthForCloud = request.Content.Headers.ContentLength;

            var obj = new UploadedImage();
            foreach (var stream in filesReadToProvider.Contents)
            {
                var type = stream.Headers.ContentType.MediaType.Split('/');
                obj.ContentType = stream.Headers.ContentType.MediaType;
                obj.Data = await stream.ReadAsByteArrayAsync();
                obj.Name = CommonMethods.GetUniqueKey(6) + stream.Headers.ContentDisposition.FileName.Replace("\"", "");
                obj.Url = _imageRootPath + "/" + obj.Name;
            }

            await Task.Run(() => _imageService.AddImageToBlobStorageAsync(obj));

            var attachment = new List<FileGroupItemsModel>();

            attachment.Add(new FileGroupItemsModel()
            {
                Filename = obj.Name,
                Path = obj.Url,
                MimeType = obj.ContentType,
                Size = bodylengthForCloud,
                OriginalName = obj.Name
            });

            return attachment.Select(x => x.ToViewModel()).ToList();
        }

        [Route("expert/file/group/items/upload")]
        [HttpPost]
        public async Task<IEnumerable<FileGroupItemsViewModel>> ExpertFileGroupItemsAdd(HttpRequestMessage request)
        {
            var provider = new PhotoMultipartFormDataStreamProvider(_uploadPath);
            await request.Content.ReadAsMultipartAsync(provider);
            var bodylength = request.Content.Headers.ContentLength;
            return _fileUploadService.ProcessDocs(provider.FileData, Convert.ToInt32(bodylength)).Select(x => x.ToViewModel()).ToList();
        }

        [Route("file/group/item/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult FileGroupItemDelete(long? id)
        {
            var userId = User.Identity.GetUserId<long>();
            _fileGroupService.FileGroupItemsDelete(id, userId);
            return Ok("File Deleted".SuccessResponse());
        }
    }
}
