using LotusLaundry.Framework.ViewModels.Feedback;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Feedback;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class FeedbackController : ApiController
    {
        private IFeedbackService _feedbackService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public FeedbackController(IFeedbackService feedbackService, IExceptionService exceptionService)
        {
            _feedbackService = feedbackService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Feedback
        /// </summary>
        /// <returns></returns>
        [Route("Feedbacks")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllFeedbacks(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var feedbacks = _feedbackService.SelectFeedback(param).Select(x => x.ToViewModel()); ;
            	return Ok(feedbacks.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Feedback, (int)ErrorFunctionCode.Feedback_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Feedback_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get feedback by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("feedback/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetFeedback(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var feedback = _feedbackService.SelectFeedback(param).FirstOrDefault().ToViewModel();
	            if (feedback.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(feedback.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Feedback, (int)ErrorFunctionCode.Feedback_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Feedback_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save feedback
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("feedback")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveFeedback(FeedbackViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _feedbackService.FeedbackInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.FEEDBACK + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Feedback save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Feedback, (int)ErrorFunctionCode.Feedback_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Feedback_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update feedback
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("feedback")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateFeedback(FeedbackViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _feedbackService.FeedbackUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Feedback updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Feedback, (int)ErrorFunctionCode.Feedback_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Feedback_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete feedback by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("feedback/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteFeedback(string id)
        {
        	try{
	            FeedbackViewModel model = new FeedbackViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _feedbackService.FeedbackUpdate(model.ToModel());
	            return Ok("Feedback Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Feedback, (int)ErrorFunctionCode.Feedback_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Feedback_Delete).ToString().ErrorResponse());
            }
        }
    }
}

