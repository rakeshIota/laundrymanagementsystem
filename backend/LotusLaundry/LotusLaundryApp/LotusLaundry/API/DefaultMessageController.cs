using LotusLaundry.Framework.ViewModels.DefaultMessage;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.DefaultMessage;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class DefaultMessageController : ApiController
    {
        private IDefaultMessageService _defaultmessageService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DefaultMessageController(IDefaultMessageService defaultmessageService, IExceptionService exceptionService)
        {
            _defaultmessageService = defaultmessageService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all DefaultMessage
        /// </summary>
        /// <returns></returns>
        [Route("DefaultMessages")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllDefaultMessages(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var defaultmessages = _defaultmessageService.SelectDefaultMessage(param).Select(x => x.ToViewModel()); ;
            	return Ok(defaultmessages.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessage, (int)ErrorFunctionCode.DefaultMessage_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessage_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get defaultmessage by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("defaultmessage/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetDefaultMessage(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var defaultmessage = _defaultmessageService.SelectDefaultMessage(param).FirstOrDefault().ToViewModel();
	            if (defaultmessage.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(defaultmessage.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessage, (int)ErrorFunctionCode.DefaultMessage_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessage_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save defaultmessage
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("defaultmessage")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDefaultMessage(DefaultMessageViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _defaultmessageService.DefaultMessageInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DEFAULTMESSAGE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("DefaultMessage save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessage, (int)ErrorFunctionCode.DefaultMessage_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessage_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update defaultmessage
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("defaultmessage")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDefaultMessage(DefaultMessageViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _defaultmessageService.DefaultMessageUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("DefaultMessage updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessage, (int)ErrorFunctionCode.DefaultMessage_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessage_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete defaultmessage by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("defaultmessage/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDefaultMessage(string id)
        {
        	try{
	            DefaultMessageViewModel model = new DefaultMessageViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _defaultmessageService.DefaultMessageUpdate(model.ToModel());
	            return Ok("DefaultMessage Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessage, (int)ErrorFunctionCode.DefaultMessage_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessage_Delete).ToString().ErrorResponse());
            }
        }
    }
}

