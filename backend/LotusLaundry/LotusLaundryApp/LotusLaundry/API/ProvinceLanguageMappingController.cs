using LotusLaundry.Framework.ViewModels.ProvinceLanguageMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.ProvinceLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class ProvinceLanguageMappingController : ApiController
    {
        private IProvinceLanguageMappingService _provincelanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProvinceLanguageMappingController(IProvinceLanguageMappingService provincelanguagemappingService, IExceptionService exceptionService)
        {
            _provincelanguagemappingService = provincelanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all ProvinceLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("ProvinceLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProvinceLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var provincelanguagemappings = _provincelanguagemappingService.SelectProvinceLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(provincelanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProvinceLanguageMapping, (int)ErrorFunctionCode.ProvinceLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProvinceLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get provincelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("provincelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetProvinceLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var provincelanguagemapping = _provincelanguagemappingService.SelectProvinceLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (provincelanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(provincelanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProvinceLanguageMapping, (int)ErrorFunctionCode.ProvinceLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProvinceLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save provincelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("provincelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveProvinceLanguageMapping(ProvinceLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _provincelanguagemappingService.ProvinceLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PROVINCELANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("ProvinceLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProvinceLanguageMapping, (int)ErrorFunctionCode.ProvinceLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProvinceLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update provincelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("provincelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateProvinceLanguageMapping(ProvinceLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _provincelanguagemappingService.ProvinceLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("ProvinceLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProvinceLanguageMapping, (int)ErrorFunctionCode.ProvinceLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProvinceLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete provincelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("provincelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteProvinceLanguageMapping(string id)
        {
        	try{
	            ProvinceLanguageMappingViewModel model = new ProvinceLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _provincelanguagemappingService.ProvinceLanguageMappingUpdate(model.ToModel());
	            return Ok("ProvinceLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProvinceLanguageMapping, (int)ErrorFunctionCode.ProvinceLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProvinceLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

