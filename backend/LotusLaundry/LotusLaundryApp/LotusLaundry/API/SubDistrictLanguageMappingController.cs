using LotusLaundry.Framework.ViewModels.SubDistrictLanguageMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.SubDistrictLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class SubDistrictLanguageMappingController : ApiController
    {
        private ISubDistrictLanguageMappingService _subdistrictlanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public SubDistrictLanguageMappingController(ISubDistrictLanguageMappingService subdistrictlanguagemappingService, IExceptionService exceptionService)
        {
            _subdistrictlanguagemappingService = subdistrictlanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all SubDistrictLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("SubDistrictLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllSubDistrictLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var subdistrictlanguagemappings = _subdistrictlanguagemappingService.SelectSubDistrictLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(subdistrictlanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrictLanguageMapping, (int)ErrorFunctionCode.SubDistrictLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrictLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get subdistrictlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("subdistrictlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetSubDistrictLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var subdistrictlanguagemapping = _subdistrictlanguagemappingService.SelectSubDistrictLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (subdistrictlanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(subdistrictlanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrictLanguageMapping, (int)ErrorFunctionCode.SubDistrictLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrictLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save subdistrictlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("subdistrictlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveSubDistrictLanguageMapping(SubDistrictLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _subdistrictlanguagemappingService.SubDistrictLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.SUBDISTRICTLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("SubDistrictLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrictLanguageMapping, (int)ErrorFunctionCode.SubDistrictLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrictLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update subdistrictlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("subdistrictlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateSubDistrictLanguageMapping(SubDistrictLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _subdistrictlanguagemappingService.SubDistrictLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("SubDistrictLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrictLanguageMapping, (int)ErrorFunctionCode.SubDistrictLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrictLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete subdistrictlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("subdistrictlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteSubDistrictLanguageMapping(string id)
        {
        	try{
	            SubDistrictLanguageMappingViewModel model = new SubDistrictLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _subdistrictlanguagemappingService.SubDistrictLanguageMappingUpdate(model.ToModel());
	            return Ok("SubDistrictLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.SubDistrictLanguageMapping, (int)ErrorFunctionCode.SubDistrictLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.SubDistrictLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

