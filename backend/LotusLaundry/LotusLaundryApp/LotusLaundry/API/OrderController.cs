using LotusLaundry.Framework.ViewModels.Order;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class OrderController : ApiController
    {
        private IOrderService _orderService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public OrderController(IOrderService orderService, IExceptionService exceptionService)
        {
            _orderService = orderService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Order
        /// </summary>
        /// <returns></returns>
        [Route("Orders")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllOrders(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var orders = _orderService.SelectOrder(param).Select(x => x.ToViewModel()); ;
            	return Ok(orders.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get order by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetOrder(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var order = _orderService.SelectOrder(param).FirstOrDefault().ToViewModel();
	            if (order.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(order.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("order")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveOrder(OrderViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _orderService.OrderInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.ORDER + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Order save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("order")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrder(OrderViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderService.OrderUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Order updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete order by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("order/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteOrder(string id)
        {
        	try{
	            OrderViewModel model = new OrderViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderService.OrderUpdate(model.ToModel());
	            return Ok("Order Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Order, (int)ErrorFunctionCode.Order_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Order_Delete).ToString().ErrorResponse());
            }
        }
    }
}

