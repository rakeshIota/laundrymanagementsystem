using LotusLaundry.Framework.ViewModels.Province;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Province;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class ProvinceController : ApiController
    {
        private IProvinceService _provinceService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProvinceController(IProvinceService provinceService, IExceptionService exceptionService)
        {
            _provinceService = provinceService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Province
        /// </summary>
        /// <returns></returns>
        [Route("Provinces")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProvinces(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var provinces = _provinceService.SelectProvince(param).Select(x => x.ToViewModel()); ;
            	return Ok(provinces.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Province, (int)ErrorFunctionCode.Province_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get province by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("province/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetProvince(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var province = _provinceService.SelectProvince(param).FirstOrDefault().ToViewModel();
	            if (province.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(province.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Province, (int)ErrorFunctionCode.Province_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save province
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("province")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveProvince(ProvinceViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _provinceService.ProvinceInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PROVINCE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Province save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Province, (int)ErrorFunctionCode.Province_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update province
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("province")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateProvince(ProvinceViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _provinceService.ProvinceUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Province updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Province, (int)ErrorFunctionCode.Province_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete province by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("province/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteProvince(string id)
        {
        	try{
	            ProvinceViewModel model = new ProvinceViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _provinceService.ProvinceUpdate(model.ToModel());
	            return Ok("Province Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Province, (int)ErrorFunctionCode.Province_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Province_Delete).ToString().ErrorResponse());
            }
        }
    }
}

