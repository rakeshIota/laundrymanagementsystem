using LotusLaundry.Framework.ViewModels.PaymentLog;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class PaymentLogController : ApiController
    {
        private IPaymentLogService _paymentlogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PaymentLogController(IPaymentLogService paymentlogService, IExceptionService exceptionService)
        {
            _paymentlogService = paymentlogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all PaymentLog
        /// </summary>
        /// <returns></returns>
        [Route("PaymentLogs")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPaymentLogs(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var paymentlogs = _paymentlogService.SelectPaymentLog(param).Select(x => x.ToViewModel()); ;
            	return Ok(paymentlogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PaymentLog, (int)ErrorFunctionCode.PaymentLog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PaymentLog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get paymentlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("paymentlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetPaymentLog(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var paymentlog = _paymentlogService.SelectPaymentLog(param).FirstOrDefault().ToViewModel();
	            if (paymentlog.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(paymentlog.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PaymentLog, (int)ErrorFunctionCode.PaymentLog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PaymentLog_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save paymentlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("paymentlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePaymentLog(PaymentLogViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _paymentlogService.PaymentLogInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PAYMENTLOG + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("PaymentLog save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PaymentLog, (int)ErrorFunctionCode.PaymentLog_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PaymentLog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update paymentlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("paymentlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePaymentLog(PaymentLogViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _paymentlogService.PaymentLogUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("PaymentLog updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PaymentLog, (int)ErrorFunctionCode.PaymentLog_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PaymentLog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete paymentlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("paymentlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePaymentLog(string id)
        {
        	try{
	            PaymentLogViewModel model = new PaymentLogViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _paymentlogService.PaymentLogUpdate(model.ToModel());
	            return Ok("PaymentLog Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.PaymentLog, (int)ErrorFunctionCode.PaymentLog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.PaymentLog_Delete).ToString().ErrorResponse());
            }
        }
    }
}

