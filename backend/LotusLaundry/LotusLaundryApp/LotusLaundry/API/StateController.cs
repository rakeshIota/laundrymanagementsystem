using LotusLaundry.Framework.ViewModels.State;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.State;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class StateController : ApiController
    {
        private IStateService _stateService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public StateController(IStateService stateService, IExceptionService exceptionService)
        {
            _stateService = stateService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all State
        /// </summary>
        /// <returns></returns>
        [Route("States")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllStates(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var states = _stateService.SelectState(param).Select(x => x.ToViewModel()); ;
            	return Ok(states.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.State, (int)ErrorFunctionCode.State_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.State_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get state by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("state/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetState(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var state = _stateService.SelectState(param).FirstOrDefault().ToViewModel();
	            if (state.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(state.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.State, (int)ErrorFunctionCode.State_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.State_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save state
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("state")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveState(StateViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _stateService.StateInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.STATE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("State save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.State, (int)ErrorFunctionCode.State_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.State_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update state
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("state")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateState(StateViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _stateService.StateUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("State updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.State, (int)ErrorFunctionCode.State_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.State_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete state by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("state/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteState(string id)
        {
        	try{
	            StateViewModel model = new StateViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _stateService.StateUpdate(model.ToModel());
	            return Ok("State Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.State, (int)ErrorFunctionCode.State_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.State_Delete).ToString().ErrorResponse());
            }
        }
    }
}

