using LotusLaundry.Framework.ViewModels.CartItemServiceMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.CartItemServiceMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class CartItemServiceMappingController : ApiController
    {
        private ICartItemServiceMappingService _cartitemservicemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CartItemServiceMappingController(ICartItemServiceMappingService cartitemservicemappingService, IExceptionService exceptionService)
        {
            _cartitemservicemappingService = cartitemservicemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all CartItemServiceMapping
        /// </summary>
        /// <returns></returns>
        [Route("CartItemServiceMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCartItemServiceMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var cartitemservicemappings = _cartitemservicemappingService.SelectCartItemServiceMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(cartitemservicemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItemServiceMapping, (int)ErrorFunctionCode.CartItemServiceMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItemServiceMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get cartitemservicemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cartitemservicemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCartItemServiceMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var cartitemservicemapping = _cartitemservicemappingService.SelectCartItemServiceMapping(param).FirstOrDefault().ToViewModel();
	            if (cartitemservicemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(cartitemservicemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItemServiceMapping, (int)ErrorFunctionCode.CartItemServiceMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItemServiceMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save cartitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cartitemservicemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCartItemServiceMapping(CartItemServiceMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _cartitemservicemappingService.CartItemServiceMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CARTITEMSERVICEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("CartItemServiceMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItemServiceMapping, (int)ErrorFunctionCode.CartItemServiceMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItemServiceMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update cartitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("cartitemservicemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCartItemServiceMapping(CartItemServiceMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartitemservicemappingService.CartItemServiceMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("CartItemServiceMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItemServiceMapping, (int)ErrorFunctionCode.CartItemServiceMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItemServiceMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete cartitemservicemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("cartitemservicemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCartItemServiceMapping(string id)
        {
        	try{
	            CartItemServiceMappingViewModel model = new CartItemServiceMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cartitemservicemappingService.CartItemServiceMappingUpdate(model.ToModel());
	            return Ok("CartItemServiceMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.CartItemServiceMapping, (int)ErrorFunctionCode.CartItemServiceMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.CartItemServiceMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

