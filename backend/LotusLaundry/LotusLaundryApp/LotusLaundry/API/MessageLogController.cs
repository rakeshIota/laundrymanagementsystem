using LotusLaundry.Framework.ViewModels.MessageLog;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.MessageLog;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class MessageLogController : ApiController
    {
        private IMessageLogService _messagelogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public MessageLogController(IMessageLogService messagelogService, IExceptionService exceptionService)
        {
            _messagelogService = messagelogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all MessageLog
        /// </summary>
        /// <returns></returns>
        [Route("MessageLogs")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllMessageLogs(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var messagelogs = _messagelogService.SelectMessageLog(param).Select(x => x.ToViewModel()); ;
            	return Ok(messagelogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.MessageLog, (int)ErrorFunctionCode.MessageLog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.MessageLog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get messagelog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("messagelog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetMessageLog(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var messagelog = _messagelogService.SelectMessageLog(param).FirstOrDefault().ToViewModel();
	            if (messagelog.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(messagelog.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.MessageLog, (int)ErrorFunctionCode.MessageLog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.MessageLog_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save messagelog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("messagelog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveMessageLog(MessageLogViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _messagelogService.MessageLogInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.MESSAGELOG + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("MessageLog save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.MessageLog, (int)ErrorFunctionCode.MessageLog_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.MessageLog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update messagelog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("messagelog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateMessageLog(MessageLogViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _messagelogService.MessageLogUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("MessageLog updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.MessageLog, (int)ErrorFunctionCode.MessageLog_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.MessageLog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete messagelog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("messagelog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteMessageLog(string id)
        {
        	try{
	            MessageLogViewModel model = new MessageLogViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _messagelogService.MessageLogUpdate(model.ToModel());
	            return Ok("MessageLog Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.MessageLog, (int)ErrorFunctionCode.MessageLog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.MessageLog_Delete).ToString().ErrorResponse());
            }
        }
    }
}

