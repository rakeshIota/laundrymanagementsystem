using LotusLaundry.Framework.ViewModels.OrderLog;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.OrderLog;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class OrderLogController : ApiController
    {
        private IOrderLogService _orderlogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public OrderLogController(IOrderLogService orderlogService, IExceptionService exceptionService)
        {
            _orderlogService = orderlogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all OrderLog
        /// </summary>
        /// <returns></returns>
        [Route("OrderLogs")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllOrderLogs(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var orderlogs = _orderlogService.SelectOrderLog(param).Select(x => x.ToViewModel()); ;
            	return Ok(orderlogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderLog, (int)ErrorFunctionCode.OrderLog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderLog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get orderlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetOrderLog(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var orderlog = _orderlogService.SelectOrderLog(param).FirstOrDefault().ToViewModel();
	            if (orderlog.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(orderlog.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderLog, (int)ErrorFunctionCode.OrderLog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderLog_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save orderlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveOrderLog(OrderLogViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _orderlogService.OrderLogInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.ORDERLOG + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("OrderLog save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderLog, (int)ErrorFunctionCode.OrderLog_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderLog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update orderlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderlog")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrderLog(OrderLogViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderlogService.OrderLogUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("OrderLog updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderLog, (int)ErrorFunctionCode.OrderLog_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderLog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete orderlog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderlog/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteOrderLog(string id)
        {
        	try{
	            OrderLogViewModel model = new OrderLogViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderlogService.OrderLogUpdate(model.ToModel());
	            return Ok("OrderLog Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderLog, (int)ErrorFunctionCode.OrderLog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderLog_Delete).ToString().ErrorResponse());
            }
        }
    }
}

