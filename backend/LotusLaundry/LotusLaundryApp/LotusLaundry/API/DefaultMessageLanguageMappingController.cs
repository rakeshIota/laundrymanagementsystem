using LotusLaundry.Framework.ViewModels.DefaultMessageLanguageMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.DefaultMessageLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class DefaultMessageLanguageMappingController : ApiController
    {
        private IDefaultMessageLanguageMappingService _defaultmessagelanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public DefaultMessageLanguageMappingController(IDefaultMessageLanguageMappingService defaultmessagelanguagemappingService, IExceptionService exceptionService)
        {
            _defaultmessagelanguagemappingService = defaultmessagelanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all DefaultMessageLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("DefaultMessageLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllDefaultMessageLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var defaultmessagelanguagemappings = _defaultmessagelanguagemappingService.SelectDefaultMessageLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(defaultmessagelanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessageLanguageMapping, (int)ErrorFunctionCode.DefaultMessageLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessageLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get defaultmessagelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("defaultmessagelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetDefaultMessageLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var defaultmessagelanguagemapping = _defaultmessagelanguagemappingService.SelectDefaultMessageLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (defaultmessagelanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(defaultmessagelanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessageLanguageMapping, (int)ErrorFunctionCode.DefaultMessageLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessageLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save defaultmessagelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("defaultmessagelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveDefaultMessageLanguageMapping(DefaultMessageLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _defaultmessagelanguagemappingService.DefaultMessageLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.DEFAULTMESSAGELANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("DefaultMessageLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessageLanguageMapping, (int)ErrorFunctionCode.DefaultMessageLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessageLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update defaultmessagelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("defaultmessagelanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateDefaultMessageLanguageMapping(DefaultMessageLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _defaultmessagelanguagemappingService.DefaultMessageLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("DefaultMessageLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessageLanguageMapping, (int)ErrorFunctionCode.DefaultMessageLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessageLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete defaultmessagelanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("defaultmessagelanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteDefaultMessageLanguageMapping(string id)
        {
        	try{
	            DefaultMessageLanguageMappingViewModel model = new DefaultMessageLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _defaultmessagelanguagemappingService.DefaultMessageLanguageMappingUpdate(model.ToModel());
	            return Ok("DefaultMessageLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.DefaultMessageLanguageMapping, (int)ErrorFunctionCode.DefaultMessageLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.DefaultMessageLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

