using LotusLaundry.Framework.ViewModels.City;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.City;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class CityController : ApiController
    {
        private ICityService _cityService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CityController(ICityService cityService, IExceptionService exceptionService)
        {
            _cityService = cityService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all City
        /// </summary>
        /// <returns></returns>
        [Route("Cities")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllCities(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var cities = _cityService.SelectCity(param).Select(x => x.ToViewModel()); ;
            	return Ok(cities.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.City, (int)ErrorFunctionCode.City_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.City_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get city by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("city/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetCity(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var city = _cityService.SelectCity(param).FirstOrDefault().ToViewModel();
	            if (city.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(city.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.City, (int)ErrorFunctionCode.City_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.City_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save city
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("city")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveCity(CityViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _cityService.CityInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.CITY + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("City save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.City, (int)ErrorFunctionCode.City_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.City_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update city
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("city")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateCity(CityViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cityService.CityUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("City updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.City, (int)ErrorFunctionCode.City_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.City_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete city by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("city/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteCity(string id)
        {
        	try{
	            CityViewModel model = new CityViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _cityService.CityUpdate(model.ToModel());
	            return Ok("City Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.City, (int)ErrorFunctionCode.City_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.City_Delete).ToString().ErrorResponse());
            }
        }
    }
}

