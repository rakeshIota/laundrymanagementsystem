using LotusLaundry.Framework.ViewModels.UserAddress;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.UserAddress;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class UserAddressController : ApiController
    {
        private IUserAddressService _useraddressService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public UserAddressController(IUserAddressService useraddressService, IExceptionService exceptionService)
        {
            _useraddressService = useraddressService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all UserAddress
        /// </summary>
        /// <returns></returns>
        [Route("UserAddresses")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllUserAddresses(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var useraddresses = _useraddressService.SelectUserAddress(param).Select(x => x.ToViewModel()); ;
            	return Ok(useraddresses.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get useraddress by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("useraddress/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetUserAddress(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var useraddress = _useraddressService.SelectUserAddress(param).FirstOrDefault().ToViewModel();
	            if (useraddress.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(useraddress.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save useraddress
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("useraddress")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveUserAddress(UserAddressViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _useraddressService.UserAddressInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.USERADDRESS + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("UserAddress save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update useraddress
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("useraddress")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateUserAddress(UserAddressViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _useraddressService.UserAddressUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("UserAddress updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete useraddress by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("useraddress/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteUserAddress(string id)
        {
        	try{
	            UserAddressViewModel model = new UserAddressViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _useraddressService.UserAddressUpdate(model.ToModel());
	            return Ok("UserAddress Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.UserAddress, (int)ErrorFunctionCode.UserAddress_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.UserAddress_Delete).ToString().ErrorResponse());
            }
        }
    }
}

