using LotusLaundry.Framework.ViewModels.Language;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Language;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class LanguageController : ApiController
    {
        private ILanguageService _languageService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public LanguageController(ILanguageService languageService, IExceptionService exceptionService)
        {
            _languageService = languageService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Language
        /// </summary>
        /// <returns></returns>
        [Route("Languages")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllLanguages(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var languages = _languageService.SelectLanguage(param).Select(x => x.ToViewModel()); ;
            	return Ok(languages.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get language by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("language/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetLanguage(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var language = _languageService.SelectLanguage(param).FirstOrDefault().ToViewModel();
	            if (language.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(language.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("language")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveLanguage(LanguageViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _languageService.LanguageInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Language save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("language")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateLanguage(LanguageViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _languageService.LanguageUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Language updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete language by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("language/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteLanguage(string id)
        {
        	try{
	            LanguageViewModel model = new LanguageViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _languageService.LanguageUpdate(model.ToModel());
	            return Ok("Language Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Language, (int)ErrorFunctionCode.Language_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Language_Delete).ToString().ErrorResponse());
            }
        }
    }
}

