using LotusLaundry.Framework.ViewModels.ProductLanguageMapping;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.ProductLanguageMapping;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class ProductLanguageMappingController : ApiController
    {
        private IProductLanguageMappingService _productlanguagemappingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ProductLanguageMappingController(IProductLanguageMappingService productlanguagemappingService, IExceptionService exceptionService)
        {
            _productlanguagemappingService = productlanguagemappingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all ProductLanguageMapping
        /// </summary>
        /// <returns></returns>
        [Route("ProductLanguageMappings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllProductLanguageMappings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var productlanguagemappings = _productlanguagemappingService.SelectProductLanguageMapping(param).Select(x => x.ToViewModel()); ;
            	return Ok(productlanguagemappings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductLanguageMapping, (int)ErrorFunctionCode.ProductLanguageMapping_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductLanguageMapping_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get productlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("productlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetProductLanguageMapping(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var productlanguagemapping = _productlanguagemappingService.SelectProductLanguageMapping(param).FirstOrDefault().ToViewModel();
	            if (productlanguagemapping.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(productlanguagemapping.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductLanguageMapping, (int)ErrorFunctionCode.ProductLanguageMapping_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductLanguageMapping_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save productlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("productlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveProductLanguageMapping(ProductLanguageMappingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _productlanguagemappingService.ProductLanguageMappingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PRODUCTLANGUAGEMAPPING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("ProductLanguageMapping save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductLanguageMapping, (int)ErrorFunctionCode.ProductLanguageMapping_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductLanguageMapping_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update productlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("productlanguagemapping")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateProductLanguageMapping(ProductLanguageMappingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productlanguagemappingService.ProductLanguageMappingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("ProductLanguageMapping updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductLanguageMapping, (int)ErrorFunctionCode.ProductLanguageMapping_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductLanguageMapping_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete productlanguagemapping by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("productlanguagemapping/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteProductLanguageMapping(string id)
        {
        	try{
	            ProductLanguageMappingViewModel model = new ProductLanguageMappingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _productlanguagemappingService.ProductLanguageMappingUpdate(model.ToModel());
	            return Ok("ProductLanguageMapping Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.ProductLanguageMapping, (int)ErrorFunctionCode.ProductLanguageMapping_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.ProductLanguageMapping_Delete).ToString().ErrorResponse());
            }
        }
    }
}

