using LotusLaundry.Framework.ViewModels.Notification;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Notification;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class NotificationController : ApiController
    {
        private INotificationService _notificationService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public NotificationController(INotificationService notificationService, IExceptionService exceptionService)
        {
            _notificationService = notificationService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Notification
        /// </summary>
        /// <returns></returns>
        [Route("Notifications")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllNotifications(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var notifications = _notificationService.SelectNotification(param).Select(x => x.ToViewModel()); ;
            	return Ok(notifications.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Notification, (int)ErrorFunctionCode.Notification_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Notification_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get notification by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notification/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetNotification(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var notification = _notificationService.SelectNotification(param).FirstOrDefault().ToViewModel();
	            if (notification.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(notification.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Notification, (int)ErrorFunctionCode.Notification_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Notification_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save notification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notification")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveNotification(NotificationViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _notificationService.NotificationInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.NOTIFICATION + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Notification save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Notification, (int)ErrorFunctionCode.Notification_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Notification_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update notification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("notification")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateNotification(NotificationViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationService.NotificationUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Notification updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Notification, (int)ErrorFunctionCode.Notification_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Notification_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete notification by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("notification/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteNotification(string id)
        {
        	try{
	            NotificationViewModel model = new NotificationViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _notificationService.NotificationUpdate(model.ToModel());
	            return Ok("Notification Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Notification, (int)ErrorFunctionCode.Notification_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Notification_Delete).ToString().ErrorResponse());
            }
        }
    }
}

