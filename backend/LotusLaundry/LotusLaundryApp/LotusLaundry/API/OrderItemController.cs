using LotusLaundry.Framework.ViewModels.OrderItem;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.OrderItem;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class OrderItemController : ApiController
    {
        private IOrderItemService _orderitemService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public OrderItemController(IOrderItemService orderitemService, IExceptionService exceptionService)
        {
            _orderitemService = orderitemService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all OrderItem
        /// </summary>
        /// <returns></returns>
        [Route("OrderItems")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllOrderItems(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var orderitems = _orderitemService.SelectOrderItem(param).Select(x => x.ToViewModel()); ;
            	return Ok(orderitems.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItem, (int)ErrorFunctionCode.OrderItem_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItem_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get orderitem by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderitem/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetOrderItem(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var orderitem = _orderitemService.SelectOrderItem(param).FirstOrDefault().ToViewModel();
	            if (orderitem.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(orderitem.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItem, (int)ErrorFunctionCode.OrderItem_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItem_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save orderitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderitem")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SaveOrderItem(OrderItemViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _orderitemService.OrderItemInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.ORDERITEM + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("OrderItem save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItem, (int)ErrorFunctionCode.OrderItem_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItem_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update orderitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("orderitem")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdateOrderItem(OrderItemViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderitemService.OrderItemUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("OrderItem updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItem, (int)ErrorFunctionCode.OrderItem_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItem_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete orderitem by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("orderitem/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeleteOrderItem(string id)
        {
        	try{
	            OrderItemViewModel model = new OrderItemViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _orderitemService.OrderItemUpdate(model.ToModel());
	            return Ok("OrderItem Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.OrderItem, (int)ErrorFunctionCode.OrderItem_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.OrderItem_Delete).ToString().ErrorResponse());
            }
        }
    }
}

