using LotusLaundry.Framework.ViewModels.Packing;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Enums;
using LotusLaundry.Domain;
using LotusLaundry.Framework.CustomFilters;
using LotusLaundry.Framework.GenericResponse;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Service.Packing;
using LotusLaundry.Service.Exception;
using LotusLaundry.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LotusLaundry.Common.Enums; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.API
{
    [RoutePrefix("api")]
    public class PackingController : ApiController
    {
        private IPackingService _packingService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public PackingController(IPackingService packingService, IExceptionService exceptionService)
        {
            _packingService = packingService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Packing
        /// </summary>
        /// <returns></returns>
        [Route("Packings")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult GetAllPackings(SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var packings = _packingService.SelectPacking(param).Select(x => x.ToViewModel()); ;
            	return Ok(packings.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Packing, (int)ErrorFunctionCode.Packing_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Packing_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get packing by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("packing/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetPacking(string id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;	            
	            var packing = _packingService.SelectPacking(param).FirstOrDefault().ToViewModel();
	            if (packing.Id == null){
                	return Ok("No record found".ErrorResponse());    
                }
                return Ok(packing.SuccessResponse());
                
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Packing, (int)ErrorFunctionCode.Packing_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Packing_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save packing
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("packing")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPost]
        [ValidateModelState]
        public IHttpActionResult SavePacking(PackingViewModel model)
        {
        	try
            {
            	//set tenantid
            	model.TenantId = ManageClaims.GetUserClaim().TenantId;
            	
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _packingService.PackingInsert(model.ToModel());
	            
	            return Ok(CryptoEngine.Encrypt(FileType.PACKING + "_" + responseId.ToString(), KeyConstant.Key).SuccessResponse("Packing save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Packing, (int)ErrorFunctionCode.Packing_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Packing_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update packing
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("packing")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpPut]
        [ValidateModelState]
        public IHttpActionResult UpdatePacking(PackingViewModel model)
        {
        	try{
        		model.TenantId = ManageClaims.GetUserClaim().TenantId;
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _packingService.PackingUpdate(model.ToModel());
	            return Ok(model.Id.SuccessResponse("Packing updated successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Packing, (int)ErrorFunctionCode.Packing_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Packing_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete packing by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("packing/{id}")]
        [CustomAuthorize("ROLE_ADMIN")]
        [HttpDelete]
        public IHttpActionResult DeletePacking(string id)
        {
        	try{
	            PackingViewModel model = new PackingViewModel();
	            model.TenantId = ManageClaims.GetUserClaim().TenantId;
	            model.Id = id;
	            model.IsDeleted = true;
	            model.UpdatedBy = User.Identity.GetUserId<long>();
	            _packingService.PackingUpdate(model.ToModel());
	            return Ok("Packing Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Packing, (int)ErrorFunctionCode.Packing_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Packing_Delete).ToString().ErrorResponse());
            }
        }
    }
}

