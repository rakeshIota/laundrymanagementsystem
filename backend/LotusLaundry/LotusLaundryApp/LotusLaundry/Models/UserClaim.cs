﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusLaundry.Models
{
    public class UserClaim
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }
}
