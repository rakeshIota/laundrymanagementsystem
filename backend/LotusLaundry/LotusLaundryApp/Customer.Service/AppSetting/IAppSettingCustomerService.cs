﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.AppSetting
{
    public interface IAppSettingCustomerService
    {
        List<AppSettingModel> AppSettingSelect(SearchParam param);

        ApplicationSettingModel ApplicationSettingSelect(SearchParam param);
    }
}
