﻿using Customer.DBRepository.AppSetting;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.AppSetting
{
    public class AppSettingCustomerService : IAppSettingCustomerService
    {

        public AppSettingCustomerDBService _appSettingCustomerDBService;
        public AppSettingCustomerService()
        {
            _appSettingCustomerDBService = new AppSettingCustomerDBService();
        }


        public List<AppSettingModel> AppSettingSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            var response = _appSettingCustomerDBService.AppSettingSelect(param);
            return response;
        }

        //ApplicationSettingSelect

        public ApplicationSettingModel ApplicationSettingSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            var response = _appSettingCustomerDBService.AppSettingSelect(param);
            ApplicationSettingModel appset = new ApplicationSettingModel();
            response.ForEach(x =>
            {
                if(x.Key== "SAMEDAY_DELIVERY_RATE")
                {
                    appset.SAMEDAY_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "EXPRESS_DELIVERY_RATE")
                {
                    appset.EXPRESS_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "NORMAL_DELIVERY_RATE")
                {
                    appset.NORMAL_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "GoogleKeyRefresh")
                {
                    appset.GoogleKeyRefresh = x.Value;
                }
                if (x.Key == "timeSlot")
                {
                    appset.timeSlot = x.Value;
                }
                //=========  

                if (x.Key == "deliveryStartTime")
                {
                    appset.deliveryStartTime = x.Value;
                }
                if (x.Key == "expressEndTime")
                {
                    appset.expressEndTime = x.Value;
                }
                if (x.Key == "normalEndTime")
                {
                    appset.normalEndTime = x.Value;
                }
                if (x.Key == "sameDayEndTime")
                {
                    appset.sameDayEndTime = x.Value;
                }


                if (x.Key == "minimumOrderAmount")
                {
                    appset.minimumOrderAmount = x.Value;
                }
                if (x.Key == "logisticCharge")
                {
                    appset.logisticCharge = x.Value;
                }
                if (x.Key == "availablePaymentTypes")
                {
                    appset.availablePaymentTypes = x.Value;
                }
                if (x.Key == "driverLocationInterval")
                {
                    appset.driverLocationInterval = x.Value;
                }
                if (x.Key == "driverOrderInterval")
                {
                    appset.driverOrderInterval = x.Value;
                }
                //



            });

            
            


            return appset;
        }
    }
}
