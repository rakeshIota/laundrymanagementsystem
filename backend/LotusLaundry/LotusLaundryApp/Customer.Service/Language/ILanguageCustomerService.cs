﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Language
{
    public interface ILanguageCustomerService
    {
        List<LanguageModel> SelectLanguage(SearchParam param);
    }
}
