﻿using Customer.DBRepository.Language;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Language;
using LotusLaundry.Service.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Language
{
   public class LanguageCustomerService: ILanguageCustomerService
    {
        public LanguageCustomerDBService _languageCustomerDBService;
        private IXmlService _xmlService;
        public LanguageCustomerService(IXmlService xmlService)
        {
            _languageCustomerDBService = new LanguageCustomerDBService();
            _xmlService = xmlService;
        }

        public List<LanguageModel> SelectLanguage(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            var response = _languageCustomerDBService.LanguageSelect(param);
            return response;
        }
    }
}
