﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Users;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Service.Xml;
using System.Configuration;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.Extensions;

namespace Customer.Service.Customer
{
    public class CustomerService : ICustomerService
    {
        
        private readonly string _imageOption;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;

        public CustomerService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            
            // _usersDBService = new UsersDBService();
          
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _fileGroupService = fileGroupService;
        }
        public string ProfilePicInsert(UsersModel model)
        {
            long id = Convert.ToInt64(model.Id);
           
            if (model.File != null)
            {
                //set target path and move file from target location
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //set target path and move file from target location
                   
                    //model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, id, FileType.PROFILEPIC); 
                }
                model.File.Type = FileType.PROFILEPIC;
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, id, model.File.XmlSerialize());
            }
            return model.File.Path;
        }

    }
}
