﻿using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using Customer.DBRepository.UserAddress;

namespace Customer.Service.UserAddress
{
    public class UserAddressService : IUserAddressService
    {	
        public UserAddressDBService _useraddressDBService;
        
        public UserAddressService(IFileGroupService fileGroupService)
        {
            _useraddressDBService = new UserAddressDBService();
        }
        
        public long UserAddressInsert(UserAddressModel model)
        {
            var id = _useraddressDBService.UserAddressInsert(model);
            return id;
        }
        


        public void UserAddressUpdate(UserAddressModel model)
        {
            _useraddressDBService.UserAddressUpdate(model);
        }

        public List<UserAddressModel> SelectUserAddress(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _useraddressDBService.SelectUserAddress(param);
            return response;
        }
        // List<UserAddressModel> SelectAddressInActivePostalCode(SearchParam param);



        //SelectCustomerAddress
        public List<UserAddressModel> SelectCustomerAddress(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.UserId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _useraddressDBService.SelectUserAddress(param);
            return response;
        }


        public List<UserAddressModel> SelectAddressInActivePostalCode(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _useraddressDBService.SelectAddressInActivePostalCode(param);
            return response;
        }


        //DeliveryAddressInsert
        public long DeliveryAddressInsert(UserAddressModel model,Int32 Orderid)
        {
            var id = _useraddressDBService.DeliveryAddressInsert(model, Orderid);
            return id;
        }

    }
}
