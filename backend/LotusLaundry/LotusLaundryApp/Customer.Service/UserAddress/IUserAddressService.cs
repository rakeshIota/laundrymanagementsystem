﻿using LotusLaundry.Domain.UserAddress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace Customer.Service.UserAddress
{
    public interface IUserAddressService
    {
        /// <summary>
        /// used for insertion useraddress
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long UserAddressInsert(UserAddressModel model);
        
        /// <summary>
        /// Method for update useraddress
        /// </summary>
        /// <param name="model"></param>
        void UserAddressUpdate(UserAddressModel model);

        /// <summary>
        /// use to select all useraddress or select useraddress by id 
        /// </summary>
        /// <param name="useraddressId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<UserAddressModel> SelectUserAddress(SearchParam param);

        List<UserAddressModel> SelectCustomerAddress(SearchParam param);

        List<UserAddressModel> SelectAddressInActivePostalCode(SearchParam param);


        long DeliveryAddressInsert(UserAddressModel model,Int32 orderid);
    }
}
