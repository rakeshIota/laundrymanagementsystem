﻿using LotusLaundry.DBRepository.UserTwilioCalling;
using LotusLaundry.Domain.UserTwilioCalling;
using LotusLaundry.Service.UserTwilioCalling;

namespace CarersCouch.Service.UserTwilioCalling
{
    public class UserCallingService : IUserCallingService
    {
        public UserTwilioCallingDBService _userCallingDbService;

        public UserCallingService()
        {
            _userCallingDbService = new UserTwilioCallingDBService();
        }

        /// <summary>
        ///  user calling status update
        /// </summary>
        /// <param name="model"></param>
        public void UserCallingStatusUpdate(UserTwilioCallingModel model)
        {
            _userCallingDbService.UserCallingStatusUpdate(model);
        }

        /// <summary>
        /// check user active status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool? CheckUserCallingStatus(UserTwilioCallingModel model)
        {
            return _userCallingDbService.CheckUserCallingStatus(model);
        }
        //CheckUserCallingDeviceStatus

        public bool? CheckUserCallingDeviceStatus(UserTwilioCallingModel model)
        {
            return _userCallingDbService.CheckUserCallingDeviceStatus(model);
        }
        public void UserCallingStatusDeactivate(long? UserId)
        {
            _userCallingDbService.UserCallingStatusDeactivate(UserId);
        }
    }
}
