﻿using LotusLaundry.Domain.UserTwilioCalling;

namespace LotusLaundry.Service.UserTwilioCalling
{
    public interface IUserCallingService
    {
        void UserCallingStatusUpdate(UserTwilioCallingModel model);
        bool? CheckUserCallingStatus(UserTwilioCallingModel model);

        bool? CheckUserCallingDeviceStatus(UserTwilioCallingModel model);

        void UserCallingStatusDeactivate(long? UserId);
    }
}
