﻿using LotusLaundry.Domain.CartItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace Customer.Service.CartItem
{
    public interface ICartItemService
    {
        /// <summary>
        /// used for insertion cartitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CartItemInsert(CartItemModel model);
        
        /// <summary>
        /// Method for update cartitem
        /// </summary>
        /// <param name="model"></param>
        void CartItemUpdate(CartItemModel model);

        /// <summary>
        /// use to select all cartitem or select cartitem by id 
        /// </summary>
        /// <param name="cartitemId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CartItemModel> SelectCartItem(SearchParam param);

        void CartItemUpdateByService(CartItemModel model);
    }
}
