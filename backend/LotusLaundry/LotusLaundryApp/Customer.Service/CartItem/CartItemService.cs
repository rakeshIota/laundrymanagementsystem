﻿using LotusLaundry.Domain.CartItem;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using Customer.DBRepository.CartItem;

namespace Customer.Service.CartItem
{
    public class CartItemService : ICartItemService
    {	
        public CartItemDBService _cartitemDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CartItemService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _cartitemDBService = new CartItemDBService();
            _fileGroupService = fileGroupService;
            _xmlService = xmlService;
            
        }
        
        public long CartItemInsert(CartItemModel model)
        {
            var id = _cartitemDBService.CartItemInsert(model);
            return id;
        }

        public void CartItemUpdate(CartItemModel model)
        {
            _cartitemDBService.CartItemUpdate(model);
        }
        // void CartItemUpdateByService(CartItemModel model);
        public void CartItemUpdateByService(CartItemModel model)
        {
            _cartitemDBService.CartItemUpdateByService(model);
        }
        public List<CartItemModel> SelectCartItem(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _cartitemDBService.SelectCartItem(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
