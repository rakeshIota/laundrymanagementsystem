﻿using LotusLaundry.Domain.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.CartItem;

namespace Customer.Service.Cart
{
    public interface ICartService
    {
        /// <summary>
        /// used for insertion cart
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CartInsert(CartModel model);
        
        /// <summary>
        /// Method for update cart
        /// </summary>
        /// <param name="model"></param>
        void CartUpdate(CartModel model);

        /// <summary>
        /// use to select all cart or select cart by id 
        /// </summary>
        /// <param name="cartId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CartModel> SelectCart(SearchParam param);
        ResponseModel CartItemInsert(List<CartItemModel> model, int? TenantId, long userId);
        ResponseModel CartItemUpdate(string service, string product, int? TenantId, long userId, int? quantity);
        ResponseModel CartItemDelete(string id, int? TenantId, long userId, int? quantity);
        ResponseModel CartDeliveryTypeUpdate(string deliveryType, int? TenantId, long userId);
        ResponseModel GetCartItemQuantity(int? TenantId, long userId);

        void CartWithItemDelete(CartModel model);


        

            ResponseModel CartDeliveryDateAndTimeeUpdate( int? TenantId, long? userId, DateTimeOffset? DeliveryDate, string deliverySlot = null);

            ResponseModel CartPickupDateAndTimeeUpdate( int? TenantId, long? userId, DateTimeOffset? PickUpdate,  DateTimeOffset? DeliveryDate, string PickupSlot = null, string deliverySlot = null,string laundryInstruction=null);
    }
}
