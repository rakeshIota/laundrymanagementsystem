﻿using LotusLaundry.Domain.Cart;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using Customer.DBRepository.Cart;
using LotusLaundry.Domain.CartItem;
using Customer.DBRepository.Product;
using Customer.DBRepository.CartItem;
using LotusLaundry.Common.Enums;

namespace Customer.Service.Cart
{
    public class CartService : ICartService
    {
        public CartDBService _cartDBService;
        public CartItemDBService _cartItemDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        public ProductCustomerDBService _productCustomerDBService;

        public CartService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _cartDBService = new CartDBService();
            _fileGroupService = fileGroupService;
            _xmlService = xmlService;
            _productCustomerDBService = new ProductCustomerDBService();
            _cartItemDBService = new CartItemDBService();

        }

        public long CartInsert(CartModel model)
        {
            var id = _cartDBService.CartInsert(model);
            return id;
        }

        public ResponseModel CartItemInsert(List<CartItemModel> model, int? TenantId, long userId)
        {
            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;
            
            ///check cart is created
            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null)
            {
                cart = CreateUserCart(userId, TenantId,null,null,null,null);
            }
            else {
                cart.CartItems = _xmlService.GetCartItemsByXml(cart.CartItmeXml);
            }
            cart.TotalItems = 0;
            List<CartItemModel> cartItems = new List<CartItemModel>();
            cartItems = cart.CartItems != null ? cart.CartItems : cartItems;
            cart.CartItems = new List<CartItemModel>();
            foreach (var item in model)
            {
                //update new cart Item
                var checkAlreadyInCart = cartItems.Where(x => x.ProductId == item.ProductId && x.ServiceId == item.ServiceId).FirstOrDefault();
                if (checkAlreadyInCart != null)
                {
                    item.Id = checkAlreadyInCart.Id;
                    item.Quantity += checkAlreadyInCart.Quantity;
                    //item.Price = productPrice.Price;
                    //item.TotalPrice = productPrice.Price * item.Quantity;
                }
                cart.TotalItems += item.Quantity;
                item.Price = 0;
                item.TotalPrice = 0;
                item.CartId = cart.Id;
                item.TenantId = TenantId;
                cart.CartItems.Add(item);

            }
            _cartItemDBService.CartItemXMLSaveApp(cart.CartItems.XmlSerialize());
            _cartDBService.CartUpdate(cart);
            return response;
        }



        public ResponseModel CartItemUpdate(string service, string product, int? TenantId, long userId, int? quantity)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }
            var productId = Convert.ToInt64(CryptoEngine.Decrypt(product, KeyConstant.Key));
            var serviceId = Convert.ToInt64(CryptoEngine.Decrypt(service, KeyConstant.Key));
            cart.CartItems = _xmlService.GetCartItemsByXml(cart.CartItmeXml);
            var checkAlreadyInCart = cart.CartItems.Where(x => x.ProductId == productId && x.ServiceId == x.ServiceId).FirstOrDefault();

            if (checkAlreadyInCart == null)
            {
                response.Status = false;
                response.Message = "Product cannot find in cart";
                return response;
            }
            int? totalItems = 0;
            cart.CartItems.ForEach(x =>
            {
                if (x.Id == checkAlreadyInCart.Id)
                {
                    x.Quantity = quantity;
                    x.IsDeleted = x.Quantity == 0 ? true : x.IsDeleted;
                }
                totalItems += x.Quantity;
            });
            cart.TotalItems = quantity;
            _cartItemDBService.CartItemXMLSaveApp(cart.CartItems.XmlSerialize());
            _cartDBService.CartUpdate(cart);

            return response;
        }




        public ResponseModel CartItemDelete(string id, int? TenantId, long userId, int? quantity)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }
            var cartitemId = Convert.ToInt64(CryptoEngine.Decrypt(id, KeyConstant.Key));
            cart.CartItems = _xmlService.GetCartItemsByXml(cart.CartItmeXml);
            var checkAlreadyInCart = cart.CartItems.Where(x => x.Id == cartitemId).FirstOrDefault();

            if (checkAlreadyInCart == null)
            {
                response.Status = false;
                response.Message = "Product cannot find in cart";
                return response;
            }
            int? totalItems = 0;
            cart.CartItems.ForEach(x =>
            {
                if (x.Id == checkAlreadyInCart.Id)
                {
                    x.Quantity = quantity;
                    x.IsDeleted = x.Quantity == 0 ? true : x.IsDeleted;
                }
                totalItems += x.Quantity;
            });
            cart.TotalItems = quantity;
            _cartItemDBService.CartItemXMLSaveApp(cart.CartItems.XmlSerialize());
            _cartDBService.CartUpdate(cart);

            return response;
        }
        public void CartUpdate(CartModel model)
        {
            _cartDBService.CartUpdate(model);
        }


        public void CartWithItemDelete(CartModel model)
        {
            _cartDBService.CartWithItemDelete(model);
        }

        //public void UpdateCartItem(CartItemModel newRequest ,CartItemModel oldRequest)
        //{

        //}



        //public CartItemModel InsertCartItem(CartItemModel model)
        //{
        //    return model;
        //}


        public CartModel CreateUserCart(long? userId, int? tenantId, DateTimeOffset? DeliveryDate, DateTimeOffset? PickupDate,string pickupSlot=null,string deliverySlot=null)
        {
            CartModel cart = new CartModel();
            cart.CreatedBy = cart.UpdatedBy = cart.CustomerId = userId;
            cart.TenantId = tenantId;
            cart.IsActive = true;
            cart.IsLocked = false;
            cart.DeliveryType = DeliveryType.NORMAL.ToString();
            cart.Tax = cart.SubTotal = cart.TotalItems = 0;
            cart.TotalPrice = 0;
            cart.DeliveryDate = DeliveryDate;
            cart.PickupDate = PickupDate;
            cart.pickupSlot = pickupSlot;
            cart.deliverySlot = deliverySlot;


            var id = _cartDBService.CartInsert(cart);
            SearchParam param = new SearchParam();
            param.RecordId = id;
            return _cartDBService.SelectCart(param).FirstOrDefault();
        }

        public List<CartModel> SelectCart(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }

            var response = _cartDBService.SelectCart(param);
            response.ForEach(x =>
            {
                x.CartItems = _xmlService.GetCartItemsByXml(x.CartItmeXml);
            });
            return response;
        }


        public ResponseModel CartDeliveryTypeUpdate(string deliveryType, int? TenantId, long userId)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }
            cart.DeliveryType = deliveryType;
            _cartDBService.CartUpdate(cart);

            return response;
        }

        public ResponseModel GetCartItemQuantity(int? TenantId, long userId)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }

            cart.CartItems = _xmlService.GetCartItemsByXml(cart.CartItmeXml);

            int itemCount = 0;
            foreach (var item in cart.CartItems)
            {
                itemCount = itemCount + item.Quantity.GetValueOrDefault();
            }



            response.Message = cart.CartItems != null ? itemCount.ToString() : "0";
            return response;
        }

        


        public ResponseModel CartDeliveryDateAndTimeeUpdate(int? TenantId, long? userId, DateTimeOffset? DeliveryDate, string deliverySlot = null)
        {
            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }
            cart.DeliveryDate = DeliveryDate;
            cart.deliverySlot = deliverySlot;

            _cartDBService.CartUpdate(cart);

            return response;
        }

        ResponseModel ICartService.CartPickupDateAndTimeeUpdate(int? TenantId, long? userId, DateTimeOffset? PickUpdate,  DateTimeOffset? DeliveryDate, string PickupSlot, string deliverySlot = null,string laundryInstruction=null)
        {
            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var cart = _cartDBService.SelectCart(param).FirstOrDefault();
            if (cart == null || cart.Id == null)
            {
                response.Status = false;
                response.Message = "No cart found";
                return response;
            }
            cart.laundryInstruction = laundryInstruction;
            cart.PickupDate = PickUpdate;
            cart.pickupSlot = PickupSlot;
            cart.deliverySlot = deliverySlot;
            cart.DeliveryDate = DeliveryDate;
            _cartDBService.CartUpdate(cart);

            return response;
        }
    }
}
