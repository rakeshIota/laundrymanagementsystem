﻿using Customer.DBRepository.Service;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Service;
using LotusLaundry.Service.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Service
{
    public class ServiceCustomerService : IServiceCustomerService
    {
        public ServiceCustomerDBService _serviceCustomerDBService;
        private IXmlService _xmlService;
        public ServiceCustomerService(IXmlService xmlService)
        {
            _serviceCustomerDBService = new ServiceCustomerDBService();
            _xmlService = xmlService;
        }


        public List<ServiceModel> ServiceSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            var response = _serviceCustomerDBService.ServiceSelect(param);
            response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
            });
            return response;
        }
    }
}
