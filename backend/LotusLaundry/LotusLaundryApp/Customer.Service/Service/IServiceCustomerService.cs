﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Service
{
    public interface IServiceCustomerService
    {
        List<ServiceModel> ServiceSelect(SearchParam param);
    }
}
