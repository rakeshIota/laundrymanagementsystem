﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.SubDistrict
{
   public interface ISubDistrictCustomerService
    {
        List<SubDistrictModel> SubDistrictSelect(SearchParam param);
    }
}
