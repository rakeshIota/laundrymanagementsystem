﻿using Customer.DBRepository.SubDistrict;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.SubDistrict
{
    public class SubDistrictCustomerService : ISubDistrictCustomerService
    {

        public SubDistrictCustomerDBService _subDistrictCustomerDBService;

        public SubDistrictCustomerService()
        {
            _subDistrictCustomerDBService = new SubDistrictCustomerDBService();
        }


        public List<SubDistrictModel> SubDistrictSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            var response = _subDistrictCustomerDBService.SubDistrictSelect(param);
            return response;
        }
    }
}
