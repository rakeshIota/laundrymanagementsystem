﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Product
{
   public interface IProductCustomerService
    {
        List<ProductModel> ProductSelect(SearchParam param);
        List<ProductModel> ServiceProductSelect(SearchParam param);
    }
}
