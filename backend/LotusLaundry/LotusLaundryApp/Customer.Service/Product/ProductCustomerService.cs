﻿using Customer.DBRepository.Product;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Product;
using LotusLaundry.Service.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Product
{
    public class ProductCustomerService : IProductCustomerService
    {

        public ProductCustomerDBService _productCustomerDBService;
        private IXmlService _xmlService;
        public ProductCustomerService(IXmlService xmlService)
        {
            _productCustomerDBService = new ProductCustomerDBService();
            _xmlService = xmlService;
        }


        public List<ProductModel> ProductSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            if (param.ServiceIdStr != null)
            {
                param.ServiceId = Convert.ToInt64(CryptoEngine.Decrypt(param.ServiceIdStr, KeyConstant.Key));
            }
            var response = _productCustomerDBService.ProductSelect(param);
            response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
            });
            return response;
        }

        public List<ProductModel> ServiceProductSelect(SearchParam param)
        {
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            if (param.ServiceIdStr != null)
            {
                param.ServiceId = Convert.ToInt64(CryptoEngine.Decrypt(param.ServiceIdStr, KeyConstant.Key));
            }
            var response = _productCustomerDBService.ServiceProductSelect(param);
            response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
            });
            return response;
        }

        public List<ProductModel> GetProdcutPrice(SearchParam param,long ProductId)
        {
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            if (param.ServiceIdStr != null)
            {
                param.ServiceId = Convert.ToInt64(CryptoEngine.Decrypt(param.ServiceIdStr, KeyConstant.Key));
            }
            var response = _productCustomerDBService.GetProductPrice(param, ProductId);
            response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
            });
            return response;
        }


    }
}
