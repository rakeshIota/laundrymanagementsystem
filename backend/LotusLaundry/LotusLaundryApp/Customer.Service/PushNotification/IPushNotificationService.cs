﻿using Customer.DBRepository;
using Customer.DBRepository.PushNotification;
using LotusLaundry.DBRepository;
using LotusLaundry.Domain;
using LotusLaundry.Domain.PushNotification;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Customer.Service.PushNotification
{
    public interface IPushNotificationService
    {
        Task SendNotificationByFCM(FCMNotificationModel model);
        long? PushNotificationInsert(PushNotificationModel model);
        Task PushNotificationInsertAsync(PushNotificationModel model);
        void PushNotificationRead(PushNotificationModel model);
        List<PushNotificationModel> PushNotificationSelect(SearchParam param); 
        PushNotificationSelectForUser_Result PushNotificationSelectForUser(long? userId);
        long? PushNotificationLogInsert(PushNotificationModel model);
        void PushNotificationChatAsync(PushNotificationModel model);
      //  List<PushNotificationModel> GetNotificationLogForAppDashboard(long? userId);
        void SendNotificationToActiveUsers(PushNotificationModel model);

        Task SendNotificationByFCMToUser(string TargetId, long userid, string Type, string title, string message);
        Task TestSendNotificationByFCMToUser(string token,string TargetId, long userid, string Type, string title, string message);

        //TestSendNotificationByFCMToUser

        Task BroadcastNotificationByPOSTALCODE(string customerType, string postalCodes, string customerGender, string customerOrderType, int orderCount, string message);

        Task BroadcastNotificationByToGroup(string customerType, List<string> postalCodes,string customerGender,string customerOrderType, int  orderCount,string message);

        List<Broadcastmodel> BroadcastMessageSelect(SearchParam param);
    }
}
