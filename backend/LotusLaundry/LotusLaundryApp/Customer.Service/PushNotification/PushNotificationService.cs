﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using Customer.DBRepository.PushNotification;
using LotusLaundry.DBRepository;

using Customer.DBRepository.PushNotification;
using LotusLaundry.Domain.PushNotification;
using Customer.Service.PushNotification;
using LotusLaundry.Core.PushNotification;
using Customer.DBRepository;
using LotusLaundry.Service.Users;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Success;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using System.Net.Http.Headers;
using LotusLaundry.Domain.Users;
using System.Linq;
using System;
using System.Web.Script.Serialization;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Service.Notification
{
    public class PushNotificationService : IPushNotificationService
    {

        PushNotificationDBService _pushnotificationDBService;
        UsersService _usersService;
        IXmlService _xmlService;
        IFileGroupService fileGroupService;
        public PushNotificationService(IXmlService xmlService, IFileGroupService fileGroupService)
        {
            _pushnotificationDBService = new PushNotificationDBService();
            _usersService = new UsersService(xmlService, fileGroupService);
        }

        /* send notification via FCM */
        public async Task SendNotificationByFCM(FCMNotificationModel model)
        {
         
            await Task.Run(() => PushNotificationManager.SendNotificationAsync(model.DeviceToken, model.Message, model.Title, model.TargetId, model.Type));

            // PushNotificationModel model1 = new PushNotificationModel();
            //model1.Message = model.Message;
            //model1.UserId = model.TargetId;
            //_pushnotificationDBService.PushNotificationInsert(model1);
        }

        //Task SendNotificationByFCMToUser( string orderid,List<string> device);
        public async Task SendNotificationByFCMToUser(string TargetId, long  userid,string Type,string title,string message)
        {

           
            
                var device = _usersService.UserDeviceSelect(userid);
                if(device.Count>0)
                { 
                List<string> deviceToken = new List<string>();
                foreach (var item in device)
                {
                    string token = item.NotificationToken;
                    deviceToken.Add(token);
                }
                FCMNotificationModel nm = new FCMNotificationModel();
                nm.TargetId = TargetId;
                nm.Type = Type;
                nm.Title = title;
                nm.Message = message;


                    nm.DeviceToken = deviceToken;
                    SendNotificationByFCM(nm);
                }

            
        }

        public async Task TestSendNotificationByFCMToUser(string token,string TargetId, long userid, string Type, string title, string message)
        {



            //var device = _usersService.UserDeviceSelect(userid);
            //if (device.Count > 0)
            {
                List<string> deviceToken = new List<string>();
                
                deviceToken.Add(token);
                FCMNotificationModel nm = new FCMNotificationModel();
                nm.TargetId = TargetId;
                nm.Type = Type;
                nm.Title = title;
                nm.Message = message;


                nm.DeviceToken = deviceToken;
                SendNotificationByFCM(nm);
            }


        }

        /* notification log */
        public long? PushNotificationInsert(PushNotificationModel model)
        {
            var userDevices = PushNotificationSelectForUser(model.UserId);
            //if(userDevices == null) 
            Task.Run(() => SendNotificationByFCM(new FCMNotificationModel
            {
                DeviceToken = userDevices != null ? userDevices.DeviceList : null,
                TargetId = model.TargetId,
                Message = model.Message,
                Title = model.Title,
                Type = model.Type,
            }));
            
            
            return _pushnotificationDBService.PushNotificationInsert(model);
        }

        public void PushNotificationRead(PushNotificationModel model)
        {
            _pushnotificationDBService.PushNotificationRead(model);
        }

        public List<PushNotificationModel> PushNotificationSelect(SearchParam param)
        {
            return _pushnotificationDBService.PushNotificationSelect(param);
        }
        public List<Broadcastmodel> BroadcastMessageSelect(SearchParam param)
        {
            return _pushnotificationDBService.BroadcastSelect(param);
        }

        

        public PushNotificationSelectForUser_Result PushNotificationSelectForUser(long? userId)
        {
            return _pushnotificationDBService.PushNotificationSelectForUser(userId ?? 0);
        }

        public PushNotificationSelectForOtherActiveUser_Result PushNotificationSelectForOtherActiveUser(long? userId, string deviceId)
        {
            return _pushnotificationDBService.NotificationSelectForOtherActiveUser(userId, deviceId);
        }

        public long? PushNotificationLogInsert(PushNotificationModel model)
        {
            return _pushnotificationDBService.PushNotificationInsert(model);
        }

        public Task PushNotificationInsertAsync(PushNotificationModel model)
        {
            var userDevices = PushNotificationSelectForUser(model.UserId);

            Task.Run(() => SendNotificationByFCM(new FCMNotificationModel
            {
                DeviceToken = userDevices != null ? userDevices.DeviceList : null,
                TargetId = model.TargetId,
                Message = model.Message,
                Title = model.Title,
                Type = model.TargetType,
            }));

            return Task.Run(() => _pushnotificationDBService.PushNotificationInsert(model));
        }

        public void SendNotificationToActiveUsers(PushNotificationModel model)
        {
            var userDevices = PushNotificationSelectForOtherActiveUser(model.UserId, model.DeviceId);

            Task.Run(() => SendNotificationByFCM(new FCMNotificationModel
            {
                DeviceToken = userDevices != null ? userDevices.OtherDeviceList : null,
                TargetId = model.TargetId,
                Message = model.Message,
                Title = model.Title,
                Type = model.TargetType,
            }));
        }

        public void PushNotificationChatAsync(PushNotificationModel model)
        {
            var userDevices = PushNotificationSelectForUser(model.UserId);

            Task.Run(() => SendNotificationByFCM(new FCMNotificationModel
            {
                DeviceToken = userDevices != null ? userDevices.DeviceList : null,
                TargetId = model.TargetId,
                Message = model.Message,
                Title = model.Title,
                Type = model.TargetType,
            }));

            Task.Run(() => _pushnotificationDBService.PushNotificationInsert(model));
        }

        /// <summary>
        /// get notification log for app dashboard
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //public List<PushNotificationModel> GetNotificationLogForAppDashboard(long? userId)
        //{
        //    return _pushnotificationDBService.GetNotificationLogForAppDashboard(userId);
        //}

        public async Task BroadcastNotificationByToGroup_Deleted(string customerType, List<string> postalCodes, string customerGender, string customerOrderType, int orderCount, string message)
        {

            Broadcastmodel model = new Broadcastmodel();
            string postalcode="";
            foreach(var item in postalCodes)
            {
                postalcode = postalcode + "," + item;
            }
            BroadcastParam param = new BroadcastParam();
            param.customerType = customerType;
            param.customerGender = customerGender;
            param.customerOrderType = customerOrderType;
            param.orderCount = orderCount;

            List<UsersGroupModel> usermodel = new List<UsersGroupModel>();

            usermodel = _pushnotificationDBService.BroadcastUsersSelect(param);

            int TotalUsers = 0;
            if(usermodel!=null)
            {
                if(usermodel.Count>0)
                {
                    TotalUsers = usermodel.FirstOrDefault().TotalCount.GetValueOrDefault();
                }
            }         

            model.message = message;
            model.TenantId = 1;
            model.CreatedBy = 1;
            model.Slug = null;
            model.IsActive = true;
            model.TotalUsers = TotalUsers;
            if(TotalUsers<100)
            {
                model.lastBatch = 1;
                model.PendingUsers = 0;
                model.next = 1;
                model.offset = 100;

            }
            else
            {
                
            }

            _pushnotificationDBService.BroadCastNotificationInsert(model);




            //-- table insert process  msg1 10000    TODO 
            //-- loop with bacth count 
            //{  
            int a = TotalUsers / 100;

            for (int i = 0; i < a; i++)
            {
                param.offset = 100;
                param.next = i + 1;

                usermodel = _pushnotificationDBService.BroadcastUsersSelect(param);

                List<string> deviceToken = new List<string>();
                foreach (var item in usermodel)
                {
                    string token = item.DeiviceToken;
                    deviceToken.Add(token);
                    FCMNotificationModel nm = new FCMNotificationModel();
                    nm.TargetId = item.Id.ToString();
                    nm.Type = "BroadCastMsg";
                    nm.Title = "Important Message";
                    nm.Message = message;


                    nm.DeviceToken = deviceToken;
                    SendNotificationByFCM(nm);


                }
                
            }
            //
            //}
        }

        //public async Task SendNotificationByGroup(FCMNotificationModel model)
        //{

        //    await Task.Run(() => PushNotificationManager.SendNotificationAsync(model.DeviceToken, model.Message, model.Title, model.TargetId, model.Type));

        //     PushNotificationModel model1 = new PushNotificationModel();
        //    model1.Message = model.Message;
        //    model1.UserId = Convert.ToInt32( model.TargetId);
        //    model1.Title = model.Title;
        //    model1.
        //    _pushnotificationDBService.PushNotificationInsert(model1);
        //}


        public async Task SendNotificationByGroup(FCMNotificationModel model, Broadcastmodel Bmodel)
        {

             var result=await Task.Run(() => PushNotificationManager.SendNotificationAsync(model.DeviceToken, model.Message, model.Title, model.TargetId, model.Type));
            Bmodel.Result = result.ToString();
            Bmodel.Result = new JavaScriptSerializer().Serialize(result);
            _pushnotificationDBService.BroadCastNotificationUpdate(Bmodel);
        }


        public Task BroadcastNotificationByToGroup(string customerType, List<string> postalCodes, string customerGender, string customerOrderType, int orderCount, string message)
        {
            PushNotificationModel model = new PushNotificationModel();

            Broadcastmodel BroadModel = new Broadcastmodel();
           

            // maintaing the record of total users , current batch etc
            BroadcastParam param = new BroadcastParam();
            param.customerType = customerType;
            param.customerGender = customerGender;
            param.customerOrderType = customerOrderType;
            param.orderCount = orderCount;
            param.next = null;
            param.offset = null;
            List<UsersGroupModel> usermodel = new List<UsersGroupModel>();
            usermodel = _pushnotificationDBService.BroadcastUsersSelect(param);


            int TotalUsers = usermodel.Count;
            
            if(TotalUsers<=0)
            {
                return  Task.FromResult("No Users Found");
            }

            BroadModel.message = message;
            BroadModel.TenantId = 1;
            BroadModel.CreatedBy = 1;
            BroadModel.Slug = null;
            BroadModel.IsActive = true;
            BroadModel.TotalUsers = TotalUsers;
            int TotalBatches = 0;
            if (TotalUsers <= 500)
            {
                BroadModel.lastBatch = 1;
                BroadModel.PendingUsers = 0;
                BroadModel.next = 500;
                BroadModel.offset = 1;
                BroadModel.Totalbatche = 1;
              
            }
            else
            {
                TotalBatches = TotalUsers / 500+1;

                BroadModel.lastBatch = 1;
                BroadModel.PendingUsers = TotalUsers-500;
                BroadModel.next = 500;
                BroadModel.offset = 1;
                BroadModel.Totalbatche = TotalBatches;
            }
            var response=_pushnotificationDBService.BroadCastNotificationInsert(BroadModel);
            int a = 0;
            if (TotalUsers <= 500)
            {
                a = 0;
            }
            else
            {
                a= TotalUsers / 500+1;
            }
            List<string> deviceToken = new List<string>();
            Broadcastmodel Bmodel = new Broadcastmodel();
            for (int i = 0; i < a+1; i++)
            {
                param.offset = i+1;
                param.next = 500;

                usermodel = _pushnotificationDBService.BroadcastUsersSelect(param);
                Bmodel.lastBatch = i + 1;
                Bmodel.Id = response;
                Bmodel.next = param.next.GetValueOrDefault();
                Bmodel.offset = param.offset.GetValueOrDefault();
                Bmodel.PendingUsers = TotalUsers - (500 * Bmodel.lastBatch)<0?0: TotalUsers - (500 * Bmodel.lastBatch);


                foreach (var item in usermodel)
                {
                    string token = item.DeiviceToken;
                    deviceToken.Add(token);
                }


                Task.Run(() => SendNotificationByGroup(new FCMNotificationModel
                {
                    DeviceToken = deviceToken != null ? deviceToken : null,
                    TargetId = "1",
                    Message = message,
                    Title = "Broadcast",
                    Type = model.TargetType,
                }, Bmodel));


            }


            // old code 
            // var userDevices = PushNotificationSelectForUser(model.UserId);                  

            model.CreatedBy = 1;
            model.IsActive = true;
            model.IsDeleted = false;
            model.Message = message;
            model.TargetType = "Broadcast Msg";
            model.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + "1", KeyConstant.Key);
            model.UserId = 0001;

            return Task.Run(() => _pushnotificationDBService.PushNotificationInsert(model));

            //return Task.Run(() => _pushnotificationDBService.BroadCastNotificationUpdate(Bmodel));
        }
        // 
        public Task BroadcastNotificationByPOSTALCODE(string customerType, string postalCodes, string customerGender, string customerOrderType, int orderCount, string message)
        {
            PushNotificationModel model = new PushNotificationModel();

            Broadcastmodel BroadModel = new Broadcastmodel();
            

            // maintaing the record of total users , current batch etc
            BroadcastParam param = new BroadcastParam();
            param.customerType = customerType;
            param.customerGender = customerGender;
            param.customerOrderType = customerOrderType;
            param.orderCount = orderCount;
         
            param.PostalCode = postalCodes;
            param.next = null;
            param.offset = null;
            List<UsersGroupModel> usermodel = new List<UsersGroupModel>();
            usermodel = _pushnotificationDBService.BroadcastUsersSelectWithPostalCode(param);


            int TotalUsers = usermodel.Count;

            if (TotalUsers <= 0)
            {
                return Task.FromResult("No Users Found");
            }

            BroadModel.message = message;
            BroadModel.TenantId = 1;
            BroadModel.CreatedBy = 1;
            BroadModel.Slug = null;
            BroadModel.IsActive = true;
            BroadModel.TotalUsers = TotalUsers;
            BroadModel.Gender = customerGender;
            BroadModel.UserType = customerType;
            BroadModel.orderCount = orderCount;
            BroadModel.customerOrderType = customerOrderType;
            int TotalBatches = 0;
            if (TotalUsers <= 500)
            {
                BroadModel.lastBatch = 1;
                BroadModel.PendingUsers = 0;
                BroadModel.next = 500;
                BroadModel.offset = 1;
                BroadModel.Totalbatche = 1;

            }
            else
            {
                TotalBatches = TotalUsers / 500 + 1;

                BroadModel.lastBatch = 1;
                BroadModel.PendingUsers = TotalUsers - 500;
                BroadModel.next = 500;
                BroadModel.offset = 1;
                BroadModel.Totalbatche = TotalBatches;
            }
            var response = _pushnotificationDBService.BroadCastNotificationInsert(BroadModel);
            int a = 0;
            if (TotalUsers <= 500)
            {
                a = 0;
            }
            else
            {
                a = TotalUsers / 500 + 1;
            }
            List<string> deviceToken = new List<string>();
            Broadcastmodel Bmodel = new Broadcastmodel();
            for (int i = 0; i < a + 1; i++)
            {
                param.offset = i + 1;
                param.next = 500;

                usermodel = _pushnotificationDBService.BroadcastUsersSelectWithPostalCode(param);
                Bmodel.lastBatch = i + 1;
                Bmodel.Id = response;
                Bmodel.next = param.next.GetValueOrDefault();
                Bmodel.offset = param.offset.GetValueOrDefault();
                Bmodel.PendingUsers = TotalUsers - (500 * Bmodel.lastBatch) < 0 ? 0 : TotalUsers - (500 * Bmodel.lastBatch);


                foreach (var item in usermodel)
                {
                    string token = item.DeiviceToken;
                    deviceToken.Add(token);
                }


                Task.Run(() => SendNotificationByGroup(new FCMNotificationModel
                {
                    DeviceToken = deviceToken != null ? deviceToken : null,
                    TargetId = "1",
                    Message = message,
                    Title = "Broadcast",
                    Type = model.TargetType,
                }, Bmodel));


            }


            // old code 
            // var userDevices = PushNotificationSelectForUser(model.UserId);                  

            model.CreatedBy = 1;
            model.IsActive = true;
            model.IsDeleted = false;
            model.Message = message;
            model.TargetType = "Broadcast Msg";
            //model.TargetId = CryptoEngine.Encrypt("1", KeyConstant.Key);
            model.TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + "1", KeyConstant.Key);
            model.UserId = 0001;

            return Task.Run(() => _pushnotificationDBService.PushNotificationInsert(model));

            //return Task.Run(() => _pushnotificationDBService.BroadCastNotificationUpdate(Bmodel));
        }

    }
}
