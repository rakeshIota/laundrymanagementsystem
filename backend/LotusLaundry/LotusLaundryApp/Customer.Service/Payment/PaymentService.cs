﻿using Customer.DBRepository.Service;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Payment;
using LotusLaundry.Domain.Service;
using LotusLaundry.Service.Xml;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Payment
{
    public class PaymentService : IPaymentService
    {
        public ServiceCustomerDBService _serviceCustomerDBService;
        private IXmlService _xmlService;
        public PaymentService(IXmlService xmlService)
        {
           // _serviceCustomerDBService = new ServiceCustomerDBService();
            //_xmlService = xmlService;
        }

        public async Task<PaymentToken> GetElibilityToken(string key)
        {
           
                string baseAddress = @"https://openapi-sandbox.kasikornbank.com/oauth/token";
                string grant_type = "client_credentials";
                var form = new Dictionary<string, string>
                {
                    {"grant_type", grant_type}

                };
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", key);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                client.DefaultRequestHeaders.Add("x-test-mode", "true");
                client.DefaultRequestHeaders.Add("env-id", "OAUTH2");
                var tokenResponse =  client.PostAsync(baseAddress, new FormUrlEncodedContent(form)).GetAwaiter().GetResult();                
                    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
                    var tok = JsonConvert.DeserializeObject<PaymentToken>(jsonContent);
                    return tok;
          
        }

        

        //public async Task<PaymentQRcodeModel> GetQRCode2(string key, decimal amount)
        //{


        //    //using (var client = new HttpClient())
        //    //{
        //    //    //person p = new person { name = "Sourav", surname = "Kayal" };
        //    //    client.BaseAddress = new Uri("http://localhost:1565/");
        //    //    var response = client.PostAsJsonAsync("api/person", p).Result;
        //    //    if (response.IsSuccessStatusCode)
        //    //    {
        //    //        Console.Write("Success");
        //    //    }
        //    //    else
        //    //        Console.Write("Error");
        //    //}



        //    DateTime utcTime = DateTime.Now;
        //    string baseAddress = @"https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request";
        //    //string grant_type = "client_credentials";




        //        var form = new Dictionary<string, string>
        //        {
                   
        //            {"partnerTxnUid",  PaymentConstant.payKeys.partnerTxnUid.ToString()},
        //            {"partnerId", PaymentConstant.payKeys.partnerId.ToString()},
        //            {"partnerSecret", PaymentConstant.payKeys.partnerSecret.ToString()},
        //            {"requestDt", "2020-08-12T10:05:45-06:0"},
        //            {"merchantId", PaymentConstant.payKeys.merchantId.ToString()},
        //            //{"terminalId", PaymentConstant.payKeys.terminalId.ToString()},
        //            {"qrType", PaymentConstant.payKeys.qrType.ToString()},
        //            {"txnAmount", amount.ToString()},
        //            {"txnCurrencyCode", PaymentConstant.payKeys.txnCurrencyCode.ToString()},
        //            {"reference1", "INV001"},
        //            {"reference2", "HELLOWORLD"},
        //            {"reference3","INV001"},
        //            {"reference4", "INV001"}
        //            //,{"metadata", PaymentConstant.payKeys.partnerTxnUid.ToString()}
        //        };

           

        //    var json = JsonConvert.SerializeObject(form, Formatting.Indented);
        //   // var stringContent = new StringContent(json);


        //    HttpClient client = new HttpClient();
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    client.DefaultRequestHeaders.Add("x-test-mode", "true");
        //    client.DefaultRequestHeaders.Add("env-id", "QR002");
        //    client.BaseAddress = new Uri("http://localhost:1565/");
        //    //var tokenResponse = client.PostAsync(baseAddress, new form  (form)).GetAwaiter().GetResult();
        //    // HttpResponseMessage tokenResponse = client.GetAsync(form).Result;
        //    // HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest).ConfigureAwait(false);
        //    //  var tokenResponse =client.PostAsync(baseAddress, stringContent).GetAwaiter().GetResult();

        //    //var response = client.PostAsync("api/person", json).Result;

        //    var tokenResponse = client.PostAsync(baseAddress, new FormUrlEncodedContent(form)).GetAwaiter().GetResult();
        //    var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
        //    var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(jsonContent);
        //    return QRCode;


            


        //}

        //public  async  Task<PaymentQRcodeModel> GetQRCode123(string key, decimal amount)
        //{
        //    var form = new Dictionary<string, string>
        //        {

        //            {"partnerTxnUid",  PaymentConstant.payKeys.partnerTxnUid.ToString()},
        //            {"partnerId", PaymentConstant.payKeys.partnerId.ToString()},
        //            {"partnerSecret", PaymentConstant.payKeys.partnerSecret.ToString()},
        //            {"requestDt", "2020-08-20T10:05:45-06:0"},
        //            {"merchantId", PaymentConstant.payKeys.merchantId.ToString()},
        //            //{"terminalId", PaymentConstant.payKeys.terminalId.ToString()},
        //            {"qrType", PaymentConstant.payKeys.qrType.ToString()},
        //            {"txnAmount", amount.ToString()},
        //            {"txnCurrencyCode", PaymentConstant.payKeys.txnCurrencyCode.ToString()},
        //            {"reference1", "INV001"},
        //            {"reference2", "HELLOWORLD"},
        //            {"reference3","INV001"},
        //            {"reference4", "INV001"}
        //            //,{"metadata", PaymentConstant.payKeys.partnerTxnUid.ToString()}
        //        };
        //    var json = JsonConvert.SerializeObject(form, Formatting.Indented);
        //    using (var stringContent = new StringContent(json.ToString(), System.Text.Encoding.UTF8, "application/json"))
        //    using (var client = new HttpClient())
        //    {
        //        try
        //        {
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            client.DefaultRequestHeaders.Add("x-test-mode", "true");
        //            client.DefaultRequestHeaders.Add("env-id", "QR002");
        //            var response = client.PostAsync("https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request", stringContent).GetAwaiter().GetResult(); ;
        //            var result = await response.Content.ReadAsStringAsync();
        //            //var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
        //            var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(result);

        //            return QRCode;
        //        }
        //        catch (Exception ex)
        //        {
        //            var response = await client.PostAsync("https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request", stringContent);
        //            var result = await response.Content.ReadAsStringAsync();
        //            var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(result);
        //            return QRCode;
        //        }
        //    }
        //}



        public PaymentQRcodeModel GetQRCode(string key, decimal amount)
        {
            try
            {
                QrForGenarateModel model = new QrForGenarateModel();
                model.partnerTxnUid = PaymentConstant.payKeys.partnerTxnUid.ToString();
                model.partnerId = PaymentConstant.payKeys.partnerId.ToString();
                model.partnerSecret = PaymentConstant.payKeys.partnerSecret.ToString();
                model.requestDt = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss zzz"));
                model.merchantId = PaymentConstant.payKeys.merchantId.ToString();
                model.qrType = PaymentConstant.payKeys.qrType.ToString();
                model.txnAmount = amount;
                model.txnCurrencyCode = PaymentConstant.payKeys.txnCurrencyCode.ToString();
                model.reference1 = "INV001";
                model.reference2 = "HELLOWORLD";
                model.reference3 = "INV001";
                model.reference4 = "INV001";

                string accessToken = key;
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + accessToken;
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                    wc.Headers[HttpRequestHeader.Accept] = "application/json";
                    var requestJson = JsonConvert.SerializeObject(model);
                   
                    wc.Headers.Add("x-test-mode", "true");
                    wc.Headers.Add("env-id", "QR002");


                    var response = wc.UploadString(new Uri("https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request"), "POST", requestJson);

                   // var responseModel = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);

                    //var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);
                    var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);

                    return QRCode;
                }
            }
            catch (WebException ex)
            {
                PaymentQRcodeModel Errormodel = new PaymentQRcodeModel();

                if (ex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)ex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            
                            var model = JsonConvert.DeserializeObject<PaymentQRcodeModel>(error);
                            return model;

                        }
                    }
                }
                else
                {
                   
                }
                return Errormodel;
            }
        }

        public PaymentQRcodeModel CancelQRCode(string key, decimal amount)
        {
            try
            {
                QrMOdel model = new QrMOdel();
                model.partnerTxnUid = PaymentConstant.payKeys.partnerTxnUid.ToString();
                model.partnerId = PaymentConstant.payKeys.partnerId.ToString();
                model.partnerSecret = PaymentConstant.payKeys.partnerSecret.ToString();
                model.requestDt = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss zzz"));
                model.merchantId = PaymentConstant.payKeys.merchantId.ToString();
                model.qrType = PaymentConstant.payKeys.qrType.ToString();
                model.terminalId = PaymentConstant.payKeys.terminalId.ToString(); ;
                model.origPartnerTxnUid = PaymentConstant.payKeys.origPartnerTxnUid.ToString();
                //model.reference1 = "INV001";
                //model.reference2 = "HELLOWORLD";
                //model.reference3 = "INV001";
                //model.reference4 = "INV001";

                string accessToken = key;
                using (WebClient wc = new WebClient())
                {
                    wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + accessToken;
                    wc.Headers[HttpRequestHeader.ContentType] = "application/json";
                  //  wc.Headers[HttpRequestHeader.Accept] = "application/json";
                    var requestJson = JsonConvert.SerializeObject(model);

                 //   wc.Headers.Add("x-test-mode", "true");
                 //   wc.Headers.Add("env-id", "QR002");


                    var response = wc.UploadString(new Uri("https://openapi-sandbox.kasikornbank.com/v1/qrpayment/cancel"), "POST", requestJson);

                    // var responseModel = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);

                    //var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);
                    var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(response);

                    return QRCode;
                }
            }
            catch (WebException ex)
            {
                PaymentQRcodeModel Errormodel = new PaymentQRcodeModel();

                if (ex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)ex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();

                            var model = JsonConvert.DeserializeObject<PaymentQRcodeModel>(error);
                            return model;

                        }
                    }
                }
                else
                {

                }
                return Errormodel;
            }
        }



        //public async Task<PaymentQRcodeModel> GetQRCode321(string key, decimal amount )
        //{


        //   // var client = new RestClient("https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request");
        //    //client.Timeout = -1;
        //    //var request = new RestRequest(Method.POST);
        //    //request.AddHeader("Content-Type", "application/json");
        //    //request.AddHeader("Authorization", "Bearer 8sTfzXGF6EGgnwVH3wE0j2kEPnPO");
        //    //request.AddHeader("x-test-mode", "true");
        //    //request.AddHeader("env-id", "QR002");
        //    //request.AddParameter("application/json", "{\r\n\"partnerTxnUid\": \"PARTNERTEST0001\",\r\n  \"partnerId\": \"PTR1051673\",\r\n  \"partnerSecret\": \"d4bded59200547bc85903574a293831b\",\r\n  \"merchantId\": \"KB102057149704\",\r\n \"qrType\": \"3\",\r\n \"txnAmount\": 80.99,\r\n\"txnCurrencyCode\": \"THB\",\r\n \"reference1\": \"INV001\",\r\n \"reference2\": \"HELLOWORLD\",\r\n  \"reference3\": \"INV001\",\r\n  \"reference4\": \"INV001\",\r\n \"requestDt\": \"2020-08-20T10:05:45-06:00\"\r\n}", ParameterType.RequestBody);
        //    //IRestResponse response = client.Execute(request);
        //    //Console.WriteLine(response.Content);



        //    string baseTokenUrl = "https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request";
        //    using (var client_token = new HttpClient())
        //    {
        //        client_token.BaseAddress = new Uri($"{baseTokenUrl}");
        //        dynamic dynamicJson = new ExpandoObject();
        //        dynamicJson.partnerTxnUid = PaymentConstant.payKeys.partnerTxnUid.ToString();
        //        dynamicJson.partnerId = PaymentConstant.payKeys.partnerId.ToString();
        //        dynamicJson.partnerSecret = PaymentConstant.payKeys.partnerSecret.ToString();
        //        dynamicJson.requestDt = "2020-08-20T10:05:45-06:0";
        //        dynamicJson.merchantId = PaymentConstant.payKeys.merchantId.ToString();
        //        dynamicJson.qrType = PaymentConstant.payKeys.qrType.ToString();
        //        dynamicJson.txnAmount = amount;
        //        dynamicJson.txnCurrencyCode = PaymentConstant.payKeys.txnCurrencyCode.ToString();
        //        dynamicJson.reference1 = "INV001";
        //        dynamicJson.reference2 = "HELLOWORLD";
        //        dynamicJson.reference3 = "INV001";
        //        dynamicJson.reference4 = "INV001";




        //        string json = "{\r\n\"partnerTxnUid\": \"PARTNERTEST0001\",\r\n  \"partnerId\": \"PTR1051673\",\r\n  \"partnerSecret\": \"d4bded59200547bc85903574a293831b\",\r\n  \"merchantId\": \"KB102057149704\",\r\n \"qrType\": \"3\",\r\n \"txnAmount\": 80.99,\r\n\"txnCurrencyCode\": \"THB\",\r\n \"reference1\": \"INV001\",\r\n \"reference2\": \"HELLOWORLD\",\r\n  \"reference3\": \"INV001\",\r\n  \"reference4\": \"INV001\",\r\n \"requestDt\": \"2020-08-20T10:05:45-06:00\"\r\n}";
        //        //json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
        //        // var client = new System.Net.Http.HttpClient();

        //      //  client_token.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(
        //        //       "Bearer", key);
        //       // client_token.DefaultRequestHeaders.Add("x-test-mode", "true");
        //       // client_token.DefaultRequestHeaders.Add("env-id", "QR002");

        //       // client_token.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
        //        //_logger.LogInformation(string.Format(CultureInfo.InvariantCulture,
        //        //                $"Getting access token from {AD_API} @ {client_token.BaseAddress.AbsoluteUri}"));
        //        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");




        //       // client_token.DefaultRequestHeaders.Add("Content-Type", "application/json");
        //       // client_token.DefaultRequestHeaders.Add("Authorization", "Bearer 8sTfzXGF6EGgnwVH3wE0j2kEPnPO");
        //        client_token.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", key));
        //        client_token.DefaultRequestHeaders.Add("x-test-mode", "true");

        //        client_token.DefaultRequestHeaders.Add("env-id", "QR002");
        //        //("application/json", "{\r\n\"partnerTxnUid\": \"PARTNERTEST0001\",\r\n  \"partnerId\": \"PTR1051673\",\r\n  \"partnerSecret\": \"d4bded59200547bc85903574a293831b\",\r\n  \"merchantId\": \"KB102057149704\",\r\n \"qrType\": \"3\",\r\n \"txnAmount\": 80.99,\r\n\"txnCurrencyCode\": \"THB\",\r\n \"reference1\": \"INV001\",\r\n \"reference2\": \"HELLOWORLD\",\r\n  \"reference3\": \"INV001\",\r\n  \"reference4\": \"INV001\",\r\n \"requestDt\": \"2020-08-20T10:05:45-06:00\"\r\n}", ParameterType.RequestBody);



        //        var response1 = client_token.PostAsync(client_token.BaseAddress.AbsoluteUri, stringContent
        //        ).Result;
        //        var result = response1.Content.ReadAsStringAsync().Result;


        //       // var result = await respon.Content.ReadAsStringAsync();

        //        var QRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(result);
        //        return QRCode;
        //    }


        //}


        public async Task<PaymentQRcodeModel> enquireTransaction(string key)
        {
            string baseAddress = @"https://openapi-sandbox.kasikornbank.com/v1/qrpayment/inquiry";
            string grant_type = "client_credentials";
            var form = new Dictionary<string, string>
                {
                    {"partnerTxnUid", grant_type},
                    {"partnerId", grant_type},
                    {"partnerSecret", grant_type},
                   // {"partnerSecret", grant_type},

                    {"requestDt", grant_type},
                    {"merchantId", grant_type},
                    {"terminalId", grant_type},
                    {"qrType", grant_type},
                    {"origPartnerTxnUid", grant_type}

                

                };
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //  client.DefaultRequestHeaders.Add("x-test-mode", "true");
            //client.DefaultRequestHeaders.Add("env-id", "OAUTH2");
            var tokenResponse = client.PostAsync(baseAddress, new FormUrlEncodedContent(form)).GetAwaiter().GetResult();
            var jsonContent = await tokenResponse.Content.ReadAsStringAsync();
            var CancelQRCode = JsonConvert.DeserializeObject<PaymentQRcodeModel>(jsonContent);
            return CancelQRCode;
        }

        
    }
}
