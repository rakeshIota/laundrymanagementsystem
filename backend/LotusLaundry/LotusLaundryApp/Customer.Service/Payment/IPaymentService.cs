﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Payment;
using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Payment
{
    public interface IPaymentService
    {
       

         Task<PaymentToken> GetElibilityToken(string key);

       // Task<PaymentQRcodeModel> GetQRCode(string key,decimal amount);

        //Task<PaymentQRcodeModel> GetQRCode2(string key, decimal amount);
        PaymentQRcodeModel GetQRCode(string key, decimal amount);
        PaymentQRcodeModel CancelQRCode(string key, decimal amount);
    }
}
