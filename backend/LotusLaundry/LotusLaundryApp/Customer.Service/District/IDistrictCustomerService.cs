﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.District
{
    public interface IDistrictCustomerService
    {
        List<DistrictModel> DistrictSelect(SearchParam param);
    }
}
