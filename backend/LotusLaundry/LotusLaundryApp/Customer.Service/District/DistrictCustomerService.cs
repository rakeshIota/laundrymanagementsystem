﻿using Customer.DBRepository.District;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.District
{
    public class DistrictCustomerService : IDistrictCustomerService
    {
        public DistrictCustomerDBService _districtCustomerDBService;

        public DistrictCustomerService()
        {
            _districtCustomerDBService = new DistrictCustomerDBService();
        }


        public List<DistrictModel> DistrictSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            var response = _districtCustomerDBService.DistrictSelect(param);
            return response;
        }
    }
}
