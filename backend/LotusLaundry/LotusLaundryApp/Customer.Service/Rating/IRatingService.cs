﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Rating;

namespace Driver.Service.Rating
{
    public interface IRatingService
    {
        /// <summary>
        /// used for insertion postalcode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long RatingInsert(RatingModel model);
        
        /// <summary>
        /// Method for update postalcode
        /// </summary>
        /// <param name="model"></param>
        void RatingUpdate(RatingModel model);

        /// <summary>
        /// use to select all postalcode or select postalcode by id 
        /// </summary>
        /// <param name="postalcodeId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<RatingModel> SelectRating(SearchParam param);
        //
      
    }
}
