﻿
using LotusLaundry.Domain.Rating;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Rating;

using Driver.DBRepository.Rating;

namespace Driver.Service.Rating
{
    public class RatingService : IRatingService
    {	
        public RatingDBService _ratingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public RatingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _ratingDBService = new RatingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long RatingInsert(RatingModel model)
        {
            var id = _ratingDBService.RatingInsert(model);
            return id;
        }

        public void RatingUpdate(RatingModel model)
        {
            _ratingDBService.RatingUpdate(model);
        }

        public List<RatingModel> SelectRating(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            
         
            var response = _ratingDBService.SelectRating(param);

            response.ForEach(x =>
            {
            });
            return response;
        }

     

    }
}
