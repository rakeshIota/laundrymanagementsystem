﻿using LotusLaundry.Domain;
using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Province
{
    public interface IProvinceCustomerService
    {
        List<ProvinceModel> ProvinceSelect(SearchParam param);
    }
}
