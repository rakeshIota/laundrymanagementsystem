﻿using Customer.DBRepository.Province;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Province
{
    public class ProvinceCustomerService : IProvinceCustomerService
    {
        public ProvinceCustomerDBService _provinceCustomerDBService;
        public ProvinceCustomerService()
        {
            _provinceCustomerDBService = new ProvinceCustomerDBService();
        }

        public List<ProvinceModel> ProvinceSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.LanguageIdStr != null)
            {
                param.LanguageId = Convert.ToInt64(CryptoEngine.Decrypt(param.LanguageIdStr, KeyConstant.Key));
            }
            var response = _provinceCustomerDBService.ProvinceSelect(param);
            return response;
        }
    }
}
