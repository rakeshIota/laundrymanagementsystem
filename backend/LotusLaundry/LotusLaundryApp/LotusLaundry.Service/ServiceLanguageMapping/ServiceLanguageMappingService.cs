﻿using LotusLaundry.DBRepository.ServiceLanguageMapping;
using LotusLaundry.Domain.ServiceLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.ServiceLanguageMapping
{
    public class ServiceLanguageMappingService : IServiceLanguageMappingService
    {	
        public ServiceLanguageMappingDBService _servicelanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ServiceLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _servicelanguagemappingDBService = new ServiceLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long ServiceLanguageMappingInsert(ServiceLanguageMappingModel model)
        {
            var id = _servicelanguagemappingDBService.ServiceLanguageMappingInsert(model);
            return id;
        }

        public void ServiceLanguageMappingUpdate(ServiceLanguageMappingModel model)
        {
            _servicelanguagemappingDBService.ServiceLanguageMappingUpdate(model);
        }

        public List<ServiceLanguageMappingModel> SelectServiceLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _servicelanguagemappingDBService.SelectServiceLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
