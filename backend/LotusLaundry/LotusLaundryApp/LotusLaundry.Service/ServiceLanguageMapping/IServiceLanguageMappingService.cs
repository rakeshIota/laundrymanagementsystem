﻿using LotusLaundry.Domain.ServiceLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.ServiceLanguageMapping
{
    public interface IServiceLanguageMappingService
    {
        /// <summary>
        /// used for insertion servicelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ServiceLanguageMappingInsert(ServiceLanguageMappingModel model);
        
        /// <summary>
        /// Method for update servicelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void ServiceLanguageMappingUpdate(ServiceLanguageMappingModel model);

        /// <summary>
        /// use to select all servicelanguagemapping or select servicelanguagemapping by id 
        /// </summary>
        /// <param name="servicelanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ServiceLanguageMappingModel> SelectServiceLanguageMapping(SearchParam param);
    }
}
