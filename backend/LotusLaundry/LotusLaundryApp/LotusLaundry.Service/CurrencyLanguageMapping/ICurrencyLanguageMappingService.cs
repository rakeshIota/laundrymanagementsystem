﻿using LotusLaundry.Domain.CurrencyLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.CurrencyLanguageMapping
{
    public interface ICurrencyLanguageMappingService
    {
        /// <summary>
        /// used for insertion currencylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CurrencyLanguageMappingInsert(CurrencyLanguageMappingModel model);
        
        /// <summary>
        /// Method for update currencylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void CurrencyLanguageMappingUpdate(CurrencyLanguageMappingModel model);

        /// <summary>
        /// use to select all currencylanguagemapping or select currencylanguagemapping by id 
        /// </summary>
        /// <param name="currencylanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CurrencyLanguageMappingModel> SelectCurrencyLanguageMapping(SearchParam param);
    }
}
