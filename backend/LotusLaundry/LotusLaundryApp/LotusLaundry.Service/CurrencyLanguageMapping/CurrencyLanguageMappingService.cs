﻿using LotusLaundry.DBRepository.CurrencyLanguageMapping;
using LotusLaundry.Domain.CurrencyLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.CurrencyLanguageMapping
{
    public class CurrencyLanguageMappingService : ICurrencyLanguageMappingService
    {	
        public CurrencyLanguageMappingDBService _currencylanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CurrencyLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _currencylanguagemappingDBService = new CurrencyLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CurrencyLanguageMappingInsert(CurrencyLanguageMappingModel model)
        {
            var id = _currencylanguagemappingDBService.CurrencyLanguageMappingInsert(model);
            return id;
        }

        public void CurrencyLanguageMappingUpdate(CurrencyLanguageMappingModel model)
        {
            _currencylanguagemappingDBService.CurrencyLanguageMappingUpdate(model);
        }

        public List<CurrencyLanguageMappingModel> SelectCurrencyLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _currencylanguagemappingDBService.SelectCurrencyLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
