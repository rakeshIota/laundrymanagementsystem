﻿using LotusLaundry.DBRepository.DefaultMessageLanguageMapping;
using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.DefaultMessageLanguageMapping
{
    public class DefaultMessageLanguageMappingService : IDefaultMessageLanguageMappingService
    {	
        public DefaultMessageLanguageMappingDBService _defaultmessagelanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DefaultMessageLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _defaultmessagelanguagemappingDBService = new DefaultMessageLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long DefaultMessageLanguageMappingInsert(DefaultMessageLanguageMappingModel model)
        {
            var id = _defaultmessagelanguagemappingDBService.DefaultMessageLanguageMappingInsert(model);
            return id;
        }

        public void DefaultMessageLanguageMappingUpdate(DefaultMessageLanguageMappingModel model)
        {
            _defaultmessagelanguagemappingDBService.DefaultMessageLanguageMappingUpdate(model);
        }

        public List<DefaultMessageLanguageMappingModel> SelectDefaultMessageLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _defaultmessagelanguagemappingDBService.SelectDefaultMessageLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
