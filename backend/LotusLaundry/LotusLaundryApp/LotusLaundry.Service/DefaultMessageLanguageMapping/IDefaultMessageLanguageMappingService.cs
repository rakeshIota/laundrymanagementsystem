﻿using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.DefaultMessageLanguageMapping
{
    public interface IDefaultMessageLanguageMappingService
    {
        /// <summary>
        /// used for insertion defaultmessagelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DefaultMessageLanguageMappingInsert(DefaultMessageLanguageMappingModel model);
        
        /// <summary>
        /// Method for update defaultmessagelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void DefaultMessageLanguageMappingUpdate(DefaultMessageLanguageMappingModel model);

        /// <summary>
        /// use to select all defaultmessagelanguagemapping or select defaultmessagelanguagemapping by id 
        /// </summary>
        /// <param name="defaultmessagelanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DefaultMessageLanguageMappingModel> SelectDefaultMessageLanguageMapping(SearchParam param);
    }
}
