﻿using LotusLaundry.DBRepository.Currency;
using LotusLaundry.Domain.Currency;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.Currency
{
    public class CurrencyService : ICurrencyService
    {	
        public CurrencyDBService _currencyDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CurrencyService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _currencyDBService = new CurrencyDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CurrencyInsert(CurrencyModel model)
        {
            var id = _currencyDBService.CurrencyInsert(model);
            return id;
        }

        public void CurrencyUpdate(CurrencyModel model)
        {
            _currencyDBService.CurrencyUpdate(model);
        }

        public List<CurrencyModel> SelectCurrency(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _currencyDBService.SelectCurrency(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
