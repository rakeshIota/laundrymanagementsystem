﻿using LotusLaundry.Domain.Currency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Currency
{
    public interface ICurrencyService
    {
        /// <summary>
        /// used for insertion currency
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CurrencyInsert(CurrencyModel model);
        
        /// <summary>
        /// Method for update currency
        /// </summary>
        /// <param name="model"></param>
        void CurrencyUpdate(CurrencyModel model);

        /// <summary>
        /// use to select all currency or select currency by id 
        /// </summary>
        /// <param name="currencyId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CurrencyModel> SelectCurrency(SearchParam param);
    }
}
