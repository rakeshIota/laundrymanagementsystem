﻿using LotusLaundry.DBRepository.State;
using LotusLaundry.Domain.State;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.StateLanguageMapping;

namespace LotusLaundry.Service.State
{
    public class StateService : IStateService
    {
        public StateDBService _stateDBService;
        public StateLanguageMappingDBService _stateLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public StateService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _stateDBService = new StateDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _stateLanguageMappingDBService = new StateLanguageMappingDBService();


        }
        
        public long StateInsert(StateModel model)
        {
            var id = _stateDBService.StateInsert(model);
            if (model.StateLanguages != null && model.StateLanguages.Count > 0)
            {
                model.StateLanguages.ForEach(x =>
                {
                    x.StateId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _stateLanguageMappingDBService.StateLanguageMappingXMLSave(model.StateLanguages.XmlSerialize());
            }
            return id;
        }

        public void StateUpdate(StateModel model)
        {
            _stateDBService.StateUpdate(model);
            if (model.StateLanguages != null && model.StateLanguages.Count > 0)
            {
                model.StateLanguages.ForEach(x =>
                {
                    x.StateId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _stateLanguageMappingDBService.StateLanguageMappingXMLSave(model.StateLanguages.XmlSerialize());
            }
        }

        public List<StateModel> SelectState(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _stateDBService.SelectState(param);
			response.ForEach(x =>
            {
                x.StateLanguages = _xmlService.GetStateLanguageMappingsByXml(x.StateLanguageXml);
            });
            return response;
        }
    }
}
