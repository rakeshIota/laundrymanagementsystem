﻿using LotusLaundry.Domain.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.State
{
    public interface IStateService
    {
        /// <summary>
        /// used for insertion state
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long StateInsert(StateModel model);
        
        /// <summary>
        /// Method for update state
        /// </summary>
        /// <param name="model"></param>
        void StateUpdate(StateModel model);

        /// <summary>
        /// use to select all state or select state by id 
        /// </summary>
        /// <param name="stateId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<StateModel> SelectState(SearchParam param);
    }
}
