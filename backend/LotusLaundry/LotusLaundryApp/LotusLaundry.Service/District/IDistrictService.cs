﻿using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.District
{
    public interface IDistrictService
    {
        /// <summary>
        /// used for insertion district
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DistrictInsert(DistrictModel model);
        
        /// <summary>
        /// Method for update district
        /// </summary>
        /// <param name="model"></param>
        void DistrictUpdate(DistrictModel model);

        /// <summary>
        /// use to select all district or select district by id 
        /// </summary>
        /// <param name="districtId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DistrictModel> SelectDistrict(SearchParam param);
    }
}
