﻿using LotusLaundry.DBRepository.District;
using LotusLaundry.Domain.District;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.DistrictLanguageMapping;

namespace LotusLaundry.Service.District
{
    public class DistrictService : IDistrictService
    {
        public DistrictDBService _districtDBService;
        public DistrictLanguageMappingDBService _districtLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DistrictService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _districtDBService = new DistrictDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _districtLanguageMappingDBService = new DistrictLanguageMappingDBService();


        }
        
        public long DistrictInsert(DistrictModel model)
        {
            var id = _districtDBService.DistrictInsert(model);
            if (model.DistrictLanguages != null && model.DistrictLanguages.Count > 0)
            {
                model.DistrictLanguages.ForEach(x =>
                {
                    x.DistrictId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _districtLanguageMappingDBService.DistrictLanguageMappingXMLSave(model.DistrictLanguages.XmlSerialize());
            }
            return id;
        }

        public void DistrictUpdate(DistrictModel model)
        {

            

            _districtDBService.DistrictUpdate(model);
            if (model.DistrictLanguages != null && model.DistrictLanguages.Count > 0)
            {
                model.DistrictLanguages.ForEach(x =>
                {
                    x.DistrictId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _districtLanguageMappingDBService.DistrictLanguageMappingXMLSave(model.DistrictLanguages.XmlSerialize());
            }
        }

        public List<DistrictModel> SelectDistrict(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _districtDBService.SelectDistrict(param);
			response.ForEach(x =>
            {
                x.DistrictLanguages = _xmlService.GetDistrictLanguageMappingsByXml(x.DistrictLanguageXml);
            });
            return response;
        }
    }
}
