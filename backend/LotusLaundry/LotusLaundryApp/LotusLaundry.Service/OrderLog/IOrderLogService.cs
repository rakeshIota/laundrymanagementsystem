﻿using LotusLaundry.Domain.OrderLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.OrderLog
{
    public interface IOrderLogService
    {
        /// <summary>
        /// used for insertion orderlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long OrderLogInsert(OrderLogModel model);
        
        /// <summary>
        /// Method for update orderlog
        /// </summary>
        /// <param name="model"></param>
        void OrderLogUpdate(OrderLogModel model);

        /// <summary>
        /// use to select all orderlog or select orderlog by id 
        /// </summary>
        /// <param name="orderlogId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<OrderLogModel> SelectOrderLog(SearchParam param);
    }
}
