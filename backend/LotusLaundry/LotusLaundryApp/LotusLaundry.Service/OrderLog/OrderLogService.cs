﻿using LotusLaundry.DBRepository.OrderLog;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.OrderLog
{
    public class OrderLogService : IOrderLogService
    {	
        public OrderLogDBService _orderlogDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public OrderLogService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _orderlogDBService = new OrderLogDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long OrderLogInsert(OrderLogModel model)
        {
            var id = _orderlogDBService.OrderLogInsert(model);
            return id;
        }

        public void OrderLogUpdate(OrderLogModel model)
        {
            _orderlogDBService.OrderLogUpdate(model);
        }

        public List<OrderLogModel> SelectOrderLog(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _orderlogDBService.SelectOrderLog(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
