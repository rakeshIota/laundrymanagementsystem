﻿using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Language
{
    public interface ILanguageService
    {
        /// <summary>
        /// used for insertion language
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long LanguageInsert(LanguageModel model);
        
        /// <summary>
        /// Method for update language
        /// </summary>
        /// <param name="model"></param>
        void LanguageUpdate(LanguageModel model);

        /// <summary>
        /// use to select all language or select language by id 
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<LanguageModel> SelectLanguage(SearchParam param);
    }
}
