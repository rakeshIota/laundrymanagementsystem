﻿using LotusLaundry.DBRepository.Language;
using LotusLaundry.Domain.Language;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.Language
{
    public class LanguageService : ILanguageService
    {	
        public LanguageDBService _languageDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public LanguageService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _languageDBService = new LanguageDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long LanguageInsert(LanguageModel model)
        {
            var id = _languageDBService.LanguageInsert(model);
            return id;
        }

        public void LanguageUpdate(LanguageModel model)
        {
            _languageDBService.LanguageUpdate(model);
        }

        public List<LanguageModel> SelectLanguage(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _languageDBService.SelectLanguage(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
