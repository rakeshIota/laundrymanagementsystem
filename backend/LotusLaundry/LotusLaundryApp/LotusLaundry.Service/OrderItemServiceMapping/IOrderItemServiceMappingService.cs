﻿using LotusLaundry.Domain.OrderItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.OrderItemServiceMapping
{
    public interface IOrderItemServiceMappingService
    {
        /// <summary>
        /// used for insertion orderitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long OrderItemServiceMappingInsert(OrderItemServiceMappingModel model);
        
        /// <summary>
        /// Method for update orderitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        void OrderItemServiceMappingUpdate(OrderItemServiceMappingModel model);

        /// <summary>
        /// use to select all orderitemservicemapping or select orderitemservicemapping by id 
        /// </summary>
        /// <param name="orderitemservicemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<OrderItemServiceMappingModel> SelectOrderItemServiceMapping(SearchParam param);
    }
}
