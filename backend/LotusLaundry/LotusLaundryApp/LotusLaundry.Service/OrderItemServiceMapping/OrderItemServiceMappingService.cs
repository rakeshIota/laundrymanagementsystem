﻿using LotusLaundry.DBRepository.OrderItemServiceMapping;
using LotusLaundry.Domain.OrderItemServiceMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.OrderItemServiceMapping
{
    public class OrderItemServiceMappingService : IOrderItemServiceMappingService
    {	
        public OrderItemServiceMappingDBService _orderitemservicemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public OrderItemServiceMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _orderitemservicemappingDBService = new OrderItemServiceMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long OrderItemServiceMappingInsert(OrderItemServiceMappingModel model)
        {
            var id = _orderitemservicemappingDBService.OrderItemServiceMappingInsert(model);
            return id;
        }

        public void OrderItemServiceMappingUpdate(OrderItemServiceMappingModel model)
        {
            _orderitemservicemappingDBService.OrderItemServiceMappingUpdate(model);
        }

        public List<OrderItemServiceMappingModel> SelectOrderItemServiceMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _orderitemservicemappingDBService.SelectOrderItemServiceMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
