﻿using LotusLaundry.Domain.DefaultMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.DefaultMessage
{
    public interface IDefaultMessageService
    {
        /// <summary>
        /// used for insertion defaultmessage
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DefaultMessageInsert(DefaultMessageModel model);
        
        /// <summary>
        /// Method for update defaultmessage
        /// </summary>
        /// <param name="model"></param>
        void DefaultMessageUpdate(DefaultMessageModel model);

        /// <summary>
        /// use to select all defaultmessage or select defaultmessage by id 
        /// </summary>
        /// <param name="defaultmessageId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DefaultMessageModel> SelectDefaultMessage(SearchParam param);
    }
}
