﻿using LotusLaundry.DBRepository.DefaultMessage;
using LotusLaundry.Domain.DefaultMessage;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.DefaultMessage
{
    public class DefaultMessageService : IDefaultMessageService
    {	
        public DefaultMessageDBService _defaultmessageDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DefaultMessageService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _defaultmessageDBService = new DefaultMessageDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long DefaultMessageInsert(DefaultMessageModel model)
        {
            var id = _defaultmessageDBService.DefaultMessageInsert(model);
            return id;
        }

        public void DefaultMessageUpdate(DefaultMessageModel model)
        {
            _defaultmessageDBService.DefaultMessageUpdate(model);
        }

        public List<DefaultMessageModel> SelectDefaultMessage(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _defaultmessageDBService.SelectDefaultMessage(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
