﻿using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.DeliveryTypeLanguageMapping
{
    public interface IDeliveryTypeLanguageMappingService
    {
        /// <summary>
        /// used for insertion deliverytypelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DeliveryTypeLanguageMappingInsert(DeliveryTypeLanguageMappingModel model);
        
        /// <summary>
        /// Method for update deliverytypelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void DeliveryTypeLanguageMappingUpdate(DeliveryTypeLanguageMappingModel model);

        /// <summary>
        /// use to select all deliverytypelanguagemapping or select deliverytypelanguagemapping by id 
        /// </summary>
        /// <param name="deliverytypelanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DeliveryTypeLanguageMappingModel> SelectDeliveryTypeLanguageMapping(SearchParam param);
    }
}
