﻿using LotusLaundry.DBRepository.DeliveryTypeLanguageMapping;
using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.DeliveryTypeLanguageMapping
{
    public class DeliveryTypeLanguageMappingService : IDeliveryTypeLanguageMappingService
    {	
        public DeliveryTypeLanguageMappingDBService _deliverytypelanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DeliveryTypeLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _deliverytypelanguagemappingDBService = new DeliveryTypeLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long DeliveryTypeLanguageMappingInsert(DeliveryTypeLanguageMappingModel model)
        {
            var id = _deliverytypelanguagemappingDBService.DeliveryTypeLanguageMappingInsert(model);
            return id;
        }

        public void DeliveryTypeLanguageMappingUpdate(DeliveryTypeLanguageMappingModel model)
        {
            _deliverytypelanguagemappingDBService.DeliveryTypeLanguageMappingUpdate(model);
        }

        public List<DeliveryTypeLanguageMappingModel> SelectDeliveryTypeLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _deliverytypelanguagemappingDBService.SelectDeliveryTypeLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
