﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Stripe;

namespace LotusLaundry.Service.Stripe
{
    public interface IStripeService
    {
        StripeResponseModel MakePayment(string description, string email, string stripeTokenId,decimal? amount);
        long StripeDetails(StripeResponseModel model);

        string CardPayment(string StripeCustomerId, string StripeCardId, string Amount, string CurrencyCode);

    }
}
