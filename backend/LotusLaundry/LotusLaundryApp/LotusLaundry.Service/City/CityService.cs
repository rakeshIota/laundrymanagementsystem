﻿using LotusLaundry.DBRepository.City;
using LotusLaundry.Domain.City;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.CityLanguageMapping;
using LotusLaundry.DBRepository.District;

namespace LotusLaundry.Service.City
{
    public class CityService : ICityService
    {
        
        public CityDBService _cityDBService;
        public CityLanguageMappingDBService _cityLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CityService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            
            _cityDBService = new CityDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _cityLanguageMappingDBService = new CityLanguageMappingDBService();


        }
        
        public long CityInsert(CityModel model)
        {
            var id = _cityDBService.CityInsert(model);
            if (model.CityLanguages != null && model.CityLanguages.Count > 0)
            {
                model.CityLanguages.ForEach(x =>
                {
                    x.CityId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _cityLanguageMappingDBService.CityLanguageMappingXMLSave(model.CityLanguages.XmlSerialize());
            }
            return id;
        }

        public void CityUpdate(CityModel model)
        {
           

            _cityDBService.CityUpdate(model);
            if (model.CityLanguages != null && model.CityLanguages.Count > 0)
            {
                model.CityLanguages.ForEach(x =>
                {
                    x.CityId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _cityLanguageMappingDBService.CityLanguageMappingXMLSave(model.CityLanguages.XmlSerialize());
            }
        }

        public List<CityModel> SelectCity(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _cityDBService.SelectCity(param);
			response.ForEach(x =>
            {
                x.CityLanguages = _xmlService.GetCityLanguageMappingsByXml(x.CityLanguageXml);
            });
            return response;
        }
    }
}
