﻿using LotusLaundry.Domain.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.City
{
    public interface ICityService
    {
        /// <summary>
        /// used for insertion city
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CityInsert(CityModel model);
        
        /// <summary>
        /// Method for update city
        /// </summary>
        /// <param name="model"></param>
        void CityUpdate(CityModel model);

        /// <summary>
        /// use to select all city or select city by id 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CityModel> SelectCity(SearchParam param);
    }
}
