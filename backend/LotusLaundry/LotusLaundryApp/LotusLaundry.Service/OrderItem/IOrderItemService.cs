﻿using LotusLaundry.Domain.OrderItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.OrderItem
{
    public interface IOrderItemService
    {
        /// <summary>
        /// used for insertion orderitem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long OrderItemInsert(OrderItemModel model);
        
        /// <summary>
        /// Method for update orderitem
        /// </summary>
        /// <param name="model"></param>
        void OrderItemUpdate(OrderItemModel model);

        /// <summary>
        /// use to select all orderitem or select orderitem by id 
        /// </summary>
        /// <param name="orderitemId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<OrderItemModel> SelectOrderItem(SearchParam param);

        void OrderItemUpdateByService(OrderItemModel model);
    }
}
