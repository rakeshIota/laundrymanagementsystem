﻿using LotusLaundry.DBRepository.OrderItem;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.OrderItem
{
    public class OrderItemService : IOrderItemService
    {	
        public OrderItemDBService _orderitemDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public OrderItemService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _orderitemDBService = new OrderItemDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long OrderItemInsert(OrderItemModel model)
        {
            var id = _orderitemDBService.OrderItemInsert(model);
            return id;
        }

        public void OrderItemUpdate(OrderItemModel model)
        {
            _orderitemDBService.OrderItemUpdate(model);
        }
        //OrderItemUpdateByService
        public void OrderItemUpdateByService(OrderItemModel model)
        {
            _orderitemDBService.OrderItemUpdate(model);
        }
        public List<OrderItemModel> SelectOrderItem(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _orderitemDBService.SelectOrderItem(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
