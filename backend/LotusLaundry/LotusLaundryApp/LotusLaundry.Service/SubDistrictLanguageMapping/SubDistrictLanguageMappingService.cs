﻿using LotusLaundry.DBRepository.SubDistrictLanguageMapping;
using LotusLaundry.Domain.SubDistrictLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.SubDistrictLanguageMapping
{
    public class SubDistrictLanguageMappingService : ISubDistrictLanguageMappingService
    {	
        public SubDistrictLanguageMappingDBService _subdistrictlanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public SubDistrictLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _subdistrictlanguagemappingDBService = new SubDistrictLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long SubDistrictLanguageMappingInsert(SubDistrictLanguageMappingModel model)
        {
            var id = _subdistrictlanguagemappingDBService.SubDistrictLanguageMappingInsert(model);
            return id;
        }

        public void SubDistrictLanguageMappingUpdate(SubDistrictLanguageMappingModel model)
        {
            _subdistrictlanguagemappingDBService.SubDistrictLanguageMappingUpdate(model);
        }

        public List<SubDistrictLanguageMappingModel> SelectSubDistrictLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _subdistrictlanguagemappingDBService.SelectSubDistrictLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
