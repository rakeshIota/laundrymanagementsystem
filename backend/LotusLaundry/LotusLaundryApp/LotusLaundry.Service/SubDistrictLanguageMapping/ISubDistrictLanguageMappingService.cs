﻿using LotusLaundry.Domain.SubDistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.SubDistrictLanguageMapping
{
    public interface ISubDistrictLanguageMappingService
    {
        /// <summary>
        /// used for insertion subdistrictlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long SubDistrictLanguageMappingInsert(SubDistrictLanguageMappingModel model);
        
        /// <summary>
        /// Method for update subdistrictlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void SubDistrictLanguageMappingUpdate(SubDistrictLanguageMappingModel model);

        /// <summary>
        /// use to select all subdistrictlanguagemapping or select subdistrictlanguagemapping by id 
        /// </summary>
        /// <param name="subdistrictlanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<SubDistrictLanguageMappingModel> SelectSubDistrictLanguageMapping(SearchParam param);
    }
}
