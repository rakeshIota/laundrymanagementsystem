﻿using LotusLaundry.Domain.MessageLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.MessageLog
{
    public interface IMessageLogService
    {
        /// <summary>
        /// used for insertion messagelog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long MessageLogInsert(MessageLogModel model);
        
        /// <summary>
        /// Method for update messagelog
        /// </summary>
        /// <param name="model"></param>
        void MessageLogUpdate(MessageLogModel model);

        /// <summary>
        /// use to select all messagelog or select messagelog by id 
        /// </summary>
        /// <param name="messagelogId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<MessageLogModel> SelectMessageLog(SearchParam param);
    }
}
