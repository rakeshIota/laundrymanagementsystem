﻿using LotusLaundry.DBRepository.MessageLog;
using LotusLaundry.Domain.MessageLog;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.MessageLog
{
    public class MessageLogService : IMessageLogService
    {	
        public MessageLogDBService _messagelogDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public MessageLogService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _messagelogDBService = new MessageLogDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long MessageLogInsert(MessageLogModel model)
        {
            var id = _messagelogDBService.MessageLogInsert(model);
            return id;
        }

        public void MessageLogUpdate(MessageLogModel model)
        {
            _messagelogDBService.MessageLogUpdate(model);
        }

        public List<MessageLogModel> SelectMessageLog(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _messagelogDBService.SelectMessageLog(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
