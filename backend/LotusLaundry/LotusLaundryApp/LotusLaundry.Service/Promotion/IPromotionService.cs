﻿using LotusLaundry.Domain.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Promotion
{
    public interface IPromotionService
    {
        /// <summary>
        /// used for insertion projectdocument
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PromotionInsert(PromotionModel model);
        
        /// <summary>
        /// Method for update projectdocument
        /// </summary>
        /// <param name="model"></param>
        void PromotionUpdate(PromotionModel model);

        /// <summary>
        /// use to select all projectdocument or select projectdocument by id 
        /// </summary>
        /// <param name="projectdocumentId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PromotionModel> SelectPromotion(SearchParam param);
    }
}
