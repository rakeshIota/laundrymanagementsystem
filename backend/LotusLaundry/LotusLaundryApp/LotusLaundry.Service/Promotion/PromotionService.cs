﻿
using LotusLaundry.Domain.Promotion;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;

using LotusLaundry.Service.Promotion;
using LotusLaundry.DBRepository.Promotion;

namespace LotusLaundry.Service.Promotion
{
    public class PromotionService : IPromotionService
    {	
        public PromotionDBService _promotionDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PromotionService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _promotionDBService = new PromotionDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long PromotionInsert(PromotionModel model)
        {
            var id = _promotionDBService.PromotionInsert(model);
		            if (model.File != null)
		            {
		                //set target path and move file from target location
		                if (_imageOption == FileType.TYPE_FOLDER){
		                    //set target path and move file from target location
		                    model.File = _fileGroupService.SetPathAndMoveFile(model.File, id, FileType.PROMOTION);
		                }
		                model.File.ForEach(x => x.Type = FileType.PROMOTION);
		                //Save list of file in our DB
		                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, id, model.File.XmlSerialize());
		            }
            return id;
        }

        public void PromotionUpdate(PromotionModel model)
        {

            if (model.File != null)
            {
                
                    model.File.ForEach(x => x.Type = FileType.PROMOTION);
                   
                    _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, model.Id, model.File.XmlSerialize());
                

            }
            _promotionDBService.PromotionUpdate(model);
        }

        public List<PromotionModel> SelectPromotion(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _promotionDBService.SelectPromotion(param);
			response.ForEach(x =>
            {
            
             x.File = _xmlService.GetFileGroupItemsByXml(x.FileXml);
            });
            return response;
        }
    }
}
