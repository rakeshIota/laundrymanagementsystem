﻿using LotusLaundry.Domain.DistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.DistrictLanguageMapping
{
    public interface IDistrictLanguageMappingService
    {
        /// <summary>
        /// used for insertion districtlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DistrictLanguageMappingInsert(DistrictLanguageMappingModel model);
        
        /// <summary>
        /// Method for update districtlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void DistrictLanguageMappingUpdate(DistrictLanguageMappingModel model);

        /// <summary>
        /// use to select all districtlanguagemapping or select districtlanguagemapping by id 
        /// </summary>
        /// <param name="districtlanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DistrictLanguageMappingModel> SelectDistrictLanguageMapping(SearchParam param);
    }
}
