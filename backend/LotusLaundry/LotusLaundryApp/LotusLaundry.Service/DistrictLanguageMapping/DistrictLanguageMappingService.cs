﻿using LotusLaundry.DBRepository.DistrictLanguageMapping;
using LotusLaundry.Domain.DistrictLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.DistrictLanguageMapping
{
    public class DistrictLanguageMappingService : IDistrictLanguageMappingService
    {	
        public DistrictLanguageMappingDBService _districtlanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DistrictLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _districtlanguagemappingDBService = new DistrictLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long DistrictLanguageMappingInsert(DistrictLanguageMappingModel model)
        {
            var id = _districtlanguagemappingDBService.DistrictLanguageMappingInsert(model);
            return id;
        }

        public void DistrictLanguageMappingUpdate(DistrictLanguageMappingModel model)
        {
            _districtlanguagemappingDBService.DistrictLanguageMappingUpdate(model);
        }

        public List<DistrictLanguageMappingModel> SelectDistrictLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _districtlanguagemappingDBService.SelectDistrictLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
