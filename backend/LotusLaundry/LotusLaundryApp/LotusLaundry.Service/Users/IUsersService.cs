﻿using LotusLaundry.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.RoleMaster;
using LotusLaundry.Domain.UserDevice;

namespace LotusLaundry.Service.Users
{
    public interface IUsersService
    {
        UsersModel SelectUserByUniqueCode(string uniqueCode);
        
        List<UsersModel> SelectUser(SearchParam param);

        List<UsersModel> SelectDeriver(SearchParam param);

        string FileInsert(UsersModel model);

       

        List<RoleMasterModel> SelectAllRoles(SearchParam param);


        List<UsersModel> SelectCustomer(SearchParam param);

        List<UsersModel> SelectProfile(long id);

        void UserDeviceSaveOrUpdate(UserDeviceModel model);

        void UserDeviceDelete(String id);
        List<UserDeviceModel> UserDeviceSelect(Nullable<long> userId, string deviceType=null, string userType=null);

        List<UsersModel> UserBasicInfoSelect(string userIds);

        void SetUserIdle();
    }
}
