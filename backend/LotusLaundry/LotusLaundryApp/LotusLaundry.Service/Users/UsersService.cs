﻿using LotusLaundry.DBRepository.Users;
using LotusLaundry.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Domain;
using LotusLaundry.Service.Xml;
using System.Configuration;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain.RoleMaster;
using LotusLaundry.Domain.UserDevice;

namespace LotusLaundry.Service.Users
{
    public class UsersService : IUsersService
    {

        public UsersDBService _usersDBService;
        private IXmlService _xmlService;
        private readonly string _imageOption;
        private IFileGroupService _fileGroupService;
        public UsersService(IXmlService xmlService, IFileGroupService fileGroupService)
        {
            _xmlService = xmlService;
            _usersDBService = new UsersDBService();
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _fileGroupService = fileGroupService;
        }
        
        public UsersModel SelectUserByUniqueCode(string uniqueCode)
        {
            return _usersDBService.UserProfileByUniqueCode(uniqueCode);
        }
        
        public List<UsersModel> SelectUser(SearchParam param)
        {
            if(param != null)
            {
                if (param.Id != null)
                {
                    param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
                }
                if(param.district!=null)
                {
                    param.districtId= Convert.ToInt64(CryptoEngine.Decrypt(param.district, KeyConstant.Key));
                }
                if (param.subdistrict != null)
                {
                    param.subdistrictId = Convert.ToInt64(CryptoEngine.Decrypt(param.subdistrict, KeyConstant.Key));
                }
                
            }
          
            


            var response = _usersDBService.SelectUser(param);
            response.ForEach(x =>
            {

               
                //  x.AddressDetail = _xmlService.GetUserAddressesByXml(x.addressXml);
                x.AddressDetail = _xmlService.GetCustomerAddressesByXml(x.addressXml);
              
                //GetCustomerAddressesByXml
            });

          int a=  response.Where(x => x.Role == "ADMIN").Count();
           
            return response;


        }

        public List<UsersModel> SelectProfile(long id)
        {
            SearchParam param = new SearchParam();
            param.RecordId = id;

            return _usersDBService.SelectUser(param);
        }


        // List<UsersModel> SelectProfile(Int32 id);
        public List<UsersModel> SelectDeriver(SearchParam param)
        {
            if (param != null)
            {
                if (param.Id != null)
                {
                    param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
                }
            }

           
            var response = _usersDBService.SelectDriver(param);
            response.ForEach(x =>
            {

                x.AllFile = _xmlService.GetFileGroupItemsByXml(x.FileXml);
            });

            if (param.filterOn != null)
                if (param.filterOn == "NAME" && param.filterType == "DESC")
                {
                    response = response.OrderByDescending(x => x.FirstName).ToList();

                }
                else if (param.filterOn == "NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.FirstName).ToList();
                }

                else if (param.filterOn == "NICK_NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.NickName).ToList();
                }
                else if (param.filterOn == "NICK_NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.NickName).ToList();
                }

                else if (param.filterOn == "PHONE_NUMBER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.PhoneNumber).ToList();
                }
                else if (param.filterOn == "PHONE_NUMBER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.PhoneNumber).ToList();
                }

                else if (param.filterOn == "EMAIL" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.Email).ToList();
                }
                else if (param.filterOn == "EMAIL" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.Email).ToList();
                }

                else if (param.filterOn == "EMAIL_VERIFIED" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.EmailConfirmed).ToList();
                }
                else if (param.filterOn == "EMAIL_VERIFIED" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.EmailConfirmed).ToList();
                }

                else if (param.filterOn == "ORDER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }
                else if (param.filterOn == "ORDER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }

                else if (param.filterOn == "LAST_ORDER_DATE" && param.filterType == "ASC")
                {
                    response = response.Where(x => x.lastOrderDate != null).ToList();
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }
                else if (param.filterOn == "LAST_ORDER_DATE" && param.filterType == "ASC")
                {
                    response = response.Where(x => x.lastOrderDate != null).ToList();
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }

                else if (param.filterOn == "EMPLOYEE_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }
                else if (param.filterOn == "EMPLOYEE_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }

                else if (param.filterOn == "STAFF_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }
                else if (param.filterOn == "STAFF_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }

                else if (param.filterOn == "LICENSE_PLATE" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.LicencePlate).ToList();
                }
                else if (param.filterOn == "LICENSE_PLATE" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.LicencePlate).ToList();
                }

                else if (param.filterOn == "STATUS" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.status).ToList();
                }
                else if (param.filterOn == "STATUS" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.status).ToList();
                }

            return response;
        }

        //FileInsert
        public string FileInsert(UsersModel model)
        {
            long id = Convert.ToInt64(model.Id);
            model.CreatedBy = id;
            if (model.AllFile != null)
            {
               
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, id, FileType.PROFILEPIC); 
                }
                //model.AllFile.ForEach(x => x.Type = FileType.PROFILEPIC);
                //model.File.Type = FileType.PROFILEPIC;
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, id, model.AllFile.XmlSerialize());
            }
            return id.ToString();
        }


        //
        public List<RoleMasterModel> SelectAllRoles(SearchParam param)
        {
            if (param != null)
            {
                if (param.Id != null)
                {
                    param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
                }
            }

            //return 
            var response = _usersDBService.SelectAllRoles(param);

            return response;



        }

      

        //SelectCustomer
        public List<UsersModel> SelectCustomer(SearchParam param)
        {
            if (param != null)
            {
                if (param.Id != null)
                {
                    param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
                }
            }
            
            var response = _usersDBService.SelectCustomer(param);
            response.ForEach(x =>
            {

                x.AllFile = _xmlService.GetFileGroupItemsByXml(x.FileXml);
              //  x.AddressDetail = _xmlService.GetUserAddressesByXml(x.addressXml);
                x.AddressDetail = _xmlService.GetCustomerAddressesByXml(x.addressXml);

                //GetCustomerAddressesByXml
            });

            if (param.filterOn != null)
                if (param.filterOn == "NAME" && param.filterType == "DESC")
                {
                    response = response.OrderByDescending(x => x.FirstName).ToList();

                }
                else if (param.filterOn == "NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.FirstName).ToList();
                }

                else if (param.filterOn == "NICK_NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.NickName).ToList();
                }
                else if (param.filterOn == "NICK_NAME" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.NickName).ToList();
                }

                else if (param.filterOn == "PHONE_NUMBER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.PhoneNumber).ToList();
                }
                else if (param.filterOn == "PHONE_NUMBER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.PhoneNumber).ToList();
                }

                else if (param.filterOn == "EMAIL" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.Email).ToList();
                }
                else if (param.filterOn == "EMAIL" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.Email).ToList();
                }

                else if (param.filterOn == "EMAIL_VERIFIED" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.EmailConfirmed).ToList();
                }
                else if (param.filterOn == "EMAIL_VERIFIED" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.EmailConfirmed).ToList();
                }

                else if (param.filterOn == "ORDER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }
                else if (param.filterOn == "ORDER" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }

                else if (param.filterOn == "LAST_ORDER_DATE" && param.filterType == "ASC")
                {
                    response = response.Where(x => x.lastOrderDate != null).ToList();
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }
                else if (param.filterOn == "LAST_ORDER_DATE" && param.filterType == "ASC")
                {
                    response = response.Where(x => x.lastOrderDate != null).ToList();
                    response = response.OrderBy(x => x.ordersCount).ToList();
                }

                else if (param.filterOn == "EMPLOYEE_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }
                else if (param.filterOn == "EMPLOYEE_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }

                else if (param.filterOn == "STAFF_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }
                else if (param.filterOn == "STAFF_ID" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.employeeid).ToList();
                }

                else if (param.filterOn == "LICENSE_PLATE" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.LicencePlate).ToList();
                }
                else if (param.filterOn == "LICENSE_PLATE" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.LicencePlate).ToList();
                }

                else if (param.filterOn == "STATUS" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.status).ToList();
                }
                else if (param.filterOn == "STATUS" && param.filterType == "ASC")
                {
                    response = response.OrderBy(x => x.status).ToList();
                }

            return response;
        }


        public void UserDeviceSaveOrUpdate(UserDeviceModel model)
        {
            _usersDBService.UserDeviceSaveOrUpdate(model);
        }

        public void UserDeviceDelete(String id)
        {
            _usersDBService.UserDeviceDelete(id);
        }

        public List<UserDeviceModel> UserDeviceSelect(Nullable<long> userId, string deviceType=null, string userType=null)
        {
            return _usersDBService.UserDeviceSelect(userId, deviceType, userType);
        }
        //SetUserIdle
        public void SetUserIdle()
        {
            _usersDBService.SetUserIdle();
        }

        public List<UsersModel> UserBasicInfoSelect(string userIds)
        {
            var responseModel = _usersDBService.UserBasicInfoSelect(userIds);
            //responseModel.ForEach(x =>
            //{
            //    x.UserRoles = _xmlService.getusertol(x.RolesXml);
            //});

            return responseModel;
        }

    }
}
