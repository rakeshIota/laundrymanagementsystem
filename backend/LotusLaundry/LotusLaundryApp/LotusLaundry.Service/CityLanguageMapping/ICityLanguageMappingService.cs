﻿using LotusLaundry.Domain.CityLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.CityLanguageMapping
{
    public interface ICityLanguageMappingService
    {
        /// <summary>
        /// used for insertion citylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CityLanguageMappingInsert(CityLanguageMappingModel model);
        
        /// <summary>
        /// Method for update citylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void CityLanguageMappingUpdate(CityLanguageMappingModel model);

        /// <summary>
        /// use to select all citylanguagemapping or select citylanguagemapping by id 
        /// </summary>
        /// <param name="citylanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CityLanguageMappingModel> SelectCityLanguageMapping(SearchParam param);
    }
}
