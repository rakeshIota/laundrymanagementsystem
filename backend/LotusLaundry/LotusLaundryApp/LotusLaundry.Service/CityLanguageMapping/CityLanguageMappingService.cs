﻿using LotusLaundry.DBRepository.CityLanguageMapping;
using LotusLaundry.Domain.CityLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.CityLanguageMapping
{
    public class CityLanguageMappingService : ICityLanguageMappingService
    {	
        public CityLanguageMappingDBService _citylanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CityLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _citylanguagemappingDBService = new CityLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CityLanguageMappingInsert(CityLanguageMappingModel model)
        {
            var id = _citylanguagemappingDBService.CityLanguageMappingInsert(model);
            return id;
        }

        public void CityLanguageMappingUpdate(CityLanguageMappingModel model)
        {
            _citylanguagemappingDBService.CityLanguageMappingUpdate(model);
        }

        public List<CityLanguageMappingModel> SelectCityLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _citylanguagemappingDBService.SelectCityLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
