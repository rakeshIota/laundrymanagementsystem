﻿using LotusLaundry.DBRepository.Country;
using LotusLaundry.Domain.Country;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.CountryLanguageMapping;

namespace LotusLaundry.Service.Country
{
    public class CountryService : ICountryService
    {	
        public CountryDBService _countryDBService;
        public CountryLanguageMappingDBService _countrylanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CountryService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _countryDBService = new CountryDBService();
            _countrylanguagemappingDBService = new CountryLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CountryInsert(CountryModel model)
        {
            var id = _countryDBService.CountryInsert(model);
            if (model.CountryLanguages != null && model.CountryLanguages.Count > 0)
            {
                model.CountryLanguages.ForEach(x =>
                {
                    x.CountryId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _countrylanguagemappingDBService.CountryLanguageMappingXMLSave(model.CountryLanguages.XmlSerialize());
            }
            return id;
        }

        public void CountryUpdate(CountryModel model)
        {
            _countryDBService.CountryUpdate(model);
            if (model.CountryLanguages != null && model.CountryLanguages.Count > 0)
            {
                model.CountryLanguages.ForEach(x =>
                {
                    x.CountryId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _countrylanguagemappingDBService.CountryLanguageMappingXMLSave(model.CountryLanguages.XmlSerialize());
            }
        }

        public List<CountryModel> SelectCountry(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _countryDBService.SelectCountry(param);
			response.ForEach(x =>
            {
                x.CountryLanguages = _xmlService.GetCountryLanguageMappingsByXml(x.CountryLanguageXml);
            });
            return response;
        }
    }
}
