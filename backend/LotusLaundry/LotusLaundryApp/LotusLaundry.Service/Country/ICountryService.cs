﻿using LotusLaundry.Domain.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Country
{
    public interface ICountryService
    {
        /// <summary>
        /// used for insertion country
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CountryInsert(CountryModel model);
        
        /// <summary>
        /// Method for update country
        /// </summary>
        /// <param name="model"></param>
        void CountryUpdate(CountryModel model);

        /// <summary>
        /// use to select all country or select country by id 
        /// </summary>
        /// <param name="countryId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CountryModel> SelectCountry(SearchParam param);
    }
}
