﻿using LotusLaundry.Domain.ProductLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.ProductLanguageMapping
{
    public interface IProductLanguageMappingService
    {
        /// <summary>
        /// used for insertion productlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ProductLanguageMappingInsert(ProductLanguageMappingModel model);
        
        /// <summary>
        /// Method for update productlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void ProductLanguageMappingUpdate(ProductLanguageMappingModel model);

        /// <summary>
        /// use to select all productlanguagemapping or select productlanguagemapping by id 
        /// </summary>
        /// <param name="productlanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ProductLanguageMappingModel> SelectProductLanguageMapping(SearchParam param);
    }
}
