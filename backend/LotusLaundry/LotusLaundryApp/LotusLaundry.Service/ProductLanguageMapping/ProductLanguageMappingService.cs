﻿using LotusLaundry.DBRepository.ProductLanguageMapping;
using LotusLaundry.Domain.ProductLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.ProductLanguageMapping
{
    public class ProductLanguageMappingService : IProductLanguageMappingService
    {	
        public ProductLanguageMappingDBService _productlanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ProductLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _productlanguagemappingDBService = new ProductLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long ProductLanguageMappingInsert(ProductLanguageMappingModel model)
        {
            var id = _productlanguagemappingDBService.ProductLanguageMappingInsert(model);
            return id;
        }

        public void ProductLanguageMappingUpdate(ProductLanguageMappingModel model)
        {
            _productlanguagemappingDBService.ProductLanguageMappingUpdate(model);
        }

        public List<ProductLanguageMappingModel> SelectProductLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _productlanguagemappingDBService.SelectProductLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
