﻿using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.NotificationMasterLanguageMapping
{
    public interface INotificationMasterLanguageMappingService
    {
        /// <summary>
        /// used for insertion notificationmasterlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long NotificationMasterLanguageMappingInsert(NotificationMasterLanguageMappingModel model);
        
        /// <summary>
        /// Method for update notificationmasterlanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void NotificationMasterLanguageMappingUpdate(NotificationMasterLanguageMappingModel model);

        /// <summary>
        /// use to select all notificationmasterlanguagemapping or select notificationmasterlanguagemapping by id 
        /// </summary>
        /// <param name="notificationmasterlanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<NotificationMasterLanguageMappingModel> SelectNotificationMasterLanguageMapping(SearchParam param);
    }
}
