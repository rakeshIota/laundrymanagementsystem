﻿using LotusLaundry.DBRepository.NotificationMasterLanguageMapping;
using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.NotificationMasterLanguageMapping
{
    public class NotificationMasterLanguageMappingService : INotificationMasterLanguageMappingService
    {	
        public NotificationMasterLanguageMappingDBService _notificationmasterlanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public NotificationMasterLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _notificationmasterlanguagemappingDBService = new NotificationMasterLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long NotificationMasterLanguageMappingInsert(NotificationMasterLanguageMappingModel model)
        {
            var id = _notificationmasterlanguagemappingDBService.NotificationMasterLanguageMappingInsert(model);
            return id;
        }

        public void NotificationMasterLanguageMappingUpdate(NotificationMasterLanguageMappingModel model)
        {
            _notificationmasterlanguagemappingDBService.NotificationMasterLanguageMappingUpdate(model);
        }

        public List<NotificationMasterLanguageMappingModel> SelectNotificationMasterLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _notificationmasterlanguagemappingDBService.SelectNotificationMasterLanguageMapping(param);
			response.ForEach(x =>
            {
             x.LanguageIdDetail = _xmlService.GetLanguagesByXml(x.LanguageXml).FirstOrDefault();
            });
            return response;
        }
    }
}
