﻿using LotusLaundry.DBRepository.PostalCode;
using LotusLaundry.Domain.PostalCode;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.PostalCode
{
    public class PostalCodeService : IPostalCodeService
    {	
        public PostalCodeDBService _postalcodeDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PostalCodeService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _postalcodeDBService = new PostalCodeDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long PostalCodeInsert(PostalCodeModel model)
        {
            var id = _postalcodeDBService.PostalCodeInsert(model);
            return id;
        }

        public void PostalCodeUpdate(PostalCodeModel model)
        {
            _postalcodeDBService.PostalCodeUpdate(model);
        }

        public List<PostalCodeModel> SelectPostalCode(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _postalcodeDBService.SelectPostalCode(param);
			response.ForEach(x =>
            {
            });
            return response;
        }

        //ValidatePostalCode

        public List<PostalCodeModel> ValidatePostalCode(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _postalcodeDBService.ValidatePostalCode(param);
            
            return response;
        }
    }
}
