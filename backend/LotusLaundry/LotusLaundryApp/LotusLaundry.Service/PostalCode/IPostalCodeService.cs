﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.PostalCode
{
    public interface IPostalCodeService
    {
        /// <summary>
        /// used for insertion postalcode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PostalCodeInsert(PostalCodeModel model);
        
        /// <summary>
        /// Method for update postalcode
        /// </summary>
        /// <param name="model"></param>
        void PostalCodeUpdate(PostalCodeModel model);

        /// <summary>
        /// use to select all postalcode or select postalcode by id 
        /// </summary>
        /// <param name="postalcodeId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PostalCodeModel> SelectPostalCode(SearchParam param);

        List<PostalCodeModel> ValidatePostalCode(SearchParam param);
    }
}
