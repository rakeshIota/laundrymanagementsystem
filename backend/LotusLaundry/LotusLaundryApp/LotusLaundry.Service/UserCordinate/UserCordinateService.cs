﻿using LotusLaundry.DBRepository.UserCordinate;
using LotusLaundry.Domain.UserCordinate;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.UserCordinate
{
    public class UserCordinateService : IUserCordinateService
    {	
        public UserCordinateDBService _usercordinateDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public UserCordinateService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _usercordinateDBService = new UserCordinateDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long UserCordinateInsert(UserCordinateModel model)
        {
            var id = _usercordinateDBService.UserCordinateInsert(model);
            return id;
        }

        public void UserCordinateUpdate(UserCordinateModel model)
        {
            _usercordinateDBService.UserCordinateUpdate(model);
        }

        public List<UserCordinateModel> SelectUserCordinate(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _usercordinateDBService.SelectUserCordinate(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
