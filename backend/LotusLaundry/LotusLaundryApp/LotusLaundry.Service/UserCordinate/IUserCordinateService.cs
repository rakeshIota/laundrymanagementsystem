﻿using LotusLaundry.Domain.UserCordinate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.UserCordinate
{
    public interface IUserCordinateService
    {
        /// <summary>
        /// used for insertion usercordinate
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long UserCordinateInsert(UserCordinateModel model);
        
        /// <summary>
        /// Method for update usercordinate
        /// </summary>
        /// <param name="model"></param>
        void UserCordinateUpdate(UserCordinateModel model);

        /// <summary>
        /// use to select all usercordinate or select usercordinate by id 
        /// </summary>
        /// <param name="usercordinateId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<UserCordinateModel> SelectUserCordinate(SearchParam param);
    }
}
