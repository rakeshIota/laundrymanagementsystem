﻿using LotusLaundry.Domain.CartItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.CartItemServiceMapping
{
    public interface ICartItemServiceMappingService
    {
        /// <summary>
        /// used for insertion cartitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CartItemServiceMappingInsert(CartItemServiceMappingModel model);
        
        /// <summary>
        /// Method for update cartitemservicemapping
        /// </summary>
        /// <param name="model"></param>
        void CartItemServiceMappingUpdate(CartItemServiceMappingModel model);

        /// <summary>
        /// use to select all cartitemservicemapping or select cartitemservicemapping by id 
        /// </summary>
        /// <param name="cartitemservicemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CartItemServiceMappingModel> SelectCartItemServiceMapping(SearchParam param);
    }
}
