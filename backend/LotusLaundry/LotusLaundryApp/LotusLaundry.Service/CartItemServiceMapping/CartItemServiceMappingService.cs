﻿using LotusLaundry.DBRepository.CartItemServiceMapping;
using LotusLaundry.Domain.CartItemServiceMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.CartItemServiceMapping
{
    public class CartItemServiceMappingService : ICartItemServiceMappingService
    {	
        public CartItemServiceMappingDBService _cartitemservicemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CartItemServiceMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _cartitemservicemappingDBService = new CartItemServiceMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CartItemServiceMappingInsert(CartItemServiceMappingModel model)
        {
            var id = _cartitemservicemappingDBService.CartItemServiceMappingInsert(model);
            return id;
        }

        public void CartItemServiceMappingUpdate(CartItemServiceMappingModel model)
        {
            _cartitemservicemappingDBService.CartItemServiceMappingUpdate(model);
        }

        public List<CartItemServiceMappingModel> SelectCartItemServiceMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _cartitemservicemappingDBService.SelectCartItemServiceMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
