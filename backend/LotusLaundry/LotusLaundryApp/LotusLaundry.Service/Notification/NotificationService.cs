﻿using LotusLaundry.DBRepository.Notification;
using LotusLaundry.Domain.Notification;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.Notification
{
    public class NotificationService : INotificationService
    {	
        public NotificationDBService _notificationDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public NotificationService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _notificationDBService = new NotificationDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long NotificationInsert(NotificationModel model)
        {
            var id = _notificationDBService.NotificationInsert(model);
            return id;
        }

        public void NotificationUpdate(NotificationModel model)
        {
            _notificationDBService.NotificationUpdate(model);
        }
      

        public List<NotificationModel> SelectNotification(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _notificationDBService.SelectNotification(param);
			response.ForEach(x =>
            {
            });
            return response;
        }

        //public Task NotificationInsertAsync(NotificationModel model)
        //{
        //    var userDevices = NotificationSelectForUser(model.UserId);

        //    Task.Run(() => SendNotificationByFCM(new FCMNotificationModel
        //    {
        //        DeviceToken = userDevices != null ? userDevices.DeviceList : null,
        //        TargetId = model.TargetId,
        //        Message = model.Message,
        //        Title = model.Title,
        //        Type = model.TargetType,
        //    }));

        //    return Task.Run(() => _notificationDBService.NotificationInsert(model));
        //}
    }
}
