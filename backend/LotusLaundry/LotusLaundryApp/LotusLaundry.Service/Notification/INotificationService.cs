﻿using LotusLaundry.Domain.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Notification
{
    public interface INotificationService
    {
        /// <summary>
        /// used for insertion notification
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long NotificationInsert(NotificationModel model);
        
        /// <summary>
        /// Method for update notification
        /// </summary>
        /// <param name="model"></param>
        void NotificationUpdate(NotificationModel model);

        /// <summary>
        /// use to select all notification or select notification by id 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<NotificationModel> SelectNotification(SearchParam param);
    }
}
