﻿using LotusLaundry.Domain.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Packing
{
    public interface IPackingService
    {
        /// <summary>
        /// used for insertion packing
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PackingInsert(PackingModel model);
        
        /// <summary>
        /// Method for update packing
        /// </summary>
        /// <param name="model"></param>
        void PackingUpdate(PackingModel model);

        /// <summary>
        /// use to select all packing or select packing by id 
        /// </summary>
        /// <param name="packingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PackingModel> SelectPacking(SearchParam param);
    }
}
