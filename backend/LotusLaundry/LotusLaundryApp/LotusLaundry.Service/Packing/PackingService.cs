﻿using LotusLaundry.DBRepository.Packing;
using LotusLaundry.Domain.Packing;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.Packing
{
    public class PackingService : IPackingService
    {	
        public PackingDBService _packingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PackingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _packingDBService = new PackingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long PackingInsert(PackingModel model)
        {
            var id = _packingDBService.PackingInsert(model);
            return id;
        }

        public void PackingUpdate(PackingModel model)
        {
            _packingDBService.PackingUpdate(model);
        }

        public List<PackingModel> SelectPacking(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _packingDBService.SelectPacking(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
