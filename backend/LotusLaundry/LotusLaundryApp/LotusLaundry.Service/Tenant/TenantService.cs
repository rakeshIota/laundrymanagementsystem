﻿using LotusLaundry.DBRepository.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Tenant;

namespace LotusLaundry.Service.Tenant
{
    public class TenantService:ITenantService
    {
        private TenantDBService _tenantDBService;
        public TenantService()
        {
            _tenantDBService = new TenantDBService();
        }

        public TenantModel TenantSelect(Guid uniqueId)
        {
            return _tenantDBService.TenantSelect(uniqueId);
        }
    }
}
