﻿using LotusLaundry.Domain.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Service.Tenant
{
    public interface ITenantService
    {
        TenantModel TenantSelect(Guid uniqueId);
    }
}
