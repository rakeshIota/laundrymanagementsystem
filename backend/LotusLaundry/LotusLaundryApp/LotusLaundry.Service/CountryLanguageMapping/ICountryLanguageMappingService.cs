﻿using LotusLaundry.Domain.CountryLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.CountryLanguageMapping
{
    public interface ICountryLanguageMappingService
    {
        /// <summary>
        /// used for insertion countrylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CountryLanguageMappingInsert(CountryLanguageMappingModel model);
        
        /// <summary>
        /// Method for update countrylanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void CountryLanguageMappingUpdate(CountryLanguageMappingModel model);

        /// <summary>
        /// use to select all countrylanguagemapping or select countrylanguagemapping by id 
        /// </summary>
        /// <param name="countrylanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CountryLanguageMappingModel> SelectCountryLanguageMapping(SearchParam param);
    }
}
