﻿using LotusLaundry.DBRepository.CountryLanguageMapping;
using LotusLaundry.Domain.CountryLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.CountryLanguageMapping
{
    public class CountryLanguageMappingService : ICountryLanguageMappingService
    {	
        public CountryLanguageMappingDBService _countrylanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public CountryLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _countrylanguagemappingDBService = new CountryLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long CountryLanguageMappingInsert(CountryLanguageMappingModel model)
        {
            var id = _countrylanguagemappingDBService.CountryLanguageMappingInsert(model);
            return id;
        }

        public void CountryLanguageMappingUpdate(CountryLanguageMappingModel model)
        {
            _countrylanguagemappingDBService.CountryLanguageMappingUpdate(model);
        }

        public List<CountryLanguageMappingModel> SelectCountryLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _countrylanguagemappingDBService.SelectCountryLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
