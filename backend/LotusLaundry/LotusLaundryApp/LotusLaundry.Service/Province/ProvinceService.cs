﻿using LotusLaundry.DBRepository.Province;
using LotusLaundry.Domain.Province;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.ProvinceLanguageMapping;

namespace LotusLaundry.Service.Province
{
    public class ProvinceService : IProvinceService
    {
        public ProvinceDBService _provinceDBService;
        public ProvinceLanguageMappingDBService _provinceLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ProvinceService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _provinceDBService = new ProvinceDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _provinceLanguageMappingDBService = new ProvinceLanguageMappingDBService();


        }
        
        public long ProvinceInsert(ProvinceModel model)
        {
            var id = _provinceDBService.ProvinceInsert(model);
            if (model.ProvinceLanguages != null && model.ProvinceLanguages.Count > 0)
            {
                model.ProvinceLanguages.ForEach(x =>
                {
                    x.ProvinceId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _provinceLanguageMappingDBService.ProvinceLanguageMappingXMLSave(model.ProvinceLanguages.XmlSerialize());
            }
            return id;
        }

        public void ProvinceUpdate(ProvinceModel model)
        {
            _provinceDBService.ProvinceUpdate(model);
            if (model.ProvinceLanguages != null && model.ProvinceLanguages.Count > 0)
            {
                model.ProvinceLanguages.ForEach(x =>
                {
                    x.ProvinceId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _provinceLanguageMappingDBService.ProvinceLanguageMappingXMLSave(model.ProvinceLanguages.XmlSerialize());
            }
        }

        public List<ProvinceModel> SelectProvince(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _provinceDBService.SelectProvince(param);
			response.ForEach(x =>
            {
                x.ProvinceLanguages = _xmlService.GetProvinceLanguageMappingsByXml(x.ProvinceLanguageXml);
            });
            return response;
        }
    }
}
