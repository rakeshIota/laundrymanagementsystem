﻿using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Province
{
    public interface IProvinceService
    {
        /// <summary>
        /// used for insertion province
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ProvinceInsert(ProvinceModel model);
        
        /// <summary>
        /// Method for update province
        /// </summary>
        /// <param name="model"></param>
        void ProvinceUpdate(ProvinceModel model);

        /// <summary>
        /// use to select all province or select province by id 
        /// </summary>
        /// <param name="provinceId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ProvinceModel> SelectProvince(SearchParam param);
    }
}
