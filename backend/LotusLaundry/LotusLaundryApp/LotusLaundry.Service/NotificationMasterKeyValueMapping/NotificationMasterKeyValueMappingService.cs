﻿using LotusLaundry.DBRepository.NotificationMasterKeyValueMapping;
using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.NotificationMasterKeyValueMapping
{
    public class NotificationMasterKeyValueMappingService : INotificationMasterKeyValueMappingService
    {	
        public NotificationMasterKeyValueMappingDBService _notificationmasterkeyvaluemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public NotificationMasterKeyValueMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _notificationmasterkeyvaluemappingDBService = new NotificationMasterKeyValueMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long NotificationMasterKeyValueMappingInsert(NotificationMasterKeyValueMappingModel model)
        {
            var id = _notificationmasterkeyvaluemappingDBService.NotificationMasterKeyValueMappingInsert(model);
            return id;
        }

        public void NotificationMasterKeyValueMappingUpdate(NotificationMasterKeyValueMappingModel model)
        {
            _notificationmasterkeyvaluemappingDBService.NotificationMasterKeyValueMappingUpdate(model);
        }

        public List<NotificationMasterKeyValueMappingModel> SelectNotificationMasterKeyValueMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _notificationmasterkeyvaluemappingDBService.SelectNotificationMasterKeyValueMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
