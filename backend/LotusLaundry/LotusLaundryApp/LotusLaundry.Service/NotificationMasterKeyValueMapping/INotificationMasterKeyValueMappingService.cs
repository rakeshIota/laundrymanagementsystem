﻿using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.NotificationMasterKeyValueMapping
{
    public interface INotificationMasterKeyValueMappingService
    {
        /// <summary>
        /// used for insertion notificationmasterkeyvaluemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long NotificationMasterKeyValueMappingInsert(NotificationMasterKeyValueMappingModel model);
        
        /// <summary>
        /// Method for update notificationmasterkeyvaluemapping
        /// </summary>
        /// <param name="model"></param>
        void NotificationMasterKeyValueMappingUpdate(NotificationMasterKeyValueMappingModel model);

        /// <summary>
        /// use to select all notificationmasterkeyvaluemapping or select notificationmasterkeyvaluemapping by id 
        /// </summary>
        /// <param name="notificationmasterkeyvaluemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<NotificationMasterKeyValueMappingModel> SelectNotificationMasterKeyValueMapping(SearchParam param);
    }
}
