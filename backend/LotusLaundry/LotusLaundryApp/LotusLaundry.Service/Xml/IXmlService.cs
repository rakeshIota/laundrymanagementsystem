﻿using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.Feedback;
using LotusLaundry.Domain.UserDevice;
using LotusLaundry.Domain.PasswordLog;
using LotusLaundry.Domain.MessageLog;
using LotusLaundry.Domain.Notification;
using LotusLaundry.Domain.AppSetting;
using LotusLaundry.Domain.Country;
using LotusLaundry.Domain.CountryLanguageMapping;
using LotusLaundry.Domain.Language;
using LotusLaundry.Domain.State;
using LotusLaundry.Domain.StateLanguageMapping;
using LotusLaundry.Domain.City;
using LotusLaundry.Domain.CityLanguageMapping;
using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Domain.District;
using LotusLaundry.Domain.DistrictLanguageMapping;
using LotusLaundry.Domain.SubDistrict;
using LotusLaundry.Domain.SubDistrictLanguageMapping;
using LotusLaundry.Domain.Province;
using LotusLaundry.Domain.ProvinceLanguageMapping;
using LotusLaundry.Domain.UserCordinate;
using LotusLaundry.Domain.Service;
using LotusLaundry.Domain.ServiceLanguageMapping;
using LotusLaundry.Domain.Product;
using LotusLaundry.Domain.ProductLanguageMapping;
using LotusLaundry.Domain.ProductPrice;
using LotusLaundry.Domain.DeliveryType;
using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using LotusLaundry.Domain.Cart;
using LotusLaundry.Domain.CartItem;
using LotusLaundry.Domain.CartItemServiceMapping;
using LotusLaundry.Domain.Order;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Domain.OrderItemServiceMapping;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Domain.Packing;
using LotusLaundry.Domain.PackingLanguageMapping;
using LotusLaundry.Domain.DefaultMessage;
using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using LotusLaundry.Domain.Currency;
using LotusLaundry.Domain.CurrencyLanguageMapping;//@@ADD_NEW_NAMESPACE_XML_CODE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Users;
using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using LotusLaundry.Domain.NotificationMaster;

namespace LotusLaundry.Service.Xml
{
    public interface IXmlService
    {
        List<FileGroupItemsModel> GetFileGroupItemsByXml(string attachmentFileXml);
        List<UsersModel> GetUsersByXml(string attachmentFileXml);

       // List<UserRolesModel> GetUserRolesByXml(string userRolesXml);
        List<UsersModel> GetDriverByXml(string attachmentFileXml);


        List<FeedbackModel> GetFeedbacksByXml(string attachmentFileXml); List<UserDeviceModel> GetUserDevicesByXml(string attachmentFileXml); List<PasswordLogModel> GetPasswordLogsByXml(string attachmentFileXml); List<MessageLogModel> GetMessageLogsByXml(string attachmentFileXml); List<NotificationModel> GetNotificationsByXml(string attachmentFileXml); List<AppSettingModel> GetAppSettingsByXml(string attachmentFileXml); List<CountryModel> GetCountriesByXml(string attachmentFileXml); List<CountryLanguageMappingModel> GetCountryLanguageMappingsByXml(string attachmentFileXml); List<LanguageModel> GetLanguagesByXml(string attachmentFileXml); List<StateModel> GetStatesByXml(string attachmentFileXml); List<StateLanguageMappingModel> GetStateLanguageMappingsByXml(string attachmentFileXml); List<CityModel> GetCitiesByXml(string attachmentFileXml); List<CityLanguageMappingModel> GetCityLanguageMappingsByXml(string attachmentFileXml);
        List<UserAddressModel> GetUserAddressesByXml(string attachmentFileXml);
        List<UserAddressModel> GetCustomerAddressesByXml(string attachmentFileXml);

        List<DistrictModel> GetDistrictsByXml(string attachmentFileXml); List<DistrictLanguageMappingModel> GetDistrictLanguageMappingsByXml(string attachmentFileXml); List<SubDistrictModel> GetSubDistrictsByXml(string attachmentFileXml); List<SubDistrictLanguageMappingModel> GetSubDistrictLanguageMappingsByXml(string attachmentFileXml); List<ProvinceModel> GetProvincesByXml(string attachmentFileXml); List<ProvinceLanguageMappingModel> GetProvinceLanguageMappingsByXml(string attachmentFileXml); List<UserCordinateModel> GetUserCordinatesByXml(string attachmentFileXml); List<ServiceModel> GetServicesByXml(string attachmentFileXml); List<ServiceLanguageMappingModel> GetServiceLanguageMappingsByXml(string attachmentFileXml); List<ProductModel> GetProductsByXml(string attachmentFileXml); List<ProductLanguageMappingModel> GetProductLanguageMappingsByXml(string attachmentFileXml); List<ProductPriceModel> GetProductPricesByXml(string attachmentFileXml); List<DeliveryTypeModel> GetDeliveryTypeByXml(string attachmentFileXml); List<DeliveryTypeLanguageMappingModel> GetDeliveryTypeLanguageMappingsByXml(string attachmentFileXml); List<CartModel> GetCartByXml(string attachmentFileXml);
        List<CartItemModel> GetCartItemsByXml(string attachmentFileXml);

        List<CartItemServiceMappingModel> GetCartItemServiceMappingsByXml(string attachmentFileXml); List<OrderModel> GetOrdersByXml(string attachmentFileXml);
        List<OrderItemModel> GetOrderItemsByXml(string attachmentFileXml);

        List<OrderItemServiceMappingModel> GetOrderItemServiceMappingsByXml(string attachmentFileXml); List<OrderLogModel> GetOrderLogsByXml(string attachmentFileXml); List<PaymentLogModel> GetPaymentLogsByXml(string attachmentFileXml); List<PackingModel> GetPackingsByXml(string attachmentFileXml); List<PackingLanguageMappingModel> GetPackingLanguageMappingsByXml(string attachmentFileXml); List<DefaultMessageModel> GetDefaultMessagesByXml(string attachmentFileXml); List<DefaultMessageLanguageMappingModel> GetDefaultMessageLanguageMappingsByXml(string attachmentFileXml); List<CurrencyModel> GetCurrencyByXml(string attachmentFileXml); List<CurrencyLanguageMappingModel> GetCurrencyLanguageMappingsByXml(string attachmentFileXml); //@@ADD_NEW_XML_CODE


        List<NotificationMasterKeyValueMappingModel> GetNotificationMasterKeyValueMappingsByXml(string attachmentFileXml);
        List<NotificationMasterLanguageMappingModel> GetNotificationMasterLanguageMappingsByXml(string attachmentFileXml);
        List<NotificationMasterModel> GetNotificationMastersByXml(string attachmentFileXml);
    }
}










































