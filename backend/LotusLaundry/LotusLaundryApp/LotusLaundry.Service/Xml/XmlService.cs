﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Upload;
using System.Xml.Linq;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain.Users;
using LotusLaundry.Domain.Feedback;
using LotusLaundry.Domain.UserDevice;
using LotusLaundry.Domain.PasswordLog;
using LotusLaundry.Domain.MessageLog;
using LotusLaundry.Domain.Notification;
using LotusLaundry.Domain.AppSetting;
using LotusLaundry.Domain.Country;
using LotusLaundry.Domain.CountryLanguageMapping;
using LotusLaundry.Domain.Language;
using LotusLaundry.Domain.State;
using LotusLaundry.Domain.StateLanguageMapping;
using LotusLaundry.Domain.City;
using LotusLaundry.Domain.CityLanguageMapping;
using LotusLaundry.Domain.UserAddress;
using LotusLaundry.Domain.District;
using LotusLaundry.Domain.DistrictLanguageMapping;
using LotusLaundry.Domain.SubDistrict;
using LotusLaundry.Domain.SubDistrictLanguageMapping;
using LotusLaundry.Domain.Province;
using LotusLaundry.Domain.ProvinceLanguageMapping;
using LotusLaundry.Domain.UserCordinate;
using LotusLaundry.Domain.Service;
using LotusLaundry.Domain.ServiceLanguageMapping;
using LotusLaundry.Domain.Product;
using LotusLaundry.Domain.ProductLanguageMapping;
using LotusLaundry.Domain.ProductPrice;
using LotusLaundry.Domain.DeliveryType;
using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using LotusLaundry.Domain.Cart;
using LotusLaundry.Domain.CartItem;
using LotusLaundry.Domain.CartItemServiceMapping;
using LotusLaundry.Domain.Order;
using LotusLaundry.Domain.OrderItem;
using LotusLaundry.Domain.OrderItemServiceMapping;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Domain.Packing;
using LotusLaundry.Domain.PackingLanguageMapping;
using LotusLaundry.Domain.DefaultMessage;
using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using LotusLaundry.Domain.Currency;
using LotusLaundry.Domain.CurrencyLanguageMapping;//@@ADD_NEW_NAMESPACE_XML_CODE
using LotusLaundry.Domain.NotificationMaster;
using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using LotusLaundry.Domain.NotificationMasterKeyValueMapping;

namespace LotusLaundry.Service.Xml
{
    public class XmlService : IXmlService
    {
        #region Attachement file XML to List

        public List<FileGroupItemsModel> GetFileGroupItemsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<FileGroupItemsModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("FG").Select(x => new FileGroupItemsModel
            {
                Id = x.Element("Id").GetValue<long>(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                Filename = x.Element("Filename").GetStrValue(),
                MimeType = x.Element("MimeType").GetStrValue(),
                Thumbnail = x.Element("Thumbnail").GetStrValue(),
                Path = x.Element("Path").GetStrValue(),
                OriginalName = x.Element("OriginalName").GetStrValue(),
                OnServer = x.Element("OnServer").GetStrValue(),
                TypeId = x.Element("TypeId").GetValue<long>(),
                Type = x.Element("Type").GetStrValue(),
                Size = x.Element("Size").GetValue<long>(),
            }).ToList();
        }

        public List<UsersModel> GetUsersByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UsersModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("users").Select(x => new UsersModel
            {
                Id = x.Element("Id").GetValue<long>(),
                Email = x.Element("Email").GetStrValue(),
                PhoneNumber = x.Element("PhoneNumber").GetStrValue(),
                UserName = x.Element("UserName").GetStrValue(),
                FirstName = x.Element("FirstName").GetStrValue(),
                LastName = x.Element("LastName").GetStrValue(),
                
            }).ToList();
        }
        // List<UsersModel> GetDriverByXml(string attachmentFileXml);
        public List<UsersModel> GetDriverByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UsersModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("users").Select(x => new UsersModel
            {
                Id = x.Element("Id").GetValue<long>(),
                Email = x.Element("Email").GetStrValue(),
                PhoneNumber = x.Element("PhoneNumber").GetStrValue(),
                UserName = x.Element("UserName").GetStrValue(),
                FirstName = x.Element("FirstName").GetStrValue(),
                LastName = x.Element("LastName").GetStrValue(),
                BikeInformation=x.Element("BikeInformation").GetStrValue(),
                NationalId=x.Element("NationalId").GetStrValue(),
                LicenceId = x.Element("LicenceId").GetStrValue(),
                LicencePlate=x.Element("LicencePlate").GetStrValue(),
                ProfileImageUrl=x.Element("profilePic").GetStrValue(),
                Latitude = x.Element("DriverLocation").Element("Latitude").GetValue<decimal>(),
                Longititude =x.Element("DriverLocation").Element("Longititude").GetValue<decimal>(),

            }).ToList();
        }
        public List<FeedbackModel> GetFeedbacksByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<FeedbackModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("feedback").Select(x => new FeedbackModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                OrderId = x.Element("OrderId").GetValue<long>(),
                Type = x.Element("Type").GetStrValue(),
                Message = x.Element("Message").GetStrValue(),
                Rating = x.Element("Rating").GetValue<long>(),
                DriverId = x.Element("DriverId").GetValue<long>(),
                UserId = x.Element("UserId").GetValue<long>(),
            }).ToList();
        }

        public List<UserDeviceModel> GetUserDevicesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UserDeviceModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("userdevice").Select(x => new UserDeviceModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                DeviceId = x.Element("DeviceId").GetStrValue(),
                NotificationToken = x.Element("NotificationToken").GetStrValue(),
            }).ToList();
        }

        public List<PasswordLogModel> GetPasswordLogsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<PasswordLogModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("passwordlog").Select(x => new PasswordLogModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                Count = x.Element("Count").GetValue<int>(),
            }).ToList();
        }

        public List<MessageLogModel> GetMessageLogsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<MessageLogModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("messagelog").Select(x => new MessageLogModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                Message = x.Element("Message").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Type = x.Element("Type").GetStrValue(),
            }).ToList();
        }

        public List<NotificationModel> GetNotificationsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<NotificationModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("notification").Select(x => new NotificationModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                Message = x.Element("Message").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Type = x.Element("Type").GetStrValue(),
                Token = x.Element("Token").GetStrValue(),
                Status = x.Element("Status").GetStrValue(),
                Response = x.Element("Response").GetStrValue(),
            }).ToList();
        }

        public List<AppSettingModel> GetAppSettingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<AppSettingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("appsetting").Select(x => new AppSettingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Key = x.Element("Key").GetStrValue(),
                Value = x.Element("Value").GetStrValue(),
            }).ToList();
        }

        public List<CountryModel> GetCountriesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CountryModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("country").Select(x => new CountryModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
            }).ToList();
        }

        public List<CountryLanguageMappingModel> GetCountryLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CountryLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("countrylanguagemapping").Select(x => new CountryLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                CountryId = x.Element("CountryId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<LanguageModel> GetLanguagesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<LanguageModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("language").Select(x => new LanguageModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
            }).ToList();
        }

        public List<StateModel> GetStatesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<StateModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("state").Select(x => new StateModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                CountryId = x.Element("CountryId").GetValue<long>(),
            }).ToList();
        }

        public List<StateLanguageMappingModel> GetStateLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<StateLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("statelanguagemapping").Select(x => new StateLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                StateId = x.Element("StateId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()

            }).ToList();
        }

        public List<CityModel> GetCitiesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CityModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("city").Select(x => new CityModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                StateId = x.Element("StateId").GetValue<long>()
            }).ToList();
        }

        public List<CityLanguageMappingModel> GetCityLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CityLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("citylanguagemapping").Select(x => new CityLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                CityId = x.Element("CityId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        //public List<UserAddressModel> GetUserAddressesByXml(string attachmentFileXml)
        //{
        //    if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UserAddressModel>();
        //    return XDocument.Parse(attachmentFileXml).Element("root").Elements("useraddress").Select(x => new UserAddressModel
        //    {
        //        Id = x.Element("Id").GetValue<long>(),
        //        TenantId = x.Element("TenantId").GetValue<int>(),
        //        Slug = x.Element("Slug").GetStrValue(),
        //        CreatedBy = x.Element("CreatedBy").GetValue<long>(),
        //        UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
        //        CreatedOn = x.Element("CreatedOn").GetDateOffset(),
        //        UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
        //        IsDeleted = x.Element("IsDeleted").GetBoolVal(),
        //        IsActive = x.Element("IsActive").GetBoolVal(),
        //        UserId = x.Element("UserId").GetValue<long>(),

        //        DistrictId = x.Element("DistrictId").GetValue<long>(),
        //        SubDistrictId = x.Element("SubDistrictId").GetValue<long>(),
        //        PostalCode = x.Element("ZipCode").GetStrValue(),

        //        ProvinceId = x.Element("ProvinceId").GetValue<long>(),
        //        Latitude = x.Element("Latitude").GetValue<decimal>(),
        //        Longitude = x.Element("Longitude").GetValue<decimal>(),

        //        AddressLine1 = x.Element("AddressLine1").GetStrValue(),
        //        AddressLine2 = x.Element("AddressLine2").GetStrValue(),
        //        Type = x.Element("Type").GetStrValue(),


        //        HouseNumber = x.Element("HouseNumber").GetStrValue(),
        //        StreetNumber = x.Element("StreetNumber").GetStrValue(),
        //        Note = x.Element("Note").GetStrValue(),
        //        BuildingName = x.Element("BuildingName").GetStrValue(),
        //        Floor = x.Element("Floor").GetStrValue(),
        //        UnitNo = x.Element("UnitNo").GetStrValue(),
        //        PhoneNo = x.Element("PhoneNo").GetStrValue(),

        //        Ext = x.Element("PhoneExt").GetStrValue(),
        //        ResidenceType = x.Element("ResidenceType").GetStrValue(),

        //        IsDefault = x.Element("IsDefault").GetBoolVal(),
        //        Tag = x.Element("Tag").GetStrValue(),

        //    }).ToList();
        //}

        public List<UserAddressModel> GetUserAddressesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UserAddressModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("R").Select(x => new UserAddressModel
            {
                //DistrictName=  x.Element("D").Element("DistrictName").GetStrValue(),
                //SubDistrictName = x.Element("D").Element("SD").Element("SubDistrictName").GetStrValue(),
                //DistrictNameL = x.Element("D").Element("SD").Element("DistrictNameL").GetStrValue(),
                //SubDistrictNameL = x.Element("D").Element("SD").Element("SubDistrictNameL").GetStrValue(),

                DistrictName = x.Element("PD").Element("D").Element("DistrictName").GetStrValue(),
                SubDistrictName = x.Element("PD").Element("D").Element("SD").Element("SubDistrictName").GetStrValue(),
                DistrictNameL = x.Element("PD").Element("D").Element("SD").Element("DistrictNameL").GetStrValue(),
                SubDistrictNameL = x.Element("PD").Element("D").Element("SD").Element("SubDistrictNameL").GetStrValue(),

                ProvinceName = x.Element("PD").Element("ProvinceName").GetStrValue(),

                ProvinceNameL = x.Element("PD").Element("D").Element("SD").Element("ProvinceNameL").GetStrValue(),

                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),

                DistrictId = x.Element("DistrictId").GetValue<long>(),
                SubDistrictId = x.Element("SubDistrictId").GetValue<long>(),
                //PostalCode = x.Element("ZipCode").GetStrValue(),
                PostalCode = x.Element("PostalCode").GetStrValue(),

                ProvinceId = x.Element("ProvinceId").GetValue<long>(),
                Latitude = x.Element("Latitude").GetValue<decimal>(),
                Longitude = x.Element("Longitude").GetValue<decimal>(),

                AddressLine1 = x.Element("AddressLine1").GetStrValue(),
                AddressLine2 = x.Element("AddressLine2").GetStrValue(),
                Type=x.Element("Type").GetStrValue(),


                HouseNumber = x.Element("HouseNumber").GetStrValue(),
                StreetNumber = x.Element("StreetNumber").GetStrValue(),
                Note = x.Element("Note").GetStrValue(),
                BuildingName = x.Element("BuildingName").GetStrValue(),
                Floor = x.Element("Floor").GetStrValue(),
                UnitNo = x.Element("UnitNo").GetStrValue(),
                PhoneNo = x.Element("PhoneNo").GetStrValue(),

                alterNumber = x.Element("alterNumber").GetStrValue(),
                ResidenceType = x.Element("ResidenceType").GetStrValue(),

                IsDefault = x.Element("IsDefault").GetBoolVal(),
                Tag = x.Element("Tag").GetStrValue(),

            }).ToList();
        }


        //GetCustomerAddressesByXml

        public List<UserAddressModel> GetCustomerAddressesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UserAddressModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("R").Select(x => new UserAddressModel
            {
                DistrictName=  x.Element("D").Element("DistrictName").GetStrValue(),
                SubDistrictName = x.Element("D").Element("SD").Element("SubDistrictName").GetStrValue(),
                
                DistrictNameL =    x.Element("D").Element("SD").Element("PD").Element("DistrictNameL").GetStrValue(),
                SubDistrictNameL = x.Element("D").Element("SD").Element("PD").Element("SubDistrictNameL").GetStrValue(),

                //DistrictName = x.Element("PD").Element("D").Element("DistrictName").GetStrValue(),
                //SubDistrictName = x.Element("PD").Element("D").Element("SD").Element("SubDistrictName").GetStrValue(),
                //DistrictNameL = x.Element("PD").Element("D").Element("SD").Element("DistrictNameL").GetStrValue(),
                //SubDistrictNameL = x.Element("PD").Element("D").Element("SD").Element("SubDistrictNameL").GetStrValue(),

                ProvinceName = x.Element("D").Element("SD").Element("PD").Element("ProvinceName").GetStrValue(),

                ProvinceNameL = x.Element("D").Element("SD").Element("PD").Element("ProvinceNameL").GetStrValue(),

                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),

                DistrictId = x.Element("DistrictId").GetValue<long>(),
                SubDistrictId = x.Element("SubDistrictId").GetValue<long>(),
                //PostalCode = x.Element("ZipCode").GetStrValue(),
                PostalCode = x.Element("PostalCode").GetStrValue(),

                ProvinceId = x.Element("ProvinceId").GetValue<long>(),
                Latitude = x.Element("Latitude").GetValue<decimal>(),
                Longitude = x.Element("Longitude").GetValue<decimal>(),

                AddressLine1 = x.Element("AddressLine1").GetStrValue(),
                AddressLine2 = x.Element("AddressLine2").GetStrValue(),
                Type = x.Element("Type").GetStrValue(),


                HouseNumber = x.Element("HouseNumber").GetStrValue(),
                StreetNumber = x.Element("StreetNumber").GetStrValue(),
                Note = x.Element("Note").GetStrValue(),
                BuildingName = x.Element("BuildingName").GetStrValue(),
                Floor = x.Element("Floor").GetStrValue(),
                UnitNo = x.Element("UnitNo").GetStrValue(),
                PhoneNo = x.Element("PhoneNo").GetStrValue(),

                alterNumber = x.Element("alterNumber").GetStrValue(),
                ResidenceType = x.Element("ResidenceType").GetStrValue(),

                IsDefault = x.Element("IsDefault").GetBoolVal(),
                Tag = x.Element("Tag").GetStrValue(),

            }).ToList();
        }

        public List<DistrictModel> GetDistrictsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DistrictModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("district").Select(x => new DistrictModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
            }).ToList();
        }

        public List<DistrictLanguageMappingModel> GetDistrictLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DistrictLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("districtlanguagemapping").Select(x => new DistrictLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                DistrictId = x.Element("DistrictId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<SubDistrictModel> GetSubDistrictsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<SubDistrictModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("subdistrict").Select(x => new SubDistrictModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                DistrictId = x.Element("DistrictId").GetValue<long>(),
            }).ToList();
        }

        public List<SubDistrictLanguageMappingModel> GetSubDistrictLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<SubDistrictLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("subdistrictlanguagemapping").Select(x => new SubDistrictLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue(),
                SubDistrictId = x.Element("SubDistrictId").GetValue<long>(),
            }).ToList();
        }

        public List<ProvinceModel> GetProvincesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ProvinceModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("province").Select(x => new ProvinceModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),

            }).ToList();
        }

        public List<ProvinceLanguageMappingModel> GetProvinceLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ProvinceLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("provincelanguagemapping").Select(x => new ProvinceLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                Name = x.Element("Name").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                ProvinceId = x.Element("ProvinceId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<UserCordinateModel> GetUserCordinatesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<UserCordinateModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("usercordinate").Select(x => new UserCordinateModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                Latitude = x.Element("Latitude").GetValue<decimal>(),
                Longitude = x.Element("Longitude").GetValue<decimal>(),
                OrderId = x.Element("OrderId").GetValue<long>(),
            }).ToList();
        }

        public List<ServiceModel> GetServicesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ServiceModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("service").Select(x => new ServiceModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
            }).ToList();
        }

        public List<ServiceLanguageMappingModel> GetServiceLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ServiceLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("servicelanguagemapping").Select(x => new ServiceLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Description = x.Element("Description").GetStrValue(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<ProductModel> GetProductsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ProductModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("product").Select(x => new ProductModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Image = GetFileGroupItemsByXml(x.Element("Image").GetStrValue()),
                Gender = x.Element("Gender").GetStrValue(),
            }).ToList();
        }

        public List<ProductLanguageMappingModel> GetProductLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ProductLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("productlanguagemapping").Select(x => new ProductLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<ProductPriceModel> GetProductPricesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<ProductPriceModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("productprice").Select(x => new ProductPriceModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                Price = x.Element("Price").GetValue<decimal>(),
                ServiceName = x.Element("service").Element("ServiceName").GetStrValue()
            }).ToList();
        }

        public List<DeliveryTypeModel> GetDeliveryTypeByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DeliveryTypeModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("deliverytype").Select(x => new DeliveryTypeModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                PriceType = x.Element("PriceType").GetStrValue(),
                Price = x.Element("Price").GetValue<decimal>(),
            }).ToList();
        }

        public List<DeliveryTypeLanguageMappingModel> GetDeliveryTypeLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DeliveryTypeLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("deliverytypelanguagemapping").Select(x => new DeliveryTypeLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Name = x.Element("Name").GetStrValue(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                DeliveryTypeId = x.Element("DeliveryTypeId").GetValue<long>(),
            }).ToList();
        }

        public List<CartModel> GetCartByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CartModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("cart").Select(x => new CartModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                IsLocked = x.Element("IsLocked").GetBoolVal(),
                CustomerId = x.Element("CustomerId").GetValue<long>(),
                Status = x.Element("Status").GetStrValue(),
                Tax = x.Element("Tax").GetValue<decimal>(),
                SubTotal = x.Element("SubTotal").GetValue<decimal>(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
                TotalItems = x.Element("TotalItems").GetValue<int>(),
            }).ToList();
        }

        public List<CartItemModel> GetCartItemsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CartItemModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("cartitem").Select(x => new CartItemModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                CartId = x.Element("CartId").GetValue<long>(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Quantity = x.Element("Quantity").GetValue<int>(),
                Price = x.Element("product").Element("service").Element("Price").GetValue<decimal>(),
                Icon = x.Element("product").Element("service").Element("Path").GetStrValue(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
                ProductName = x.Element("product").Element("service").Element("ProductNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ProductNameL").GetStrValue() : x.Element("product").Element("ProductName").GetStrValue(),
                ServiceName = x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() : x.Element("product").Element("service").Element("ServiceName").GetStrValue()
            }).ToList();
        }

        public List<CartItemServiceMappingModel> GetCartItemServiceMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CartItemServiceMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("cartitemservicemapping").Select(x => new CartItemServiceMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                CartItemId = x.Element("CartItemId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Quantity = x.Element("Quantity").GetValue<int>(),
                Price = x.Element("Price").GetValue<decimal>(),
                SubTotal = x.Element("SubTotal").GetValue<decimal>(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
            }).ToList();
        }

        public List<OrderModel> GetOrdersByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<OrderModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("order").Select(x => new OrderModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                DeliveryDate = x.Element("DeliveryDate").GetDateOffset(),
                //DeliveryType = x.Element("DeliveryType").GetValue<long>(),
                DeliveryType = x.Element("DeliveryType").GetStrValue(),

                TotalItems = x.Element("TotalItems").GetValue<int>(),
                DeliveryPrice = x.Element("DeliveryPrice").GetValue<decimal>(),
                DriverId = x.Element("DriverId").GetValue<long>(),
                Status = x.Element("Status").GetStrValue(),
                PaymentType = x.Element("PaymentType").GetStrValue(),
                PaidAmount = x.Element("PaidAmount").GetValue<decimal>(),
                Tax = x.Element("Tax").GetValue<decimal>(),
                SubTotal = x.Element("SubTotal").GetValue<decimal>(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
                AdressId = x.Element("AdressId").GetValue<long>(),
                ExpectedPickUpMin = x.Element("ExpectedPickUpMin").GetValue<long>(),
                ExpectedPickUpMax = x.Element("ExpectedPickUpMax").GetValue<long>(),
                ExpectedDeliveryMin = x.Element("ExpectedDeliveryMin").GetValue<long>(),
                ExpectedDeliveryMax = x.Element("ExpectedDeliveryMax").GetValue<long>(),
                CartId = x.Element("CartId").GetValue<long>(),
            }).ToList();
        }

        public List<OrderItemModel> GetOrderItemsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<OrderItemModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("OrderItem").Select(x => new OrderItemModel
            {
                Id = x.Element("Id").GetValue<long>(),
                //TenantId = x.Element("TenantId").GetValue<int>(),
                //Slug = x.Element("Slug").GetStrValue(),
                //CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                //UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                //CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                //UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                //IsActive = x.Element("IsActive").GetBoolVal(),
                OrderId = x.Element("OrderId").GetValue<long>(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Quantity = x.Element("Quantity").GetValue<int>(),
                IsPacking = x.Element("IsPacking").GetBoolVal(),
                PackingPrice = x.Element("PackingPrice").GetValue<decimal>(),
                SubTotal = x.Element("SubTotal").GetValue<decimal>(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
                PackingId = x.Element("PackingId").GetValue<long>(),

                Icon = x.Element("product").Element("service").Element("Path").GetStrValue(),
                ProductName = x.Element("product").Element("service").Element("ProductNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ProductNameL").GetStrValue() : x.Element("product").Element("ProductName").GetStrValue(),
                ServiceName = x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() : x.Element("product").Element("service").Element("ServiceName").GetStrValue()



            }).ToList();
        }


        public List<CartItemModel> GetListOrderItemsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CartItemModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("cartitem").Select(x => new CartItemModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                CartId = x.Element("CartId").GetValue<long>(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Quantity = x.Element("Quantity").GetValue<int>(),
                Price = x.Element("product").Element("service").Element("Price").GetValue<decimal>(),
                Icon = x.Element("product").Element("service").Element("Path").GetStrValue(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
                ProductName = x.Element("product").Element("service").Element("ProductNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ProductNameL").GetStrValue() : x.Element("product").Element("ProductName").GetStrValue(),
                ServiceName = x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() != null ? x.Element("product").Element("service").Element("ServiceNameL").GetStrValue() : x.Element("product").Element("service").Element("ServiceName").GetStrValue()
            }).ToList();
        }

        public List<OrderItemServiceMappingModel> GetOrderItemServiceMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<OrderItemServiceMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("orderitemservicemapping").Select(x => new OrderItemServiceMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                OrderItemId = x.Element("OrderItemId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Quantity = x.Element("Quantity").GetValue<int>(),
                Price = x.Element("Price").GetValue<decimal>(),
                SubTotal = x.Element("SubTotal").GetValue<decimal>(),
                TotalPrice = x.Element("TotalPrice").GetValue<decimal>(),
            }).ToList();
        }

        public List<OrderLogModel> GetOrderLogsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<OrderLogModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("orderlog").Select(x => new OrderLogModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                OrderId = x.Element("OrderId").GetValue<long>(),
                Note = x.Element("Note").GetStrValue(),
            }).ToList();
        }

        public List<PaymentLogModel> GetPaymentLogsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<PaymentLogModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("paymentlog").Select(x => new PaymentLogModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UserId = x.Element("UserId").GetValue<long>(),
                CardNumber = x.Element("CardNumber").GetStrValue(),
                Amount = x.Element("Amount").GetValue<decimal>(),
                OrderId = x.Element("OrderId").GetValue<long>(),
                TransactionId = x.Element("TransactionId").GetStrValue(),
                Status = x.Element("Status").GetStrValue(),
            }).ToList();
        }

        public List<PackingModel> GetPackingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<PackingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("packing").Select(x => new PackingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                ProductId = x.Element("ProductId").GetValue<long>(),
                ServiceId = x.Element("ServiceId").GetValue<long>(),
                Price = x.Element("Price").GetValue<decimal>(),
            }).ToList();
        }

        public List<PackingLanguageMappingModel> GetPackingLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<PackingLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("packinglanguagemapping").Select(x => new PackingLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                PackingId = x.Element("PackingId").GetValue<long>(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Name = x.Element("Name").GetStrValue(),
            }).ToList();
        }

        public List<DefaultMessageModel> GetDefaultMessagesByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DefaultMessageModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("defaultmessage").Select(x => new DefaultMessageModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Type = x.Element("Type").GetStrValue(),
            }).ToList();
        }

        public List<DefaultMessageLanguageMappingModel> GetDefaultMessageLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<DefaultMessageLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("defaultmessagelanguagemapping").Select(x => new DefaultMessageLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                DefaultMessageId = x.Element("DefaultMessageId").GetValue<long>(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Message = x.Element("Message").GetStrValue(),
            }).ToList();
        }

        public List<CurrencyModel> GetCurrencyByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CurrencyModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("currency").Select(x => new CurrencyModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Price = x.Element("Price").GetValue<decimal>(),
            }).ToList();
        }

        public List<CurrencyLanguageMappingModel> GetCurrencyLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CurrencyLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("currencylanguagemapping").Select(x => new CurrencyLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                CurrencyId = x.Element("CurrencyId").GetValue<long>(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Name = x.Element("Name").GetStrValue(),
            }).ToList();
        }

        public List<NotificationMasterModel> GetNotificationMastersByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<NotificationMasterModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("notificationmaster").Select(x => new NotificationMasterModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Type = x.Element("Type").GetStrValue(),
                CanBeDelete = x.Element("CanBeDelete").GetBoolVal(),
            }).ToList();
        }

        public List<NotificationMasterLanguageMappingModel> GetNotificationMasterLanguageMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<NotificationMasterLanguageMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("notificationmasterlanguagemapping").Select(x => new NotificationMasterLanguageMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                LanguageId = x.Element("LanguageId").GetValue<long>(),
                Message = x.Element("Message").GetStrValue(),
                NotificationMasterId = x.Element("NotificationMasterId").GetValue<long>(),
                LanguageName = x.Element("language").Element("LanguageName").GetStrValue()
            }).ToList();
        }

        public List<NotificationMasterKeyValueMappingModel> GetNotificationMasterKeyValueMappingsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<NotificationMasterKeyValueMappingModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("notificationmasterkeyvaluemapping").Select(x => new NotificationMasterKeyValueMappingModel
            {
                Id = x.Element("Id").GetValue<long>(),
                TenantId = x.Element("TenantId").GetValue<int>(),
                Slug = x.Element("Slug").GetStrValue(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Key = x.Element("Key").GetStrValue(),
                Value = x.Element("Value").GetStrValue(),
                NotificationId = x.Element("NotificationId").GetValue<long>(),
            }).ToList();
        }

        //@@ADD_NEW_XML_CODE
        #endregion
    }
}






































