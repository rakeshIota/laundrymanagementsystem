﻿using LotusLaundry.DBRepository.Feedback;
using LotusLaundry.Domain.Feedback;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.Feedback
{
    public class FeedbackService : IFeedbackService
    {	
        public FeedbackDBService _feedbackDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public FeedbackService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _feedbackDBService = new FeedbackDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long FeedbackInsert(FeedbackModel model)
        {
            var id = _feedbackDBService.FeedbackInsert(model);
            return id;
        }

        public void FeedbackUpdate(FeedbackModel model)
        {
            _feedbackDBService.FeedbackUpdate(model);
        }

        public List<FeedbackModel> SelectFeedback(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _feedbackDBService.SelectFeedback(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
