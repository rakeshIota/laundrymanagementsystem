﻿using LotusLaundry.Domain.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Feedback
{
    public interface IFeedbackService
    {
        /// <summary>
        /// used for insertion feedback
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long FeedbackInsert(FeedbackModel model);
        
        /// <summary>
        /// Method for update feedback
        /// </summary>
        /// <param name="model"></param>
        void FeedbackUpdate(FeedbackModel model);

        /// <summary>
        /// use to select all feedback or select feedback by id 
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<FeedbackModel> SelectFeedback(SearchParam param);
    }
}
