﻿using LotusLaundry.DBRepository.DeliveryType;
using LotusLaundry.Domain.DeliveryType;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.DeliveryType
{
    public class DeliveryTypeService : IDeliveryTypeService
    {	
        public DeliveryTypeDBService _deliverytypeDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public DeliveryTypeService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _deliverytypeDBService = new DeliveryTypeDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long DeliveryTypeInsert(DeliveryTypeModel model)
        {
            var id = _deliverytypeDBService.DeliveryTypeInsert(model);
            return id;
        }

        public void DeliveryTypeUpdate(DeliveryTypeModel model)
        {
            _deliverytypeDBService.DeliveryTypeUpdate(model);
        }

        public List<DeliveryTypeModel> SelectDeliveryType(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _deliverytypeDBService.SelectDeliveryType(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
