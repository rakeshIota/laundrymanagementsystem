﻿using LotusLaundry.Domain.DeliveryType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.DeliveryType
{
    public interface IDeliveryTypeService
    {
        /// <summary>
        /// used for insertion deliverytype
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long DeliveryTypeInsert(DeliveryTypeModel model);
        
        /// <summary>
        /// Method for update deliverytype
        /// </summary>
        /// <param name="model"></param>
        void DeliveryTypeUpdate(DeliveryTypeModel model);

        /// <summary>
        /// use to select all deliverytype or select deliverytype by id 
        /// </summary>
        /// <param name="deliverytypeId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<DeliveryTypeModel> SelectDeliveryType(SearchParam param);
    }
}
