﻿using LotusLaundry.DBRepository.StateLanguageMapping;
using LotusLaundry.Domain.StateLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.StateLanguageMapping
{
    public class StateLanguageMappingService : IStateLanguageMappingService
    {	
        public StateLanguageMappingDBService _statelanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public StateLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _statelanguagemappingDBService = new StateLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long StateLanguageMappingInsert(StateLanguageMappingModel model)
        {
            var id = _statelanguagemappingDBService.StateLanguageMappingInsert(model);
            return id;
        }

        public void StateLanguageMappingUpdate(StateLanguageMappingModel model)
        {
            _statelanguagemappingDBService.StateLanguageMappingUpdate(model);
        }

        public List<StateLanguageMappingModel> SelectStateLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _statelanguagemappingDBService.SelectStateLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
