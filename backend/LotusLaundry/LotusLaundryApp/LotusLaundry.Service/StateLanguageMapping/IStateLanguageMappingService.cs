﻿using LotusLaundry.Domain.StateLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.StateLanguageMapping
{
    public interface IStateLanguageMappingService
    {
        /// <summary>
        /// used for insertion statelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long StateLanguageMappingInsert(StateLanguageMappingModel model);
        
        /// <summary>
        /// Method for update statelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void StateLanguageMappingUpdate(StateLanguageMappingModel model);

        /// <summary>
        /// use to select all statelanguagemapping or select statelanguagemapping by id 
        /// </summary>
        /// <param name="statelanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<StateLanguageMappingModel> SelectStateLanguageMapping(SearchParam param);
    }
}
