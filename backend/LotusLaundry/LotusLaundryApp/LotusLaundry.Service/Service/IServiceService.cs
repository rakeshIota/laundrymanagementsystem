﻿using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Service
{
    public interface IServiceService
    {
        /// <summary>
        /// used for insertion service
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ServiceInsert(ServiceModel model);
        
        /// <summary>
        /// Method for update service
        /// </summary>
        /// <param name="model"></param>
        void ServiceUpdate(ServiceModel model);

        /// <summary>
        /// use to select all service or select service by id 
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ServiceModel> SelectService(SearchParam param);
    }
}
