﻿using LotusLaundry.DBRepository.Service;
using LotusLaundry.Domain.Service;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.ServiceLanguageMapping;

namespace LotusLaundry.Service.Service
{
    public class ServiceService : IServiceService
    {	
        public ServiceDBService _serviceDBService;
        public ServiceLanguageMappingDBService _serviceLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ServiceService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _serviceDBService = new ServiceDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _serviceLanguageMappingDBService = new ServiceLanguageMappingDBService();


        }
        
        public long ServiceInsert(ServiceModel model)
        {
            var id = _serviceDBService.ServiceInsert(model);
            if (model.Image != null)
            {
                //set target path and move file from target location
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //set target path and move file from target location
                    model.Image = _fileGroupService.SetPathAndMoveFile(model.Image, id, FileType.SERVICE);
                }
                model.Image.ForEach(x => x.Type = FileType.SERVICE_IMAGE);
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, id, model.Image.XmlSerialize());
            }
            if (model.ServiceLanguages != null && model.ServiceLanguages.Count > 0)
            {
                model.ServiceLanguages.ForEach(x =>
                {
                    x.ServiceId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _serviceLanguageMappingDBService.ServiceLanguageSaveXml(model.ServiceLanguages.XmlSerialize());
            }
            return id;
        }

        public void ServiceUpdate(ServiceModel model)
        {
            _serviceDBService.ServiceUpdate(model);
            if (model.Image != null)
            {
                //set target path and move file from target location
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //set target path and move file from target location
                    model.Image = _fileGroupService.SetPathAndMoveFile(model.Image, model.Id, FileType.SERVICE);
                }
                model.Image.ForEach(x => x.Type = FileType.SERVICE_IMAGE);
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, model.Id, model.Image.XmlSerialize());
            }
            if (model.ServiceLanguages != null && model.ServiceLanguages.Count > 0)
            {
                model.ServiceLanguages.ForEach(x =>
                {
                    x.ServiceId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _serviceLanguageMappingDBService.ServiceLanguageSaveXml(model.ServiceLanguages.XmlSerialize());
            }
        }

        public List<ServiceModel> SelectService(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _serviceDBService.SelectService(param);
			response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
                x.ServiceLanguages = _xmlService.GetServiceLanguageMappingsByXml(x.ServiceLanguageXml);
            });
            return response;
        }
    }
}
