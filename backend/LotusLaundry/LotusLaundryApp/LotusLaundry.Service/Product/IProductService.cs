﻿using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Product
{
    public interface IProductService
    {
        /// <summary>
        /// used for insertion product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ProductInsert(ProductModel model);
        
        /// <summary>
        /// Method for update product
        /// </summary>
        /// <param name="model"></param>
        void ProductUpdate(ProductModel model);

        /// <summary>
        /// use to select all product or select product by id 
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ProductModel> SelectProduct(SearchParam param);
    }
}
