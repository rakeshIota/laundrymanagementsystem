﻿using LotusLaundry.DBRepository.Product;
using LotusLaundry.Domain.Product;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.ProductLanguageMapping;
using LotusLaundry.DBRepository.ProductPrice;

namespace LotusLaundry.Service.Product
{
    public class ProductService : IProductService
    {
        public ProductDBService _productDBService;
        public ProductLanguageMappingDBService _productlanguagemappingDBService;
        public ProductPriceDBService _productPriceDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ProductService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _productDBService = new ProductDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _productlanguagemappingDBService = new ProductLanguageMappingDBService();
            _productPriceDBService = new ProductPriceDBService();


        }

        public long ProductInsert(ProductModel model)
        {
            var id = _productDBService.ProductInsert(model);
            if (model.Image != null)
            {
                //set target path and move file from target location
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //set target path and move file from target location
                    model.Image = _fileGroupService.SetPathAndMoveFile(model.Image, id, FileType.PRODUCT);
                }
                model.Image.ForEach(x => x.Type = FileType.PRODUCT_IMAGE);
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, id, model.Image.XmlSerialize());
            }
            if (model.ProductLanguages != null && model.ProductLanguages.Count > 0)
            {
                model.ProductLanguages.ForEach(x =>
                {
                    x.ProductId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                    x.TenantId = model.TenantId;
                });
                _productlanguagemappingDBService.ProductLanguageMappingXMLSave(model.ProductLanguages.XmlSerialize());
            }
            if (model.ProductPrices != null && model.ProductPrices.Count > 0)
            {
                model.ProductPrices.ForEach(x =>
                {
                    x.ProductId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                    x.TenantId = model.TenantId;
                });
                _productPriceDBService.ProductPriceXMLSave(model.ProductPrices.XmlSerialize());
            }
            return id;
        }

        public void ProductUpdate(ProductModel model)
        {
            _productDBService.ProductUpdate(model);
            if (model.Image != null)
            {
                //set target path and move file from target location
                if (_imageOption == FileType.TYPE_FOLDER)
                {
                    //set target path and move file from target location
                    model.Image = _fileGroupService.SetPathAndMoveFile(model.Image, model.Id, FileType.PRODUCT);
                }
                model.Image.ForEach(x => x.Type = FileType.PRODUCT_IMAGE);
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsertXml(model.CreatedBy, model.Id, model.Image.XmlSerialize());
            }
            if (model.ProductLanguages != null && model.ProductLanguages.Count > 0)
            {
                model.ProductLanguages.ForEach(x =>
                {
                    x.ProductId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                    x.TenantId = model.TenantId;
                });
                _productlanguagemappingDBService.ProductLanguageMappingXMLSave(model.ProductLanguages.XmlSerialize());
            }
            if (model.ProductPrices != null && model.ProductPrices.Count > 0)
            {
                model.ProductPrices.ForEach(x =>
                {
                    x.ProductId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                    x.TenantId = model.TenantId;
                });
                _productPriceDBService.ProductPriceXMLSave(model.ProductPrices.XmlSerialize());
            }
        }

        public List<ProductModel> SelectProduct(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _productDBService.SelectProduct(param);
			response.ForEach(x =>
            {
                x.Image = _xmlService.GetFileGroupItemsByXml(x.ImageXml);
                x.ProductLanguages = _xmlService.GetProductLanguageMappingsByXml(x.ProductLanguageXml);
                x.ProductPrices = _xmlService.GetProductPricesByXml(x.ProductPriceXml);
            });
            return response;
        }
    }
}
