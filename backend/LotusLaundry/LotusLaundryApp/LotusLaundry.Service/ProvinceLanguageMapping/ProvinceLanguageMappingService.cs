﻿using LotusLaundry.DBRepository.ProvinceLanguageMapping;
using LotusLaundry.Domain.ProvinceLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.ProvinceLanguageMapping
{
    public class ProvinceLanguageMappingService : IProvinceLanguageMappingService
    {	
        public ProvinceLanguageMappingDBService _provincelanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ProvinceLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _provincelanguagemappingDBService = new ProvinceLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long ProvinceLanguageMappingInsert(ProvinceLanguageMappingModel model)
        {
            var id = _provincelanguagemappingDBService.ProvinceLanguageMappingInsert(model);
            return id;
        }

        public void ProvinceLanguageMappingUpdate(ProvinceLanguageMappingModel model)
        {
            _provincelanguagemappingDBService.ProvinceLanguageMappingUpdate(model);
        }

        public List<ProvinceLanguageMappingModel> SelectProvinceLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _provincelanguagemappingDBService.SelectProvinceLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
