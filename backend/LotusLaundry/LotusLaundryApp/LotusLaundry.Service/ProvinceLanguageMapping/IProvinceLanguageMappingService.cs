﻿using LotusLaundry.Domain.ProvinceLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.ProvinceLanguageMapping
{
    public interface IProvinceLanguageMappingService
    {
        /// <summary>
        /// used for insertion provincelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ProvinceLanguageMappingInsert(ProvinceLanguageMappingModel model);
        
        /// <summary>
        /// Method for update provincelanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void ProvinceLanguageMappingUpdate(ProvinceLanguageMappingModel model);

        /// <summary>
        /// use to select all provincelanguagemapping or select provincelanguagemapping by id 
        /// </summary>
        /// <param name="provincelanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ProvinceLanguageMappingModel> SelectProvinceLanguageMapping(SearchParam param);
    }
}
