﻿using LotusLaundry.Domain.Image;
using System.Threading.Tasks;

namespace LotusLaundry.Service.ImageUpload
{
    public interface IImageService
    {
        Task<UploadedImage> CreateUploadedImage(string base64String, string fileName, string type);
        Task AddImageToBlobStorageAsync(UploadedImage image);
    }
}
