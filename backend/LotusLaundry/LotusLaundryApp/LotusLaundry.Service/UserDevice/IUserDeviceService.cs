﻿using LotusLaundry.Domain.UserDevice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.UserDevice
{
    public interface IUserDeviceService
    {
        /// <summary>
        /// used for insertion userdevice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long UserDeviceInsert(UserDeviceModel model);
        
        /// <summary>
        /// Method for update userdevice
        /// </summary>
        /// <param name="model"></param>
        void UserDeviceUpdate(UserDeviceModel model);

        /// <summary>
        /// use to select all userdevice or select userdevice by id 
        /// </summary>
        /// <param name="userdeviceId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<UserDeviceModel> SelectUserDevice(SearchParam param);
    }
}
