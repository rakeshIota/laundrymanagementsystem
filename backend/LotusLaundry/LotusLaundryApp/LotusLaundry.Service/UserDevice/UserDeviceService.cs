﻿using LotusLaundry.DBRepository.UserDevice;
using LotusLaundry.Domain.UserDevice;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.UserDevice
{
    public class UserDeviceService : IUserDeviceService
    {	
        public UserDeviceDBService _userdeviceDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public UserDeviceService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _userdeviceDBService = new UserDeviceDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long UserDeviceInsert(UserDeviceModel model)
        {
            var id = _userdeviceDBService.UserDeviceInsert(model);
            return id;
        }

        public void UserDeviceUpdate(UserDeviceModel model)
        {
            _userdeviceDBService.UserDeviceUpdate(model);
        }

        public List<UserDeviceModel> SelectUserDevice(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _userdeviceDBService.SelectUserDevice(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
