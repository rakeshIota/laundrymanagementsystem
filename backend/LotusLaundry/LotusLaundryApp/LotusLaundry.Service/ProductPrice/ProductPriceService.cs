﻿using LotusLaundry.DBRepository.ProductPrice;
using LotusLaundry.Domain.ProductPrice;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.ProductPrice
{
    public class ProductPriceService : IProductPriceService
    {	
        public ProductPriceDBService _productpriceDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public ProductPriceService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _productpriceDBService = new ProductPriceDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long ProductPriceInsert(ProductPriceModel model)
        {
            var id = _productpriceDBService.ProductPriceInsert(model);
            return id;
        }

        public void ProductPriceUpdate(ProductPriceModel model)
        {
            _productpriceDBService.ProductPriceUpdate(model);
        }

        public List<ProductPriceModel> SelectProductPrice(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _productpriceDBService.SelectProductPrice(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
