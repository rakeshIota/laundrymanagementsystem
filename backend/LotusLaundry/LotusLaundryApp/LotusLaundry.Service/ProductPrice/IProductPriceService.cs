﻿using LotusLaundry.Domain.ProductPrice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.ProductPrice
{
    public interface IProductPriceService
    {
        /// <summary>
        /// used for insertion productprice
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long ProductPriceInsert(ProductPriceModel model);
        
        /// <summary>
        /// Method for update productprice
        /// </summary>
        /// <param name="model"></param>
        void ProductPriceUpdate(ProductPriceModel model);

        /// <summary>
        /// use to select all productprice or select productprice by id 
        /// </summary>
        /// <param name="productpriceId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<ProductPriceModel> SelectProductPrice(SearchParam param);
    }
}
