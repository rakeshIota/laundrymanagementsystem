﻿using LotusLaundry.DBRepository.NotificationMaster;
using LotusLaundry.Domain.NotificationMaster;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.NotificationMasterLanguageMapping;
using LotusLaundry.DBRepository.NotificationMasterKeyValueMapping;

namespace LotusLaundry.Service.NotificationMaster
{
    public class NotificationMasterService : INotificationMasterService
    {
        public NotificationMasterDBService _notificationmasterDBService;
        public NotificationMasterLanguageMappingDBService _notificationMasterLanguageMappingDBService;
        public NotificationMasterKeyValueMappingDBService _notificationMasterKeyValueMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        public NotificationMasterService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _notificationmasterDBService = new NotificationMasterDBService();
            _notificationMasterKeyValueMappingDBService = new NotificationMasterKeyValueMappingDBService();
            _notificationMasterLanguageMappingDBService = new NotificationMasterLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
        }
        
        public long NotificationMasterInsert(NotificationMasterModel model)
        {
            var id = _notificationmasterDBService.NotificationMasterInsert(model);
            if (model.NotificationMasterLanguages != null && model.NotificationMasterLanguages.Count > 0)
            {
                model.NotificationMasterLanguages.ForEach(x =>
                {
                    x.NotificationMasterId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _notificationMasterLanguageMappingDBService.NotificationMasterLanguageMappingXMLSave(model.NotificationMasterLanguages.XmlSerialize());
            }
            if (model.NotificationMasterKeyValues != null && model.NotificationMasterKeyValues.Count > 0)
            {
                model.NotificationMasterKeyValues.ForEach(x =>
                {
                    x.NotificationId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _notificationMasterKeyValueMappingDBService.NotificationMasterKeyValueMappingXMLSave(model.NotificationMasterKeyValues.XmlSerialize());
            }
            return id;
        }

        public void NotificationMasterUpdate(NotificationMasterModel model)
        {
            _notificationmasterDBService.NotificationMasterUpdate(model);
            if (model.NotificationMasterLanguages != null && model.NotificationMasterLanguages.Count > 0)
            {
                model.NotificationMasterLanguages.ForEach(x =>
                {
                    x.NotificationMasterId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _notificationMasterLanguageMappingDBService.NotificationMasterLanguageMappingXMLSave(model.NotificationMasterLanguages.XmlSerialize());
            }
            if (model.NotificationMasterKeyValues != null && model.NotificationMasterKeyValues.Count > 0)
            {
                model.NotificationMasterKeyValues.ForEach(x =>
                {
                    x.NotificationId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _notificationMasterKeyValueMappingDBService.NotificationMasterKeyValueMappingXMLSave(model.NotificationMasterKeyValues.XmlSerialize());
            }

        }

        public List<NotificationMasterModel> SelectNotificationMaster(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _notificationmasterDBService.SelectNotificationMaster(param);
			response.ForEach(x =>
            {
                x.NotificationMasterLanguages = _xmlService.GetNotificationMasterLanguageMappingsByXml(x.NotificationMasterLanguageXml);
                x.NotificationMasterKeyValues = _xmlService.GetNotificationMasterKeyValueMappingsByXml(x.NotificationMasterKeyValueXml);
            });
            return response;
        }
    }
}
