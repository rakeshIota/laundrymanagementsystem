﻿using LotusLaundry.Domain.NotificationMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.NotificationMaster
{
    public interface INotificationMasterService
    {
        /// <summary>
        /// used for insertion notificationmaster
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long NotificationMasterInsert(NotificationMasterModel model);
        
        /// <summary>
        /// Method for update notificationmaster
        /// </summary>
        /// <param name="model"></param>
        void NotificationMasterUpdate(NotificationMasterModel model);

        /// <summary>
        /// use to select all notificationmaster or select notificationmaster by id 
        /// </summary>
        /// <param name="notificationmasterId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<NotificationMasterModel> SelectNotificationMaster(SearchParam param);
    }
}
