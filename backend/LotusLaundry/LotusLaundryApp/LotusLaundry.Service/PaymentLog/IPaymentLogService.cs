﻿using LotusLaundry.Domain.PaymentLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Payment;

namespace LotusLaundry.Service.PaymentLog
{
    public interface IPaymentLogService
    {
        /// <summary>
        /// used for insertion paymentlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PaymentLogInsert(PaymentLogModel model);
        
        /// <summary>
        /// Method for update paymentlog
        /// </summary>
        /// <param name="model"></param>
        void PaymentLogUpdate(PaymentLogModel model);

        /// <summary>
        /// use to select all paymentlog or select paymentlog by id 
        /// </summary>
        /// <param name="paymentlogId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PaymentLogModel> SelectPaymentLog(SearchParam param);

        long QrLogInsert(PaymentQRcodeModel model);
    }
}
