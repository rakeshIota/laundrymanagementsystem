﻿using LotusLaundry.DBRepository.PaymentLog;
using LotusLaundry.Domain.PaymentLog;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Payment;

namespace LotusLaundry.Service.PaymentLog
{
    public class PaymentLogService : IPaymentLogService
    {	
        public PaymentLogDBService _paymentlogDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PaymentLogService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _paymentlogDBService = new PaymentLogDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long PaymentLogInsert(PaymentLogModel model)
        {
            var id = _paymentlogDBService.PaymentLogInsert(model);
            return id;
        }

        //long QrLogInsert(PaymentQRcodeModel model);
        public long QrLogInsert(PaymentQRcodeModel model)
        {
            var id = _paymentlogDBService.QrLogInsert(model);
            return id;
        }
        public void PaymentLogUpdate(PaymentLogModel model)
        {
            _paymentlogDBService.PaymentLogUpdate(model);
        }

        public List<PaymentLogModel> SelectPaymentLog(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _paymentlogDBService.SelectPaymentLog(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
