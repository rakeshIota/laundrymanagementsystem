﻿using LotusLaundry.DBRepository.AppSetting;
using LotusLaundry.Domain.AppSetting;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.AppSetting
{
    public class AppSettingService : IAppSettingService
    {	
        public AppSettingDBService _appsettingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public AppSettingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _appsettingDBService = new AppSettingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long AppSettingInsert(AppSettingModel model)
        {
            var id = _appsettingDBService.AppSettingInsert(model);
            return id;
        }

        public void AppSettingUpdate(AppSettingModel model)
        {
            _appsettingDBService.AppSettingUpdate(model);
        }

        public List<AppSettingModel> SelectAppSetting(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _appsettingDBService.SelectAppSetting(param);
			response.ForEach(x =>
            {
            });
            return response;
        }

        public ApplicationSettingModel ApplicationSettingSelect(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            var response = _appsettingDBService.AppSettingSelect(param);
            ApplicationSettingModel appset = new ApplicationSettingModel();
            response.ForEach(x =>
            {
                if (x.Key == "SAMEDAY_DELIVERY_RATE")
                {
                    appset.SAMEDAY_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "EXPRESS_DELIVERY_RATE")
                {
                    appset.EXPRESS_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "NORMAL_DELIVERY_RATE")
                {
                    appset.NORMAL_DELIVERY_RATE = x.Value;
                }
                if (x.Key == "GoogleKeyRefresh")
                {
                    appset.GoogleKeyRefresh = x.Value;
                }
                if (x.Key == "timeSlot")
                {
                    appset.timeSlot = x.Value;
                }
                //=========  

                if (x.Key == "deliveryStartTime")
                {
                    appset.deliveryStartTime = x.Value;
                }
                if (x.Key == "expressEndTime")
                {
                    appset.expressEndTime = x.Value;
                }
                if (x.Key == "normalEndTime")
                {
                    appset.normalEndTime = x.Value;
                }
                if (x.Key == "sameDayEndTime")
                {
                    appset.sameDayEndTime = x.Value;
                }


                if (x.Key == "minimumOrderAmount")
                {
                    appset.minimumOrderAmount = x.Value;
                }
                if (x.Key == "logisticCharge")
                {
                    appset.logisticCharge = x.Value;
                }
                if (x.Key == "availablePaymentTypes")
                {
                    appset.availablePaymentTypes = x.Value;
                }
                if (x.Key == "driverLocationInterval")
                {
                    appset.driverLocationInterval = x.Value;
                }

                //



            });





            return appset;
        }
    }
}
