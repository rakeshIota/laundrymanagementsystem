﻿using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.AppSetting
{
    public interface IAppSettingService
    {
        /// <summary>
        /// used for insertion appsetting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long AppSettingInsert(AppSettingModel model);
        
        /// <summary>
        /// Method for update appsetting
        /// </summary>
        /// <param name="model"></param>
        void AppSettingUpdate(AppSettingModel model);

        /// <summary>
        /// use to select all appsetting or select appsetting by id 
        /// </summary>
        /// <param name="appsettingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<AppSettingModel> SelectAppSetting(SearchParam param);

        ApplicationSettingModel ApplicationSettingSelect(SearchParam param);
    }
}
