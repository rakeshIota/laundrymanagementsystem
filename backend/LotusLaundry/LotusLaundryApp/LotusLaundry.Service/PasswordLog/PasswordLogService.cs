﻿using LotusLaundry.DBRepository.PasswordLog;
using LotusLaundry.Domain.PasswordLog;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.PasswordLog
{
    public class PasswordLogService : IPasswordLogService
    {	
        public PasswordLogDBService _passwordlogDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PasswordLogService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _passwordlogDBService = new PasswordLogDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }


        public long PasswordLogInsert(PasswordLogModel model)
        {
            var id = _passwordlogDBService.PasswordLogInsert(model);
            return id;
        }

        public long OtpLogInsert(long? userId)
        {
            PasswordLogModel model = new PasswordLogModel();
            model.CreatedBy = 1;
            model.UpdatedBy = 1;
            model.UserId = userId;
            model.TenantId = 1;
            model.IsActive = true;
            var id = _passwordlogDBService.PasswordLogInsert(model);
            return id;
        }

        public void PasswordLogUpdate(PasswordLogModel model)
        {
            _passwordlogDBService.PasswordLogUpdate(model);
        }

        public void OtpLogDelete(long? userId)
        {
            PasswordLogModel model = new PasswordLogModel();
            model.IsDeleted = true;
            model.UserId = userId;
            _passwordlogDBService.PasswordLogUpdate(model);
        }
        public List<PasswordLogModel>OtpLogSelect(long? userId)
        {
            SearchParam param = new SearchParam();
            param.UserId = userId;
            return _passwordlogDBService.SelectPasswordLog(param);
        }

        public List<PasswordLogModel> SelectPasswordLog(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _passwordlogDBService.SelectPasswordLog(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
