﻿using LotusLaundry.Domain.PasswordLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.PasswordLog
{
    public interface IPasswordLogService
    {
        /// <summary>
        /// used for insertion passwordlog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PasswordLogInsert(PasswordLogModel model);
        long OtpLogInsert(long? userId);
        void OtpLogDelete(long? userId);
        List<PasswordLogModel> OtpLogSelect(long? userId);

        /// <summary>
        /// Method for update passwordlog
        /// </summary>
        /// <param name="model"></param>
        void PasswordLogUpdate(PasswordLogModel model);

        /// <summary>
        /// use to select all passwordlog or select passwordlog by id 
        /// </summary>
        /// <param name="passwordlogId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PasswordLogModel> SelectPasswordLog(SearchParam param);
    }
}
