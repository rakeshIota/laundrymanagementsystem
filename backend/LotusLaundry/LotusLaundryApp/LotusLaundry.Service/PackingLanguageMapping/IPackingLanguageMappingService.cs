﻿using LotusLaundry.Domain.PackingLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.PackingLanguageMapping
{
    public interface IPackingLanguageMappingService
    {
        /// <summary>
        /// used for insertion packinglanguagemapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long PackingLanguageMappingInsert(PackingLanguageMappingModel model);
        
        /// <summary>
        /// Method for update packinglanguagemapping
        /// </summary>
        /// <param name="model"></param>
        void PackingLanguageMappingUpdate(PackingLanguageMappingModel model);

        /// <summary>
        /// use to select all packinglanguagemapping or select packinglanguagemapping by id 
        /// </summary>
        /// <param name="packinglanguagemappingId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<PackingLanguageMappingModel> SelectPackingLanguageMapping(SearchParam param);
    }
}
