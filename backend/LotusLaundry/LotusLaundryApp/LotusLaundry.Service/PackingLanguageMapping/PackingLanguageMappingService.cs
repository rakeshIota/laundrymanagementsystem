﻿using LotusLaundry.DBRepository.PackingLanguageMapping;
using LotusLaundry.Domain.PackingLanguageMapping;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;


namespace LotusLaundry.Service.PackingLanguageMapping
{
    public class PackingLanguageMappingService : IPackingLanguageMappingService
    {	
        public PackingLanguageMappingDBService _packinglanguagemappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public PackingLanguageMappingService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _packinglanguagemappingDBService = new PackingLanguageMappingDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long PackingLanguageMappingInsert(PackingLanguageMappingModel model)
        {
            var id = _packinglanguagemappingDBService.PackingLanguageMappingInsert(model);
            return id;
        }

        public void PackingLanguageMappingUpdate(PackingLanguageMappingModel model)
        {
            _packinglanguagemappingDBService.PackingLanguageMappingUpdate(model);
        }

        public List<PackingLanguageMappingModel> SelectPackingLanguageMapping(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _packinglanguagemappingDBService.SelectPackingLanguageMapping(param);
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
