﻿using LotusLaundry.DBRepository.SubDistrict;
using LotusLaundry.Domain.SubDistrict;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.SubDistrictLanguageMapping;

namespace LotusLaundry.Service.SubDistrict
{
    public class SubDistrictService : ISubDistrictService
    {
        public SubDistrictDBService _subdistrictDBService;
        public SubDistrictLanguageMappingDBService _subDistrictLanguageMappingDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public SubDistrictService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _subdistrictDBService = new SubDistrictDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _subDistrictLanguageMappingDBService = new SubDistrictLanguageMappingDBService();
        }
        
        public long SubDistrictInsert(SubDistrictModel model)
        {
            var id = _subdistrictDBService.SubDistrictInsert(model);
            if (model.SubDistrictLanguages != null && model.SubDistrictLanguages.Count > 0)
            {
                model.SubDistrictLanguages.ForEach(x =>
                {
                    x.SubDistrictId = id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _subDistrictLanguageMappingDBService.SubDistrictLanguageMappingXMLSave(model.SubDistrictLanguages.XmlSerialize());
            }
            return id;
        }

        public void SubDistrictUpdate(SubDistrictModel model)
        {
            _subdistrictDBService.SubDistrictUpdate(model);
            if (model.SubDistrictLanguages != null && model.SubDistrictLanguages.Count > 0)
            {
                model.SubDistrictLanguages.ForEach(x =>
                {
                    x.SubDistrictId = model.Id;
                    x.CreatedBy = model.CreatedBy;
                    x.UpdatedBy = x.CreatedBy;
                });
                _subDistrictLanguageMappingDBService.SubDistrictLanguageMappingXMLSave(model.SubDistrictLanguages.XmlSerialize());
            }
        }

        public List<SubDistrictModel> SelectSubDistrict(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _subdistrictDBService.SelectSubDistrict(param);
			response.ForEach(x =>
            {
                x.SubDistrictLanguages = _xmlService.GetSubDistrictLanguageMappingsByXml(x.SubDistrictLanguageXml);
            });
            return response;
        }
    }
}
