﻿using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.SubDistrict
{
    public interface ISubDistrictService
    {
        /// <summary>
        /// used for insertion subdistrict
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long SubDistrictInsert(SubDistrictModel model);
        
        /// <summary>
        /// Method for update subdistrict
        /// </summary>
        /// <param name="model"></param>
        void SubDistrictUpdate(SubDistrictModel model);

        /// <summary>
        /// use to select all subdistrict or select subdistrict by id 
        /// </summary>
        /// <param name="subdistrictId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<SubDistrictModel> SelectSubDistrict(SearchParam param);
    }
}
