﻿using LotusLaundry.DBRepository.Exception;
using LotusLaundry.Domain.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Service.Exception
{
   public class ExceptionService : IExceptionService
    {
        public ExceptionDBService _exceptionDBService;
        public ExceptionService()
        {
            _exceptionDBService = new ExceptionDBService();
        }
        public void InsertLog(ExceptionModel exception )
        {
             _exceptionDBService.ExceptionLogInsert(exception);
        }
    }
}
