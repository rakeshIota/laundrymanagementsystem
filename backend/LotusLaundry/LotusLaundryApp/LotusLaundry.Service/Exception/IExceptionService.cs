﻿using LotusLaundry.Domain.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Service.Exception
{
   public interface IExceptionService
    {
        void InsertLog(ExceptionModel exception);
    }
}
