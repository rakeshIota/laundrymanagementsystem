﻿using LotusLaundry.DBRepository.Order;
using LotusLaundry.Domain.Order;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.DBRepository.OrderItem;
using LotusLaundry.Service.AppSetting;
using LotusLaundry.Domain.OrderItem;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Customer.DBRepository.Product;
using LotusLaundry.Common.Enums;
using LotusLaundry.Common.Success;
using LotusLaundry.Domain.PushNotification;
using LotusLaundry.Domain.OrderLog;
using LotusLaundry.Service.OrderLog;

namespace LotusLaundry.Service.Order
{
    public class OrderService : IOrderService
    {	
        public OrderDBService _orderDBService;
        public OrderItemDBService _orderItemDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        private IAppSettingService _appSettingService;
        public OrderItemDBService _orderitemDBService;
        public ProductCustomerDBService _productCustomerDBService;
        private IOrderLogService _orderlogService;


        public OrderService(IFileGroupService fileGroupService, IXmlService xmlService, IAppSettingService appSettingService, IOrderLogService orderLogService)
        {
            _orderlogService = orderLogService;
            _orderitemDBService= new OrderItemDBService();
            _appSettingService = appSettingService;
            _orderDBService = new OrderDBService();
            _orderItemDBService = new OrderItemDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            _productCustomerDBService = new ProductCustomerDBService();


        }
        
        public long OrderInsert(OrderModel model)
        {
            var id = _orderDBService.OrderInsert(model);
            return id;
        }

        public void OrderUpdate(OrderModel model)
        {
            
            
            if(model.DriverId==0)
            {
                model.DriverId = null;
            }

            _orderDBService.OrderUpdate(model);
        }


        
        public void OrderPaymentUpdate(long id, string qrCode, string paymentStatus ,long? updatedby)
        {

            _orderDBService.OrderPaymentUpdate( id,  qrCode,  paymentStatus, updatedby.GetValueOrDefault());
        }
        //_pushNotificationService.SendNotificationByFCMToUser(orderid, model.UserId.GetValueOrDefault(), PushNotificationType.OrderPlaced.ToString(), PushNotificationMSG.Title.Order_placed.ToString(), PushNotificationMSG.message.ORDER_PlACED.ToString());



        public void OrderStatusUpdate(OrderModel model)
        {
            if (model.DriverId == 0)
            {
                model.DriverId = null;
            }
            _orderDBService.OrderStatusUpdate(model);
        }

        // void OrderComplete(OrderModel model);
        public void OrderComplete(OrderModel model)
        {
            
            

            _orderDBService.OrderStatusUpdate(model);

            



        }


        public ResponseModel OrderLogInsert(long? responseId, long? Userid, int? TenantId, string Note = null)
        {
            ResponseModel result = new ResponseModel();
            
            {
                OrderLogModel orderlog = new OrderLogModel();
                orderlog.OrderId = responseId;
                orderlog.CreatedBy = Userid;
                orderlog.TenantId = TenantId;
                orderlog.Note = Note;
                orderlog.IsActive = true;
                var response = _orderlogService.OrderLogInsert(orderlog);

                result.Status = true;
                result.Message = response.ToString();

                return result;
            }
            
        }

        public List<OrderModel> SelectOrder(SearchParam param)
        {	
         	if (param.Id != null) {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            
            var response= _orderDBService.SelectOrder(param);
			response.ForEach(x =>
            {
            });
            return response;
        }

        public List<OrderModel> SelectMyOrder(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _orderDBService.SelectMyOrderlist(param);
            response.ForEach(x =>
            {
            });
            return response;
        }

        //
        public List<OrderModel> SelectMyOrderDetails(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            if (param.district != null)
            {
                param.districtId = Convert.ToInt64(CryptoEngine.Decrypt(param.district, KeyConstant.Key));
            }
            if (param.subdistrict != null)
            {
                param.subdistrictId = Convert.ToInt64(CryptoEngine.Decrypt(param.subdistrict, KeyConstant.Key));
            }
            if (param.driver != null)
            {
                param.driverId = Convert.ToInt64(CryptoEngine.Decrypt(param.driver, KeyConstant.Key));
            }
            if (param.orderId != null)
            {
                param.RecordId = Convert.ToInt64(param.orderId.TrimStart(new Char[] { '0' }));
            }
            

            var response = _orderDBService.SelectMyOrderDetails(param);
            response.ForEach(x =>
            {
                x.OrderItems = _xmlService.GetOrderItemsByXml(x.OrderItemXml);
                //    x.DeliveryAddress = _xmlService.GetUserAddressesByXml(x.DeliveryAddressXml);
                x.DeliveryAddress = _xmlService.GetCustomerAddressesByXml(x.DeliveryAddressXml);
                x.DriverDetails = _xmlService.GetDriverByXml(x.DriverXml);
                x.customerdetails = _xmlService.GetUsersByXml(x.customerXml).FirstOrDefault();
                x.DeliveryDriverDetails = _xmlService.GetDriverByXml(x.DeliveryDriverName);
                x.PickupDriverDetails = _xmlService.GetDriverByXml(x.PickupDriverName);


            });

            
           // if(param.filterOn!=null)
           // if(param.filterOn== "ORDER_ID" && param.filterType== "DESC")
           // {
           //     response= response.OrderByDescending(x => x.Id).ToList();

           // }
           // else if (param.filterOn == "ORDER_ID" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.Id).ToList();
           // }

           // else if (param.filterOn == "CUSTOMER_FULLNAME" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.customerdetails.FullName).ToList();

           // }
           // else if (param.filterOn == "CUSTOMER_FULLNAME" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.customerdetails.FullName).ToList();
           // }

           // else if (param.filterOn == "CREATED_ON" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.CreatedOn).ToList();

           // }
           // else if (param.filterOn == "CREATED_ON" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.CreatedOn).ToList();
           // }

           // else if (param.filterOn == "PICKUP_DATE" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.PickupDate).ToList();

           // }
           // else if (param.filterOn == "PICKUP_DATE" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.PickupDate).ToList();
           // }

           // else if (param.filterOn == "DELIVER_YDATE" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.deliveredDate).ToList();

           // }
           // else if (param.filterOn == "DELIVER_YDATE" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.deliveredDate).ToList();
           // }

           //else if (param.filterOn == "DELIVERY_TYPE" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.DeliveryType).ToList();

           // }
           // else if (param.filterOn == "DELIVERY_TYPE" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.DeliveryType).ToList();
           // }

           // else if (param.filterOn == "DISTRICT_NAME" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.DeliveryAddress.FirstOrDefault().DistrictName).ToList();

           // }
           // else if (param.filterOn == "DISTRICT_NAME" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.DeliveryAddress.FirstOrDefault().DistrictName).ToList();
           // }

           // else if (param.filterOn == "SUBDISTRICT_NAME" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.DeliveryAddress.FirstOrDefault().SubDistrictName).ToList();

           // }
           // else if (param.filterOn == "SUBDISTRICT_NAME" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.DeliveryAddress.FirstOrDefault().SubDistrictName).ToList();
           // }

           // else if (param.filterOn == "TOTAL_PRICE" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.TotalPrice).ToList();

           // }
           // else if (param.filterOn == "TOTAL_PRICE" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.TotalPrice).ToList();
           // }

           // else if (param.filterOn == "PAYMENT_STATUS" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.paymentStatus).ToList();

           // }
           // else if (param.filterOn == "PAYMENT_STATUS" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.paymentStatus).ToList();
           // }

           // else if (param.filterOn == "PAYMENT_TYPE" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.PaymentType).ToList();

           // }
           // else if (param.filterOn == "PAYMENT_TYPE" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.PaymentType).ToList();
           // }

           // else if (param.filterOn == "INVOICE_NO" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.Id).ToList();

           // }
           // else if (param.filterOn == "INVOICE_NO" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.Id).ToList();
           // }

           // else if (param.filterOn == "STATUS" && param.filterType == "DESC")
           // {
           //     response = response.OrderByDescending(x => x.Status).ToList();

           // }
           // else if (param.filterOn == "STATUS" && param.filterType == "ASC")
           // {
           //     response = response.OrderBy(x => x.Status).ToList();
           // }

           // else if (param.filterOn == "DRIVER_FULLNAME" && param.filterType == "DESC")
           // {
           //         response = response.Where(x => x.DriverDetails.Count() > 0).ToList();
           //         response = response.OrderByDescending(x => x.DriverDetails.FirstOrDefault().FirstName).ToList();

           // }
           // else if (param.filterOn == "DRIVER_FULLNAME" && param.filterType == "ASC")
           // {

           //         response = response.Where(x => x.DriverDetails.Count() >0).ToList();
           //          response = response.OrderBy(x => x.DriverDetails.FirstOrDefault().FirstName).ToList();
                
           // }
           // else
           // {

           // }

            return response;
        }

        //SelectOrderlist
        public List<OrderModel> SelectOrderlist(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }
            if (param.district != null)
            {
                param.districtId = Convert.ToInt64(CryptoEngine.Decrypt(param.district, KeyConstant.Key));
            }
            if (param.subdistrict != null)
            {
                param.subdistrictId = Convert.ToInt64(CryptoEngine.Decrypt(param.subdistrict, KeyConstant.Key));
            }
            if (param.driver != null)
            {
                param.driverId = Convert.ToInt64(CryptoEngine.Decrypt(param.driver, KeyConstant.Key));
            }
            if (param.orderId != null)
            {
                param.RecordId = Convert.ToInt64(param.orderId.TrimStart(new Char[] { '0' }));
            }
            if(param.filterOn==null)
            {
                param.filterOn = "DEFAULT";
            }

            var response = _orderDBService.SelectOrderlist(param);
          


           

            return response;
        }

        public List<OrderStatsModel> SelectOrderStats(SearchParam param)
        {
            var response = _orderDBService.OrderStats(param);
            return response;
        }
        public ResponseModel OrderItemUpdate(string service, string product, int? TenantId, long userId, int? quantity)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            param.UserId = userId;

            var order = _orderDBService.SelectOrder(param).FirstOrDefault();
            if (order == null || order.Id == null)
            {
                response.Status = false;
                response.Message = "No Order found";
                return response;
            }
            var productId = Convert.ToInt64(CryptoEngine.Decrypt(product, KeyConstant.Key));
            var serviceId = Convert.ToInt64(CryptoEngine.Decrypt(service, KeyConstant.Key));
            order.OrderItems = _xmlService.GetOrderItemsByXml(order.OrderItemXml);
            var checkAlreadyInOrder = order.OrderItems.Where(x => x.ProductId == productId && x.ServiceId == x.ServiceId).FirstOrDefault();

            if (checkAlreadyInOrder == null)
            {
                response.Status = false;
                response.Message = "Product cannot find in Order";
                return response;
            }
            int? totalItems = 0;

            order.OrderItems.ForEach(x =>
            {
                if (x.Id == checkAlreadyInOrder.Id)
                {
                    x.Quantity = quantity;
                    x.IsDeleted = x.Quantity == 0 ? true : x.IsDeleted;
                }
                totalItems += x.Quantity;
            });
            order.TotalItems = quantity;
            _orderItemDBService.OrderItemXMLSaveApp(order.OrderItems.XmlSerialize());


            SearchParam Settingparam = new SearchParam();
            var services = _appSettingService.ApplicationSettingSelect(Settingparam);

            decimal sameDayRate = Convert.ToDecimal(services.SAMEDAY_DELIVERY_RATE);
            decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
            decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);
            decimal logisticCharge = Convert.ToDecimal(services.logisticCharge);
            decimal minimumOrderAmount = Convert.ToDecimal(services.minimumOrderAmount);

            order.SubTotal = getSubtotal(order);            // cart.SubTotal;
            order.Discount = order.Discount != null ? order.Discount : 0;
            order.Tax = Convert.ToDecimal(String.Format("{0:0.00}", getTax(order.SubTotal)));
            order.DeliveryPrice = getDeliveryFees(order, order.DeliveryType, sameDayRate, expressRate, normalRate);
            order.LogisticCharge = getLogisticCharge(order.SubTotal, order.DeliveryPrice, order.Tax, logisticCharge, minimumOrderAmount);
            order.TotalPrice = getTotal(order.SubTotal, order.DeliveryPrice, order.Tax, order.LogisticCharge);

            _orderDBService.OrderUpdate(order);

            return response;
        }


        //new code 
        //OrderUpdateWithItem(OrderModel model);
        //  public ResponseModel OrderUpdateWithItem(OrderModel model)
        //  {

        //      ResponseModel response = new ResponseModel();
        //      response.Status = true;
        //      SearchParam param = new SearchParam();
        ////      param.UserId = model.UserId;
        //      param.RecordId = model.Id;

        //      var orderList = _orderDBService.SelectMyOrderDetails(param);
        //      orderList.ForEach(x =>
        //      {
        //          x.OrderItems = _xmlService.GetOrderItemsByXml(x.OrderItemXml);
        //      });
        //      var order = orderList.FirstOrDefault();

        //      if (order == null || order.Id == null)
        //      {
        //          response.Status = false;
        //          response.Message = "No Order found";
        //          return response;
        //      }


        //      //testing 
        //      foreach (var item in model.OrderItems)
        //      {
        //          var productId = item.ProductId;
        //          var serviceId = item.ServiceId;
        //          order.OrderItems = _xmlService.GetOrderItemsByXml(order.OrderItemXml);
        //          var checkAlreadyInOrder = order.OrderItems.Where(x => x.ProductId == productId && x.ServiceId == x.ServiceId).FirstOrDefault();
        //          int? totalItems = 0;
        //          if (checkAlreadyInOrder == null)
        //          {

        //              //response.Status = false;
        //              //response.Message = "Product cannot find in Order";
        //              //return response;
        //              totalItems += item.Quantity;
        //          }
        //          else
        //          {


        //              order.OrderItems.ForEach(x =>
        //              {
        //                  if (x.Id == checkAlreadyInOrder.Id)
        //                  {
        //                      x.Quantity = item.Quantity;
        //                      x.IsDeleted = x.Quantity == 0 ? true : x.IsDeleted;
        //                  }
        //                  totalItems += x.Quantity;
        //              });
        //          }
        //          order.TotalItems = item.Quantity;
        //          _orderItemDBService.OrderItemXMLSaveApp(order.OrderItems.XmlSerialize());

        //      }
        //      SearchParam Settingparam = new SearchParam();
        //      var services = _appSettingService.ApplicationSettingSelect(Settingparam);

        //      decimal sameDayRate = Convert.ToDecimal(services.SAMEDAY_DELIVERY_RATE);
        //      decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
        //      decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);
        //      decimal logisticCharge = Convert.ToDecimal(services.logisticCharge);
        //      decimal minimumOrderAmount = Convert.ToDecimal(services.minimumOrderAmount);

        //      order.SubTotal = getSubtotal(order);            // cart.SubTotal;
        //      order.Discount = order.Discount != null ? order.Discount : 0;
        //      order.Tax = Convert.ToDecimal(String.Format("{0:0.00}", getTax(order.SubTotal)));
        //      order.DeliveryPrice = getDeliveryFees(order, order.DeliveryType, sameDayRate, expressRate, normalRate);
        //      order.LogisticCharge = getLogisticCharge(order.SubTotal, order.DeliveryPrice, order.Tax, logisticCharge, minimumOrderAmount);
        //      order.TotalPrice = getTotal(order.SubTotal, order.DeliveryPrice, order.Tax, order.LogisticCharge);

        //      _orderDBService.OrderUpdate(order);

        //      return response;
        //  }

        public ResponseModel OrderUpdateWithItem(OrderModel model)
        {

            ResponseModel response = new ResponseModel();
            response.Status = true;
            SearchParam param = new SearchParam();
            //      param.UserId = model.UserId;
            param.RecordId = model.Id;

            var orderList = _orderDBService.SelectMyOrderDetails(param);
            orderList.ForEach(x =>
            {
                x.OrderItems = _xmlService.GetOrderItemsByXml(x.OrderItemXml);
            });
            var order = orderList.FirstOrDefault();
             
            if (order == null || order.Id == null)
            {

             
                response.Status = false;
                response.Message = "No Order found";
                return response;
            }


            //testing 
            foreach (var item in model.OrderItems)
            {
                var productId = item.ProductId;
                var serviceId = item.ServiceId;

                SearchParam paramProduct = new SearchParam();
                paramProduct.ServiceId = item.ServiceId;
                var Product = _productCustomerDBService.GetProductPrice(paramProduct, item.ProductId.GetValueOrDefault()).FirstOrDefault(); ;
                if(Product==null)
                {
                    response.Status = false;
                    response.Message = "No Product found";
                    return response;
                }
                // order.OrderItems = _xmlService.GetOrderItemsByXml(order.OrderItemXml);
                var checkAlreadyInOrder = order.OrderItems.Where(x => x.ProductId == productId && x.ServiceId == serviceId).FirstOrDefault();
                int? totalItems = 0;
                OrderItemModel Newitem = new OrderItemModel();
            
                if (checkAlreadyInOrder == null && item.Id == 0)   // case for new item added 
                {
                    Newitem.ProductId = item.ProductId;
                    Newitem.Quantity = item.Quantity;
                    Newitem.OrderId = model.Id;
                    Newitem.IsActive = true;
                    Newitem.IsDeleted = model.IsDeleted;
                    Newitem.PackingId = item.PackingId;
                    Newitem.TenantId = model.TenantId;
                    Newitem.SubTotal = Product.Price;
                    Newitem.TotalPrice = item.Quantity * Product.Price;
                    Newitem.CreatedBy = model.UpdatedBy;
                    Newitem.IsPacking = item.IsPacking;
                    Newitem.ServiceId = item.ServiceId;
                    Newitem.PackingId = item.PackingId != null ? item.PackingId : 0;
                    Newitem.PackingPrice = item.PackingPrice!=null?item.PackingPrice:0;
                    var id = _orderitemDBService.OrderItemInsert(Newitem);
                  
                }
                else if (checkAlreadyInOrder == null && item.Id != 0) // case for product or service change 
                {
                    Newitem.Id = item.Id;
                    Newitem.Quantity = item.Quantity;
                    Newitem.ProductId = item.ProductId;
                    Newitem.ServiceId = item.ServiceId;
                    Newitem.UpdatedBy = model.UpdatedBy;
                    Newitem.IsDeleted = item.Quantity == 0 ? true : item.IsDeleted;
                    Newitem.IsActive = item.IsDeleted == false ? true : false;
                    Newitem.PackingId = item.PackingId;
                    Newitem.TenantId = model.TenantId;
                    Newitem.SubTotal = Product.Price;
                    Newitem.TotalPrice = item.Quantity != 0 ? item.Quantity * Product.Price : 0;
                    _orderItemDBService.OrderItemUpdate(Newitem);
                }
                else  if(checkAlreadyInOrder!=null && item.IsDeleted==true)                                                    // item deleted 
                {

                        Newitem.Id = item.Id;
                        Newitem.IsDeleted = item.IsDeleted;
                        Newitem.IsActive = false;
                        Newitem.Quantity = 0;
                        Newitem.SubTotal = 0;
                        Newitem.TotalPrice = 0;
                    Newitem.TenantId = model.TenantId;
                       _orderItemDBService.OrderItemUpdate(Newitem);
                }
                else if (checkAlreadyInOrder != null && item.IsDeleted == false)                                                    // quantity
                {

                    if (checkAlreadyInOrder.Quantity != item.Quantity)
                    {
                        Newitem.Id = item.Id;
                        Newitem.Quantity = item.Quantity;
                        Newitem.ProductId = item.ProductId;
                        Newitem.ServiceId = item.ServiceId;
                        Newitem.UpdatedBy = model.UpdatedBy;
                        Newitem.IsDeleted = item.Quantity == 0 ? true : item.IsDeleted;
                        Newitem.IsActive = item.IsDeleted == false ? true : false;
                        Newitem.PackingId = item.PackingId;
                        Newitem.TenantId = model.TenantId;
                        Newitem.SubTotal = Product.Price;
                        Newitem.TotalPrice = item.Quantity != 0 ? item.Quantity * Product.Price : 0;
                        _orderItemDBService.OrderItemUpdate(Newitem);
                    }
                }
                else
                {

                }
                order.TotalItems = item.Quantity;

               
               // _orderItemDBService.OrderItemXMLSaveApp(order.OrderItems.XmlSerialize());

            }

            SearchParam OrderParam = new SearchParam();
            //      param.UserId = model.UserId;
            OrderParam.RecordId = model.Id;

            var UpdatedOrders = _orderDBService.SelectMyOrderDetails(OrderParam) ;
            int itemCount = 0;
            UpdatedOrders.ForEach(x =>
            {
                x.OrderItems = _xmlService.GetOrderItemsByXml(x.OrderItemXml);

                foreach (var item in x.OrderItems)
                {
                    itemCount = itemCount + item.Quantity.GetValueOrDefault();
                }

            });

           

           
            

            var UpdatedOrder = UpdatedOrders.FirstOrDefault();
            SearchParam Settingparam = new SearchParam();
            var services = _appSettingService.ApplicationSettingSelect(Settingparam);

            decimal sameDayRate = Convert.ToDecimal(services.SAMEDAY_DELIVERY_RATE);
            decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
            decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);
            decimal logisticCharge = Convert.ToDecimal(services.logisticCharge);
            decimal minimumOrderAmount = Convert.ToDecimal(services.minimumOrderAmount);
            //UpdatedOrder.TotalItems = UpdatedOrder.OrderItems.Count();
            UpdatedOrder.TotalItems = itemCount;


            UpdatedOrder.SubTotal = getSubtotal(UpdatedOrder);            // cart.SubTotal;
            UpdatedOrder.Discount = UpdatedOrder.Discount != null ? UpdatedOrder.Discount : 0;
            UpdatedOrder.Tax = Convert.ToDecimal(String.Format("{0:0.00}", getTax(UpdatedOrder.SubTotal)));
            UpdatedOrder.DeliveryPrice = getDeliveryFees(UpdatedOrder, UpdatedOrder.DeliveryType, sameDayRate, expressRate, normalRate);
            UpdatedOrder.LogisticCharge = getLogisticCharge(UpdatedOrder.SubTotal, UpdatedOrder.DeliveryPrice, UpdatedOrder.Tax, logisticCharge, minimumOrderAmount);
            UpdatedOrder.TotalPrice = getTotal(UpdatedOrder.SubTotal, UpdatedOrder.DeliveryPrice, UpdatedOrder.Tax, UpdatedOrder.LogisticCharge);

            _orderDBService.OrderUpdate(UpdatedOrder);

            return response;
        }


        


        public List<OrderModel> SelectDriverActiveOrder(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _orderDBService.SelectDriverActiveOrder(param);
            response.ForEach(x =>
            {
                x.DeliveryAddress = _xmlService.GetCustomerAddressesByXml(x.DeliveryAddressXml);
            });
            return response;
        }

        //
        public List<OrderModel> SelectDriverRides(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            var response = _orderDBService.SelectDriverRides(param);
            response.ForEach(x =>
            {
                x.DeliveryAddress = _xmlService.GetCustomerAddressesByXml(x.DeliveryAddressXml);
            });
            return response;
        }
        public decimal getSubtotal(OrderModel order)
        {
            decimal total = 0.00m;

            foreach (var item in order.OrderItems)
            {
                //  total = (total + Convert.ToInt32(item.Quantity) * item.Price).GetValueOrDefault();
                total = (total + item.TotalPrice).GetValueOrDefault();

            }

            return total;
        }

        public decimal? getTax(decimal? subtotal)
        {
            decimal? taxAmount = 0.0m;

            return subtotal * taxAmount;
        }


        public decimal? getTotal(decimal? subtotal, decimal? DeliveryFees, decimal? Tax, decimal? logisticCharges)
        {

            return subtotal + DeliveryFees + Tax + logisticCharges;
        }

        public decimal? getLogisticCharge(decimal? subtotal, decimal? DeliveryFees, decimal? Tax, decimal? logisticCharge, decimal? minimumOrder)
        {
            decimal? total = subtotal + DeliveryFees + Tax;



            if (total < minimumOrder)
            {
                return logisticCharge;
            }
            return 0;
        }

        public decimal? getDeliveryFees(OrderModel model, string DeliveryType, decimal sameDayRate, decimal expressRate, decimal normalRate)
        {
            //SearchParam param = new SearchParam();
            //var services = _appSettingCustomerService.ApplicationSettingSelect(param);

            //decimal sameDayRate = Convert.ToDecimal( services.SAMEDAY_DELIVERY_RATE);
            //decimal expressRate = Convert.ToDecimal(services.EXPRESS_DELIVERY_RATE);
            //decimal normalRate = Convert.ToDecimal(services.NORMAL_DELIVERY_RATE);

            if (DeliveryType.ToLower() == "sameday")
            {
                return model.SubTotal * sameDayRate;
            }
            if (DeliveryType.ToLower() == "express")
            {

                return model.SubTotal * expressRate;
            }
            if (DeliveryType.ToLower() == "normal")
            {
                return model.SubTotal * normalRate;
            }
            return sameDayRate;
        }

        //SelectMyActiveOrder
        public List<OrderModel> SelectMyActiveOrder(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            //var response = _orderDBService.SelectMyActiveOrder(param);
            //response.ForEach(x =>
            //{
            //});



            var response = _orderDBService.SelectMyOrderDetails(param);
            response.ForEach(x =>
            {
                x.OrderItems = _xmlService.GetOrderItemsByXml(x.OrderItemXml);
                //    x.DeliveryAddress = _xmlService.GetUserAddressesByXml(x.DeliveryAddressXml);
                x.DeliveryAddress = _xmlService.GetCustomerAddressesByXml(x.DeliveryAddressXml);
                x.DriverDetails = _xmlService.GetDriverByXml(x.DriverXml);
                x.customerdetails = _xmlService.GetUsersByXml(x.customerXml).FirstOrDefault();

            });

            var rs = response.Where(s => s.Status == OrderConstants.AWAITING_COLLECTION || s.Status == OrderConstants.ON_THE_WAY).ToList();

            return rs;
        }

    }
}
