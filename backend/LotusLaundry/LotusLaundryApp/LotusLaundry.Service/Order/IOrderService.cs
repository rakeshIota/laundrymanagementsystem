﻿using LotusLaundry.Domain.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.Service.Order
{
    public interface IOrderService
    {
        /// <summary>
        /// used for insertion order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long OrderInsert(OrderModel model);
        
        /// <summary>
        /// Method for update order
        /// </summary>
        /// <param name="model"></param>
        void OrderUpdate(OrderModel model);

        void OrderPaymentUpdate(long id,string qrCode,string paymentStatus,long? updatedby);

       // void OrderPaymentUpdate(long id, string txno, string paymentStatus, long updatedby);



        void OrderStatusUpdate(OrderModel model);

        /// <summary>
        /// use to select all order or select order by id 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<OrderModel> SelectOrder(SearchParam param);
        List<OrderModel> SelectMyOrder(SearchParam param);
        List<OrderModel> SelectMyActiveOrder(SearchParam param);
        List<OrderModel> SelectMyOrderDetails(SearchParam param);

        List<OrderModel> SelectOrderlist(SearchParam param);
        List<OrderStatsModel> SelectOrderStats(SearchParam param);

        List<OrderModel> SelectDriverActiveOrder(SearchParam param);

        List<OrderModel> SelectDriverRides(SearchParam param);

        ResponseModel OrderItemUpdate(string service, string product, int? TenantId, long userId, int? quantity);

        void OrderComplete(OrderModel model);

        ResponseModel OrderUpdateWithItem(OrderModel model);
        ResponseModel OrderLogInsert(long? responseId, long? Userid, int? TenantId, string Note = null);

    }
}
