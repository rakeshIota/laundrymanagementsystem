﻿using CarersCouch.Domain.UserTwilioCalling;
using System;
using System.Linq;

namespace CarersCouch.DBRepository.UserTwilioCalling
{
    public class UserTwilioCallingDBService
    {
        CarersCouchDbEntities DbContext { get { return new CarersCouchDbEntities(); } }

        public void UserCallingStatusUpdate(UserTwilioCallingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserCallingStatusUpdate(model.FromUserId, model.ToUserId, model.IsActive);
            }
        }

        public bool CheckUserCallingStatus(UserTwilioCallingModel model)
        {
            using (var dbctx = DbContext)
            {
                var isActive = dbctx.CheckUserCallingStatus(model.FromUserId, model.ToUserId).FirstOrDefault();
                return Convert.ToBoolean(isActive);
            }
        }

        public void UserCallingStatusDeactivate(long? UserId)
        {
            using (var dbctx = DbContext)
            {
               dbctx.UserCallingStatusDeactivate(UserId);
            }
        }
    }
}
