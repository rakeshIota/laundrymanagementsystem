﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Enums
{
    public enum OrderStatus
    {
        NEW,
        ASSIGNED_DRIVER,
        AWAITING_COLLECTION,
        COLLECTED,
        CLEANING_IN_PROGRESS,
        CLEANING_COMPLETED,
        ASSIGNED_DRIVER_FOR_DELIVERY,
        ON_THE_WAY,
        DELIVERED,
        CANCELLED,
    }
}
