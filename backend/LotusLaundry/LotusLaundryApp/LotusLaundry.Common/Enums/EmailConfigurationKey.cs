﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Enums
{
    public enum EmailConfigurationKey
    {
        EmailVerification,
        AccountActivated,
        AccountDeactivated,
        ForgotPassword,
        ContactEnquiry,
        WelcomeEmail,
        EmailOTPVerification,
        OrderEmail,
        NewOrderRecevied,
        OrderCancel,
        ForgotPasswordAdminPanel,
        orderDelivered
    }
    public enum UserRegistered
    {
        AlreadyRegister,
        NowRegitered,
        Failed
    }
    public enum Gender
    {
        MALE,
        FEMALE
    }

    public enum DeliveryType
    {
        NORMAL
    }
    public enum AddressType
    {
        CONTACT,
        PICKUP_DELIVERY
    }
}
