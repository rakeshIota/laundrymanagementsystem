﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Enums
{
    public enum PushNotificationType
    {
        ORDER_PLACED,
         ASSIGNED_DRIVER_FOR_COLLECTION,
         AWAITING_COLLECTION,
         COLLECTED,
        CLEANING_IN_PROGRESS,
        CLEANING_COMPLETED,
        ASSIGNED_DRIVER_FOR_DELIVERY,
        ON_THE_WAY,
        DELIVERED,
        CANCELLED,
        ORDER_MODIFIED_BY_OPERATION_MANAGER,
        ORDER_UPDATE,
        ORDER_PAYMENT_FAILURE,
        ORDER_PAYMENT_COMPLETE,
        ORDER_CHAT_NOTIFICATION,

        CALL_PICKED

    }
}
