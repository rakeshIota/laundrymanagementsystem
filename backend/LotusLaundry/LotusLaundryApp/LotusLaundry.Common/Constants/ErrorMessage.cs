﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LotusLaundry.Common.Constants
{
    public partial class ErrorMessage
    {
        public class AccountError
        {
            public const string INCORRECT = "Username or password incorrect.";
            public const string ACTIVATE = "Your account is not activated by admin.";
            public const string CONFIRM = "Your account is not confirm";
            public const string DELETED = "User Does not Exist.";
            public const string INVALID_LINK = "Invalid Confirmation Link.";
            public const string INVALID_EMAIL = "Email does not register with us";
            public const string INVALID = "Invalid Request.";
            public const string EMAIL_ALREADY_REGISTERED = "Email Id {0} is already registered with another account.";
            public const string EMAIL_ALREADY_EXISTS = "Email is already registered with another account.";
            public const string PHONE_ALREADY_EXISTS = "Mobile Number is already registered with another account.";
            public const string PHONENUMBER_ALREADY_CONFIRM = "Phone number already verify";
            public const string INVALID_OTP = "OTP is invalid";
            public const string OTP_LIMIT = "Exceed the limit";
            public const string DISTRICT_NOT_ALLOW_TO_DELETE = "District Cannot Delete While Its Subdistrict Exists";
            public const string CITY_NOT_ALLOW_TO_DELETE = "City Cannot Delete While Its district Exists";
            public const string STATE_NOT_ALLOW_TO_DELETE = "State Cannot Delete While Its city Exists";
            public const string COUNTRY_NOT_ALLOW_TO_DELETE = "Country Cannot Delete While Its State Exists";

            public const string ACCOUNT_LOCK_INCORRECT = "Your account is locked. Please use forget password to recover your account";
            public const string EMAIL_NOT_VeriFied = "Email verification is pending,Please verify first.";
            public const string MOBILE_NOT_VeriFied = "Mobile verification is pending,Please verify first.";
            public const string EMAIL_Already_VeriFied = "Email already verified.";

            //Mobile number is already linked with your account
            public const string PHONENUMBER_ALREADY_REGISTERED = "Mobile number is already linked with your account";

            public const string USER_ALREADY_REGISTERED = "User Name {0} is already registered with another account.";

            public const string NATIONALID_ALREADY_REGISTERED = "User with national id {0} is already registered with another account.";
            public const string LICENCE_ALREADY_REGISTERED = "User with Licence id {0} is already registered with another account.";
            public const string PlATE_ALREADY_REGISTERED = "User with same License plate {0} is already registered with another account.";
            public const string Bike_ALREADY_REGISTERED = "User with same bike number {0} is already registered with another account.";


            //INVALID_USERID
            public const string INVALID_USERID = "Invalid UserID.";
            public const string DRIVERHASODERTODELIVER = "Driver has some active orders";
            //“  adress related msgs 



        }
        public class OrderError
        {
            public const string INAVLID_REQUEST = "Invalid Request";
            public const string INAVLID_PAYMENTTYPE = "Invalid Payment Type";
            public const string INAVLID_ADDRESS = "Invalid Address";

            public const string CART_IN_PAYMENT_MODE = "Your cart in payment mode";
            public const string UNABLE_TO_ORDER = "Unable to Order";
            public const string PAYMENT_FAILED = "Payment Failed";
            public const string ORDER_PLACED = "Order has been placed successfully";

            public const string PRICE_UPGRADED = "Price Issue";

            public const string DRIVER_ASSIGNED = "Driver Assigned";
            public const string ORDER_CANCELLED_ALREADY = "ORDER IS ALREADY CANCELLED";

            public const string QR_CODE_NOT_GENERATED = "QR Code not generated ";

            //"Order has been placed successfully"

        }
    }
}