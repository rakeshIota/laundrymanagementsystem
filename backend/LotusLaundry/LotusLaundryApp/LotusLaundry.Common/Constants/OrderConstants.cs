﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Constants
{
    public class OrderConstants
    {
        //New
        public const string NEW = "NEW";
        //  pickup procee
         public const string ASSIGNED_DRIVER = "ASSIGNED_DRIVER";
        public const string AWAITING_COLLECTION = "AWAITING_COLLECTION";
        public const string COLLECTED = "COLLECTED";
        //CLeaning
        public const string CLEANING_IN_PROGRESS = "CLEANING_IN_PROGRESS";
        public const string CLEANING_COMPLETED = "CLEANING_COMPLETED";
        //Delivery process
        public const string ASSIGNED_DRIVER_FOR_DELIVERY = "ASSIGNED_DRIVER_FOR_DELIVERY";       
        public const string ON_THE_WAY = "ON_THE_WAY";
        //Delivered
        public const string DELIVERED = "DELIVERED";
        //Cancelled
        public const string CANCELLED = "CANCELLED";
        
        //Payment
        public const string PAYMENTPAIDSTATUS = "PAID";
        public const string PAYMENTUNPAINSTATUS = "UNPAID";


        public const string REFUND = "REFUND";


















    }
}
