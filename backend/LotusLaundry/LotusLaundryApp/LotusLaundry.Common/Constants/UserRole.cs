﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Constants
{
    public class UserRole
    {
        public const string Admin = "ROLE_ADMIN";
        public const string User = "ROLE_USER";
        public const string Customer = "ROLE_CUSTOMER";
        public const string Driver = "ROLE_DRIVER";
        public const string OPERATIONMANAGER = "ROLE_OPERATION";
        public const string SUPERADMIN = "ROLE_SUPER_ADMIN";
        //ROLE_SUPER_ADMIN
    }
}
