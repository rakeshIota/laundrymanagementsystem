﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Constants
{
    public class FileType
    {
        public const string TYPE_FOLDER = "Folder";
        public const string USER = "USER";
        public const string USERS = "USERS";
        public const string FEEDBACK = "FEEDBACK";
        public const string USERDEVICE = "USERDEVICE";
        public const string PASSWORDLOG = "PASSWORDLOG";
        public const string MESSAGELOG = "MESSAGELOG";
        public const string NOTIFICATION = "NOTIFICATION";
        public const string APPSETTING = "APPSETTING";
        public const string COUNTRY = "COUNTRY";
        public const string COUNTRYLANGUAGEMAPPING = "COUNTRYLANGUAGEMAPPING";
        public const string LANGUAGE = "LANGUAGE";
        public const string STATE = "STATE";
        public const string STATELANGUAGEMAPPING = "STATELANGUAGEMAPPING";
        public const string CITY = "CITY";
        public const string CITYLANGUAGEMAPPING = "CITYLANGUAGEMAPPING";
        public const string USERADDRESS = "USERADDRESS";
        public const string DISTRICT = "DISTRICT";
        public const string DISTRICTLANGUAGEMAPPING = "DISTRICTLANGUAGEMAPPING";
        public const string SUBDISTRICT = "SUBDISTRICT";
        public const string SUBDISTRICTLANGUAGEMAPPING = "SUBDISTRICTLANGUAGEMAPPING";
        public const string PROVINCE = "PROVINCE";
        public const string PROVINCELANGUAGEMAPPING = "PROVINCELANGUAGEMAPPING";
        public const string USERCORDINATE = "USERCORDINATE";
        public const string SERVICE = "SERVICE";
        public const string SERVICE_IMAGE = "SERVICE_IMAGE";
        public const string SERVICELANGUAGEMAPPING = "SERVICELANGUAGEMAPPING";
        public const string PRODUCT = "PRODUCT";
        public const string PRODUCT_IMAGE = "PRODUCT_IMAGE";
        public const string PRODUCTLANGUAGEMAPPING = "PRODUCTLANGUAGEMAPPING";
        public const string PRODUCTPRICE = "PRODUCTPRICE";
        public const string DELIVERYTYPE = "DELIVERYTYPE";
        public const string DELIVERYTYPELANGUAGEMAPPING = "DELIVERYTYPELANGUAGEMAPPING";
        public const string CART = "CART";
        public const string CARTITEM = "CARTITEM";
        public const string CARTITEMSERVICEMAPPING = "CARTITEMSERVICEMAPPING";
        public const string ORDER = "ORDER";
        public const string ORDERITEM = "ORDERITEM";
        public const string ORDERITEMSERVICEMAPPING = "ORDERITEMSERVICEMAPPING";
        public const string ORDERLOG = "ORDERLOG";
        public const string PAYMENTLOG = "PAYMENTLOG";
        public const string PACKING = "PACKING";
        public const string PACKINGLANGUAGEMAPPING = "PACKINGLANGUAGEMAPPING";
        public const string DEFAULTMESSAGE = "DEFAULTMESSAGE";
        public const string DEFAULTMESSAGELANGUAGEMAPPING = "DEFAULTMESSAGELANGUAGEMAPPING";
        public const string CURRENCY = "CURRENCY";
        public const string CURRENCYLANGUAGEMAPPING = "CURRENCYLANGUAGEMAPPING";
        public const string POSTALCODE = "POSTALCODE";
        public const string NOTIFICATIONMASTERLANGUAGEMAPPING = "NOTIFICATIONMASTERLANGUAGEMAPPING";
        public const string NOTIFICATIONMASTER = "NOTIFICATIONMASTER";
        public const string NOTIFICATIONMASTERKEYVALUEMAPPING = "NOTIFICATIONMASTERKEYVALUEMAPPING";
        public const string FILEGROUPITEM = "FILEGROUPITEM";
        //USERS_PROFILEPIC
        public const string PROFILEPIC = "USERS_PROFILEPIC";
        public const string IDCARDFRONT = "DRIVER_IDCARDFRONT";
        public const string IDCARDBACK = "DRIVER_IDCARDBACK";
        public const string DRIVERLICENCEFRONT = "DRIVER_DRIVERLICENCEFRONT";
        public const string DRIVERLICENCEBACK = "DRIVER_DRIVERLICENCEBACK";
        public const string ROLEMASTER = "ROLEMASTER";

        public const string USERCARDDETAIL = "USERCARDDETAIL";
        public const string LOCATION = "LOCATION";
        public const string Rating = "Rating";

        public const string PROMOTION = "PROMOTION";


        //PROJECTDOCUMENT

    }

    // change key name according to the project name
    public class KeyConstant
    {
        public const string Key = "LotusL@undry@$$#";
    }
}
