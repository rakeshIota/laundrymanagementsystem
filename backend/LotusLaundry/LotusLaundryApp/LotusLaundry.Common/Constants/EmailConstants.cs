﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Constants
{
    public class EmailConstants
    {
        public const string CONFIRM_YOUR_ACCOUNT = "Verify your email address";
        public const string FORGOT_PASSWORD = "Forgot Password";
        public const string ACCOUNT_ACTIVATED = "Account Activated";
        public const string ACCOUNT_DEACTIVATED = "Account Deactivated";
        public const string CONTACT_ENQUIRY = "Contact Enquiry";
        public const string WELOME_EMAIL = "Welcome to Lotus Laundry!";
        public const string OTP_VERIFICATION = "OTP For Verification";
        public const string CONFIRM_YOUR_EMAIL = "Verification Reminder Email";
        public const string RECEIVE_ORDER = "New Order Received";
        public const string ORDER_PlACED = "Your Lotus Laundry Order {0}";
        public const string ORDER_DELIVERED = "Delivery Confirmation: Lotus Laundry Order {0}";


        public const string ORDER_UPDATED = "Your Lotus Laundry Order {0} Has Been Updated BY Operation Manager ";
        public const string CANCEL_ORDER = "Your Lotus Laundry Order {0} is cancelled";
        public const string CHANGE_PASSWORD = "Change Password";
        //
        //Verification Reminder Email

    }
}
