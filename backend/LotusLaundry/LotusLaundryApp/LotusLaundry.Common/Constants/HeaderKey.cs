﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Constants
{
   public class HeaderKey
    {
        public const string Language = "Language";
        public const string Authorization = "Authorization";
    }
}
