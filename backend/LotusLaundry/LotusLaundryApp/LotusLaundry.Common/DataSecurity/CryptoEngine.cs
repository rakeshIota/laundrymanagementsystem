﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace LotusLaundry.Common.DataSecurity
{
   public class CryptoEngine
    {         
        public static string Encrypt(string input, string key)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();

            var id = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            id = System.Web.HttpUtility.UrlEncode(id);
            id = id.Replace("%", "5AHAM8");
            return id;
            //return Convert.ToBase64String(resultArray, 0, resultArray.Length).Replace('/', '*');
        }
        
        public static string Decrypt(string input, string key)
        {
            var id = input.Replace("5AHAM8", "%");
            id = System.Web.HttpUtility.UrlDecode(id);
            byte[] inputArray = Convert.FromBase64String(id);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            id = UTF8Encoding.UTF8.GetString(resultArray);
            var val = id.Split('_');
            return val[1];
        }
    }
}
