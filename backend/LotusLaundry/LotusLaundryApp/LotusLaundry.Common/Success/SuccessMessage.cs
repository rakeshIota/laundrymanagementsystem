﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Success
{
   public class SuccessMessage
    {
        public class AccountSuccess
        {
            public const string USER_REGISTERED = "Account Registered Successfully, We have sent a confirmation account link to your email";

            public const string MOBILE_VERIFY = "We have sent a otp to your register mobile number";
            public const string PASSWORD_CHANGED = "Password Changed Successfully. Please login";
            public const string EMAIL_CONFIRM = "Your email confirm";
            public const string ACTIVATED = "User Activated Successfully";
            public const string DEACTIVATED = "User DeActivated Successfully";
            public const string DELETED = "User Deleted SuccessFully";
            public const string USER_DETAIL_UPDATE = "User detail update SuccessFully";
            public const string FORGOT_PASSWORD_LINK = "We have sent password change link to your email";
            public const string PROFILE_UPDATE = "Profile Updated Successfully";
            public const string RESEND_OTP = "Resend otp Successfully";

            public const string MOBILEANDEMAIL_VERIFY = "We have sent a otp to your register mobile number and a verification link to your email id";
            public const string MOBILE_VERIFY_CONFIRM = "Mobile number verified successfully";
            public const string FORGOT_PASSWORD_OTP = "We have sent an OTP to your registered email,please verify";

            public const string EMAIL_VERIFY_LINK = "Please check your email at {0}.";
            public const string SEND_OTP_SMSCONTENT = "Use {0} OTP to reset password";

            //Please check your email at {0}
        }

        public class OrderSucces
        {
            public const string ORDER_CANCEL = "Order is cancelled successfully";
        }
        public class AccountFailure
        {
            public const string PASSWORD_SAME = "The new password must differ from your previous password";
            public const string OLD_PASSWORD_NOT_MATCHED = "Old password does not match";
            public const string INVALID_USER = "Invalid user please login.";

            //Old password does not match Invalid user please login.
        }
    }
}
