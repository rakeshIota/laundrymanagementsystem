﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Common.Success
{
   public class PushNotificationMSG
    {
        public class Title
        {
            public const string Order_placed = "Order Confirmed";
            public const string DRIVER_ASSIGNED_FOR_COLLECTION = "Driver assigned to pickup order";
            public const string AWAITING_COLLECTION = "Driver on-way to pickup order";
            public const string COLLECTED = "Order Collected";
            public const string CLEANING_IN_PROGRESS = "Cleaning in Progress";
            public const string CLEANING_COMPLETED = "Cleaning Completed";
            public const string ASSIGNED_DRIVER_FOR_DELIVERY = "Driver assigned for delivery";
            public const string ON_THE_WAY = "Driver on-way to deliver order";
            public const string DELIVERED = "Order Delivered";
            public const string ORDER_CANCEL_ByUSER = "Order Cancelled by customer";
            public const string ORDER_CANCEL_MANAGER = "Order Cancelled by Lotus Laundry";


            public const string ORDER_CANCEL = "Order cancelled successfully";
            

            public const string ORDER_ASSIGNED_FOR_COLLECTION = "New order is assigned for Pickup";
            public const string ORDER_ASSIGNED_FOR_DELIVERY = "New order is assigned for Delivery";

            public const string DRIVER_ON_THE_WAY = "Driver is on the way pickup the order ";
           
            //AWAITING_COLLECTION ON_THE_WAY  
           
            public const string ORDER_PAYMENT_COMPLETE = "Payment received successfully";
            public const string ORDER_PAYMENT_FAILURE = "Payment Failed";





            public const string ORDER_UPDATE = "Order Updated";

            public const string ORDER_PICKUP_DATE_UPDATE = "Order Modified";
            public const string ORDER_DELIVERY_DATE_UPDATE = "Order Modified";



        }

        public class message
        {
            public const string ORDER_CANCEL = "Order id {0} has been cancelled successfully, please check your email for more details .";
            public const string ORDER_PlACED = "Order id {0} has been confirmed please check your email for more details .";
            public const string DRIVER_ASSIGNED_FOR_COLLECTION = "{0} having vehicle number {1} is assigned to your Order id {2}.";
            public const string AWAITING_COLLECTION = "{0} having vehicle number {1} is on the way to collect your Order id {2}.";
            public const string COLLECTED = "order id {0} has been successfully collected";
            public const string CLEANING_IN_PROGRESS = "We have begun cleaning your items for Order Id {0}";
            public const string CLEANING_COMPLETED = "We have finished cleaning your items for Order Id {0}";
            public const string  ASSIGNED_DRIVER_FOR_DELIVERY = "{0} having vehicle number {1} is assigned to deliver your Order Id {2}.";
            public const string ON_THE_WAY = "{0} having vehicle number {1} is on the way to deliver your Order Id {2}.";
            public const string DELIVERED = "order id {0} has been successfully delivered";

            public const string ORDER_CANCEL_BYUser = "Order id {0} has been cancelled successfully, please check your email for more details .";
            public const string ORDER_CANCEL_BYManager = "Order id {0} has been cancelled, please check your email for more details .";
            public const string ORDER_CANCEL_BYManager_Driver = "Order id {0} has been cancelled, please proceed to next order's task.";



            //public const string ORDER_ASSIGNED_FOR_COLLECTION = "Order no. {0} has been assigned for pickup.";
            public const string ORDER_ASSIGNED_FOR_COLLECTION = "Order Id {0} has been assigned for pickup on {1} between  {2}";

            public const string ORDER_ASSIGNED_FOR_DELIVERY = "Order Id {0} has been assigned for delivery on {1} between {2}.";




            public const string ORDER_PAYMENT_COMPLETE = "Order {0} Payment Successfully";
            public const string ORDER_PAYMENT_FAILURE = "Order {0} Payment Failed";






            public const string ORDER_ITEM_UPDATE = "Items of order id {0} has been Successfully updated by Operation Manager, Please Check your email for more details ";
            public const string ORDER_ITEM_DELIVERY_UPDATE = "Delivery date for Order Id {0} has been updated";
            public const string ORDER_ITEM_PICKUP_UPDATE = "Pickup date for Order Id {0} has been updated";



            public const string ORDER_UPDATE = "order id {0} has been updated by Operation Manager Successfully";




            public const string ORDER_PICKUP_DATE_UPDATE = "Order Id {0} has been modified, pick-up on {1} between {2}";
            public const string ORDER_DELIVERY_DATE_UPDATE = "Order Id {0} has been modified, delivery on {1} between {2}";




        }
        public class AccountFailure
        {
            public const string PASSWORD_SAME = "The new password must differ from your previous password";
            public const string OLD_PASSWORD_NOT_MATCHED = "Old password does not match";
            public const string INVALID_USER = "Invalid user please login.";

            //Old password does not match Invalid user please login.
        }
    }
}
