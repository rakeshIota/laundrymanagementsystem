﻿using LotusLaundry.Domain.DefaultMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.DefaultMessage
{
    public class DefaultMessageDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DefaultMessageInsert(DefaultMessageModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DefaultMessageInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Type,model.message).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DefaultMessageUpdate(DefaultMessageModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DefaultMessageUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Type, model.message);
            }

        }
        public List<DefaultMessageModel> SelectDefaultMessage(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DefaultMessageSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DefaultMessageModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Type = x.Type,
                            message=x.message,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
