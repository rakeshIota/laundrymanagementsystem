﻿using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Service
{
    public class ServiceDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ServiceInsert(ServiceModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ServiceInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.Description).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ServiceUpdate(ServiceModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ServiceUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Name,model.Description);
            }

        }
        public List<ServiceModel> SelectService(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ServiceSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ServiceModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    ServiceLanguageXml = x.ServiceLanguageXml,
                    Description = x.Description,
                    ImageXml =  x.ImageXml
                }).ToList();
            }
        }
    }
}
