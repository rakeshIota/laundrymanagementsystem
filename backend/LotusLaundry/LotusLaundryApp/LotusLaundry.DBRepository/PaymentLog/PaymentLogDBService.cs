﻿using LotusLaundry.Domain.PaymentLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Payment;

namespace LotusLaundry.DBRepository.PaymentLog
{
    public class PaymentLogDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PaymentLogInsert(PaymentLogModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PaymentLogInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.CardNumber,model.Amount,model.OrderId,model.TransactionId,model.Status).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        //rLogInsert(PaymentQRcodeModel model);
        public long QrLogInsert(PaymentQRcodeModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.QrLogInsert(model.tenantid, "", model.CreatedBy, true, model.userid, model.qrCode, model.amount, model.orderid, model.partnerTxnUid, model.statusCode,model.output,model.authkey,model.txnCurrencyCode.ToString(),model.loyaltyId,model.txnNo,model.additionalInfo).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PaymentLogUpdate(PaymentLogModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PaymentLogUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.CardNumber,model.Amount,model.OrderId,model.TransactionId,model.Status);
            }

        }
        public List<PaymentLogModel> SelectPaymentLog(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PaymentLogSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new PaymentLogModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			CardNumber = x.CardNumber,
	            			Amount = x.Amount,
	            			OrderId = x.OrderId,
	            			TransactionId = x.TransactionId,
	            			Status = x.Status,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
