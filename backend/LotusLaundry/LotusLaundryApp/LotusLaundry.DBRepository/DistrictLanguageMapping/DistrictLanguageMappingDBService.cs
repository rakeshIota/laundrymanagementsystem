﻿using LotusLaundry.Domain.DistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.DistrictLanguageMapping
{
    public class DistrictLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DistrictLanguageMappingInsert(DistrictLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DistrictLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.DistrictId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DistrictLanguageMappingUpdate(DistrictLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DistrictLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.DistrictId);
            }

        }
        public void DistrictLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DistrictLanguageMappingXMLSave(xml);
            }

        }
        public List<DistrictLanguageMappingModel> SelectDistrictLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DistrictLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DistrictLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			DistrictId = x.DistrictId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
