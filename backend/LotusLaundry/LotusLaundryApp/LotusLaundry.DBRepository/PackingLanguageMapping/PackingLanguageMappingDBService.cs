﻿using LotusLaundry.Domain.PackingLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.PackingLanguageMapping
{
    public class PackingLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PackingLanguageMappingInsert(PackingLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PackingLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.PackingId,model.LanguageId,model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PackingLanguageMappingUpdate(PackingLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PackingLanguageMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.PackingId,model.LanguageId,model.Name);
            }

        }
        public List<PackingLanguageMappingModel> SelectPackingLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PackingLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new PackingLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			PackingId = x.PackingId,
	            			LanguageId = x.LanguageId,
	            			Name = x.Name,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
