﻿using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.DeliveryTypeLanguageMapping
{
    public class DeliveryTypeLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DeliveryTypeLanguageMappingInsert(DeliveryTypeLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DeliveryTypeLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.DeliveryTypeId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DeliveryTypeLanguageMappingUpdate(DeliveryTypeLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DeliveryTypeLanguageMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Name,model.LanguageId,model.DeliveryTypeId);
            }

        }
        public List<DeliveryTypeLanguageMappingModel> SelectDeliveryTypeLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DeliveryTypeLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DeliveryTypeLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			DeliveryTypeId = x.DeliveryTypeId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
