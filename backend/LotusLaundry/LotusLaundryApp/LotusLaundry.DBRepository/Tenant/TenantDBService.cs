﻿using LotusLaundry.Domain.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.DBRepository.Tenant
{
    public class TenantDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public TenantModel TenantSelect(Guid uniqueId)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.TenantSelect(uniqueId).Select(x => new TenantModel
                {
                    Id=x.Id,
                    Address=x.Address,
                    Name=x.Name,
                    Email=x.Email,
                    PhoneNo=x.PhoneNo,
                    UniqueId=x.UniqueId
                }).FirstOrDefault();
            }
        }
    }
}
