﻿using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.NotificationMasterKeyValueMapping
{
    public class NotificationMasterKeyValueMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long NotificationMasterKeyValueMappingInsert(NotificationMasterKeyValueMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.NotificationMasterKeyValueMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Key,model.Value,model.NotificationId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void NotificationMasterKeyValueMappingUpdate(NotificationMasterKeyValueMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationMasterKeyValueMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Key, model.Value, model.NotificationId);
            }

        }
        public void NotificationMasterKeyValueMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationMasterKeyValueMappingXMLSave(xml);
            }

        }
        public List<NotificationMasterKeyValueMappingModel> SelectNotificationMasterKeyValueMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.NotificationMasterKeyValueMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new NotificationMasterKeyValueMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Key = x.Key,
	            			Value = x.Value,
	            			NotificationId = x.NotificationId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
