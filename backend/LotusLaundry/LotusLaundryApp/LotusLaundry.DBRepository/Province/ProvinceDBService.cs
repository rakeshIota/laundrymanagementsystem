﻿using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Province
{
    public class ProvinceDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ProvinceInsert(ProvinceModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ProvinceInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ProvinceUpdate(ProvinceModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProvinceUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name);
            }

        }
        public List<ProvinceModel> SelectProvince(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProvinceSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ProvinceModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Name = x.Name,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    ProvinceLanguageXml =  x.ProvinceLanguageXml
                }).ToList();
            }
        }
    }
}
