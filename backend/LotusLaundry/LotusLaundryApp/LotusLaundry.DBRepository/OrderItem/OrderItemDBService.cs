﻿using LotusLaundry.Domain.OrderItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.OrderItem
{
    public class OrderItemDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long OrderItemInsert(OrderItemModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.OrderItemInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.OrderId,model.ProductId,model.Quantity,model.IsPacking,model.PackingPrice,model.SubTotal,model.TotalPrice,model.PackingId,model.ServiceId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void OrderItemUpdate(OrderItemModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderItemUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.OrderId,model.ProductId,model.Quantity,model.IsPacking,model.PackingPrice,model.SubTotal,model.TotalPrice,model.PackingId, model.ServiceId);
            }

        }

        //OrderItemUpdate
        public void OrderItemUpdateByService(OrderItemModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderItemUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.OrderId, model.ProductId, model.Quantity, model.IsPacking, model.PackingPrice, model.SubTotal, model.TotalPrice, model.PackingId, model.ServiceId);
            }

        }
        public List<OrderItemModel> SelectOrderItem(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderItemSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new OrderItemModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			OrderId = x.OrderId,
	            			ProductId = x.ProductId,
	            			Quantity = x.Quantity,
	            			IsPacking = x.IsPacking,
	            			PackingPrice = x.PackingPrice,
	            			SubTotal = x.SubTotal,
	            			TotalPrice = x.TotalPrice,
	            			PackingId = x.PackingId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public void OrderItemXMLSaveApp(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderItemXMLSaveApp(xml);
            }

        }
    }
}
