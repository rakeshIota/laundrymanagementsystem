﻿using LotusLaundry.Domain.PasswordLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.PasswordLog
{
    public class PasswordLogDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PasswordLogInsert(PasswordLogModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PasswordLogInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.Count).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PasswordLogUpdate(PasswordLogModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PasswordLogUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.Count);
            }

        }
        public List<PasswordLogModel> SelectPasswordLog(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PasswordLogSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId,param.UserId).Select(x => new PasswordLogModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			Count = x.Count,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
