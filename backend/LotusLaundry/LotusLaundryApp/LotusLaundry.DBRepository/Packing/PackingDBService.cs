﻿using LotusLaundry.Domain.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Packing
{
    public class PackingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PackingInsert(PackingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PackingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.ProductId,model.ServiceId,model.Price).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PackingUpdate(PackingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PackingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.ProductId,model.ServiceId,model.Price);
            }

        }
        public List<PackingModel> SelectPacking(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PackingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new PackingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			ProductId = x.ProductId,
	            			ServiceId = x.ServiceId,
	            			Price = x.Price,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
