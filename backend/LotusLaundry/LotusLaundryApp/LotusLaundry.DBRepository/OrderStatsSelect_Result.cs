//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LotusLaundry.DBRepository
{
    using System;
    
    public partial class OrderStatsSelect_Result
    {
        public string TotalOrderTitle { get; set; }
        public Nullable<int> TotalOrderCount { get; set; }
        public Nullable<int> TotalNormalOrderCount { get; set; }
        public Nullable<int> TotalSameDayOrderCount { get; set; }
        public Nullable<int> TotalExpressOrderCount { get; set; }
        public string NewOrderTitle { get; set; }
        public Nullable<int> NewOrderTotalCount { get; set; }
        public Nullable<int> NewSameDayOrderCount { get; set; }
        public Nullable<int> NewExpressOrderCount { get; set; }
        public Nullable<int> NewNormalOrderCount { get; set; }
        public string CollectionOrderTitle { get; set; }
        public Nullable<int> CollectionOrderTotalCount { get; set; }
        public Nullable<int> CollectionAssignedOrderCount { get; set; }
        public Nullable<int> CollectionCollectedOrderCount { get; set; }
        public string CleaningOrderTitle { get; set; }
        public Nullable<int> CleaningOrderTotalCount { get; set; }
        public Nullable<int> CleaningInprogressOrderCount { get; set; }
        public Nullable<int> CleaningCompletedOrderCount { get; set; }
        public string DeliveryOrderTitle { get; set; }
        public Nullable<int> DeliveryOrderTotalCount { get; set; }
    }
}
