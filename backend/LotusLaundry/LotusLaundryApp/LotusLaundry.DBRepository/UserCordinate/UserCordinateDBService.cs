﻿using LotusLaundry.Domain.UserCordinate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.UserCordinate
{
    public class UserCordinateDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long UserCordinateInsert(UserCordinateModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.UserCordinateInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.Latitude,model.Longitude,model.OrderId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void UserCordinateUpdate(UserCordinateModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserCordinateUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.Latitude,model.Longitude,model.OrderId);
            }

        }
        public List<UserCordinateModel> SelectUserCordinate(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserCordinateSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new UserCordinateModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			Latitude = x.Latitude,
	            			Longitude = x.Longitude,
	            			OrderId = x.OrderId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
