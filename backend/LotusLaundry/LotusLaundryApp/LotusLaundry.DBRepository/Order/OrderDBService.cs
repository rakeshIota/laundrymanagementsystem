﻿using LotusLaundry.Domain.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Order
{
    public class OrderDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long OrderInsert(OrderModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.OrderInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.UserId, model.DeliveryDate, model.DeliveryType, model.TotalItems, model.DeliveryPrice, model.DriverId, model.Status, model.PaymentType, model.PaidAmount, model.Tax, model.SubTotal, model.TotalPrice, model.AdressId, model.ExpectedPickUpMin, model.ExpectedPickUpMax, model.ExpectedDeliveryMin, model.ExpectedDeliveryMax, model.CartId, null, model.PickupDate, model.deliverySlot, model.pickupSlot, model.LogisticCharge, model.Discount, model.paymentResponse, model.paymentStatus, model.change, model.note,model.qrCode,model.laundryInstruction).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void OrderUpdate(OrderModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.UserId, model.DeliveryDate, model.DeliveryType, model.TotalItems, model.DeliveryPrice, model.DriverId, model.Status, model.PaymentType, model.PaidAmount, model.Tax, model.SubTotal, model.TotalPrice, model.AdressId, model.ExpectedPickUpMin, model.ExpectedPickUpMax, model.ExpectedDeliveryMin, model.ExpectedDeliveryMax, model.CartId, null, model.PickupDate, model.deliverySlot, model.pickupSlot, model.LogisticCharge, model.Discount, model.paymentResponse, model.paymentStatus, model.change, model.note,model.adminNote);
            }

        }
        //OrderPaymentUpdate
        public void OrderPaymentUpdate(long id, string qrCode, string paymentStatus,long updatedby)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderPaymentUpdate(id, updatedby, paymentStatus, qrCode);
            }

        }
        public void OrderStatusUpdate(OrderModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderStatusUpdate(model.Id, model.TenantId, model.UpdatedBy, model.UserId, model.Status);
            }

        }

        public List<OrderModel> SelectOrder(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    UserId = x.UserId,
                    DeliveryDate = x.DeliveryDate,
                    DeliveryType = x.DeliveryType,
                    TotalItems = x.TotalItems,
                    DeliveryPrice = x.DeliveryPrice,
                    DriverId = x.DriverId,
                    Status = x.Status,
                    PaymentType = x.PaymentType,
                    PaidAmount = x.PaidAmount,
                    Tax = x.Tax,
                    SubTotal = x.SubTotal,
                    TotalPrice = x.TotalPrice,
                    //AdressId = x.AdressId,
                    ExpectedPickUpMin = x.ExpectedPickUpMin,
                    ExpectedPickUpMax = x.ExpectedPickUpMax,
                    ExpectedDeliveryMin = x.ExpectedDeliveryMin,
                    ExpectedDeliveryMax = x.ExpectedDeliveryMax,
                    CartId = x.CartId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }



        public List<OrderModel> SelectMyOrderDetails(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderSelectApp(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId,param.districtId,param.subdistrictId,param.driverId,param.status,param.name,param.email,param.phoneNumber,param.deliveryType).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    // IsLocked = x.IsLocked,
                    UserId = x.Userid,
                    Status = x.Status,
                    Tax = x.Tax,
                    SubTotal = x.SubTotal,
                    TotalPrice = x.TotalPrice,
                    TotalItems = x.TotalItems,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    OrderItemXml = x.OrderItemXml,
                    DeliveryAddressXml = x.AddressXml,
                    DeliveryType = x.DeliveryType,
                    DeliveryPrice = x.deliveryprice,
                    LogisticCharge = x.LogisticCharge,
                    DriverXml = x.DriverXml,
                    DeliveryDate = x.Deliverydate,
                    deliverySlot = x.deliverySlot,
                    pickupSlot = x.pickupSlot,
                    PickupDate = x.Pickupdate,
                    paymentResponse = x.Paymentresponse,
                    paymentStatus = x.paymentstatus,
                    PaymentType = x.paymentType,
                    customerXml=x.CustomerXml,
                    adminNote=x.adminnote,
                    deliveredDate=x.deliveredDate,
                    paymentDate=x.paymentDate,
                    collectedDate=x.collectedDate,
                    isRating=  x.rating
                    ,
                    change=x.change,
                    DeliveryDriverName=x.DeliveryDriverXml,
                    PickupDriverName=x.PickUpDriverXml,
                    qrCode=x.qrcode,
                    laundryInstruction=x.LaundryInstruction,
                    Isdelivered= x.IsDelivered,
                   activeOrders= x.activeorder.GetValueOrDefault(),
                }).ToList();
            }
        }

        //
        public List<OrderModel> SelectOrderlist(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderList(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId, param.districtId, param.subdistrictId, param.driverId, param.status, param.name, param.email, param.phoneNumber, param.deliveryType,param.filterOn,param.filterType).Select(x => new OrderModel
                {
                    Id = x.id,                   
                    IsDeleted =false,
                    IsActive = true,                   
                    UserId = x.CustomerId,
                    Status = x.status,        
                    TotalPrice = x.totalPrice,
                    DeliveryType = x.DeliveryType,
                    DeliveryDate = x.DeliveryDate,
                    PickupDate=x.PickupDate,
                    paymentStatus = x.paymentStatus,
                    PaymentType = x.paymentType,
                    CreatedOn=x.createdon,
                    customerName=x.Customername,
                    districtName=x.districtname,
                    subDistrictName=x.Subdistrictname,
                    driverName =x.driverName,
                    DriverId=x.DriverId,
                    pickupSlot=x.pickupslot,
                    deliverySlot=x.deliverySlot,
                    change=x.change,
                    TotalCount = x.overall_count.GetValueOrDefault(),

                }).ToList();
            }
        }


        public List<OrderModel> SelectMyOrderlist(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.AllOrderSelectApp(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId, param.IsClose).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TotalItems = x.TotalItems,
                    //paymentResponse=x.Paymentresponse,
                    CreatedOn = x.CreatedOn,
                    Status = x.Status,
                    paymentStatus = x.PaymentStatus,
                    PaymentType = x.paymenttype,
                    DeliveryType = x.deliveryType,

                    PickupDate = x.PickupDate,
                    DeliveryDate = x.DeliveryDate,
                    pickupSlot = x.pickupSlot,
                    deliverySlot = x.deliverySlot,
                    TotalPrice = x.TotalPrice,



                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
        //SelectDriverActiveOrder
        public List<OrderModel> SelectDriverRides(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DriverCompleteRidesSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId, param.isCompleted, param.driverId, param.date, param.Type).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TotalItems = x.TotalItems,
                    //paymentResponse=x.Paymentresponse,
                    CreatedOn = x.CreatedOn,
                    Status = x.Status,
                    paymentStatus = x.PaymentStatus,
                    PaymentType = x.paymenttype,
                    DeliveryType = x.deliveryType,

                    PickupDate = x.PickupDate,
                    DeliveryDate = x.DeliveryDate,
                    pickupSlot = x.pickupSlot,
                    deliverySlot = x.deliverySlot,
                    TotalPrice = x.TotalPrice,
                    DeliveryAddressXml = x.AddressXml,
                    customerName = x.customerName,
                    customerId = x.customerid,
                    RideType=x.RideType,

                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public List<OrderModel> SelectDriverActiveOrder(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DriverActiveOrderSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId, param.isCompleted, param.driverId,param.date,param.Type).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TotalItems = x.TotalItems,
                    //paymentResponse=x.Paymentresponse,
                    CreatedOn = x.CreatedOn,
                    Status = x.Status,
                    paymentStatus = x.PaymentStatus,
                    PaymentType = x.paymenttype,
                    DeliveryType = x.deliveryType,

                    PickupDate = x.PickupDate,
                    DeliveryDate = x.DeliveryDate,
                    pickupSlot = x.pickupSlot,
                    deliverySlot = x.deliverySlot,
                    TotalPrice = x.TotalPrice,
                    DeliveryAddressXml=x.AddressXml,
                   customerName=x.customerName,
                  customerId=x.customerid,
                   qrCode=x.QRCode,

                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
        public List<OrderStatsModel> OrderStats(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderStatsSelect(param.TenantId).Select(x => new OrderStatsModel
                {
                    TotalOrderTitle = x.TotalOrderTitle,
                    TotalOrderCount = x.TotalOrderCount,
                    TotalNormalOrderCount = x.TotalNormalOrderCount,
                    TotalSameDayOrderCount = x.TotalSameDayOrderCount,
                    TotalExpressOrderCount = x.TotalExpressOrderCount,
                    NewOrderTitle = x.NewOrderTitle,
                    NewOrderTotalCount = x.NewOrderTotalCount,
                    NewSameDayOrderCount = x.NewSameDayOrderCount,
                    NewExpressOrderCount = x.NewExpressOrderCount,
                    NewNormalOrderCount = x.NewNormalOrderCount,
                    CollectionOrderTitle = x.CollectionOrderTitle,
                    CollectionOrderTotalCount = x.CollectionOrderTotalCount,
                    CollectionAssignedOrderCount = x.CollectionAssignedOrderCount,
                    CollectionCollectedOrderCount = x.CollectionCollectedOrderCount,
                    CleaningOrderTitle = x.CleaningOrderTitle,
                    CleaningOrderTotalCount = x.CleaningOrderTotalCount,
                    CleaningInprogressOrderCount = x.CleaningInprogressOrderCount,
                    CleaningCompletedOrderCount = x.CleaningCompletedOrderCount,
                    DeliveryOrderTitle = x.DeliveryOrderTitle,
                    DeliveryOrderTotalCount = x.DeliveryOrderTotalCount
                }).ToList();
            }
        }


      
        public List<OrderModel> SelectMyActiveOrder(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.AllActiveOrderSelectApp(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId, param.LanguageId, param.UserId, param.IsClose).Select(x => new OrderModel
                {
                    Id = x.Id,
                    TotalItems = x.TotalItems,
                    //paymentResponse=x.Paymentresponse,
                    CreatedOn = x.CreatedOn,
                    Status = x.Status,
                    paymentStatus = x.PaymentStatus,
                    PaymentType = x.paymenttype,
                    DeliveryType = x.deliveryType,

                    PickupDate = x.PickupDate,
                    DeliveryDate = x.DeliveryDate,
                    pickupSlot = x.pickupSlot,
                    deliverySlot = x.deliverySlot,
                    TotalPrice = x.TotalPrice,



                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

    }
}
