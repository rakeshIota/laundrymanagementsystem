﻿using LotusLaundry.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.RoleMaster;
using LotusLaundry.Domain.UserDevice;

namespace LotusLaundry.DBRepository.Users
{
    public class UsersDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }
       
        public UsersModel UserProfileByUniqueCode(string uniqueCode)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.SelectUserProfileByUniqueCode(uniqueCode).Select(x => new UsersModel
                {
                    Email = x.Email,
                    FirstName = x.firstName,
                    FullName = x.firstName + " " + x.lastName,
                    LastName = x.lastName,
                    IsActive = x.IsActive,
                    Id = x.Id,
                    PhoneNumber = x.PhoneNumber,
                    ProfileImageUrl = x.ProfilePic,
                    UniqueCode = x.UniqueCode
                 }).FirstOrDefault();
            }
        }
        //

        public List<UsersModel> SelectUser(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserSelect(param.RecordId, param.Role, param.Next, param.Offset,param.firstName,param.lastName,param.phoneNumber,param.email,param.address,param.districtId,param.subdistrictId,param.lastOrderDate,param.registerDate,param.nickName, param.filterOn, param.filterType,param.searchText).Select(x => new UsersModel
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    IsDeleted = x.IsDeleted,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber,
                    UserName = x.UserName,
                    UniqueCode = x.UniqueCode,
                    TenantId = x.TenantId,
                    RoleName = x.RoleName,
                    Role=x.Role,
                    EmailConfirmed=x.EmailConfirmed,
                   DOB=x.DOB,
                   employeeid=x.EmployeeId,
                   NationalId=x.NationalId,
                   LicenceId=x.LicenceId,
                   BikeInformation=x.BikeInformation,
                   LicencePlate=x.LicencePlate,
                   Position=x.Position,
                   MobileNo=x.PhoneNumber,
                   NickName=x.NickName,
                   status=x.Status,
                   Gender=x.Gender,
                   orderId=x.lastOrder,
                   lastOrderDate=x.lastOrderDate,
                   addressXml=x.AddressXml,
                    //FileXml=x.FileXml,
                   adminNote =x.adminNote,
                    ordersCount = x.ordersCount.GetValueOrDefault(),
                   ProfileImageUrl =x.ProfilePic,
                   TotalCount = x.overall_count.GetValueOrDefault(),
                   adminCount=x.admincount.GetValueOrDefault(),
                }).ToList();
            }
        }

        public List<UsersModel> SelectDriver(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DriverUserSelect(param.RecordId, param.Role, param.Next, param.Offset,param.searchText).Select(x => new UsersModel
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    IsDeleted = x.IsDeleted,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber,
                    UserName = x.UserName,
                    UniqueCode = x.UniqueCode,
                    TenantId = x.TenantId,
                    RoleName = x.RoleName,
                    Role = x.Role,
                    DOB = x.DOB,
                    employeeid = x.EmployeeId,
                    NationalId = x.NationalId,
                    LicenceId = x.LicenceId,
                    BikeInformation = x.BikeInformation,
                    LicencePlate = x.LicencePlate,
                    Position = x.Position,
                    MobileNo = x.PhoneNumber,
                    workLoad = x.Order_Count,
                    status = x.Status,
                    FileXml=x.FileXml,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public List<RoleMasterModel> SelectAllRoles(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.RolesSelect(param.RecordId, param.Role, param.Next, param.Offset).Select(x => new RoleMasterModel
                {
                    Id = x.Id,
                    Role = x.Role,
                    Name = x.Name

                }).ToList();
            }
        }

        public List<UsersModel> SelectCustomer(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CustomerWithAddressSelect(param.RecordId, param.Role, param.Next, param.Offset,param.RelationTable,param.LRelationId,param.filterOn,param.filterType).Select(x => new UsersModel
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    IsDeleted = x.IsDeleted,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber,
                    UserName = x.UserName,
                    UniqueCode = x.UniqueCode,
                    TenantId = x.TenantId,
                    RoleName = x.RoleName,
                    Role = x.Role,
                    EmailConfirmed = x.EmailConfirmed,
                    DOB = x.DOB,
                    employeeid = x.EmployeeId,
                    NationalId = x.NationalId,
                    LicenceId = x.LicenceId,
                    BikeInformation = x.BikeInformation,
                    LicencePlate = x.LicencePlate,
                    Position = x.Position,
                    MobileNo = x.PhoneNumber,
                    NickName = x.NickName,
                    status = x.Status,
                    Gender = x.Gender,
                    FileXml=x.FileXml,
                    addressXml = x.AddressXml,
                    ProfileImageUrl=x.ProfilePic,
                    LanguageId=x.languageid,
                    adminNote=x.adminNote,
                    userName=x.UserName,
                   PhoneConfirmed=x.PhoneNumberConfirmed
                
                  

                }).ToList();
            }
        }

        public void UserDeviceSaveOrUpdate(UserDeviceModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserDeviceInsert(model.DeviceId,model.UserId,"mobile","", model.NotificationToken,model.IsDeleted);
            }
        }

        public void UserDeviceDelete(string id)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserDevicedelete(id);
            }
        }

        public void SetUserIdle()
        {
            using (var dbctx = DbContext)
            {
                dbctx.SetUserIdle(null,null);
            }
        }

        /*get user device information */

        public List<UserDeviceModel> UserDeviceSelect(Nullable<long> userId, string deviceType, string userType)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserDeviceSelect(userId, deviceType, userType).Select(x => new UserDeviceModel
                {
                    Id = x.Id,
                    CreatedOn = x.CreatedOn,
                    DeviceId = x.DeviceId,
                    
                    NotificationToken = x.DeviceToken,
                    DeviceType = x.DeviceType,
                    IsDeleted = x.IsDeleted,
                    UserId = x.UserId,
                   // UserType = x.UserType
                }).ToList();
            }
        }

        public List<UsersModel> UserBasicInfoSelect(string userIds)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserBasicInfoSelect(userIds).Select(x => new UsersModel
                {
                    Id = x.Id,
                    ProfileImageUrl = x.ProfilePic,
                    FirstName = x.FirstName,
                    RolesXml = x.RolesXml,
                }).ToList();
            }
        }

    }


}
