﻿using LotusLaundry.Domain.ProductPrice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.ProductPrice
{
    public class ProductPriceDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ProductPriceInsert(ProductPriceModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ProductPriceInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.ServiceId,model.ProductId,model.Price).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ProductPriceUpdate(ProductPriceModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProductPriceUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.ServiceId, model.ProductId, model.Price);
            }

        }
        public void ProductPriceXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProductPriceXMLSave(xml);
            }

        }
        public List<ProductPriceModel> SelectProductPrice(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProductPriceSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ProductPriceModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			ServiceId = x.ServiceId,
	            			ProductId = x.ProductId,
	            			Price = x.Price,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
