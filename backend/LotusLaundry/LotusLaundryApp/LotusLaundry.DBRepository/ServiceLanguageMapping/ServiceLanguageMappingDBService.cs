﻿using LotusLaundry.Domain.ServiceLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.ServiceLanguageMapping
{
    public class ServiceLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ServiceLanguageMappingInsert(ServiceLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ServiceLanguageMappingInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Name, model.LanguageId, model.ServiceId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ServiceLanguageMappingUpdate(ServiceLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ServiceLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.ServiceId);
            }

        }
        public List<ServiceLanguageMappingModel> SelectServiceLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ServiceLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ServiceLanguageMappingModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Name = x.Name,
                    LanguageId = x.LanguageId,
                    ServiceId = x.ServiceId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public void ServiceLanguageSaveXml(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ServiceLanguageMappingXMLSave(xml);
            }

        }
    }
}
