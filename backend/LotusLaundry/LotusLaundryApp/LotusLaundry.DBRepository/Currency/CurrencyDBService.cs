﻿using LotusLaundry.Domain.Currency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Currency
{
    public class CurrencyDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CurrencyInsert(CurrencyModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CurrencyInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Price).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CurrencyUpdate(CurrencyModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CurrencyUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Price);
            }

        }
        public List<CurrencyModel> SelectCurrency(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CurrencySelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CurrencyModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Price = x.Price,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
