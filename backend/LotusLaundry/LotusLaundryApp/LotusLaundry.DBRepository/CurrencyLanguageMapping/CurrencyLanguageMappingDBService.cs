﻿using LotusLaundry.Domain.CurrencyLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.CurrencyLanguageMapping
{
    public class CurrencyLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CurrencyLanguageMappingInsert(CurrencyLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CurrencyLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.CurrencyId,model.LanguageId,model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CurrencyLanguageMappingUpdate(CurrencyLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CurrencyLanguageMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.CurrencyId,model.LanguageId,model.Name);
            }

        }
        public List<CurrencyLanguageMappingModel> SelectCurrencyLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CurrencyLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CurrencyLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			CurrencyId = x.CurrencyId,
	            			LanguageId = x.LanguageId,
	            			Name = x.Name,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
