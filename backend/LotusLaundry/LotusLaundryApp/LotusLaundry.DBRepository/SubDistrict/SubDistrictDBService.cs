﻿using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.SubDistrict
{
    public class SubDistrictDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long SubDistrictInsert(SubDistrictModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.SubDistrictInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.DistrictId,model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void SubDistrictUpdate(SubDistrictModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.SubDistrictUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.DistrictId,model.Name);
            }

        }
        public List<SubDistrictModel> SelectSubDistrict(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.SubDistrictSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new SubDistrictModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    DistrictId = x.DistrictId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    DistrictName = x.DistrictName,
                    SubDistrictLanguageXml =  x.SubDistrictLanguageXml
                }).ToList();
            }
        }
    }
}
