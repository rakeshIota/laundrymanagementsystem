﻿using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.District
{
    public class DistrictDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DistrictInsert(DistrictModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DistrictInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Name,model.CityId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DistrictUpdate(DistrictModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DistrictUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name,model.CityId);
            }

        }
        public List<DistrictModel> SelectDistrict(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DistrictSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DistrictModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    CityName = x.CityName,
                    CityId = x.CityId,
                    DistrictLanguageXml = x.CityLanguageXml
                }).ToList();
            }
        }
    }
}
