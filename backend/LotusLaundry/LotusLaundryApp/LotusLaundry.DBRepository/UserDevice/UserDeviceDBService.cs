﻿using LotusLaundry.Domain.UserDevice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.UserDevice
{
    public class UserDeviceDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long UserDeviceInsert(UserDeviceModel model)
        {
            using (var dbctx = DbContext)
            {
               // var id = dbctx.UserDeviceInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.DeviceId,model.NotificationToken).FirstOrDefault();
                var id = dbctx.UserDeviceInsert(model.DeviceId, model.UserId, "", "",model.NotificationToken,model.IsDeleted).FirstOrDefault();

                return Convert.ToInt64(id ?? 0);
            }

        }
        public void UserDeviceUpdate(UserDeviceModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.UserDeviceUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.DeviceId,model.NotificationToken);
            }

        }
        public List<UserDeviceModel> SelectUserDevice(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.UserDeviceSelect(param.UserId,null,null).Select(x => new UserDeviceModel
                {
	            			Id = x.Id,
	            			//TenantId = x.TenantId,
	            			//Slug = x.Slug,
	            			//CreatedBy = x.CreatedBy,
	            			//UpdatedBy = x.UpdatedBy,
	            			//CreatedOn = x.CreatedOn,
	            			//UpdatedOn = x.UpdatedOn,
	            			//IsDeleted = x.IsDeleted,
	            			//IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			DeviceId = x.DeviceId,
	            			NotificationToken = x.DeviceToken,
                   
                }).ToList();
            }
        }
    }
}
