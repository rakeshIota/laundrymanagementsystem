﻿using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Language
{
    public class LanguageDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long LanguageInsert(LanguageModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.LanguageInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Name, model.Code).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void LanguageUpdate(LanguageModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.LanguageUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.Code);
            }

        }
        public List<LanguageModel> SelectLanguage(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.LanguageSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new LanguageModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Name = x.Name,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Code = x.Code,
                    IsDefault=x.IsDefault,
                }).ToList();
            }
        }
    }
}
