﻿using LotusLaundry.Domain.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Notification
{
    public class NotificationDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long NotificationInsert(NotificationModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.NotificationInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.Message,model.LanguageId,model.Type,model.Token,model.Status,model.Response).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void NotificationUpdate(NotificationModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.Message,model.LanguageId,model.Type,model.Token,model.Status,model.Response);
            }

        }
        public List<NotificationModel> SelectNotification(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.NotificationSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new NotificationModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			Message = x.Message,
	            			LanguageId = x.LanguageId,
	            			Type = x.Type,
	            			Token = x.Token,
	            			Status = x.Status,
	            			Response = x.Response,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
