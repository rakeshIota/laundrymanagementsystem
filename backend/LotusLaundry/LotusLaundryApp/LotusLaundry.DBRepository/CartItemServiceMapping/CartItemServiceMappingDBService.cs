﻿using LotusLaundry.Domain.CartItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.CartItemServiceMapping
{
    public class CartItemServiceMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CartItemServiceMappingInsert(CartItemServiceMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CartItemServiceMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.CartItemId,model.ServiceId,model.Quantity,model.Price,model.SubTotal,model.TotalPrice).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CartItemServiceMappingUpdate(CartItemServiceMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CartItemServiceMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.CartItemId,model.ServiceId,model.Quantity,model.Price,model.SubTotal,model.TotalPrice);
            }

        }
        public List<CartItemServiceMappingModel> SelectCartItemServiceMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CartItemServiceMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CartItemServiceMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			CartItemId = x.CartItemId,
	            			ServiceId = x.ServiceId,
	            			Quantity = x.Quantity,
	            			Price = x.Price,
	            			SubTotal = x.SubTotal,
	            			TotalPrice = x.TotalPrice,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
