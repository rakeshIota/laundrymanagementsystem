﻿using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Product
{
    public class ProductDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ProductInsert(ProductModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ProductInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Gender, model.Name,model.Rank).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ProductUpdate(ProductModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProductUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Gender, model.Name,model.Rank);
            }

        }
        public List<ProductModel> SelectProduct(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProductSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ProductModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    ImageXml = x.ImageXml,
                    Gender = x.Gender,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    ProductLanguageXml = x.ProductLanguageXml,
                    ProductPriceXml =  x.ProductPriceXml,
                    Rank=x.Rank.GetValueOrDefault(),

                }).ToList();
            }
        }
    }
}
