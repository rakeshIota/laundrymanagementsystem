﻿using LotusLaundry.Domain.DeliveryType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.DeliveryType
{
    public class DeliveryTypeDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DeliveryTypeInsert(DeliveryTypeModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DeliveryTypeInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.PriceType,model.Price).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DeliveryTypeUpdate(DeliveryTypeModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DeliveryTypeUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.PriceType,model.Price);
            }

        }
        public List<DeliveryTypeModel> SelectDeliveryType(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DeliveryTypeSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DeliveryTypeModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			PriceType = x.PriceType,
	            			Price = x.Price,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
