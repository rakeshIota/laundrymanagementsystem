﻿using LotusLaundry.Domain.Country;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Country
{
    public class CountryDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CountryInsert(CountryModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CountryInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CountryUpdate(CountryModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CountryUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name);
            }

        }
        public List<CountryModel> SelectCountry(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CountrySelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CountryModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    CountryLanguageXml =  x.CountryLanguageXml
                }).ToList();
            }
        }
    }
}
