﻿using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.NotificationMasterLanguageMapping
{
    public class NotificationMasterLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long NotificationMasterLanguageMappingInsert(NotificationMasterLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.NotificationMasterLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.LanguageId,model.Message,model.NotificationMasterId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void NotificationMasterLanguageMappingUpdate(NotificationMasterLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationMasterLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.LanguageId, model.Message, model.NotificationMasterId);
            }

        }

        public void NotificationMasterLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationMasterLanguageMappingXMLSave(xml);
            }

        }
        public List<NotificationMasterLanguageMappingModel> SelectNotificationMasterLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.NotificationMasterLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new NotificationMasterLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			LanguageId = x.LanguageId,
					 		LanguageXml = x.LanguageXml,
	            			Message = x.Message,
	            			NotificationMasterId = x.NotificationMasterId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
