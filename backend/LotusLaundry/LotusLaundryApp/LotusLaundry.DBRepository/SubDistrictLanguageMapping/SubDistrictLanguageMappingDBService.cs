﻿using LotusLaundry.Domain.SubDistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.SubDistrictLanguageMapping
{
    public class SubDistrictLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long SubDistrictLanguageMappingInsert(SubDistrictLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.SubDistrictLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.SubDistrictId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void SubDistrictLanguageMappingUpdate(SubDistrictLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.SubDistrictLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.SubDistrictId);
            }

        }


        public void SubDistrictLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.SubDistrictLanguageMappingXMLSave(xml);
            }

        }
        public List<SubDistrictLanguageMappingModel> SelectSubDistrictLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.SubDistrictLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new SubDistrictLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			SubDistrictId = x.SubDistrictId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
