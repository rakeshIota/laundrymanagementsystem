﻿using LotusLaundry.Domain.CityLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.CityLanguageMapping
{
    public class CityLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CityLanguageMappingInsert(CityLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CityLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.CityId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CityLanguageMappingUpdate(CityLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CityLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.CityId);
            }

        }
        public void CityLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CityLanguageMappingXMLSave(xml);
            }
        }
        public List<CityLanguageMappingModel> SelectCityLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CityLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CityLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			CityId = x.CityId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
