﻿using LotusLaundry.Domain.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.Feedback
{
    public class FeedbackDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long FeedbackInsert(FeedbackModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.FeedbackInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.OrderId,model.Type,model.Message,model.Rating,model.DriverId,model.UserId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void FeedbackUpdate(FeedbackModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.FeedbackUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.OrderId,model.Type,model.Message,model.Rating,model.DriverId,model.UserId);
            }

        }
        public List<FeedbackModel> SelectFeedback(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.FeedbackSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new FeedbackModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			OrderId = x.OrderId,
	            			Type = x.Type,
	            			Message = x.Message,
	            			Rating = x.Rating,
	            			DriverId = x.DriverId,
	            			UserId = x.UserId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
