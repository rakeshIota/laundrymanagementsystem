﻿using LotusLaundry.Domain.StateLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.StateLanguageMapping
{
    public class StateLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long StateLanguageMappingInsert(StateLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.StateLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.StateId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void StateLanguageMappingUpdate(StateLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.StateLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.StateId);
            }

        }
        public void StateLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.StateLanguageMappingXMLSave(xml);
            }

        }
        public List<StateLanguageMappingModel> SelectStateLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.StateLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new StateLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			StateId = x.StateId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
