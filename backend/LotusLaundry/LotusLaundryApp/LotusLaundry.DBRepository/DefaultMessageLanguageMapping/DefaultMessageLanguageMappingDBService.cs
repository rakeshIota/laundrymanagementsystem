﻿using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.DefaultMessageLanguageMapping
{
    public class DefaultMessageLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long DefaultMessageLanguageMappingInsert(DefaultMessageLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DefaultMessageLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.DefaultMessageId,model.LanguageId,model.Message).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void DefaultMessageLanguageMappingUpdate(DefaultMessageLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DefaultMessageLanguageMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.DefaultMessageId,model.LanguageId,model.Message);
            }

        }
        public List<DefaultMessageLanguageMappingModel> SelectDefaultMessageLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DefaultMessageLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new DefaultMessageLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			DefaultMessageId = x.DefaultMessageId,
	            			LanguageId = x.LanguageId,
	            			Message = x.Message,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
