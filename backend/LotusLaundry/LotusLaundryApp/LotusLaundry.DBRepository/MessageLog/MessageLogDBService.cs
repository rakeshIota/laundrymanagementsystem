﻿using LotusLaundry.Domain.MessageLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.MessageLog
{
    public class MessageLogDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long MessageLogInsert(MessageLogModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.MessageLogInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.UserId,model.Message,model.LanguageId,model.Type).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void MessageLogUpdate(MessageLogModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.MessageLogUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.UserId,model.Message,model.LanguageId,model.Type);
            }

        }
        public List<MessageLogModel> SelectMessageLog(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.MessageLogSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new MessageLogModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			UserId = x.UserId,
	            			Message = x.Message,
	            			LanguageId = x.LanguageId,
	            			Type = x.Type,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
