﻿using LotusLaundry.Domain.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.City
{
    public class CityDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CityInsert(CityModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CityInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.StateId, model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CityUpdate(CityModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CityUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.StateId, model.Name);
            }

        }
        public List<CityModel> SelectCity(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CitySelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CityModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    StateId = x.StateId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    StateName = x.StateName,
                    CityLanguageXml = x.CityLanguageXml
                }).ToList();
            }
        }
    }
}
