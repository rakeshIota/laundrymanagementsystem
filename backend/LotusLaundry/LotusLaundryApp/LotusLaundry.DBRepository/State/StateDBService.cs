﻿using LotusLaundry.Domain.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.State
{
    public class StateDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long StateInsert(StateModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.StateInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.CountryId, model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void StateUpdate(StateModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.StateUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.CountryId, model.Name);
            }

        }
        public List<StateModel> SelectState(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.StateSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new StateModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    CountryId = x.CountryId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Name = x.Name,
                    StateLanguageXml = x.StateLanguageXml,
                    CountryName =  x.CountryName
                }).ToList();
            }
        }
    }
}
