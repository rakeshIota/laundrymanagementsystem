﻿using LotusLaundry.Domain.OrderLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.OrderLog
{
    public class OrderLogDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long OrderLogInsert(OrderLogModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.OrderLogInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.OrderId,model.Note).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void OrderLogUpdate(OrderLogModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderLogUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.OrderId,model.Note);
            }

        }
        public List<OrderLogModel> SelectOrderLog(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderLogSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new OrderLogModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			OrderId = x.OrderId,
	            			Note = x.Note,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
