﻿using LotusLaundry.Domain.OrderItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.OrderItemServiceMapping
{
    public class OrderItemServiceMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long OrderItemServiceMappingInsert(OrderItemServiceMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.OrderItemServiceMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.OrderItemId,model.ServiceId,model.Quantity,model.Price,model.SubTotal,model.TotalPrice).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void OrderItemServiceMappingUpdate(OrderItemServiceMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.OrderItemServiceMappingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.OrderItemId,model.ServiceId,model.Quantity,model.Price,model.SubTotal,model.TotalPrice);
            }

        }
        public List<OrderItemServiceMappingModel> SelectOrderItemServiceMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.OrderItemServiceMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new OrderItemServiceMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			OrderItemId = x.OrderItemId,
	            			ServiceId = x.ServiceId,
	            			Quantity = x.Quantity,
	            			Price = x.Price,
	            			SubTotal = x.SubTotal,
	            			TotalPrice = x.TotalPrice,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
