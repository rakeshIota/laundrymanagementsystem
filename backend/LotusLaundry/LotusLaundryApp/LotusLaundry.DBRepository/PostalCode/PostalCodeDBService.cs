﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.PostalCode
{
    public class PostalCodeDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PostalCodeInsert(PostalCodeModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PostalCodeInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.PostalCode).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PostalCodeUpdate(PostalCodeModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PostalCodeUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.PostalCode);
            }

        }
        public List<PostalCodeModel> SelectPostalCode(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PostalCodeSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new PostalCodeModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			PostalCode = x.PostalCode,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public List<PostalCodeModel> ValidatePostalCode(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ValidatePostalCode(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId,param.postalCode).Select(x => new PostalCodeModel
                {
                    Id = x.Id,
                    IsActive=x.IsActive,
                    PostalCode=x.PostalCode
                }).ToList();
            }
        }
    }
}
