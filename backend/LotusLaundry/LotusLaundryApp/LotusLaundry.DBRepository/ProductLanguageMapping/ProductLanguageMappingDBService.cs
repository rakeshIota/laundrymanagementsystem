﻿using LotusLaundry.Domain.ProductLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.ProductLanguageMapping
{
    public class ProductLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long ProductLanguageMappingInsert(ProductLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.ProductLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.ProductId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void ProductLanguageMappingUpdate(ProductLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProductLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.ProductId);
            }

        }
        public void ProductLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.ProductLanguageMappingXMLSave(xml);
            }

        }
        public List<ProductLanguageMappingModel> SelectProductLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ProductLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new ProductLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			ProductId = x.ProductId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
