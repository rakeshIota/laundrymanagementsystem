﻿using LotusLaundry.Domain.NotificationMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.NotificationMaster
{
    public class NotificationMasterDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long NotificationMasterInsert(NotificationMasterModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.NotificationMasterInsert(model.TenantId, model.Slug, model.CreatedBy, model.IsActive, model.Type, model.CanBeDelete, model.Message).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void NotificationMasterUpdate(NotificationMasterModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.NotificationMasterUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Type, model.CanBeDelete, model.Message);
            }

        }
        public List<NotificationMasterModel> SelectNotificationMaster(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.NotificationMasterSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new NotificationMasterModel
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    Slug = x.Slug,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Type = x.Type,
                    CanBeDelete = x.CanBeDelete,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Message =  x.Message,
                    NotificationMasterKeyValueXml = x.NotificationMasterKeyValueXml,
                    NotificationMasterLanguageXml =  x.NotificationMasterLanguageXml
                }).ToList();
            }
        }
    }
}
