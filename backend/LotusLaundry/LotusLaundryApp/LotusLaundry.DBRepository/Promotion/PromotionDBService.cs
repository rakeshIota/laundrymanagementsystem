﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Promotion;

namespace LotusLaundry.DBRepository.Promotion
{
    public class PromotionDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long PromotionInsert(PromotionModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.PromotionInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.title, model.url).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void PromotionUpdate(PromotionModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.PromotionUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.title,model.url);
            }

        }
        public List<PromotionModel> SelectPromotion(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.PromotionSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new PromotionModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			title=x.Title,
                          
							FileXml = x.FileXml,						
	            			url=x.url,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
