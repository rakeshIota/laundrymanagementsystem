﻿using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.AppSetting
{
    public class AppSettingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long AppSettingInsert(AppSettingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.AppSettingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Key,model.Value).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void AppSettingUpdate(AppSettingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.AppSettingUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Key,model.Value);
            }

        }
        public List<AppSettingModel> SelectAppSetting(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.AppSettingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new AppSettingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Key = x.Key,
	            			Value = x.Value,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

        public List<AppSettingModel> AppSettingSelect(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.AppSettingSelectApp(param.RecordId, param.Next, param.Offset, param.TenantId
                    ).Select(x => new AppSettingModel
                    {
                        Key = x.Key,
                        Value = x.Value
                    }).ToList();
            }

        }
    }
}
