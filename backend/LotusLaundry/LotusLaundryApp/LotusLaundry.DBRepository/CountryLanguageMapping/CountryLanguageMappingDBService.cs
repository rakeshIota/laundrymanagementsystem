﻿using LotusLaundry.Domain.CountryLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;

namespace LotusLaundry.DBRepository.CountryLanguageMapping
{
    public class CountryLanguageMappingDBService
    {
        LotusLaundryDbEntities DbContext { get { return new LotusLaundryDbEntities(); } }

        public long CountryLanguageMappingInsert(CountryLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CountryLanguageMappingInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Name,model.LanguageId,model.CountryId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CountryLanguageMappingUpdate(CountryLanguageMappingModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CountryLanguageMappingUpdate(model.Id, model.TenantId, model.Slug, model.UpdatedBy, model.IsDeleted, model.IsActive, model.Name, model.LanguageId, model.CountryId);
            }

        }
        public void CountryLanguageMappingXMLSave(string xml)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CountryLanguageMappingXMLSave(xml);
            }

        }
        public List<CountryLanguageMappingModel> SelectCountryLanguageMapping(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CountryLanguageMappingSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new CountryLanguageMappingModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
	            			LanguageId = x.LanguageId,
	            			CountryId = x.CountryId,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
