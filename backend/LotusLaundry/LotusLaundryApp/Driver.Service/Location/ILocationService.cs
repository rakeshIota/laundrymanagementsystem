﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Location;

namespace Driver.Service.Location
{
    public interface ILocationService
    {
        /// <summary>
        /// used for insertion postalcode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long LocationInsert(LocationModel model);
        
        /// <summary>
        /// Method for update postalcode
        /// </summary>
        /// <param name="model"></param>
        void LocationUpdate(LocationModel model);

        /// <summary>
        /// use to select all postalcode or select postalcode by id 
        /// </summary>
        /// <param name="postalcodeId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<LocationModel> SelectLocation(SearchParam param);
        //SelectLocationForParticularUsers
        List<LocationModel> SelectLocationForParticularUsers(SearchParam param);
    }
}
