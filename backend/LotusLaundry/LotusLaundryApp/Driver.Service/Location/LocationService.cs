﻿
using LotusLaundry.Domain.Location;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.Constants; 
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Location;

using Driver.DBRepository.Location;

namespace Driver.Service.Location
{
    public class LocationService : ILocationService
    {	
        public LocationDBService _locationDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        private readonly string _imageOption;
        
        
        
        public LocationService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _locationDBService = new LocationDBService();
            _fileGroupService = fileGroupService;
            _imageOption = ConfigurationManager.AppSettings["IMAGEOPTION"];
            _xmlService = xmlService;
            
        }
        
        public long LocationInsert(LocationModel model)
        {
            var id = _locationDBService.LocationInsert(model);
            return id;
        }

        public void LocationUpdate(LocationModel model)
        {
            _locationDBService.LocationUpdate(model);
        }

        public List<LocationModel> SelectLocation(SearchParam param)
        {	
         	if (param.Id != null) 
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            
         
            var response = _locationDBService.SelectLocation(param);

            response.ForEach(x =>
            {
            });
            return response;
        }

        public List<LocationModel> SelectLocationForParticularUsers(SearchParam param)
        {
            if (param.Id != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Id, KeyConstant.Key));
            }
            if (param.Slug != null)
            {
                param.RecordId = Convert.ToInt64(CryptoEngine.Decrypt(param.Slug, KeyConstant.Key));
            }
            if (param.RelationId != null)
            {
                param.LRelationId = Convert.ToInt64(CryptoEngine.Decrypt(param.RelationId, KeyConstant.Key));
            }

            List<LocationModel> Locations = new List<LocationModel>();

            if (param.drivers!=null &&  param.drivers.Count > 0)
            {
                foreach (var item in param.drivers)
                {
                    LocationModel Lm = new LocationModel();
                    param.driverId = Convert.ToInt64(CryptoEngine.Decrypt(item, KeyConstant.Key));
                    param.RelationTable = "User";
                    param.LRelationId = param.driverId;
                    var DriverLoctaion = _locationDBService.SelectLocation(param);
                    if (DriverLoctaion.Count > 0)
                    {
                        Lm.Longitude = DriverLoctaion.FirstOrDefault().Longitude;
                        Lm.Latitude = DriverLoctaion.FirstOrDefault().Latitude;
                        Lm.UserId = DriverLoctaion.FirstOrDefault().UserId;
                        Lm.IsActive = DriverLoctaion.FirstOrDefault().IsActive;
                        Lm.Name = DriverLoctaion.FirstOrDefault().Name;
                        Locations.Add(Lm);
                    }

                }
            }
            else
            {
                 Locations = _locationDBService.SelectLocation(param);
            }
            var response = Locations;

            response.ForEach(x =>
            {
            });
            return response;
        }

    }
}
