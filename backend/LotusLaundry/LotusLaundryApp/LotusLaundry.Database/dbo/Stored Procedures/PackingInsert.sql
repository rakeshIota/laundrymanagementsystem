-- Procedure : PackingInsert

/***** Object:  StoredProcedure [dbo].[PackingInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ProductId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Packing]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[ProductId],[ServiceId],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@ProductId,@ServiceId,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
