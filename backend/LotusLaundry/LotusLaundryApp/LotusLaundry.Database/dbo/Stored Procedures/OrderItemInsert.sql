-- Procedure : OrderItemInsert

/***** Object:  StoredProcedure [dbo].[OrderItemInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderItemInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
		  	@IsPacking    BIT=NULL,
			@PackingPrice    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
		  	@PackingId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderItem]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[ProductId],[Quantity],[IsPacking],[PackingPrice],[SubTotal],[TotalPrice],[PackingId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@ProductId,@Quantity,@IsPacking,@PackingPrice,@SubTotal,@TotalPrice,@PackingId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
