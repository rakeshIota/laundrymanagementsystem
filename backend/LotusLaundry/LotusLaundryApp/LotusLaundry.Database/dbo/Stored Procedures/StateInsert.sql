-- Procedure : StateInsert

/***** Object:  StoredProcedure [dbo].[StateInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StateInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CountryId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [State]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CountryId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CountryId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
