-- Procedure : CurrencyLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[CurrencyLanguageMappingInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CurrencyId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CurrencyLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CurrencyId],[LanguageId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CurrencyId,@LanguageId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
