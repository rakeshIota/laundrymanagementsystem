-- Procedure : CartUpdate

/***** Object:  StoredProcedure  [dbo].[CartUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@IsLocked    BIT=NULL,
		  	@CustomerId    BIGINT=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Tax    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@TotalItems   INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Cart]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[IsLocked] = ISNULL(@IsLocked,[IsLocked]),
			 	[CustomerId] = ISNULL(@CustomerId,[CustomerId]),
			 	[Status] = ISNULL(@Status,[Status]),
			 	[Tax] = ISNULL(@Tax,[Tax]),
			 	[SubTotal] = ISNULL(@SubTotal,[SubTotal]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice]),
			 	[TotalItems] = ISNULL(@TotalItems,[TotalItems])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

