-- Procedure : PasswordLogInsert

/***** Object:  StoredProcedure [dbo].[PasswordLogInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PasswordLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@Count   INT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PasswordLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[Count]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@Count
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
