-- Procedure : OrderLogInsert

/***** Object:  StoredProcedure [dbo].[OrderLogInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@OrderId    BIGINT=NULL,
			@Note NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [OrderLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[OrderId],[Note]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@OrderId,@Note
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
