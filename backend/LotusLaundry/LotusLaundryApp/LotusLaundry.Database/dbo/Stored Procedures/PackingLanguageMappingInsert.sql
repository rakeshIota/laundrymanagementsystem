-- Procedure : PackingLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[PackingLanguageMappingInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackingLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@PackingId    BIGINT=NULL,
		  	@LanguageId    BIGINT=NULL,
			@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PackingLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[PackingId],[LanguageId],[Name]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@PackingId,@LanguageId,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
