-- Procedure : OrderInsert

/***** Object:  StoredProcedure [dbo].[OrderInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@DeliveryDate  DATETIMEOFFSET(7)=NULL,
		  	@DeliveryType    BIGINT=NULL,
			@TotalItems   INT=NULL,
			@DeliveryPrice    DECIMAL(18,2)=NULL,
		  	@DriverId    BIGINT=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@PaymentType NVARCHAR(MAX)=NULL,
			@PaidAmount    DECIMAL(18,2)=NULL,
			@Tax    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
		  	@AdressId    BIGINT=NULL,
		  	@ExpectedPickUpMin    BIGINT=NULL,
		  	@ExpectedPickUpMax    BIGINT=NULL,
		  	@ExpectedDeliveryMin    BIGINT=NULL,
		  	@ExpectedDeliveryMax    BIGINT=NULL,
		  	@CartId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Order]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[DeliveryDate],[DeliveryType],[TotalItems],[DeliveryPrice],[DriverId],[Status],[PaymentType],[PaidAmount],[Tax],[SubTotal],[TotalPrice],[AdressId],[ExpectedPickUpMin],[ExpectedPickUpMax],[ExpectedDeliveryMin],[ExpectedDeliveryMax],[CartId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@DeliveryDate,@DeliveryType,@TotalItems,@DeliveryPrice,@DriverId,@Status,@PaymentType,@PaidAmount,@Tax,@SubTotal,@TotalPrice,@AdressId,@ExpectedPickUpMin,@ExpectedPickUpMax,@ExpectedDeliveryMin,@ExpectedDeliveryMax,@CartId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
