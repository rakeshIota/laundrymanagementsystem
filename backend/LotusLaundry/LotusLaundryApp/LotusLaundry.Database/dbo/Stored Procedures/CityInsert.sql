-- Procedure : CityInsert

/***** Object:  StoredProcedure [dbo].[CityInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CityInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@StateId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [City]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[StateId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@StateId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
