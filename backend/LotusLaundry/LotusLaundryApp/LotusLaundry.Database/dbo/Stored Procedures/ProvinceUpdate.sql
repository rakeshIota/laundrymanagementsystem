-- Procedure : ProvinceUpdate

/***** Object:  StoredProcedure  [dbo].[ProvinceUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
			@Name NVARCHAR(MAX)=NULL,
		  	@LanguageId    BIGINT=NULL,
		  	@ProvinceId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Province]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[Name] = ISNULL(@Name,[Name]),
			 	[LanguageId] = ISNULL(@LanguageId,[LanguageId]),
			 	[ProvinceId] = ISNULL(@ProvinceId,[ProvinceId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

