-- Procedure : UserAddressUpdate

/***** Object:  StoredProcedure  [dbo].[UserAddressUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@AdressLine1 NVARCHAR(MAX)=NULL,
			@AdressLine2 NVARCHAR(MAX)=NULL,
		  	@DistrictId    BIGINT=NULL,
		  	@SubDistrictId    BIGINT=NULL,
			@ZipCode NVARCHAR(MAX)=NULL,
		  	@IsDefault    BIT=NULL,
			@Tag NVARCHAR(MAX)=NULL,
		  	@ProvinceId    BIGINT=NULL,
			@Latitude    DECIMAL(18,2)=NULL,
			@Longitude    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [UserAddress]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[AdressLine1] = ISNULL(@AdressLine1,[AdressLine1]),
			 	[AdressLine2] = ISNULL(@AdressLine2,[AdressLine2]),
			 	[DistrictId] = ISNULL(@DistrictId,[DistrictId]),
			 	[SubDistrictId] = ISNULL(@SubDistrictId,[SubDistrictId]),
			 	[ZipCode] = ISNULL(@ZipCode,[ZipCode]),
			 	[IsDefault] = ISNULL(@IsDefault,[IsDefault]),
			 	[Tag] = ISNULL(@Tag,[Tag]),
			 	[ProvinceId] = ISNULL(@ProvinceId,[ProvinceId]),
			 	[Latitude] = ISNULL(@Latitude,[Latitude]),
			 	[Longitude] = ISNULL(@Longitude,[Longitude])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

