-- Procedure : CartItemServiceMappingXMLSave

GO
/***** Object:  StoredProcedure [dbo].[CartItemServiceMappingXMLSave] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingXMLSave]
 @CartItemServiceMappingXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CartItemId)[1]', 'BIGINT') AS 'CartItemId',
		NDS.DT.value('(ServiceId)[1]', 'BIGINT') AS 'ServiceId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price',
		NDS.DT.value('(SubTotal)[1]', 'DECIMAL(18,2)') AS 'SubTotal',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice'
  FROM 
	@CartItemServiceMappingXml.nodes('/root/CartItemServiceMapping') AS NDS(DT)
   )
   MERGE INTO CartItemServiceMapping R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[CartItemId] =ISNULL(x.[CartItemId] ,R.[CartItemId]),R.[ServiceId] =ISNULL(x.[ServiceId] ,R.[ServiceId]),R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),R.[Price] =ISNULL(x.[Price] ,R.[Price]),R.[SubTotal] =ISNULL(x.[SubTotal] ,R.[SubTotal]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[CartItemId],
		[ServiceId],
		[Quantity],
		[Price],
		[SubTotal],
		[TotalPrice]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[CartItemId],x.[ServiceId],x.[Quantity],x.[Price],x.[SubTotal],x.[TotalPrice]
    );
END