-- Procedure : CartItemServiceMappingUpdate

/***** Object:  StoredProcedure  [dbo].[CartItemServiceMappingUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemServiceMappingUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartItemId    BIGINT=NULL,
		  	@ServiceId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [CartItemServiceMapping]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[CartItemId] = ISNULL(@CartItemId,[CartItemId]),
			 	[ServiceId] = ISNULL(@ServiceId,[ServiceId]),
			 	[Quantity] = ISNULL(@Quantity,[Quantity]),
			 	[Price] = ISNULL(@Price,[Price]),
			 	[SubTotal] = ISNULL(@SubTotal,[SubTotal]),
			 	[TotalPrice] = ISNULL(@TotalPrice,[TotalPrice])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

