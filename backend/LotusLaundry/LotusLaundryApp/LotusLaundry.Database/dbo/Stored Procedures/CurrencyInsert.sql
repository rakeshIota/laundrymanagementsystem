-- Procedure : CurrencyInsert

/***** Object:  StoredProcedure [dbo].[CurrencyInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CurrencyInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Currency]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
