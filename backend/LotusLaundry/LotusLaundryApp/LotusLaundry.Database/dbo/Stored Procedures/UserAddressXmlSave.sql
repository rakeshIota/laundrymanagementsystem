-- Procedure : UserAddressXMLSave

GO
/***** Object:  StoredProcedure [dbo].[UserAddressXMLSave] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressXMLSave]
 @UserAddressXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(UserId)[1]', 'BIGINT') AS 'UserId',
		NDS.DT.value('(AdressLine1)[1]', 'NVARCHAR') AS 'AdressLine1',
		NDS.DT.value('(AdressLine2)[1]', 'NVARCHAR') AS 'AdressLine2',
		NDS.DT.value('(DistrictId)[1]', 'BIGINT') AS 'DistrictId',
		NDS.DT.value('(SubDistrictId)[1]', 'BIGINT') AS 'SubDistrictId',
		NDS.DT.value('(ZipCode)[1]', 'NVARCHAR') AS 'ZipCode',
	  	NDS.DT.value('(IsDefault)[1]', 'BIT') AS 'IsDefault',
		NDS.DT.value('(Tag)[1]', 'NVARCHAR') AS 'Tag',
		NDS.DT.value('(ProvinceId)[1]', 'BIGINT') AS 'ProvinceId',
		NDS.DT.value('(Latitude)[1]', 'DECIMAL(18,2)') AS 'Latitude',
		NDS.DT.value('(Longitude)[1]', 'DECIMAL(18,2)') AS 'Longitude'
  FROM 
	@UserAddressXml.nodes('/root/UserAddress') AS NDS(DT)
   )
   MERGE INTO UserAddress R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[UserId] =ISNULL(x.[UserId] ,R.[UserId]),R.[AdressLine1] =ISNULL(x.[AdressLine1] ,R.[AdressLine1]),R.[AdressLine2] =ISNULL(x.[AdressLine2] ,R.[AdressLine2]),R.[DistrictId] =ISNULL(x.[DistrictId] ,R.[DistrictId]),R.[SubDistrictId] =ISNULL(x.[SubDistrictId] ,R.[SubDistrictId]),R.[ZipCode] =ISNULL(x.[ZipCode] ,R.[ZipCode]),R.[IsDefault] =ISNULL(x.[IsDefault] ,R.[IsDefault]),R.[Tag] =ISNULL(x.[Tag] ,R.[Tag]),R.[ProvinceId] =ISNULL(x.[ProvinceId] ,R.[ProvinceId]),R.[Latitude] =ISNULL(x.[Latitude] ,R.[Latitude]),R.[Longitude] =ISNULL(x.[Longitude] ,R.[Longitude])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[UserId],
		[AdressLine1],
		[AdressLine2],
		[DistrictId],
		[SubDistrictId],
		[ZipCode],
		[IsDefault],
		[Tag],
		[ProvinceId],
		[Latitude],
		[Longitude]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[UserId],x.[AdressLine1],x.[AdressLine2],x.[DistrictId],x.[SubDistrictId],x.[ZipCode],x.[IsDefault],x.[Tag],x.[ProvinceId],x.[Latitude],x.[Longitude]
    );
END