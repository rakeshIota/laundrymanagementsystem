-- Procedure : ProductInsert

/***** Object:  StoredProcedure [dbo].[ProductInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Gender NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Product]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Gender]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Gender
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
