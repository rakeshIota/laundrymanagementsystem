-- Procedure : SubDistrictInsert

/***** Object:  StoredProcedure [dbo].[SubDistrictInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DistrictId    BIGINT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [SubDistrict]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[DistrictId]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@DistrictId
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
