-- Procedure : CartInsert

/***** Object:  StoredProcedure [dbo].[CartInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@IsLocked    BIT=NULL,
		  	@CustomerId    BIGINT=NULL,
			@Status NVARCHAR(MAX)=NULL,
			@Tax    DECIMAL(18,2)=NULL,
			@SubTotal    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL,
			@TotalItems   INT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Cart]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[IsLocked],[CustomerId],[Status],[Tax],[SubTotal],[TotalPrice],[TotalItems]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@IsLocked,@CustomerId,@Status,@Tax,@SubTotal,@TotalPrice,@TotalItems
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
