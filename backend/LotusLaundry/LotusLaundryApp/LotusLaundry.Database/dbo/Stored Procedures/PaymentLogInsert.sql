-- Procedure : PaymentLogInsert

/***** Object:  StoredProcedure [dbo].[PaymentLogInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@CardNumber NVARCHAR(MAX)=NULL,
			@Amount    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL,
			@TransactionId NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [PaymentLog]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[CardNumber],[Amount],[OrderId],[TransactionId],[Status]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@CardNumber,@Amount,@OrderId,@TransactionId,@Status
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
