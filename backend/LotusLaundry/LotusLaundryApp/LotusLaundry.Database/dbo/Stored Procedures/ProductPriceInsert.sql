-- Procedure : ProductPriceInsert

/***** Object:  StoredProcedure [dbo].[ProductPriceInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPriceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@ServiceId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Price    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProductPrice]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[ServiceId],[ProductId],[Price]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@ServiceId,@ProductId,@Price
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
