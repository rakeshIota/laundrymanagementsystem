-- Procedure : CartItemInsert

/***** Object:  StoredProcedure [dbo].[CartItemInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@CartId    BIGINT=NULL,
		  	@ProductId    BIGINT=NULL,
			@Quantity   INT=NULL,
			@Price    DECIMAL(18,2)=NULL,
			@TotalPrice    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [CartItem]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[CartId],[ProductId],[Quantity],[Price],[TotalPrice]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@CartId,@ProductId,@Quantity,@Price,@TotalPrice
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
