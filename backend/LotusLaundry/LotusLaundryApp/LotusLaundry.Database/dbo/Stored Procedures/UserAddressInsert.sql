-- Procedure : UserAddressInsert

/***** Object:  StoredProcedure [dbo].[UserAddressInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserAddressInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@AdressLine1 NVARCHAR(MAX)=NULL,
			@AdressLine2 NVARCHAR(MAX)=NULL,
		  	@DistrictId    BIGINT=NULL,
		  	@SubDistrictId    BIGINT=NULL,
			@ZipCode NVARCHAR(MAX)=NULL,
		  	@IsDefault    BIT=NULL,
			@Tag NVARCHAR(MAX)=NULL,
		  	@ProvinceId    BIGINT=NULL,
			@Latitude    DECIMAL(18,2)=NULL,
			@Longitude    DECIMAL(18,2)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [UserAddress]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[AdressLine1],[AdressLine2],[DistrictId],[SubDistrictId],[ZipCode],[IsDefault],[Tag],[ProvinceId],[Latitude],[Longitude]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@AdressLine1,@AdressLine2,@DistrictId,@SubDistrictId,@ZipCode,@IsDefault,@Tag,@ProvinceId,@Latitude,@Longitude
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
