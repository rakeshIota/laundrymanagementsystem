-- Procedure : ProvinceLanguageMappingInsert

/***** Object:  StoredProcedure [dbo].[ProvinceLanguageMappingInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProvinceLanguageMappingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [ProvinceLanguageMapping]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
