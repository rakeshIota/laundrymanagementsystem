-- Procedure : CartItemXMLSave

GO
/***** Object:  StoredProcedure [dbo].[CartItemXMLSave] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CartItemXMLSave]
 @CartItemXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(TenantId)[1]', 'INT') AS 'TenantId',
		NDS.DT.value('(Slug)[1]', 'NVARCHAR') AS 'Slug',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(CartId)[1]', 'BIGINT') AS 'CartId',
		NDS.DT.value('(ProductId)[1]', 'BIGINT') AS 'ProductId',
		NDS.DT.value('(Quantity)[1]', 'INT') AS 'Quantity',
		NDS.DT.value('(Price)[1]', 'DECIMAL(18,2)') AS 'Price',
		NDS.DT.value('(TotalPrice)[1]', 'DECIMAL(18,2)') AS 'TotalPrice'
  FROM 
	@CartItemXml.nodes('/root/CartItem') AS NDS(DT)
   )
   MERGE INTO CartItem R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.[TenantId] =ISNULL(x.[TenantId] ,R.[TenantId]),R.[Slug] =ISNULL(x.[Slug] ,R.[Slug]),R.[CreatedBy] =ISNULL(x.[CreatedBy] ,R.[CreatedBy]),R.[UpdatedBy] =ISNULL(x.[UpdatedBy] ,R.[UpdatedBy]),R.[UpdatedOn] =ISNULL(x.[UpdatedOn] ,R.[UpdatedOn]),R.[IsDeleted] =ISNULL(x.[IsDeleted] ,R.[IsDeleted]),R.[IsActive] =ISNULL(x.[IsActive] ,R.[IsActive]),R.[CartId] =ISNULL(x.[CartId] ,R.[CartId]),R.[ProductId] =ISNULL(x.[ProductId] ,R.[ProductId]),R.[Quantity] =ISNULL(x.[Quantity] ,R.[Quantity]),R.[Price] =ISNULL(x.[Price] ,R.[Price]),R.[TotalPrice] =ISNULL(x.[TotalPrice] ,R.[TotalPrice])
  WHEN NOT MATCHED 
  THEN 
    INSERT
	(
		
		[TenantId],
		[Slug],
		[CreatedBy],
		[UpdatedBy],
		
		
		[IsActive],
		[CartId],
		[ProductId],
		[Quantity],
		[Price],
		[TotalPrice]
	)
    VALUES(
    x.[TenantId],x.[Slug],x.[CreatedBy],x.[UpdatedBy],x.[IsActive],x.[CartId],x.[ProductId],x.[Quantity],x.[Price],x.[TotalPrice]
    );
END