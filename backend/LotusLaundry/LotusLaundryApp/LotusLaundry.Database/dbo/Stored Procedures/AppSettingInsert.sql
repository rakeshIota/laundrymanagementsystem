-- Procedure : AppSettingInsert

/***** Object:  StoredProcedure [dbo].[AppSettingInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AppSettingInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
			@Key NVARCHAR(MAX)=NULL,
			@Value NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [AppSetting]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[Key],[Value]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@Key,@Value
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
