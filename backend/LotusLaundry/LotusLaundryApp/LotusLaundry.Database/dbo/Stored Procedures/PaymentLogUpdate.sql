-- Procedure : PaymentLogUpdate

/***** Object:  StoredProcedure  [dbo].[PaymentLogUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaymentLogUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@CardNumber NVARCHAR(MAX)=NULL,
			@Amount    DECIMAL(18,2)=NULL,
		  	@OrderId    BIGINT=NULL,
			@TransactionId NVARCHAR(MAX)=NULL,
			@Status NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [PaymentLog]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[UserId] = ISNULL(@UserId,[UserId]),
			 	[CardNumber] = ISNULL(@CardNumber,[CardNumber]),
			 	[Amount] = ISNULL(@Amount,[Amount]),
			 	[OrderId] = ISNULL(@OrderId,[OrderId]),
			 	[TransactionId] = ISNULL(@TransactionId,[TransactionId]),
			 	[Status] = ISNULL(@Status,[Status])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

