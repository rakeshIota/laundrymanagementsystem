-- Procedure : UserDeviceInsert

/***** Object:  StoredProcedure [dbo].[UserDeviceInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserDeviceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL,
		  	@UserId    BIGINT=NULL,
			@DeviceId NVARCHAR(MAX)=NULL,
			@NotificationToken NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [UserDevice]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive],[UserId],[DeviceId],[NotificationToken]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive,@UserId,@DeviceId,@NotificationToken
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
