-- Procedure : SubDistrictUpdate

/***** Object:  StoredProcedure  [dbo].[SubDistrictUpdate] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SubDistrictUpdate]
		  	@Id    BIGINT=NULL,
			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@UpdatedBy    BIGINT=NULL,
		  	@IsDeleted    BIT=NULL,
		  	@IsActive    BIT=NULL,
		  	@DistrictId    BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [SubDistrict]
	SET
			 	[Slug] = ISNULL(@Slug,[Slug]),
			 	[UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
		 		[UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
			 	[IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
			 	[IsActive] = ISNULL(@IsActive,[IsActive]),
			 	[DistrictId] = ISNULL(@DistrictId,[DistrictId])
	 WHERE
	 (
	  [Id]=@Id
	 )
	 AND
	 (
		[TenantId] =  @TenantId
	 )
END

