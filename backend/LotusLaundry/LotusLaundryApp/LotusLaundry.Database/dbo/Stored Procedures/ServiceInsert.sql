-- Procedure : ServiceInsert

/***** Object:  StoredProcedure [dbo].[ServiceInsert] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ServiceInsert](

			@TenantId   INT=NULL,
			@Slug NVARCHAR(MAX)=NULL,
		  	@CreatedBy    BIGINT=NULL,
		  	@IsActive    BIT=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Service]
	  (
	   [TenantId],[Slug],[CreatedBy],[UpdatedBy],[IsActive]
	  )
	  VALUES
	  ( 
	   @TenantId,@Slug,@CreatedBy,@CreatedBy,@IsActive
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 
