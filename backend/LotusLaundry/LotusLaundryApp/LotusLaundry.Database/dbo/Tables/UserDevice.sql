-- Table Script : UserDevice

/***** Object:  StoredProcedure [dbo].[UserDevice] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDevice] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_UserDevice_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_UserDevice_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_UserDevice_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_UserDevice_IsActive] DEFAULT ((1)) NOT NULL,
	  	[UserId]    BIGINT             NULL,
		[DeviceId] NVARCHAR (100) NULL,
		[NotificationToken] NVARCHAR (100) NULL,
CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED ([Id]))

