-- Table Script : Feedback

/***** Object:  StoredProcedure [dbo].[Feedback] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Feedback_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Feedback_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Feedback_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Feedback_IsActive] DEFAULT ((1)) NOT NULL,
	  	[OrderId]    BIGINT             NULL,
		[Type] NVARCHAR (100) NULL,
		[Message] NVARCHAR (max) NULL,
	  	[Rating]    BIGINT             NULL,
	  	[DriverId]    BIGINT             NULL,
	  	[UserId]    BIGINT             NULL,
CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED ([Id]))

