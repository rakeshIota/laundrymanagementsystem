CREATE TABLE [dbo].[Tenant](
[Id] [int] IDENTITY(1,1) NOT NULL,
[Name] [nvarchar](250) NULL,
[Address] [nvarchar](250) NULL,
[PhoneNo] [nvarchar](20) NULL,
[Email] [nvarchar](50) NULL,
[IsDeleted] [bit] NOT NULL,
[IsActive] [bit] NOT NULL,
[CreatedOn] [datetimeoffset](7) NOT NULL,
[UpdatedOn] [datetimeoffset](7) NOT NULL,
[CreatedBy] [bigint] NULL,
[UpdatedBy] [bigint] NULL,
[UniqueId] [uniqueidentifier] NOT NULL,
CONSTRAINT [PK_Tanant] PRIMARY KEY CLUSTERED 
(
[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [DF_Tanant_IsDeleted] DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [DF_Tanant_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [DF_Tanant_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [DF_Tanant_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) FOR [UpdatedOn]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [DF_Tenant_UniqueId] DEFAULT (newid()) FOR [UniqueId]
GO
