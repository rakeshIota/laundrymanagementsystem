-- Table Script : OrderItemServiceMapping

/***** Object:  StoredProcedure [dbo].[OrderItemServiceMapping] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItemServiceMapping] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_OrderItemServiceMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_OrderItemServiceMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_OrderItemServiceMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_OrderItemServiceMapping_IsActive] DEFAULT ((1)) NOT NULL,
	  	[OrderItemId]    BIGINT             NULL,
	  	[ServiceId]    BIGINT             NOT NULL,
		[Quantity]    INT        NULL,
		[Price]    DECIMAL(18,2)             NOT NULL,
		[SubTotal]    DECIMAL(18,2)             NOT NULL,
		[TotalPrice]    DECIMAL(18,2)             NOT NULL,
CONSTRAINT [PK_OrderItemServiceMapping] PRIMARY KEY CLUSTERED ([Id]))

