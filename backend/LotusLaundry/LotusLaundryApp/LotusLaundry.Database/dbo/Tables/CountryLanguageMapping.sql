-- Table Script : CountryLanguageMapping

/***** Object:  StoredProcedure [dbo].[CountryLanguageMapping] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryLanguageMapping] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CountryLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CountryLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_CountryLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_CountryLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
		[Name] NVARCHAR (100) NULL,
	  	[LanguageId]    BIGINT             NOT NULL,
	  	[CountryId]    BIGINT             NOT NULL,
CONSTRAINT [PK_CountryLanguageMapping] PRIMARY KEY CLUSTERED ([Id]))

