-- Table Script : PasswordLog

/***** Object:  StoredProcedure [dbo].[PasswordLog] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasswordLog] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PasswordLog_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PasswordLog_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_PasswordLog_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_PasswordLog_IsActive] DEFAULT ((1)) NOT NULL,
	  	[UserId]    BIGINT             NULL,
		[Count]    INT        NULL,
CONSTRAINT [PK_PasswordLog] PRIMARY KEY CLUSTERED ([Id]))

