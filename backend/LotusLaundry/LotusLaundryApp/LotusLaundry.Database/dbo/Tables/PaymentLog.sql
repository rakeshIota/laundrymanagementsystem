-- Table Script : PaymentLog

/***** Object:  StoredProcedure [dbo].[PaymentLog] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentLog] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PaymentLog_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_PaymentLog_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_PaymentLog_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_PaymentLog_IsActive] DEFAULT ((1)) NOT NULL,
	  	[UserId]    BIGINT             NOT NULL,
		[CardNumber] NVARCHAR (20) NULL,
		[Amount]    DECIMAL(18,2)             NOT NULL,
	  	[OrderId]    BIGINT             NOT NULL,
		[TransactionId] NVARCHAR (100) NOT NULL,
		[Status] NVARCHAR (100) NOT NULL,
CONSTRAINT [PK_PaymentLog] PRIMARY KEY CLUSTERED ([Id]))

