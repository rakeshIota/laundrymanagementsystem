-- Table Script : OrderItem

/***** Object:  StoredProcedure [dbo].[OrderItem] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_OrderItem_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_OrderItem_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_OrderItem_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_OrderItem_IsActive] DEFAULT ((1)) NOT NULL,
	  	[OrderId]    BIGINT             NOT NULL,
	  	[ProductId]    BIGINT             NOT NULL,
		[Quantity]    INT        NULL,
	  	[IsPacking]    BIT               CONSTRAINT [DF_OrderItem_IsPacking] DEFAULT ((0)) NOT NULL,
		[PackingPrice]    DECIMAL(18,2)             NOT NULL,
		[SubTotal]    DECIMAL(18,2)             NOT NULL,
		[TotalPrice]    DECIMAL(18,2)             NOT NULL,
	  	[PackingId]    BIGINT             NOT NULL,
CONSTRAINT [PK_OrderItem] PRIMARY KEY CLUSTERED ([Id]))

