-- Table Script : Order

/***** Object:  StoredProcedure [dbo].[Order] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Order_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Order_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Order_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Order_IsActive] DEFAULT ((1)) NOT NULL,
	  	[UserId]    BIGINT             NOT NULL,
		[DeliveryDate]  DATETIMEOFFSET (7) CONSTRAINT [DF_Order_DeliveryDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NOT NULL,
	  	[DeliveryType]    BIGINT             NOT NULL,
		[TotalItems]    INT        NOT NULL,
		[DeliveryPrice]    DECIMAL(18,2)             NULL,
	  	[DriverId]    BIGINT             NULL,
		[Status] NVARCHAR (30) NOT NULL,
		[PaymentType] NVARCHAR (30) NULL,
		[PaidAmount]    DECIMAL(18,2)             NULL,
		[Tax]    DECIMAL(18,2)             NOT NULL,
		[SubTotal]    DECIMAL(18,2)             NOT NULL,
		[TotalPrice]    DECIMAL(18,2)             NOT NULL,
	  	[AdressId]    BIGINT             NOT NULL,
	  	[ExpectedPickUpMin]    BIGINT             NULL,
	  	[ExpectedPickUpMax]    BIGINT             NULL,
	  	[ExpectedDeliveryMin]    BIGINT             NULL,
	  	[ExpectedDeliveryMax]    BIGINT             NULL,
	  	[CartId]    BIGINT             NULL,
CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([Id]))

