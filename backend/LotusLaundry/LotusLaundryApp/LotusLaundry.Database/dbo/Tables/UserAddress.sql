-- Table Script : UserAddress

/***** Object:  StoredProcedure [dbo].[UserAddress] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAddress] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_UserAddress_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_UserAddress_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_UserAddress_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_UserAddress_IsActive] DEFAULT ((1)) NOT NULL,
	  	[UserId]    BIGINT             NULL,
		[AdressLine1] NVARCHAR (max) NULL,
		[AdressLine2] NVARCHAR (max) NULL,
	  	[DistrictId]    BIGINT             NULL,
	  	[SubDistrictId]    BIGINT             NULL,
		[ZipCode] NVARCHAR (40) NULL,
	  	[IsDefault]    BIT               CONSTRAINT [DF_UserAddress_IsDefault] DEFAULT ((0)) NOT NULL,
		[Tag] NVARCHAR (40) NOT NULL,
	  	[ProvinceId]    BIGINT             NULL,
		[Latitude]    DECIMAL(18,2)             NULL,
		[Longitude]    DECIMAL(18,2)             NULL,
CONSTRAINT [PK_UserAddress] PRIMARY KEY CLUSTERED ([Id]))

