-- Table Script : CityLanguageMapping

/***** Object:  StoredProcedure [dbo].[CityLanguageMapping] *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CityLanguageMapping] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
		[TenantId]    INT        NOT NULL,
		[Slug] NVARCHAR (255) NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CityLanguageMapping_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_CityLanguageMapping_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_CityLanguageMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_CityLanguageMapping_IsActive] DEFAULT ((1)) NOT NULL,
		[Name] NVARCHAR (100) NULL,
	  	[LanguageId]    BIGINT             NOT NULL,
	  	[CityId]    BIGINT             NULL,
CONSTRAINT [PK_CityLanguageMapping] PRIMARY KEY CLUSTERED ([Id]))

