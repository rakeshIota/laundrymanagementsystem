﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Driver.Location;
using LotusLaundry.Domain.Location;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Location
{
    public static class LocationModelViewModelConverter
    {
        public static LocationViewModel ToViewModel(this LocationModel x)
        {
            if (x == null) return new LocationViewModel();
            return new LocationViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.LOCATION + "_" + x.Id.ToString(), KeyConstant.Key),
               // TenantId = x.TenantId,
               // Slug = CryptoEngine.Encrypt(FileType.LOCATION + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
               // UpdatedBy = x.UpdatedBy,
                //CreatedOn = x.CreatedOn,
               // UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
               // IsActive = x.IsActive,
                Longitude = x.Longitude,
                name=x.Name,
                Latitude = x.Latitude,
                Userid = x.UserId != null ? CryptoEngine.Encrypt(FileType.USER + "_" + x.UserId.ToString(), KeyConstant.Key) : null,
               // TotalCount =x.TotalCount
            };
        }
		
		 public static LocationModel ToModel(this LocationViewModel x)
        {
            if (x == null) return new LocationModel();
            return new LocationModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Slug != null ? CryptoEngine.Decrypt(x.Slug.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
                Longitude = x.Longitude,
                Latitude = x.Latitude,
                UserId = x.Userid != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Userid.ToString(), KeyConstant.Key)) : 0,
            };
        }
    }
}
