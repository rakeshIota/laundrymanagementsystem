﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.Driver.Location
{
    public class LocationViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
       
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }



       
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }




        [JsonProperty("latitude")]
        public decimal? Latitude { get; set; }
        [JsonProperty("longitude")]
        public decimal? Longitude { get; set; }

        [JsonProperty("userId")]
        public String Userid { get; set; }


        [JsonProperty("name")]
        public string name { get; set; }


      
    }
}

