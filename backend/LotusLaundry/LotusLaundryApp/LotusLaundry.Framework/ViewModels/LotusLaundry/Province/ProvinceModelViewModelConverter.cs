﻿using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProvinceLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Province
{
    public static class ProvinceModelViewModelConverter
    {
        public static ProvinceViewModel ToViewModel(this ProvinceModel x)
        {
            if (x == null) return new ProvinceViewModel();
            return new ProvinceViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PROVINCE + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PROVINCE + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                TotalCount = x.TotalCount,
                ProvinceLanguages = x.ProvinceLanguages != null ? x.ProvinceLanguages.Select(y => y.ToViewModel()).ToList() : null
            };
        }

        public static ProvinceModel ToModel(this ProvinceViewModel x)
        {
            if (x == null) return new ProvinceModel();
            return new ProvinceModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                ProvinceLanguages = x.ProvinceLanguages != null ? x.ProvinceLanguages.Select(y => y.ToModel()).ToList() : null
            };
        }
    }
}
