﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.PasswordLog
{
    public class PasswordLogViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [Range(0, 9999999999999999, ErrorMessage = "User value is more than max length of 100")]
        [JsonProperty("userId")]
        public long? UserId { get; set; }
        [Range(0, 9999999999999999, ErrorMessage = "Count value is more than max length of 100")]
        [RegularExpression(RegularExpressionType.OnlyNumber, ErrorMessage = "Count must be numeric")]
        [JsonProperty("count")]
        public int? Count { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
    }
}

