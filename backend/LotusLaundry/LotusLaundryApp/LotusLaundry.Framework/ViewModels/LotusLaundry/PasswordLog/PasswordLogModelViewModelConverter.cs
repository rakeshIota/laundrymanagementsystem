﻿using LotusLaundry.Domain.PasswordLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.PasswordLog
{
    public static class PasswordLogModelViewModelConverter
    {
        public static PasswordLogViewModel ToViewModel(this PasswordLogModel x)
        {
            if (x == null) return new PasswordLogViewModel();
            return new PasswordLogViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.PASSWORDLOG + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.PASSWORDLOG + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				UserId = x.UserId,
        				Count = x.Count,
				TotalCount=x.TotalCount
            };
        }
		
		 public static PasswordLogModel ToModel(this PasswordLogViewModel x)
        {
            if (x == null) return new PasswordLogModel();
            return new PasswordLogModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			UserId = x.UserId,
        			Count = x.Count,
            };
        }
    }
}
