﻿using LotusLaundry.Domain.PaymentLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.PaymentLog
{
    public static class PaymentLogModelViewModelConverter
    {
        public static PaymentLogViewModel ToViewModel(this PaymentLogModel x)
        {
            if (x == null) return new PaymentLogViewModel();
            return new PaymentLogViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PAYMENTLOG + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PAYMENTLOG + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                UserId = x.UserId,
                CardNumber = x.CardNumber,
                Amount = x.Amount,
                OrderId = x.OrderId,
                TransactionId = x.TransactionId,
                Status = x.Status,
                TotalCount = x.TotalCount
            };
        }

        public static PaymentLogModel ToModel(this PaymentLogViewModel x)
        {
            if (x == null) return new PaymentLogModel();
            return new PaymentLogModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                UserId = x.UserId,
                CardNumber = x.CardNumber,
                Amount = x.Amount,
                OrderId = x.OrderId,
                TransactionId = x.TransactionId,
                Status = x.Status,
            };
        }
    }
}
