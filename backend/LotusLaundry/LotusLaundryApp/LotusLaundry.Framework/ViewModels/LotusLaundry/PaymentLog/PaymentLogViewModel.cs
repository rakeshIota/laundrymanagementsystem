﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.PaymentLog
{
   public class PaymentLogViewModel
    {
               [JsonProperty("id")]
				public string Id { get; set; }
				[JsonProperty("tenantId")]
				public int? TenantId { get; set; }
				[JsonProperty("slug")]
        		public string Slug { get; set; }
			  	[JsonProperty("createdBy")]
			  	public long? CreatedBy { get; set; }
			  	[JsonProperty("updatedBy")]
			  	public long? UpdatedBy { get; set; }
				[JsonProperty("createdOn")]
			  	public DateTimeOffset?  CreatedOn { get; set; }			
				[JsonProperty("updatedOn")]
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			[Required(ErrorMessage = "Deleted? is requried")]
				[JsonProperty("isDeleted")]
			  	public bool? IsDeleted { get; set; }
			[Required(ErrorMessage = "Active? is requried")]
				[JsonProperty("isActive")]
			  	public bool? IsActive { get; set; }
			[Required(ErrorMessage = "UserId is requried")]
			[Range(0, 9999999999999999, ErrorMessage = "UserId value is more than max length of 100")]
			  	[JsonProperty("userId")]
			  	public long? UserId { get; set; }
			[MaxLength(20, ErrorMessage = "CardNumber Max length 20")]
				[JsonProperty("cardNumber")]
        		public string CardNumber { get; set; }
			[Required(ErrorMessage = "Amount is requried")]
			[Range(0.1, 100, ErrorMessage = "Amount value is more than max limit of 100")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Amount")]
				[JsonProperty("amount")]
			  	public decimal? Amount { get; set; }
			[Required(ErrorMessage = "OrderId is requried")]
			[Range(0, 9999999999999999, ErrorMessage = "OrderId value is more than max length of 100")]
			  	[JsonProperty("orderId")]
			  	public long? OrderId { get; set; }
			[Required(ErrorMessage = "Transaction is requried")]
			[MaxLength(100, ErrorMessage = "Transaction Max length 100")]
				[JsonProperty("transactionId")]
        		public string TransactionId { get; set; }
			[Required(ErrorMessage = "Status is requried")]
			[MaxLength(100, ErrorMessage = "Status Max length 100")]
				[JsonProperty("status")]
        		public string Status { get; set; }
			[JsonProperty("totalCount")]
		  	public int TotalCount  { get; set; }
    }
}

