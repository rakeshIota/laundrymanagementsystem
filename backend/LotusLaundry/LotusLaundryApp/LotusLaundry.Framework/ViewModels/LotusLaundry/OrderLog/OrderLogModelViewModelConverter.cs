﻿using LotusLaundry.Domain.OrderLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.OrderLog
{
    public static class OrderLogModelViewModelConverter
    {
        public static OrderLogViewModel ToViewModel(this OrderLogModel x)
        {
            if (x == null) return new OrderLogViewModel();
            return new OrderLogViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.ORDERLOG + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.ORDERLOG + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				OrderId = x.OrderId,
        				Note = x.Note,
				TotalCount=x.TotalCount
            };
        }
		
		 public static OrderLogModel ToModel(this OrderLogViewModel x)
        {
            if (x == null) return new OrderLogModel();
            return new OrderLogModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			OrderId = x.OrderId,
        			Note = x.Note,
            };
        }
    }
}
