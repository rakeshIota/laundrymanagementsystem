﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain.Order;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Order
{
    public class OrderStatsViewModel
    {
        [JsonProperty("totalOrder")]
        public TotalOrder TotalOrder { get; set; }
        [JsonProperty("newOrder")]
        public NewOrder NewOrder { get; set; }
        [JsonProperty("collectionOrder")]
        public CollectionOrder CollectionOrder { get; set; }
        [JsonProperty("cleaningOrder")]
        public CleaningOrder CleaningOrder { get; set; }
        [JsonProperty("deliveredOrder")]
        public DeliveredOrder DeliveredOrder { get; set; }

    }
    public class TotalOrder
    {
        [JsonProperty("totalOrderTitle")]
        public string TotalOrderTitle { get; set; }
        [JsonProperty("totalOrderCount")]
        public long? TotalOrderCount { get; set; }
        [JsonProperty("totalNormalOrderCount")]
        public long? TotalNormalOrderCount { get; set; }
        [JsonProperty("totalSameDayOrderCount")]
        public long? TotalSameDayOrderCount { get; set; }
        [JsonProperty("totalExpressOrderCount")]
        public long? TotalExpressOrderCount { get; set; }
    }
    public class NewOrder
    {
        [JsonProperty("newOrderTitle")]
        public string NewOrderTitle { get; set; }
        [JsonProperty("newOrderTotalCount")]
        public long? NewOrderTotalCount { get; set; }
        [JsonProperty("newSameDayOrderCount")]
        public long? NewSameDayOrderCount { get; set; }
        [JsonProperty("newExpressOrderCount")]
        public long? NewExpressOrderCount { get; set; }
        [JsonProperty("newNormalOrderCount")]
        public long? NewNormalOrderCount { get; set; }
    }

    public class CollectionOrder
    {
        [JsonProperty("collectionOrderTitle")]
        public string CollectionOrderTitle { get; set; }
        [JsonProperty("collectionOrderTotalCount")]
        public long? CollectionOrderTotalCount { get; set; }
        [JsonProperty("collectionAssignedOrderCount")]
        public long? CollectionAssignedOrderCount { get; set; }
        [JsonProperty("collectionCollectedOrderCount")]
        public long? CollectionCollectedOrderCount { get; set; }
    }
    public class CleaningOrder
    {
        [JsonProperty("cleaningOrderTitle")]
        public string CleaningOrderTitle { get; set; }
        [JsonProperty("cleaningOrderTotalCount")]
        public long? CleaningOrderTotalCount { get; set; }
        [JsonProperty("cleaningInprogressOrderCount")]
        public long? CleaningInprogressOrderCount { get; set; }
        [JsonProperty("cleaningCompletedOrderCount")]
        public long? CleaningCompletedOrderCount { get; set; }
    }
    public class DeliveredOrder
    {
        [JsonProperty("deliveryOrderTitle")]
        public string DeliveryOrderTitle { get; set; }
        [JsonProperty("deliveryOrderTotalCount")]
        public long? DeliveryOrderTotalCount { get; set; }
    }
    public static class OrderStatsModelViewModelConverter
    {
        public static OrderStatsViewModel ToViewModel(this OrderStatsModel x)
        {
            if (x == null) return new OrderStatsViewModel();
            return new OrderStatsViewModel
            {
                TotalOrder = new TotalOrder
                {
                    TotalOrderTitle = x.TotalOrderTitle,
                    TotalOrderCount = x.TotalOrderCount,
                    TotalNormalOrderCount = x.TotalNormalOrderCount,
                    TotalSameDayOrderCount = x.TotalSameDayOrderCount,
                    TotalExpressOrderCount = x.TotalExpressOrderCount
                },
                NewOrder = new NewOrder
                {
                    NewOrderTitle = x.NewOrderTitle,
                    NewOrderTotalCount = x.NewOrderTotalCount,
                    NewNormalOrderCount = x.NewNormalOrderCount,
                    NewSameDayOrderCount = x.NewSameDayOrderCount,
                    NewExpressOrderCount = x.NewExpressOrderCount
                },
                CollectionOrder = new CollectionOrder
                {
                    CollectionOrderTitle = x.CollectionOrderTitle,
                    CollectionOrderTotalCount = x.CollectionOrderTotalCount,
                    CollectionAssignedOrderCount = x.CollectionAssignedOrderCount,
                    CollectionCollectedOrderCount = x.CollectionCollectedOrderCount
                },
                CleaningOrder = new CleaningOrder
                {
                    CleaningOrderTitle = x.CleaningOrderTitle,
                    CleaningOrderTotalCount = x.CleaningOrderTotalCount,
                    CleaningInprogressOrderCount = x.CleaningInprogressOrderCount,
                    CleaningCompletedOrderCount = x.CleaningCompletedOrderCount
                },
                DeliveredOrder = new DeliveredOrder
                {
                    DeliveryOrderTitle = x.DeliveryOrderTitle,
                    DeliveryOrderTotalCount = x.DeliveryOrderTotalCount
                }
            };
        }
    }
}
