﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
//using LotusLaundry.Domain.UserAddress;



namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Order
{
    public class OrderbasicViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
       
       
     
       


        [JsonProperty("deliveryDate")]
        public DateTimeOffset? DeliveryDate { get; set; }


        [JsonProperty("driverId")]
        public string DriverId { get; set; }

        
        [MaxLength(30, ErrorMessage = "Status Max length 30")]
        [JsonProperty("status")]
        public string Status { get; set; }


        [JsonProperty("pickupDate")]
        public DateTimeOffset? PickupDate { get; set; }

        [JsonProperty("deliverySlot")]
        public string deliverySlot { get; set; }

        [JsonProperty("pickupSlot")]
        public string pickupSlot { get; set; }

        [JsonProperty("adminNote")]
        public string adminNote { get; set; }

        [JsonProperty("paymentStatus")]
        public string paymentStatus { get; set; }


        [JsonProperty("collectedDate")]
        public DateTimeOffset? collectedDate { get; set; }


        [JsonProperty("deliveredDate")]
        public DateTimeOffset? deliveredDate { get; set; }


        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
    }
}

