﻿using LotusLaundry.Domain.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
//using LotusLaundry.Framework.ViewModels.Customer.Users;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Framework.WebExtensions;
using System.Globalization;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Order
{
    public static class OrderModelViewModelConverter
    {
        public static OrderViewModel ToViewModel(this OrderModel x)
        {
            if(x.DriverDetails!=null)
            {
                if(x.DriverDetails.Count()==0)
                {
                    x.DriverDetails = null;
                }
            }
            if (x == null) return new OrderViewModel();
            return new OrderViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                UserId = x.UserId,
                DeliveryDate = x.DeliveryDate,
                DeliveryType = x.DeliveryType,
                TotalItems = x.TotalItems,
                DeliveryPrice = x.DeliveryPrice,
                //DriverId = CryptoEngine.Encrypt(FileType.USER + "_" + x.DriverId.ToString(), KeyConstant.Key),
                DriverId = x.DriverId != null ? CryptoEngine.Encrypt(FileType.USER + "_" + x.DriverId.ToString(), KeyConstant.Key) : null,

                Status = x.Status,
                PaymentType = x.PaymentType,
                PaidAmount = x.PaidAmount,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                TotalPrice = x.TotalPrice,
                AdressId = x.AdressId,
                ExpectedPickUpMin = x.ExpectedPickUpMin,
                ExpectedPickUpMax = x.ExpectedPickUpMax,
                ExpectedDeliveryMin = x.ExpectedDeliveryMin,
                ExpectedDeliveryMax = x.ExpectedDeliveryMax,
                CartId = x.CartId,
                DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.FirstOrDefault().ToViewModel() : null,
                OrderItems = x.OrderItems != null ? x.OrderItems.Select(y => y.ToViewModel()).ToList() : null,
                LogisticCharge = x.LogisticCharge,
                Discount = x.Discount,
                paymentResponse = x.paymentResponse,
                paymentStatus = x.paymentStatus,
                driverDetails = x.DriverDetails != null ? x.DriverDetails.FirstOrDefault().ToViewModel() : null,
                customerDetails = x.customerdetails != null ? x.customerdetails.ToViewModel() : null,
                customerName = x.customerName,
                change = x.change != null ? x.change : 0,
                customerId = x.customerId != null ? CryptoEngine.Encrypt(FileType.USER + "_" + x.customerId.ToString(), KeyConstant.Key) : null,


                PickupDate = x.PickupDate,
                deliverySlot = x.deliverySlot,
                pickupSlot = x.pickupSlot,

                OrderId = x.Id.ToString().PadLeft(6, '0'),

                deliveredDate = x.deliveredDate,
                paymentDate = x.paymentDate,
                collectedDate = x.collectedDate,
                adminNote = x.adminNote,
                pickupDriverDetails = x.PickupDriverDetails != null ? x.PickupDriverDetails.FirstOrDefault().ToViewModel() : null,

                deliveryDriverDetails = x.DeliveryDriverDetails != null ? x.DeliveryDriverDetails.FirstOrDefault().ToViewModel() : null,
                rideType = x.RideType,
            
                qrCode=x.qrCode,
                laundryInstruction=x.laundryInstruction,
              // laundryInstruction=x.laundaryInstruction
                TotalCount =x.TotalCount
            };
        }
		
		 public static OrderModel ToModel(this OrderViewModel x)
        {
            if (x == null) return new OrderModel();
            return new OrderModel
            { 
                    Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			UserId = x.UserId,
        			DeliveryDate = x.DeliveryDate,
        			DeliveryType = x.DeliveryType,
        			TotalItems = x.TotalItems,
        			DeliveryPrice = x.DeliveryPrice,
        			DriverId = x.DriverId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.DriverId.ToString(), KeyConstant.Key)) : 0,
        			Status = x.Status,
        			PaymentType = x.PaymentType,
        			PaidAmount = x.PaidAmount,
        			Tax = x.Tax,
        			SubTotal = x.SubTotal,
        			TotalPrice = x.TotalPrice,
        			AdressId = x.AdressId,
        			ExpectedPickUpMin = x.ExpectedPickUpMin,
        			ExpectedPickUpMax = x.ExpectedPickUpMax,
        			ExpectedDeliveryMin = x.ExpectedDeliveryMin,
        			ExpectedDeliveryMax = x.ExpectedDeliveryMax,
        			CartId = x.CartId,
                  //  DeliveryAddress=x.DeliveryAddress,
                    PickupDate=x.PickupDate,
                    deliverySlot=x.deliverySlot,
                    pickupSlot=x.pickupSlot,
                   LogisticCharge=x.LogisticCharge,
                   Discount=x.Discount,
                   paymentResponse=x.paymentResponse,
                   paymentStatus=x.paymentStatus,
                change = x.change != null ? x.change : 0,
                note =x.note,
                   OrderItems=x.OrderItems!=null ? x.OrderItems.Select(a=>a.ToModel()).ToList() : null,
                   qrCode=x.qrCode,
                   laundryInstruction=x.laundryInstruction
                    




            };
        }


        public static OrderModel ToBasicModel(this OrderbasicViewModel x)
        {
            if (x == null) return new OrderModel();
            return new OrderModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = false,
                IsActive = true,    
                PickupDate=x.PickupDate,
                pickupSlot=x.pickupSlot,
                DeliveryDate=x.DeliveryDate,
                deliverySlot=x.deliverySlot,
                DriverId = x.DriverId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.DriverId.ToString(), KeyConstant.Key)) : 0,
                Status = x.Status,
                adminNote=x.adminNote,
                collectedDate=x.collectedDate,
                deliveredDate=x.deliveredDate
                
            };
        }


         public static OrderViewModel ToListViewModel(this OrderModel x)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
            if (x == null) return new OrderViewModel();
            return new OrderViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
        		       TenantId = x.TenantId,
                     	Slug = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),        				
        				CreatedOn = x.CreatedOn,        			
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				UserId = x.UserId,
        				DeliveryDate = x.DeliveryDate,
                         PickupDate = x.PickupDate,
                         DeliveryType = x.DeliveryType,
        			
                        DriverId = x.DriverId != null ? CryptoEngine.Encrypt(FileType.USERS + "_" + x.DriverId.ToString(), KeyConstant.Key) : null,
                                                    

                        Status = x.Status,
        				PaymentType = x.PaymentType,
        				TotalPrice = x.TotalPrice,
                        paymentStatus = x.paymentStatus,
                        customerName= txtInfo.ToTitleCase(x.customerName!=null?x.customerName:"") ,
                        driverName=(x.driverName),
                        districtName=x.districtName,
                        subDistrictName=x.subDistrictName,   
                        OrderId = x.Id.ToString().PadLeft(6, '0'),
                change = x.change != null ? x.change : 0,
                deliveredDate =x.deliveredDate,
               pickupSlot=x.pickupSlot,
               deliverySlot=x.deliverySlot,
                

                TotalCount =x.TotalCount
            };
        }
    }
}
