﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
using LotusLaundry.Framework.ViewModels.Customer.Users;
using LotusLaundry.Framework.ViewModels.Users;
//using LotusLaundry.Domain.UserAddress;



namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Order
{
    public class OrderViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
   //     [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
   //     [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [Required(ErrorMessage = "User is requried")]
      
        [JsonProperty("userId")]
        public long? UserId { get; set; }
        [Required(ErrorMessage = "Delivery Date is requried")]
        [JsonProperty("deliveryDate")]
        public DateTimeOffset? DeliveryDate { get; set; }
        [Required(ErrorMessage = "Delivery Type is requried")]
        
        [JsonProperty("deliveryType")]
        public String DeliveryType { get; set; }
        [Required(ErrorMessage = "TotalItems is requried")]
      
        [RegularExpression(RegularExpressionType.OnlyNumber, ErrorMessage = "TotalItems must be numeric")]
        [JsonProperty("totalItems")]
        public int? TotalItems { get; set; }
        
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Delivery Price")]
        [JsonProperty("deliveryPrice")]
        public decimal? DeliveryPrice { get; set; }
    
        [JsonProperty("driverId")]
        public string DriverId { get; set; }
        [Required(ErrorMessage = "Status is requried")]
        [MaxLength(30, ErrorMessage = "Status Max length 30")]
        [JsonProperty("status")]
        public string Status { get; set; }
        [MaxLength(30, ErrorMessage = "Payment Type Max length 30")]
        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }
        
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for PaidAmount")]
        [JsonProperty("paidAmount")]
        public decimal? PaidAmount { get; set; }
        [Required(ErrorMessage = "Tax is requried")]
       
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Tax")]
        [JsonProperty("tax")]
        public decimal? Tax { get; set; }
        [Required(ErrorMessage = "Sub Total is requried")]
       
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Sub Total")]
        [JsonProperty("subTotal")]
        public decimal? SubTotal { get; set; }
        [Required(ErrorMessage = "Total Price is requried")]
       
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Total Price")]
        [JsonProperty("totalPrice")]
        public decimal? TotalPrice { get; set; }
     //   [Required(ErrorMessage = "Adress Id is requried")]
        
        [JsonProperty("adressId")]
        public long? AdressId { get; set; }
    
        [JsonProperty("expectedPickUpMin")]
        public long? ExpectedPickUpMin { get; set; }
       
        [JsonProperty("expectedPickUpMax")]
        public long? ExpectedPickUpMax { get; set; }
       
        [JsonProperty("expectedDeliveryMin")]
        public long? ExpectedDeliveryMin { get; set; }
     
        [JsonProperty("expectedDeliveryMax")]
        public long? ExpectedDeliveryMax { get; set; }
    
        [JsonProperty("cartId")]
        public long? CartId { get; set; }


        //[JsonProperty("deliveryAddress")]
        //public string DeliveryAddress { get; set; }


        [JsonProperty("pickupDate")]
        public DateTimeOffset? PickupDate { get; set; }

        [JsonProperty("deliverySlot")]
        public string deliverySlot { get; set; }

        [JsonProperty("pickupSlot")]
        public string pickupSlot { get; set; }



        [JsonProperty("logisticCharge")]
        public decimal? LogisticCharge { get; set; }

        [JsonProperty("discount")]
        public decimal? Discount { get; set; }

        [JsonProperty("paymentResponse")]
        public string paymentResponse { get; set; }

        [JsonProperty("paymentStatus")]
        public string paymentStatus { get; set; }

        

        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("deliveryAddress")]
        public  UserAddressViewModel DeliveryAddress { get; set; }

        [JsonProperty("change")]
        public decimal? change { get; set; }

        [JsonProperty("note")]
        public string note { get; set; }

        [JsonProperty("orderItems")]
        public List<OrderItemViewModel> OrderItems { get; set; }


        [JsonProperty("driverDetails")]
        public UsersViewModel driverDetails { get; set; }


        [JsonProperty("customerDetails")]
        public UsersViewModel customerDetails { get; set; }


        [JsonProperty("paymentDate")]
        public DateTimeOffset? paymentDate { get; set; }
        [JsonProperty("collectedDate")]
        public DateTimeOffset? collectedDate { get; set; }
        [JsonProperty("deliveredDate")]
        public DateTimeOffset? deliveredDate { get; set; }

        // driverDetails
        [JsonProperty("adminNote")]
        public string adminNote { get; set; }

        [JsonProperty("customerName")]
        public string customerName { get; set; }

        [JsonProperty("driverName")]
        public string driverName { get; set; }

        [JsonProperty("districtName")]
        public string districtName { get; set; }

        [JsonProperty("subDistrictName")]
        public string subDistrictName { get; set; }


        [JsonProperty("customerId")]
        public string customerId { get; set; }

        [JsonProperty("deliveryDriverDetails")]
        public UsersViewModel deliveryDriverDetails { get; set; }

        [JsonProperty("pickupDriverDetail")]
        public UsersViewModel pickupDriverDetails { get; set; }

        [JsonProperty("rideType")]
        public string rideType { get; set; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }

        [JsonProperty("qrCode")]
        public string qrCode { get; set; }


        [JsonProperty("laundryInstruction")]
        public string laundryInstruction { get; set; }
    }
}

