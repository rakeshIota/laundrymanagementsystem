﻿using LotusLaundry.Domain.DistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.DistrictLanguageMapping
{
    public static class DistrictLanguageMappingModelViewModelConverter
    {
        public static DistrictLanguageMappingViewModel ToViewModel(this DistrictLanguageMappingModel x)
        {
            if (x == null) return new DistrictLanguageMappingViewModel();
            return new DistrictLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.DISTRICTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.DISTRICTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId  != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key) : null,
                DistrictId = x.DistrictId != null ? CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.DistrictId.ToString(), KeyConstant.Key) : null,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName
            };
        }

        public static DistrictLanguageMappingModel ToModel(this DistrictLanguageMappingViewModel x)
        {
            if (x == null) return new DistrictLanguageMappingModel();
            return new DistrictLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                DistrictId = x.DistrictId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.DistrictId.ToString(), KeyConstant.Key)) : 0,
                LanguageName = x.LanguageName
            };
        }
    }
}
