﻿using LotusLaundry.Domain.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Feedback
{
    public static class FeedbackModelViewModelConverter
    {
        public static FeedbackViewModel ToViewModel(this FeedbackModel x)
        {
            if (x == null) return new FeedbackViewModel();
            return new FeedbackViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.FEEDBACK + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.FEEDBACK + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				OrderId = x.OrderId,
        				Type = x.Type,
        				Message = x.Message,
        				Rating = x.Rating,
        				DriverId = x.DriverId,
        				UserId = x.UserId,
				TotalCount=x.TotalCount
            };
        }
		
		 public static FeedbackModel ToModel(this FeedbackViewModel x)
        {
            if (x == null) return new FeedbackModel();
            return new FeedbackModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			OrderId = x.OrderId,
        			Type = x.Type,
        			Message = x.Message,
        			Rating = x.Rating,
        			DriverId = x.DriverId,
        			UserId = x.UserId,
            };
        }
    }
}
