﻿using LotusLaundry.Domain.RoleMaster;
using LotusLaundry.Framework.ViewModels.RoleMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.WebExtensions
{
    public static class RoleMasterModelViewModelConverter
    {
        public static RoleMasterViewModel ToViewModel(this RoleMasterModel x)
        {
            if (x == null) return new RoleMasterViewModel();
            return new RoleMasterViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.ROLEMASTER + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.ROLEMASTER + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				Name = x.Name,
        				Description = x.Description,
				TotalCount=x.TotalCount
            };
        }
		
		 public static RoleMasterModel ToModel(this RoleMasterViewModel x)
        {
            if (x == null) return new RoleMasterModel();
            return new RoleMasterModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
        			Description = x.Description,
            };
        }


        public static RoleMasterViewModel ToRoleViewModel(this RoleMasterModel x)
        {
            if (x == null) return new RoleMasterViewModel();
            return new RoleMasterViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + x.Id.ToString(), KeyConstant.Key)
                ,Role=x.Role,
                
                Name = x.Name


            };
        }
    }
}
