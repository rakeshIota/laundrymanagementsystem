﻿using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Language
{
    public static class LanguageModelViewModelConverter
    {
        public static LanguageViewModel ToViewModel(this LanguageModel x)
        {
            if (x == null) return new LanguageViewModel();
            return new LanguageViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                TotalCount = x.TotalCount,
                Code =  x.Code
            };
        }
		
		 public static LanguageModel ToModel(this LanguageViewModel x)
        {
            if (x == null) return new LanguageModel();
            return new LanguageModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
                    Code = x.Code
            };
        }
    }
}
