﻿using LotusLaundry.Domain.CartItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.CartItemServiceMapping
{
    public static class CartItemServiceMappingModelViewModelConverter
    {
        public static CartItemServiceMappingViewModel ToViewModel(this CartItemServiceMappingModel x)
        {
            if (x == null) return new CartItemServiceMappingViewModel();
            return new CartItemServiceMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.CARTITEMSERVICEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.CARTITEMSERVICEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				CartItemId = x.CartItemId,
        				ServiceId = x.ServiceId,
        				Quantity = x.Quantity,
        				Price = x.Price,
        				SubTotal = x.SubTotal,
        				TotalPrice = x.TotalPrice,
				TotalCount=x.TotalCount
            };
        }
		
		 public static CartItemServiceMappingModel ToModel(this CartItemServiceMappingViewModel x)
        {
            if (x == null) return new CartItemServiceMappingModel();
            return new CartItemServiceMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			CartItemId = x.CartItemId,
        			ServiceId = x.ServiceId,
        			Quantity = x.Quantity,
        			Price = x.Price,
        			SubTotal = x.SubTotal,
        			TotalPrice = x.TotalPrice,
            };
        }
    }
}
