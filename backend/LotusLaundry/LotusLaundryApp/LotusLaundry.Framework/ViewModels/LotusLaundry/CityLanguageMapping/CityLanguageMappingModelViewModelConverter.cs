﻿using LotusLaundry.Domain.CityLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.CityLanguageMapping
{
    public static class CityLanguageMappingModelViewModelConverter
    {
        public static CityLanguageMappingViewModel ToViewModel(this CityLanguageMappingModel x)
        {
            if (x == null) return new CityLanguageMappingViewModel();
            return new CityLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.CITYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.CITYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId
                .ToString(), KeyConstant.Key),
                CityId = x.CityId,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName
            };
        }

        public static CityLanguageMappingModel ToModel(this CityLanguageMappingViewModel x)
        {
            if (x == null) return new CityLanguageMappingModel();
            return new CityLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                CityId = x.CityId,
                LanguageName =  x.LanguageName


            };
        }
    }
}
