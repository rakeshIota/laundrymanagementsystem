﻿using LotusLaundry.Domain.Country;
using LotusLaundry.Domain.CountryLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CountryLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Country
{
    public static class CountryModelViewModelConverter
    {
        public static CountryViewModel ToViewModel(this CountryModel x)
        {
            if (x == null) return new CountryViewModel();
            return new CountryViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.COUNTRY + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.COUNTRY + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                TotalCount = x.TotalCount,
                Name = x.Name,
                CountryLanguages = x.CountryLanguages != null ? x.CountryLanguages.Select(y => y.ToViewModel()).ToList() : null
            };
        }

        public static CountryModel ToModel(this CountryViewModel x)
        {
            if (x == null) return new CountryModel();
            return new CountryModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                CountryLanguages = x.CountryLanguages != null ? x.CountryLanguages.Select(y => y.ToModel()).ToList() : null
            };
        }
    }
}
