﻿using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProductLanguageMapping;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProductPrice;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Product
{
    public static class ProductModelViewModelConverter
    {
        public static ProductViewModel ToViewModel(this ProductModel x)
        {
            if (x == null) return new ProductViewModel();
            return new ProductViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Image = x.Image != null ? x.Image.Select(y => y.ToViewModel()).ToList() : null,
                Gender = x.Gender,
                TotalCount = x.TotalCount,
                Name = x.Name,
                ProductLanguages =  x.ProductLanguages!=null ? x.ProductLanguages.Select(y=> y.ToViewModel()).ToList() :null,
                ProductPrices = x.ProductPrices != null ? x.ProductPrices.Select(y => y.ToViewModel()).ToList() : null,
                Rank=x.Rank
            };
        }

        public static ProductModel ToModel(this ProductViewModel x)
        {
            if (x == null) return new ProductModel();
            return new ProductModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Image = x.Image != null ? x.Image.Select(y => y.ToModel()).ToList() : null,
                Gender = x.Gender,
                Name = x.Name,
                ProductLanguages = x.ProductLanguages != null ? x.ProductLanguages.Select(y => y.ToModel()).ToList() : null,
                ProductPrices = x.ProductPrices != null ? x.ProductPrices.Select(y => y.ToModel()).ToList() : null,
                Rank=x.Rank
            };
        }
    }
}
