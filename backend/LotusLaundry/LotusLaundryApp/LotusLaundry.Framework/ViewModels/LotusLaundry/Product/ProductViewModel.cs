﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProductPrice;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ProductLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Product
{
    public class ProductViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("image")]
        public List<FileGroupItemsViewModel> Image { get; set; }
        [Required(ErrorMessage = "Gender is requried")]
        [MaxLength(30, ErrorMessage = "Gender Max length 30")]
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("rank")]
        public long Rank { get; set; }

        [JsonProperty("productPrices")]
        public List<ProductPriceViewModel> ProductPrices { get; set; }
        [JsonProperty("productLanguages")]
        public List<ProductLanguageMappingViewModel> ProductLanguages { get; set; }
    }
}

