﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Domain.Users;
using LotusLaundry.Framework.WebExtensions;
using System.Globalization;

namespace LotusLaundry.Framework.ViewModels.Users
{
    public static class UsersModelViewModelConverter
    {
        

        public static UsersModel ToModel(this UsersViewModel x)
        {
            if (x == null) return new UsersModel();
            return new UsersModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,

                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                IsDeleted = x.IsDeleted,
                //UserName = x.UserName,
                //FirstName = x.FirstName,
                //LastName = x.LastName,
                //Email = x.Email,

                //Country = x.Country,
                //File = x.File != null ? x.File.Select(y => y.ToModel()).ToList() : null,

                File = x.File != null ? x.File.ToModel():null,



                FirstName = x.FirstName,
                LastName = x.LastName,
                UserName = x.Email,
                FullName = x.FirstName + " " + x.LastName,
                //ProfileImageUrl = x.ProfilePic,
                // LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key),
                MobileNo = x.PhoneNumber,
                NickName = x.NickName,
                Email = x.Email,
                PhoneNumber = x.PhoneNumber,
                // PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                //EmailConfirmed = x.EmailConfirmed,
                DOB = x.DateOfbirth,
                Gender = x.Gender,
               
                AllFile = x.AllFile != null ? x.AllFile.Select(y => y.ToModel()).ToList() : null,

            };
        }
    }
}
