﻿using LotusLaundry.Framework.ViewModels.Customer.UserAddress;
using LotusLaundry.Framework.ViewModels.Upload;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Users
{
    public class UsersbasicViewModel
    {
        
     
        [JsonProperty("emailVerifed")]
        public bool? EmailConfirmed { get; set; }

        [JsonProperty("phoneVerified")]
        public bool? PhoneVerified { get; set; }

        [JsonProperty("inServiceArea")]
        public bool? InServiceArea { get; set; }
       
        

    }

  

}
