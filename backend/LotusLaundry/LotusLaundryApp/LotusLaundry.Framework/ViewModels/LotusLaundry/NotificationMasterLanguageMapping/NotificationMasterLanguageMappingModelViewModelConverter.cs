﻿using LotusLaundry.Domain.NotificationMasterLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Language;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterLanguageMapping
{
    public static class NotificationMasterLanguageMappingModelViewModelConverter
    {
        public static NotificationMasterLanguageMappingViewModel ToViewModel(this NotificationMasterLanguageMappingModel x)
        {
            if (x == null) return new NotificationMasterLanguageMappingViewModel();
            return new NotificationMasterLanguageMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
						LanguageId = x.LanguageId != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" +  x.LanguageId.ToString(), KeyConstant.Key) : "",
						LanguageIdDetail=x.LanguageIdDetail!=null?x.LanguageIdDetail.ToViewModel():null, 	
        				Message = x.Message,
        				//NotificationMasterId = x.NotificationMasterId,
				TotalCount=x.TotalCount,
                LanguageName = x.LanguageName
            };
        }
		
		 public static NotificationMasterLanguageMappingModel ToModel(this NotificationMasterLanguageMappingViewModel x)
        {
            if (x == null) return new NotificationMasterLanguageMappingModel();
            return new NotificationMasterLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
        			Message = x.Message,
        			NotificationMasterId = x.NotificationMasterId,
            };
        }
    }
}
