﻿using LotusLaundry.Domain.ProductLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.ProductLanguageMapping
{
    public static class ProductLanguageMappingModelViewModelConverter
    {
        public static ProductLanguageMappingViewModel ToViewModel(this ProductLanguageMappingModel x)
        {
            if (x == null) return new ProductLanguageMappingViewModel();
            return new ProductLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PRODUCTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PRODUCTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key) : "",
                ProductId = x.ProductId != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.ProductId.ToString(), KeyConstant.Key) : "",
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName
            };
        }
		
		 public static ProductLanguageMappingModel ToModel(this ProductLanguageMappingViewModel x)
        {
            if (x == null) return new ProductLanguageMappingModel();
            return new ProductLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
        			LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
        			ProductId = x.ProductId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProductId.ToString(), KeyConstant.Key)) : 0,
                    LanguageName = x.LanguageName
            };
        }
    }
}
