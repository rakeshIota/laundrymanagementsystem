﻿using LotusLaundry.Domain.City;
using LotusLaundry.Framework.ViewModels.LotusLaundry.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CityLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.City
{
    public static class CityModelViewModelConverter
    {
        public static CityViewModel ToViewModel(this CityModel x)
        {
            if (x == null) return new CityViewModel();
            return new CityViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.CITY + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.CITY + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                StateId = x.StateId != null ? CryptoEngine.Encrypt(FileType.STATE + "_" + x.StateId.ToString(), KeyConstant.Key) : "",
                TotalCount = x.TotalCount,
                Name = x.Name,
                StateName = x.StateName,
                CityLanguages = x.CityLanguages != null ? x.CityLanguages.Select(y =>
y.ToViewModel()).ToList() : null
            };
        }

        public static CityModel ToModel(this CityViewModel x)
        {
            if (x == null) return new CityModel();
            return new CityModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                StateId = x.StateId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.StateId.ToString(), KeyConstant.Key)) : 0,
                Name = x.Name,
                StateName = x.StateName,
                CityLanguages = x.CityLanguages != null ? x.CityLanguages.Select(y =>
y.ToModel()).ToList() : null
            };
        }
    }
}
