﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CityLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Extra
{
    public class CallStatusViewModel
    {
       
        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("fromId")]
        public string fromId { get; set; }

        [JsonProperty("toId")]
        public string toId { get; set; }
    }
}

