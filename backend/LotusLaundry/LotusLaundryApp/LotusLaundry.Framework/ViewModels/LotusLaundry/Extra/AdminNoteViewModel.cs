﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.CityLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Extra
{
    public class AdminNoteViewModel
    {
       
        [JsonProperty("note")]
        public string note { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

       
    }
}

