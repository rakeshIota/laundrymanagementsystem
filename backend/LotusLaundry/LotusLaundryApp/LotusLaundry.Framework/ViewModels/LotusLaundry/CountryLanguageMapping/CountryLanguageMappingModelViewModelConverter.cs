﻿using LotusLaundry.Domain.CountryLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.CountryLanguageMapping
{
    public static class CountryLanguageMappingModelViewModelConverter
    {
        public static CountryLanguageMappingViewModel ToViewModel(this CountryLanguageMappingModel x)
        {
            if (x == null) return new CountryLanguageMappingViewModel();
            return new CountryLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.COUNTRYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.COUNTRYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId
                .ToString(), KeyConstant.Key),
                CountryId = x.CountryId,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName
            };
        }

        public static CountryLanguageMappingModel ToModel(this CountryLanguageMappingViewModel x)
        {
            if (x == null) return new CountryLanguageMappingModel();
            return new CountryLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                CountryId = x.CountryId,
                LanguageName = x.LanguageName
            };
        }
    }
}
