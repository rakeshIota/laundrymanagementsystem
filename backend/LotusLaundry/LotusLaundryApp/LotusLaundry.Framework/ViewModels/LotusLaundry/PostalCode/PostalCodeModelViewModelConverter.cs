﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Location
{
    public static class PostalCodeModelViewModelConverter
    {
        public static PostalCodeViewModel ToViewModel(this PostalCodeModel x)
        {
            if (x == null) return new PostalCodeViewModel();
            return new PostalCodeViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.POSTALCODE + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.POSTALCODE + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				PostalCode = x.PostalCode,
				TotalCount=x.TotalCount
            };
        }
		
		 public static PostalCodeModel ToModel(this PostalCodeViewModel x)
        {
            if (x == null) return new PostalCodeModel();
            return new PostalCodeModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			PostalCode = x.PostalCode,
            };
        }
    }
}
