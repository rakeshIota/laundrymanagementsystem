﻿using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.LotusLaundry.ViewModels.AppSetting
{
    public static class AppSettingModelViewModelConverter
    {
        public static AppSettingViewModel ToViewModel(this AppSettingModel x)
        {
            if (x == null) return new AppSettingViewModel();
            return new AppSettingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.APPSETTING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.APPSETTING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				Key = x.Key,
        				Value = x.Value,
				TotalCount=x.TotalCount
            };
        }
		
		 public static AppSettingModel ToModel(this AppSettingViewModel x)
        {
            if (x == null) return new AppSettingModel();
            return new AppSettingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Key = x.Key,
        			Value = x.Value,
            };
        }
    }
}
