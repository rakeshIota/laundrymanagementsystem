﻿using LotusLaundry.Domain.UserAddress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress
{
    public static class UserAddressModelViewModelConverter
    {
        public static UserAddressViewModel ToViewModel(this UserAddressModel x)
        {
            if (x == null) return new UserAddressViewModel();
            return new UserAddressViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERADDRESS + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //    	Slug = CryptoEngine.Encrypt(FileType.USERADDRESS + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                //CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //UserId = x.UserId,
                //AddressLine1 = x.AddressLine1,
                //AddressLine2 = x.AddressLine2,
                DistrictId = x.DistrictId != null ? CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.DistrictId.ToString(), KeyConstant.Key):null,
                SubDistrictId = x.SubDistrictId  != null ? CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + x.SubDistrictId.ToString(), KeyConstant.Key) : null,
                PostalCode = x.PostalCode,
                //IsDefault = x.IsDefault,
                Tag = x.Tag,
                ProvinceId = x.ProvinceId != null ? CryptoEngine.Encrypt(FileType.PROVINCE + "_" + x.ProvinceId.ToString(), KeyConstant.Key) : null,
                ProvinceName=x.ProvinceName,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Type = x.Type,
                Note = x.Note,
                HouseNumber = x.HouseNumber,
                StreetNumber = x.StreetNumber,
                DistrictName = x.DistrictName,
                SubDistrictName = x.SubDistrictName,

                BuildingName=x.BuildingName,
                Floor=x.Floor,
                UnitNo=x.UnitNo,
                PhoneNo=x.PhoneNo,
                alterNumber = x.alterNumber,
                ResidenceType=x.ResidenceType,

               

                //TotalCount=x.TotalCount
            };
        }

        public static UserAddressModel ToModel(this UserAddressViewModel x)
        {
            if (x == null) return new UserAddressModel();
            return new UserAddressModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                UserId = x.UserId,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                DistrictId = x.DistrictId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.DistrictId.ToString(), KeyConstant.Key)) : 0,
                SubDistrictId = x.SubDistrictId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.SubDistrictId.ToString(), KeyConstant.Key)) : 0,
                PostalCode = x.PostalCode,
                IsDefault = x.IsDefault,
                Tag = x.Tag,
                ProvinceId = x.ProvinceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProvinceId.ToString(), KeyConstant.Key)) : 0, //x.ProvinceId,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Type = x.Type,
                Note = x.Note,
                HouseNumber = x.HouseNumber,
                StreetNumber = x.StreetNumber,
                DistrictName = x.DistrictName,
                ProvinceName=x.ProvinceName,
                BuildingName = x.BuildingName,
                Floor = x.Floor,
                UnitNo = x.UnitNo,
                PhoneNo = x.PhoneNo,
                alterNumber = x.alterNumber,
                ResidenceType = x.ResidenceType,

            };
        }
    }
}
