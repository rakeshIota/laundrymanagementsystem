﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress
{
    public class UserAddressViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("userId")]
        public long? UserId { get; set; }
        [JsonProperty("addressLine1")]
        public string AddressLine1 { get; set; }
        [JsonProperty("addressLine2")]
        public string AddressLine2 { get; set; }
        [JsonProperty("districtId")]
        public string DistrictId { get; set; }
        [JsonProperty("subDistrictId")]
        public string SubDistrictId { get; set; }
        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }
        [JsonProperty("isDefault")]
        public bool? IsDefault { get; set; }
        [JsonProperty("tag")]
        public string Tag { get; set; }
        [JsonProperty("provinceId")]
        public string ProvinceId { get; set; }

        [JsonProperty("provinceName")]
        public string ProvinceName { get; set; }



        [JsonProperty("latitude")]
        public decimal? Latitude { get; set; }
        [JsonProperty("longitude")]
        public decimal? Longitude { get; set; }
        [JsonIgnore]
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("houseNumber")]
        public string HouseNumber { get; set; }
        [JsonProperty("streetNumber")]
        public string StreetNumber { get; set; }
        [JsonProperty("note")]
        public string Note { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("districtName")]
        public string DistrictName { get; set; }
        [JsonProperty("subDistrictName")]
        public string SubDistrictName { get; set; }

        [JsonProperty("buildingName")]
        public string BuildingName { get; set; }

        [JsonProperty("floor")]
        public string Floor { get; set; }

        [JsonProperty("unit")]
        public string UnitNo { get; set; }

        [JsonProperty("phoneNo")]
        public string PhoneNo { get; set; }

        [JsonProperty("ext")]
        public string Ext { get; set; }


        [JsonProperty("alternatePhoneNo")]
        public string alterNumber { get; set; }

        [JsonProperty("residenceType")]
        public string ResidenceType { get; set; }

    }
}

