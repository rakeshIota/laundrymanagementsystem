﻿using LotusLaundry.Domain.NotificationMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterKeyValueMapping;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMaster
{
    public static class NotificationMasterModelViewModelConverter
    {
        public static NotificationMasterViewModel ToViewModel(this NotificationMasterModel x)
        {
            if (x == null) return new NotificationMasterViewModel();
            return new NotificationMasterViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTER + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTER + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Type = x.Type,
                CanBeDelete = x.CanBeDelete,
                TotalCount = x.TotalCount,
                Message = x.Message,
                NotificationMasterKeyValues = x.NotificationMasterKeyValues != null ? x.NotificationMasterKeyValues.Select(y => y.ToViewModel()).ToList() : null,
                NotificationMasterLanguages = x.NotificationMasterLanguages != null ? x.NotificationMasterLanguages.Select(y => y.ToViewModel()).ToList() : null
            };
        }

        public static NotificationMasterModel ToModel(this NotificationMasterViewModel x)
        {
            if (x == null) return new NotificationMasterModel();
            return new NotificationMasterModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Type = x.Type,
                CanBeDelete = x.CanBeDelete,
                Message = x.Message,
                NotificationMasterKeyValues = x.NotificationMasterKeyValues != null ? x.NotificationMasterKeyValues.Select(y => y.ToModel()).ToList() : null,
                NotificationMasterLanguages = x.NotificationMasterLanguages != null ? x.NotificationMasterLanguages.Select(y => y.ToModel()).ToList() : null
            };
        }
    }
}
