﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterKeyValueMapping;
using LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMaster
{
    public class NotificationMasterViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [MaxLength(100, ErrorMessage = "Type Max length 100")]
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("canBeDelete")]
        public bool? CanBeDelete { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("notificationMasterKeyValues")]
        public List<NotificationMasterKeyValueMappingViewModel> NotificationMasterKeyValues { get; set; }
        [JsonProperty("notificationMasterLanguages")]
        public List<NotificationMasterLanguageMappingViewModel> NotificationMasterLanguages { get; set; }
    }
}

