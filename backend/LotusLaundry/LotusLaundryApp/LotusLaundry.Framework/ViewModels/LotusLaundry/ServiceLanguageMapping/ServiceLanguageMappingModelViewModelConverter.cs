﻿using LotusLaundry.Domain.ServiceLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.ServiceLanguageMapping
{
    public static class ServiceLanguageMappingModelViewModelConverter
    {
        public static ServiceLanguageMappingViewModel ToViewModel(this ServiceLanguageMappingModel x)
        {
            if (x == null) return new ServiceLanguageMappingViewModel();
            return new ServiceLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SERVICELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.SERVICELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId
                .ToString(), KeyConstant.Key),
                ServiceId = x.ServiceId,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName,
                Description =  x.Description
            };
        }

        public static ServiceLanguageMappingModel ToModel(this ServiceLanguageMappingViewModel x)
        {
            if (x == null) return new ServiceLanguageMappingModel();
            return new ServiceLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                ServiceId = x.ServiceId,
                Description = x.Description
            };
        }
    }
}
