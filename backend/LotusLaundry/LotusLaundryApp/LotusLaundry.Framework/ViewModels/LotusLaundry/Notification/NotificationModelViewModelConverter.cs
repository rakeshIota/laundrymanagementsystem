﻿using LotusLaundry.Domain.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Notification
{
    public static class NotificationModelViewModelConverter
    {
        public static NotificationViewModel ToViewModel(this NotificationModel x)
        {
            if (x == null) return new NotificationViewModel();
            return new NotificationViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.NOTIFICATION + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.NOTIFICATION + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				UserId = x.UserId,
        				Message = x.Message,
        				LanguageId = x.LanguageId,
        				Type = x.Type,
        				Token = x.Token,
        				Status = x.Status,
        				Response = x.Response,
				TotalCount=x.TotalCount
            };
        }
		
		 public static NotificationModel ToModel(this NotificationViewModel x)
        {
            if (x == null) return new NotificationModel();
            return new NotificationModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			UserId = x.UserId,
        			Message = x.Message,
        			LanguageId = x.LanguageId,
        			Type = x.Type,
        			Token = x.Token,
        			Status = x.Status,
        			Response = x.Response,
            };
        }
    }
}
