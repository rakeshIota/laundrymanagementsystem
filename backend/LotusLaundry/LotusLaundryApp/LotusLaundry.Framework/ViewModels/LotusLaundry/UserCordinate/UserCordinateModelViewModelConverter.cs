﻿using LotusLaundry.Domain.UserCordinate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.UserCordinate
{
    public static class UserCordinateModelViewModelConverter
    {
        public static UserCordinateViewModel ToViewModel(this UserCordinateModel x)
        {
            if (x == null) return new UserCordinateViewModel();
            return new UserCordinateViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.USERCORDINATE + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.USERCORDINATE + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				UserId = x.UserId,
        				Latitude = x.Latitude,
        				Longitude = x.Longitude,
        				OrderId = x.OrderId,
				TotalCount=x.TotalCount
            };
        }
		
		 public static UserCordinateModel ToModel(this UserCordinateViewModel x)
        {
            if (x == null) return new UserCordinateModel();
            return new UserCordinateModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			UserId = x.UserId,
        			Latitude = x.Latitude,
        			Longitude = x.Longitude,
        			OrderId = x.OrderId,
            };
        }
    }
}
