﻿using LotusLaundry.Domain.NotificationMasterKeyValueMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.NotificationMasterKeyValueMapping
{
    public static class NotificationMasterKeyValueMappingModelViewModelConverter
    {
        public static NotificationMasterKeyValueMappingViewModel ToViewModel(this NotificationMasterKeyValueMappingModel x)
        {
            if (x == null) return new NotificationMasterKeyValueMappingViewModel();
            return new NotificationMasterKeyValueMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERKEYVALUEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.NOTIFICATIONMASTERKEYVALUEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				Key = x.Key,
        				Value = x.Value,
        				NotificationId = x.NotificationId,
				TotalCount=x.TotalCount
            };
        }
		
		 public static NotificationMasterKeyValueMappingModel ToModel(this NotificationMasterKeyValueMappingViewModel x)
        {
            if (x == null) return new NotificationMasterKeyValueMappingModel();
            return new NotificationMasterKeyValueMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Key = x.Key,
        			Value = x.Value,
        			NotificationId = x.NotificationId,
            };
        }
    }
}
