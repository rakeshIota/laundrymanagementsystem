﻿using LotusLaundry.Domain.PackingLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.PackingLanguageMapping
{
    public static class PackingLanguageMappingModelViewModelConverter
    {
        public static PackingLanguageMappingViewModel ToViewModel(this PackingLanguageMappingModel x)
        {
            if (x == null) return new PackingLanguageMappingViewModel();
            return new PackingLanguageMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.PACKINGLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.PACKINGLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				PackingId = x.PackingId,
        				LanguageId = x.LanguageId,
        				Name = x.Name,
				TotalCount=x.TotalCount
            };
        }
		
		 public static PackingLanguageMappingModel ToModel(this PackingLanguageMappingViewModel x)
        {
            if (x == null) return new PackingLanguageMappingModel();
            return new PackingLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			PackingId = x.PackingId,
        			LanguageId = x.LanguageId,
        			Name = x.Name,
            };
        }
    }
}
