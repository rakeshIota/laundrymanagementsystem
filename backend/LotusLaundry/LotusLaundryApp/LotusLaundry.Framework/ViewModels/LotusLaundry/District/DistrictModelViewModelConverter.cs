﻿using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.DistrictLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.District
{
    public static class DistrictModelViewModelConverter
    {
        public static DistrictViewModel ToViewModel(this DistrictModel x)
        {
            if (x == null) return new DistrictViewModel();
            return new DistrictViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                TotalCount = x.TotalCount,
                Name = x.Name,
                CityName = x.CityName,
                DistrictLanguages = x.DistrictLanguages != null ? x.DistrictLanguages.Select(y => y.ToViewModel()).ToList() : null,
                CityId = x.CityId != null? CryptoEngine.Encrypt(FileType.CITY + "_" + x.CityId.ToString(), KeyConstant.Key) : null
            };
        }

        public static DistrictModel ToModel(this DistrictViewModel x)
        {
            if (x == null) return new DistrictModel();
            return new DistrictModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                DistrictLanguages = x.DistrictLanguages != null ? x.DistrictLanguages.Select(y => y.ToModel()).ToList() : null,
                CityId = x.CityId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.CityId.ToString(), KeyConstant.Key)) : 0,
                Name = x.Name
            };
        }
    }
}
