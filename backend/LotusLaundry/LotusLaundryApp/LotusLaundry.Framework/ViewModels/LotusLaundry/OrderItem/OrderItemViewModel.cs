﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Product;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Service;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem
{
   public class OrderItemViewModel
    {
               [JsonProperty("id")]
				public string Id { get; set; }
				[JsonProperty("tenantId")]
				public int? TenantId { get; set; }
				[JsonProperty("slug")]
        		public string Slug { get; set; }
			  	[JsonProperty("createdBy")]
			  	public long? CreatedBy { get; set; }
			  	[JsonProperty("updatedBy")]
			  	public long? UpdatedBy { get; set; }
				[JsonProperty("createdOn")]
			  	public DateTimeOffset?  CreatedOn { get; set; }			
				[JsonProperty("updatedOn")]
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			
				[JsonProperty("isDeleted")]
			  	public bool? IsDeleted { get; set; }
			
				[JsonProperty("isActive")]
			  	public bool? IsActive { get; set; }
		
			
			  	[JsonProperty("orderId")]
			  	public long? OrderId { get; set; }
			[Required(ErrorMessage = "Product is requried")]
			  	[JsonProperty("productId")]
			  	public string ProductId { get; set; }
		 	[RegularExpression(RegularExpressionType.OnlyNumber, ErrorMessage = "Quantity must be numeric")]
				[JsonProperty("quantity")]
				public int? Quantity { get; set; }
			[Required(ErrorMessage = "Is Packing is requried")]
				[JsonProperty("isPacking")]
			  	public bool? IsPacking { get; set; }
			[Required(ErrorMessage = "PackingPrice is requried")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for PackingPrice")]
				[JsonProperty("packingPrice")]
			  	public decimal? PackingPrice { get; set; }
			[Required(ErrorMessage = "SubTotal is requried")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for SubTotal")]
				[JsonProperty("subTotal")]
			  	public decimal? SubTotal { get; set; }
			[Required(ErrorMessage = "Total Price is requried")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Total Price")]
				[JsonProperty("totalPrice")]
			  	public decimal? TotalPrice { get; set; }
			
			  	[JsonProperty("packingId")]
			  	public long? PackingId { get; set; }

        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }

		[JsonProperty("product")]
		public ProductViewModel Product { get; set; }
		[JsonProperty("service")]
		public ServiceViewModel Service { get; set; }



		[JsonProperty("totalCount")]
		  	public int TotalCount  { get; set; }
    }
}

