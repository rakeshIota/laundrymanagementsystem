﻿using LotusLaundry.Domain.OrderItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.Product;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem
{
    public static class OrderItemModelViewModelConverter
    {
        public static OrderItemViewModel ToViewModel(this OrderItemModel x)
        {
            if (x == null) return new OrderItemViewModel();
            return new OrderItemViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.ORDERITEM + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.ORDERITEM + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				OrderId = x.OrderId,
        				ProductId = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.ProductId.ToString(), KeyConstant.Key),// x.ProductId,
        				Quantity = x.Quantity,
        				IsPacking = x.IsPacking,
        				PackingPrice = x.PackingPrice,
        				SubTotal = x.SubTotal,
        				TotalPrice = x.TotalPrice,
        				PackingId = x.PackingId,
                        ServiceId= CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.ServiceId.ToString(), KeyConstant.Key),//x.ServiceId,

                Product = new Product.ProductViewModel
                {
                    Id = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.ProductId.ToString(), KeyConstant.Key),
                    Name = x.ProductName,
                    Icon = x.Icon
                },
                Service = new Service.ServiceViewModel
                {
                    Id = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.ServiceId.ToString(), KeyConstant.Key),
                    Name = x.ServiceName
                },


                TotalCount =x.TotalCount
            };
        }
		
		 public static OrderItemModel ToModel(this OrderItemViewModel x)
        {
            if (x == null) return new OrderItemModel();
            return new OrderItemModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			OrderId = x.OrderId,
        			ProductId = x.ProductId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProductId.ToString(), KeyConstant.Key)) : 0,// x.ProductId,
        			Quantity = x.Quantity,
        			IsPacking = x.IsPacking,
        			PackingPrice = x.PackingPrice,
        			SubTotal = x.SubTotal,
        			TotalPrice = x.TotalPrice,
        			PackingId = x.PackingId,
                    ServiceId= x.ServiceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ServiceId.ToString(), KeyConstant.Key)) : 0,//x.ServiceId

            };
        }
    }
}
