﻿using LotusLaundry.Domain.Currency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Currency
{
    public static class CurrencyModelViewModelConverter
    {
        public static CurrencyViewModel ToViewModel(this CurrencyModel x)
        {
            if (x == null) return new CurrencyViewModel();
            return new CurrencyViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.CURRENCY + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.CURRENCY + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				Price = x.Price,
				TotalCount=x.TotalCount
            };
        }
		
		 public static CurrencyModel ToModel(this CurrencyViewModel x)
        {
            if (x == null) return new CurrencyModel();
            return new CurrencyModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Price = x.Price,
            };
        }
    }
}
