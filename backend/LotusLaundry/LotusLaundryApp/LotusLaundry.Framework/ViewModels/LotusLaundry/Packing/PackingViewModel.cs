﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Packing
{
   public class PackingViewModel
    {
               [JsonProperty("id")]
				public string Id { get; set; }
				[JsonProperty("tenantId")]
				public int? TenantId { get; set; }
				[JsonProperty("slug")]
        		public string Slug { get; set; }
			  	[JsonProperty("createdBy")]
			  	public long? CreatedBy { get; set; }
			  	[JsonProperty("updatedBy")]
			  	public long? UpdatedBy { get; set; }
				[JsonProperty("createdOn")]
			  	public DateTimeOffset?  CreatedOn { get; set; }			
				[JsonProperty("updatedOn")]
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			[Required(ErrorMessage = "Deleted? is requried")]
				[JsonProperty("isDeleted")]
			  	public bool? IsDeleted { get; set; }
			[Required(ErrorMessage = "Active? is requried")]
				[JsonProperty("isActive")]
			  	public bool? IsActive { get; set; }
			[Required(ErrorMessage = "Product is requried")]
			[Range(0, 9999999999999999, ErrorMessage = "Product value is more than max length of 100")]
			  	[JsonProperty("productId")]
			  	public long? ProductId { get; set; }
			[Required(ErrorMessage = "Service is requried")]
			[Range(0, 9999999999999999, ErrorMessage = "Service value is more than max length of 100")]
			  	[JsonProperty("serviceId")]
			  	public long? ServiceId { get; set; }
			[Required(ErrorMessage = "Price is requried")]
			[Range(0.1, 100, ErrorMessage = "Price value is more than max limit of 100")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Price")]
				[JsonProperty("price")]
			  	public decimal? Price { get; set; }
			[JsonProperty("totalCount")]
		  	public int TotalCount  { get; set; }
    }
}

