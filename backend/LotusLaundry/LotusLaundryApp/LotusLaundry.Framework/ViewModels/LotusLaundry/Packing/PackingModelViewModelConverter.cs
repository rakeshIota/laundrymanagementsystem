﻿using LotusLaundry.Domain.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Packing
{
    public static class PackingModelViewModelConverter
    {
        public static PackingViewModel ToViewModel(this PackingModel x)
        {
            if (x == null) return new PackingViewModel();
            return new PackingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.PACKING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.PACKING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				ProductId = x.ProductId,
        				ServiceId = x.ServiceId,
        				Price = x.Price,
				TotalCount=x.TotalCount
            };
        }
		
		 public static PackingModel ToModel(this PackingViewModel x)
        {
            if (x == null) return new PackingModel();
            return new PackingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			ProductId = x.ProductId,
        			ServiceId = x.ServiceId,
        			Price = x.Price,
            };
        }
    }
}
