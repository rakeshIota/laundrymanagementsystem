﻿using LotusLaundry.Domain.DeliveryType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.DeliveryType
{
    public static class DeliveryTypeModelViewModelConverter
    {
        public static DeliveryTypeViewModel ToViewModel(this DeliveryTypeModel x)
        {
            if (x == null) return new DeliveryTypeViewModel();
            return new DeliveryTypeViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.DELIVERYTYPE + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.DELIVERYTYPE + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				PriceType = x.PriceType,
        				Price = x.Price,
				TotalCount=x.TotalCount
            };
        }
		
		 public static DeliveryTypeModel ToModel(this DeliveryTypeViewModel x)
        {
            if (x == null) return new DeliveryTypeModel();
            return new DeliveryTypeModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			PriceType = x.PriceType,
        			Price = x.Price,
            };
        }
    }
}
