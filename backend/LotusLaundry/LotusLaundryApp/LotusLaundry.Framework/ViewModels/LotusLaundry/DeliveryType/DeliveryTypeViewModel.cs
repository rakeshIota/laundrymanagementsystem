﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.DeliveryType
{
   public class DeliveryTypeViewModel
    {
               [JsonProperty("id")]
				public string Id { get; set; }
				[JsonProperty("tenantId")]
				public int? TenantId { get; set; }
				[JsonProperty("slug")]
        		public string Slug { get; set; }
			  	[JsonProperty("createdBy")]
			  	public long? CreatedBy { get; set; }
			  	[JsonProperty("updatedBy")]
			  	public long? UpdatedBy { get; set; }
				[JsonProperty("createdOn")]
			  	public DateTimeOffset?  CreatedOn { get; set; }			
				[JsonProperty("updatedOn")]
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
			[Required(ErrorMessage = "Deleted? is requried")]
				[JsonProperty("isDeleted")]
			  	public bool? IsDeleted { get; set; }
			[Required(ErrorMessage = "Active? is requried")]
				[JsonProperty("isActive")]
			  	public bool? IsActive { get; set; }
			[MaxLength(100, ErrorMessage = "PriceType Max length 100")]
				[JsonProperty("priceType")]
        		public string PriceType { get; set; }
			[Required(ErrorMessage = "Price is requried")]
			[Range(0.1, 100, ErrorMessage = "Price value is more than max limit of 100")]
		 	[RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Price")]
				[JsonProperty("price")]
			  	public decimal? Price { get; set; }
			[JsonProperty("totalCount")]
		  	public int TotalCount  { get; set; }
    }
}

