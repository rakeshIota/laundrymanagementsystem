﻿using LotusLaundry.Domain.ProductPrice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.ProductPrice
{
    public static class ProductPriceModelViewModelConverter
    {
        public static ProductPriceViewModel ToViewModel(this ProductPriceModel x)
        {
            if (x == null) return new ProductPriceViewModel();
            return new ProductPriceViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PRODUCTPRICE + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PRODUCTPRICE + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                ServiceId = x.ServiceId != null ? CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.ServiceId.ToString(), KeyConstant.Key) : "",
                ProductId = x.ProductId != null ? CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.ProductId.ToString(), KeyConstant.Key) : "",
                Price = x.Price,
                TotalCount = x.TotalCount,
                ServiceName = x.ServiceName
            };
        }

        public static ProductPriceModel ToModel(this ProductPriceViewModel x)
        {
            if (x == null) return new ProductPriceModel();
            return new ProductPriceModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                ServiceId = x.ServiceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ServiceId.ToString(), KeyConstant.Key)) : 0,
                ProductId = x.ProductId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProductId.ToString(), KeyConstant.Key)) : 0,
                Price = x.Price,
                ServiceName = x.ServiceName

            };
        }
    }
}
