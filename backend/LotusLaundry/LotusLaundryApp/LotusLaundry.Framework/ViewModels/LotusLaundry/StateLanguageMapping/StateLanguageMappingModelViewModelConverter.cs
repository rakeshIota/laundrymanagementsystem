﻿using LotusLaundry.Domain.StateLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.StateLanguageMapping
{
    public static class StateLanguageMappingModelViewModelConverter
    {
        public static StateLanguageMappingViewModel ToViewModel(this StateLanguageMappingModel x)
        {
            if (x == null) return new StateLanguageMappingViewModel();
            return new StateLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.STATELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.STATELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId
                .ToString(), KeyConstant.Key),
                StateId = x.StateId,
                TotalCount = x.TotalCount,
                LanguageName =  x.LanguageName
            };
        }

        public static StateLanguageMappingModel ToModel(this StateLanguageMappingViewModel x)
        {
            if (x == null) return new StateLanguageMappingModel();
            return new StateLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                StateId = x.StateId,
                LanguageName = x.LanguageName
            };
        }
    }
}
