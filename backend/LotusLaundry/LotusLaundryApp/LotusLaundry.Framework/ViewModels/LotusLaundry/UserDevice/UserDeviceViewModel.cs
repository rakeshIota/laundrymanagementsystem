﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.UserDevice
{
   public class UserDeviceViewModel
    {
               [JsonProperty("id")]
				public string Id { get; set; }
				[JsonProperty("tenantId")]
				public int? TenantId { get; set; }
				[JsonProperty("slug")]
        		public string Slug { get; set; }
			  	[JsonProperty("createdBy")]
			  	public long? CreatedBy { get; set; }
			  	[JsonProperty("updatedBy")]
			  	public long? UpdatedBy { get; set; }
				[JsonProperty("createdOn")]
			  	public DateTimeOffset?  CreatedOn { get; set; }			
				[JsonProperty("updatedOn")]
			  	public DateTimeOffset?  UpdatedOn { get; set; }			
	
				[JsonProperty("isDeleted")]
			  	public bool? IsDeleted { get; set; }
		
				[JsonProperty("isActive")]
			  	public bool? IsActive { get; set; }
		
			  	[JsonProperty("userId")]
			  	public long? UserId { get; set; }
			    [MaxLength(100, ErrorMessage = "Device Max length 100")]
				[JsonProperty("deviceId")]
        		public string DeviceId { get; set; }
			    //[MaxLength(100, ErrorMessage = "Notification Token Max length 100")]
				[JsonProperty("notificationToken")]
        		public string NotificationToken { get; set; }
			[JsonProperty("totalCount")]
		  	public int TotalCount  { get; set; }
    }
}

