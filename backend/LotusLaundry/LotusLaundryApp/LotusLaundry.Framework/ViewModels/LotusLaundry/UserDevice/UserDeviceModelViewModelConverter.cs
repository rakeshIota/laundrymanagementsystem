﻿using LotusLaundry.Domain.UserDevice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.UserDevice
{
    public static class UserDeviceModelViewModelConverter
    {
        public static UserDeviceViewModel ToViewModel(this UserDeviceModel x)
        {
            if (x == null) return new UserDeviceViewModel();
            return new UserDeviceViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.USERDEVICE + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.USERDEVICE + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				UserId = x.UserId,
        				DeviceId = x.DeviceId,
        				NotificationToken = x.NotificationToken,
				TotalCount=x.TotalCount
            };
        }
		
		 public static UserDeviceModel ToModel(this UserDeviceViewModel x)
        {
            if (x == null) return new UserDeviceModel();
            return new UserDeviceModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			UserId = x.UserId,
        			DeviceId = x.DeviceId,
        			NotificationToken = x.NotificationToken,
            };
        }
    }
}
