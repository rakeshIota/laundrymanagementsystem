﻿using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ServiceLanguageMapping;
using LotusLaundry.Framework.WebExtensions;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Service
{
    public static class ServiceModelViewModelConverter
    {
        public static ServiceViewModel ToViewModel(this ServiceModel x)
        {
            if (x == null) return new ServiceViewModel();
            return new ServiceViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                TotalCount = x.TotalCount,
                Name = x.Name,
                ServiceLanguages = x.ServiceLanguages != null ? x.ServiceLanguages.Select(y => y.ToViewModel()).ToList() : null,
                Image = x.Image != null ? x.Image.Select(y => y.ToViewModel()).ToList() : null,
                Description = x.Description
            };
        }

        public static ServiceModel ToModel(this ServiceViewModel x)
        {
            if (x == null) return new ServiceModel();
            return new ServiceModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                ServiceLanguages = x.ServiceLanguages!=null ? x.ServiceLanguages.Select(y=> y.ToModel()).ToList():null,
                Image = x.Image != null ? x.Image.Select(y => y.ToModel()).ToList() : null,
                Description = x.Description

            };
        }
    }
}
