﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.LotusLaundry.ServiceLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Service
{
    public class ServiceViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("serviceLanguages")]
        public List<ServiceLanguageMappingViewModel> ServiceLanguages { get; set; }

        [JsonProperty("image")]
        public List<FileGroupItemsViewModel> Image { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        
        

    }
}

