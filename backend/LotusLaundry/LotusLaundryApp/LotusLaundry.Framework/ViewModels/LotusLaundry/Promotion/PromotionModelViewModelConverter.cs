﻿using LotusLaundry.Domain.Promotion;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Promotion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Framework.ViewModels.LotusLaundry.Promotion;
using LotusLaundry.Domain.Promotion;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Promotion
{ 
    public static class PromotionModelViewModelConverter
    {
        public static PromotionViewModel ToViewModel(this PromotionModel x)
        {
            if (x == null) return new PromotionViewModel();
            return new PromotionViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PROMOTION + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PROMOTION + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,

                title = x.title,
              
                File = x.File != null ? x.File.Select(y => y.ToViewModel()).ToList() : null,
                url = x.url,
                
                TotalCount = x.TotalCount
            };
        }

        public static PromotionModel ToModel(this PromotionViewModel x)
        {
            if (x == null) return new PromotionModel();
            return new PromotionModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,

                File = x.File != null ? x.File.Select(y => y.ToModel()).ToList() : null,

                title = x.title,
              


                url = x.url,
             



            };
        }
    }
}
