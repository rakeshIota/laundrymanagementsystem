﻿using LotusLaundry.Domain.ProvinceLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.ProvinceLanguageMapping
{
    public static class ProvinceLanguageMappingModelViewModelConverter
    {
        public static ProvinceLanguageMappingViewModel ToViewModel(this ProvinceLanguageMappingModel x)
        {
            if (x == null) return new ProvinceLanguageMappingViewModel();
            return new ProvinceLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PROVINCELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.PROVINCELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName,
                ProvinceId =  x.ProvinceId != null ? CryptoEngine.Encrypt(FileType.PROVINCE + "_" + x.ProvinceId.ToString(), KeyConstant.Key) :null,
                LanguageId = x.LanguageId != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key) : null,
                Name = x.Name
            };
        }

        public static ProvinceLanguageMappingModel ToModel(this ProvinceLanguageMappingViewModel x)
        {
            if (x == null) return new ProvinceLanguageMappingModel();
            return new ProvinceLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                LanguageName = x.LanguageName,
                ProvinceId = x.ProvinceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProvinceId.ToString(), KeyConstant.Key)) : 0,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                Name = x.Name 
            };
        }
    }
}
