﻿using LotusLaundry.Domain.DefaultMessageLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.DefaultMessageLanguageMapping
{
    public static class DefaultMessageLanguageMappingModelViewModelConverter
    {
        public static DefaultMessageLanguageMappingViewModel ToViewModel(this DefaultMessageLanguageMappingModel x)
        {
            if (x == null) return new DefaultMessageLanguageMappingViewModel();
            return new DefaultMessageLanguageMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.DEFAULTMESSAGELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.DEFAULTMESSAGELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				DefaultMessageId = x.DefaultMessageId,
        				LanguageId = x.LanguageId,
        				Message = x.Message,
				TotalCount=x.TotalCount
            };
        }
		
		 public static DefaultMessageLanguageMappingModel ToModel(this DefaultMessageLanguageMappingViewModel x)
        {
            if (x == null) return new DefaultMessageLanguageMappingModel();
            return new DefaultMessageLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			DefaultMessageId = x.DefaultMessageId,
        			LanguageId = x.LanguageId,
        			Message = x.Message,
            };
        }
    }
}
