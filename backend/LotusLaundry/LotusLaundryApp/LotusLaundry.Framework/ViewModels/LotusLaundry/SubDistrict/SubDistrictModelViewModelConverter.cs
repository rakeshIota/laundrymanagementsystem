﻿using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.SubDistrictLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.SubDistrict
{
    public static class SubDistrictModelViewModelConverter
    {
        public static SubDistrictViewModel ToViewModel(this SubDistrictModel x)
        {
            if (x == null) return new SubDistrictViewModel();
            return new SubDistrictViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                DistrictId = x.DistrictId != null ? CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.Id.ToString(), KeyConstant.Key):null,
                TotalCount = x.TotalCount,
                Name = x.Name,
                DistrictName = x.DistrictName,
                SubDistrictLanguages =  x.SubDistrictLanguages != null ? x.SubDistrictLanguages.Select(y=> y.ToViewModel()).ToList():null
            };
        }
		
		 public static SubDistrictModel ToModel(this SubDistrictViewModel x)
        {
            if (x == null) return new SubDistrictModel();
            return new SubDistrictModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			DistrictId = x.DistrictId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.DistrictId.ToString(), KeyConstant.Key)) : 0,
                SubDistrictLanguages = x.SubDistrictLanguages != null ? x.SubDistrictLanguages.Select(y => y.ToModel()).ToList() : null,
                Name = x.Name,
                DistrictName = x.DistrictName
            };
        }
    }
}
