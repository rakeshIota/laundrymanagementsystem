﻿using LotusLaundry.Domain.OrderItemServiceMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItemServiceMapping
{
    public static class OrderItemServiceMappingModelViewModelConverter
    {
        public static OrderItemServiceMappingViewModel ToViewModel(this OrderItemServiceMappingModel x)
        {
            if (x == null) return new OrderItemServiceMappingViewModel();
            return new OrderItemServiceMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.ORDERITEMSERVICEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.ORDERITEMSERVICEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				OrderItemId = x.OrderItemId,
        				ServiceId = x.ServiceId,
        				Quantity = x.Quantity,
        				Price = x.Price,
        				SubTotal = x.SubTotal,
        				TotalPrice = x.TotalPrice,
				TotalCount=x.TotalCount
            };
        }
		
		 public static OrderItemServiceMappingModel ToModel(this OrderItemServiceMappingViewModel x)
        {
            if (x == null) return new OrderItemServiceMappingModel();
            return new OrderItemServiceMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			OrderItemId = x.OrderItemId,
        			ServiceId = x.ServiceId,
        			Quantity = x.Quantity,
        			Price = x.Price,
        			SubTotal = x.SubTotal,
        			TotalPrice = x.TotalPrice,
            };
        }
    }
}
