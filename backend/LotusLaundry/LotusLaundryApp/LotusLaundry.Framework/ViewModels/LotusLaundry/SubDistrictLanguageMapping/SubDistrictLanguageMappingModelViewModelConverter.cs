﻿using LotusLaundry.Domain.SubDistrictLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.SubDistrictLanguageMapping
{
    public static class SubDistrictLanguageMappingModelViewModelConverter
    {
        public static SubDistrictLanguageMappingViewModel ToViewModel(this SubDistrictLanguageMappingModel x)
        {
            if (x == null) return new SubDistrictLanguageMappingViewModel();
            return new SubDistrictLanguageMappingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SUBDISTRICTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.SUBDISTRICTLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key) : null,
                SubDistrictId = x.SubDistrictId != null ? CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + x.SubDistrictId.ToString(), KeyConstant.Key) : null,
                TotalCount = x.TotalCount,
                LanguageName = x.LanguageName
            };
        }

        public static SubDistrictLanguageMappingModel ToModel(this SubDistrictLanguageMappingViewModel x)
        {
            if (x == null) return new SubDistrictLanguageMappingModel();
            return new SubDistrictLanguageMappingModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                LanguageId = x.LanguageId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.LanguageId.ToString(), KeyConstant.Key)) : 0,
                SubDistrictId = x.SubDistrictId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.SubDistrictId.ToString(), KeyConstant.Key)) : 0,
                LanguageName = x.LanguageName
            };
        }
    }
}
