﻿using LotusLaundry.Domain.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.LotusLaundry.StateLanguageMapping;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.State
{
    public static class StateModelViewModelConverter
    {
        public static StateViewModel ToViewModel(this StateModel x)
        {
            if (x == null) return new StateViewModel();
            return new StateViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.STATE + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.STATE + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                CountryId =  CryptoEngine.Encrypt(FileType.COUNTRY + "_" + x.CountryId.ToString(), KeyConstant.Key),
                TotalCount = x.TotalCount,
                Name = x.Name,
                StateLanguages = x.StateLanguages != null ? x.StateLanguages.Select(y => y.ToViewModel()).ToList() : null,
                CountryName = x.CountryName

            };
        }

        public static StateModel ToModel(this StateViewModel x)
        {
            if (x == null) return new StateModel();
            return new StateModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Name = x.Name,
                CountryId = x.CountryId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.CountryId.ToString(), KeyConstant.Key)) : 0,
                StateLanguages = x.StateLanguages != null ? x.StateLanguages.Select(y => y.ToModel()).ToList() : null,
                CountryName = x.CountryName
            };
        }
    }
}
