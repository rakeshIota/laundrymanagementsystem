﻿using LotusLaundry.Domain.DeliveryTypeLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.DeliveryTypeLanguageMapping
{
    public static class DeliveryTypeLanguageMappingModelViewModelConverter
    {
        public static DeliveryTypeLanguageMappingViewModel ToViewModel(this DeliveryTypeLanguageMappingModel x)
        {
            if (x == null) return new DeliveryTypeLanguageMappingViewModel();
            return new DeliveryTypeLanguageMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.DELIVERYTYPELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.DELIVERYTYPELANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				Name = x.Name,
        				LanguageId = x.LanguageId,
        				DeliveryTypeId = x.DeliveryTypeId,
				TotalCount=x.TotalCount
            };
        }
		
		 public static DeliveryTypeLanguageMappingModel ToModel(this DeliveryTypeLanguageMappingViewModel x)
        {
            if (x == null) return new DeliveryTypeLanguageMappingModel();
            return new DeliveryTypeLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
        			LanguageId = x.LanguageId,
        			DeliveryTypeId = x.DeliveryTypeId,
            };
        }
    }
}
