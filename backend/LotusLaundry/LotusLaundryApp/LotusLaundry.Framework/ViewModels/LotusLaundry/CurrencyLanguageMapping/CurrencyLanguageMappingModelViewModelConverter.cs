﻿using LotusLaundry.Domain.CurrencyLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants; 

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.CurrencyLanguageMapping
{
    public static class CurrencyLanguageMappingModelViewModelConverter
    {
        public static CurrencyLanguageMappingViewModel ToViewModel(this CurrencyLanguageMappingModel x)
        {
            if (x == null) return new CurrencyLanguageMappingViewModel();
            return new CurrencyLanguageMappingViewModel
            {
                	Id = CryptoEngine.Encrypt(FileType.CURRENCYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				TenantId = x.TenantId,
                	Slug = CryptoEngine.Encrypt(FileType.CURRENCYLANGUAGEMAPPING + "_" + x.Id.ToString(), KeyConstant.Key),
        				CreatedBy = x.CreatedBy,
        				UpdatedBy = x.UpdatedBy,
        				CreatedOn = x.CreatedOn,
        				UpdatedOn = x.UpdatedOn,
        				IsDeleted = x.IsDeleted,
        				IsActive = x.IsActive,
        				CurrencyId = x.CurrencyId,
        				LanguageId = x.LanguageId,
        				Name = x.Name,
				TotalCount=x.TotalCount
            };
        }
		
		 public static CurrencyLanguageMappingModel ToModel(this CurrencyLanguageMappingViewModel x)
        {
            if (x == null) return new CurrencyLanguageMappingModel();
            return new CurrencyLanguageMappingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			CurrencyId = x.CurrencyId,
        			LanguageId = x.LanguageId,
        			Name = x.Name,
            };
        }
    }
}
