﻿
using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain;
using LotusLaundry.Domain.PushNotification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.PushNotification
{
    public static class PushNotificationModelViewModelConverter
    {

       

        public static PushNotificationViewModel ToViewModel(this PushNotificationModel x)
        {
            if (x == null) return new PushNotificationViewModel();
            return new PushNotificationViewModel
            {
                Id = x.Id,
                Title = x.Title,
                Message = x.Message,
                IsSeen = x.IsSeen,
                UserRole = x.UserRole,
                ArchiveDate = x.ArchiveDate,
                ArchivedBy = x.ArchivedBy,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                IsActive = x.IsActive,
                IsArchive = x.IsArchive,
                IsDeleted = x.IsDeleted,
                overall_count = x.overall_count,
                TargetId = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.TargetId.ToString(), KeyConstant.Key),
                TargetType = x.TargetType,
                Type = x.Type,
                UpdatedBy = x.UpdatedBy,
                UpdatedOn = x.UpdatedOn,
                UserId = x.UserId,
                UnreadCount = x.UnreadCount,
            };
        }

        public static PushNotificationModel ToModel(this PushNotificationViewModel x)
        {
            if (x == null) return new PushNotificationModel();
            return new PushNotificationModel
            {
                Id = x.Id,
                Title = x.Title,
                Message = x.Message,
                IsSeen = x.IsSeen,
                UserRole = x.UserRole,
                ArchiveDate = x.ArchiveDate,
                ArchivedBy = x.ArchivedBy,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn,
                IsActive = x.IsActive,
                IsArchive = x.IsArchive,
                IsDeleted = x.IsDeleted,
                overall_count = x.overall_count,
                TargetId = x.TargetId,
                TargetType = x.TargetType,
                Type = x.Type,
                UpdatedBy = x.UpdatedBy,
                UpdatedOn = x.UpdatedOn,
                UserId = x.UserId,
                orderid=x.orderid,
                customerid= x.customerid,
                driverid=x.driverid

            };
        }


        public static BroadcastViewModel ToViewModel(this Broadcastmodel x)
        {
            if (x == null) return new BroadcastViewModel();
            return new BroadcastViewModel
            {
                Id = x.Id.GetValueOrDefault(),
               
                Message = x.message,
                totalBatch = x.Totalbatche,
                lastBatch = x.lastBatch,
                totalUser = x.TotalUsers,
                pendingUser = x.PendingUsers,
                CreatedBy = x.CreatedBy,
                CreatedOn = x.CreatedOn.GetValueOrDefault(),
               
                overall_count = x.OverallCount,
               
                UpdatedBy = x.UpdatedBy,
               
            };
        }

    }
}
