﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.PushNotification
{
    public class BroadcastViewModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("ids")]
        public string Ids { get; set; }
        [JsonProperty("createdBy")]
        public Nullable<long> CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public Nullable<long> UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public System.DateTimeOffset CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public System.DateTimeOffset UpdatedOn { get; set; }
        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }
        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("isArchive")]
        public bool IsArchive { get; set; }
        [JsonProperty("targetId")]
        public string TargetId { get; set; }
        [JsonProperty("targetType")]
        public string TargetType { get; set; }
        [JsonProperty("userId")]
        public Nullable<long> UserId { get; set; }
        [JsonProperty("userRole")]
        public string UserRole { get; set; }
        [JsonProperty("isSeen")]
        public bool IsSeen { get; set; }
        [JsonProperty("archivedBy")]
        public Nullable<long> ArchivedBy { get; set; }
        [JsonProperty("archiveDate")]
        public Nullable<System.DateTimeOffset> ArchiveDate { get; set; }
        [JsonProperty("overall_count")]
        public Nullable<int> overall_count { get; set; }
        [JsonProperty("fromUserId")]
        public long? FromUserId { get; set; }
        [JsonProperty("fromUserName")]
        public string FromUserName { get; set; }
        [JsonProperty("toUserName")]
        public string ToUserName { get; set; }
        [JsonProperty("toUserId")]
        public Nullable<long> ToUserId { get; set; }
        [JsonProperty("unreadCount")]
        public int? UnreadCount { get; set; }


        [JsonProperty("totalBatch")]
        public long? totalBatch { get; set; }

        [JsonProperty("lastBatch")]
        public long? lastBatch { get; set; }

        [JsonProperty("totalUser")]
        public long? totalUser { get; set; }

        [JsonProperty("pendingUser")]
        public long? pendingUser { get; set; }


    }
}
