﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.Product
{
    public static class ProductModelViewModelConverter
    {
        public static ProductViewModel ToViewModel(this ProductModel x)
        {
            if (x == null) return new ProductViewModel();
            return new ProductViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name,
                Gender = x.Gender,
                Price = x.Price,
                Icon = (x.Image != null && x.Image.Count() > 0) ? x.Image.FirstOrDefault().Path : null
            };
        }
    }
}
