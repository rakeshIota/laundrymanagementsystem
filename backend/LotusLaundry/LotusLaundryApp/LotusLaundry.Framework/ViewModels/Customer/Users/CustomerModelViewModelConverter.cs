﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer;
using LotusLaundry.Framework.ViewModels.Customer.UserAddress;

using LotusLaundry.Domain.Users;
using LotusLaundry.Framework.WebExtensions;
using System.Globalization;

namespace LotusLaundry.Framework.ViewModels.Customer.Users
{
    public static class CustomerModelViewModelConverter
    {
        

        public static UsersModel ToModel(this CustomerViewModel x)
        {
            if (x == null) return new UsersModel();
            return new UsersModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,

                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                IsDeleted = x.IsDeleted,
                //UserName = x.UserName,
                //FirstName = x.FirstName,
                //LastName = x.LastName,
                //Email = x.Email,

                //Country = x.Country,
                //File = x.File != null ? x.File.Select(y => y.ToModel()).ToList() : null,

                File = x.File != null ? x.File.ToModel():null,



                FirstName = x.FirstName,
                LastName = x.LastName,
                UserName = x.Email,
                FullName = x.FirstName + " " + x.LastName,
                //ProfileImageUrl = x.ProfilePic,
               // LanguageId = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key),
                MobileNo = x.PhoneNumber,
                NickName = x.NickName,
                Email = x.Email,
                PhoneNumber = x.PhoneNumber,
                // PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                //EmailConfirmed = x.EmailConfirmed,
                DOB = x.DateOfbirth,
                Gender = x.Gender,

                AllFile = x.AllFile != null ? x.AllFile.Select(y => y.ToModel()).ToList() : null,

            };
        }

        public static CustomerViewModel ToViewModel(this UsersModel x)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;
            if (x == null) return new CustomerViewModel();
            return new CustomerViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + x.Id.ToString(), KeyConstant.Key),
                FirstName = x.FirstName,
                LastName = x.LastName,
                FullName = txtInfo.ToTitleCase( x.FirstName + " " + x.LastName),
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Address = x.Address,
                City = x.City,
                Code = x.Code,
                Country = x.Country,
                CreatedBy = x.CreatedBy,
                FacebookId = x.FacebookId,
                Gender = x.Gender,
                GoogleId = x.GoogleId,
                IsActive = x.IsActive,
                IsDeleted = x.IsDeleted,
                IsFacebookConnected = x.IsFacebookConnected,
                IsGoogleConnected = x.IsGoogleConnected,
                ProfileImageUrl = x.ProfileImageUrl,
                RoleName = x.RoleName,
                Role = x.Role,

                Position = x.Position,
                employeeid = x.employeeid,

                NationalId = x.NationalId,
                DateOfbirth = x.DOB,
                LicenceId = x.LicenceId,
                LicenseNo = x.LicenceId,
                BikeInformation = x.BikeInformation,
                LicencePlate = x.LicencePlate,
                EmailConfirmed = x.EmailConfirmed,
                status = x.status,
                MobileNo = x.MobileNo,
                File= x.AllFile != null ? x.AllFile.FirstOrDefault().ToViewModel() : null,
                AllFile = x.AllFile != null ? x.AllFile.Select(y => y.ToViewModel()).ToList() : null,
                NickName = x.NickName,
                PhoneNumberConfirmed=x.PhoneConfirmed,
                UserName=x.userName,
                LanguageId= CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.LanguageId.ToString(), KeyConstant.Key),


                addressdetails =  x.AddressDetail.Count>0  ? x.AddressDetail.Select(y => y.ToViewModel()).ToList() : null,
            };
        }
    }
}
