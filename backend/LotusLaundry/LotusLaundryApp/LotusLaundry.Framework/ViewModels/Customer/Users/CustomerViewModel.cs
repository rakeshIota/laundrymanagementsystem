﻿using LotusLaundry.Framework.ViewModels.Customer.UserAddress;
using LotusLaundry.Framework.ViewModels.Upload;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.Users
{
    public class CustomerViewModel  
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("emailConfirmed")]
        public bool? EmailConfirmed { get; set; }
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }
       
        [JsonProperty("fullName")]
        public string FullName { get; set; }
        [JsonProperty("userProfileEmail")]
        public string UserProfileEmail { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("profileimageUrl")]
        public string ProfileImageUrl { get; set; }
        [JsonProperty("roleName")]
        public string RoleName { get; set; }
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("mobileNo")]
        public string MobileNo { get; set; }
        [JsonProperty("createdBy")]
        public Nullable<long> CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public Nullable<long> UpdatedBy { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("pin")]
        public int? Pin { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("uniqueCode")]
        public string UniqueCode { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }
        [JsonProperty("base64String")]
        public string Base64String { get; set; }
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }
        [JsonProperty("roles")]
        public IList<string> Roles { get; set; }
        [JsonProperty("isFacebookConnected")]
        public bool? IsFacebookConnected { get; set; }
        [JsonProperty("isGoogleConnected")]
        public bool? IsGoogleConnected { get; set; }
        [JsonProperty("facebookId")]
        public string FacebookId { get; set; }
        [JsonProperty("googleId")]
        public string GoogleId { get; set; }

        [JsonProperty("OTP")]
        public long? OTP { get; set; }
        [JsonProperty("oTPValidTill")]
        public DateTime? OTPValidTill { get; set; }
        [JsonProperty("licenseNo")]
        public string LicenseNo { get; set; }
        [JsonProperty("languageId")]
        public string LanguageId { get; set; }

        [JsonProperty("nickName")]
        public string NickName { get; set; }


        [JsonProperty("pinNo")]
        public string PinNo { get; set; }

        [JsonProperty("mobileNoConfirmed")]
        public bool? PhoneNumberConfirmed { get; set; }

        [JsonProperty("contactAddress")]
        public CustomerAddressViewModel ContactAddress { get; set; }

        [JsonProperty("deliveryAddress")]
        public CustomerAddressViewModel DeliveryAddress { get; set; }


        [JsonProperty("locality")]
        public string Locality { get; set; }

        [JsonProperty("commonRoleName")]
        public string Role { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("DOB")]
        public DateTime? DateOfbirth { get; set; }


        [JsonProperty("profilePic")]
        public FileGroupItemsViewModel File { get; set; }

        [JsonProperty("attachments")]
        public List< FileGroupItemsViewModel> AllFile { get; set; }

        [JsonProperty("employeeId")]
        public string employeeid { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        //
        [JsonProperty("nationalId")]
        public string NationalId { get; set; }
        [JsonProperty("licenseId")]
        public string LicenceId { get; set; }
        [JsonProperty("bikeInformation")]
        public string BikeInformation  { get; set; }
        [JsonProperty("licencePlate")]
        public string LicencePlate { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }


        [JsonProperty("addressdetails")]
        public List<CustomerAddressViewModel> addressdetails { get; set; }

    }

    public class CustomerCodeViewModel
    {
        [JsonProperty("uniqueCode")]
        public string uniqueCode { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }

}
