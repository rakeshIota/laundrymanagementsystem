﻿using LotusLaundry.Domain.CartItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;
using LotusLaundry.Domain.OrderItem;

namespace LotusLaundry.Framework.ViewModels.Customer.orderItem
{
    public static class CustomerOrderItemModelViewModelConverter
    {
        public static CustomerOrderItemViewModel ToViewModel(this OrderItemModel x)
        {
            if (x == null) return new CustomerOrderItemViewModel();
            return new CustomerOrderItemViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.ORDERITEM + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CARTITEM + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                //CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //CartId = x.CartId,
                Product =  new Product.ProductViewModel {
                    Id = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.ProductId.ToString(), KeyConstant.Key),
                    Name =  x.ProductName,
                    Icon =  x.Icon
                },
                Service =  new Service.ServiceViewModel
                {
                    Id = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.ServiceId.ToString(), KeyConstant.Key),
                    Name = x.ServiceName
                },
                ProductId = x.ProductId,
                Quantity = x.Quantity,
                Price = x.SubTotal,
                TotalPrice=x.TotalPrice,
                //TotalPrice = x.TotalPrice,
                //TotalCount = x.TotalCount
            };
        }

        public static OrderItemModel ToModel(this CustomerOrderItemViewModel x)
        {
            if (x == null) return new OrderItemModel();
            return new OrderItemModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                //CartId = x.CartId,
                ProductId = x.ProductId,
                Quantity = x.Quantity,
               // Price = x.Price,
                TotalPrice = x.TotalPrice,
            };
        }



        public static OrderItemModel ToModel(this CartItemCustomerViewModel x)
        {
            if (x == null) return new OrderItemModel();
            return new OrderItemModel
            {
                ProductId = x.ProductId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProductId.ToString(), KeyConstant.Key)) : 0,
                ServiceId = x.ServiceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ServiceId.ToString(), KeyConstant.Key)) : 0,
                Quantity = x.Quantity
            };
        }
    }
}
