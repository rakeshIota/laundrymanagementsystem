﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.Customer.Product;
using LotusLaundry.Framework.ViewModels.Customer.Service;

namespace LotusLaundry.Framework.ViewModels.Customer.orderItem
{
    public class CustomerOrderItemViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("cartId")]
        public long? CartId { get; set; }
        [JsonProperty("productId")]
        public long? ProductId { get; set; }
        [JsonProperty("quantity")]
        public int? Quantity { get; set; } 
        [JsonProperty("price")]
        public decimal? Price { get; set; }
        [JsonProperty("totalPrice")]
        public decimal? TotalPrice { get; set; }
        [JsonIgnore]
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("product")]
        public ProductViewModel Product { get; set; }
        [JsonProperty("service")]
        public ServiceViewModel Service { get; set; }
    }


    public class CartItemCustomerViewModel
    {
        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("productId")]
        public string ProductId { get; set; }



    }
}

