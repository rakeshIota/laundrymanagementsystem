﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.SubDistrict;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.SubDistrict
{
    public static class SubDistrictModelViewModelConverter
    {

        public static SubDistrictViewModel ToViewModel(this SubDistrictModel x)
        {
            if (x == null) return new SubDistrictViewModel();
            return new SubDistrictViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SUBDISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name,

            };
        }
    }
}
