﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.District;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.District
{
   public static class DistrictModelViewModelConverter
    {
        public static DistrictViewModel ToViewModel(this DistrictModel x)
        {
            if (x == null) return new DistrictViewModel();
            return new DistrictViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.DISTRICT + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name,
              
            };
        }
    }
}
