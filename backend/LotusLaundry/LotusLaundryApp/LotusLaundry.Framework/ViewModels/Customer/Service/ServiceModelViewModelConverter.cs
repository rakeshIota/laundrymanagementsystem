﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.Service
{
   public static class ServiceModelViewModelConverter
    {
        public static ServiceViewModel ToViewModel(this ServiceModel x)
        {
            if (x == null) return new ServiceViewModel();
            return new ServiceViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name,
                Description = x.Description,
                Icon =  (x.Image != null && x.Image.Count() > 0) ? x.Image.FirstOrDefault().Path: null
            };
        }
    }
}
