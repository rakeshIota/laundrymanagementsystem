﻿using LotusLaundry.Domain.CartItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;

namespace LotusLaundry.Framework.ViewModels.Customer.CartItem
{
    public static class CartItemModelViewModelConverter
    {
        public static CartItemViewModel ToViewModel(this CartItemModel x)
        {
            if (x == null) return new CartItemViewModel();
            return new CartItemViewModel 
            {
                Id = CryptoEngine.Encrypt(FileType.CARTITEM + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CARTITEM + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                //CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //CartId = x.CartId,
                Product =  new Product.ProductViewModel {
                    Id = CryptoEngine.Encrypt(FileType.PRODUCT + "_" + x.ProductId.ToString(), KeyConstant.Key),
                    Name =  x.ProductName,
                    Icon =  x.Icon
                },
                Service =  new Service.ServiceViewModel
                {
                    Id = CryptoEngine.Encrypt(FileType.SERVICE + "_" + x.ServiceId.ToString(), KeyConstant.Key),
                    Name = x.ServiceName
                },
                //ProductId = x.ProductId,
                Quantity = x.Quantity,
                Price = x.Price,
                //TotalPrice = x.TotalPrice,
                //TotalCount = x.TotalCount
            };
        }

        public static CartItemModel ToModel(this CartItemViewModel x)
        {
            if (x == null) return new CartItemModel();
            return new CartItemModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                ServiceId= x.serviceIdCode != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.serviceIdCode.ToString(), KeyConstant.Key)) : 0,
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                CartId = x.CartId,
                ProductId = x.ProductId,
                Quantity = x.Quantity,
                Price = x.Price,
                TotalPrice = x.TotalPrice,
            };
        }



        public static CartItemModel ToModel(this CartItemCustomerViewModel x)
        {
            if (x == null) return new CartItemModel();
            return new CartItemModel
            {
                ProductId = x.ProductId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ProductId.ToString(), KeyConstant.Key)) : 0,
                ServiceId = x.ServiceId != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.ServiceId.ToString(), KeyConstant.Key)) : 0,
                Quantity = x.Quantity,
                
            };
        }
    }
}
