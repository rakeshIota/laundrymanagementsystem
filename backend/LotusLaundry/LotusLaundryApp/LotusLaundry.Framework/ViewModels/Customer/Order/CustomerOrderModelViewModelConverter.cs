﻿using LotusLaundry.Domain.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.CartItem;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Framework.ViewModels.Customer.orderItem;
using LotusLaundry.Framework.WebExtensions;
using LotusLaundry.Domain.Order;

namespace LotusLaundry.Framework.ViewModels.Customer.Order
{
    public static class CustomerOrderModelViewModelConverter
    {
        public static CustomerOrderViewModel ToViewModel(this OrderModel x)
        {
            if (x == null) return new CustomerOrderViewModel();
            return new CustomerOrderViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CART + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //IsLocked = x.IsLocked,
                //Userid = CryptoEngine.Encrypt(FileType.CART + "_" + x.UserId.ToString(), KeyConstant.Key) ,
                Status = x.Status,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                //TotalPrice = x.TotalPrice,
                TotalItems = x.TotalItems,
               // OrderId = x.Id.ToString(),
                //  OrderId = x.Id.ToString(), //number.ToString().PadLeft(4, '0');

                OrderId = x.Id.ToString().PadLeft(6, '0'),


                LogisticCharge = x.LogisticCharge,
                TotalPrice=x.TotalPrice,
                Discount=x.Discount,
                paymentResponse=x.paymentResponse,
                paymentStatus=x.paymentStatus,
                paymentType=x.PaymentType,

                //TotalCount = x.TotalCount,
                OrderItems = x.OrderItems != null ? x.OrderItems.Select(y => y.ToViewModel()).ToList() : null,
                DeliveryType = x.DeliveryType,
                deliveryPrice=x.DeliveryPrice,
                DeliveryDate=x.DeliveryDate,
                PickupDate=x.PickupDate,
                deliverySlot=x.deliverySlot,
                pickupSlot=x.pickupSlot,
                
                //DeliveryAddress= x.DeliveryAddress != null ? x.DeliveryAddress.Select(y => y.ToViewModel()).ToList() : null,
                //driverDetails = x.DriverDetails!=null  ? x.DriverDetails.Select(y => y.ToViewModel()).ToList() : null,

                DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.FirstOrDefault().ToViewModel() : null,
                driverDetails = x.DriverDetails != null  ? x.DriverDetails.FirstOrDefault().ToViewModel() : null,

            };
        }

        public static CustomerOrderViewModel ToDetailsViewModel(this OrderModel x)
        {
            if (x == null) return new CustomerOrderViewModel();
            return new CustomerOrderViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CART + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //IsLocked = x.IsLocked,
                //Userid = CryptoEngine.Encrypt(FileType.CART + "_" + x.UserId.ToString(), KeyConstant.Key) ,
                Status = x.Status,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                //TotalPrice = x.TotalPrice,
                TotalItems = x.TotalItems,
                //  OrderId = x.Id.ToString(), //number.ToString().PadLeft(4, '0');

                OrderId = x.Id.ToString().PadLeft(6, '0'),

                LogisticCharge = x.LogisticCharge,
                TotalPrice = x.TotalPrice,
                Discount = x.Discount,
                paymentResponse = x.paymentResponse,
                paymentStatus = x.paymentStatus,
                paymentType = x.PaymentType,
                IsRating = x.isRating,
                //TotalCount = x.TotalCount,
                OrderItems = x.OrderItems != null ? x.OrderItems.Select(y => y.ToViewModel()).ToList() : null,
                DeliveryType = x.DeliveryType,
                deliveryPrice = x.DeliveryPrice,
                DeliveryDate = x.DeliveryDate,
                PickupDate = x.PickupDate,
                deliverySlot = x.deliverySlot,
                pickupSlot = x.pickupSlot,
                collectedDate = x.collectedDate,
                deliveredDate = x.deliveredDate,
                change = x.change != null ? x.change : 0,
                // DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.Select(y => y.ToViewModel()).ToList() : null,

                DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.FirstOrDefault().ToViewModel() : null,
                driverDetails = x.DriverDetails.Count > 0 ? x.DriverDetails.FirstOrDefault().ToViewModel() : null,

                CustomerDetails = x.customerdetails != null ? x.customerdetails.ToViewModel() : null,
                qrCode = x.qrCode,
                isDelivered = x.Isdelivered == 0 ? false : true,
                isPickup = x.IsPickup == 0 ? false :true,
                activeOrder=x.activeOrders


            };
        }


        public static CustomerOrderBasicViewModel ToBasicViewModel(this OrderModel x)
        {
            if (x == null) return new CustomerOrderBasicViewModel();
            return new CustomerOrderBasicViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CART + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //IsLocked = x.IsLocked,
                //Userid = CryptoEngine.Encrypt(FileType.CART + "_" + x.UserId.ToString(), KeyConstant.Key) ,
                Status = x.Status,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                //TotalPrice = x.TotalPrice,
                TotalItems = x.TotalItems,
                //  OrderId = x.Id.ToString(), //number.ToString().PadLeft(4, '0');

                OrderId = x.Id.ToString().PadLeft(6, '0'),

                LogisticCharge = x.LogisticCharge,
                TotalPrice = x.TotalPrice,
                Discount = x.Discount,
                paymentResponse = x.paymentResponse,
                paymentStatus = x.paymentStatus,
                paymentType = x.PaymentType,
                IsRating = x.isRating,
                //TotalCount = x.TotalCount,
                OrderItems = x.OrderItems != null ? x.OrderItems.Select(y => y.ToViewModel()).ToList() : null,
                DeliveryType = x.DeliveryType,
                deliveryPrice = x.DeliveryPrice,
                DeliveryDate = x.DeliveryDate,
                PickupDate = x.PickupDate,
                deliverySlot = x.deliverySlot,
                pickupSlot = x.pickupSlot,
                collectedDate = x.collectedDate,
                deliveredDate = x.deliveredDate,
                change = x.change != null ? x.change : 0,
                // DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.Select(y => y.ToViewModel()).ToList() : null,

                DeliveryAddress = x.DeliveryAddress != null ? x.DeliveryAddress.FirstOrDefault().ToViewModel() : null,
                driverDetails = x.DriverDetails.Count > 0 ? x.DriverDetails.FirstOrDefault().ToViewModel() : null,

                CustomerDetails = x.customerdetails != null ? x.customerdetails.ToViewModel() : null,
                qrCode = x.qrCode,
                isDelivered = x.Isdelivered == 0 ? false : true,
                isPickup = x.IsPickup == 0 ? false : true
                //activeOrder = x.activeOrders


            };
        }



        public static OrderModel ToModel(this CustomerOrderViewModel x)
        {
            if (x == null) return new OrderModel();
            return new OrderModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
               
                UserId = x.Userid,
                Status = x.Status,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                TotalPrice = x.TotalPrice,
                TotalItems = x.TotalItems,
                DeliveryType = x.DeliveryType,
                paymentStatus=x.paymentStatus,
                DeliveryDate = x.DeliveryDate,
                PickupDate = x.PickupDate,
                deliverySlot = x.deliverySlot,
                pickupSlot = x.pickupSlot
            };
        }
    }
}
