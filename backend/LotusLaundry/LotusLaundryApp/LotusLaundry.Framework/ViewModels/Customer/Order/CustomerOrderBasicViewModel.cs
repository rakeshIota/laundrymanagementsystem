﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.Customer.CartItem;
using LotusLaundry.Framework.ViewModels.LotusLaundry.OrderItem;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using LotusLaundry.Framework.ViewModels.Customer.orderItem;
using LotusLaundry.Domain.Users;
using LotusLaundry.Framework.ViewModels.Users;

namespace LotusLaundry.Framework.ViewModels.Customer.Order
{
    public class CustomerOrderBasicViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [Required(ErrorMessage = "Is Locked is requried")]
        [JsonProperty("isLocked")]
        public bool? IsLocked { get; set; }
        [Required(ErrorMessage = "Customer is requried")]
        [Range(0, 9999999999999999, ErrorMessage = "Customer value is more than max length of 100")]
        [JsonProperty("userId")]
        public long? Userid { get; set; }
        [Required(ErrorMessage = "Status is requried")]
        [MaxLength(30, ErrorMessage = "Status Max length 30")]
        [JsonProperty("status")]
        public string Status { get; set; }
        [Required(ErrorMessage = "Tax is requried")]
        [Range(0.1, 100, ErrorMessage = "Tax value is more than max limit of 100")]
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Tax")]
        [JsonProperty("tax")]
        public decimal? Tax { get; set; }
        [Required(ErrorMessage = "Sub Total is requried")]
        [Range(0.1, 100, ErrorMessage = "Sub Total value is more than max limit of 100")]
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Sub Total")]
        [JsonProperty("subTotal")]
        public decimal? SubTotal { get; set; }
        [Required(ErrorMessage = "Total Price is requried")]
        [Range(0.1, 100, ErrorMessage = "Total Price value is more than max limit of 100")]
        [RegularExpression(RegularExpressionType.DecimalNumber, ErrorMessage = "Invalid decimal value for Total Price")]
        [JsonProperty("totalPrice")]
        public decimal? TotalPrice { get; set; }
        [Required(ErrorMessage = "Total Items is requried")]
        [Range(0, 9999999999999999, ErrorMessage = "Total Items value is more than max length of 100")]
        [RegularExpression(RegularExpressionType.OnlyNumber, ErrorMessage = "Total Items must be numeric")]
        [JsonProperty("totalItems")]
        public int? TotalItems { get; set; }
        [JsonIgnore]
        [JsonProperty("totalCount")] 
        public int TotalCount { get; set; }
        [JsonProperty("orderItems")]
        public List<CustomerOrderItemViewModel> OrderItems { get; set; }
        [JsonProperty("deliveryType")]
        public string DeliveryType { get; set; }

        [JsonProperty("deliveryDate")]
        public DateTimeOffset? DeliveryDate { get; set; }

        [JsonProperty("pickupDate")]
        public DateTimeOffset? PickupDate { get; set; }

        [JsonProperty("deliverySlot")]
        public string deliverySlot { get; set; }

        [JsonProperty("deliveryPrice")]
        public decimal? deliveryPrice { get; set; }

        [JsonProperty("pickupSlot")]
        public string pickupSlot { get; set; }



        [JsonProperty("logisticCharge")]
        public decimal? LogisticCharge { get; set; }

        [JsonProperty("discount")]
        public decimal? Discount { get; set; }

        [JsonProperty("paymentResponse")]
        public string paymentResponse { get; set; }

        [JsonProperty("paymentType")]
        public string paymentType { get; set; }

        [JsonProperty("paymentStatus")]
        public string paymentStatus { get; set; }

        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("deliveryAddress")]
        public UserAddressViewModel DeliveryAddress { get; set; }

        [JsonProperty("driverDetails")]

        public UsersViewModel driverDetails { get; set; }

        [JsonProperty("rating")]
        public bool? IsRating { get; set; }


        [JsonProperty("customerDetails")]

        public UsersViewModel CustomerDetails { get; set; }

        [JsonProperty("collectedDate")]
        public DateTimeOffset? collectedDate { get; set; }

        [JsonProperty("deliveredDate")]
        public DateTimeOffset? deliveredDate { get; set; }

        [JsonProperty("change")]
        public decimal? change { get; set; }

        [JsonProperty("qrCode")]
        public string qrCode { get; set; }

        [JsonProperty("isDelivered")]
        public bool? isDelivered { get; set; }

        [JsonProperty("isPickup")]
        public bool? isPickup { get; set; }

       // [JsonIgnore]
       

    }
}

