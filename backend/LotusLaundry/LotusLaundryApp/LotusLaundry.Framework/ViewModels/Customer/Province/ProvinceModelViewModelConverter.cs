﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Province;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.Province
{
    public static class ProvinceModelViewModelConverter
    {

        public static ProvinceViewModel ToViewModel(this ProvinceModel x)
        {
            if (x == null) return new ProvinceViewModel();
            return new ProvinceViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.PROVINCE + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name
            };
        }
    }
}
