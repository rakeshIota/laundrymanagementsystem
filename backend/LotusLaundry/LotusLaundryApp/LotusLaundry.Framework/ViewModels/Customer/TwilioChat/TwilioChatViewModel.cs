﻿using Newtonsoft.Json;

namespace LotusLaundry.Framework.ViewModels.Customer.TwilioChat
{
    public class TwilioChatViewModel
    {
        [JsonProperty("roleSid")]
        public string RoleSid { get; set; }
        [JsonProperty("pathSid")]
        public string PathSid { get; set; }
        [JsonProperty("channelSid")]
        public string ChannelSid { get; set; }
        [JsonProperty("body")]
        public string Body { get; set; }
        [JsonProperty("userRole")]
        public string UserRole { get; set; }
        [JsonProperty("from")]
        public string From { get; set; }
        [JsonProperty("accountSid")]
        public string AccountSid { get; set; }
        [JsonProperty("authToken")]
        public string AuthToken { get; set; }
        [JsonProperty("identity")]
        public string Identity { get; set; }
    }

}

