﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Domain.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.Language
{
    public static class LanguageModelViewModelConverter
    {
        public static LanguageViewModel ToViewModel(this LanguageModel x)
        {
            if (x == null) return new LanguageViewModel();
            return new LanguageViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.LANGUAGE + "_" + x.Id.ToString(), KeyConstant.Key),
                Name = x.Name,
                Code = x.Code
            };
        }
    }
}
