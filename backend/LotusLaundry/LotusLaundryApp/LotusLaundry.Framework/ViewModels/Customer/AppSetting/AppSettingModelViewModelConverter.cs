﻿using LotusLaundry.Domain.AppSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.AppSetting
{
   public static class AppSettingModelViewModelConverter
    {
        public static AppSettingViewModel ToViewModel(this AppSettingModel x)
        {
            if (x == null) return new AppSettingViewModel();
            return new AppSettingViewModel
            {
                Key = x.Key,
                Value = x.Value
            };
        }

        public static ApplicationSettingViewModel ApplicationToViewModel(this ApplicationSettingModel x)
        {
            if (x == null) return new ApplicationSettingViewModel();
            return new ApplicationSettingViewModel
            {
                EXPRESS_DELIVERY_RATE=x.EXPRESS_DELIVERY_RATE,
                GoogleKeyRefresh=x.GoogleKeyRefresh,
                NORMAL_DELIVERY_RATE=x.NORMAL_DELIVERY_RATE,
                SAMEDAY_DELIVERY_RATE=x.SAMEDAY_DELIVERY_RATE,
                timeSlot=x.timeSlot,

                deliveryStartTime=x.deliveryStartTime,
                expressEndTime=x.expressEndTime,
                normalEndTime=x.normalEndTime,
                sameDayEndTime=x.sameDayEndTime,
                logisticCharge=x.logisticCharge,
                minimumOrderAmount=x.minimumOrderAmount,
                availablePaymentTypes=x.availablePaymentTypes,

                driverLocationInterval=x.driverLocationInterval

            };

        }
    }
}
