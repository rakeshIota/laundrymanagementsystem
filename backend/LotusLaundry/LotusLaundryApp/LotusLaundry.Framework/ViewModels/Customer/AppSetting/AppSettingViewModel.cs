﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotusLaundry.Framework.ViewModels.Customer.AppSetting
{
   public class AppSettingViewModel
    {
        [JsonProperty("key")]
        public string Key { get; set; }
       
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class ApplicationSettingViewModel
    {
        [JsonProperty("samedayDeliveryRate")]
        public string SAMEDAY_DELIVERY_RATE { get; set; }

        [JsonProperty("expressDeliveryRate")]
        public string EXPRESS_DELIVERY_RATE { get; set; }

        [JsonProperty("normalDeliveryRate")]
        public string NORMAL_DELIVERY_RATE { get; set; }
        [JsonProperty("googleKeyRefresh")]
        public string GoogleKeyRefresh { get; set; }


        [JsonProperty("timeSlot")]
        public string timeSlot { get; set; }



        [JsonProperty("sameDayEndTime")]
        public string sameDayEndTime { get; set; }

        [JsonProperty("expressEndTime")]
        public string expressEndTime { get; set; }

        [JsonProperty("normalEndTime")]
        public string normalEndTime { get; set; }

        [JsonProperty("deliveryStartTime")]
        public string deliveryStartTime { get; set; }

        // minimumOrderAmount

        [JsonProperty("minimumOrderAmount")]
        public string minimumOrderAmount { get; set; }

        [JsonProperty("logisticCharge")]
        public string logisticCharge { get; set; }
        //availablePaymentTypes
        [JsonProperty("availablePaymentTypes")]
        public string availablePaymentTypes { get; set; }


        //
        [JsonProperty("driverLocationInterval")]
        public string driverLocationInterval { get; set; }
    }
}
