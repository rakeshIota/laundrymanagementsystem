﻿using LotusLaundry.Domain.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Customer.CartItem;

namespace LotusLaundry.Framework.ViewModels.Customer.Cart
{
    public static class CartModelViewModelConverter
    {
        public static CartViewModel ToViewModel(this CartModel x)
        {
            if (x == null) return new CartViewModel();
            return new CartViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.CART + "_" + x.Id.ToString(), KeyConstant.Key),
                //TenantId = x.TenantId,
                //Slug = CryptoEngine.Encrypt(FileType.CART + "_" + x.Id.ToString(), KeyConstant.Key),
                //CreatedBy = x.CreatedBy,
                //UpdatedBy = x.UpdatedBy,
                //CreatedOn = x.CreatedOn,
                //UpdatedOn = x.UpdatedOn,
                //IsDeleted = x.IsDeleted,
                //IsActive = x.IsActive,
                //IsLocked = x.IsLocked,
                //CustomerId = x.CustomerId,
                //Status = x.Status,
                //Tax = x.Tax,
                //SubTotal = x.SubTotal,
                //TotalPrice = x.TotalPrice,
                //TotalItems = x.TotalItems,
                //TotalCount = x.TotalCount,
                CartItems = x.CartItems != null ? x.CartItems.Select(y => y.ToViewModel()).ToList():null,
                DeliveryType = x.DeliveryType,

                DeliveryDate=x.DeliveryDate,
                PickupDate=x.PickupDate,
                deliverySlot=x.deliverySlot,
                pickupSlot=x.pickupSlot,
                laundryInstruction=x.laundryInstruction

            };
        }

        public static CartModel ToModel(this CartViewModel x)
        {
            if (x == null) return new CartModel();
            return new CartModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                IsLocked = x.IsLocked,
                CustomerId = x.CustomerId,
                Status = x.Status,
                Tax = x.Tax,
                SubTotal = x.SubTotal,
                TotalPrice = x.TotalPrice,
                TotalItems = x.TotalItems,
                DeliveryType = x.DeliveryType,

                DeliveryDate = x.DeliveryDate,
                PickupDate = x.PickupDate,
                deliverySlot = x.deliverySlot,
                pickupSlot = x.pickupSlot
            };
        }
    }
}
