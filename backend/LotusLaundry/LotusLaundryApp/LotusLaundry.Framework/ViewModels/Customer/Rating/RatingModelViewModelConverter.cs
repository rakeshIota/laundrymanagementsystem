﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Common.DataSecurity; 
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Driver.Rating;
using LotusLaundry.Domain.Location;
using LotusLaundry.Domain.Rating;

namespace LotusLaundry.Framework.ViewModels.LotusLaundry.Rating
{
    public static class RatingModelViewModelConverter
    {
        public static RatingViewModel ToViewModel(this RatingModel x)
        {
            if (x == null) return new RatingViewModel();
            return new RatingViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.LOCATION + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.LOCATION + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,

                Feedback = x.Feedback,
                OverallSatisfaction = x.OverallSatisfaction,
                Packaging = x.Packaging,

                Staff = x.Staff,
                DeliveryService = x.DeliveryService,
                quality = x.quality,
                orderid = x.Orderid != null ? CryptoEngine.Encrypt(FileType.ORDER + "_" + x.Orderid.ToString(), KeyConstant.Key) : null,
              

                TotalCount = x.TotalCount
            };
        }
		
		 public static RatingModel ToModel(this RatingViewModel x)
        {
            if (x == null) return new RatingModel();
            return new RatingModel
            {
                	Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,                	
        			TenantId = x.TenantId,
                	Slug = x.Slug != null ? CryptoEngine.Decrypt(x.Slug.ToString(), KeyConstant.Key) : "",
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,


                Feedback = x.Feedback,
                OverallSatisfaction = x.OverallSatisfaction,
                Packaging = x.Packaging,

                Staff = x.Staff,
                DeliveryService = x.DeliveryService,
                quality = x.quality,
              
                Orderid = x.orderid != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.orderid.ToString(), KeyConstant.Key)) : 0,
            };
        }
    }
}
