﻿
using LotusLaundry.Framework.ViewModels.Customer.UserCardDetail;
using System;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using LotusLaundry.Domain.UserCardDetail;
using LotusLaundry.Framework.ViewModels.Users;

namespace LotusLaundry.Framework.WebExtensions
{
    public static class UserCardDetailModelViewModelConverter
    {
        public static UserCardDetailViewModel ToViewModel(this UserCardDetailModel x)
        {
            if (x == null) return new UserCardDetailViewModel();
            return new UserCardDetailViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERCARDDETAIL + "_" + x.Id.ToString(), KeyConstant.Key),
                TenantId = x.TenantId,
                Slug = CryptoEngine.Encrypt(FileType.USERCARDDETAIL + "_" + x.Id.ToString(), KeyConstant.Key),
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                
                Brand = x.Brand,
                LastFourDigits = x.LastFourDigits,              
                TotalCount = x.TotalCount,               
                TotalAmount = x.TotalAmount,

                //DeliveryAddress=x.DeliveryAddress,
                CurrencyCode=x.CurrencyCode,
                PaymentType=x.PaymentType,
                TaxAmount = x.TaxAmount
            };
        }

        public static UserCardDetailModel ToModel(this UserCardDetailViewModel x)
        {
            if (x == null) return new UserCardDetailModel();
            return new UserCardDetailModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                TenantId = x.TenantId,
                Slug = x.Id != null ? CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key) : "",
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
              
                Brand = x.Brand,
                LastFourDigits = x.LastFourDigits,             
                TotalAmount = x.TotalAmount,

                //DeliveryAddress=x.DeliveryAddress,
                CurrencyCode = x.CurrencyCode,
                PaymentType = x.PaymentType,
                TaxAmount = x.TaxAmount

            };
        }
    }
}
