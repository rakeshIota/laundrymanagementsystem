﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using LotusLaundry.Common.Constants;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;

namespace LotusLaundry.Framework.ViewModels.Customer.UserCardDetail
{
    public class UserCardDetailViewModel
    { 
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("tenantId")]
        public int? TenantId { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [Required(ErrorMessage = "Deleted? is requried")]
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [Required(ErrorMessage = "Active? is requried")]
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
          
        [MaxLength(50, ErrorMessage = "Brand Max length 50")]
        [JsonProperty("brand")]
        public string Brand { get; set; }
        [MaxLength(10, ErrorMessage = "Last Four Digits Max length 10")]
        [JsonProperty("lastFourDigits")]
        public string LastFourDigits { get; set; }       
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }     
        [JsonProperty("totalAmount")]
        public decimal? TotalAmount { get; set; }



        [JsonProperty("deliveryAddress")]
        public UserAddressViewModel DeliveryAddress { get; set; }


        [JsonProperty("currencyCode")]
        public String CurrencyCode { get; set; }
       
        [JsonProperty("paymentType")]
        public String PaymentType { get; set; }

        [JsonProperty("taxAmount")]
        public decimal? TaxAmount { get; set; }

        [JsonProperty("discount")]
        public decimal? Discount { get; set; }

        [JsonProperty("change")]
        public decimal? change { get; set; }

        [JsonProperty("note")]
        public string note { get; set; }



    }

   
}

