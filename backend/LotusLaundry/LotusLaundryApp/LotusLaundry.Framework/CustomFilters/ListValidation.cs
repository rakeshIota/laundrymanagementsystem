﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace LotusLaundry
{
    public class ListValidation: ValidationAttribute
    {

        private readonly int _min;
        private readonly int _max;

        public ListValidation(int min, int max)
        {
            _min = min;
            _max = max;
        }

        public override bool IsValid(object value)
        {
            var currentListCount = 0;
            var list = value as IList;
            if (list != null)
            {
                foreach (var item in list)
                {
                    var isDeletedValue = item.GetType().GetProperty("IsDeleted").GetValue(item, null);
                    if (isDeletedValue == null || Convert.ToBoolean(isDeletedValue) == false)
                    {
                        currentListCount++;
                    }

                }
            }

            if (currentListCount < _min || currentListCount > _max)
                return false;

            return true;
        }
    }
}
