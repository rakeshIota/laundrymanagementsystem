﻿using LotusLaundry.Common.Constants;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Domain.Upload;
using LotusLaundry.Framework.ViewModels.Upload;
using System;
using System.Linq;

namespace LotusLaundry.Framework.WebExtensions
{
    public static class ViewModelToModel
    {
        public static FileGroupItemsModel ToModel(this FileGroupItemsViewModel x)
        {
            if (x == null) return new FileGroupItemsModel();
            return new FileGroupItemsModel
            {
                Id = x.Id != null ? Convert.ToInt64(CryptoEngine.Decrypt(x.Id.ToString(), KeyConstant.Key)) : 0,
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Filename = x.Filename,
                MimeType = x.MimeType,
                Thumbnail = x.Thumbnail,
                Size = x.Size,
                Path = x.Path,
                OriginalName = x.OriginalName,
                OnServer = x.OnServer,
                TypeId = x.TypeId,
                Type=x.Type
            };
        }
    }
}
