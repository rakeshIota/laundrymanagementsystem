using LotusLaundry.Domain.Upload;
using LotusLaundry.Domain.Users;
using LotusLaundry.Framework.ViewModels.Upload;
using LotusLaundry.Framework.ViewModels.Users;
using LotusLaundry.Common.Extensions;
using LotusLaundry.Common.DataSecurity;
using LotusLaundry.Common.Constants;
using System.Linq;
using LotusLaundry.Framework.ViewModels.LotusLaundry.UserAddress;
using System.Globalization;

namespace LotusLaundry.Framework.WebExtensions
{
   
    public static class ModelToViewModel
    {
       
        public static FileGroupItemsViewModel ToViewModel(this FileGroupItemsModel x)
        {
            if (x == null) return new FileGroupItemsViewModel();
            return new FileGroupItemsViewModel
            {
                Id = x.Id != null ? CryptoEngine.Encrypt(FileType.FILEGROUPITEM + "_" + x.Id.ToString(), KeyConstant.Key) :null,
                CreatedBy = x.CreatedBy,
                UpdatedBy = x.UpdatedBy,
                CreatedOn = x.CreatedOn,
                UpdatedOn = x.UpdatedOn,
                IsDeleted = x.IsDeleted,
                IsActive = x.IsActive,
                Filename = x.Filename,
                MimeType = x.MimeType,
                Thumbnail = x.Thumbnail,
                Size = x.Size,
                Path = x.Path,
                OriginalName = x.OriginalName,
                OnServer = x.OnServer,
                TypeId = x.TypeId,
                Type=x.Type
            };
        }
        public static UsersViewModel ToViewModel(this UsersModel x)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;

            if(x!=null)
            if (x.AddressDetail!=null)
            {
                if(x.AddressDetail.Count()==0)
                {
                    x.AddressDetail = null;
                }
            }
            if (x == null) return new UsersViewModel();
            return new UsersViewModel
            {
                Id = CryptoEngine.Encrypt(FileType.USERS + "_" + x.Id.ToString(), KeyConstant.Key),
                FirstName = txtInfo.ToTitleCase(x.FirstName),
                LastName = txtInfo.ToTitleCase(x.LastName),
                FullName = txtInfo.ToTitleCase( x.FirstName) + " " + txtInfo.ToTitleCase(x.LastName),
                PhoneNumber = x.PhoneNumber,
                Email = x.Email,
                Address = x.Address,
                City = x.City,
                Code = x.Code,
                Country = x.Country,
                CreatedBy = x.CreatedBy,
                FacebookId = x.FacebookId,
                Gender = x.Gender,
                GoogleId = x.GoogleId,
                IsActive = x.IsActive,
                IsDeleted = x.IsDeleted,
                IsFacebookConnected = x.IsFacebookConnected,
                IsGoogleConnected = x.IsGoogleConnected,
                ProfileImageUrl = x.ProfileImageUrl,
                RoleName = x.RoleName,
                Role = x.Role,
                Position = x.Position,
                employeeid = x.employeeid,
                NationalId = x.NationalId,
                DateOfbirth = x.DOB,
                LicenceId = x.LicenceId,
                LicenseNo = x.LicenceId,
                BikeInformation = x.BikeInformation,
                LicencePlate = x.LicencePlate,
                EmailConfirmed = x.EmailConfirmed,
                status = x.status,
                MobileNo = x.MobileNo,
                AllFile = x.AllFile != null ? x.AllFile.Select(y => y.ToViewModel()).ToList() : null,
                NickName = x.NickName,
                addressdetails = x.AddressDetail != null ? x.AddressDetail.Select(y => y.ToViewModel()).ToList() : null,
                workLoad = x.workLoad,
                orderid = x.orderId != null ? x.orderId.ToString().PadLeft(6, '0') : null,
                lastOrderDate=x.lastOrderDate,
                Latitude=x.Latitude,
                Longitude=x.Longititude,
                ordersCount=x.ordersCount,
                adminNote=x.adminNote,
                adminCount=x.adminCount,
                TotalCount = x.TotalCount
            };
        }

        public static UserBasicViewModel TobasicViewModel(this UsersModel x)
        {
            TextInfo txtInfo = new CultureInfo("en-us", false).TextInfo;

            
            if (x == null) return new UserBasicViewModel();
            return new UserBasicViewModel
            {
                Id =  x.Id.ToString(),
                FirstName = txtInfo.ToTitleCase(x.FirstName),
                ProfileImageUrl=x.ProfileImageUrl,
              //LastName = x.LastName,
                //FullName = txtInfo.ToTitleCase(x.FirstName) + " " + txtInfo.ToTitleCase(x.LastName),
                

                TotalCount = x.TotalCount
            };
        }

    }
}