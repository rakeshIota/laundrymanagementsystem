﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using LotusLaundry.Core.Infrastructure;
using LotusLaundry.Service.Upload;
using LotusLaundry.Service.Users;
using LotusLaundry.Service.Xml;
using LotusLaundry.Service.FileGroup;
using LotusLaundry.Service.Users;
using LotusLaundry.Service.Feedback;
using LotusLaundry.Service.UserDevice;
using LotusLaundry.Service.PasswordLog;
using LotusLaundry.Service.MessageLog;
using LotusLaundry.Service.Notification;
using LotusLaundry.Service.AppSetting;
using LotusLaundry.Service.Country;
using LotusLaundry.Service.CountryLanguageMapping;
using LotusLaundry.Service.Language;
using LotusLaundry.Service.State;
using LotusLaundry.Service.StateLanguageMapping;
using LotusLaundry.Service.City;
using LotusLaundry.Service.CityLanguageMapping;
using Customer.Service.UserAddress;
using LotusLaundry.Service.District;
using LotusLaundry.Service.DistrictLanguageMapping;
using LotusLaundry.Service.SubDistrict;
using LotusLaundry.Service.SubDistrictLanguageMapping;
using LotusLaundry.Service.Province;
using LotusLaundry.Service.ProvinceLanguageMapping;
using LotusLaundry.Service.UserCordinate;
using LotusLaundry.Service.Service;
using LotusLaundry.Service.ServiceLanguageMapping;
using LotusLaundry.Service.Product;
using LotusLaundry.Service.ProductLanguageMapping;
using LotusLaundry.Service.ProductPrice;
using LotusLaundry.Service.DeliveryType;
using LotusLaundry.Service.DeliveryTypeLanguageMapping;
using Customer.Service.Cart;
using LotusLaundry.Service.CartItemServiceMapping;
using LotusLaundry.Service.Order;
using LotusLaundry.Service.OrderItem;
using LotusLaundry.Service.OrderItemServiceMapping;
using LotusLaundry.Service.OrderLog;
using LotusLaundry.Service.PaymentLog;
using LotusLaundry.Service.Packing;
using LotusLaundry.Service.PackingLanguageMapping;
using LotusLaundry.Service.DefaultMessage;
using LotusLaundry.Service.DefaultMessageLanguageMapping;
using LotusLaundry.Service.Currency;
using LotusLaundry.Service.CurrencyLanguageMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Service.Exception;
using LotusLaundry.Service.Configuration;
using LotusLaundry.Service.ImageUpload;
using Customer.Service.Service;
using Customer.Service.Product;
using Customer.Service.Language;
using Customer.Service.AppSetting;
using Customer.Service.CartItem;
using Customer.Service.District;
using Customer.Service.SubDistrict;
using Customer.Service.Province;
using LotusLaundry.Service.PostalCode;
using LotusLaundry.Service.NotificationMaster;
using LotusLaundry.Service.NotificationMasterLanguageMapping;
using LotusLaundry.Service.NotificationMasterKeyValueMapping;
using Customer.Service.Customer;
using LotusLaundry.Core.Stripe;
using LotusLaundry.Service.Stripe;
using Driver.Service.Location;
using Driver.Service.Rating;
using CarersCouch.Service.UserTwilioCalling;
using LotusLaundry.Service.UserTwilioCalling;

using LotusLaundry.Service.Promotion;
using Customer.Service.PushNotification;
using Customer.Service.Payment;

namespace LotusLaundry.Framework
{
    public class DependancyRegisterar : IDependencyRegistrar
    {
        public int Order
        {
            get
            {
                return 0;
            }
        }

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            var assemblyArr = typeFinder.GetAssemblies().ToArray();
            builder.RegisterControllers(assemblyArr);
            builder.RegisterApiControllers(assemblyArr);

            builder.RegisterType<ExceptionService>().As<IExceptionService>().InstancePerLifetimeScope();
            builder.RegisterType<UsersService>().As<IUsersService>().InstancePerLifetimeScope();
            builder.RegisterType<FileUploadService>().As<IFileUploadService>().InstancePerLifetimeScope();
            builder.RegisterType<FileGroupService>().As<IFileGroupService>().InstancePerLifetimeScope();
            builder.RegisterType<XmlService>().As<IXmlService>().InstancePerLifetimeScope();
            builder.RegisterType<EmailConfigurationService>().As<IEmailConfigurationService>().InstancePerLifetimeScope();
            builder.RegisterType<ImageService>().As<IImageService>().InstancePerLifetimeScope();

            builder.RegisterType<FeedbackService>().As<IFeedbackService>().InstancePerLifetimeScope();
            builder.RegisterType<UserDeviceService>().As<IUserDeviceService>().InstancePerLifetimeScope();
            builder.RegisterType<PasswordLogService>().As<IPasswordLogService>().InstancePerLifetimeScope();
            builder.RegisterType<MessageLogService>().As<IMessageLogService>().InstancePerLifetimeScope();
            builder.RegisterType<NotificationService>().As<INotificationService>().InstancePerLifetimeScope();
            builder.RegisterType<AppSettingService>().As<IAppSettingService>().InstancePerLifetimeScope();
            builder.RegisterType<CountryService>().As<ICountryService>().InstancePerLifetimeScope();
            builder.RegisterType<CountryLanguageMappingService>().As<ICountryLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<LanguageService>().As<ILanguageService>().InstancePerLifetimeScope();
            builder.RegisterType<StateService>().As<IStateService>().InstancePerLifetimeScope();
            builder.RegisterType<StateLanguageMappingService>().As<IStateLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<CityService>().As<ICityService>().InstancePerLifetimeScope();
            builder.RegisterType<CityLanguageMappingService>().As<ICityLanguageMappingService>().InstancePerLifetimeScope();
           
            builder.RegisterType<DistrictService>().As<IDistrictService>().InstancePerLifetimeScope();
            builder.RegisterType<DistrictLanguageMappingService>().As<IDistrictLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<SubDistrictService>().As<ISubDistrictService>().InstancePerLifetimeScope();
            builder.RegisterType<SubDistrictLanguageMappingService>().As<ISubDistrictLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<ProvinceService>().As<IProvinceService>().InstancePerLifetimeScope();
            builder.RegisterType<ProvinceLanguageMappingService>().As<IProvinceLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<UserCordinateService>().As<IUserCordinateService>().InstancePerLifetimeScope();
            builder.RegisterType<ServiceService>().As<IServiceService>().InstancePerLifetimeScope();
            builder.RegisterType<ServiceLanguageMappingService>().As<IServiceLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductService>().As<IProductService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductLanguageMappingService>().As<IProductLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductPriceService>().As<IProductPriceService>().InstancePerLifetimeScope();
            builder.RegisterType<DeliveryTypeService>().As<IDeliveryTypeService>().InstancePerLifetimeScope();
            builder.RegisterType<DeliveryTypeLanguageMappingService>().As<IDeliveryTypeLanguageMappingService>().InstancePerLifetimeScope();
           
            builder.RegisterType<OrderService>().As<IOrderService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderItemService>().As<IOrderItemService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderItemServiceMappingService>().As<IOrderItemServiceMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderLogService>().As<IOrderLogService>().InstancePerLifetimeScope();
            builder.RegisterType<PaymentLogService>().As<IPaymentLogService>().InstancePerLifetimeScope();
            builder.RegisterType<PackingService>().As<IPackingService>().InstancePerLifetimeScope();
            builder.RegisterType<PackingLanguageMappingService>().As<IPackingLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultMessageService>().As<IDefaultMessageService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultMessageLanguageMappingService>().As<IDefaultMessageLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<CurrencyService>().As<ICurrencyService>().InstancePerLifetimeScope();
            builder.RegisterType<CurrencyLanguageMappingService>().As<ICurrencyLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<PostalCodeService>().As<IPostalCodeService>().InstancePerLifetimeScope();
            builder.RegisterType<NotificationMasterService>().As<INotificationMasterService>().InstancePerLifetimeScope();
            builder.RegisterType<NotificationMasterLanguageMappingService>().As<INotificationMasterLanguageMappingService>().InstancePerLifetimeScope();
            builder.RegisterType<NotificationMasterKeyValueMappingService>().As<INotificationMasterKeyValueMappingService>().InstancePerLifetimeScope();


            ////Customer Service Start
            builder.RegisterType<ServiceCustomerService>().As<IServiceCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductCustomerService>().As<IProductCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<LanguageCustomerService>().As<ILanguageCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<AppSettingCustomerService>().As<IAppSettingCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<DistrictCustomerService>().As<IDistrictCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<SubDistrictCustomerService>().As<ISubDistrictCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<ProvinceCustomerService>().As<IProvinceCustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<UserAddressService>().As<IUserAddressService>().InstancePerLifetimeScope();
            builder.RegisterType<CartService>().As<ICartService>().InstancePerLifetimeScope();
            builder.RegisterType<CartItemService>().As<ICartItemService>().InstancePerLifetimeScope();
            builder.RegisterType<CartItemServiceMappingService>().As<ICartItemServiceMappingService>().InstancePerLifetimeScope();
            ////Customer Service End
            ///
            builder.RegisterType<CustomerService>().As<ICustomerService>().InstancePerLifetimeScope();
            builder.RegisterType<StripeService>().As<IStripeService>().InstancePerLifetimeScope();

            builder.RegisterType<LocationService>().As<ILocationService>().InstancePerLifetimeScope();
            //ILocationService

            builder.RegisterType<UserCallingService>().As<IUserCallingService>().InstancePerLifetimeScope();

            builder.RegisterType<RatingService>().As<IRatingService>().InstancePerLifetimeScope();

            builder.RegisterType<PromotionService>().As<IPromotionService>().InstancePerLifetimeScope();
            builder.RegisterType<PushNotificationService>().As<IPushNotificationService>().InstancePerLifetimeScope();

            builder.RegisterType<PaymentService>().As<IPaymentService>().InstancePerLifetimeScope();



        }
    }
}
