﻿using LotusLaundry.Domain.PostalCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LotusLaundry.Domain;
using LotusLaundry.Domain.Location;

namespace Driver.DBRepository.Location
{
    public class LocationDBService
    {
        DriverdbEntities DbContext { get { return new DriverdbEntities(); } }
        
        public long LocationInsert(LocationModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.DriverLocationInsert(model.TenantId,model.Slug,model.CreatedBy,model.IsActive,model.Longitude,model.Latitude,model.UserId).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void LocationUpdate(LocationModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.DriverLocationUpdate(model.Id,model.TenantId,model.Slug,model.UpdatedBy,model.IsDeleted,model.IsActive,model.Longitude,model.Latitude,model.UserId);
            }

        }
        public List<LocationModel> SelectLocation(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.DriverLocationSelect(param.RecordId, param.Slug, param.Next, param.Offset, param.RelationTable, param.LRelationId, param.TenantId).Select(x => new LocationModel
                {
	            			Id = x.Id,
	            			TenantId = x.TenantId,
	            			Slug = x.Slug,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Longitude = x.Longititude,
                            Latitude=x.Latitude,
                            UserId=x.userId,
                            Name=x.Name,

                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }

      
    }
}
